FROM node:18-alpine AS build-stage
LABEL maintainer="business.intelligence.jds"
WORKDIR /app/
COPY package*.json /app/
RUN apk update
RUN apk upgrade
RUN apk add --no-cache git bash curl
RUN curl -1sLf 'https://dl.cloudsmith.io/public/infisical/infisical-cli/setup.alpine.sh' | bash
RUN apk add infisical=0.14.0
RUN npm ci --cache .npm --prefer-offline --legacy-peer-deps
COPY ./ /app/
ARG INFISICAL_TOKEN
ARG INFISICAL_API_URL
ENV ENV_INFISICAL_TOKEN=${INFISICAL_TOKEN}
ENV ENV_INFISICAL_API_URL=${INFISICAL_API_URL}
RUN infisical run --env=staging --domain=${ENV_INFISICAL_API_URL} --token=${ENV_INFISICAL_TOKEN} -- npm run build:stag

FROM nginx:alpine
COPY --from=build-stage /app/dist/out/ /usr/share/nginx/html/
COPY nginx.conf /etc/nginx/nginx.conf
CMD ["nginx", "-g", "daemon off;"]
