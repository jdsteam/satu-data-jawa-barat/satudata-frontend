FROM node:18-alpine AS build-stage
WORKDIR /app/
COPY package*.json /app/
RUN apk update
RUN apk upgrade
RUN apk add --no-cache git bash curl
RUN curl -1sLf 'https://dl.cloudsmith.io/public/infisical/infisical-cli/setup.alpine.sh' | bash
RUN apk add infisical=0.14.0
RUN npm ci --legacy-peer-deps
COPY ./ /app/
ARG INFISICAL_TOKEN
ARG INFISICAL_API_URL
ENV ENV_INFISICAL_TOKEN=${INFISICAL_TOKEN}
ENV ENV_INFISICAL_API_URL=${INFISICAL_API_URL}
RUN infisical run --env=dev --domain=${ENV_INFISICAL_API_URL} --token=${ENV_INFISICAL_TOKEN} -- npm run build:dev

FROM nginx:1.23.3-alpine
COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=build-stage /app/dist/out/ /usr/share/nginx/html/
