FROM nginx:alpine

LABEL maintainer="business.intelligence.jds"

ARG INFISICAL_TOKEN
ARG INFISICAL_API_URL
ENV ENV_INFISICAL_TOKEN=${INFISICAL_TOKEN}
ENV ENV_INFISICAL_API_URL=${INFISICAL_API_URL}

WORKDIR /app
COPY . .

RUN apk update
RUN apk upgrade
RUN apk add --no-cache nodejs npm py-pip make git bash curl g++
RUN curl -1sLf 'https://dl.cloudsmith.io/public/infisical/infisical-cli/setup.alpine.sh' | bash
RUN apk add infisical=0.14.0

RUN npm ci --legacy-peer-deps
RUN infisical run --env=staging --domain=${ENV_INFISICAL_API_URL} --token=${ENV_INFISICAL_TOKEN} -- npm run build:stag
RUN cp -r ./dist/out/. /usr/share/nginx/html

# Configure NGINX
COPY ./nginx.conf /etc/nginx/nginx.conf

RUN chgrp -R root /var/cache/nginx /var/run /var/log/nginx && \
  chmod -R 770 /var/cache/nginx /var/run /var/log/nginx

EXPOSE 3000

CMD ["nginx", "-g", "daemon off;"]