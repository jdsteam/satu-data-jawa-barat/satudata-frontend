## Merge Request Checklist

Please check if your Merge Request fulfills the following requirements:

- [ ] The commit message follows [Conventional Commits](https://www.conventionalcommits.org/en/)
- [ ] Tests for the changes have been added (for bug fixes / features)
- [ ] Docs have been added / updated (for bug fixes / features)

## Merge Request Type

What kind of change does this Merge Request introduce?

- [ ] Bugfix
- [ ] Feature
- [ ] Refactoring
- [ ] Grunt task
- [ ] Improves performance
- [ ] Code style update
- [ ] Build or CI related changes
- [ ] Documentation content changes
- [ ] Test changes

## What is the current behavior?

Closes # <!-- link to a relevant issue. -->

## What is the new behavior?

## Does this Merge Request introduce a breaking change?

- [ ] Yes
- [ ] No
