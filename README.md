# Satu Data Frontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.12.

## Development server

Run `npm run dev` for a dev server. Navigate to `http://localhost:4300/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `npm run build:prod` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Lint

Run `npm run lint:eslint` to execute the linter via [ESLint](https://eslint.org/).
Run `npm run lint:prettier` to execute the linter via [Prettier](https://prettier.io/).
Run `npm run lint:stylelint` to execute the linter via [Stylelint](https://stylelint.io/).
Run `npm run lint:lockfile` to execute the linter via [Lockfile Linting](https://github.com/lirantal/lockfile-lint).

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
