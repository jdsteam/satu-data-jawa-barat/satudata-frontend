module.exports = [
  {
    id: 'satudata',
    title: 'Satu Data Jabar',
    sub: [
      {
        id: 'what-is-satu-data-jabar',
        title: 'Apa itu Satu Data Jabar',
        content:
          '<div class="mb-20">Satu Data Jabar merupakan aplikasi pendukung percepatan pengambilan keputusan dan perumusan \
          kebijakan yang lebih akurat di lingkungan Pemerintah Provinsi Jawa Barat melalui partisipasi aktif \
          perangkat daerah di Jawa Barat dalam pengelolaan, penyimpanan, pencarian, dan penerjemahan data.</div> \
          <div class="mb-20">Ketersediaan Satu Data Jabar diharapkan dapat membantu tidak hanya pemerintah provinsi namun juga \
          pemerintah Kab/Kota di Jawa Barat untuk memiliki akses data yang mudah, cepat, akurat, dan gratis.</div>',
      },
      {
        id: 'data-classification',
        title: 'Klasifikasi Data',
        content:
          'Data yang tersedia di Satu Data Jabar terbagi tiga klasifikasi, yaitu : \
          <ol> \
            <li>Klasifikasi <strong>Data Publik</strong></li> \
            <div>Data dengan klasifikasi Publik adalah data yang dapat diakses atau diperuntukan untuk masyarakat \
            luas. Seluruh data klasifikasi Publik yang diinput ke dalam aplikasi Satu Data Jabar secara otomatis \
            akan terintegrasi dengan aplikasi Open Data Jabar untuk dapat diakses langsung oleh masyarakat luas.</div> \
            <li>Klasifikasi <strong>Data Internal</strong></li> \
            <div>Data klasifikasi Internal adalah data yang hanya dapat diakses atau diperuntukan khusus untuk seluruh \
            Perangkat Daerah dalam lingkup Provinsi Jawa Barat.</div> \
            <li>Klasifikasi <strong>Data Dikecualikan</strong></li> \
            <div>Data dengan klasifikasi Dikecualikan adalah data yang hanya dapat diakses oleh Perangkat Daerah \
            tertentu yang telah ditentukan hak aksesnya oleh Perangkat Daerah pemilik data.</div> \
          </ol>',
      },
      {
        id: 'users-satu-data-jabar',
        title: 'Pengguna Satu Data Jabar',
        content:
          'Seluruh Perangkat Daerah dalam lingkup Provinsi Jawa Barat berhak untuk menggunakan platform Satu \
          Data Jabar. Berikut adalah tingkatan akun dalam aplikasi Satu Data Jabar : \
          <ol> \
            <li><strong>Walidata Provinsi</strong></li> \
            <div><strong>Bidang Statistik</strong> Dinas Informasi dan Komunikasi Jawa Barat (Diskominfo Jabar) sebagai \
            <strong>Walidata</strong> dapat melakukan pengelolaan terhadap seluruh data yang ada di Satu Data Jabar, \
            yaitu meliputi penambahan data (dataset, indikator, dan visualisasi), perubahan data, pengarsipan data, \
            hingga pemantauan seluruh aktivitas yang terjadi pada aplikasi Satu Data Jabar.</div> \
            <li><strong>OPD Pengelola</strong></li> \
            <div>Seluruh Perangkat Daerah akan diberikan satu akun dengan tingkatan <strong>OPD Pengelola</strong>. \
            OPD Pengelola dapat melihat dan mengakses data dari Perangkat Daerah dalam lingkup Provinsi Jawa Barat \
            yang sudah tersedia dalam aplikasi Satu Data Jabar. Selain itu, akun OPD Pengelola dapat melakukan perubahan \
            data, pengarsipan data, serta penambahan data.</div> \
            <li><strong>OPD Pengguna</strong></li> \
            <div>Akun <strong>OPD Pengguna</strong> dapat digunakan oleh setiap ASN Perangkat Daerah dengan jabatan \
            struktural dalam lingkup Provinsi Jawa Barat. Pada tingkatan OPD Pengguna, pengguna dapat melihat dan \
            mengakses data dari seluruh Perangkat Daerah dalam lingkup Provinsi Jawa Barat yang sudah tersedia dalam \
            aplikasi Satu Data Jabar.</div> \
          </ol>',
      },
      {
        id: 'data-collection-mechanism',
        title: 'Mekanisme Pengumpulan Data',
        content:
          '<div class="mb-20">Satu Data Jabar dikelola oleh Diskominfo Jawa Barat sebagai walidata dari seluruh data yang ada \
          di Pemdaprov Jawa Barat. Data yang tersedia pada Satu Data Jabar adalah data dengan klasifikasi publik, \
          internal, dan dikecualikan.</div>',
      },
      {
        id: 'users-manual-satu-data-jabar',
        title: 'User Manual Satu Data Jabar',
        content: ``,
      },
    ],
  },
  {
    id: 'dataset',
    title: 'Dataset',
    sub: [
      {
        id: 'what-is-dataset',
        title: 'Apa itu Dataset?',
        content:
          'Dataset merupakan kumpulan data-data mentah berupa tabel yang dapat dioleh lebih lanjut.',
      },
      {
        id: 'license-dataset',
        title: 'Lisensi Dataset',
        content:
          'Anda dapat mengetahui perbedaan dalam penggunaan beberapa lisensi dataset sebagai berikut : \
          <ol> \
            <li> \
            <b>Creative Commons Atribution</b> \
            <br> \
            Lisensi ini memungkinkan orang lain mendistribusikan, menggabungkan, mengadaptasi, dan membangun di atas karya Anda, \
            bahkan secara komersial, selama mereka memberi kredit Anda untuk ciptaan aslinya.\
            </li> \
            <li> \
            <b>Creative Commons Atribution Share-Alike</b> \
            <br> \
            Lisensi ini memungkinkan orang lain untuk menggabungkan, mengadaptasi, dan membangun di atas karya Anda bahkan untuk \
            tujuan komersial, selama mereka memberi kredit kepada Anda dan melisensikan ciptaan baru mereka di bawah persyaratan yang sama.\
            </li> \
            <li> \
            <b>Creative Commons CCZero</b> \
            <br> \
            CCZero memungkinkan ilmuwan, pendidik, seniman, dan pencipta lain serta pemilik konten yang dilindungi hak cipta \
            atau basis data untuk mengesampingkan kepentingan tersebut dalam karya mereka dan dengan demikian menempatkannya \
            selengkap mungkin dalam domain publik, sehingga orang lain dapat dengan bebas membangun, meningkatkan, dan \
            menggunakan kembali karya untuk tujuan apa pun tanpa batasan berdasarkan undang-undang hak cipta atau basis data.\
            </li> \
            <li> \
            <b>Creative Commons Non-Comercial (Any)</b> \
            <br> \
            Lisensi ini memungkinkan orang lain untuk membuat menggabungkan, mengadaptasi, dan membangun di atas karya Anda \
            secara non-komersial, dan meskipun karya baru mereka juga harus mengakui Anda dan bersifat non-komersial, mereka \
            tidak harus melisensikan karya turunan mereka dengan persyaratan yang sama.\
            </li> \
            <li> \
            <b>GNU Free Documentation License</b> \
            <br> \
            Lisensi Dokumentasi Gratis GNU (GNU FDL atau  GFDL) adalah lisensi copyleft untuk dokumentasi gratis, yang \
            dirancang oleh Free Software Foundation (FSF) untuk Proyek GNU. Ini mirip dengan Lisensi Publik Umum GNU, \
            memberi pembaca hak untuk menyalin, mendistribusikan ulang, dan memodifikasi (kecuali untuk "bagian invarian") \
            sebuah karya dan mengharuskan semua salinan dan turunannya tersedia di bawah lisensi yang sama. Salinan juga \
            dapat dijual secara komersial, tetapi, jika diproduksi dalam jumlah yang lebih besar (lebih dari 100), dokumen \
            asli atau kode sumber harus tersedia bagi penerima karya.\
            </li> \
            <li> \
            <b>Open Data CCommons Attributuin License</b> \
            <br> \
            Lisensi Atribusi Open Data Commons adalah perjanjian lisensi yang dimaksudkan untuk memungkinkan pengguna berbagi, \
            memodifikasi, dan menggunakan Basis Data ini secara bebas hanya dengan tunduk pada persyaratan atribusi yang ditetapkan sebelumnya.\
            </li> \
            <li> \
            <b>UK Open Government License (OGL)</b> \
            <br> \
            OGL mengizinkan siapa pun untuk menyalin, menerbitkan, mendistribusikan, mengirimkan, dan mengadaptasi karya berlisensi, \
            dan untuk mengeksploitasinya baik secara komersial maupun non-komersial. Sebagai imbalannya, pengguna ulang karya \
            berlisensi harus mengakui sumber karya dan (jika mungkin) memberikan tautan ke OGL.\
            </li> \
          </ol>',
      },
      {
        id: 'how-to-search-dataset',
        title: 'Cara mencari Dataset',
        content:
          '<div class="mb-10">Untuk melakukan pencarian dataset, Anda dapat memilih satu diantara tiga cara berikut :</div> \
          <strong>Cara Pertama : Melalui halaman Beranda</strong> \
          <ol class="mb-20"> \
            <li>Masukkan kata kunci pencarian pada kolom <strong>"Cari Dataset, Visualisasi, dan Indikator"</strong>, \
            Anda akan diharuskan untuk memilih salah satu opsi untuk memilih pencarian berdasarkan jenis data yang diinginkan, \
            untuk kondisi pencarian dataset Anda dapat memilih <strong>"cari di Dataset"</strong></li> \
            <li>Anda akan diarahkan pada halaman <strong>"Katalog Dataset"</strong> dimana hasil pencarian berdasarkan \
            <strong>"keywords"</strong> ditampilkan pada halaman tersebut.</li> \
            <li>Anda juga dapat memanfaatkan <strong>"Filter Organisasi"</strong>, <strong>"Filter Topik"</strong>, \
            <strong>"Filter Tags"</strong>, <strong>"Filter Lisensi"</strong>, <strong>"Filter Klasifikasi Data"</strong> \
            untuk membantu pencarian Anda agar lebih spesifik.</li> \
          </ol> \
          <strong>Cara Kedua : Melalui halaman Dataset</strong> \
          <ol class="mb-20"> \
            <li>Pilih menu <strong>"Dataset"</strong>.</li> \
            <li>Masukkan kata kunci pencarian pada kolom <strong>"Cari Dataset"</strong>, lalu tekan tombol \
            <strong>"enter"</strong>.</li> \
            <li>Hasil pencarian anda akan muncul dalam bentuk <strong>"List Dataset"</strong>. Anda dapat menggunakan \
            fungsi <strong>halaman/pagination</strong> untuk melihat list data lebih banyak.</li> \
            <li>Anda juga dapat memanfaatkan <strong>"Filter Organisasi"</strong>, <strong>"Filter Topik"</strong>, \
            <strong>"Filter Tags"</strong>, <strong>"Filter Lisensi"</strong>, <strong>"Filter Klasifikasi Data"</strong> \
            untuk membantu pencarian Anda agar lebih spesifik.</li> \
          </ol> \
          <strong>Cara Kedua : Melalui halaman Profil Organisasi</strong> \
          <ol class="mb-20"> \
            <li>Pilih menu <strong>"Organisasi"</strong>.</li> \
            <li>Anda akan diarahkan pada halaman <strong>"Organisasi"</strong> yang menampilkan seluruh Organisasi \
            Perangkat Daerah (OPD) yang ada di Jawa Barat. Pilih salah satu OPD sesuai dengan dataset yang anda cari.</li> \
            <li>Anda akan diarahkan pada halaman <strong>"Profil Organisasi"</strong>, pilih <strong>"Tab Dataset"</strong></li> \
            <li>Masukkan kata kunci pencarian pada kolom <strong>"Cari Dataset"</strong>, lalu tekan tombol \
            <strong>"enter"</strong>.</li> \
            <li>Hasil pencarian anda akan muncul dalam bentuk <strong>"List Dataset"</strong>. Anda dapat menggunakan \
            fungsi <strong>halaman/pagination</strong> untuk melihat list data lebih banyak.</li> \
            <li>Anda juga dapat memanfaatkan <strong>"Filter Organisasi"</strong>, <strong>"Filter Topik"</strong>, \
            <strong>"Filter Tags"</strong>, <strong>"Filter Lisensi"</strong>, <strong>"Filter Klasifikasi Data"</strong> \
            untuk membantu pencarian Anda agar lebih spesifik.</li> \
          </ol>',
      },
      {
        id: 'how-to-download-dataset',
        title: 'Cara mengunduh Dataset',
        content:
          'Untuk mengunduh Dataset dapat dilakukan dengan cara sebagai berikut : \
          <ol> \
            <li>PIlih salah satu dataset yang ingin diunduh.</li> \
            <li>Kemudian Anda akan diarahkan pada laman <strong>"Detail Dataset"</strong>.</li> \
            <li>Klik tombol <strong>"Unduh"</strong>, lalu pilih format dataset yang diinginkan (CSV/ Excel/ API).</li> \
            <li>Tunggu proses pengunduhan hingga muncul pemberitahuan bahwa dataset telah berhasil diunduh.</li> \
          </ol>',
      },
      {
        id: 'format-download-dataset',
        title: 'Format Unduh Dataset',
        content:
          '<div class="mb-10">Satu Data Jabar menyedia tiga format Dataset yang dapat diunduh maupun diakses, diantaranya :</div> \
          <div class="mb-20"> \
            <strong>CSV</strong> <br> \
            <div class="mb-20">Comma Separated Value atau CSV adalah format data yang memudahkan penggunanya melakukan penginputan data ke \
            database secara sederhana. CSV bisa digunakan dalam standar file ASCII, yang mana setiap record dipisahkan \
            dengan tanda koma (,) atau titik koma (;).</div> \
          </div> \
          <div class="mb-20"> \
            <strong>Excel</strong> <br> \
            <div class="mb-20">Data atau file dengan extensi .XLSX merupakan sebuah file Microsoft Excel Open XML Spreadsheet yang dibuat \
            dengan memalau aplikasi Microsoft Excel. File dengan format .XLSX dadpat dibukan dengan sejumlah aplikasi \
            seperti Open Office, Google Docs, hingga Apple Numbers.</div> \
            <div class="mb-20">Pada file .XLSX terdapat spreadsheet yang dapat digunakan untuk melakukan analisis, mengatur, hingga \
            menyimpan data yang menggunakan tabel. Setiap kolom bisa berisi data numerik ataupun teks yang dapat digabungkan \
            sesuai dengan rumus matematika.</div> \
          </div> \
          <div class="mb-20"> \
            <strong>API</strong> <br> \
            <div class="mb-20">Satu Data Jabar menyediakan akses dataset berupa API untuk mendukung terjadinya interoperabilitas data. API merupakan singkatan dari Application Programming Interface yaitu sebuah software yang memungkinkan para developer untuk dapat mengintegrasikan dan mengizinkan dua aplikasi yang berbeda secara bersamaan untuk saling terhubung satu sama lain.</div> \
            <div class="mb-20">API yang disediakan oleh Satu Data Jabar disediakan dari Core Data Jabar lengkap beserta dokumentasi untuk membantu para developer mengakses dan menggunakan API tersebut.</div> \
          </div> \
          ',
      },
      {
        id: 'preview-dataset-in-graph-form',
        title: 'Preview Dataset dalam bentuk Grafik',
        content:
          'Anda dapat melihat ataupun melakukan pengaturan terkait visualisasi/ grafik dari dataset dengan cara sebagai berikut : \
          <ol> \
            <li>Pilih salah satu dataset yang ingin anda lihat preview grafiknya.</li> \
            <li>Klik tab "Grafik" pada halaman Detail Dataset.</li> \
            <li>Secara otomatis sistem akan langsung menghasilkan grafik dari dataset, namun Anda dapat melakukan pengaturan pada grafik tersebut.</li> \
            <li>Pilih <strong>"Gaya Grafik"</strong> untuk menentukan gaya grafik yang akan ditampilkan apakah <strong>Bar Chart</strong> ataupun <strong>Line Chart</strong>.</li> \
            <li>Pilih kolom yang valuenya akan digunakan sebagai Axis A atau sumbu horisontal pada grafik.</li> \
            <li>Pilih kolom yang valuenya akan digunakan sebagai Axis B atau sumbu vertikal pada grafik.</li> \
            <li>PIlih kolom yang valuenya akan digunakan sebagai pengelompokan data/ grup kolom.</li> \
            <li>Tekan <strong>"pratinjau"</strong> untuk mengubah grafik sesuai dengan pengaturan yang sudah dilakukan.</li> \
            <li>Hasil dari pengaturan grafik yang sudah dilakukan akan langsung muncul pada layar.</li> \
          </ol>',
      },
      {
        id: 'preview-dataset-in-map-form',
        title: 'Preview Dataset dalam bentuk Peta',
        content:
          'Anda dapat melihat ataupun melakukan pengaturan terkait visualisasi peta dari dataset dengan cara sebagai berikut : \
          <ol> \
            <li>Pilih salah satu dataset yang ingin anda lihat visualisasi petanya.</li> \
            <li>Klik tab "Peta" pada halaman Detail Dataset.</li> \
            <li>Secara otomatis sistem akan langsung menghasilkan visualisasi dalam bentuk peta dari dataset, namun Anda dapat melakukan pengaturan pada visualisasi peta tersebut.</li> \
            <li>Pilih kolom yang valuenya akan digunakan sebagai Spasial (wilayah) atau informasi lokasi pada visualisasi peta.</li> \
            <li>Pilih kolom yang valuenya akan digunakan sebagai Value (nilai) atau informasi deskriptif pada visualisasi peta.</li> \
            <li>Pilih kolom yang valuenya akan digunakan sebagai pengelompokan data/ grup value (nilai).</li> \
            <li>Tekan <strong>"pratinjau"</strong> untuk mengubah peta sesuai dengan pengaturan yang sudah dilakukan.</li> \
            <li>Hasil dari pengaturan visualisasi peta yang sudah dilakukan akan langsung muncul pada layar.</li> \
          </ol>',
      },
      {
        id: 'request-dataset',
        title: 'Permohonan Dataset',
        content:
          'Anda dapat melakukan permohonan dataset yang dibutuhkan jika dataset yang anda cari tidak ditemukan di Satu Data Jabar. Untuk melakukan permohonan dataset dapat dilakukan dengan cara berikut : \
          <ol> \
            <li>Pada halaman <strong>"Katalog Dataset"</strong> Tekan tombol <strong>"Permohonan Dataset"</strong> yang terletak di sisi kiri layar.</li> \
            <li>Anda akan ditampilkan form <strong>"Permohonan Dataset"</strong>.</li> \
            <li>Lakukan pengisian pada kolom <strong>"Judul Dataset"</strong> dengan judul dataset yang anda butuhkan.</li> \
            <li>Lakukan pengisian pada kolom "Organisasi" dengan organisasi sumber data yang anda butuhkan.</li> \
            <li>Lakukan pengisian pada kolom <strong>"Deskripsi"</strong> terkait kebutuhan deskripsi dari dataset yang anda butuhkan.</li> \
            <li>Tekan tombol <strong>"Kirim"</strong>. Anda akan mendapatkan informasi bahwa permohonan dataset telah berhasil dilakukan.</li> \
            <li>Setelah melalui tahap ini, anda akan mendapatkan informasi dari <strong>Walidata</strong> terkait permohonan dataset yang anda lakukan dalam 1 x 24 jam melalui notifikasi di aplikasi Satu Data Jabar.</li> \
          </ol>',
      },
    ],
  },
  {
    id: 'visualization',
    title: 'Visualisasi',
    sub: [
      {
        id: 'what-is-visualization',
        title: 'Apa itu Visualisasi?',
        content:
          'Visualisasi merupakan gambaran informasi data tertentu dalam bentuk grafik. Tujuan utama dari visualisasi data adalah untuk mengkomunikasikan informasi secara jelas dan efisien kepada penggunanya melalui grafik infomasi yang dipilih, seperti tabel-tabel atau grafik.',
      },
      {
        id: 'how-to-search-visualization',
        title: 'Cara mencari Visualisasi',
        content:
          '<div class="mb-10">Untuk melakukan pencarian visualisasi, Anda dapat memilih satu diantara tiga cara berikut:</div> \
          <strong>Cara Pertama : Melalui halaman Beranda</strong> \
          <ol class="mb-20"> \
            <li>Masukkan kata kunci pencarian pada kolom <strong>"Cari Dataset, Visualisasi, dan Indikator"</strong>, \
            Anda akan diharuskan untuk memilih salah satu opsi untuk memilih pencarian berdasarkan jenis data yang diinginkan, \
            untuk kondisi pencarian dataset Anda dapat memilih <strong>"cari di Visualisasi"</strong></li> \
            <li>Anda akan diarahkan pada halaman <strong>"Katalog Visualisasi"</strong> dimana hasil pencarian berdasarkan \
            <strong>"keywords"</strong> ditampilkan pada halaman tersebut.</li> \
            <li>Anda juga dapat memanfaatkan <strong>"Filter Organisasi"</strong>, <strong>"Filter Topik"</strong>, \
            <strong>"Filter Klasifikasi Data"</strong> untuk membantu pencarian Anda agar lebih spesifik.</li> \
          </ol> \
          <strong>Cara Kedua : Melalui halaman Visualisasi</strong> \
          <ol class="mb-20"> \
            <li>Pilih menu <strong>"Visualisasi"</strong>.</li> \
            <li>Masukkan kata kunci pencarian pada kolom <strong>"Cari Visualisasi"</strong>, lalu tekan tombol \
            <strong>"enter"</strong>.</li> \
            <li>Hasil pencarian anda akan muncul dalam bentuk <strong>"List Visualisasi"</strong>. Anda dapat menggunakan \
            fungsi <strong>halaman/pagination</strong> untuk melihat list data lebih banyak.</li> \
            <li>Anda juga dapat memanfaatkan <strong>"Filter Organisasi"</strong>, <strong>"Filter Topik"</strong>, \
            <strong>"Filter Klasifikasi Data"</strong> untuk membantu pencarian Anda agar lebih spesifik.</li> \
          </ol> \
          <strong>Cara Kedua : Melalui halaman Profil Organisasi</strong> \
          <ol class="mb-20"> \
            <li>Pilih menu <strong>"Organisasi"</strong>.</li> \
            <li>Anda akan diarahkan pada halaman <strong>"Organisasi"</strong> yang menampilkan seluruh Organisasi \
            Perangkat Daerah (OPD) yang ada di Jawa Barat. Pilih salah satu OPD sesuai dengan dataset yang anda cari.</li> \
            <li>Anda akan diarahkan pada halaman <strong>"Profil Organisasi"</strong>, pilih <strong>"Tab Visualisasi"</strong></li> \
            <li>Masukkan kata kunci pencarian pada kolom <strong>"Cari Visualisasi"</strong>, lalu tekan tombol \
            <strong>"enter"</strong>.</li> \
            <li>Hasil pencarian anda akan muncul dalam bentuk <strong>"List Visualisasi"</strong>. Anda dapat menggunakan \
            fungsi <strong>halaman/pagination</strong> untuk melihat list data lebih banyak.</li> \
            <li>Anda juga dapat memanfaatkan <strong>"Filter Organisasi"</strong>, <strong>"Filter Topik"</strong>, \
            <strong>"Filter Klasifikasi Data"</strong> untuk membantu pencarian Anda agar lebih spesifik.</li> \
          </ol>',
      },
    ],
  },
  {
    id: 'indicator',
    title: 'Indikator',
    sub: [
      {
        id: 'what-is-indicator',
        title: 'Apa itu Indikator?',
        content:
          'Indikator kinerja merupakan alat ukur spesifik secara kuantitatif atau kualitatif untuk masukan, proses, keluaran, hasil, manfaat dan dampak yang menggambarkan tingkat capaian kinerja suatu program atau kegiatan.',
      },
      {
        id: 'how-to-search-indicator',
        title: 'Cara mencari Indikator',
        content:
          '<div class="mb-10">Untuk melakukan pencarian indikator, Anda dapat memilih satu diantara tiga cara berikut:</div> \
          <strong>Cara Pertama : Melalui halaman Beranda</strong> \
          <ol class="mb-20"> \
            <li>Masukkan kata kunci pencarian pada kolom <strong>"Cari Dataset, Visualisasi, dan Indikator"</strong>, \
            Anda akan diharuskan untuk memilih salah satu opsi untuk memilih pencarian berdasarkan jenis data yang diinginkan, \
            untuk kondisi pencarian dataset Anda dapat memilih <strong>"cari di Indikator"</strong></li> \
            <li>Anda akan diarahkan pada halaman <strong>"Katalog Indikator"</strong> dimana hasil pencarian berdasarkan \
            <strong>"keywords"</strong> ditampilkan pada halaman tersebut.</li> \
            <li>Anda juga dapat memanfaatkan <strong>"Filter Organisasi"</strong> dan \
            <strong>"Filter Klasifikasi"</strong> untuk membantu pencarian Anda agar lebih spesifik.</li> \
          </ol> \
          <strong>Cara Kedua : Melalui halaman Indikator</strong> \
          <ol class="mb-20"> \
            <li>Pilih menu <strong>"Indikator"</strong>.</li> \
            <li>Masukkan kata kunci pencarian pada kolom <strong>"Cari Indikator"</strong>, lalu tekan tombol \
            <strong>"enter"</strong>.</li> \
            <li>Hasil pencarian anda akan muncul dalam bentuk <strong>"List Indikator"</strong>. Anda dapat menggunakan \
            fungsi <strong>halaman/pagination</strong> untuk melihat list data lebih banyak.</li> \
            <li>Anda juga dapat memanfaatkan <strong>"Filter Organisasi"</strong> dan \
            <strong>"Filter Klasifikasi"</strong> untuk membantu pencarian Anda agar lebih spesifik.</li> \
          </ol> \
          <strong>Cara Kedua : Melalui halaman Profil Organisasi</strong> \
          <ol class="mb-20"> \
            <li>Pilih menu <strong>"Organisasi"</strong>.</li> \
            <li>Anda akan diarahkan pada halaman <strong>"Organisasi"</strong> yang menampilkan seluruh Organisasi \
            Perangkat Daerah (OPD) yang ada di Jawa Barat. Pilih salah satu OPD sesuai dengan dataset yang anda cari.</li> \
            <li>Anda akan diarahkan pada halaman <strong>"Profil Organisasi"</strong>, pilih <strong>"Tab Indikator"</strong></li> \
            <li>Masukkan kata kunci pencarian pada kolom <strong>"Cari Indikator"</strong>, lalu tekan tombol \
            <strong>"enter"</strong>.</li> \
            <li>Hasil pencarian anda akan muncul dalam bentuk <strong>"List Indikator"</strong>. Anda dapat menggunakan \
            fungsi <strong>halaman/pagination</strong> untuk melihat list data lebih banyak.</li> \
            <li>Anda juga dapat memanfaatkan <strong>"Filter Organisasi"</strong> dan \
            <strong>"Filter Klasifikasi"</strong> untuk membantu pencarian Anda agar lebih spesifik.</li> \
          </ol>',
      },
    ],
  },
];
