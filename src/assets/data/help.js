module.exports = [
  {
    id: 1,
    name: 'Tentang Satu Data',
    expanded: false,
    sub: [
      {
        name: 'Apa itu Satu Data?',
        content:
          '<p><span style="font-weight: 400;">Aplikasi yang memudahkan Perangkat Daerah mengelola  dan berbagi pakai data di lingkungan Pemerintah Provinsi Jawa Barat untuk membuat pengambilan keputusan dan perumusan kebijakan lebih cepat dan akurat.</span></p>',
      },
      {
        name: 'Siapa saja yang berhak menggunakan Satu Data?',
        content: `<p dir="ltr" id="docs-internal-guid-a6c4e651-7fff-bafc-eacc-2c0b0b702b54">
          Seluruh Perangkat Daerah dalam lingkup Provinsi Jawa Barat berhak untuk
          menggunakan platform Satu Data. Berikut adalah tingkatan user dalam
          platform Satu Data:
      </p>
      <ol class="font-weight-bolder">
          <li dir="ltr">
              <p dir="ltr" class="font-weight-bolder">
                  Walidata Provinsi
              </p>
          </li>
      </ol>
      <p dir="ltr" class="pl-15">
          <b>Bidang Statistik</b> Dinas Informasi dan Komunikasi Jawa Barat (Diskominfo
          Jabar) sebagai  <b>Walidata</b> dapat melakukan pengelolaan terhadap seluruh data
          yang ada di Satu Data, yaitu meliputi penambahan data (dataset, indikator,
          dan visualisasi), perubahan data, pengarsipan data, hingga pemantauan
          seluruh aktivitas yang terjadi pada <i>platform</i> Satu Data.
      </p>
      <ol start="2" class="font-weight-bolder">
          <li dir="ltr">
              <p dir="ltr" class="font-weight-bolder">
                  OPD Pengelola
              </p>
          </li>
      </ol>
      <p dir="ltr" class="pl-15">
          Seluruh <b>Perangkat Daerah</b> akan diberikan <b>satu</b> <i>user</i> <b>OPD Pengelola</b>. <i>OPD
          Pengelola</i> dapat melihat dan mengakses data dari Perangkat Daerah dalam
          lingkup Provinsi Jawa Barat yang sudah tersedia dalam <i>platform</i> Satu Data.
          Selain itu, pada <i>OPD Pengelola</i>, pengguna dapat melakukan pengelolaan
          terhadap data yang dimiliki oleh Perangkat Daerah, yaitu meliputi perubahan
          data, pengarsipan data, serta verifikasi terhadap dataset dan indikator
          yang telah diinput oleh <i>Walidata Provinsi.</i>
      </p>
      <ol start="3" class="font-weight-bolder">
          <li dir="ltr">
              <p dir="ltr" class="font-weight-bolder">
                  OPD Pengguna
              </p>
          </li>
      </ol>
      <p dir="ltr" class="pl-15">
          <i>User</i> <b>OPD Pengguna</b> dapat digunakan oleh setiap <b>ASN Perangkat Daerah</b> dalam
          lingkup Provinsi Jawa Barat. Pada tingkatan <i>OPD Pengguna</i>, pengguna dapat
          melihat dan mengakses data dari seluruh Perangkat Daerah dalam lingkup
          Provinsi Jawa Barat yang sudah tersedia dalam <i>platform</i> Satu Data.
      </p>
      `,
      },
      {
        name: 'Istilah dalam Satu Data',
        content: `<p><span style="font-weight: 400;">Definisi dari istilah-istilah yang ada dalam </span><em><span style="font-weight: 400;">platform</span></em><span style="font-weight: 400;"> Satu Data, meliputi:</span></p>
         <ol>
         <li style="font-weight: 400;"><em><span style="font-weight: 400;">Dataset</span></em><span style="font-weight: 400;">: Kumpulan data yang disusun secara tematis oleh perorangan atau instansi.</span></li>
         <li style="font-weight: 400;"><em><span style="font-weight: 400;">Metadata</span></em><span style="font-weight: 400;">: Informasi dari suatu data dalam format dan struktur yang distandarisasi untuk menggambarkan, menjelaskan, menempatkan, atau memudahkan cara untuk mencari, menggunakan, atau mengelola informasi dari data yang bersangkutan.&nbsp;</span></li>
         </ol>
         <p><em><span style="font-weight: 400;">Klasifikasi Data &ldquo;Publik&rdquo;</span></em><span style="font-weight: 400;">: Data dengan klasifikasi </span><em><span style="font-weight: 400;">Publik</span></em><span style="font-weight: 400;"> adalah data yang dapat diakses atau diperuntukan untuk masyarakat luas. Seluruh Data klasifikasi </span><em><span style="font-weight: 400;">Publik </span></em><span style="font-weight: 400;">yang diinput ke dalam </span><em><span style="font-weight: 400;">platform</span></em><span style="font-weight: 400;"> Satu Data akan terintegrasi dengan </span><em><span style="font-weight: 400;">platform</span></em> <strong>Open Data</strong><span style="font-weight: 400;"> untuk dapat diakses langsung oleh masyarakat luas.</span></p>`,
      },
    ],
  },
  {
    id: 2,
    name: 'Dataset',
    expanded: false,
    sub: [
      {
        name: 'Apa itu Dataset?',
        content:
          '<p>Dataset merupakan kumpulan data yang disusun secara tematis oleh perorangan atau instansi.</p>',
      },
      {
        name: 'Klasifikasi Dataset',
        content: `<p dir="ltr" id="docs-internal-guid-b85df1e4-7fff-c134-6777-a9c12fee5ba1" style="line-height:1.38;margin-left: 72pt;margin-top:0pt;margin-bottom:0pt;"><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Dalam Satu Data terdapat tiga Klasifikasi Data, diantaranya :&nbsp;</span></p>

          <ol style="margin-top:0;margin-bottom:0;">
            <li dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;margin-left: 72pt;">
            <p dir="ltr" role="presentation" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Klasifikasi Data Publik</span></p>
            </li>
          </ol>

          <p dir="ltr" class="pl-15" style="line-height:1.38;margin-left: 108pt;margin-top:0pt;margin-bottom:0pt;"><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Data dengan klasifikasi </span><em style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:italic; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Publik</em><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap"> adalah data yang dapat diakses atau diperuntukan untuk masyarakat luas. Seluruh data klasifikasi </span><em style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:italic; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Publik </em><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">yang diinput ke dalam </span><em style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:italic; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">platform</em><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap"> Satu Data akan terintegrasi dengan </span><em style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:italic; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">platform</em><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap"> Open Data untuk dapat diakses langsung oleh masyarakat luas.&nbsp;</span></p>


          <ol start="2" style="margin-top:0;margin-bottom:0;">
            <li dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;margin-left: 72pt;">
            <p dir="ltr" role="presentation" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Klasifikasi Data Internal</span></p>
            </li>
          </ol>
          </p>

          <p dir="ltr" class="pl-15" style="line-height:1.38;margin-left: 108pt;margin-top:0pt;margin-bottom:0pt;"><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Data dengan klasifikasi </span><em style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:italic; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Internal</em><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap"> adalah data yang hanya dapat diakses atau diperuntukan khusus untuk seluruh Perangkat Daerah dalam lingkup Provinsi Jawa Barat.</span></p>

          <ol start="3" style="margin-top:0;margin-bottom:0;">
            <li dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;margin-left: 72pt;">
            <p dir="ltr" role="presentation" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Klasifikasi Data Dikecualikan</span></p>
            </li>
          </ol>
          </p>

          <p dir="ltr" class="pl-15" style="line-height:1.38;margin-left: 108pt;margin-top:0pt;margin-bottom:0pt;"><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Data dengan klasifikasi </span><em style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:italic; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Dikecualikan</em><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap"> adalah data yang hanya dapat diakses oleh Perangkat Daerah tertentu yang telah ditentukan hak aksesnya oleh Perangkat Daerah pemilik data.&nbsp;</span></p>
          `,
      },
      {
        name: 'Bagaimana cara melihat Dataset yang ada dalam Satu Data?',
        content: `<p dir="ltr" id="docs-internal-guid-32c46fc3-7fff-60ff-69aa-5779ca89aa64" style="line-height:1.38;margin-left: 72pt;margin-top:0pt;margin-bottom:0pt;"><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Berikut adalah beberapa cara untuk&nbsp; melihat Dataset yang ada di dalam Satu Data:&nbsp;</span></p>

        <ol style="margin-top:0;margin-bottom:0;">
          <li dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:700;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;margin-left: 72pt;">
          <p dir="ltr" role="presentation" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><strong style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:700; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Katalog Dataset</strong></p>
          </li>
        </ol>

        <p dir="ltr" style="line-height:1.38;margin-left: 108pt;margin-top:0pt;margin-bottom:0pt;"><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Pengguna dapat melihat Dataset yang sudah tersedia di dalam Satu Data dengan masuk ke halaman </span><em style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:italic; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Katalog Dataset</em><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">, dengan cara memilih menu </span><strong style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:700; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Dataset</strong></p>

        <p dir="ltr" style="line-height:1.38;margin-left: 108pt;margin-top:0pt;margin-bottom:0pt;"><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap"><span style="border:none; display:inline-block; height:40px; overflow:hidden; width:541px"><img alt="" height="40" src="https://lh3.googleusercontent.com/QREuAf_0IKpYvwTYdYpQZbMXFwiahcEiwl7KBUc938kjc3qXgP-3QmtweXInSjwHiYH6B8Bzrfilm5QJBBLuJEXAHuUb_hze9fUmmjACLlnwt4TGPQd1XM91tfYl2kPN6yK_Rc1B" style="margin-left:0px; margin-top:0px" width="541" /></span></span></p>



        <ol start="2" style="margin-top:0;margin-bottom:0;">
          <li dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:700;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;margin-left: 72pt;">
          <p dir="ltr" role="presentation" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><strong style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:700; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Profil Organisasi</strong></p>
          </li>
        </ol>

        <p dir="ltr" style="line-height:1.38;margin-left: 108pt;margin-top:0pt;margin-bottom:0pt;"><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Pengguna dapat melihat Dataset dengan masuk ke halaman </span><em style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:italic; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Profil Organisasi</em><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap"> apabila pengguna sudah mengetahui Dataset apa yang akan dicari berdasarkan Perangkat Daerah pemilik Dataset tersebut. Cara ini dapat dilakukan dengan memilih menu </span><strong style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:700; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Organisasi</strong><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">.&nbsp;</span></p>

        <p dir="ltr" style="line-height:1.38;margin-left: 108pt;margin-top:0pt;margin-bottom:0pt;"><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap"><span style="border:none; display:inline-block; height:40px; overflow:hidden; width:540px"><img alt="" height="40" src="https://lh4.googleusercontent.com/ZzqnW753cCsy7GJ_6GRtgNSicizPSsrCZzuNC4lEMx28JmzxV3IsK1gcElUF1PhqmYfyKOePg2s22Te_LYnGf6sg46EWWwszz4iSJvkv-9__ZQd-iGPxiqTd808x-qhJiB4Ei_EN" style="margin-left:0px; margin-top:0px" width="540" /></span></span></p>

        <p dir="ltr" style="line-height:1.38;margin-left: 108pt;margin-top:0pt;margin-bottom:0pt;"><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">&nbsp;&nbsp;</span></p>

        <p dir="ltr" style="line-height:1.38;margin-left: 108pt;margin-top:0pt;margin-bottom:0pt;"><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap"><span style="border:none; display:inline-block; height:318px; overflow:hidden; width:536px"><img alt="" height="318" src="https://lh4.googleusercontent.com/dH0UKyxo4i5__BZQux6xwmJdGaAX28cM-IcyK_LqckKoKjTRREL7hVpDCZ1Pn0v-NmGw4DPExPhSQfhHqvyrUM4zVPzdYX-DQsf22WsTLpiD4jQy7jMvFzgoFewgWFEVzx4j7cPN" style="margin-left:0px; margin-top:0px" width="536" /></span></span></p>

        <p dir="ltr" style="line-height:1.38;margin-left: 108pt;margin-top:0pt;margin-bottom:0pt;"><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Pengguna dapat memilih Perangkat Daerah atau Organisasi mana yang Datasetnya ingin dilihat.&nbsp;</span></p>

        <p dir="ltr" style="line-height:1.38;margin-left: 108pt;margin-top:0pt;margin-bottom:0pt;"><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Setelah memilih Perangkat Daerah, pengguna akan langsung diarahkan pada halaman Profil Organisasi untuk dapat melihat seluruh koleksi Dataset milik Perangkat Daerah yang dipilih.</span></p>

        <ol start="3" style="margin-top:0;margin-bottom:0;">
          <li dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:700;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;margin-left: 72pt;">
          <p dir="ltr" role="presentation" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><strong style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:700; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Beranda - Fitur Pencarian</strong></p>
          </li>
        </ol>

        <p dir="ltr" style="line-height:1.38;margin-left: 108pt;margin-top:0pt;margin-bottom:0pt;"><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Pengguna dapat menggunakan </span><em style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:italic; font-variant:normal; font-weight:700; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Fitur Pencarian</em><em style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:italic; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap"> </em><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">untuk mencari Dataset yang diinginkan dengan memasukkan </span><em style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:italic; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">keyword </em><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">pada kolom pencarian yang berada di halaman </span><strong style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:700; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Beranda</strong><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">. </span><br />
        <span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap"><span style="border:none; display:inline-block; height:242px; overflow:hidden; width:537px"><img alt="" height="242" src="https://lh4.googleusercontent.com/LsVq1S2YdGBOEt4I6zVRl9UmA1HAy14-4tr1qC6ht_dmDetPqb7XhTu4DzXGI-WbYR6wE0Eunbz7qtvxDq2iaXR2cxCJXXX89JGgLq2rr7OQfUwYZ_Ds_-EQWCQjHe8IdXuPWGFh" style="margin-left:0px; margin-top:0px" width="537" /></span></span></p>

        <p dir="ltr" style="line-height:1.38;margin-left: 108pt;margin-top:0pt;margin-bottom:0pt;"><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Kemudian, pengguna akan diarahkan ke halaman </span><em style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:italic; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Katalog Dataset </em><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">yang menampilkan hasil dari </span><em style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:italic; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">keyword </em><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">yang telah dimasukkan.</span></p>
        `,
      },
      {
        name: 'Bagaimana cara mengunduh Dataset?',
        content: `<ol id="docs-internal-guid-bf68e2b9-7fff-f395-17e1-aea5c1b59105" style="margin-top:0;margin-bottom:0;">
          <li dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;margin-left: 72pt;">
          <p dir="ltr" role="presentation" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Klik </span><em style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:italic; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Judul Dataset </em><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">untuk masuk ke </span><em style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:italic; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Detail Dataset</em><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap"> yang dipilih</span></p>
          </li>
          <li dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;margin-left: 72pt;">
          <p dir="ltr" role="presentation" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Pada halaman </span><em style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:italic; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Detail Dataset </em><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">arahkan kursor pada tombol </span><em style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:italic; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Unduh Dataset</em></p>
          </li>
          <li dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;margin-left: 72pt;">
          <p dir="ltr" role="presentation" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><em style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:italic; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Dataset tersedia dalam dua format, yaitu </em><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">.CSV dan API yang bebas dipilih pengguna sesuai dengan kebutuhan.</span></p>
          </li>
        </ol>
        `,
      },
      {
        name: 'Bagaimana jika Dataset (.CSV) yang saya download tidak rapi saat dibuka dengan Ms.Excel?',
        content: `<p dir="ltr" id="docs-internal-guid-0fe32dcd-7fff-fa01-879d-83a6d014405c" style="line-height:1.38;margin-left: 72pt;margin-top:0pt;margin-bottom:0pt;"><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Saat mengunduh Dataset dengan format .CSV, terdapat kemungkinan data yang muncul menjadi tidak rapi saat dibuka menggunakan Microsoft Excel karena perbedaan konfigurasi Microsoft Excel yang digunakan. Untuk mengantisipasi hal tersebut, lakukan cara berikut (menggunakan Microsoft Excel 2016):&nbsp;</span></p>

          <ol style="margin-top:0;margin-bottom:0;">
            <li dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;margin-left: 72pt;">
            <p dir="ltr" role="presentation" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Buka file Dataset dengan format .CSV yang telah diunduh</span></p>
            </li>
            <li dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;margin-left: 72pt;">
            <p dir="ltr" role="presentation" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Pilih area kolom yang akan diproses</span></p>
            </li>
            <li dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;margin-left: 72pt;">
            <p dir="ltr" role="presentation" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Pilih </span><em style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:italic; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Menu Bar - Data</em><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">&nbsp;</span></p>
            </li>
            <li dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;margin-left: 72pt;">
            <p dir="ltr" role="presentation" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Pada Data Tools pilih </span><em style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:italic; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Text</em><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap"> </span><em style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:italic; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">to Columns</em></p>
            </li>
            <li dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;margin-left: 72pt;">
            <p dir="ltr" role="presentation" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Pilih </span><em style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:italic; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Delimited</em><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap"> dan klik tombol </span><em style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:italic; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Next</em></p>
            </li>
            <li dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;margin-left: 72pt;">
            <p dir="ltr" role="presentation" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><em style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:italic; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Uncheck</em><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap"> tab dan lakukan </span><em style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:italic; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Checklist</em><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap"> pada kotak </span><em style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:italic; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Comma</em><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap"> dan klik tombol </span><em style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:italic; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Next</em></p>
            </li>
            <li dir="ltr" style="list-style-type:decimal;font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;margin-left: 72pt;">
            <p dir="ltr" role="presentation" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Akan terlihat </span><em style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:italic; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">preview</em><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap"> dari data tabel yang telah dikonfigurasi, klik </span><em style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:italic; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap">Next</em><span style="background-color:transparent; color:#000000; font-family:Arial; font-size:11pt; font-style:normal; font-variant:normal; font-weight:400; text-decoration:none; vertical-align:baseline; white-space:pre-wrap"> jika sudah sesuai.</span></p>
            </li>
          </ol>
          `,
      },
    ],
  },
];
