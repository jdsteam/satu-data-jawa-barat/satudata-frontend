window.dataLayer = window.dataLayer || [];
function gtag() {
  window.dataLayer.push(arguments);
}
gtag('js', new Date());

gtag('config', '%GOOGLE_ANALYTICS%');