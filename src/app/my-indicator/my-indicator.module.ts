import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyIndicatorRoutingModule } from './my-indicator-routing.module';

@NgModule({
  imports: [CommonModule, MyIndicatorRoutingModule],
  declarations: [],
})
export class MyIndicatorModule {}
