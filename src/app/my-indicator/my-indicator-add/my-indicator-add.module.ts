import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';

import { BreadcrumbComponent } from '@components-v4';
import { FormComponent as HeaderFormComponent } from '@components-v3/header/form/form.component';
import { CreateComponent } from '@components-v3/form/indicator/create/create.component';

import { MyIndicatorAddComponent } from './my-indicator-add.component';

export const routes: Routes = [
  {
    path: '',
    component: MyIndicatorAddComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    BreadcrumbComponent,
    HeaderFormComponent,
    CreateComponent,
  ],
  declarations: [MyIndicatorAddComponent],
  bootstrap: [MyIndicatorAddComponent],
})
export class MyIndicatorAddModule {}
