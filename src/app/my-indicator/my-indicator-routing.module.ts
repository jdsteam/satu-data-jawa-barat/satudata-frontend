import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// GUARD
import {
  MyIndicatorAddGuard,
  MyIndicatorEditGuard,
  MyIndicatorPreviewGuard,
  StoreIndicatorDetailGuard,
} from '@guards-v3';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'add',
        canActivate: [MyIndicatorAddGuard],
        loadChildren: () =>
          import('./my-indicator-add/my-indicator-add.module').then(
            (m) => m.MyIndicatorAddModule,
          ),
      },
      {
        path: 'edit/:id',
        canActivate: [MyIndicatorEditGuard, StoreIndicatorDetailGuard],
        loadChildren: () =>
          import('./my-indicator-edit/my-indicator-edit.module').then(
            (m) => m.MyIndicatorEditModule,
          ),
      },
      {
        path: 'preview/:id',
        canActivate: [MyIndicatorPreviewGuard],
        loadChildren: () =>
          import('./my-indicator-preview/my-indicator-preview.module').then(
            (m) => m.MyIndicatorPreviewModule,
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MyIndicatorRoutingModule {}
