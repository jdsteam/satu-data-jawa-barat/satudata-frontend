import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';

import { BreadcrumbComponent } from '@components-v4';
import { FormComponent as HeaderFormComponent } from '@components-v3/header/form/form.component';
import { PreviewComponent } from '@components-v3/form/indicator/preview/preview.component';

import { MyIndicatorPreviewComponent } from './my-indicator-preview.component';

export const routes: Routes = [
  {
    path: '',
    component: MyIndicatorPreviewComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    BreadcrumbComponent,
    HeaderFormComponent,
    PreviewComponent,
  ],
  declarations: [MyIndicatorPreviewComponent],
  bootstrap: [MyIndicatorPreviewComponent],
})
export class MyIndicatorPreviewModule {}
