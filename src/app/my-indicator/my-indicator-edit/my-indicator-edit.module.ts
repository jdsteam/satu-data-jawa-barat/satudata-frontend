import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';

import { BreadcrumbComponent } from '@components-v4';
import { FormComponent as HeaderFormComponent } from '@components-v3/header/form/form.component';
import { UpdateComponent } from '@components-v3/form/indicator/update/update.component';

import { MyIndicatorEditComponent } from './my-indicator-edit.component';

export const routes: Routes = [
  {
    path: '',
    component: MyIndicatorEditComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    BreadcrumbComponent,
    HeaderFormComponent,
    UpdateComponent,
  ],
  declarations: [MyIndicatorEditComponent],
  bootstrap: [MyIndicatorEditComponent],
})
export class MyIndicatorEditModule {}
