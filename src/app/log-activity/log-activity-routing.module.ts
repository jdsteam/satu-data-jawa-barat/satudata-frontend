import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// GUARD
import { LogActivityPageGuard } from '@guards-v3';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        canActivate: [LogActivityPageGuard],
        loadChildren: () =>
          import('./log-activity-page/log-activity-page.module').then(
            (m) => m.LogActivityPageModule,
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LogActivityRoutingModule {}
