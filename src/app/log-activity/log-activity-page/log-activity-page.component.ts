import {
  Component,
  OnInit,
  OnDestroy,
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';

// INTERFACE
import { Breadcrumb } from '@interfaces-v4';

// MODEL
import { LoginData } from '@models-v3';

// SERVICE
import { AuthenticationService, StateService } from '@services-v3';
import { GlobalService } from '@services-v4';

import { isEmpty, isNil, set } from 'lodash';
import moment from 'moment';
import { fromWorker } from 'observable-webworker';

export interface Filter {
  sort: string;
  directory: string;
  perPage: number;
  currentPage: number;
  search: string;
  status: string;
  data: string;
  dateStart: string;
  dateEnd: string;
  date: string;
}

@Component({
  selector: 'app-log-activity-page',
  templateUrl: './log-activity-page.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LogActivityPageComponent
  implements OnInit, OnDestroy, AfterViewInit
{
  satudataUser: LoginData;
  satudataToken: string;
  moment = moment;
  jds = jds;

  // Log
  log = null;
  pageInfo = null;
  deviceInfo = null;
  userInfo = null;

  // Setting
  title: string;
  label = 'Log Aktivitas';
  labelHelper: string;
  breadcrumb: Breadcrumb[] = [
    { label: 'Log Aktivitas', link: '/log-activity' },
  ];

  filter = {
    sort: 'date_start',
    directory: 'desc',
    perPage: 10,
    currentPage: 1,
    search: '',
    status: 'login',
    data: 'dataset',
    dateStart: null,
    dateEnd: null,
    date: null,
  };

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private globalService: GlobalService,
    private authenticationService: AuthenticationService,
    private stateService: StateService,
    private titleService: Title,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
    this.authenticationService.satudataToken.subscribe(
      (x) => (this.satudataToken = x),
    );
    this.stateService.currentLogUser.subscribe((x) => (this.userInfo = x));
  }

  ngOnInit(): void {
    this.settingsAll();
    this.queryParams();
    this.logPage('init');
  }

  ngOnDestroy(): void {
    this.logPage('destroy');
  }

  ngAfterViewInit(): void {
    this.cdRef.detectChanges();
  }

  settingsAll(): void {
    this.labelHelper =
      this.satudataUser.role_name === 'walidata' ? `Satu Data` : `Data Saya`;
    this.label = `${this.label} ${this.labelHelper}`;

    this.title = this.globalService.title;
    this.titleService.setTitle(`${this.label} - ${this.title}`);
    this.globalService.changeLabel(this.label);
    this.globalService.changeBreadcrumb(this.breadcrumb);
  }

  queryParams(): void {
    this.route.queryParams.subscribe((p) => {
      const pPerPage = p.perpage;
      const pCurrentPage = p.currentpage;
      const pSort = p.sort;
      const pDirectory = p.directory;
      const pSearch = p.q;
      const pStatus = p.status;
      const pData = p.data;
      const pDateStart = p.dateStart;
      const pDateEnd = p.dateEnd;

      let dateStart: string;
      let dateEnd: string;

      this.filter.perPage = !isNil(pPerPage) ? Number(pPerPage) : 10;
      this.filter.currentPage = !isNil(pCurrentPage) ? Number(pCurrentPage) : 1;
      this.filter.sort = !isNil(pSort) ? pSort : 'date_start';
      this.filter.directory = !isNil(pDirectory) ? pDirectory : 'desc';
      this.filter.search = !isNil(pSearch) ? pSearch : '';
      this.filter.data = !isNil(pData) ? pData : 'dataset';

      switch (isNil(pStatus)) {
        case true:
          if (this.satudataUser.role_name === 'walidata') {
            this.filter.status = 'login';
          } else {
            this.filter.status = 'view';
          }
          break;
        case false:
          this.filter.status = pStatus;
          break;
        default:
          break;
      }

      switch (isNil(pDateStart)) {
        case true:
          this.filter.dateStart = null;
          break;
        case false:
          this.filter.dateStart = pDateStart;
          dateStart = moment(pDateStart).format('LL');
          break;
        default:
          break;
      }

      switch (isNil(pDateEnd)) {
        case true:
          this.filter.dateEnd = null;
          break;
        case false:
          this.filter.dateEnd = pDateEnd;
          dateEnd = moment(pDateEnd).format('LL');
          break;
        default:
          break;
      }

      this.filter.date =
        !isEmpty(dateStart) && !isEmpty(dateEnd)
          ? `${dateStart} - ${dateEnd}`
          : null;
    });
  }

  filterDate(event: Filter): void {
    this.filter.date = event.date;
    this.filter.dateStart = event.dateStart;
    this.filter.dateEnd = event.dateEnd;
    this.filter.currentPage = event.currentPage;
    this.filter = { ...this.filter };
  }

  filterData(event: Filter): void {
    this.filter.data = event.data;
    this.filter.currentPage = event.currentPage;
    this.filter = { ...this.filter };
  }

  filterSearch(event: Filter): void {
    this.filter.search = event.search;
    this.filter.currentPage = event.currentPage;
    this.filter = { ...this.filter };
  }

  filterStatus(event: Filter): void {
    this.filter.status = event.status;
    this.filter.currentPage = event.currentPage;
    this.filter = { ...this.filter };
  }

  filterSort(event: Filter): void {
    this.filter.sort = event.sort;
    this.filter.currentPage = event.currentPage;
    this.filter = { ...this.filter };
  }

  filterDirectory(event: Filter): void {
    this.filter.directory = event.directory;
    this.filter.currentPage = event.currentPage;
    this.filter = { ...this.filter };
  }

  logPage(type: string): void {
    if (type === 'init') {
      this.pageInfo = {
        page: this.router.url,
        date_start: moment().format('YYYY-MM-DD HH:mm:ss'),
      };

      this.log = {
        log: { ...this.pageInfo, ...this.userInfo },
        token: this.satudataToken,
      };
    } else if (type === 'destroy') {
      if (typeof Worker !== 'undefined') {
        set(this.log.log, 'date_end', moment().format('YYYY-MM-DD HH:mm:ss'));

        const log = of(this.log);

        fromWorker<object, string>(
          () =>
            new Worker(
              new URL('src/app/__v4/workers/log-page.worker', import.meta.url),
              { type: 'module' },
            ),
          log,
        ).subscribe((message) => {
          if (!environment.production) {
            console.info(`Log page: ${message}`);
          }
        });
      }
    }
  }
}
