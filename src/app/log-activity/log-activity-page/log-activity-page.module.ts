import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';

import { LogActivityComponent as HeaderLogActivityComponent } from '@components-v3/header/log-activity/log-activity.component';
import { LogActivityComponent as TabLogActivityComponent } from '@components-v3/tab/log-activity/log-activity.component';
import { LoginComponent } from '@components-v3/list/log-activity/login/login.component';
import { PageComponent } from '@components-v3/list/log-activity/page/page.component';
import { ViewComponent } from '@components-v3/list/log-activity/view/view.component';
import { AccessComponent } from '@components-v3/list/log-activity/access/access.component';
import { LogActivityPageComponent } from './log-activity-page.component';

export const routes: Routes = [
  {
    path: '',
    component: LogActivityPageComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    HeaderLogActivityComponent,
    TabLogActivityComponent,
    LoginComponent,
    PageComponent,
    ViewComponent,
    AccessComponent,
  ],
  declarations: [LogActivityPageComponent],
  bootstrap: [LogActivityPageComponent],
})
export class LogActivityPageModule {}
