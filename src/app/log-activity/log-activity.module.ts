import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LogActivityRoutingModule } from './log-activity-routing.module';

@NgModule({
  imports: [CommonModule, LogActivityRoutingModule],
  declarations: [],
})
export class LogActivityModule {}
