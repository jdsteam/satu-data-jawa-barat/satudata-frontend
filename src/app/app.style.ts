import { Injectable } from '@angular/core';

// STYLE
import { jdsBiColor } from '@jds-bi/cdk';

// PLUGIN
import { css } from '@emotion/css';

@Injectable()
export class AppStyle {
  getDynamicStyle(): any {
    const main = css`
      a.t-link {
        display: inline-flex;
        color: ${jdsBiColor.blue['600']};

        &:hover {
          color: ${jdsBiColor.blue['800']};
        }
      }
    `;

    return {
      main,
    };
  }
}
