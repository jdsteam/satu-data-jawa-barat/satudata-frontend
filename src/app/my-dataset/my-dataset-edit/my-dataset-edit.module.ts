import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';

import { BreadcrumbComponent } from '@components-v4';
import { FormComponent as HeaderFormComponent } from '@components-v3/header/form/form.component';
import { UpdateComponent as FormDatasetUpdateComponent } from '@components-v3/form/dataset/update/update.component';

import { MyDatasetEditV2Component } from './my-dataset-edit-v2.component';

export const routes: Routes = [
  {
    path: '',
    component: MyDatasetEditV2Component,
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    BreadcrumbComponent,
    HeaderFormComponent,
    FormDatasetUpdateComponent,
  ],
  declarations: [MyDatasetEditV2Component],
  bootstrap: [MyDatasetEditV2Component],
})
export class MyDatasetEditModule {}
