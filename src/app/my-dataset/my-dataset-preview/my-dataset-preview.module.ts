import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';

import { BreadcrumbComponent } from '@components-v4';
import { FormComponent as HeaderFormComponent } from '@components-v3/header/form/form.component';
import { PreviewComponent as FormDatasetPreviewComponent } from '@components-v3/form/dataset/preview/preview.component';

import { MyDatasetPreviewComponent } from './my-dataset-preview.component';

export const routes: Routes = [
  {
    path: '',
    component: MyDatasetPreviewComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    BreadcrumbComponent,
    HeaderFormComponent,
    FormDatasetPreviewComponent,
  ],
  declarations: [MyDatasetPreviewComponent],
  bootstrap: [MyDatasetPreviewComponent],
})
export class MyDatasetPreviewModule {}
