import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModalModule } from '@components-v3/modal';
import { JdsBiModalModule } from '@jds-bi/core';
import { DatasetLeaveComponent as ModalDatasetLeaveComponent } from '@components-v3/modal/dataset-leave/dataset-leave.component';

import { MyDatasetRoutingModule } from './my-dataset-routing.module';

@NgModule({
  imports: [
    CommonModule,
    MyDatasetRoutingModule,
    ModalModule,
    JdsBiModalModule,
  ],
  declarations: [ModalDatasetLeaveComponent],
})
export class MyDatasetModule {}
