import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';

import { BreadcrumbComponent } from '@components-v4';
import { FormComponent as HeaderFormComponent } from '@components-v3/header/form/form.component';
import { CreateComponent as FormDatasetCreateComponent } from '@components-v3/form/dataset/create/create.component';

import { MyDatasetAddV2Component } from './my-dataset-add-v2.component';

export const routes: Routes = [
  {
    path: '',
    component: MyDatasetAddV2Component,
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    BreadcrumbComponent,
    HeaderFormComponent,
    FormDatasetCreateComponent,
  ],
  declarations: [MyDatasetAddV2Component],
  bootstrap: [MyDatasetAddV2Component],
})
export class MyDatasetAddModule {}
