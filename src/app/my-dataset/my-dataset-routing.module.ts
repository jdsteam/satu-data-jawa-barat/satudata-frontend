import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// GUARD
import {
  CanDeactivateGuard,
  MyDatasetAddGuard,
  MyDatasetEditGuard,
  MyDatasetPreviewGuard,
  StoreDatasetDetailGuard,
} from '@guards-v3';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'add',
        canActivate: [MyDatasetAddGuard],
        canDeactivate: [CanDeactivateGuard],
        loadChildren: () =>
          import('./my-dataset-add/my-dataset-add.module').then(
            (m) => m.MyDatasetAddModule,
          ),
      },
      {
        path: 'edit/:id',
        canActivate: [MyDatasetEditGuard, StoreDatasetDetailGuard],
        canDeactivate: [CanDeactivateGuard],
        loadChildren: () =>
          import('./my-dataset-edit/my-dataset-edit.module').then(
            (m) => m.MyDatasetEditModule,
          ),
      },
      {
        path: 'preview/:id',
        canActivate: [MyDatasetPreviewGuard],
        loadChildren: () =>
          import('./my-dataset-preview/my-dataset-preview.module').then(
            (m) => m.MyDatasetPreviewModule,
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MyDatasetRoutingModule {}
