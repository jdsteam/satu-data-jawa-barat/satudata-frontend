import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';

import { DatasetComponent as FormRequestDatasetComponent } from '@components-v3/form/request/dataset/dataset.component';

import { RequestDatasetComponent } from './request-dataset.component';

export const routes: Routes = [
  {
    path: '',
    component: RequestDatasetComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormRequestDatasetComponent,
  ],
  declarations: [RequestDatasetComponent],
  bootstrap: [RequestDatasetComponent],
})
export class RequestDatasetModule {}
