import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        redirectTo: 'dataset',
        pathMatch: 'full',
      },
      {
        path: 'dataset',
        loadChildren: () =>
          import('./request-dataset/request-dataset.module').then(
            (m) => m.RequestDatasetModule,
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RequestRoutingModule {}
