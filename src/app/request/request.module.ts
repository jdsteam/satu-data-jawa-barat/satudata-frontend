import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequestRoutingModule } from './request-routing.module';

@NgModule({
  imports: [CommonModule, RequestRoutingModule],
  declarations: [],
})
export class RequestModule {}
