import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

// MODEL
import { LogDownloadIndicator } from '@models-v3';

@Injectable({
  providedIn: 'root',
})
export class LogDownloadIndicatorService {
  // API URL
  apiUrl = `${environment.logURL}satudata/download_indicator`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(private http: HttpClient) {}

  getTotal(params: any): Observable<LogDownloadIndicator> {
    const merge = new URLSearchParams(params);
    return this.http
      .get<LogDownloadIndicator>(
        `${this.apiUrl}/total?${merge}`,
        this.httpOptions,
      )
      .pipe(
        map((response) => new LogDownloadIndicator().deserialize(response)),
      );
  }

  getList(params: any): Observable<LogDownloadIndicator> {
    const merge = new URLSearchParams(params);
    return this.http
      .get<LogDownloadIndicator>(`${this.apiUrl}?${merge}`, this.httpOptions)
      .pipe(
        map((response) => new LogDownloadIndicator().deserialize(response)),
      );
  }
}
