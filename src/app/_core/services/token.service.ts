import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

import Utf8 from 'crypto-js/enc-utf8';
import Base64 from 'crypto-js/enc-base64';
import hmacSHA256 from 'crypto-js/hmac-sha256';

@Injectable({
  providedIn: 'root',
})
export class TokenService {
  base64url(source) {
    let encodedSource: string;
    encodedSource = Base64.stringify(source);
    encodedSource = encodedSource.replace(/=+$/, '');
    encodedSource = encodedSource.replace(/\+/g, '-');
    encodedSource = encodedSource.replace(/\//g, '_');

    return encodedSource;
  }

  createToken(item: string): string {
    const header = {
      alg: environment.jwtAlgorithm,
      typ: 'JWT',
    };

    const stringifiedHeader = Utf8.parse(JSON.stringify(header));
    const encodedHeader = this.base64url(stringifiedHeader);

    const data = {
      item,
    };

    const stringifiedData = Utf8.parse(JSON.stringify(data));
    const encodedData = this.base64url(stringifiedData);

    const token = `${encodedHeader}.${encodedData}`;

    const secret = environment.jwtSecretKey;

    let signature = hmacSHA256(token, secret);
    signature = this.base64url(signature);

    return `${token}.${signature}`;
  }
}
