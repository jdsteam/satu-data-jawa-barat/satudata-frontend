import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

// MODEL
import { LogIndicator } from '@models-v3';

@Injectable({
  providedIn: 'root',
})
export class LogIndicatorService {
  // API URL
  apiUrl = `${environment.logURL}satudata/indicator`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(private http: HttpClient) {}

  getTotal(params: any): Observable<LogIndicator> {
    const merge = new URLSearchParams(params);
    return this.http
      .get<LogIndicator>(`${this.apiUrl}/total?${merge}`, this.httpOptions)
      .pipe(map((response) => new LogIndicator().deserialize(response)));
  }

  getList(params: any): Observable<LogIndicator> {
    const merge = new URLSearchParams(params);
    return this.http
      .get<LogIndicator>(`${this.apiUrl}?${merge}`, this.httpOptions)
      .pipe(map((response) => new LogIndicator().deserialize(response)));
  }
}
