import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class CoreDataService {
  // API URL
  apiUrl = `${environment.backendURL}bigdata`;
  apiGeoJSON = `${environment.backendURL}geojson`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(private http: HttpClient) {}

  getTotal(url: string, params: any): Observable<any> {
    const merge = new URLSearchParams(params);
    return this.http.get<any>(`${url}?${merge}&count=true`, this.httpOptions);
  }

  getList(url: string, params: any): Observable<any> {
    const merge = new URLSearchParams(params);
    return this.http.get<any>(`${url}?${merge}`, this.httpOptions);
  }

  getCSV(url: string, params: any): Observable<any> {
    const merge = new URLSearchParams(params);
    return this.http.get(`${url}?${merge}`, {
      responseType: 'blob',
      reportProgress: true,
      observe: 'events',
      headers: {
        'Cache-Control': 'no-cache',
      },
    });
  }

  getListV2(url: string, params: any): Observable<any> {
    const merge = new URLSearchParams(params);
    return this.http.get<any>(
      `${this.apiUrl}${url}?${merge}`,
      this.httpOptions,
    );
  }

  getGeoJSON(params: any): Observable<any> {
    const merge = new URLSearchParams(params);
    return this.http.get<any>(`${this.apiGeoJSON}?${merge}`, this.httpOptions);
  }
}
