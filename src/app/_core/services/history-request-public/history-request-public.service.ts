import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class HistoryRequestPublicService {
  // API path
  basePath = `${environment.backendURL}history_request_public`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(public http: HttpClient) {}

  getItems(params: string): Observable<any> {
    return this.http.get<any>(this.basePath + params);
  }

  getItemsTotal(params: string): Observable<any> {
    return this.http.get<any>(this.basePath + params);
  }

  createItem(item): Observable<any> {
    return this.http.post<any>(
      this.basePath,
      JSON.stringify(item),
      this.httpOptions,
    );
  }

  getStatus(status: number) {
    let name: string;

    switch (status) {
      case 1:
        name = 'Diajukan';
        break;
      case 2:
        name = 'Diproses';
        break;
      case 3:
        name = 'Ditolak';
        break;
      case 4:
        name = 'Diakomodir';
        break;
      default:
        name = '';
        break;
    }

    return name;
  }
}
