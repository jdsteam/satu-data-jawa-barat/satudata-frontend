import { Renderer2, Inject, Injectable } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Injectable({
  providedIn: 'root',
})
export class ScriptService {
  constructor(@Inject(DOCUMENT) private document: Document) {}

  public loadPartytownScript(
    renderer: Renderer2,
    src: string,
  ): HTMLScriptElement {
    const script = renderer.createElement('script');
    script.type = 'text/partytown';
    script.src = src;
    renderer.appendChild(this.document.body, script);
    return script;
  }

  public loadPartytownText(renderer: Renderer2, text: any): HTMLScriptElement {
    const script = renderer.createElement('script');
    script.type = 'text/partytown';
    script.text = text;
    renderer.appendChild(this.document.body, script);
    return script;
  }

  public loadJSScript(renderer: Renderer2, src: string): HTMLScriptElement {
    const script = renderer.createElement('script');
    script.src = src;
    renderer.appendChild(this.document.body, script);
    return script;
  }

  public loadJSText(renderer: Renderer2, text: any): HTMLScriptElement {
    const script = renderer.createElement('script');
    script.text = text;
    renderer.appendChild(this.document.body, script);
    return script;
  }
}
