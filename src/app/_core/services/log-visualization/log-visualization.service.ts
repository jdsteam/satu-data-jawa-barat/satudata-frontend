import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

// MODEL
import { LogVisualization } from '@models-v3';

@Injectable({
  providedIn: 'root',
})
export class LogVisualizationService {
  // API URL
  apiUrl = `${environment.logURL}satudata/visualization`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(private http: HttpClient) {}

  getTotal(params: any): Observable<LogVisualization> {
    const merge = new URLSearchParams(params);
    return this.http
      .get<LogVisualization>(`${this.apiUrl}/total?${merge}`, this.httpOptions)
      .pipe(map((response) => new LogVisualization().deserialize(response)));
  }

  getList(params: any): Observable<LogVisualization> {
    const merge = new URLSearchParams(params);
    return this.http
      .get<LogVisualization>(`${this.apiUrl}?${merge}`, this.httpOptions)
      .pipe(map((response) => new LogVisualization().deserialize(response)));
  }
}
