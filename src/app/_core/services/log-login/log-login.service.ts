import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

// MODEL
import { LogLogin } from '@models-v3';

@Injectable({
  providedIn: 'root',
})
export class LogLoginService {
  // API URL
  apiUrl = `${environment.logURL}satudata/login`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(private http: HttpClient) {}

  getTotal(params: any): Observable<LogLogin> {
    const merge = new URLSearchParams(params);
    return this.http
      .get<LogLogin>(`${this.apiUrl}/total?${merge}`, this.httpOptions)
      .pipe(map((response) => new LogLogin().deserialize(response)));
  }

  getList(params: any): Observable<LogLogin> {
    const merge = new URLSearchParams(params);
    return this.http
      .get<LogLogin>(`${this.apiUrl}?${merge}`, this.httpOptions)
      .pipe(map((response) => new LogLogin().deserialize(response)));
  }
}
