import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class LogService {
  // Variable
  public isBrowser: any;
  env = environment;
  random: number;
  index: number;

  // API URL
  apiUrl = `${environment.logURL}`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(
    private http: HttpClient,
    @Inject(PLATFORM_ID) private platformId,
  ) {}

  getDevice(device: any): any {
    return {
      user_agent: device.userAgent,
      os: device.os,
      browser: device.browser,
      device: device.device,
      os_version: device.os_version,
      browser_version: device.browser_version,
      device_type: device.deviceType,
      orientation: device.orientation,
      is_desktop: device.isDesktop,
      is_mobile: device.isMobile,
      is_tablet: device.isTablet,
    };
  }

  createItem(log: string, item: any): Observable<any> {
    return this.http.post<any>(
      `${this.apiUrl}${log}`,
      JSON.stringify(item),
      this.httpOptions,
    );
  }
}
