import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class StateService {
  // State Authentication Login
  private authenticationLoginSource = new BehaviorSubject({});
  currentAuthenticationLogin = this.authenticationLoginSource.asObservable();

  changeAuthenticationLogin(data: any) {
    this.authenticationLoginSource.next(data);
  }

  // State Log User
  private logUserSource = new BehaviorSubject({});
  currentLogUser = this.logUserSource.asObservable();

  changeLogUser(data: any) {
    this.logUserSource.next(data);
  }

  // State Detail Dataset
  private datasetDetailSource = new BehaviorSubject({});
  currentDatasetDetail = this.datasetDetailSource.asObservable();

  changeDatasetDetail(data: any) {
    this.datasetDetailSource.next(data);
  }
}
