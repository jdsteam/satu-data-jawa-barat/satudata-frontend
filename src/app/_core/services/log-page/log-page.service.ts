import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

// MODEL
import { LogPage } from '@models-v3';

@Injectable({
  providedIn: 'root',
})
export class LogPageService {
  // API URL
  apiUrl = `${environment.logURL}satudata/page`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(private http: HttpClient) {}

  getTotal(params: any): Observable<LogPage> {
    const merge = new URLSearchParams(params);
    return this.http
      .get<LogPage>(`${this.apiUrl}/total?${merge}`, this.httpOptions)
      .pipe(map((response) => new LogPage().deserialize(response)));
  }

  getList(params: any): Observable<LogPage> {
    const merge = new URLSearchParams(params);
    return this.http
      .get<LogPage>(`${this.apiUrl}?${merge}`, this.httpOptions)
      .pipe(map((response) => new LogPage().deserialize(response)));
  }
}
