import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class LicenseService {
  // API URL
  apiUrl = `${environment.backendURL}license`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(private http: HttpClient) {}

  getTotal(params: any): Observable<any> {
    const merge = new URLSearchParams(params);
    return this.http.get<any>(
      `${this.apiUrl}?${merge}&count=true`,
      this.httpOptions,
    );
  }

  getList(params: any): Observable<any> {
    const merge = new URLSearchParams(params);
    return this.http.get<any>(`${this.apiUrl}?${merge}`, this.httpOptions);
  }

  getSingle(id: number, params: any): Observable<any> {
    const merge = new URLSearchParams(params);
    return this.http.get<any>(
      `${this.apiUrl}/${id}?${merge}`,
      this.httpOptions,
    );
  }

  getCounter(id: number, params: any): Observable<any> {
    const merge = new URLSearchParams(params);
    return this.http.get<any>(
      `${this.apiUrl}/counter/${id}?${merge}`,
      this.httpOptions,
    );
  }

  getSingleByTitle(title: string, params: any): Observable<any> {
    const merge = new URLSearchParams(params);
    return this.http.get<any>(
      `${this.apiUrl}/${title}?${merge}`,
      this.httpOptions,
    );
  }

  getCounterByTitle(title: string, params: any): Observable<any> {
    const merge = new URLSearchParams(params);
    return this.http.get<any>(
      `${this.apiUrl}/counter/${title}?${merge}`,
      this.httpOptions,
    );
  }
}
