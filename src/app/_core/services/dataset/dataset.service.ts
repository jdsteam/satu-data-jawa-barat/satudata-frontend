/* eslint-disable @typescript-eslint/dot-notation */
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

import { jdsBiColor } from '@jds-bi/cdk';

import _ from 'lodash';
import accounting from 'accounting-js';

@Injectable({
  providedIn: 'root',
})
export class DatasetService {
  // end point
  apiUrl = `${environment.backendURL}dataset`;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(private http: HttpClient) {}

  getTotal(params: any): Observable<any> {
    const merge = new URLSearchParams(params);
    return this.http.get<any>(
      `${this.apiUrl}?${merge}&count=true`,
      this.httpOptions,
    );
  }

  getList(params: any): Observable<any> {
    const merge = new URLSearchParams(params);
    return this.http.get<any>(`${this.apiUrl}?${merge}`, this.httpOptions);
  }

  getSingle(id: number, params: any): Observable<any> {
    const merge = new URLSearchParams(params);
    return this.http.get<any>(
      `${this.apiUrl}/${id}?${merge}`,
      this.httpOptions,
    );
  }

  createItem(item: any): Observable<any> {
    return this.http.post<any>(
      this.apiUrl,
      JSON.stringify(item),
      this.httpOptions,
    );
  }

  updateItem(uniq: any, item, params: any): Observable<any> {
    const merge = new URLSearchParams(params);
    return this.http.put<any>(
      `${this.apiUrl}/${uniq}?${merge}`,
      JSON.stringify(item),
      this.httpOptions,
    );
  }

  getCounter(id: number, params: any): Observable<any> {
    const merge = new URLSearchParams(params);
    return this.http.get<any>(
      `${this.apiUrl}/counter/${id}?${merge}`,
      this.httpOptions,
    );
  }

  getSingleByTitle(title: string, params: any): Observable<any> {
    const merge = new URLSearchParams(params);
    return this.http.get<any>(
      `${this.apiUrl}/${title}?${merge}`,
      this.httpOptions,
    );
  }

  getCounterByTitle(title: string, params: any): Observable<any> {
    const merge = new URLSearchParams(params);
    return this.http.get<any>(
      `${this.apiUrl}/counter/${title}?${merge}`,
      this.httpOptions,
    );
  }

  getClassification(id: number) {
    let name: string;

    switch (id) {
      case 3:
        name = 'public';
        break;
      case 4:
        name = 'private';
        break;
      case 5:
        name = 'internal';
        break;
      default:
        break;
    }

    return name;
  }

  getClassificationColor(id: number) {
    let color: string;

    switch (id) {
      case 3:
        color = 'green';
        break;
      case 4:
        color = 'red';
        break;
      case 5:
        color = 'blue-gray';
        break;
      default:
        break;
    }

    return color;
  }

  getSource(isRealtime: boolean, isPermanent: boolean) {
    let status: string;
    let name: string;
    let className: string;
    let color: string;
    let icon: string;

    if (isRealtime) {
      status = 'realtime';
      name = 'Realtime';
      className = 'realtime-status';
      color = 'blue';
      icon = 'clock-rotate-left';
    } else {
      // eslint-disable-next-line no-lonely-if
      if (isPermanent) {
        status = 'permanent';
        name = 'Tetap';
        className = 'permanent-status';
        color = 'green';
        icon = 'circle-check';
      } else {
        status = 'temporary';
        name = 'Sementara';
        className = 'temporary-status';
        color = 'yellow';
        icon = 'file-timer';
      }
    }

    return {
      status,
      name,
      className,
      color,
      icon,
    };
  }

  getQualityScoreColor(score: number) {
    let color: string;

    if (score >= 90) {
      color = jdsBiColor.green['600'];
    } else if (score > 75) {
      color = jdsBiColor.blue['600'];
    } else if (score >= 60) {
      color = jdsBiColor.yellow['900'];
    } else if (score < 60) {
      color = jdsBiColor.red['600'];
    } else {
      color = jdsBiColor.gray['800'];
    }

    return color;
  }

  getMetadata(metadata: any, join = false) {
    const metadataBusiness = _.filter(metadata, { metadata_type_id: 1 });
    const metadataCustom = _.filter(metadata, { metadata_type_id: 4 });

    const start = _.find(metadataBusiness, { key: 'Dimensi Dataset Awal' });
    const end = _.find(metadataBusiness, { key: 'Dimensi Dataset Akhir' });
    const source = _.find(metadataBusiness, { key: 'Sumber External' });

    const data = _.filter(metadataBusiness, (res) => {
      return (
        res.key !== 'Dimensi Dataset Awal' &&
        res.key !== 'Dimensi Dataset Akhir' &&
        res.key !== 'Sumber External'
      );
    });

    if (start && end) {
      data.push({
        key: 'Dimensi Dataset',
        value: `${start.value} - ${end.value}`,
      });
    }

    if (source && source.value) {
      data.push(source);
    }

    let custom = data;
    if (join && metadataCustom.length > 0) {
      custom = data.concat(metadataCustom);
    }

    return custom;
  }

  getMetadataDimension(metadata: any) {
    const start = metadata.find((o) => o.key === 'Dimensi Dataset Awal');
    const end = metadata.find((o) => o.key === 'Dimensi Dataset Akhir');
    const available = metadata.find((o) => o.key === 'Tahun Tersedia Dataset');

    let result: string;
    if (start && end) {
      result = `${start.value} - ${end.value}`;
    } else if (available) {
      result = available.value;
    } else {
      result = '-';
    }

    return result;
  }

  formatNumber(value) {
    let val = value;
    const number = val.match(/\d/g);
    // eslint-disable-next-line no-useless-escape
    const letter = val.match(/[A-Za-z\-]/g);
    const numberTotal = number ? number.length : 0;
    const letterTotal = letter ? letter.length : 0;

    if (numberTotal >= letterTotal) {
      if (val.includes('.') && val.includes(',')) {
        val = null;
      } else if (val.includes('.')) {
        val = accounting.unformat(val, '.');
      } else if (val.includes(',')) {
        val = null;
      }
    } else {
      // eslint-disable-next-line no-lonely-if
      if (val === '-') {
        val = null;
      }
    }

    return value;
  }

  checkTypeOf(metadata, value) {
    let val = value;
    val = String(val);

    const conditions = ['jumlah', 'persen', 'nilai', 'luas', 'indeks'];
    const check = conditions.some((el) => metadata.includes(el));

    if (check) {
      if (
        val === '' ||
        val === 'null' ||
        val === 'undefined' ||
        val === undefined
      ) {
        val = null;
      } else {
        val = this.formatNumber(val);
      }
    }

    // eslint-disable-next-line no-restricted-globals
    const num = !isNaN(val) && isFinite(val) && !/e/i.test(val);
    if (num) {
      if (val !== null) {
        return Number(val);
      }

      return val;
    }

    const bool = val === 'true' || val === 'false';
    if (bool) {
      return val === 'true';
    }

    return val;
  }

  checkUndefined(metadata, temp) {
    let numEmpty = 0;
    // eslint-disable-next-line no-restricted-syntax
    for (const value of metadata) {
      if (temp[value] === null) {
        numEmpty += 1;
      }
    }

    if (
      numEmpty < metadata.length &&
      Object.keys(temp).length === metadata.length
    ) {
      const convertToArray = Object.keys(temp).map((key) => temp[key]);

      return convertToArray.every((val) => {
        if (typeof val === 'undefined' || val === 'undefined' || val === null) {
          return true;
        }
        return false;
      });
    }

    return true;
  }

  getDataJson(spreadsheet: any, config: any) {
    const metadata = [];
    const data = [];

    for (let row = 0; row <= config.num_row; row += 1) {
      const temp = {};
      for (let col = 0; col <= config.num_col; col += 1) {
        if (row === 0) {
          if (spreadsheet.cell(row, col)) {
            metadata.push(spreadsheet.cell(row, col)['text']);
          }
        } else {
          // eslint-disable-next-line no-lonely-if
          if (col < metadata.length) {
            if (spreadsheet.cell(row, col)) {
              const value = this.checkTypeOf(
                metadata[col],
                spreadsheet.cell(row, col)['text'],
              );
              temp[metadata[col]] = value;
            } else {
              temp[metadata[col]] = null;
            }
          }
        }
      }

      const allUndefined = this.checkUndefined(metadata, temp);

      if (!allUndefined) {
        data.push(temp);
      }
    }

    return {
      data,
      metadata,
    };
  }
}
