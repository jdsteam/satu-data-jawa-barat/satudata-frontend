import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  // end point
  apiUrl = `${environment.backendURL}user`;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(private http: HttpClient) {}

  getSingle(id: number, params: any): Observable<any> {
    const merge = new URLSearchParams(params);
    return this.http.get<any>(
      `${this.apiUrl}/${id}?${merge}`,
      this.httpOptions,
    );
  }

  updatePassword(uniq: any, item, params: any): Observable<any> {
    const merge = new URLSearchParams(params);
    return this.http.put<any>(
      `${this.apiUrl}/change/${uniq}?${merge}`,
      JSON.stringify(item),
      this.httpOptions,
    );
  }

  updateAgreement(uniq: any, token, item): Observable<any> {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      }),
    };

    return this.http.put<any>(
      `${this.apiUrl}/${uniq}`,
      JSON.stringify(item),
      headers,
    );
  }
}
