import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

// MODEL
import { Table } from '@models-v3';

@Injectable({
  providedIn: 'root',
})
export class TableService {
  // API URL
  apiUrl = `${environment.backendURL}bigdata/table`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(private http: HttpClient) {}

  getList(params: any): Observable<Table> {
    const merge = new URLSearchParams(params);
    return this.http
      .get<Table>(`${this.apiUrl}?${merge}`, this.httpOptions)
      .pipe(map((response) => new Table().deserialize(response)));
  }
}
