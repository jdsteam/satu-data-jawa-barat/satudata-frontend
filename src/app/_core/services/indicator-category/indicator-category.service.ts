import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class IndicatorCategoryService {
  // API URL
  apiUrl = `${environment.backendURL}indikator_category`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(private http: HttpClient) {}

  getTotal(params: any): Observable<any> {
    const merge = new URLSearchParams(params);
    return this.http.get<any>(
      `${this.apiUrl}?${merge}&count=true`,
      this.httpOptions,
    );
  }

  getList(params: any): Observable<any> {
    const merge = new URLSearchParams(params);
    return this.http.get<any>(`${this.apiUrl}?${merge}`, this.httpOptions);
  }

  getSingle(uniq: any, params: any): Observable<any> {
    const merge = new URLSearchParams(params);
    return this.http.get<any>(
      `${this.apiUrl}/${uniq}?${merge}`,
      this.httpOptions,
    );
  }

  getCounter(uniq: any, params: any): Observable<any> {
    const merge = new URLSearchParams(params);
    return this.http.get<any>(
      `${this.apiUrl}/counter/${uniq}?${merge}`,
      this.httpOptions,
    );
  }
}
