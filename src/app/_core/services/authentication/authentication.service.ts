import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

// MODEL
import { Login, LoginData } from '@models-v3';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  // API URL
  apiUrl = `${environment.backendURL}auth`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  private satudataUserSubject: BehaviorSubject<LoginData>;
  public satudataUser: Observable<LoginData>;
  private satudataTokenSubject: BehaviorSubject<string>;
  public satudataToken: Observable<string>;

  public get satudataUserValue(): LoginData {
    return this.satudataUserSubject.value;
  }

  public get satudataTokenValue(): string {
    return this.satudataTokenSubject.value;
  }

  constructor(private http: HttpClient) {
    this.satudataUserSubject = new BehaviorSubject<LoginData>(
      JSON.parse(localStorage.getItem('satudata-user')),
    );
    this.satudataUser = this.satudataUserSubject.asObservable();
    this.satudataTokenSubject = new BehaviorSubject<string>(
      localStorage.getItem('satudata-token'),
    );
    this.satudataToken = this.satudataTokenSubject.asObservable();
  }

  configSSO() {
    const clientID = !environment.production ? 'dev-satudata' : 'satudata';
    const redirectURI = `${environment.satudataURL}auth/autologin_sso`;
    const authURL = `${environment.ssoURL}realms/ssojabar/protocol/openid-connect/auth?response_type=code&client_id=${clientID}&redirect_uri=${redirectURI}`;

    return {
      clientID,
      redirectURI,
      authURL,
    };
  }

  configSSODigiteam() {
    const clientID = !environment.production ? 'satudata-dev' : 'satudata-prod';
    const redirectURI = `${environment.satudataURL}autologin_digiteam`;
    const authURL = `${environment.ssoDigiteamURL}realms/jabarprov/protocol/openid-connect/auth?response_type=code&client_id=${clientID}&redirect_uri=${redirectURI}`;

    return {
      clientID,
      redirectURI,
      authURL,
    };
  }

  login(
    username: string,
    password: string,
    token: string,
    isRememberMe: boolean,
  ) {
    const appCode = 'satudata-frontend';
    return this.http
      .post<Login>(
        `${this.apiUrl}/login2`,
        {
          username,
          password,
          app_code: appCode,
          token,
        },
        { observe: 'response' },
      )
      .pipe(
        map((result) => {
          if (result.status === 200) {
            if (isRememberMe) {
              localStorage.setItem('satudata-username', username);
            }
          }
          return result;
        }),
      );
  }

  verifyToken(
    username: string,
    authenticatorKey: string,
    anotationKey: string,
  ) {
    const item = {
      username,
      authenticatorKey,
      anotationKey,
    };

    return this.http.post<any>(
      `${this.apiUrl}/verify-token2`,
      JSON.stringify(item),
      this.httpOptions,
    );
  }

  verifyTokenSSO(code: string) {
    const item = {
      client_id: this.configSSO().clientID,
      code,
      redirect_uri: this.configSSO().redirectURI,
    };

    return this.http.post<any>(
      `${this.apiUrl}/verify-token-sso`,
      JSON.stringify(item),
      this.httpOptions,
    );
  }

  verifyTokenSSODigiteam(code: string) {
    const item = {
      client_id: this.configSSODigiteam().clientID,
      code,
      redirect_uri: this.configSSODigiteam().redirectURI,
    };

    return this.http.post<any>(
      `${this.apiUrl}/verify-token-digiteam`,
      JSON.stringify(item),
      this.httpOptions,
    );
  }

  setSession(data: LoginData) {
    localStorage.setItem('satudata-user', JSON.stringify(data));
    this.satudataUserSubject.next(data);

    this.setToken(data.jwt);
  }

  setToken(token: string) {
    localStorage.setItem('satudata-token', token);
    this.satudataTokenSubject.next(token);
  }

  async logout() {
    localStorage.removeItem('satudata-user');
    localStorage.removeItem('satudata-token');

    this.satudataUserSubject.next(null);
    this.satudataTokenSubject.next(null);
  }

  forgotPassword(item: any): Observable<any> {
    return this.http.post<any>(
      `${this.apiUrl}/reset`,
      JSON.stringify(item),
      this.httpOptions,
    );
  }

  get dataRememberMe() {
    return localStorage.getItem('satudata-username')
      ? localStorage.getItem('satudata-username')
      : null;
  }
}
