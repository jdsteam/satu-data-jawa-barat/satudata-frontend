import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

// MODEL
import { OrganizationStructure } from '@models-v3';

@Injectable({
  providedIn: 'root',
})
export class OrganizationStructureService {
  // API URL
  apiUrl = `${environment.backendURL}struktur_organisasi`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(private http: HttpClient) {}

  getTotal(params: any): Observable<OrganizationStructure> {
    const merge = new URLSearchParams(params);
    return this.http
      .get<OrganizationStructure>(
        `${this.apiUrl}?${merge}&count=true`,
        this.httpOptions,
      )
      .pipe(
        map((response) => new OrganizationStructure().deserialize(response)),
      );
  }

  getList(params: any): Observable<OrganizationStructure> {
    const merge = new URLSearchParams(params);
    return this.http
      .get<OrganizationStructure>(`${this.apiUrl}?${merge}`, this.httpOptions)
      .pipe(
        map((response) => new OrganizationStructure().deserialize(response)),
      );
  }

  getSingle(id: number, params: any): Observable<OrganizationStructure> {
    const merge = new URLSearchParams(params);
    return this.http
      .get<OrganizationStructure>(
        `${this.apiUrl}/${id}?${merge}`,
        this.httpOptions,
      )
      .pipe(
        map((response) => new OrganizationStructure().deserialize(response)),
      );
  }

  getSearchItem(params: any) {
    const merge = new URLSearchParams(params);
    return this.http.get<OrganizationStructure>(`${this.apiUrl}?${merge}`).pipe(
      map((response) => response.data),
      delay(500),
    );
  }
}
