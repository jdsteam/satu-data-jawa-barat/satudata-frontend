import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class IndicatorService {
  // API URL
  apiUrl = `${environment.backendURL}indikator`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(private http: HttpClient) {}

  getTotal(params: any): Observable<any> {
    const merge = new URLSearchParams(params);
    return this.http.get<any>(
      `${this.apiUrl}?${merge}&count=true`,
      this.httpOptions,
    );
  }

  getList(params: any): Observable<any> {
    const merge = new URLSearchParams(params);
    return this.http.get<any>(`${this.apiUrl}?${merge}`, this.httpOptions);
  }

  getSingle(uniq: any, params: any): Observable<any> {
    const merge = new URLSearchParams(params);
    return this.http.get<any>(
      `${this.apiUrl}/${uniq}?${merge}`,
      this.httpOptions,
    );
  }

  createItem(item: any): Observable<any> {
    return this.http.post<any>(
      this.apiUrl,
      JSON.stringify(item),
      this.httpOptions,
    );
  }

  updateItem(uniq: any, item): Observable<any> {
    return this.http.put<any>(
      `${this.apiUrl}/${uniq}`,
      JSON.stringify(item),
      this.httpOptions,
    );
  }

  getCounter(uniq: any, params: any): Observable<any> {
    const merge = new URLSearchParams(params);
    return this.http.get<any>(
      `${this.apiUrl}/counter/${uniq}?${merge}`,
      this.httpOptions,
    );
  }

  getClassification(id: number) {
    let name: string;

    switch (id) {
      case 1:
        name = 'lakip';
        break;
      case 2:
        name = 'lkpd';
        break;
      case 3:
        name = 'lkpj';
        break;
      case 4:
        name = 'rkpd';
        break;
      case 5:
        name = 'rpjmd';
        break;
      case 6:
        name = 'sdgs';
        break;
      default:
        break;
    }

    return name;
  }

  getClassificationColor(id: number) {
    let name: string;

    switch (id) {
      case 1:
        name = 'blue';
        break;
      case 2:
        name = 'green';
        break;
      case 3:
        name = 'purple';
        break;
      case 4:
        name = 'yellow';
        break;
      case 5:
        name = 'gray';
        break;
      case 6:
        name = 'pink';
        break;
      case 7:
        name = 'blue-gray';
        break;
      default:
        break;
    }

    return name;
  }
}
