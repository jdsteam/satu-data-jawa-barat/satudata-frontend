import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

// MODEL
import { Schema } from '@models-v3';

@Injectable({
  providedIn: 'root',
})
export class SchemaService {
  // API URL
  apiUrl = `${environment.backendURL}bigdata/schema`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(private http: HttpClient) {}

  getList(params: any): Observable<Schema> {
    const merge = new URLSearchParams(params);
    return this.http
      .get<Schema>(`${this.apiUrl}?${merge}`, this.httpOptions)
      .pipe(map((response) => new Schema().deserialize(response)));
  }
}
