import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class HistoryRequestPrivateService {
  // end point
  apiUrl = `${environment.backendURL}history_request_private`;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(private http: HttpClient) {}

  getTotal(params: any): Observable<any> {
    const merge = new URLSearchParams(params);
    return this.http.get<any>(
      `${this.apiUrl}?${merge}&count=true`,
      this.httpOptions,
    );
  }

  getList(params: any): Observable<any> {
    const merge = new URLSearchParams(params);
    return this.http.get<any>(`${this.apiUrl}?${merge}`, this.httpOptions);
  }

  getSingle(id: number, params: any): Observable<any> {
    const merge = new URLSearchParams(params);
    return this.http.get<any>(
      `${this.apiUrl}/${id}?${merge}`,
      this.httpOptions,
    );
  }

  createItem(item: any): Observable<any> {
    return this.http.post<any>(
      this.apiUrl,
      JSON.stringify(item),
      this.httpOptions,
    );
  }

  updateItem(uniq: any, item): Observable<any> {
    return this.http.put<any>(
      `${this.apiUrl}/${uniq}`,
      JSON.stringify(item),
      this.httpOptions,
    );
  }

  getStatus(status: number) {
    let name: string;

    switch (status) {
      case 1:
        name = 'Diajukan';
        break;
      case 2:
        name = 'Diproses';
        break;
      case 3:
        name = 'Diakomodir';
        break;
      case 4:
        name = 'Ditolak';
        break;
      default:
        name = '';
        break;
    }

    return name;
  }
}
