import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

import * as _ from 'lodash';

@Injectable({
  providedIn: 'root',
})
export class NotificationService {
  // API URL
  apiUrl = `${environment.backendURL}notification`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(private http: HttpClient) {}

  getTotal(params: any): Observable<any> {
    const merge = new URLSearchParams(params);
    return this.http.get<any>(
      `${this.apiUrl}?${merge}&count=true`,
      this.httpOptions,
    );
  }

  getList(params: any): Observable<any> {
    const merge = new URLSearchParams(params);
    return this.http.get<any>(`${this.apiUrl}?${merge}`, this.httpOptions);
  }

  getSingle(id: number, params: any): Observable<any> {
    const merge = new URLSearchParams(params);
    return this.http.get<any>(
      `${this.apiUrl}/${id}?${merge}`,
      this.httpOptions,
    );
  }

  getTrigger(id: number): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/trigger/${id}`, this.httpOptions);
  }

  getClear(): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/clear`, this.httpOptions);
  }

  updateItemBulk(params: any, item: any): Observable<any> {
    const merge = new URLSearchParams(params);
    return this.http.put<any>(
      `${this.apiUrl}?${merge}`,
      JSON.stringify(item),
      this.httpOptions,
    );
  }

  getRequestDataset(title: string, content: string) {
    let request: string;
    if (_.includes(content, 'Nomor ticket')) {
      const splitted = content.split('Nomor ticket');
      request = `${title} ${splitted[0]}<br>Nomor Tiket${splitted[1]}`;
    } else if (_.includes(content, 'Nomor tiket')) {
      const splitted = content.split('Nomor tiket');
      request = `${title} ${splitted[0]}<br>Nomor Tiket${splitted[1]}`;
    } else {
      request = `${title} ${content}`;
    }
    return request;
  }
}
