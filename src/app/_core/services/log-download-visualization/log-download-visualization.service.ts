import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

// MODEL
import { LogDownloadVisualization } from '@models-v3';

@Injectable({
  providedIn: 'root',
})
export class LogDownloadVisualizationService {
  // API URL
  apiUrl = `${environment.logURL}satudata/download_visualization`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(private http: HttpClient) {}

  getTotal(params: any): Observable<LogDownloadVisualization> {
    const merge = new URLSearchParams(params);
    return this.http
      .get<LogDownloadVisualization>(
        `${this.apiUrl}/total?${merge}`,
        this.httpOptions,
      )
      .pipe(
        map((response) => new LogDownloadVisualization().deserialize(response)),
      );
  }

  getList(params: any): Observable<LogDownloadVisualization> {
    const merge = new URLSearchParams(params);
    return this.http
      .get<LogDownloadVisualization>(
        `${this.apiUrl}?${merge}`,
        this.httpOptions,
      )
      .pipe(
        map((response) => new LogDownloadVisualization().deserialize(response)),
      );
  }
}
