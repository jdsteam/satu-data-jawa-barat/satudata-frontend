import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(private http: HttpClient) {}

  getFile(url: string, params: any): Observable<any> {
    const merge = new URLSearchParams(params);
    return this.http.get(`${url}?${merge}`, {
      responseType: 'blob',
      reportProgress: true,
      observe: 'events',
      headers: {
        'Cache-Control': 'no-cache',
      },
    });
  }

  postFile(url: string, params: any, item: any): Observable<any> {
    const merge = new URLSearchParams(params);
    return this.http.post(`${url}?${merge}`, item, {
      reportProgress: true,
      observe: 'events',
      headers: {
        'Cache-Control': 'no-cache',
      },
    });
  }

  getAPI(url: string, params: any) {
    const merge = new URLSearchParams(params);
    return this.http.get<any>(`${url}?${merge}`, {
      headers: {
        'Cache-Control': 'no-cache',
      },
    });
  }
}
