import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

// MODEL
import { LogDataset } from '@models-v3';

@Injectable({
  providedIn: 'root',
})
export class LogDatasetService {
  // API URL
  apiUrl = `${environment.logURL}satudata/dataset`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(private http: HttpClient) {}

  getTotal(params: any): Observable<LogDataset> {
    const merge = new URLSearchParams(params);
    return this.http
      .get<LogDataset>(`${this.apiUrl}/total?${merge}`, this.httpOptions)
      .pipe(map((response) => new LogDataset().deserialize(response)));
  }

  getList(params: any): Observable<LogDataset> {
    const merge = new URLSearchParams(params);
    return this.http
      .get<LogDataset>(`${this.apiUrl}?${merge}`, this.httpOptions)
      .pipe(map((response) => new LogDataset().deserialize(response)));
  }
}
