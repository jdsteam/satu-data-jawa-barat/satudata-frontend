import {
  Component,
  OnInit,
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  OnChanges,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Router, ActivatedRoute } from '@angular/router';

// STYLE
import * as jds from '@styles';

// SERVICE
import {
  DatasetService,
  IndicatorService,
  VisualizationService,
  OrganizationService,
} from '@services-v3';

// COMPONENT
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconArrowRight,
} from '@jds-bi/icons';

import { ToastrService } from 'ngx-toastr';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

import { DataStyle } from './data.style';

@Component({
  selector: 'app-statistic-data',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    NgxSkeletonLoaderModule,
    JdsBiIconsModule,
  ],
  templateUrl: './data.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DataStyle],
})
export class DataComponent implements OnInit, OnChanges, AfterViewInit {
  jds: any = jds;

  className!: any;

  // Data
  isLoadingDataset = false;
  isLoadingIndicator = false;
  isLoadingVisualization = false;
  isLoadingOrganization = false;

  isErrorDataset: boolean;
  isErrorIndicator: boolean;
  isErrorVisualization: boolean;
  isErrorOrganization: boolean;

  totalDataset: number;
  totalIndicator: number;
  totalVisualization: number;
  totalOrganization: number;

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private datasetService: DatasetService,
    private indicatorService: IndicatorService,
    private visualizationService: VisualizationService,
    private organizationService: OrganizationService,
    private toastr: ToastrService,
    private iconService: JdsBiIconsService,
    private dataStyle: DataStyle,
  ) {
    this.iconService.registerIcons([iconArrowRight]);
  }

  ngOnInit(): void {
    this.className = this.dataStyle.getDynamicStyle();

    this.getTotalDataset();
    this.getTotalIndicator();
    this.getTotalVisualization();
    this.getTotalOrganization();
  }

  ngOnChanges(): void {
    this.className = this.dataStyle.getDynamicStyle();
  }

  ngAfterViewInit(): void {
    this.cdRef.detectChanges();
  }

  getTotalDataset() {
    const pWhere = {
      is_active: true,
      is_deleted: false,
      is_validate: 3,
    };

    const params = {
      where: JSON.stringify(pWhere),
    };

    this.isLoadingDataset = true;
    this.datasetService.getTotal(params).subscribe(
      (res) => {
        this.isErrorDataset = false;
        this.totalDataset = res.data.count;
        this.isLoadingDataset = false;
        this.cdRef.markForCheck();
      },
      (error) => {
        this.isErrorDataset = true;
        this.toastr.error(error.statusText, 'Dataset');
      },
    );
  }

  getTotalIndicator() {
    const pWhere = {
      is_active: true,
      is_deleted: false,
    };

    const params = {
      where: JSON.stringify(pWhere),
    };

    this.isLoadingIndicator = true;
    this.indicatorService.getTotal(params).subscribe(
      (res) => {
        this.isErrorIndicator = false;
        this.totalIndicator = res.data.count;
        this.isLoadingIndicator = false;
        this.cdRef.markForCheck();
      },
      (error) => {
        this.isErrorIndicator = true;
        this.toastr.error(error.statusText, 'Indicator');
      },
    );
  }

  getTotalVisualization() {
    const pWhere = {
      is_active: true,
      is_deleted: false,
    };

    const params = {
      where: JSON.stringify(pWhere),
    };

    this.isLoadingVisualization = true;
    this.visualizationService.getTotal(params).subscribe(
      (res) => {
        this.isErrorVisualization = false;
        this.totalVisualization = res.data.count;
        this.isLoadingVisualization = false;
        this.cdRef.markForCheck();
      },
      (error) => {
        this.isErrorVisualization = true;
        this.toastr.error(error.statusText, 'Visualization');
      },
    );
  }

  getTotalOrganization() {
    const pWhere = {
      is_deleted: false,
      is_satudata: true,
    };

    const params = {
      where: JSON.stringify(pWhere),
    };

    this.isLoadingOrganization = true;
    this.organizationService.getTotal(params).subscribe(
      (res) => {
        this.isErrorOrganization = false;
        this.totalOrganization = res.data.count;
        this.isLoadingOrganization = false;
        this.cdRef.markForCheck();
      },
      (error) => {
        this.isErrorOrganization = true;
        this.toastr.error(error.statusText, 'Organization');
      },
    );
  }
}
