import { Injectable } from '@angular/core';

// PACKAGE
import { css } from '@emotion/css';

@Injectable()
export class DataStyle {
  getDynamicStyle(): any {
    const cardBody = css`
      padding: 0 32px;
    `;

    const link = css`
      &:hover {
        color: #0753a6;
        text-decoration: none;

        svg {
          fill: #0753a6;
        }
      }
    `;

    const contentLoading = css`
      padding: 24px 0;
      border-bottom: 1px solid #dddddd;
    `;

    const content = css`
      display: flex;
      align-items: center;
      padding: 24px 0;
      border-bottom: 1px solid #dddddd;
    `;

    const contentLast = css`
      border-bottom: 0;
    `;

    const total = css`
      font-size: 36px;
      line-height: 54px;
      letter-spacing: 0.02em;
    `;

    const icon = css`
      margin-left: auto;
    `;

    return {
      cardBody,
      link,
      contentLoading,
      content,
      contentLast,
      total,
      icon,
    };
  }
}
