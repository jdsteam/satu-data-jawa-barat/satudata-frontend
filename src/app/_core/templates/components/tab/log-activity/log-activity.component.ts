import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpEventType } from '@angular/common/http';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// MODEL
import { LoginData } from '@models-v3';

// SERVICE
import { AuthenticationService, HttpService } from '@services-v3';
import { ModalModule, ModalService } from '@components-v3/modal';

// COMPONENT
import { JdsBiInputModule, JdsBiNavModule } from '@jds-bi/core';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconDownload,
  iconMagnifyingGlass,
} from '@jds-bi/icons';

import { assign, isEmpty, isNil } from 'lodash';
import moment from 'moment';
import Swal from 'sweetalert2';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-tab-log-activity',
  standalone: true,
  imports: [
    CommonModule,
    JdsBiNavModule,
    JdsBiInputModule,
    JdsBiIconsModule,
    ModalModule,
  ],
  templateUrl: './log-activity.component.html',
  styleUrls: ['./log-activity.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LogActivityComponent implements OnInit {
  colorScheme = COLORSCHEME;
  satudataUser: LoginData;
  moment: any = moment;
  jds: any = jds;

  @Input() filterDateStart: string;
  @Input() filterDateEnd: string;
  @Output() filterOutputStatus = new EventEmitter<any>();
  @Output() filterOutputSearch = new EventEmitter<any>();
  @Output() filterOutputData = new EventEmitter<any>();

  // Variable
  filter = {
    currentPage: 1,
    search: '',
    status: 'login',
    data: 'dataset',
  };

  // Data
  downloadIsLoading = false;
  downloadName: string;
  downloadProgress = 0;

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private httpService: HttpService,
    private modalService: ModalService,
    private iconService: JdsBiIconsService,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    this.iconService.registerIcons([iconDownload, iconMagnifyingGlass]);
  }

  ngOnInit(): void {
    this.queryParams();
  }

  queryParams(): void {
    this.route.queryParams.subscribe((p) => {
      const pSearch = p.q;
      const pStatus = p.status;
      const pData = p.data;

      this.filter.search = !isNil(pSearch) ? pSearch : '';
      this.filter.data = !isNil(pData) ? pData : 'dataset';

      switch (isNil(pStatus)) {
        case true:
          if (this.satudataUser.role_name === 'walidata') {
            this.filter.status = 'login';
          } else {
            this.filter.status = 'view';
          }
          break;
        case false:
          this.filter.status = pStatus;
          break;
        default:
          break;
      }
    });
  }

  filterStatus(event: string): void {
    const value = isEmpty(event) === false ? event : null;

    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        status: value,
        currentpage: null,
        data: null,
      },
      queryParamsHandling: 'merge',
    });

    this.filter.status = value;
    this.filter.data = 'dataset';
    this.filter.currentPage = 1;
    this.filterOutputStatus.emit(this.filter);
  }

  filterSearch(event: string): void {
    const value = isEmpty(event) === false ? event : null;

    if (this.filter.search !== value) {
      this.router.navigate([], {
        relativeTo: this.route,
        queryParams: {
          q: value,
          currentpage: null,
        },
        queryParamsHandling: 'merge',
      });

      this.filter.search = value;
      this.filter.currentPage = 1;
      this.filterOutputSearch.emit(this.filter);
    }
  }

  filterData(event: string): void {
    const value = isEmpty(event) === false ? event : null;

    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        data: value,
        currentpage: null,
      },
      queryParamsHandling: 'merge',
    });

    this.filter.data = value;
    this.filter.currentPage = 1;
    this.filterOutputData.emit(this.filter);
  }

  paramData(value: string) {
    const filtersData = new Map()
      .set('dataset', { type: 'dataset' })
      .set('indicator', { type: 'indikator' })
      .set('visualization', { type: 'visualization' });

    return filtersData.get(value);
  }

  onDownload() {
    if (!this.filterDateStart && !this.filterDateEnd) {
      Swal.fire({
        type: 'warning',
        text: 'Silahkan masukan range tanggal',
        allowOutsideClick: false,
      });
      return;
    }

    this.downloadProgress = 0;

    this.openModal('modal-download');

    const pWhere = {};

    const urlLog = environment.logURL;
    let urlService: string;
    let fileName: string;
    switch (this.filter.status) {
      case 'login':
        urlService = `${urlLog}satudata/login`;
        fileName = `login`;
        assign(pWhere, { status: 'success' });
        break;
      case 'page':
        urlService = `${urlLog}satudata/page`;
        fileName = `halaman`;
        break;
      case 'view':
        if (this.filter.data === 'dataset') {
          urlService = `${urlLog}satudata/dataset`;
          fileName = `dilihat-dataset`;
        } else if (this.filter.data === 'indicator') {
          urlService = `${urlLog}satudata/indicator`;
          fileName = `dilihat-indikator`;
        } else if (this.filter.data === 'visualization') {
          urlService = `${urlLog}satudata/visualization`;
          fileName = `dilihat-visualisasi`;
        }
        assign(pWhere, { action: 'view' });
        assign(pWhere, { status: 'success' });
        break;
      case 'access':
        if (this.filter.data === 'dataset') {
          urlService = `${urlLog}satudata/download_dataset`;
          fileName = `diunduh-dataset`;
        } else if (this.filter.data === 'indicator') {
          urlService = `${urlLog}satudata/download_indicator`;
          fileName = `diunduh-indikator`;
        } else if (this.filter.data === 'visualization') {
          urlService = `${urlLog}satudata/download_visualization`;
          fileName = `diunduh-visualisasi`;
        }
        break;
      default:
        break;
    }

    const params = {
      search: this.filter.search || '',
      where: JSON.stringify(pWhere),
      download: 'xls',
      start_date: this.filterDateStart,
      end_date: this.filterDateEnd,
    };

    this.httpService.getFile(urlService, params).subscribe((response) => {
      if (response.type === HttpEventType.DownloadProgress) {
        this.downloadIsLoading = true;
        this.downloadProgress = Math.round(
          (100 * response.loaded) / response.total,
        );
        this.cdRef.markForCheck();
      } else if (response.type === HttpEventType.Response) {
        const newBlob = new Blob([response.body], {
          type: 'application/vnd.ms-excel',
        });

        setTimeout(() => {
          this.closeModal('modal-download');
          this.downloadIsLoading = false;

          saveAs(newBlob, `log-aktivitas-${fileName}.xlsx`);

          Swal.fire({
            type: 'success',
            title: 'Download',
            text: 'Download log aktivitas berhasil.',
            allowOutsideClick: false,
            cancelButtonText: 'Tutup',
            confirmButtonColor: '#217EBA',
          });
        }, 1000);
      }
    });
  }

  openModal(id: string) {
    this.modalService.open(id);
  }

  closeModal(id: string) {
    this.modalService.close(id);
  }
}
