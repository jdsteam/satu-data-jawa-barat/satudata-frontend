import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { FormsModule } from '@angular/forms';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// COMPONENT
import { JdsBiNavModule } from '@jds-bi/core';

import { isEmpty, isNil } from 'lodash';
import moment from 'moment';
import { NgSelectModule } from '@ng-select/ng-select';

@Component({
  selector: 'app-tab-history',
  standalone: true,
  imports: [CommonModule, FormsModule, JdsBiNavModule, NgSelectModule],
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HistoryComponent implements OnInit {
  colorScheme = COLORSCHEME;
  moment: any = moment;
  jds: any = jds;

  @Output() filterOutputData = new EventEmitter<any>();
  @Output() filterOutputSort = new EventEmitter<any>();
  @Output() filterOutputStatus = new EventEmitter<any>();

  // Variable
  filter = {
    sort: 'newest',
    currentPage: 1,
    data: 'dataset',
    status: 'view',
  };

  sortOption: any[] = [
    { value: 'newest', label: 'Terbaru' },
    { value: 'oldest', label: 'Terlama' },
  ];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.queryParams();
  }

  queryParams(): void {
    this.route.queryParams.subscribe((p) => {
      const pSort = p.sort;
      const pData = p.data;
      const pStatus = p.status;

      this.filter.sort = !isNil(pSort) ? pSort : 'newest';
      this.filter.data = !isNil(pData) ? pData : 'dataset';
      this.filter.status = !isNil(pStatus) ? pStatus : 'view';
    });
  }

  filterData(event: string): void {
    const value = isEmpty(event) === false ? event : null;

    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        data: value,
        status: null,
        sort: null,
        currentpage: null,
        q: null,
      },
      queryParamsHandling: 'merge',
    });

    this.filter.data = value;
    this.filter.currentPage = 1;
    this.filterOutputData.emit(this.filter);
  }

  filterSort(event: any): void {
    const value = isEmpty(event) === false ? event.value : null;

    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        sort: value,
        currentpage: null,
      },
      queryParamsHandling: 'merge',
    });

    this.filter.sort = value;
    this.filter.currentPage = 1;
    this.filterOutputSort.emit(this.filter);
  }

  filterStatus(event: string): void {
    const value = isEmpty(event) === false ? event : null;

    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        status: value,
        currentpage: null,
      },
      queryParamsHandling: 'merge',
    });

    this.filter.status = value;
    this.filter.currentPage = 1;
    this.filterOutputStatus.emit(this.filter);
  }
}
