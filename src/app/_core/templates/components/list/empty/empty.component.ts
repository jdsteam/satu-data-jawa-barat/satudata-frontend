import {
  Component,
  OnInit,
  OnChanges,
  Input,
  SimpleChanges,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { BehaviorSubject } from 'rxjs';

// STYLE
import * as jds from '@styles';

// COMPONENT
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconDatabase,
  iconBuilding,
} from '@jds-bi/icons';

// PACKAGE
import { assign, isNil } from 'lodash';

import { EmptyStyle } from './empty.style';

@Component({
  selector: 'app-list-empty',
  standalone: true,
  imports: [CommonModule, JdsBiIconsModule],
  templateUrl: './empty.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [EmptyStyle],
})
export class EmptyComponent implements OnInit, OnChanges {
  jds: any = jds;

  className!: any;
  @Input() type!: string;
  @Input() isCard!: boolean;
  defaultInputs$ = new BehaviorSubject<any>({
    type: null,
    isCard: true,
  });

  constructor(
    private cdRef: ChangeDetectorRef,
    private iconService: JdsBiIconsService,
    private emptyStyle: EmptyStyle,
  ) {
    this.iconService.registerIcons([iconDatabase, iconBuilding]);
  }

  ngOnInit(): void {
    this.defaultInputs$.next({
      ...this.defaultInputs$.getValue(),
      ...this.checkInputs(),
    });
    this.className = this.emptyStyle.getDynamicStyle();
  }

  ngOnChanges(changes: SimpleChanges): void {
    const inputs = Object.keys(changes).reduce((result: any, item: any) => {
      // eslint-disable-next-line no-param-reassign
      result[item] = changes[item].currentValue;
      return result;
    }, {});

    this.defaultInputs$.next({ ...this.defaultInputs$.getValue(), ...inputs });
    this.className = this.emptyStyle.getDynamicStyle();
  }

  checkInputs(): any {
    const inputs = {};

    if (!isNil(this.type)) {
      assign(inputs, { type: this.type });
    }

    if (!isNil(this.isCard)) {
      assign(inputs, { isCard: this.isCard });
    }

    return inputs;
  }

  getIcon(type: string): string {
    if (
      type === 'dataset' ||
      type === 'indicator' ||
      type === 'visualization'
    ) {
      return 'database';
    }

    return 'building';
  }
}
