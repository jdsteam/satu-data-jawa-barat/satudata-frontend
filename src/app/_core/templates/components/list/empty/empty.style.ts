import { Injectable } from '@angular/core';

// PACKAGE
import { css } from '@emotion/css';

@Injectable()
export class EmptyStyle {
  getDynamicStyle(): any {
    const cardBody = css`
      padding: 50px 15px;
      text-align: center;
    `;

    const icon = css`
      margin-top: 20px;
      margin-bottom: 20px;
    `;

    return {
      cardBody,
      icon,
    };
  }
}
