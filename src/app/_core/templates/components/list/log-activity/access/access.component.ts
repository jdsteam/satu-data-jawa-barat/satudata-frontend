import {
  Component,
  OnChanges,
  Input,
  SimpleChanges,
  ChangeDetectionStrategy,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

// STYLE
import * as jds from '@styles';

// MODEL
import { LoginData } from '@models-v3';

// SERVICE
import { AuthenticationService } from '@services-v3';

// COMPONENT
import { JdsBiTableHeadModule, JdsBiPaginationModule } from '@jds-bi/core';

// STORE
import {
  selectAllLogDownloadDataset,
  selectIsLoadingList as selectDownloadDatasetIsLoadingList,
  selectPagination as selectDownloadDatasetPagination,
} from '@store-v3/log-download-dataset/log-download-dataset.selectors';
import { fromLogDownloadDatasetActions } from '@store-v3/log-download-dataset/log-download-dataset.actions';

import {
  selectAllLogDownloadIndicator,
  selectIsLoadingList as selectDownloadIndicatorIsLoadingList,
  selectPagination as selectDownloadIndicatorPagination,
} from '@store-v3/log-download-indicator/log-download-indicator.selectors';
import { fromLogDownloadIndicatorActions } from '@store-v3/log-download-indicator/log-download-indicator.actions';

import {
  selectAllLogDownloadVisualization,
  selectIsLoadingList as selectDownloadVisualizationIsLoadingList,
  selectPagination as selectDownloadVisualizationPagination,
} from '@store-v3/log-download-visualization/log-download-visualization.selectors';
import { fromLogDownloadVisualizationActions } from '@store-v3/log-download-visualization/log-download-visualization.actions';

import { assign, isEmpty } from 'lodash';
import { Store, select } from '@ngrx/store';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

@Component({
  selector: 'app-list-log-activity-access',
  standalone: true,
  imports: [
    CommonModule,
    NgxSkeletonLoaderModule,
    JdsBiTableHeadModule,
    JdsBiPaginationModule,
  ],
  templateUrl: './access.component.html',
  styleUrls: ['./access.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AccessComponent implements OnChanges {
  satudataUser: LoginData;
  jds: any = jds;

  @Input() filterInput: any;

  // Variable
  filter: any;
  filterHelper = {
    sort: 'date_start',
    directory: 'desc',
  };

  perPageItems = [
    { value: 10, label: '10' },
    { value: 25, label: '25' },
  ];

  // Data
  data$: Observable<any[]>;
  isLoadingList$: Observable<boolean>;
  isError$: Observable<boolean>;
  isError = false;
  errorMessage: string;
  pagination: any;
  pages: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private store: Store<any>,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
  }

  ngOnChanges(changes: SimpleChanges): void {
    // eslint-disable-next-line no-restricted-syntax
    for (const propName in changes) {
      // eslint-disable-next-line no-prototype-builtins
      if (changes.hasOwnProperty(propName)) {
        const chng = changes[propName];

        if (propName === 'filterInput') {
          this.filter = chng.currentValue;
        }
      }
    }
    this.getAllHistory();
  }

  paramSort(value: string) {
    const filtersSort = new Map().set(
      'date_start',
      `date_start:${this.filterHelper.directory}`,
    );

    return filtersSort.get(value);
  }

  getAllHistory(): void {
    const pSort = this.paramSort(this.filter.sort);

    const pCurrent =
      Number(this.filter.perPage) * Number(this.filter.currentPage) -
      Number(this.filter.perPage);

    const pWhere = {};

    const params = {
      search: this.filter.search || '',
      sort: pSort,
      limit: this.filter.perPage,
      skip: pCurrent,
      where: JSON.stringify(pWhere),
    };

    if (this.filter.dateStart) {
      if (!Array.isArray(this.filter.dateStart)) {
        assign(params, { start_date: this.filter.dateStart });
      }
    }

    if (this.filter.dateEnd) {
      if (!Array.isArray(this.filter.dateEnd)) {
        assign(params, { end_date: this.filter.dateEnd });
      }
    }

    switch (this.filter.data) {
      case 'dataset':
        this.getAllDataset(params);
        break;
      case 'indicator':
        this.getAllIndicator(params);
        break;
      case 'visualization':
        this.getAllVisualization(params);
        break;
      default:
        break;
    }
  }

  getAllDataset(params: any): void {
    this.store.dispatch(
      fromLogDownloadDatasetActions.loadAllLogDownloadDataset({
        params,
        pagination: true,
        infinite: false,
      }),
    );

    this.isLoadingList$ = this.store.pipe(
      select(selectDownloadDatasetIsLoadingList),
    );

    this.store
      .pipe(
        select(selectDownloadDatasetPagination),
        filter((val) => val !== null),
      )
      .subscribe((result) => {
        this.pagination = result;
        if (result.empty === false) {
          this.pages = result.pages;
        }
      });

    this.data$ = this.store.pipe(select(selectAllLogDownloadDataset));
  }

  getAllIndicator(params: any): void {
    this.store.dispatch(
      fromLogDownloadIndicatorActions.loadAllLogDownloadIndicator({
        params,
        pagination: true,
        infinite: false,
      }),
    );

    this.isLoadingList$ = this.store.pipe(
      select(selectDownloadIndicatorIsLoadingList),
    );

    this.store
      .pipe(
        select(selectDownloadIndicatorPagination),
        filter((val) => val !== null),
      )
      .subscribe((result) => {
        this.pagination = result;
        if (result.empty === false) {
          this.pages = result.pages;
        }
      });

    this.data$ = this.store.pipe(select(selectAllLogDownloadIndicator));
  }

  getAllVisualization(params: any): void {
    this.store.dispatch(
      fromLogDownloadVisualizationActions.loadAllLogDownloadVisualization({
        params,
        pagination: true,
        infinite: false,
      }),
    );

    this.isLoadingList$ = this.store.pipe(
      select(selectDownloadVisualizationIsLoadingList),
    );

    this.store
      .pipe(
        select(selectDownloadVisualizationPagination),
        filter((val) => val !== null),
      )
      .subscribe((result) => {
        this.pagination = result;
        if (result.empty === false) {
          this.pages = result.pages;
        }
      });

    this.data$ = this.store.pipe(select(selectAllLogDownloadVisualization));
  }

  filterSort(event: any) {
    const value = isEmpty(event) === false ? event : null;

    let dir = '';
    if (this.filterHelper.directory === 'desc') {
      dir = 'asc';
    } else if (this.filterHelper.directory === 'asc') {
      dir = 'desc';
    }

    this.filterHelper.sort = value;
    this.filterHelper.directory = dir;
    this.getAllHistory();
  }

  filterPerPage(perPage: number) {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        perpage: perPage,
        currentpage: null,
      },
      queryParamsHandling: 'merge',
    });

    this.filter.perPage = perPage;
    this.filter.currentPage = 1;
    this.getAllHistory();
  }

  filterPage(page: number): void {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        currentpage: page,
      },
      queryParamsHandling: 'merge',
    });

    this.filter.currentPage = page;
    this.getAllHistory();
  }
}
