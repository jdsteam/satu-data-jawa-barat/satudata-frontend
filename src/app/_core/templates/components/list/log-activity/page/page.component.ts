import {
  Component,
  OnChanges,
  Input,
  SimpleChanges,
  ChangeDetectionStrategy,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

// STYLE
import * as jds from '@styles';

// MODEL
import { LoginData, LogPageData } from '@models-v3';

// SERVICE
import { AuthenticationService } from '@services-v3';

// COMPONENT
import { JdsBiTableHeadModule, JdsBiPaginationModule } from '@jds-bi/core';

// STORE
import {
  selectAllLogPage,
  selectIsLoadingList,
  selectPagination,
} from '@store-v3/log-page/log-page.selectors';
import { fromLogPageActions } from '@store-v3/log-page/log-page.actions';

import { assign, isEmpty } from 'lodash';
import { Store, select } from '@ngrx/store';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

@Component({
  selector: 'app-list-log-activity-page',
  standalone: true,
  imports: [
    CommonModule,
    NgxSkeletonLoaderModule,
    JdsBiTableHeadModule,
    JdsBiPaginationModule,
  ],
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PageComponent implements OnChanges {
  satudataUser: LoginData;
  jds: any = jds;

  @Input() filterInput: any;

  // Variable
  filter: any;
  filterHelper = {
    sort: 'date_start',
    directory: 'desc',
  };

  perPageItems = [
    { value: 10, label: '10' },
    { value: 25, label: '25' },
  ];

  // Data
  data$: Observable<LogPageData[]>;
  isLoadingList$: Observable<boolean>;
  isError$: Observable<boolean>;
  isError = false;
  errorMessage: string;
  pagination: any;
  pages: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private store: Store<any>,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
  }

  ngOnChanges(changes: SimpleChanges): void {
    // eslint-disable-next-line no-restricted-syntax
    for (const propName in changes) {
      // eslint-disable-next-line no-prototype-builtins
      if (changes.hasOwnProperty(propName)) {
        const chng = changes[propName];

        if (propName === 'filterInput') {
          this.filter = chng.currentValue;
        }
      }
    }
    this.getAllHistoryPage();
  }

  paramSort(value: string) {
    const filtersSort = new Map().set(
      'date_start',
      `date_start:${this.filterHelper.directory}`,
    );

    return filtersSort.get(value);
  }

  getAllHistoryPage(): void {
    const pSort = this.paramSort(this.filter.sort);

    const pCurrent =
      Number(this.filter.perPage) * Number(this.filter.currentPage) -
      Number(this.filter.perPage);

    const pWhere = {};

    const params = {
      search: this.filter.search || '',
      sort: pSort,
      limit: this.filter.perPage,
      skip: pCurrent,
      where: JSON.stringify(pWhere),
    };

    if (this.filter.dateStart) {
      if (!Array.isArray(this.filter.dateStart)) {
        assign(params, { start_date: this.filter.dateStart });
      }
    }

    if (this.filter.dateEnd) {
      if (!Array.isArray(this.filter.dateEnd)) {
        assign(params, { end_date: this.filter.dateEnd });
      }
    }

    this.store.dispatch(
      fromLogPageActions.loadAllLogPage({
        params,
        pagination: true,
        infinite: false,
      }),
    );

    this.isLoadingList$ = this.store.pipe(select(selectIsLoadingList));

    this.store
      .pipe(
        select(selectPagination),
        filter((val) => val !== null),
      )
      .subscribe((result) => {
        this.pagination = result;
        if (result.empty === false) {
          this.pages = result.pages;
        }
      });

    this.data$ = this.store.pipe(select(selectAllLogPage));
  }

  filterSort(event: any) {
    const value = isEmpty(event) === false ? event : null;

    let dir = '';
    if (this.filterHelper.directory === 'desc') {
      dir = 'asc';
    } else if (this.filterHelper.directory === 'asc') {
      dir = 'desc';
    }

    this.filterHelper.sort = value;
    this.filterHelper.directory = dir;
    this.getAllHistoryPage();
  }

  filterPerPage(perPage: number) {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        perpage: perPage,
        currentpage: null,
      },
      queryParamsHandling: 'merge',
    });

    this.filter.perPage = perPage;
    this.filter.currentPage = 1;
    this.getAllHistoryPage();
  }

  filterPage(page: number): void {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        currentpage: page,
      },
      queryParamsHandling: 'merge',
    });

    this.filter.currentPage = page;
    this.getAllHistoryPage();
  }
}
