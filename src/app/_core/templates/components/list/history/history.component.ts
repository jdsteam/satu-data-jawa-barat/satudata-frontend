import {
  Component,
  OnChanges,
  Input,
  SimpleChanges,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

// STYLE
import * as jds from '@styles';
// MODEL
import { LoginData } from '@models-v3';
// PIPE
import { GroupByDatePipe } from '@pipes-v4';
// SERVICE
import { AuthenticationService } from '@services-v3';

// COMPONENT
import { EmptyComponent } from '@components-v3/list/empty/empty.component';

// STORE
import {
  selectAllLogDataset,
  selectIsLoadingList as selectDatasetIsLoadingList,
  selectError as selectDatasetError,
  selectPagination as selectDatasetPagination,
} from '@store-v3/log-dataset/log-dataset.selectors';
import { fromLogDatasetActions } from '@store-v3/log-dataset/log-dataset.actions';

import {
  selectAllLogDownloadDataset,
  selectIsLoadingList as selectDownloadDatasetIsLoadingList,
  selectError as selectDownloadDatasetError,
  selectPagination as selectDownloadDatasetPagination,
} from '@store-v3/log-download-dataset/log-download-dataset.selectors';
import { fromLogDownloadDatasetActions } from '@store-v3/log-download-dataset/log-download-dataset.actions';

import {
  selectAllLogIndicator,
  selectIsLoadingList as selectIndicatorIsLoadingList,
  selectError as selectIndicatorError,
  selectPagination as selectIndicatorPagination,
} from '@store-v3/log-indicator/log-indicator.selectors';
import { fromLogIndicatorActions } from '@store-v3/log-indicator/log-indicator.actions';

import {
  selectAllLogDownloadIndicator,
  selectIsLoadingList as selectDownloadIndicatorIsLoadingList,
  selectError as selectDownloadIndicatorError,
  selectPagination as selectDownloadIndicatorPagination,
} from '@store-v3/log-download-indicator/log-download-indicator.selectors';
import { fromLogDownloadIndicatorActions } from '@store-v3/log-download-indicator/log-download-indicator.actions';

import {
  selectAllLogVisualization,
  selectIsLoadingList as selectVisualizationIsLoadingList,
  selectError as selectVisualizationError,
  selectPagination as selectVisualizationPagination,
} from '@store-v3/log-visualization/log-visualization.selectors';
import { fromLogVisualizationActions } from '@store-v3/log-visualization/log-visualization.actions';

import {
  selectAllLogDownloadVisualization,
  selectIsLoadingList as selectDownloadVisualizationIsLoadingList,
  selectError as selectDownloadVisualizationError,
  selectPagination as selectDownloadVisualizationPagination,
} from '@store-v3/log-download-visualization/log-download-visualization.selectors';
import { fromLogDownloadVisualizationActions } from '@store-v3/log-download-visualization/log-download-visualization.actions';

import moment from 'moment';
import { Store, select } from '@ngrx/store';
import { ToastrService } from 'ngx-toastr';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

@Component({
  selector: 'app-list-history',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    GroupByDatePipe,
    NgxSkeletonLoaderModule,
    EmptyComponent,
  ],
  templateUrl: './history.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HistoryComponent implements OnChanges {
  satudataUser: LoginData;
  moment: any = moment;
  jds: any = jds;

  @Input() filterInput: any;

  // Variable
  filter: any;

  // Data
  data$: Observable<any[]>;
  isLoadingList$: Observable<boolean>;
  isError$: Observable<boolean>;
  isError = false;
  errorMessage: string;
  pagination: any;
  pages: any;

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private store: Store<any>,
    private toastr: ToastrService,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
  }

  ngOnChanges(changes: SimpleChanges): void {
    // eslint-disable-next-line no-restricted-syntax
    for (const propName in changes) {
      // eslint-disable-next-line no-prototype-builtins
      if (changes.hasOwnProperty(propName)) {
        const chng = changes[propName];

        if (propName === 'filterInput') {
          this.filter = chng.currentValue;
        }
      }
    }
    this.getAllHistory();
  }

  get checkFilter() {
    if (this.filter.search) {
      return true;
    }

    return false;
  }

  getAllHistory(): void {
    let pSort: string;
    if (this.filter.sort === 'newest') {
      pSort = `date_start:desc`;
    } else if (this.filter.sort === 'oldest') {
      pSort = `date_start:asc`;
    }

    const pCurrent =
      Number(this.filter.perPage) * Number(this.filter.currentPage) -
      Number(this.filter.perPage);

    const pWhere = {
      user_id: String(this.satudataUser.id),
    };

    const params = {
      search_type: 'history',
      search: this.filter.search || '',
      sort: pSort,
      limit: this.filter.perPage,
      skip: pCurrent,
      where: JSON.stringify(pWhere),
    };

    switch (this.filter.data) {
      case 'dataset':
        if (this.filter.status === 'view') {
          this.getAllDatasetView(params);
        } else if (this.filter.status === 'access') {
          this.getAllDatasetAccess(params);
        }
        break;
      case 'indicator':
        if (this.filter.status === 'view') {
          this.getAllIndicatorView(params);
        } else if (this.filter.status === 'access') {
          this.getAllIndicatorAccess(params);
        }
        break;
      case 'visualization':
        if (this.filter.status === 'view') {
          this.getAllVisualizationView(params);
        } else if (this.filter.status === 'access') {
          this.getAllVisualizationAccess(params);
        }
        break;
      default:
        break;
    }
  }

  getAllDatasetView(params: any): void {
    if (this.filter.isFirst) {
      this.store.dispatch(fromLogDatasetActions.clearLogDataset());
    }

    this.store.dispatch(
      fromLogDatasetActions.loadAllLogDataset({
        params,
        pagination: true,
        infinite: true,
      }),
    );

    this.isLoadingList$ = this.store.pipe(select(selectDatasetIsLoadingList));

    this.store
      .pipe(
        select(selectDatasetError),
        filter((val) => val !== null && val.error),
      )
      .subscribe((result) => {
        if (!result.error) {
          this.isError = false;
        } else {
          this.isError = true;
          this.toastr.error(result.message, 'History');
        }
      });

    this.store
      .pipe(
        select(selectDatasetPagination),
        filter((val) => val !== null),
      )
      .subscribe((result) => {
        this.pagination = result;
      });

    this.data$ = this.store.pipe(select(selectAllLogDataset));
  }

  getAllDatasetAccess(params: any): void {
    if (this.filter.isFirst) {
      this.store.dispatch(
        fromLogDownloadDatasetActions.clearLogDownloadDataset(),
      );
    }

    this.store.dispatch(
      fromLogDownloadDatasetActions.loadAllLogDownloadDataset({
        params,
        pagination: true,
        infinite: true,
      }),
    );

    this.isLoadingList$ = this.store.pipe(
      select(selectDownloadDatasetIsLoadingList),
    );

    this.store
      .pipe(
        select(selectDownloadDatasetError),
        filter((val) => val !== null && val.error),
      )
      .subscribe((result) => {
        if (!result.error) {
          this.isError = false;
        } else {
          this.isError = true;
          this.toastr.error(result.message, 'History');
        }
      });

    this.store
      .pipe(
        select(selectDownloadDatasetPagination),
        filter((val) => val !== null),
      )
      .subscribe((result) => {
        this.pagination = result;
      });

    this.data$ = this.store.pipe(select(selectAllLogDownloadDataset));
  }

  getAllIndicatorView(params: any): void {
    if (this.filter.isFirst) {
      this.store.dispatch(fromLogIndicatorActions.clearLogIndicator());
    }

    this.store.dispatch(
      fromLogIndicatorActions.loadAllLogIndicator({
        params,
        pagination: true,
        infinite: true,
      }),
    );

    this.isLoadingList$ = this.store.pipe(select(selectIndicatorIsLoadingList));

    this.store
      .pipe(
        select(selectIndicatorError),
        filter((val) => val !== null && val.error),
      )
      .subscribe((result) => {
        if (!result.error) {
          this.isError = false;
        } else {
          this.isError = true;
          this.toastr.error(result.message, 'History');
        }
      });

    this.store
      .pipe(
        select(selectIndicatorPagination),
        filter((val) => val !== null),
      )
      .subscribe((result) => {
        this.pagination = result;
      });

    this.data$ = this.store.pipe(select(selectAllLogIndicator));
  }

  getAllIndicatorAccess(params: any): void {
    if (this.filter.isFirst) {
      this.store.dispatch(
        fromLogDownloadIndicatorActions.clearLogDownloadIndicator(),
      );
    }

    this.store.dispatch(
      fromLogDownloadIndicatorActions.loadAllLogDownloadIndicator({
        params,
        pagination: true,
        infinite: true,
      }),
    );

    this.isLoadingList$ = this.store.pipe(
      select(selectDownloadIndicatorIsLoadingList),
    );

    this.store
      .pipe(
        select(selectDownloadIndicatorError),
        filter((val) => val !== null && val.error),
      )
      .subscribe((result) => {
        if (!result.error) {
          this.isError = false;
        } else {
          this.isError = true;
          this.toastr.error(result.message, 'History');
        }
      });

    this.store
      .pipe(
        select(selectDownloadIndicatorPagination),
        filter((val) => val !== null),
      )
      .subscribe((result) => {
        this.pagination = result;
      });

    this.data$ = this.store.pipe(select(selectAllLogDownloadIndicator));
  }

  getAllVisualizationView(params: any): void {
    if (this.filter.isFirst) {
      this.store.dispatch(fromLogVisualizationActions.clearLogVisualization());
    }

    this.store.dispatch(
      fromLogVisualizationActions.loadAllLogVisualization({
        params,
        pagination: true,
        infinite: true,
      }),
    );

    this.isLoadingList$ = this.store.pipe(
      select(selectVisualizationIsLoadingList),
    );

    this.store
      .pipe(
        select(selectVisualizationError),
        filter((val) => val !== null && val.error),
      )
      .subscribe((result) => {
        if (!result.error) {
          this.isError = false;
        } else {
          this.isError = true;
          this.toastr.error(result.message, 'History');
        }
      });

    this.store
      .pipe(
        select(selectVisualizationPagination),
        filter((val) => val !== null),
      )
      .subscribe((result) => {
        this.pagination = result;
      });

    this.data$ = this.store.pipe(select(selectAllLogVisualization));
  }

  getAllVisualizationAccess(params: any): void {
    if (this.filter.isFirst) {
      this.store.dispatch(
        fromLogDownloadVisualizationActions.clearLogDownloadVisualization(),
      );
    }

    this.store.dispatch(
      fromLogDownloadVisualizationActions.loadAllLogDownloadVisualization({
        params,
        pagination: true,
        infinite: true,
      }),
    );

    this.isLoadingList$ = this.store.pipe(
      select(selectDownloadVisualizationIsLoadingList),
    );

    this.store
      .pipe(
        select(selectDownloadVisualizationError),
        filter((val) => val !== null && val.error),
      )
      .subscribe((result) => {
        if (!result.error) {
          this.isError = false;
        } else {
          this.isError = true;
          this.toastr.error(result.message, 'History');
        }
      });

    this.store
      .pipe(
        select(selectDownloadVisualizationPagination),
        filter((val) => val !== null),
      )
      .subscribe((result) => {
        this.pagination = result;
      });

    this.data$ = this.store.pipe(select(selectAllLogDownloadVisualization));
  }

  filterPage(value: number): void {
    this.filter.isFirst = false;
    this.isError = false;
    this.filter.currentPage = 1 + value;
    this.getAllHistory();
  }
}
