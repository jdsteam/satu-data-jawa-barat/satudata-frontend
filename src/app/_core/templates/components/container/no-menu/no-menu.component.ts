import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// COMPONENT
import {
  FooterComponent,
  FloatingFeedbackComponent,
  FloatingToTopComponent,
} from '@components-v4';
import { NoMenuComponent as PrivateNavbarNoMenuComponent } from '@components-v3/navbar/no-menu/no-menu.component';

@Component({
  selector: 'app-container-no-menu',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    PrivateNavbarNoMenuComponent,
    FooterComponent,
    FloatingFeedbackComponent,
    FloatingToTopComponent,
  ],
  template: `
    <jds-navbar-no-menu />
    <router-outlet />
    <app-footer />
    <app-floating-feedback />
    <app-floating-to-top />
  `,
})
export class NoMenuComponent {}
