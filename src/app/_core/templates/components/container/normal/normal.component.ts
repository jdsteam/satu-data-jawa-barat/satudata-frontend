import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// COMPONENT
import {
  NavbarDefaultComponent,
  FooterComponent,
  FloatingFeedbackComponent,
  FloatingToTopComponent,
} from '@components-v4';

@Component({
  selector: 'app-container-normal',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    NavbarDefaultComponent,
    FooterComponent,
    FloatingFeedbackComponent,
    FloatingToTopComponent,
  ],
  template: `
    <app-navbar-default />
    <router-outlet />
    <app-footer />
    <app-floating-feedback />
    <app-floating-to-top />
  `,
})
export class NormalComponent {}
