import {
  Component,
  OnChanges,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
} from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-star-rating',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StarRatingComponent implements OnChanges {
  @Input() rating: number;
  @Input() margin: number;
  @Input() width: number;
  @Input() readonly = true;
  @Output() rate: EventEmitter<number> = new EventEmitter<number>();

  iconClass = {
    0: 'far fa-star',
    0.5: 'fas fa-star-half-alt',
    1: 'fas fa-star',
  };

  iconSrc = {
    0: 'assets/images/star-empty.svg',
    0.5: 'assets/images/star-half.svg',
    1: 'assets/images/star-full.svg',
  };

  iconClassImage = {
    0: 'rating-empty',
    0.5: 'rating-half',
    1: 'rating-full',
  };

  stars: number[] = [0, 0, 0, 0, 0];

  ngOnChanges() {
    this.fillStars();
  }

  fillStars() {
    this.stars = [0, 0, 0, 0, 0];
    let starsToFill = Math.round(this.rating * 2) / 2;
    let i = 0;
    while (starsToFill > 0.5) {
      this.stars[i] = 1;
      i += 1;
      starsToFill -= 1;
    }
    if (starsToFill === 0.5) {
      this.stars[i] = 0.5;
    }
  }

  onClick(event: any) {
    const rate = Number(event.target.getAttribute('data-value')) + 1;
    this.rating = rate;
    this.rate.emit(rate);
  }
}
