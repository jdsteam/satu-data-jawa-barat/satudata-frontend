import { Injectable } from '@angular/core';

// PACKAGE
import { css } from '@emotion/css';

import { IStyleNormalV2 } from './normal-v2.interface';

@Injectable()
export class NormalV2Style {
  getDynamicStyle(): IStyleNormalV2 {
    const navbar = css`
      background-color: #fff;
      border-bottom: 1px solid #e7e8e9;
    `;

    const content = css`
      height: 80px;
      position: relative;
      display: flex;
      flex-wrap: wrap;
      align-items: center;
      justify-content: space-between;
    `;

    const dropdownMenuMobile = css`
      background-color: #fff;
      flex-basis: 100%;
      flex-grow: 1;
      align-items: center;

      overflow: hidden;
      transition: height 0.35s ease;
    `;

    return {
      navbar,
      content,
      dropdownMenuMobile,
    };
  }
}
