export interface IStyleNormalV2 {
  navbar: string;
  content: string;
  dropdownMenuMobile: string;
}
