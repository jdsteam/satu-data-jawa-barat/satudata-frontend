import { ChangeDetectionStrategy, Component } from '@angular/core';
import { CommonModule, NgOptimizedImage } from '@angular/common';
import { RouterModule } from '@angular/router';

// STYLE
import * as jds from '@styles';
// COMPONENT
import {
  NavbarNotificationComponent,
  NavbarAddCatalogComponent,
  NavbarProfileComponent,
} from '@components-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';

import { IStyleNormalV2 } from './normal-v2.interface';
import { NormalV2Style } from './normal-v2.style';

@Component({
  selector: 'jds-navbar-normal-v2',
  standalone: true,
  imports: [
    CommonModule,
    NgOptimizedImage,
    RouterModule,
    NavbarNotificationComponent,
    NavbarAddCatalogComponent,
    NavbarProfileComponent,
  ],
  templateUrl: './normal-v2.component.html',
  providers: [NormalV2Style],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NormalV2Component {
  satudataUser: LoginData;
  jds = jds;

  className!: IStyleNormalV2;

  // Variable
  openMobile = false;

  links = [
    { label: 'Beranda', url: '/home' },
    { label: 'Dataset', url: '/dataset' },
    { label: 'Mapset', url: '/mapset' },
    { label: 'Indikator', url: '/indicator' },
    { label: 'Visualisasi', url: '/visualization' },
    { label: 'Organisasi', url: '/organization' },
  ];

  constructor(
    private authenticationService: AuthenticationService,
    private normalV2Style: NormalV2Style,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    this.className = this.normalV2Style.getDynamicStyle();
  }
}
