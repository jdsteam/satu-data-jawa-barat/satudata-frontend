import { ChangeDetectionStrategy, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// STYLE
import * as jds from '@styles';

// MODEL
import { LoginData } from '@models-v3';

// SERVICE
import { AuthenticationService } from '@services-v3';

import { IStyleNormalV2 } from '../normal-v2/normal-v2.interface';
import { NormalV2Style } from '../normal-v2/normal-v2.style';

@Component({
  selector: 'jds-navbar-no-menu',
  standalone: true,
  imports: [CommonModule, RouterModule],
  templateUrl: './no-menu.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [NormalV2Style],
})
export class NoMenuComponent {
  satudataUser: LoginData;
  jds = jds;

  className!: IStyleNormalV2;

  // Variable
  openMobile = false;

  constructor(
    private authenticationService: AuthenticationService,
    private normalV2Style: NormalV2Style,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    this.className = this.normalV2Style.getDynamicStyle();
  }
}
