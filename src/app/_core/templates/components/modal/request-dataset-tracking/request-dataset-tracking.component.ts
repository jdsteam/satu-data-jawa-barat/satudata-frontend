import {
  Component,
  OnInit,
  Input,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

// STYLE
import * as jds from '@styles';
import { TrackingDatasetStyle } from '@components-v3/modal/tracking-dataset/tracking-dataset.style';

// MODEL
import {
  LoginData,
  HistoryRequestPrivateData,
  NotificationData,
} from '@models-v3';

// SERVICE
import {
  AuthenticationService,
  HistoryRequestPrivateService,
  NotificationService,
} from '@services-v3';

// COMPONENT
import { ModalModule, ModalService } from '@components-v3/modal';
import { JdsBiIconsModule, JdsBiIconsService, iconXmark } from '@jds-bi/icons';

// STORE
import {
  selectAllHistoryRequestPrivate,
  selectIsLoadingList,
} from '@store-v3/history-request-private/history-request-private.selectors';
import { fromHistoryRequestPrivateActions } from '@store-v3/history-request-private/history-request-private.actions';

import moment from 'moment';
import { Store, select } from '@ngrx/store';
import { LetDirective } from '@ngrx/component';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

@Component({
  selector: 'app-modal-request-dataset-tracking',
  standalone: true,
  imports: [
    CommonModule,
    JdsBiIconsModule,
    ModalModule,
    LetDirective,
    NgxSkeletonLoaderModule,
  ],
  templateUrl: './request-dataset-tracking.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [TrackingDatasetStyle],
})
export class RequestDatasetTrackingComponent implements OnInit {
  satudataUser: LoginData;
  moment: any = moment;
  jds: any = jds;

  className!: any;
  @Input() from: string;

  // Variable
  id: number;
  idModel: string;

  // Data
  data$: Observable<HistoryRequestPrivateData[]>;
  isLoadingList$: Observable<boolean>;
  isError$: Observable<boolean>;
  isError = false;
  errorMessage: string;

  notificationData: NotificationData[];
  notificationIsLoadingList: boolean;

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private notificationService: NotificationService,
    private historyRequestPrivateService: HistoryRequestPrivateService,
    private modalService: ModalService,
    private iconService: JdsBiIconsService,
    private store: Store<any>,
    private trackingDatasetStyle: TrackingDatasetStyle,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    this.iconService.registerIcons([iconXmark]);
  }

  ngOnInit(): void {
    this.className = this.trackingDatasetStyle.getDynamicStyle();
    this.idModel = `modal-request-dataset-tracking${this.from}`;
  }

  openModal() {
    this.modalService.open(this.idModel);
  }

  closeModal() {
    this.modalService.close(this.idModel);
  }

  getTracking(id: number) {
    this.id = id;
    this.openModal();
    this.getAllHistoryRequest();
    this.getNotification();
  }

  getAllHistoryRequest(): void {
    const pWhere = {
      request_private_id: this.id,
    };

    const params = {
      sort: 'datetime:desc',
      where: JSON.stringify(pWhere),
    };

    this.store.dispatch(
      fromHistoryRequestPrivateActions.loadAllHistoryRequestPrivate({
        params,
        pagination: false,
        infinite: false,
      }),
    );

    this.isLoadingList$ = this.store.pipe(select(selectIsLoadingList));

    this.data$ = this.store.pipe(select(selectAllHistoryRequestPrivate));

    this.cdRef.detectChanges();
  }

  getNotification(): void {
    const pWhere = {
      receiver: this.satudataUser.id,
      type: 'request_private',
      type_id: this.id,
    };

    const params = {
      sort: 'cdate:desc',
      limit: 1,
      where: JSON.stringify(pWhere),
    };

    this.notificationIsLoadingList = true;
    this.notificationService.getList(params).subscribe((res) => {
      // eslint-disable-next-line prefer-destructuring
      this.notificationData = res.data[0];
      this.notificationIsLoadingList = false;
      this.cdRef.detectChanges();
    });

    this.cdRef.detectChanges();
  }

  getCategory(status: number) {
    return this.historyRequestPrivateService.getStatus(status);
  }

  getNotificationContent(title: string, content: string) {
    return this.notificationService.getRequestDataset(title, content);
  }
}
