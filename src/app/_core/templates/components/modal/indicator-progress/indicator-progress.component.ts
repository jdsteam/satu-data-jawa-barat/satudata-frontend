import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  FormsModule,
  ReactiveFormsModule,
  UntypedFormGroup,
  UntypedFormControl,
  Validators,
  AbstractControl,
} from '@angular/forms';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
import { IndicatorProgressData } from '@models-v3';

import { ModalModule, ModalService } from '@components-v3/modal';
import { JdsBiInputModule } from '@jds-bi/core';
import { JdsBiIconsModule, JdsBiIconsService, iconXmark } from '@jds-bi/icons';

import { selectIsLoadingUpdate as selectIsLoadingUpdateIndicatorProgressFinal } from '@store-v3/indicator-progress-final/indicator-progress-final.selectors';
import { fromIndicatorProgressFinalActions } from '@store-v3/indicator-progress-final/indicator-progress-final.actions';

import { NgxLoadingModule } from 'ngx-loading';

function ZeroStripValidator(
  control: AbstractControl,
): { [key: string]: any } | null {
  if (control.value && (control.value === '0' || control.value === '-')) {
    return { zeroStrip: true };
  }
  return null;
}

@Component({
  selector: 'app-modal-indicator-progress',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    JdsBiInputModule,
    JdsBiIconsModule,
    ModalModule,
    NgxLoadingModule,
  ],
  templateUrl: './indicator-progress.component.html',
})
export class IndicatorProgressComponent {
  colorScheme = COLORSCHEME;
  jds: any = jds;

  title: string;

  myForm = new UntypedFormGroup({
    id: new UntypedFormControl({ value: null }, [Validators.required]),
    indikator_id: new UntypedFormControl({ value: null }, [
      Validators.required,
    ]),
    year: new UntypedFormControl({ value: null }, [Validators.required]),
    target: new UntypedFormControl({ value: null }, [Validators.required]),
    capaian: new UntypedFormControl('', [
      Validators.required,
      ZeroStripValidator,
    ]),
  });

  indicatorProgressFinalIsLoadingUpdate$: Observable<boolean>;

  constructor(
    private store: Store<any>,
    private iconService: JdsBiIconsService,
    private modalService: ModalService,
  ) {
    this.iconService.registerIcons([iconXmark]);
  }

  // ngOnInit() {}

  // get data
  get f() {
    return this.myForm.controls;
  }

  public showModal(type: string, indicatorProgress: IndicatorProgressData) {
    if (type === 'add') {
      this.title = 'Tambah Capaian';
    } else if (type === 'edit') {
      this.title = 'Ubah Capaian';

      // set value achievement
      this.f.capaian.setValue(indicatorProgress.capaian);
    }

    // set params to form
    this.f.id.setValue(indicatorProgress.id);
    this.f.indikator_id.setValue(indicatorProgress.indikator_id);
    this.f.year.setValue(indicatorProgress.year);
    this.f.target.setValue(indicatorProgress.target);

    this.openModal('modal-indicator-progress');
  }

  onSubmit() {
    const bodyProgress = this.myForm.value;

    const pWhere = {
      id: this.f.id.value,
      indikator_id: this.f.indikator_id.value,
    };

    const params = {
      where: JSON.stringify(pWhere),
    };

    this.store.dispatch(
      fromIndicatorProgressFinalActions.updateIndicatorProgressFinal({
        indicatorId: this.f.indikator_id.value,
        update: bodyProgress,
        params,
      }),
    );

    this.indicatorProgressFinalIsLoadingUpdate$ = this.store.pipe(
      select(selectIsLoadingUpdateIndicatorProgressFinal),
    );

    this.myForm.reset();
    this.closeModal('modal-indicator-progress');
  }

  openModal(id: string) {
    this.modalService.open(id);
  }

  closeModal(id: string) {
    this.modalService.close(id);
  }
}
