import {
  Component,
  OnInit,
  HostListener,
  ViewChild,
  ElementRef,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import {
  FormsModule,
  ReactiveFormsModule,
  UntypedFormGroup,
  UntypedFormControl,
  Validators,
} from '@angular/forms';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// MODEL
import { LoginData, AgreementData } from '@models-v3';
// PIPE
import { SafeHtmlPipe } from '@pipes-v4';
// SERVICE
import {
  AuthenticationService,
  StateService,
  AgreementService,
  UserService,
} from '@services-v3';

// COMPONENT
import {
  JdsBiModalModule,
  JdsBiModalService,
  JdsBiButtonModule,
} from '@jds-bi/core';

import { assign } from 'lodash';
import { ToastrService } from 'ngx-toastr';
import { NgxLoadingModule } from 'ngx-loading';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

@Component({
  selector: 'app-modal-agreement',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    JdsBiModalModule,
    JdsBiButtonModule,
    NgxLoadingModule,
    NgxSkeletonLoaderModule,
    SafeHtmlPipe,
  ],
  templateUrl: './agreement.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AgreementComponent implements OnInit {
  colorScheme = COLORSCHEME;
  jds: any = jds;

  @ViewChild('agreementContent', { static: false })
  agreementContent: ElementRef;

  // Variable
  returnUrl: string;
  isLoading = false;

  // Data
  userData: LoginData;

  agreementData: AgreementData;
  agreementIsLoading = false;
  agreementMaxHeight = 300;

  // Form
  myForm: UntypedFormGroup;

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private modalService: JdsBiModalService,
    private userService: UserService,
    private authenticationService: AuthenticationService,
    private agreementService: AgreementService,
    private stateService: StateService,
    private toastr: ToastrService,
  ) {
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/home';
  }

  @HostListener('scroll', ['$event'])
  onScroll(event: any) {
    if (
      event.target.offsetHeight + event.target.scrollTop >=
      event.target.scrollHeight - 1
    ) {
      this.f.checklist.enable();
    }
  }

  ngOnInit() {
    this.myForm = new UntypedFormGroup({
      checklist: new UntypedFormControl({ value: false, disabled: true }, [
        Validators.pattern('true'),
      ]),
    });
  }

  get f() {
    return this.myForm.controls;
  }

  getAgreement() {
    const pWhere = {};

    assign(pWhere, { is_active: true });

    const params = {
      where: JSON.stringify(pWhere),
    };

    this.agreementIsLoading = true;
    this.agreementService.getList(params).subscribe((result) => {
      this.agreementIsLoading = false;
      // eslint-disable-next-line prefer-destructuring
      this.agreementData = result.data[0];

      this.cdRef.detectChanges();
      if (
        this.agreementContent.nativeElement.offsetHeight <=
        this.agreementMaxHeight
      ) {
        this.f.checklist.enable();
      }
    });
  }

  getSession() {
    this.stateService.currentAuthenticationLogin.subscribe((result: any) => {
      this.userData = result;
    });
  }

  openModal() {
    this.modalService.open('modal-agreement');

    this.getAgreement();
    this.getSession();
  }

  closeModal() {
    this.modalService.close('modal-agreement');
  }

  onSubmit() {
    const { checklist } = this.myForm.value;

    if (!checklist) {
      return;
    }

    const body = {
      agreement_id: this.agreementData.id,
    };

    this.isLoading = true;
    this.userService
      .updateAgreement(this.userData.id, this.userData.jwt, body)
      .subscribe(
        (result) => {
          if (result.error === 0) {
            this.isLoading = false;

            this.authenticationService.setSession(this.userData);
            this.router.navigate([this.returnUrl]);
          } else {
            this.toastr.error(result.message, 'Agreement');
          }
        },
        (error) => {
          this.toastr.error(error, 'Agreement');
        },
      );
  }
}
