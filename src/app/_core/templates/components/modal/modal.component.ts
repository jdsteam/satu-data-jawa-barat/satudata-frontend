import {
  Component,
  OnInit,
  OnChanges,
  OnDestroy,
  Input,
  SimpleChanges,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  ViewEncapsulation,
  ElementRef,
  HostBinding,
  ViewChild,
  AfterViewInit,
} from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';

import * as _ from 'lodash';
import moment from 'moment';

// STYLE
import { ModalStyle } from './modal.style';

// SERVICE
import { ModalService } from './modal.service';

@Component({
  selector: 'app-modal',
  templateUrl: 'modal.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [ModalStyle],
})
export class ModalComponent
  implements OnInit, OnChanges, OnDestroy, AfterViewInit
{
  moment = moment;

  @HostBinding('style') style: SafeStyle;

  @ViewChild('modalHeader') modalHeader;
  @ViewChild('modalFooter') modalFooter;
  showHeader = false;
  showFooter = false;

  className!: any;
  @Input() id!: string;
  @Input() size!: string;
  @Input() position!: string;
  @Input() isClose = true;
  defaultInputs$ = new BehaviorSubject<any>({
    id: null,
    size: 'default',
    position: 'start',
    isClose: true,
  });

  private element: any;

  constructor(
    private cdRef: ChangeDetectorRef,
    private modalService: ModalService,
    private el: ElementRef,
    private modalStyle: ModalStyle,
    sanitizer: DomSanitizer,
  ) {
    this.element = el.nativeElement;
    this.style = sanitizer.bypassSecurityTrustStyle('display: none;');
  }

  ngOnInit(): void {
    this.defaultInputs$.next({
      ...this.defaultInputs$.getValue(),
      ...this.checkInputs(),
    });
    this.className = this.modalStyle.getDynamicStyle(
      this.defaultInputs$.getValue(),
    );

    if (!this.id) {
      console.error('modal must have an id');
      return;
    }

    document.body.appendChild(this.element);

    this.element.addEventListener('click', (el) => {
      if (this.isClose && el.target.className === this.className.modal) {
        this.close();
      }
    });

    this.modalService.add(this);
  }

  ngOnChanges(changes: SimpleChanges): void {
    const inputs = Object.keys(changes).reduce((result: any, item: any) => {
      // eslint-disable-next-line no-param-reassign
      result[item] = changes[item].currentValue;
      return result;
    }, {});

    this.defaultInputs$.next({ ...this.defaultInputs$.getValue(), ...inputs });
    this.className = this.modalStyle.getDynamicStyle(
      this.defaultInputs$.getValue(),
    );
  }

  ngOnDestroy(): void {
    this.modalService.remove(this.id);
    this.element.remove();
  }

  ngAfterViewInit() {
    this.showHeader =
      this.modalHeader.nativeElement &&
      this.modalHeader.nativeElement.children.length > 0;
    this.showFooter =
      this.modalFooter.nativeElement &&
      this.modalFooter.nativeElement.children.length > 0;

    const show = {
      showHeader: this.showHeader,
      showFooter: this.showFooter,
    };

    this.defaultInputs$.next({ ...this.defaultInputs$.getValue(), ...show });
    this.className = this.modalStyle.getDynamicStyle(
      this.defaultInputs$.getValue(),
    );

    this.cdRef.detectChanges();
  }

  checkInputs(): any {
    const inputs = {};

    if (!_.isNil(this.id)) {
      _.assign(inputs, { id: this.id });
    }

    if (!_.isNil(this.size)) {
      _.assign(inputs, { size: this.size });
    }

    if (!_.isNil(this.position)) {
      _.assign(inputs, { position: this.position });
    }

    if (!_.isNil(this.isClose)) {
      _.assign(inputs, { isClose: this.isClose });
    }

    return inputs;
  }

  open(): void {
    this.element.style.display = 'block';
    document.body.classList.add('app-modal-open');
  }

  close(): void {
    this.element.style.display = 'none';
    document.body.classList.remove('app-modal-open');
  }
}
