import { Injectable } from '@angular/core';

// PACKAGE
import { css } from '@emotion/css';

@Injectable()
export class ModalStyle {
  getDynamicStyle(inputs: any): any {
    const modal = css`
      position: fixed;
      top: 0;
      left: 0;
      z-index: 1000;
      width: 100%;
      height: 100%;
      overflow-x: hidden;
      overflow-y: auto;
      outline: 0;
    `;

    const dialog = css`
      position: relative;
      width: auto;
      margin: 5px;
      pointer-events: none;

      ${inputs.position === 'center' &&
      css`
        display: flex;
        align-items: center;
      `}

      @media (min-width: 576px) {
        margin: 28px auto;

        ${inputs.size === 'small' &&
        css`
          max-width: 300px;
        `}

        ${inputs.size === 'default' &&
        css`
          max-width: 510px;
        `}

        ${inputs.size === 'large' &&
        css`
          max-width: 766px;
        `}

        ${inputs.size === 'extra-large' &&
        css`
          max-width: 900px;
        `}

        ${inputs.size === 'extra-extra-large' &&
        css`
          max-width: 1140px;
        `}

        ${inputs.position === 'center' &&
        css`
          min-height: calc(100% - 3.5rem);
        `}
      }
    `;

    const content = css`
      position: relative;
      display: flex;
      flex-direction: column;
      width: 100%;
      pointer-events: auto;
      background-color: #fff;
      background-clip: padding-box;
      border-radius: 8px;
      outline: 0;
    `;

    const header = css`
      flex-shrink: 0;
      align-items: center;
      justify-content: space-between;
      padding: 8px 24px;
      border-top-right-radius: 8px;
      border-top-left-radius: 8px;

      ${inputs.showHeader &&
      css`
        display: flex;
      `}

      ${!inputs.showHeader &&
      css`
        display: none;
      `}

      h6 {
        // font-weight: 500 !important;
        // font-size: 21px !important;
        // line-height: 34px !important;
        margin-bottom: 0;
      }

      .btn-close {
        padding: 5px;
        display: flex;
        background: transparent;
        box-sizing: content-box;
        color: #000;
        border: 0;
        border-radius: 2.5px;
      }
    `;

    const body = css`
      position: relative;
      flex: 1 1 auto;
      padding: 12px 24px;
    `;

    const footer = css`
      flex-shrink: 0;
      align-items: center;
      justify-content: flex-end;
      padding: 16px 24px;
      background-color: #fafafa;
      border-bottom-right-radius: 8px;
      border-bottom-left-radius: 8px;

      ${inputs.showFooter &&
      css`
        display: flex;
      `}

      ${!inputs.showFooter &&
      css`
        display: none;
      `}
    `;

    const background = css`
      position: fixed;
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      background-color: #000;
      opacity: 0.5;
      z-index: 900;
    `;

    return {
      modal,
      dialog,
      content,
      header,
      body,
      footer,
      background,
    };
  }
}
