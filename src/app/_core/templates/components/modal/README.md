```
<button (click)="openModal('custom-modal-1')">Open Modal 1</button>
<app-modal #modal
           id="custom-modal-1"
           size="default"
           position="center"
           [isClose]="false">
  <ng-container ngProjectAs="header">
    <h5>Header</h5>
    <button type="button"
            class="btn-close"
            (click)="closeModal('custom-modal-1');">
      <i class="fas fa-times"></i>
    </button>
  </ng-container>
  <ng-container ngProjectAs="body">
    Body
  </ng-container>
  <ng-container ngProjectAs="footer">
    Footer
  </ng-container>
</app-modal>
```
