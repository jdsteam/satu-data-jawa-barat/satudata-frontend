import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  FormsModule,
  ReactiveFormsModule,
  UntypedFormGroup,
  UntypedFormControl,
  Validators,
} from '@angular/forms';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// SERVICES
import { AuthenticationService } from '@services-v3';

// COMPONENT
// import { ModalModule, ModalService } from '@components-v3/modal';
import {
  JdsBiModalModule,
  JdsBiModalService,
  JdsBiInputModule,
  JdsBiButtonModule,
} from '@jds-bi/core';
import { JdsBiIconsModule, JdsBiIconsService, iconXmark } from '@jds-bi/icons';

// PLUGINS
import Swal from 'sweetalert2';
import { LetDirective } from '@ngrx/component';
import { NgxLoadingModule } from 'ngx-loading';

@Component({
  selector: 'app-modal-forgot-password-v2',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    JdsBiModalModule,
    JdsBiInputModule,
    JdsBiButtonModule,
    JdsBiIconsModule,
    LetDirective,
    NgxLoadingModule,
  ],
  templateUrl: './forgot-password.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ForgotPasswordComponent implements OnInit {
  colorScheme = COLORSCHEME;
  jds: any = jds;

  // Variable
  isLoading = false;
  error: number;
  message: string;

  // Form
  myForm: UntypedFormGroup;

  constructor(
    private authenticationService: AuthenticationService,
    private modalService: JdsBiModalService,
    private iconService: JdsBiIconsService,
  ) {
    this.iconService.registerIcons([iconXmark]);
  }

  ngOnInit() {
    this.myForm = new UntypedFormGroup({
      username: new UntypedFormControl('', [Validators.required]),
      email: new UntypedFormControl('', [
        Validators.required,
        Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$'),
      ]),
    });
  }

  get f() {
    return this.myForm.controls;
  }

  openModal() {
    this.modalService.open('modal-forgot-password');
  }

  closeModal() {
    this.modalService.close('modal-forgot-password');
  }

  onSubmit() {
    this.closeModal();
    this.isLoading = true;
    this.error = 0;
    this.message = '';

    this.authenticationService
      .forgotPassword(this.myForm.value)
      .subscribe((result) => {
        if (result.error === 1) {
          this.isLoading = false;
          this.error = result.error;
          this.message = result.message;
        } else if (result.error === 0) {
          this.myForm.reset();

          this.isLoading = false;
          const typedata = 'success';
          const message = 'Reset Password Berhasil. Silahkan Cek Email Anda';

          Swal.fire({
            type: typedata,
            text: message,
            allowOutsideClick: false,
            cancelButtonText: 'Tutup',
            confirmButtonColor: '#217EBA',
          });
        }
      });
  }
}
