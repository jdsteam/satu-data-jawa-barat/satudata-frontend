import { Component } from '@angular/core';

// STYLE
import * as jds from '@styles';

// MODEL
import { JdsBiModal } from '@jds-bi/core';

@Component({
  selector: 'app-modal-dataset-leave',
  templateUrl: './dataset-leave.component.html',
})
export class DatasetLeaveComponent extends JdsBiModal {
  jds = jds;

  onInjectInputs(): void {
    throw new Error('Method not implemented.');
  }

  submitModal(): void {
    this.close(false);
  }

  closeModal() {
    this.dismiss(true);
  }
}
