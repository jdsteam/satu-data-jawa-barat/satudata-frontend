import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  OnInit,
  Output,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  FormsModule,
  ReactiveFormsModule,
  UntypedFormGroup,
  UntypedFormControl,
  Validators,
} from '@angular/forms';

// STYLE
import * as jds from '@styles';

// COMPONENT
import { ModalModule, ModalService } from '@components-v3/modal';
import { JdsBiInputModule } from '@jds-bi/core';
import { JdsBiIconsModule, JdsBiIconsService, iconXmark } from '@jds-bi/icons';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// MODEL
import { LoginData } from '@models-v3';

// SERVICES
import { AuthenticationService } from '@services-v3';

@Component({
  selector: 'app-modal-dataset-active-archive',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    JdsBiInputModule,
    JdsBiIconsModule,
    ModalModule,
  ],
  templateUrl: './dataset-active-archive.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DatasetActiveArchiveComponent implements OnInit {
  colorScheme = COLORSCHEME;
  satudataUser: LoginData;
  jds: any = jds;

  @Output() refresh: EventEmitter<any> = new EventEmitter();
  @Output() updateDataset: EventEmitter<any> = new EventEmitter();

  // Variable
  category: string;

  // Form
  myForm: UntypedFormGroup;

  constructor(
    private authenticationService: AuthenticationService,
    private modalService: ModalService,
    private iconService: JdsBiIconsService,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    this.iconService.registerIcons([iconXmark]);
  }

  ngOnInit() {
    this.myForm = new UntypedFormGroup({
      type: new UntypedFormControl({ value: 'dataset', disabled: false }),
      type_id: new UntypedFormControl(),
      category: new UntypedFormControl(),
      notes: new UntypedFormControl({ value: null, disabled: false }, [
        Validators.required,
      ]),
    });
  }

  get f() {
    return this.myForm.controls;
  }

  openModal() {
    this.modalService.open('modal-dataset-active-archive');
  }

  closeModal() {
    this.modalService.close('modal-dataset-active-archive');
  }

  getNote(id: number, category: string) {
    this.openModal();
    this.category = category;

    this.myForm.patchValue({
      type_id: id,
      category,
    });
  }

  onSubmit() {
    this.closeModal();

    this.updateDataset.emit(this.myForm.value);
  }

  resetForm() {
    this.myForm.reset();
  }
}
