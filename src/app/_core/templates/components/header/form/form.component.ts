import {
  Component,
  OnInit,
  Input,
  ChangeDetectionStrategy,
} from '@angular/core';
import { CommonModule } from '@angular/common';

// STYLE
import * as jds from '@styles';

// MODEL
import { LoginData } from '@models-v3';

// SERVICE
import { AuthenticationService } from '@services-v3';

@Component({
  selector: 'app-header-form',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormComponent implements OnInit {
  satudataUser: LoginData;
  jds: any = jds;

  @Input() type: any;
  @Input() data: any;

  // Variable
  title: string;

  constructor(private authenticationService: AuthenticationService) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
  }

  ngOnInit(): void {
    if (this.type === 'create') {
      this.title = 'Tambah';
    } else if (this.type === 'update') {
      this.title = 'Ubah';
    } else if (this.type === 'preview') {
      this.title = 'Ringkasan';
    }
  }
}
