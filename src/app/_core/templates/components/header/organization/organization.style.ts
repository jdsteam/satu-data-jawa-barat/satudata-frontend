import { Injectable } from '@angular/core';

// PACKAGE
import { css } from '@emotion/css';

@Injectable()
export class OrganizationStyle {
  getDynamicStyle(): any {
    const header = css`
      background: url(/assets/images/backgrounds/bg_header.png);
      background-position: center;
      background-size: cover;
    `;

    return {
      header,
    };
  }
}
