import { Component, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';

// MODEL
import { LoginData } from '@models-v3';

// SERVICE
import { AuthenticationService } from '@services-v3';

// COMPONENT
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconGlobe,
  iconPhone,
  iconEnvelope,
} from '@jds-bi/icons';

import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

import { OrganizationStyle } from './organization.style';

@Component({
  selector: 'app-header-organization',
  standalone: true,
  imports: [CommonModule, JdsBiIconsModule, NgxSkeletonLoaderModule],
  templateUrl: './organization.component.html',
  providers: [OrganizationStyle],
})
export class OrganizationComponent implements OnInit {
  env = environment;
  satudataUser: LoginData;
  jds = jds;

  className!: any;

  @Input() data: any;
  @Input() isLoading = false;
  @Input() create = false;

  constructor(
    private authenticationService: AuthenticationService,
    private iconService: JdsBiIconsService,
    private organizationStyle: OrganizationStyle,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    this.iconService.registerIcons([iconGlobe, iconPhone, iconEnvelope]);
  }

  ngOnInit(): void {
    this.className = this.organizationStyle.getDynamicStyle();
  }
}
