import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { FormsModule } from '@angular/forms';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// MODEL
import { LoginData } from '@models-v3';

// SERVICE
import { AuthenticationService } from '@services-v3';

// SERVICE
import { JdsBiInputModule } from '@jds-bi/core';

import { isNil, isEmpty } from 'lodash';
import { NgxDaterangepickerBootstrapModule } from 'ngx-daterangepicker-bootstrap';
import dayjs from 'dayjs';
import 'dayjs/locale/id';

@Component({
  selector: 'app-header-log-activity',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    JdsBiInputModule,
    NgxDaterangepickerBootstrapModule,
  ],
  templateUrl: './log-activity.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LogActivityComponent implements OnInit {
  colorScheme = COLORSCHEME;
  satudataUser: LoginData;
  jds: any = jds;

  @Input() label: any;
  @Output() filterOutputDate = new EventEmitter<any>();

  // Variable
  filter = {
    currentPage: 1,
    dateStart: null,
    dateEnd: null,
    date: null,
  };

  daterangepicker: any;
  maxDate?: dayjs.Dayjs;
  locale = {
    firstDay: 1,
    startDate: dayjs().startOf('day'),
    endDate: dayjs().endOf('day'),
    format: 'DD MMMM YYYY',
    applyLabel: 'Apply',
    cancelLabel: 'Cancel',
    fromLabel: 'From',
    toLabel: 'To',
  };

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
  }

  ngOnInit(): void {
    this.queryParams();
  }

  queryParams(): void {
    this.route.queryParams.subscribe((p) => {
      const pDateStart = p.dateStart;
      const pDateEnd = p.dateEnd;

      let dateStart: string;
      let dateEnd: string;

      if (!isNil(pDateStart)) {
        this.filter.dateStart = pDateStart;
        dateStart = dayjs(pDateStart).format('DD MMMM YYYY');
      } else {
        this.filter.dateStart = null;
      }

      if (!isNil(pDateEnd)) {
        this.filter.dateEnd = pDateEnd;
        dateEnd = dayjs(pDateEnd).format('DD MMMM YYYY');
      } else {
        this.filter.dateEnd = null;
      }

      this.filter.date =
        !isEmpty(dateStart) && !isEmpty(dateEnd)
          ? `${dateStart} - ${dateEnd}`
          : null;

      if (!isEmpty(dateStart) && !isEmpty(dateEnd)) {
        this.daterangepicker = {
          startDate: dayjs(pDateStart),
          endDate: dayjs(pDateEnd),
        };
      }
    });
  }

  filterDate($event: any): void {
    if (isEmpty($event.startDate) || isEmpty($event.endDate)) {
      return;
    }

    const start = dayjs($event.startDate).format('YYYY-MM-DD');
    const end = dayjs($event.endDate).format('YYYY-MM-DD');

    const valStart = isEmpty(start) === false ? start : null;
    const valEnd = isEmpty(end) === false ? end : null;

    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        dateStart: valStart,
        dateEnd: valEnd,
      },
      queryParamsHandling: 'merge',
    });

    const s = dayjs($event.startDate).format('DD MMMM YYYY');
    const e = dayjs($event.endDate).format('DD MMMM YYYY');
    this.filter.date = `${s} - ${e}`;

    this.filter.dateStart = valStart;
    this.filter.dateEnd = valEnd;
    this.filter.currentPage = 1;
    this.filterOutputDate.emit(this.filter);
  }
}
