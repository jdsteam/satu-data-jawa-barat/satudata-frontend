import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';

// STYLE
import * as jds from '@styles';

// MODEL
import { LoginData, OrganizationData } from '@models-v3';

// SERVICE
import { AuthenticationService, OrganizationService } from '@services-v3';

@Component({
  selector: 'app-header-message',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './message.component.html',
})
export class MessageComponent implements OnInit {
  satudataUser: LoginData;
  jds: any = jds;

  // Data
  organizationData: OrganizationData;
  organizationIsLoadingList = false;

  constructor(
    private authenticationService: AuthenticationService,
    private organizationService: OrganizationService,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
  }

  ngOnInit(): void {
    this.getOrganization();
  }

  getOrganization(): void {
    const pWhere = {
      kode_skpd: this.satudataUser.kode_skpd,
      is_deleted: false,
    };

    const params = {
      where: JSON.stringify(pWhere),
    };

    this.organizationIsLoadingList = true;

    this.organizationService.getList(params).subscribe((result) => {
      const index = 0;
      this.organizationData = result.data[index];
      this.organizationIsLoadingList = false;
    });
  }
}
