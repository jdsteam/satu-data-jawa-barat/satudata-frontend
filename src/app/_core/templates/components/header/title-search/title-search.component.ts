import {
  Component,
  OnInit,
  AfterViewInit,
  DoCheck,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// MODEL
import { LoginData } from '@models-v3';

// SERVICE
import { AuthenticationService } from '@services-v3';

// COMPONENT
import { JdsBiInputModule } from '@jds-bi/core';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconMagnifyingGlass,
} from '@jds-bi/icons';

import { isNil, isEmpty } from 'lodash';
import moment from 'moment';

@Component({
  selector: 'app-header-title-search',
  standalone: true,
  imports: [CommonModule, JdsBiInputModule, JdsBiIconsModule],
  templateUrl: './title-search.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TitleSearchComponent implements OnInit, AfterViewInit, DoCheck {
  colorScheme = COLORSCHEME;
  satudataUser: LoginData;
  moment: any = moment;
  jds: any = jds;

  @Input() label: any;
  @Input() labelSearch: string;
  @Output() filterOutputSearch = new EventEmitter<any>();

  // Variable
  filter = {
    currentPage: 1,
    search: '',
  };

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private iconService: JdsBiIconsService,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    this.iconService.registerIcons([iconMagnifyingGlass]);
  }

  ngOnInit(): void {
    this.queryParams();
  }

  ngAfterViewInit(): void {
    this.cdRef.detectChanges();
  }

  ngDoCheck(): void {
    this.cdRef.detectChanges();
  }

  queryParams(): void {
    this.route.queryParams.subscribe((p) => {
      const pSearch = p.q;

      this.filter.search = !isNil(pSearch) ? pSearch : '';
    });
  }

  filterSearch(event: string): void {
    const value = isEmpty(event) === false ? event : null;

    if (this.filter.search !== value) {
      this.router.navigate([], {
        relativeTo: this.route,
        queryParams: {
          q: value,
          currentpage: null,
        },
        queryParamsHandling: 'merge',
      });

      this.filter.search = value;
      this.filter.currentPage = 1;
      this.filterOutputSearch.emit(this.filter);
    }
  }
}
