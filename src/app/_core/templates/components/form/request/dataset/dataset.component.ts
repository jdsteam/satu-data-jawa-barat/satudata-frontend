/* eslint-disable @typescript-eslint/dot-notation */
import {
  Component,
  OnInit,
  ViewEncapsulation,
  Renderer2,
  OnDestroy,
  ChangeDetectionStrategy,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import {
  FormsModule,
  ReactiveFormsModule,
  UntypedFormGroup,
  UntypedFormControl,
  Validators,
} from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';
import { Actions, ofType } from '@ngrx/effects';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// SERVICE
import { AuthenticationService } from '@services-v3';

// MODEL
import { LoginData, OrganizationData } from '@models-v3';

// STORE
import {
  selectAllOrganization,
  selectIsLoadingList as selectIsLoadingListOrganization,
  selectError as selectErrorOrganization,
} from '@store-v3/organization/organization.selectors';
import { fromOrganizationActions } from '@store-v3/organization/organization.actions';

// COMPONENT
import { JdsBiInputModule } from '@jds-bi/core';

import { selectIsLoadingCreate as selectIsLoadingCreateRequestDatasetPrivate } from '@store-v3/request-dataset-private/request-dataset-private.selectors';
import { fromRequestDatasetPrivateActions } from '@store-v3/request-dataset-private/request-dataset-private.actions';

import { ToastrService } from 'ngx-toastr';

// PLUGIN
import { assign, isNull } from 'lodash';
import moment from 'moment';
import Swal from 'sweetalert2';
import { NgxLoadingModule } from 'ngx-loading';
import { NgSelectModule } from '@ng-select/ng-select';

@Component({
  selector: 'app-form-request-dataset',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    JdsBiInputModule,
    NgxLoadingModule,
    NgSelectModule,
  ],
  templateUrl: './dataset.component.html',
  styleUrls: ['./dataset.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class DatasetComponent implements OnInit, OnDestroy {
  colorScheme = COLORSCHEME;
  jds: any = jds;
  satudataUser: LoginData;
  moment: any = moment;

  // Variable
  myForm: UntypedFormGroup;
  isOrganization = false;

  purposeOption: any[] = [
    'Referensi Pembuatan Kebijakan',
    'Referensi Kajian',
    'Referensi Pribadi',
    'Lainnya',
  ];

  // Data
  organizationData$: Observable<OrganizationData[]>;
  organizationIsLoadingList$: Observable<boolean>;
  organizationIsError: boolean;

  isLoadingCreate$: Observable<boolean>;
  isError$: Observable<boolean>;
  isError = false;
  errorMessage: string;

  private unsubscribeRequest$ = new Subject<void>();

  constructor(
    private authenticationService: AuthenticationService,
    private renderer2: Renderer2,
    private router: Router,
    private route: ActivatedRoute,
    private actions$: Actions,
    private store: Store<any>,
    private toastr: ToastrService,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
  }

  ngOnInit() {
    this.myForm = new UntypedFormGroup({
      user_id: new UntypedFormControl({
        value: this.satudataUser.id,
        disabled: false,
      }),
      nama: new UntypedFormControl({
        value: this.satudataUser.name,
        disabled: false,
      }),
      email: new UntypedFormControl({
        value: this.satudataUser.email,
        disabled: false,
      }),
      telp: new UntypedFormControl({ value: null, disabled: false }, [
        Validators.required,
        Validators.maxLength(255),
      ]),
      judul_data: new UntypedFormControl({ value: null, disabled: false }, [
        Validators.required,
        Validators.maxLength(255),
      ]),
      tau_skpd: new UntypedFormControl({ value: null, disabled: false }, [
        Validators.required,
      ]),
      nama_skpd: new UntypedFormControl({ value: null, disabled: false }),
      kebutuhan_data: new UntypedFormControl({ value: null, disabled: false }, [
        Validators.required,
        Validators.maxLength(255),
      ]),
      tujuan_data: new UntypedFormControl({ value: null, disabled: false }, [
        Validators.required,
      ]),
    });

    this.getAllOrganization();
  }

  ngOnDestroy() {
    this.unsubscribeRequest$.next();
    this.unsubscribeRequest$.complete();
  }

  // GET =======================================================================
  get f() {
    return this.myForm.controls;
  }

  getAllOrganization() {
    const pSort = `nama_skpd:asc`;

    const pWhere = {
      is_satudata: true,
      is_deleted: false,
    };

    const params = {
      sort: pSort,
      where: JSON.stringify(pWhere),
    };

    this.store.dispatch(
      fromOrganizationActions.loadAllOrganization({
        params,
        pagination: false,
        infinite: false,
      }),
    );

    this.organizationIsLoadingList$ = this.store.pipe(
      select(selectIsLoadingListOrganization),
    );

    this.store
      .pipe(
        select(selectErrorOrganization),
        filter((val) => val !== null && val.error),
      )
      .subscribe((result) => {
        if (!result.error) {
          this.organizationIsError = false;
        } else {
          this.organizationIsError = true;
          this.toastr.error(result.message, 'Organization');
        }
      });

    this.organizationData$ = this.store.pipe(
      select(selectAllOrganization),
      filter((val) => val.length !== 0),
    );
  }

  // SAVE ======================================================================
  onSubmit() {
    const input = this.myForm.value;

    const isOrganization = input.tau_skpd !== 'false';
    const organization = input.nama_skpd;

    const bodyRequest = input;
    delete bodyRequest['tau_skpd'];
    delete bodyRequest['nama_skpd'];

    assign(bodyRequest, {
      tau_skpd: isOrganization,
      nama_skpd: isNull(null) ? '' : organization,
    });

    this.isLoadingCreate$ = this.store.pipe(
      select(selectIsLoadingCreateRequestDatasetPrivate),
    );

    this.store.dispatch(
      fromRequestDatasetPrivateActions.createRequestDatasetPrivate({
        create: bodyRequest,
      }),
    );

    this.actions$
      .pipe(
        ofType(
          fromRequestDatasetPrivateActions.createRequestDatasetPrivateSuccess,
        ),
        takeUntil(this.unsubscribeRequest$),
      )
      .subscribe(() => {
        Swal.fire({
          type: 'success',
          html:
            'Permohonan Dataset telah berhasil dilakukan. <br>' +
            'Informasi lebih lanjut akan dikirimkan melalui email yang telah didaftarkan dalam 1x24 jam.',
          allowOutsideClick: false,
        }).then(() => {
          this.router.navigate(['/dataset']);
        });
      });

    this.actions$
      .pipe(
        ofType(
          fromRequestDatasetPrivateActions.createRequestDatasetPrivateFailure,
        ),
        takeUntil(this.unsubscribeRequest$),
      )
      .subscribe((result) => {
        this.toastr.error(result.error.message, 'Request Dataset Private');
      });
  }

  // ONCHANGE ==================================================================
  onSelectedIsOrganization(value: string) {
    this.myForm.controls['nama_skpd'].reset();

    const isTrueSet = value === 'true';
    if (isTrueSet) {
      this.isOrganization = true;
      this.myForm.controls['nama_skpd'].setValidators([Validators.required]);
    } else {
      this.isOrganization = false;
      this.myForm.controls['nama_skpd'].clearValidators();
    }

    this.myForm.controls['nama_skpd'].updateValueAndValidity();
  }
}
