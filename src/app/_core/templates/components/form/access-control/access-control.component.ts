/* eslint-disable @typescript-eslint/dot-notation */
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  FormsModule,
  ReactiveFormsModule,
  UntypedFormGroup,
} from '@angular/forms';

// STYLE
import * as jds from '@styles';
// COMPONENT
import { JdsBiAccordionModule } from '@jds-bi/core';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';

@Component({
  selector: 'app-form-access-control',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    JdsBiAccordionModule,
  ],
  templateUrl: './access-control.component.html',
  styleUrls: ['./access-control.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AccessControlComponent implements OnChanges {
  colorScheme = COLORSCHEME;
  jds: any = jds;

  @Input() organizationStructureDataInput: any;
  @Input() accessControlInput: UntypedFormGroup;

  organizationStructureData: any;
  accessControl: UntypedFormGroup;

  ngOnChanges(changes: SimpleChanges): void {
    // eslint-disable-next-line no-restricted-syntax
    for (const propName in changes) {
      // eslint-disable-next-line no-prototype-builtins
      if (changes.hasOwnProperty(propName)) {
        if (propName === 'accessControlInput') {
          const chng = changes[propName];
          this.accessControl = chng.currentValue;
        }

        if (propName === 'organizationStructureDataInput') {
          const chng = changes[propName];
          this.organizationStructureData = chng.currentValue;
        }
      }
    }
  }

  getPositionName(organization) {
    return organization[`lv${organization.level}_unit_kerja_nama`];
  }

  onCheckAll(accessControl) {
    const checked = !!accessControl.get('checked').value;
    accessControl.get('position').setValue(checked);

    const setValue = (input) => {
      input.get('level')['controls'].forEach((res) => {
        res.get('checked').setValue(checked);
        res.get('position').setValue(checked);

        if (res.get('level')) {
          setValue(res);
        }
      });
    };

    if (accessControl.get('level')) {
      setValue(accessControl);
    }
  }
}
