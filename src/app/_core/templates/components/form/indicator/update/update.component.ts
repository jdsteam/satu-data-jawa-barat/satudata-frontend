/* eslint-disable @typescript-eslint/dot-notation */
import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
  inject,
  effect,
} from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import {
  FormsModule,
  ReactiveFormsModule,
  UntypedFormGroup,
  UntypedFormControl,
  UntypedFormArray,
  Validators,
} from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import {
  filter,
  debounceTime,
  distinctUntilChanged,
  switchMap,
  takeUntil,
} from 'rxjs/operators';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME, STATUS_DATASET, STATUS_INDICATOR } from '@constants-v4';
// SERVICE
import { AuthenticationService } from '@services-v3';
import {
  IndicatorConnectService,
  IndicatorDatasetService,
  IndicatorProgramService,
  IndicatorProgressFinalService,
  IndicatorProgressInitialService,
} from '@services-v4';
// MODEL
import {
  LoginData,
  ClassificationIndicatorData,
  IndicatorData,
  IndicatorProgressData,
  IndicatorCategoryData,
  UnitData,
  OrganizationData,
  OrganizationSubData,
} from '@models-v3';
// UTIL
import { CKEditorUploadAdapterPlugin } from '@utils-v4';
// JDS-BI
import { JdsBiAccordionModule, JdsBiInputModule } from '@jds-bi/core';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconCirclePlus,
  iconTrash,
} from '@jds-bi/icons';

// STORE
import {
  selectIndicator,
  selectIsLoadingRead as selectIsLoadingReadIndicator,
} from '@store-v3/indicator/indicator.selectors';
import { fromIndicatorActions } from '@store-v3/indicator/indicator.actions';

import { fromIndicatorClassificationHistoryActions } from '@store-v3/indicator-classification-history/indicator-classification-history.actions';

import { fromIndicatorProgressActions } from '@store-v3/indicator-progress/indicator-progress.actions';

import {
  selectAllClassificationIndicator,
  selectIsLoadingList as selectIsLoadingListClassificationIndicator,
} from '@store-v3/classification-indicator/classification-indicator.selectors';
import { fromClassificationIndicatorActions } from '@store-v3/classification-indicator/classification-indicator.actions';

import {
  selectAllIndicatorCategory,
  selectIsLoadingList as selectIsLoadingListIndicatorCategory,
} from '@store-v3/indicator-category/indicator-category.selectors';
import { fromIndicatorCategoryActions } from '@store-v3/indicator-category/indicator-category.actions';

import {
  selectAllUnit,
  selectIsLoadingList as selectIsLoadingListUnit,
} from '@store-v3/unit/unit.selectors';
import { fromUnitActions } from '@store-v3/unit/unit.actions';

import {
  selectAllOrganization,
  selectIsLoadingList as selectIsLoadingListOrganization,
} from '@store-v3/organization/organization.selectors';
import { fromOrganizationActions } from '@store-v3/organization/organization.actions';

import {
  selectAllOrganizationSub,
  selectIsLoadingList as selectIsLoadingListOrganizationSub,
} from '@store-v3/organization-sub/organization-sub.selectors';
import { fromOrganizationSubActions } from '@store-v3/organization-sub/organization-sub.actions';

// PLUGIN
// eslint-disable-next-line object-curly-newline
import { assign, forEach, head, isNil, last, map, size } from 'lodash';
import moment from 'moment';
import Swal from 'sweetalert2';
import { Store, select } from '@ngrx/store';
import { Actions, ofType } from '@ngrx/effects';
import { NgxLoadingModule } from 'ngx-loading';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgSelectModule } from '@ng-select/ng-select';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import * as CKEditor from '@scripts/ckeditor-5/ckeditor';

@Component({
  selector: 'app-form-indicator-update',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    JdsBiAccordionModule,
    JdsBiInputModule,
    JdsBiIconsModule,
    NgxLoadingModule,
    NgxSkeletonLoaderModule,
    NgSelectModule,
    CKEditorModule,
  ],
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdateComponent implements OnInit, OnDestroy {
  satudataUser: LoginData;
  satudataToken: string;
  colorScheme = COLORSCHEME;
  moment: any = moment;
  jds: any = jds;
  public Editor = CKEditor;

  // Service
  private router = inject(Router);
  private route = inject(ActivatedRoute);
  private iconService = inject(JdsBiIconsService);
  private authenticationService = inject(AuthenticationService);
  private indicatorConnectService = inject(IndicatorConnectService);
  private indicatorDatasetService = inject(IndicatorDatasetService);
  private indicatorProgramService = inject(IndicatorProgramService);
  private indicatorProgressInitialService = inject(
    IndicatorProgressInitialService,
  );
  private indicatorProgressFinalService = inject(IndicatorProgressFinalService);

  // variable
  id: number;
  param: any;
  myForm = new UntypedFormGroup({
    name: new UntypedFormControl({ value: null, disabled: false }, [
      Validators.required,
      Validators.maxLength(255),
    ]),
    classification: new UntypedFormControl([], [Validators.required]),
    indikator_category_id: new UntypedFormControl(
      { value: null, disabled: false },
      [Validators.required],
    ),
    indicator_dataset: new UntypedFormArray([]),
    indicator_connect: new UntypedFormArray([]),
    indicator_program: new UntypedFormArray([]),
    indicator_basic_data: new UntypedFormArray([]),
    rumus: new UntypedFormControl({ value: null, disabled: false }, [
      Validators.required,
    ]),
    satuan_id: new UntypedFormControl({ value: null, disabled: false }, [
      Validators.required,
    ]),
    source: new UntypedFormControl(),
    interpretation: new UntypedFormControl(),
    kode_skpd: new UntypedFormControl({ value: null, disabled: false }, [
      Validators.required,
    ]),
    kode_skpd_sub: new UntypedFormControl({ value: null, disabled: false }),
    datetime: new UntypedFormControl({ value: null, disabled: false }, [
      Validators.required,
    ]),
    year_initial: new UntypedFormControl({ value: null, disabled: false }, [
      Validators.required,
    ]),
    year_final: new UntypedFormControl({ value: null, disabled: false }, [
      Validators.required,
    ]),
    kondisi_awal: new UntypedFormArray([]),
    kondisi_akhir: new UntypedFormArray([]),
    // history_draft edit
    change: new UntypedFormControl({ value: null, disabled: false }),
  });

  // Editor
  EditorConfig: any;

  // Data
  indicatorData: IndicatorData;
  indicatorData$: Observable<IndicatorData>;
  indicatorIsLoadingRead$: Observable<boolean>;
  indicatorIsLoadingUpdate$: Observable<boolean>;

  classificationIndicatorData$: Observable<ClassificationIndicatorData[]>;
  classificationIndicatorIsLoadingList$: Observable<boolean>;
  classificationIndicatorIsError$: Observable<string>;

  indicatorCategoryData$: Observable<IndicatorCategoryData[]>;
  indicatorCategoryIsLoadingList$: Observable<boolean>;
  indicatorCategoryIsError$: Observable<string>;

  unitData$: Observable<UnitData[]>;
  unitIsLoadingList$: Observable<boolean>;
  unitIsLoadingCreate$: Observable<boolean>;
  unitData = [];
  unitAll = [];
  unitValid = true;
  unitValue: number;

  organizationData$: Observable<OrganizationData[]>;
  organizationIsLoadingList$: Observable<boolean>;
  organizationIsError$: Observable<string>;

  organizationSubData$: Observable<OrganizationSubData[]>;
  organizationSubIsLoadingList$: Observable<boolean>;
  organizationSubIsError$: Observable<string>;

  yearData = [];
  yearInitial: number;
  yearFinal: number;

  indicatorProgressInitialData = toSignal(
    this.indicatorProgressInitialService.getQueryListSingle(
      this.route.snapshot.params.id,
      {
        sort: 'index:asc',
        where: JSON.stringify({
          indikator_id: this.route.snapshot.params.id,
          index: 'awal',
        }),
      },
      {
        pagination: false,
      },
    ).result$,
    { initialValue: null },
  );

  indicatorProgressFinalData = toSignal(
    this.indicatorProgressFinalService.getQueryListSingle(
      this.route.snapshot.params.id,
      {
        sort: 'index:asc',
        where: JSON.stringify({
          indikator_id: this.route.snapshot.params.id,
          index: 'akhir',
        }),
      },
      {
        pagination: false,
      },
    ).result$,
    { initialValue: null },
  );

  inputSearchDataset$ = new Subject<string>();
  dataSearchDataset = toSignal(
    this.inputSearchDataset$.pipe(
      debounceTime(1000),
      distinctUntilChanged(),
      filter((value) => value !== null),
      switchMap((value) => {
        const pWhere = {};

        assign(pWhere, STATUS_DATASET.get('approve-active'));

        const params = {
          search: value || '',
          sort: 'name:asc',
          where: JSON.stringify(pWhere),
        };
        const options = {
          pagination: false,
        };

        return this.indicatorDatasetService.getQueryList(params, options)
          .result$;
      }),
    ),
    { initialValue: null },
  );

  inputSearchIndicatorConnect$ = new Subject<string>();
  dataSearchIndicatorConnect = toSignal(
    this.inputSearchIndicatorConnect$.pipe(
      debounceTime(1000),
      distinctUntilChanged(),
      filter((value) => value !== null),
      switchMap((value) => {
        const pWhere = {};

        assign(pWhere, STATUS_INDICATOR.get('approve-active'));

        const params = {
          search: value || '',
          sort: 'name:asc',
          where: JSON.stringify(pWhere),
        };
        const options = {
          pagination: false,
        };

        return this.indicatorConnectService.getQueryList(params, options)
          .result$;
      }),
    ),
    { initialValue: null },
  );

  inputSearchIndicatorProgram$ = new Subject<string>();
  dataSearchIndicatorProgram = toSignal(
    this.indicatorProgramService.getQueryList(
      {
        sort: 'nama_program:asc',
      },
      {
        pagination: false,
      },
    ).result$,
    { initialValue: null },
  );

  private unsubscribeIndicator$ = new Subject<void>();

  constructor(
    private actions$: Actions,
    private store: Store<any>,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
    this.authenticationService.satudataToken.subscribe(
      (x) => (this.satudataToken = x),
    );

    this.iconService.registerIcons([iconTrash, iconCirclePlus]);

    this.id = this.route.snapshot.params.id;

    this.EditorConfig = {
      name: 'file',
      toolbar: [
        'Bold',
        'Italic',
        'Underline',
        'Strikethrough',
        'Subscript',
        'Superscript',
        '|',
        'fontFamily',
        'fontSize',
        'fontColor',
        'fontBackgroundColor',
        '|',
        'imageUpload',
        '|',
        'Undo',
        'Redo',
      ],
      htmlSupport: {
        allow: [
          {
            name: 'span',
            attributes: true,
          },
        ],
      },
      extraPlugins: [CKEditorUploadAdapterPlugin],
      token: this.satudataToken,
    };
  }

  ngOnInit() {
    this.queryParams();
    this.getIndicator();
    this.getAllClassificationIndicator();
    this.getAllIndicatorCategory();
    this.getAllUnit();
    this.getAllOrganization();
    this.createYear();
  }

  ngOnDestroy() {
    this.unsubscribeIndicator$.next();
    this.unsubscribeIndicator$.complete();
  }

  queryParams(): void {
    this.route.queryParams.subscribe((p) => {
      const pPage = !isNil(p['page']) ? p['page'] : null;
      const pStatus = !isNil(p['status']) ? p['status'] : null;

      this.param = {
        page: pPage,
        status: pStatus,
      };
    });
  }

  get f() {
    return this.myForm.controls;
  }

  get fid() {
    return this.f.indicator_dataset as UntypedFormArray;
  }

  get fic() {
    return this.f.indicator_connect as UntypedFormArray;
  }

  get fip() {
    return this.f.indicator_program as UntypedFormArray;
  }

  get fibd() {
    return this.f.indicator_basic_data as UntypedFormArray;
  }

  get fikaw() {
    return this.f.kondisi_awal as UntypedFormArray;
  }

  get fikak() {
    return this.f.kondisi_akhir as UntypedFormArray;
  }

  get isIndicatorChange() {
    const data = this.indicatorData;

    if (!data) {
      return false;
    }

    let change: boolean;
    if (
      (data.is_validate === 2 || data.is_validate === 3) &&
      data.is_active &&
      !data.is_deleted
    ) {
      change = true;
    } else {
      change = false;
    }
    return change;
  }

  addUnit = (name: string) => {
    if (size(name) >= 48) {
      this.unitValid = false;
      return null;
    }
    this.unitValid = true;
    return { name, unit: name, from: 'input' };
  };

  // DYNAMIC FORM ============================================================================================
  addDataset() {
    this.fid.push(this.createDataset());
    return false;
  }

  createDataset(): UntypedFormGroup {
    return new UntypedFormGroup({
      dataset: new UntypedFormControl([]),
    });
  }

  removeDataset(i) {
    if (this.fid) {
      this.fid.removeAt(i);
    }
    return false;
  }

  addConnect() {
    this.fic.push(this.createConnect());
    return false;
  }

  createConnect(): UntypedFormGroup {
    return new UntypedFormGroup({
      connect: new UntypedFormControl([]),
    });
  }

  removeConnect(i) {
    if (this.fic) {
      this.fic.removeAt(i);
    }
    return false;
  }

  addProgram() {
    this.fip.push(this.createProgram());
    return false;
  }

  createProgram(): UntypedFormGroup {
    return new UntypedFormGroup({
      program: new UntypedFormControl([]),
    });
  }

  removeProgram(i) {
    if (this.fip) {
      this.fip.removeAt(i);
    }
    return false;
  }

  addBasicData() {
    this.fibd.push(this.createBasicData());
    return false;
  }

  createBasicData(): UntypedFormGroup {
    return new UntypedFormGroup({
      basic_data: new UntypedFormControl({ value: null, disabled: false }, [
        Validators.required,
        Validators.maxLength(255),
      ]),
    });
  }

  removeBasicData(i) {
    if (this.fibd) {
      this.fibd.removeAt(i);
    }
    return false;
  }

  createYear() {
    const year = 2010;
    const yearPlus = moment().add(10, 'years').format('YYYY');

    for (let index = Number(year); index < Number(yearPlus); index += 1) {
      this.yearData.push(index);
    }
  }

  addFinalCondition(): boolean {
    const yearNow = moment().format('YYYY');

    if (isNil(this.yearInitial) === true || isNil(this.yearFinal) === true) {
      Swal.fire({
        type: 'error',
        text: 'Tahun harus diisi.',
        allowOutsideClick: false,
      });
      return false;
    }

    const prevValue = this.fikak.getRawValue();
    this.removeFinalCondition();

    for (
      let index = Number(this.yearInitial);
      index <= Number(this.yearFinal);
      index += 1
    ) {
      const disabled = index > Number(yearNow);

      const exist = prevValue.find((item) => item.year === index);

      if (exist) {
        this.fikak.push(this.createFinalCondition(index, disabled, exist));
      } else {
        this.fikak.push(this.createFinalCondition(index, disabled));
      }
    }

    return true;
  }

  createFinalCondition(
    index: number,
    disabled: boolean,
    progress = null,
  ): UntypedFormGroup {
    const progressTarget = progress ? progress.target : null;
    const progressAchievement = progress ? progress.capaian : null;

    let achievement: any;
    if (disabled) {
      achievement = new UntypedFormControl(
        { value: progressAchievement, disabled },
        [Validators.maxLength(255)],
      );
    } else {
      achievement = new UntypedFormControl(
        { value: progressAchievement, disabled },
        [Validators.required, Validators.maxLength(255)],
      );
    }

    return new UntypedFormGroup({
      year: new UntypedFormControl({ value: index, disabled: false }, [
        Validators.required,
        Validators.maxLength(255),
      ]),
      target: new UntypedFormControl(
        { value: progressTarget, disabled: false },
        [Validators.required, Validators.maxLength(255)],
      ),
      capaian: achievement,
    });
  }

  removeFinalCondition() {
    while (this.fikak.length !== 0) {
      this.fikak.removeAt(0);
    }
  }

  // GET =====================================================================================================
  getIndicator() {
    this.indicatorIsLoadingRead$ = this.store.pipe(
      select(selectIsLoadingReadIndicator),
    );

    this.indicatorData$ = this.store.pipe(select(selectIndicator(this.id)));

    this.store
      .pipe(
        select(selectIndicator(this.id)),
        filter((val) => val !== undefined),
      )
      .subscribe((result) => {
        this.indicatorData = result;

        this.setFormIndicator(result);
        this.setFormIndicatorDataset(result.indikator_dataset);
        this.setFormIndicatorConnect(result.indikator_terhubung);
        this.setFormIndicatorProgram(result.indikator_program);
        this.setFormIndicatorBasicData(result.data_dasar_array);

        if (this.isIndicatorChange) {
          this.myForm
            .get('change')
            .setValidators([Validators.required, Validators.minLength(15)]);
          this.myForm.get('change').updateValueAndValidity();
        }
      });
  }

  getAllClassificationIndicator() {
    const pSort = `name:asc`;

    const params = {
      sort: pSort,
    };

    this.store.dispatch(
      fromClassificationIndicatorActions.loadAllClassificationIndicator({
        params,
        pagination: false,
        infinite: false,
      }),
    );

    this.classificationIndicatorIsLoadingList$ = this.store.pipe(
      select(selectIsLoadingListClassificationIndicator),
    );

    this.classificationIndicatorData$ = this.store.pipe(
      select(selectAllClassificationIndicator),
      filter((val) => val.length !== 0),
    );
  }

  getAllIndicatorCategory() {
    const pSort = `name:asc`;

    const params = {
      sort: pSort,
    };

    this.store.dispatch(
      fromIndicatorCategoryActions.loadAllIndicatorCategory({
        params,
        pagination: false,
        infinite: false,
      }),
    );

    this.indicatorCategoryIsLoadingList$ = this.store.pipe(
      select(selectIsLoadingListIndicatorCategory),
    );

    this.indicatorCategoryData$ = this.store.pipe(
      select(selectAllIndicatorCategory),
      filter((val) => val.length !== 0),
    );
  }

  getAllUnit() {
    const pSort = `name:asc`;

    const params = {
      sort: pSort,
    };

    this.store.dispatch(
      fromUnitActions.loadAllUnit({
        params,
        pagination: false,
        infinite: false,
      }),
    );

    this.unitIsLoadingList$ = this.store.pipe(select(selectIsLoadingListUnit));

    this.store
      .pipe(
        select(selectAllUnit),
        filter((val) => val.length !== 0),
      )
      .subscribe((res) => {
        const modify = [];
        forEach(res, (resUnit) => {
          modify.push({
            ...resUnit,
            from: 'database',
          });
        });

        this.unitData = modify;
      });
  }

  getAllOrganization() {
    const pSort = `nama_skpd:asc`;

    const pWhere = JSON.stringify({
      is_satudata: true,
      is_deleted: false,
    });

    const params = {
      sort: pSort,
      where: pWhere,
    };

    this.store.dispatch(
      fromOrganizationActions.loadAllOrganization({
        params,
        pagination: false,
        infinite: false,
      }),
    );

    this.organizationIsLoadingList$ = this.store.pipe(
      select(selectIsLoadingListOrganization),
    );

    this.organizationData$ = this.store.pipe(
      select(selectAllOrganization),
      filter((val) => val.length !== 0),
    );
  }

  getAllOrganizationSub(kodeSkpd: string) {
    const pSort = `nama_skpdsub:asc`;
    const pWhere = JSON.stringify({
      kode_skpd: kodeSkpd,
    });

    const params = {
      sort: pSort,
      where: pWhere,
    };

    this.store.dispatch(
      fromOrganizationSubActions.loadAllOrganizationSub({
        params,
        pagination: false,
        infinite: false,
      }),
    );

    this.organizationSubIsLoadingList$ = this.store.pipe(
      select(selectIsLoadingListOrganizationSub),
    );

    this.organizationSubData$ = this.store.pipe(
      select(selectAllOrganizationSub),
      filter((val) => val.length !== 0),
    );
  }

  // SET FORM ================================================================================================
  setFormIndicator(data: IndicatorData) {
    const classification = [];
    map(data.history_class, (res) => {
      classification.push({
        name: res.name,
        id: res.id_class,
      });
    });

    if (this.satudataUser.role_name === 'opd') {
      this.getAllOrganizationSub(this.satudataUser.kode_skpd);
    } else {
      this.getAllOrganizationSub(data.kode_skpd);
    }

    this.myForm.patchValue({
      name: data.name,
      classification,
      indikator_category_id: data.indikator_category_id,
      rumus: data.rumus,
      satuan_id: {
        name: data.satuan_name,
        id: data.satuan_id,
        from: 'database',
      },
      source: data.source,
      interpretation: data.interpretation,
      kode_skpd: data.kode_skpd,
      kode_skpd_sub: data.kode_skpd_sub,
      datetime: data.datetime ? moment(data.datetime).format('YYYY-MM') : null,
    });
  }

  setFormIndicatorDataset(data: any[]) {
    if (data.length > 0) {
      forEach(data, (item) => {
        const tmpDict = {};
        tmpDict['dataset'] = new UntypedFormControl({
          id: item.dataset_id,
          name: item.dataset_nama,
          disabled: false,
        });

        this.fid.push(new UntypedFormGroup(tmpDict));
      });
    } else {
      this.addDataset();
    }
  }

  setFormIndicatorConnect(data: any[]) {
    if (data.length > 0) {
      forEach(data, (item) => {
        const tmpDict = {};
        tmpDict['connect'] = new UntypedFormControl({
          id: item.indikator_terhubung_id,
          name: item.indikator_terhubung_nama,
          disabled: false,
        });

        this.fic.push(new UntypedFormGroup(tmpDict));
      });
    } else {
      this.addConnect();
    }
  }

  setFormIndicatorProgram(data: any[]) {
    if (data.length > 0) {
      forEach(data, (item) => {
        const tmpDict = {};
        tmpDict['program'] = new UntypedFormControl({
          id_program: item.id_program,
          nama_program: item.nama_program,
          disabled: false,
        });

        this.fip.push(new UntypedFormGroup(tmpDict));
      });
    } else {
      this.addProgram();
    }
  }

  setFormIndicatorBasicData(data: any[]) {
    if (data && data.length > 0) {
      forEach(data, (item) => {
        const tmpDict = {};
        tmpDict['basic_data'] = new UntypedFormControl({
          value: item.name,
          disabled: false,
        });

        this.fibd.push(new UntypedFormGroup(tmpDict));
      });
    } else {
      this.addBasicData();
    }
  }

  private setFormProgressInitial = effect(() => {
    const progress = this.indicatorProgressInitialData();

    if (progress.isLoading) {
      return;
    }

    this.fikaw.clear();

    progress.data.list.forEach((item) => {
      const tmpDict = {};
      tmpDict['year'] = new UntypedFormControl(
        { value: item.year, disabled: false },
        [Validators.required, Validators.maxLength(255)],
      );
      tmpDict['capaian'] = new UntypedFormControl(
        { value: item.capaian, disabled: false },
        [Validators.required, Validators.maxLength(255)],
      );

      this.fikaw.push(new UntypedFormGroup(tmpDict));
    });
  });

  private setFormProgressFinal = effect(() => {
    const progress = this.indicatorProgressFinalData();

    if (progress.isLoading) {
      return;
    }

    this.fikak.clear();

    const data = progress.data.list as unknown as IndicatorProgressData[];
    const yearNow = moment().format('YYYY');
    const first = head(data);
    const end = last(data);

    this.myForm.patchValue({
      year_initial: first.year,
      year_final: end.year,
    });
    this.yearInitial = first.year;
    this.yearFinal = end.year;

    progress.data.list.forEach((item) => {
      const tmpDict = {};
      tmpDict['year'] = new UntypedFormControl(
        { value: item.year, disabled: false },
        [Validators.required, Validators.maxLength(255)],
      );
      tmpDict['target'] = new UntypedFormControl(
        { value: item.target, disabled: false },
        [Validators.required, Validators.maxLength(255)],
      );

      const disabled = item.year > Number(yearNow);
      if (disabled) {
        tmpDict['capaian'] = new UntypedFormControl(
          { value: item.capaian, disabled },
          [Validators.maxLength(255)],
        );
      } else {
        tmpDict['capaian'] = new UntypedFormControl(
          { value: item.capaian, disabled },
          [Validators.required, Validators.maxLength(255)],
        );
      }

      this.fikak.push(new UntypedFormGroup(tmpDict));
    });
  });

  // ONCHANGE ================================================================================================
  onSelectedOrganization(value: string) {
    const kodeSkpd = value;
    this.myForm.controls['kode_skpd_sub'].reset();
    this.getAllOrganizationSub(kodeSkpd);
  }

  onClearOrganization() {
    this.myForm.controls['kode_skpd_sub'].reset();
  }

  onSelectedYearInitial(value: number) {
    this.yearInitial = value;
  }

  onSelectedYearFinal(value: number) {
    this.yearFinal = value;
  }

  // SAVE ====================================================================================================
  onSubmit() {
    const indicator = this.myForm.value;

    let categoryHistory;
    switch (this.param.status) {
      case 'draft-active':
        categoryHistory = 'new-active';
        break;
      case 'new-active':
        categoryHistory = null;
        break;
      default:
        categoryHistory = null;
        break;
    }

    const {
      indicator_dataset: indicatorDataset,
      indicator_connect: indicatorConnect,
      indicator_program: indicatorProgram,
      indicator_basic_data: indicatorBasicData,
      datetime,
      classification,
      satuan_id: unit,
      kondisi_awal: initialCondition,
      kondisi_akhir: finalCondition,
    } = this.myForm.value;

    delete indicator['indicator_dataset'];
    delete indicator['indicator_connect'];
    delete indicator['indicator_program'];
    delete indicator['indicator_basic_data'];
    delete indicator['datetime'];
    delete indicator['classification'];
    delete indicator['satuan_id'];
    delete indicator['kondisi_awal'];
    delete indicator['kondisi_akhir'];
    delete indicator['year_initial'];
    delete indicator['year_final'];
    delete indicator['change'];

    const hasEmpty = (data, field) => {
      return data.some(
        (obj) =>
          obj[field] === null ||
          (Array.isArray(obj[field]) && obj[field].length === 0),
      );
    };

    if (!hasEmpty(indicatorDataset, 'dataset')) {
      assign(indicator, {
        indikator_dataset: indicatorDataset.map((obj: any) => obj.dataset.id),
      });
    } else {
      assign(indicator, { indikator_dataset: [] });
    }

    if (!hasEmpty(indicatorConnect, 'connect')) {
      assign(indicator, {
        indikator_terhubung: indicatorConnect.map((obj: any) => obj.connect.id),
      });
    } else {
      assign(indicator, { indikator_terhubung: [] });
    }

    if (!hasEmpty(indicatorProgram, 'program')) {
      assign(indicator, {
        indikator_program: indicatorProgram.map(
          (obj: any) => obj.program.id_program,
        ),
      });
    } else {
      assign(indicator, { indikator_program: [] });
    }

    assign(indicator, {
      indikator_data_dasar: indicatorBasicData.map(
        (obj: any) => obj.basic_data,
      ),
    });

    if (unit.from === 'input') {
      const bodyUnit = { name: unit.name };

      this.store.dispatch(fromUnitActions.createUnit({ create: bodyUnit }));

      this.actions$
        .pipe(ofType(fromUnitActions.createUnitSuccess))
        .subscribe((res) => {
          const bodyIndicator = assign({}, indicator, {
            id: this.id,
            datetime: moment(datetime).format('YYYY-MM-01'),
            satuan_id: res.data.id,
          });

          this.store.dispatch(
            fromIndicatorActions.updateIndicator({
              update: bodyIndicator,
            }),
          );
        });
    } else if (unit.from === 'database') {
      const bodyIndicator = assign({}, indicator, {
        id: this.id,
        datetime: moment(datetime).format('YYYY-MM-01'),
        satuan_id: unit.id,
      });

      this.store.dispatch(
        fromIndicatorActions.updateIndicator({
          update: bodyIndicator,
        }),
      );
    }

    // TODO: activate if backend history_draft ready
    // // History
    // if (categoryHistory) {
    //   const bodyHistory = {
    //     category: categoryHistory,
    //     notes: indicator.change,
    //   };
    //   assign(indicator, { history_draft: bodyHistory });
    // }

    // Status
    if (categoryHistory) {
      assign(indicator, STATUS_INDICATOR.get(categoryHistory));
    }

    this.actions$
      .pipe(
        ofType(fromIndicatorActions.updateIndicatorSuccess),
        takeUntil(this.unsubscribeIndicator$),
      )
      .subscribe((data) => {
        this.onSubmitClassification(classification);

        this.onSubmitProgress(initialCondition, finalCondition);

        this.router.navigate(['/my-indicator/preview', data.data.id], {
          queryParams: this.param,
        });
      });
  }

  onSubmitClassification(classification) {
    const bodyClassification = [];
    map(classification, (res) => {
      bodyClassification.push({
        indikator_id: this.id,
        indikator_class_id: res.id,
      });
    });

    this.store.dispatch(
      fromIndicatorClassificationHistoryActions.updateIndicatorClassificationHistoryBulk(
        {
          indicatorId: this.id,
          update: bodyClassification,
        },
      ),
    );
  }

  onSubmitProgress(initialCondition, finalCondition) {
    const params = `?`;

    const bodyProgress = [];
    map(initialCondition, (result, index) => {
      bodyProgress.push({
        indikator_id: this.id,
        year: result.year,
        target: result.target || null,
        capaian: result.capaian || null,
        notes: result.notes,
        index: index + 1,
      });
    });

    map(finalCondition, (result, index) => {
      bodyProgress.push({
        indikator_id: this.id,
        year: result.year,
        target: result.target || null,
        capaian: result.capaian || null,
        notes: result.notes,
        index: index + 99,
      });
    });

    this.store.dispatch(
      fromIndicatorProgressActions.updateIndicatorProgressBulk({
        indicatorId: this.id,
        update: bodyProgress,
        params,
      }),
    );
  }
}
