import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  inject,
} from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { CommonModule } from '@angular/common';
import { Router } from '@angular/router';
import {
  FormsModule,
  ReactiveFormsModule,
  UntypedFormGroup,
  UntypedFormControl,
  UntypedFormArray,
  Validators,
} from '@angular/forms';
import { Subject, zip } from 'rxjs';
import {
  filter,
  debounceTime,
  distinctUntilChanged,
  switchMap,
  tap,
} from 'rxjs/operators';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME, STATUS_DATASET, STATUS_INDICATOR } from '@constants-v4';
// SERVICE
import { AuthenticationService } from '@services-v3';
import {
  ClassificationIndicatorService,
  CategoryIndicatorService,
  IndicatorConnectService,
  IndicatorDatasetService,
  IndicatorProgramService,
  UnitService,
  OrganizationService,
  OrganizationSubService,
  IndicatorService,
  IndicatorHistoryService,
  IndicatorProgressInitialService,
  IndicatorProgressFinalService,
} from '@services-v4';
// MODEL
import { LoginData } from '@models-v3';
// UTIL
import { CKEditorUploadAdapterPlugin } from '@utils-v4';
// JDS-BI
import { JdsBiAccordionModule, JdsBiInputModule } from '@jds-bi/core';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconCirclePlus,
  iconTrash,
} from '@jds-bi/icons';

// PLUGIN
import { assign, isNil, size } from 'lodash';
import moment from 'moment';
import Swal from 'sweetalert2';
import { NgxLoadingModule } from 'ngx-loading';
import { NgSelectModule } from '@ng-select/ng-select';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import * as CKEditor from '@scripts/ckeditor-5/ckeditor';
import { useMutationResult } from '@ngneat/query';
import { SubscribeDirective } from '@ngneat/subscribe';

@Component({
  selector: 'app-form-indicator-create',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    JdsBiAccordionModule,
    JdsBiInputModule,
    JdsBiIconsModule,
    NgxLoadingModule,
    NgSelectModule,
    CKEditorModule,
    SubscribeDirective,
  ],
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateComponent implements OnInit {
  satudataUser: LoginData;
  satudataToken: string;
  colorScheme = COLORSCHEME;
  moment: any = moment;
  jds: any = jds;
  public Editor = CKEditor;

  // Service
  private router = inject(Router);
  private iconService = inject(JdsBiIconsService);
  private authenticationService = inject(AuthenticationService);
  private classificationService = inject(ClassificationIndicatorService);
  private categoryService = inject(CategoryIndicatorService);
  private indicatorConnectService = inject(IndicatorConnectService);
  private indicatorDatasetService = inject(IndicatorDatasetService);
  private indicatorProgramService = inject(IndicatorProgramService);
  private indicatorService = inject(IndicatorService);
  private unitService = inject(UnitService);
  private organizationService = inject(OrganizationService);
  private organizationSubService = inject(OrganizationSubService);
  private indicatorHistoryService = inject(IndicatorHistoryService);
  private indicatorProgressInitialService = inject(
    IndicatorProgressInitialService,
  );
  private indicatorProgressFinalService = inject(IndicatorProgressFinalService);
  addUnitMutation = useMutationResult();
  addIndicatorMutation = useMutationResult();

  // Variable
  myForm = new UntypedFormGroup({
    name: new UntypedFormControl({ value: null, disabled: false }, [
      Validators.required,
      Validators.maxLength(255),
    ]),
    classification: new UntypedFormControl([], [Validators.required]),
    indikator_category_id: new UntypedFormControl(
      { value: null, disabled: false },
      [Validators.required],
    ),
    indicator_dataset: new UntypedFormArray([this.createDataset()]),
    indicator_connect: new UntypedFormArray([this.createConnect()]),
    indicator_program: new UntypedFormArray([this.createProgram()]),
    indicator_basic_data: new UntypedFormArray([this.createBasicData()]),
    rumus: new UntypedFormControl({ value: null, disabled: false }, [
      Validators.required,
    ]),
    satuan_id: new UntypedFormControl({ value: null, disabled: false }, [
      Validators.required,
    ]),
    source: new UntypedFormControl(),
    interpretation: new UntypedFormControl(),
    kode_skpd: new UntypedFormControl({ value: null, disabled: false }, [
      Validators.required,
    ]),
    kode_skpd_sub: new UntypedFormControl({ value: null, disabled: false }),
    datetime: new UntypedFormControl({ value: null, disabled: false }, [
      Validators.required,
    ]),
    year_initial: new UntypedFormControl({ value: null, disabled: false }, [
      Validators.required,
    ]),
    year_final: new UntypedFormControl({ value: null, disabled: false }, [
      Validators.required,
    ]),
    is_kondisi_awal: new UntypedFormControl({ value: null, disabled: false }, [
      Validators.required,
    ]),
    kondisi_awal: new UntypedFormArray([
      this.createInitialCondition(),
      this.createInitialCondition(),
    ]),
    kondisi_akhir: new UntypedFormArray([]),
  });

  // Editor
  EditorConfig: any;

  // Data
  unitValid = true;

  yearData = [];
  yearInitial: number;
  yearFinal: number;

  classificationData = toSignal(
    this.classificationService.getQueryList(
      {
        sort: 'name:asc',
      },
      {
        pagination: false,
      },
    ).result$,
    { initialValue: null },
  );

  categoryData = toSignal(
    this.categoryService.getQueryList(
      {
        sort: 'name:asc',
      },
      {
        pagination: false,
      },
    ).result$,
    { initialValue: null },
  );

  inputSearchDataset$ = new Subject<string>();
  dataSearchDataset = toSignal(
    this.inputSearchDataset$.pipe(
      debounceTime(1000),
      distinctUntilChanged(),
      filter((value) => value !== null),
      switchMap((value) => {
        const pWhere = {};

        assign(pWhere, STATUS_DATASET.get('approve-active'));

        const params = {
          search: value || '',
          sort: 'name:asc',
          where: JSON.stringify(pWhere),
        };
        const options = {
          pagination: false,
        };

        return this.indicatorDatasetService.getQueryList(params, options)
          .result$;
      }),
    ),
    { initialValue: null },
  );

  inputSearchIndicatorConnect$ = new Subject<string>();
  dataSearchIndicatorConnect = toSignal(
    this.inputSearchIndicatorConnect$.pipe(
      debounceTime(1000),
      distinctUntilChanged(),
      filter((value) => value !== null),
      switchMap((value) => {
        const pWhere = {};

        assign(pWhere, STATUS_INDICATOR.get('approve-active'));

        const params = {
          search: value || '',
          sort: 'name:asc',
          where: JSON.stringify(pWhere),
        };
        const options = {
          pagination: false,
        };

        return this.indicatorConnectService.getQueryList(params, options)
          .result$;
      }),
    ),
    { initialValue: null },
  );

  inputSearchIndicatorProgram$ = new Subject<string>();
  dataSearchIndicatorProgram = toSignal(
    this.indicatorProgramService.getQueryList(
      {
        sort: 'nama_program:asc',
      },
      {
        pagination: false,
      },
    ).result$,
    { initialValue: null },
  );

  unitData = toSignal(
    this.unitService
      .getQueryList(
        {
          sort: 'name:asc',
        },
        {
          pagination: false,
        },
      )
      .result$.pipe(
        tap({
          next: (result) => {
            if (result.data) {
              result.data.list.map((item) => (item.from = 'database'));
            }

            return result;
          },
        }),
      ),
    { initialValue: null },
  );

  organizationSatuDataData = toSignal(
    this.organizationService.getQueryList(
      {
        sort: 'nama_skpd:asc',
        where: JSON.stringify({
          is_deleted: false,
        }),
      },
      {
        pagination: false,
      },
      'satudata',
    ).result$,
    { initialValue: null },
  );

  inputSelectOrganizationSub$ = new Subject<string>();
  organizationSubData = toSignal(
    this.inputSelectOrganizationSub$.pipe(
      distinctUntilChanged(),
      filter((value) => value !== null),
      switchMap((value) => {
        return this.organizationSubService.getQueryList(
          {
            sort: 'nama_skpdsub:asc',
            where: JSON.stringify({
              kode_skpd: value,
            }),
          },
          {
            pagination: false,
          },
        ).result$;
      }),
    ),
    { initialValue: null },
  );

  constructor() {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
    this.authenticationService.satudataToken.subscribe(
      (x) => (this.satudataToken = x),
    );

    this.iconService.registerIcons([iconTrash, iconCirclePlus]);

    this.EditorConfig = {
      name: 'file',
      toolbar: [
        'Bold',
        'Italic',
        'Underline',
        'Strikethrough',
        'Subscript',
        'Superscript',
        '|',
        'fontFamily',
        'fontSize',
        'fontColor',
        'fontBackgroundColor',
        '|',
        'imageUpload',
        '|',
        'Undo',
        'Redo',
      ],
      htmlSupport: {
        allow: [
          {
            name: 'span',
            attributes: true,
          },
        ],
      },
      extraPlugins: [CKEditorUploadAdapterPlugin],
      token: this.satudataToken,
    };
  }

  ngOnInit() {
    this.createYear();

    // If create OPD
    if (this.satudataUser.role_name === 'opd') {
      this.getOrganization();
    }
  }

  get f() {
    return this.myForm.controls;
  }

  get fid() {
    return this.f.indicator_dataset as UntypedFormArray;
  }

  get fic() {
    return this.f.indicator_connect as UntypedFormArray;
  }

  get fip() {
    return this.f.indicator_program as UntypedFormArray;
  }

  get fibd() {
    return this.f.indicator_basic_data as UntypedFormArray;
  }

  get fikaw() {
    return this.f.kondisi_awal as UntypedFormArray;
  }

  get fikak() {
    return this.f.kondisi_akhir as UntypedFormArray;
  }

  addUnit = (name: string) => {
    if (size(name) >= 48) {
      this.unitValid = false;
      return null;
    }
    this.unitValid = true;
    return { name, unit: name, from: 'input' };
  };

  // DYNAMIC FORM ============================================================================================
  addDataset() {
    this.fid.push(this.createDataset());
    return false;
  }

  createDataset(): UntypedFormGroup {
    return new UntypedFormGroup({
      dataset: new UntypedFormControl([]),
    });
  }

  removeDataset(i) {
    if (this.fid) {
      this.fid.removeAt(i);
    }
    return false;
  }

  addConnect() {
    this.fic.push(this.createConnect());
    return false;
  }

  createConnect(): UntypedFormGroup {
    return new UntypedFormGroup({
      connect: new UntypedFormControl([]),
    });
  }

  removeConnect(i) {
    if (this.fic) {
      this.fic.removeAt(i);
    }
    return false;
  }

  addProgram() {
    this.fip.push(this.createProgram());
    return false;
  }

  createProgram(): UntypedFormGroup {
    return new UntypedFormGroup({
      program: new UntypedFormControl([]),
    });
  }

  removeProgram(i) {
    if (this.fip) {
      this.fip.removeAt(i);
    }
    return false;
  }

  addBasicData() {
    this.fibd.push(this.createBasicData());
    return false;
  }

  createBasicData(): UntypedFormGroup {
    return new UntypedFormGroup({
      basic_data: new UntypedFormControl({ value: null, disabled: false }, [
        Validators.required,
        Validators.maxLength(255),
      ]),
    });
  }

  removeBasicData(i) {
    if (this.fibd) {
      this.fibd.removeAt(i);
    }
    return false;
  }

  createInitialCondition(): UntypedFormGroup {
    return new UntypedFormGroup({
      year: new UntypedFormControl({ value: null, disabled: false }, [
        Validators.required,
        Validators.maxLength(255),
      ]),
      capaian: new UntypedFormControl({ value: null, disabled: false }, [
        Validators.required,
        Validators.maxLength(255),
      ]),
    });
  }

  createYear() {
    const year = moment().subtract(3, 'years').format('YYYY');

    const yearPlus = moment().add(10, 'years').format('YYYY');

    for (let index = Number(year); index < Number(yearPlus); index += 1) {
      this.yearData.push(index);
    }
  }

  addFinalCondition(): boolean {
    const yearNow = moment().format('YYYY');

    if (isNil(this.yearInitial) === true || isNil(this.yearFinal) === true) {
      Swal.fire({
        type: 'error',
        text: 'Tahun harus diisi.',
        allowOutsideClick: false,
      });
      return false;
    }

    this.myForm.controls.is_kondisi_awal.setValue(true);
    this.removeFinalCondition();

    for (
      let index = Number(this.yearInitial);
      index <= Number(this.yearFinal);
      index += 1
    ) {
      const disabled = index > Number(yearNow);
      this.fikak.push(this.createFinalCondition(index, disabled));
    }

    return true;
  }

  createFinalCondition(index: number, disabled: boolean): UntypedFormGroup {
    let achievement: any;
    if (disabled) {
      achievement = new UntypedFormControl({ value: null, disabled }, [
        Validators.maxLength(255),
      ]);
    } else {
      achievement = new UntypedFormControl({ value: null, disabled }, [
        Validators.required,
        Validators.maxLength(255),
      ]);
    }

    return new UntypedFormGroup({
      year: new UntypedFormControl({ value: index, disabled: false }, [
        Validators.required,
        Validators.maxLength(255),
      ]),
      target: new UntypedFormControl({ value: null, disabled: false }, [
        Validators.required,
        Validators.maxLength(255),
      ]),
      capaian: achievement,
    });
  }

  removeFinalCondition() {
    while (this.fikak.length !== 0) {
      this.fikak.removeAt(0);
    }
  }

  // GET =====================================================================================================
  getOrganization() {
    const kodeSkpd = this.satudataUser.kode_skpd;

    this.myForm.patchValue({
      kode_skpd: kodeSkpd,
    });

    this.getAllOrganizationSub(kodeSkpd);
  }

  getAllOrganizationSub(kodeSkpd: string) {
    this.inputSelectOrganizationSub$.next(kodeSkpd);
  }

  // ONCHANGE ================================================================================================
  onSelectedOrganization(value: string) {
    const kodeSkpd = value;
    this.myForm.controls['kode_skpd_sub'].reset();
    this.getAllOrganizationSub(kodeSkpd);
  }

  onClearOrganization() {
    this.myForm.controls['kode_skpd_sub'].reset();
  }

  onSelectedYearInitial(value: number) {
    this.yearInitial = value;
  }

  onSelectedYearFinal(value: number) {
    this.yearFinal = value;
  }

  // SAVE ====================================================================================================
  onSubmit() {
    const indicator = this.myForm.value;

    const categoryHistory = 'draft-active';

    const {
      indicator_dataset: indicatorDataset,
      indicator_connect: indicatorConnect,
      indicator_program: indicatorProgram,
      indicator_basic_data: indicatorBasicData,
      datetime,
      classification,
      satuan_id: unit,
      kondisi_awal: initialCondition,
      kondisi_akhir: finalCondition,
    } = this.myForm.value;

    delete indicator['indicator_dataset'];
    delete indicator['indicator_connect'];
    delete indicator['indicator_program'];
    delete indicator['indicator_basic_data'];
    delete indicator['datetime'];
    delete indicator['classification'];
    delete indicator['satuan_id'];
    delete indicator['year_initial'];
    delete indicator['year_final'];
    delete indicator['is_kondisi_awal'];
    delete indicator['kondisi_awal'];
    delete indicator['kondisi_akhir'];

    assign(indicator, {
      datetime: moment(datetime).format('YYYY-MM-01'),
    });

    const hasEmpty = (data, field) => {
      return data.some(
        (obj) =>
          obj[field] === null ||
          (Array.isArray(obj[field]) && obj[field].length === 0),
      );
    };

    if (!hasEmpty(indicatorDataset, 'dataset')) {
      assign(indicator, {
        indikator_dataset: indicatorDataset.map((obj: any) => obj.dataset.id),
      });
    }

    if (!hasEmpty(indicatorConnect, 'connect')) {
      assign(indicator, {
        indikator_terhubung: indicatorConnect.map((obj: any) => obj.connect.id),
      });
    }

    if (!hasEmpty(indicatorProgram, 'program')) {
      assign(indicator, {
        indikator_program: indicatorProgram.map(
          (obj: any) => obj.program.id_program,
        ),
      });
    }

    assign(indicator, {
      indikator_data_dasar: indicatorBasicData.map(
        (obj: any) => obj.basic_data,
      ),
    });

    if (unit.from === 'input') {
      const bodyUnit = { name: unit.name };

      this.unitService
        .createItem(bodyUnit)
        .pipe(this.addUnitMutation.track())
        .subscribe((result) => {
          assign(indicator, {
            satuan_id: result.data.id,
          });
        });
    } else {
      assign(indicator, {
        satuan_id: unit.id,
      });
    }

    // TODO: activate if backend history_draft ready
    // History
    // const bodyHistory = {
    //   category: categoryHistory,
    //   notes: null,
    // };
    // assign(indicator, { history_draft: bodyHistory });

    // Status
    assign(indicator, STATUS_INDICATOR.get(categoryHistory));

    this.indicatorService
      .createItem(indicator)
      .pipe(this.addIndicatorMutation.track())
      .subscribe((result) => {
        const indicatorId = result.data.id;

        // Insert Classification History
        const bodyClassificationHistory = classification.map((item: any) => {
          return {
            indikator_id: indicatorId,
            indikator_class_id: item.id,
          };
        });

        const insertClassificationHistory =
          this.indicatorHistoryService.createItem(bodyClassificationHistory);

        // Insert Indicator Progress Initial
        const bodyProgressInitial = initialCondition.map((item: any, index) => {
          return {
            indikator_id: indicatorId,
            year: item.year,
            target: item.target || null,
            capaian: item.capaian || null,
            notes: item.notes,
            index: index + 1,
          };
        });

        const insertProgressInitial =
          this.indicatorProgressInitialService.createItem(bodyProgressInitial);

        // Insert Indicator Progress Final
        const bodyProgressFinal = finalCondition.map((item: any, index) => {
          return {
            indikator_id: indicatorId,
            year: item.year,
            target: item.target || null,
            capaian: item.capaian || null,
            notes: item.notes,
            index: index + 99,
          };
        });

        const insertProgressFinal =
          this.indicatorProgressFinalService.createItem(bodyProgressFinal);

        zip(
          insertClassificationHistory,
          insertProgressInitial,
          insertProgressFinal,
        )
          .pipe(this.addIndicatorMutation.track())
          .subscribe(() => {
            this.router.navigate(['/my-indicator/preview', indicatorId], {
              queryParams: {
                page: 'my-indicator',
                status: categoryHistory,
              },
            });
          });
      });
  }
}
