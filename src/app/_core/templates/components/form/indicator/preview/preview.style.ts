import { Injectable } from '@angular/core';

// PACKAGE
import { css } from '@emotion/css';

@Injectable()
export class PreviewStyle {
  getDynamicStyle(): any {
    const formula = css`
      p {
        margin-bottom: 0;
      }
    `;

    const image = css`
      img {
        max-width: 100%;
        max-height: 100%;
      }
    `;

    return {
      formula,
      image,
    };
  }
}
