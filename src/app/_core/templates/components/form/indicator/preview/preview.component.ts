import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  inject,
} from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { CommonModule } from '@angular/common';
import { RouterModule, ActivatedRoute, Router } from '@angular/router';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { CLASSIFICATION_INDICATOR, STATUS_INDICATOR } from '@constants-v4';
// MODEL
import { LoginData } from '@models-v3';
// PIPE
import { SafeHtmlPipe } from '@pipes-v4';
// SERVICE
import { AuthenticationService } from '@services-v3';
import {
  IndicatorService,
  IndicatorProgressInitialService,
  IndicatorProgressFinalService,
} from '@services-v4';
// JDS-BI
import { JdsBiBadgeModule } from '@jds-bi/core';

import { assign, isNil } from 'lodash';
import moment from 'moment';
import Swal from 'sweetalert2';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgxLoadingModule } from 'ngx-loading';
import { useMutationResult } from '@ngneat/query';
import { SubscribeDirective } from '@ngneat/subscribe';

import { PreviewStyle } from './preview.style';

@Component({
  selector: 'app-form-indicator-preview',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    SafeHtmlPipe,
    JdsBiBadgeModule,
    NgxSkeletonLoaderModule,
    NgxLoadingModule,
    SubscribeDirective,
  ],
  templateUrl: './preview.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [PreviewStyle],
})
export class PreviewComponent implements OnInit {
  satudataUser: LoginData;
  moment: any = moment;
  jds: any = jds;

  className!: any;

  // Service
  private indicatorService = inject(IndicatorService);
  private indicatorProgressInitialService = inject(
    IndicatorProgressInitialService,
  );
  private indicatorProgressFinalService = inject(IndicatorProgressFinalService);
  editIndicatorMutation = useMutationResult();

  // Variable
  id: number;
  param: any;

  // Data
  indicatorData = toSignal(
    this.indicatorService.getQuerySingle(this.route.snapshot.params.id, {
      detail: true,
    }).result$,
    { initialValue: null },
  );

  indicatorProgressInitialData = toSignal(
    this.indicatorProgressInitialService.getQueryListSingle(
      this.route.snapshot.params.id,
      {
        sort: 'index:asc',
        where: JSON.stringify({
          indikator_id: this.route.snapshot.params.id,
          index: 'awal',
        }),
      },
      {
        pagination: false,
      },
    ).result$,
    { initialValue: null },
  );

  indicatorProgressFinalData = toSignal(
    this.indicatorProgressFinalService.getQueryListSingle(
      this.route.snapshot.params.id,
      {
        sort: 'index:asc',
        where: JSON.stringify({
          indikator_id: this.route.snapshot.params.id,
          index: 'akhir',
        }),
      },
      {
        pagination: false,
      },
    ).result$,
    { initialValue: null },
  );

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
    private route: ActivatedRoute,
    private previewStyle: PreviewStyle,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    // id
    this.id = this.route.snapshot.params.id;
  }

  ngOnInit() {
    this.className = this.previewStyle.getDynamicStyle();

    this.queryParams();
  }

  queryParams(): void {
    this.route.queryParams.subscribe((p) => {
      const pPage = !isNil(p['page']) ? p['page'] : null;
      const pStatus = !isNil(p['status']) ? p['status'] : null;

      this.param = {
        page: pPage,
        status: pStatus,
      };
    });
  }

  // GET =====================================================================================================
  getClassification(id: number) {
    return CLASSIFICATION_INDICATOR.get(id);
  }

  // SAVE ====================================================================================================
  onSubmit() {
    const bodyIndicator = {
      id: Number(this.id),
    };

    let categoryHistory;
    switch (this.param.status) {
      case 'draft-active':
        categoryHistory = 'new-active';
        break;
      case 'new-active':
        categoryHistory = null;
        break;
      case 'revision-active-new':
        categoryHistory = 'new-active';
        break;
      case 'revision-active-edit':
        categoryHistory = 'edit-active';
        break;
      case 'approve-active':
        categoryHistory = 'edit-active';
        break;
      case 'edit-active':
        categoryHistory = null;
        break;
      default:
        categoryHistory = null;
        break;
    }

    // TODO: activate if backend history_draft ready
    // // History
    // if (categoryHistory) {
    //   const bodyHistory = {
    //     category: categoryHistory,
    //     notes: null,
    //   };
    //   assign(bodyIndicator, { history_draft: bodyHistory });
    // }

    // Status
    if (categoryHistory) {
      // TODO: activate if backend verification ready
      // assign(bodyIndicator, STATUS_INDICATOR.get(categoryHistory));
      assign(bodyIndicator, STATUS_INDICATOR.get('approve-active'));
    }

    this.indicatorService
      .updateItem(this.id, bodyIndicator)
      .pipe(this.editIndicatorMutation.track())
      .subscribe(() => {
        let from;
        if (this.param.page === 'my-indicator') {
          from = 'ditambahkan';
        } else {
          from = 'diubah';
        }

        // TODO: activate if backend verification ready
        // let queryParams;
        // switch (this.param.status) {
        //   case 'draft-active':
        //   case 'new-active':
        //   case 'revision-active-new':
        //     queryParams = {
        //       type: 'upload-change',
        //       status: 'verification',
        //     };
        //     break;
        //   case 'approve-active':
        //   case 'edit-active':
        //   case 'revision-active-edit':
        //     queryParams = {
        //       type: 'upload-change',
        //       status: 'verification',
        //       statusVerification: 'edit',
        //     };
        //     break;
        //   default:
        //     break;
        // }

        Swal.fire({
          type: 'success',
          text: `Indikator berhasil ${from}.`,
          allowOutsideClick: false,
        }).then(() => {
          // TODO: activate if backend verification ready
          // this.router.navigate(['/desk'], {
          //   queryParams,
          // });
          this.router.navigate(['/my-data'], {
            queryParams: { catalog: 'indicator' },
          });
        });
      });
  }
}
