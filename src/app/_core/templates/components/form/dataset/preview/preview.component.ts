import {
  Component,
  OnDestroy,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, ActivatedRoute, Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { Observable, Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';
import { Actions, ofType } from '@ngrx/effects';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { STATUS_DATASET } from '@constants-v4';
// MODEL
import { LoginData, DatasetData, MetadataDatasetData } from '@models-v3';
// SERVICE
import { AuthenticationService, DatasetService } from '@services-v3';
// PIPE
import { SafeHtmlPipe } from '@pipes-v4';

// STORE
import {
  selectDataset,
  selectIsLoadingRead as selectIsLoadingReadDataset,
  selectIsLoadingUpdate as selectIsLoadingUpdateDataset,
  selectError as selectErrorDataset,
} from '@store-v3/dataset/dataset.selectors';
import { fromDatasetActions } from '@store-v3/dataset/dataset.actions';

import {
  selectAllMetadataBusinessDataset,
  selectError as selectErrorMetadataBusinessDataset,
} from '@store-v3/metadata-business-dataset/metadata-business-dataset.selectors';
import { fromMetadataBusinessDatasetActions } from '@store-v3/metadata-business-dataset/metadata-business-dataset.actions';

import {
  selectAllMetadataCustomDataset,
  selectError as selectErrorMetadataCustomDataset,
} from '@store-v3/metadata-custom-dataset/metadata-custom-dataset.selectors';
import { fromMetadataCustomDatasetActions } from '@store-v3/metadata-custom-dataset/metadata-custom-dataset.actions';

import { ToastrService } from 'ngx-toastr';

import { assign, findIndex, isNil } from 'lodash';
import moment from 'moment';
import Swal from 'sweetalert2';
import { NgxLoadingModule } from 'ngx-loading';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

@Component({
  selector: 'app-form-dataset-preview',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    NgxLoadingModule,
    NgxSkeletonLoaderModule,
    SafeHtmlPipe,
  ],
  templateUrl: './preview.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreviewComponent implements OnInit, OnDestroy {
  satudataUser: LoginData;
  moment: any = moment;
  jds: any = jds;

  // Variable
  id: number;
  filter = {
    from: 'create',
  };

  // Data
  datasetData$: Observable<DatasetData>;
  datasetIsLoadingRead$: Observable<boolean>;
  datasetIsLoadingUpdate$: Observable<boolean>;
  datasetIsError: boolean;

  dataAccess: any;
  dataAccessIsError: boolean;

  metadataBusinessData: MetadataDatasetData[];
  metadataBusinessData$: Observable<MetadataDatasetData[]>;
  metadataBusinessIsLoadingList$: Observable<boolean>;
  metadataBusinessIsError$: Observable<boolean>;
  metadataBusinessIsError: boolean;
  metadataBusinessErrorMessage: string;

  metadataCustomData$: Observable<MetadataDatasetData[]>;
  metadataCustomIsLoadingList$: Observable<boolean>;
  metadataCustomIsError$: Observable<boolean>;
  metadataCustomIsError: boolean;
  metadataCustomErrorMessage: string;

  private unsubscribeDataset$ = new Subject<void>();

  constructor(
    private cdRef: ChangeDetectorRef,
    private authenticationService: AuthenticationService,
    private datasetService: DatasetService,
    private router: Router,
    private route: ActivatedRoute,
    private actions$: Actions,
    private store: Store<any>,
    private sanitizer: DomSanitizer,
    private toastr: ToastrService,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    // id
    this.id = this.route.snapshot.params.id;
  }

  ngOnInit() {
    this.queryParams();

    this.getDataset();
  }

  ngOnDestroy() {
    this.unsubscribeDataset$.next();
    this.unsubscribeDataset$.complete();
  }

  queryParams(): void {
    this.route.queryParams.subscribe((p) => {
      const pFrom = p.from;

      this.filter.from = !isNil(pFrom) ? pFrom : 'create';
    });
  }

  // GET =====================================================================================================
  getDataset() {
    const params = {
      detail: true,
    };

    this.store.dispatch(
      fromDatasetActions.loadDataset({
        id: this.id,
        params,
      }),
    );

    this.datasetIsLoadingRead$ = this.store.pipe(
      select(selectIsLoadingReadDataset),
    );

    this.store
      .pipe(
        select(selectErrorDataset),
        filter((val) => val !== null && val.error),
      )
      .subscribe((result) => {
        if (!result.error) {
          this.datasetIsError = false;
        } else {
          this.datasetIsError = true;
          this.toastr.error(result.message, 'Dataset');
        }
      });

    this.datasetData$ = this.store.pipe(select(selectDataset(this.id)));
  }

  getAllMetadataBusinessDataset(): void {
    const pWhere = {
      dataset_id: this.id,
      metadata_type_id: 1,
    };

    const params = {
      where: JSON.stringify(pWhere),
    };

    this.store.dispatch(
      fromMetadataBusinessDatasetActions.loadAllMetadataBusinessDataset({
        params,
        pagination: false,
        infinite: false,
      }),
    );

    this.store
      .pipe(
        select(selectErrorMetadataBusinessDataset),
        filter((val) => val !== null && val.error),
      )
      .subscribe((result) => {
        if (!result.error) {
          this.metadataBusinessIsError = false;
        } else {
          this.metadataBusinessIsError = true;
          this.toastr.error(result.message, 'Metadata Business Dataset');
        }
      });

    this.store
      .pipe(
        select(selectAllMetadataBusinessDataset),
        filter((val) => val.length !== 0),
      )
      .subscribe((result) => {
        this.metadataBusinessData = result;

        const findVersionIndex = findIndex(this.metadataBusinessData, (res) => {
          return res.key === 'Versi' && !res.value;
        });

        if (findVersionIndex >= 0) {
          this.metadataBusinessData.splice(findVersionIndex, 1);
        }

        this.cdRef.detectChanges();
      });
  }

  getAllMetadataCustomDataset(): void {
    const pWhere = {
      dataset_id: this.id,
      metadata_type_id: 4,
    };

    const params = {
      where: JSON.stringify(pWhere),
    };

    this.store.dispatch(
      fromMetadataCustomDatasetActions.loadAllMetadataCustomDataset({
        params,
        pagination: false,
        infinite: false,
      }),
    );

    this.store
      .pipe(
        select(selectErrorMetadataCustomDataset),
        filter((val) => val !== null && val.error),
      )
      .subscribe((result) => {
        if (!result.error) {
          this.metadataCustomIsError = false;
        } else {
          this.metadataCustomIsError = true;
          this.toastr.error(result.message, 'Metadata Custom Dataset');
        }
      });

    this.metadataCustomData$ = this.store.pipe(
      select(selectAllMetadataCustomDataset),
    );
  }

  getSource(isRealtime: boolean, isPermanent: boolean) {
    return this.datasetService.getSource(isRealtime, isPermanent);
  }

  getMetadata(metadata: any) {
    return this.datasetService.getMetadata(metadata, true);
  }

  // SAVE ====================================================================================================
  onSubmit() {
    const bodyDataset = {
      id: Number(this.id),
    };

    const bodyHistory = {
      type: 'dataset',
      type_id: Number(this.id),
      notes: null,
    };

    switch (this.filter.from) {
      case 'create':
      case 'draft':
      case 'verification':
      case 'revision':
        assign(bodyDataset, STATUS_DATASET.get('new-active'));
        assign(bodyHistory, { category: 'waiting' });
        break;
      default:
        assign(bodyDataset, STATUS_DATASET.get('edit-active'));
        assign(bodyHistory, { category: 'edit' });
        break;
    }
    assign(bodyDataset, { history_draft: bodyHistory });

    const params = {};

    this.store.dispatch(
      fromDatasetActions.updateDataset({
        update: bodyDataset,
        params,
      }),
    );

    this.datasetIsLoadingUpdate$ = this.store.pipe(
      select(selectIsLoadingUpdateDataset),
    );

    this.actions$
      .pipe(
        ofType(fromDatasetActions.updateDatasetSuccess),
        takeUntil(this.unsubscribeDataset$),
      )
      .subscribe(() => {
        const from =
          this.filter.from === 'create' || this.filter.from === 'draft'
            ? 'ditambahkan'
            : 'diubah';

        Swal.fire({
          type: 'success',
          text: `Dataset berhasil ${from}.`,
          allowOutsideClick: false,
        }).then(() => {
          switch (this.filter.from) {
            case 'create':
            case 'draft':
            case 'revision':
              this.router.navigate(['/desk'], {
                queryParams: {
                  type: 'upload-change',
                  status: 'verification',
                },
              });
              break;
            case 'verification':
            case 'verification-edit':
              this.router.navigate(['/dataset/detail', this.id]);
              break;
            default:
              this.router.navigate(['/desk'], {
                queryParams: {
                  type: 'upload-change',
                  status: 'verification',
                  statusVerification: 'edit',
                },
              });
              break;
          }
        });
      });

    this.actions$
      .pipe(
        ofType(fromDatasetActions.updateDatasetFailure),
        takeUntil(this.unsubscribeDataset$),
      )
      .subscribe((result) => {
        this.toastr.error(result.error.message, 'Dataset');
      });
  }
}
