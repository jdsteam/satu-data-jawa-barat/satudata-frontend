/* eslint-disable @typescript-eslint/dot-notation */
import {
  Component,
  OnInit,
  Renderer2,
  OnDestroy,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  AfterViewInit,
} from '@angular/core';
import { CommonModule, ViewportScroller } from '@angular/common';
import { RouterModule, ActivatedRoute, Router } from '@angular/router';
import {
  FormsModule,
  ReactiveFormsModule,
  UntypedFormGroup,
  UntypedFormControl,
  UntypedFormArray,
  Validators,
} from '@angular/forms';
import { Observable, Subject, of, concat } from 'rxjs';
import {
  filter,
  takeUntil,
  distinctUntilChanged,
  tap,
  switchMap,
  catchError,
  debounceTime,
  take,
  delay,
} from 'rxjs/operators';
import { Store, select } from '@ngrx/store';
import { Actions, ofType } from '@ngrx/effects';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME, WHATSAPP, STATUS_DATASET } from '@constants-v4';
// SERVICE
import {
  ScriptService,
  AuthenticationService,
  DatasetService,
  CoreDataService,
  OrganizationService,
  OrganizationStructureService,
  SchemaService,
  TableService,
} from '@services-v3';
import { DatasetTagsService } from '@services-v4';

// MODEL
import {
  LoginData,
  DatasetData,
  LicenseData,
  TopicData,
  OrganizationData,
  OrganizationSubData,
  OrganizationUnitData,
  ClassificationDatasetData,
  ApplicationData,
  ApplicationServiceData,
  SchemaData,
  TableData,
  BusinessFieldData,
} from '@models-v3';

// COMPONENT
import { AccessControlComponent as FormAccessControlComponent } from '@components-v3/form/access-control/access-control.component';
import { JdsBiAccordionModule, JdsBiInputModule } from '@jds-bi/core';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconTrash,
  iconCirclePlus,
  iconCircleInfo,
  iconUserLock,
  iconBan,
  iconClockRotateLeft,
} from '@jds-bi/icons';

// STORE
import {
  selectDataset,
  selectIsLoadingRead as selectIsLoadingReadDataset,
  selectIsLoadingUpdate as selectIsLoadingUpdateDataset,
  selectError as selectErrorDataset,
} from '@store-v3/dataset/dataset.selectors';
import { fromDatasetActions } from '@store-v3/dataset/dataset.actions';

import {
  selectAllLicense,
  selectIsLoadingList as selectIsLoadingListLicense,
  selectError as selectErrorLicense,
} from '@store-v3/license/license.selectors';
import { fromLicenseActions } from '@store-v3/license/license.actions';

import {
  selectAllTopic,
  selectIsLoadingList as selectIsLoadingListTopic,
  selectError as selectErrorTopic,
} from '@store-v3/topic/topic.selectors';
import { fromTopicActions } from '@store-v3/topic/topic.actions';

import {
  selectAllOrganization,
  selectIsLoadingList as selectIsLoadingListOrganization,
  selectError as selectErrorOrganization,
} from '@store-v3/organization/organization.selectors';
import { fromOrganizationActions } from '@store-v3/organization/organization.actions';

import {
  selectAllOrganizationSub,
  selectIsLoadingList as selectIsLoadingListOrganizationSub,
  selectError as selectErrorOrganizationSub,
} from '@store-v3/organization-sub/organization-sub.selectors';
import { fromOrganizationSubActions } from '@store-v3/organization-sub/organization-sub.actions';

import {
  selectAllOrganizationUnit,
  selectIsLoadingList as selectIsLoadingListOrganizationUnit,
  selectError as selectErrorOrganizationUnit,
} from '@store-v3/organization-unit/organization-unit.selectors';
import { fromOrganizationUnitActions } from '@store-v3/organization-unit/organization-unit.actions';

import {
  selectAllClassificationDataset,
  selectIsLoadingList as selectIsLoadingListClassificationDataset,
  selectError as selectErrorClassificationDataset,
} from '@store-v3/classification-dataset/classification-dataset.selectors';
import { fromClassificationDatasetActions } from '@store-v3/classification-dataset/classification-dataset.actions';

import { BusinessFieldFacade } from '@store-v3/business-field/business-field.facade';

import { ToastrService } from 'ngx-toastr';

// PLUGIN
import {
  assign,
  each,
  filter as lfilter,
  find,
  findIndex,
  flattenDeep,
  forEach,
  isNil,
  map,
  reject,
  size,
} from 'lodash';
import moment from 'moment';
import { NgxLoadingModule } from 'ngx-loading';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import * as CKEditor from '@scripts/ckeditor-5/ckeditor';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxPopperjsModule } from 'ngx-popperjs';

// eslint-disable-next-line @typescript-eslint/naming-convention
declare const x_spreadsheet: any;
declare const XLSX: any;

@Component({
  selector: 'app-form-dataset-update',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    FormAccessControlComponent,
    JdsBiAccordionModule,
    JdsBiInputModule,
    JdsBiIconsModule,
    NgxLoadingModule,
    CKEditorModule,
    NgSelectModule,
    NgxPopperjsModule,
  ],
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdateComponent implements OnInit, OnDestroy, AfterViewInit {
  colorScheme = COLORSCHEME;
  satudataUser: LoginData;
  whatsapp = WHATSAPP;
  moment = moment;
  jds = jds;
  public Editor = CKEditor;

  // Variable
  id: number;
  filter = {
    from: 'create',
  };
  myForm: UntypedFormGroup;
  isShowAccess = false;
  redirect: any;
  dataJson: any;
  isPeriod = false;
  isRealtime = false;
  isClassificationDataset = false;

  EditorConfig = {
    toolbar: [
      'Bold',
      'Italic',
      'Underline',
      'Strikethrough',
      '|',
      'Link',
      '|',
      'Outdent',
      'Indent',
      '|',
      'BulletedList',
      'NumberedList',
      '|',
      'Undo',
      'Redo',
    ],
    htmlSupport: {
      allow: [
        {
          name: 'span',
          attributes: true,
        },
      ],
    },
  };

  // Data
  datasetData: DatasetData;
  datasetData$: Observable<DatasetData>;
  datasetIsLoadingRead$: Observable<boolean>;
  datasetIsLoadingUpdate$: Observable<boolean>;
  datasetIsError: boolean;

  tagInputIsLoading = false;
  tagInput$ = new Subject<string>();
  tagInputData$: any;
  tagInputValid = true;

  organizationInputIsLoading = false;
  organizationInput$ = new Subject<string>();
  organizationInputData$: any;
  organizationInputValid = true;

  licenseData$: Observable<LicenseData[]>;
  licenseIsLoadingList$: Observable<boolean>;
  licenseIsError: boolean;

  topicData$: Observable<TopicData[]>;
  topicIsLoadingList$: Observable<boolean>;
  topicIsError: boolean;

  organizationData$: Observable<OrganizationData[]>;
  organizationIsLoadingList$: Observable<boolean>;
  organizationIsError: boolean;

  organizationSubData$: Observable<OrganizationSubData[]>;
  organizationSubIsLoadingList$: Observable<boolean>;
  organizationSubIsError: boolean;

  organizationUnitData$: Observable<OrganizationUnitData[]>;
  organizationUnitIsLoadingList$: Observable<boolean>;
  organizationUnitIsError: boolean;

  classificationDatasetData$: Observable<ClassificationDatasetData[]>;
  classificationDatasetIsLoadingList$: Observable<boolean>;
  classificationDatasetIsError: boolean;

  businessFieldData$: Observable<BusinessFieldData[]>;
  businessFieldIsLoadingList$: Observable<boolean>;
  businessFieldIsError: boolean;

  myOrganizationData: OrganizationData;

  organizationStructureData = [];
  organizationStructureIsLoadingList = false;
  selectedPositions: any;

  applicationData$: Observable<ApplicationData[]>;
  applicationIsLoadingList$: Observable<boolean>;
  applicationIsError: boolean;

  applicationServiceData$: Observable<ApplicationServiceData[]>;
  applicationServiceIsLoadingList$: Observable<boolean>;
  applicationServiceIsError: boolean;
  applicationServiceOption = [];

  dataAccess: any;
  oldData: any;
  oldDataAccess: any;
  dataAccessIsError: boolean;
  isDataAccessFirst = true;

  loadingAccess = false;
  accessBigData = false;
  accessinfoBigData: string;
  urlCoreData: string;
  urlFile: string;
  totalRecord: number;

  schemaData: SchemaData[] = [];
  tableData: TableData[] = [];

  categoryDatasetData = [
    'Data Master',
    'Data Referensi',
    'Data Agregat',
    'Data Transaksi',
    'Data Realtime',
    'Data Indikator',
  ];
  frequencyDatasetData = [
    'Tiga Tahunan',
    'Tahunan',
    'Semesteran',
    'Triwulan',
    'Bulanan',
    'Mingguan',
    'Harian',
  ];
  dimensionDatasetStartData = [];
  dimensionDatasetEndData = [];
  metadataData = [
    { key: 'Pengukuran Dataset', field: 'pengukuran_dataset' },
    { key: 'Tingkat Penyajian Dataset', field: 'tingkat_penyajian_dataset' },
    { key: 'Cakupan Dataset', field: 'cakupan_dataset' },
    { key: 'Produsen', field: 'produsen' },
    { key: 'Bidang', field: 'bidang' },
    { key: 'Penanggung Jawab', field: 'penanggung_jawab' },
    { key: 'Kontak Produsen', field: 'kontak_produsen' },
    { key: 'Kode Indikator', field: 'kode_indikator' },
    { key: 'Bidang Urusan', field: 'bidang_urusan' },
    { key: 'Satuan Dataset', field: 'satuan_dataset' },
    { key: 'Frekuensi Dataset', field: 'frekuensi_dataset' },
    { key: 'Dimensi Dataset Awal', field: 'dimensi_dataset_awal' },
    { key: 'Dimensi Dataset Akhir', field: 'dimensi_dataset_akhir' },
    { key: 'Sumber External', field: 'sumber_external' },
  ];

  spreadsheet: any;
  spreadsheetConfig = {
    num_row: 60000,
    num_col: 52,
  };
  spreadsheetIsLoading = false;
  spreadsheetMode = 'edit';

  coredata: any[];
  metadata: any[];
  coreDataIsError: boolean;

  private unsubscribeDataset$ = new Subject<void>();

  constructor(
    private cdRef: ChangeDetectorRef,
    private viewportScroller: ViewportScroller,
    private authenticationService: AuthenticationService,
    private scriptService: ScriptService,
    private datasetTagsService: DatasetTagsService,
    private datasetService: DatasetService,
    private organizationService: OrganizationService,
    private organizationStructureService: OrganizationStructureService,
    private schemaService: SchemaService,
    private tableService: TableService,
    private businessFieldFacade: BusinessFieldFacade,
    private coreDataService: CoreDataService,
    private renderer: Renderer2,
    private router: Router,
    private route: ActivatedRoute,
    private actions$: Actions,
    private store: Store<any>,
    private iconService: JdsBiIconsService,
    private toastr: ToastrService,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    // id
    this.id = this.route.snapshot.params.id;

    this.iconService.registerIcons([
      iconBan,
      iconCircleInfo,
      iconCirclePlus,
      iconClockRotateLeft,
      iconTrash,
      iconUserLock,
    ]);

    // Dimension Dataset Data
    const startYear = new Date().getFullYear();
    for (let i = startYear; i >= 2000; i -= 1) {
      this.dimensionDatasetStartData.push(i);
    }
  }

  ngOnInit() {
    this.myForm = new UntypedFormGroup({
      name: new UntypedFormControl({ value: null, disabled: false }, [
        Validators.required,
        Validators.maxLength(255),
      ]),
      description: new UntypedFormControl({ value: null, disabled: false }, [
        Validators.required,
      ]),
      tags: new UntypedFormControl([], [Validators.required]),
      license_id: new UntypedFormControl({ value: null, disabled: false }),
      sektoral_id: new UntypedFormControl({ value: null, disabled: false }, [
        Validators.required,
      ]),
      category: new UntypedFormControl({ value: null, disabled: false }, [
        Validators.required,
      ]),
      kode_skpd: new UntypedFormControl({ value: null, disabled: false }, [
        Validators.required,
      ]),
      kode_skpdsub: new UntypedFormControl({ value: null, disabled: false }),
      kode_skpdunit: new UntypedFormControl({ value: null, disabled: false }),
      kode_indikator: new UntypedFormControl({ value: null, disabled: false }, [
        Validators.maxLength(255),
      ]),
      kode_bidang_urusan: new UntypedFormControl(
        { value: null, disabled: false },
        [Validators.required],
      ),
      dataset_class_id: new UntypedFormControl(
        { value: null, disabled: false },
        [Validators.required],
      ),
      access_control: new UntypedFormControl([]),
      jabatan_access: new UntypedFormArray([]),
      is_realtime: new UntypedFormControl({ value: null, disabled: true }, [
        Validators.required,
      ]),
      is_permanent: new UntypedFormControl({ value: null, disabled: false }, [
        Validators.required,
      ]),
      period: new UntypedFormControl({ value: null, disabled: false }),
      schema: new UntypedFormControl({ value: null, disabled: false }),
      table: new UntypedFormControl({ value: null, disabled: false }),
      // metadata
      pengukuran_dataset: new UntypedFormControl(
        { value: null, disabled: false },
        [Validators.required, Validators.maxLength(255)],
      ),
      tingkat_penyajian_dataset: new UntypedFormControl(
        { value: null, disabled: false },
        [Validators.required, Validators.maxLength(255)],
      ),
      cakupan_dataset: new UntypedFormControl(
        { value: null, disabled: false },
        [Validators.required, Validators.maxLength(255)],
      ),
      produsen: new UntypedFormControl({ value: null, disabled: false }, [
        Validators.required,
        Validators.maxLength(255),
      ]),
      bidang: new UntypedFormControl({ value: null, disabled: false }, [
        Validators.maxLength(255),
      ]),
      penanggung_jawab: new UntypedFormControl(
        { value: null, disabled: false },
        [Validators.maxLength(255)],
      ),
      kontak_produsen: new UntypedFormControl(
        { value: null, disabled: false },
        [Validators.required, Validators.maxLength(255)],
      ),
      satuan_dataset: new UntypedFormControl({ value: null, disabled: false }, [
        Validators.required,
        Validators.maxLength(255),
      ]),
      bidang_urusan: new UntypedFormControl({ value: null, disabled: false }, [
        Validators.required,
        Validators.maxLength(255),
      ]),
      frekuensi_dataset: new UntypedFormControl(
        { value: null, disabled: false },
        [Validators.required, Validators.maxLength(255)],
      ),
      dimensi_dataset_awal: new UntypedFormControl(
        { value: null, disabled: false },
        [Validators.required, Validators.maxLength(255)],
      ),
      dimensi_dataset_akhir: new UntypedFormControl(
        { value: null, disabled: false },
        [Validators.required, Validators.maxLength(255)],
      ),
      sumber_external: new UntypedFormControl(
        { value: null, disabled: false },
        [Validators.maxLength(255)],
      ),
      metadata: new UntypedFormArray([]),
      // history_draft edit
      change: new UntypedFormControl({ value: null, disabled: false }),
    });

    // this.initSpreadsheet();

    this.queryParams();

    this.getInputTag();
    this.getInputOrganization();

    this.getAllLicense();
    this.getAllTopic();
    this.getAllOrganization();
    this.getAllClassificationDataset();
    this.getAllBusinessField();

    const scriptElement = this.scriptService.loadJSScript(
      this.renderer,
      'https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.18.5/xlsx.full.min.js',
    );

    scriptElement.onload = () => {
      this.getDataset();
    };
  }

  ngOnDestroy() {
    this.unsubscribeDataset$.next();
    this.unsubscribeDataset$.complete();
  }

  ngAfterViewInit(): void {
    this.initSpreadsheet();
  }

  queryParams(): void {
    this.route.queryParams.subscribe((p) => {
      const pFrom = p.from;

      this.filter.from = !isNil(pFrom) ? pFrom : 'create';
    });
  }

  // GET =======================================================================
  get f() {
    return this.myForm.controls;
  }

  get fdm() {
    return this.f.metadata as UntypedFormArray;
  }

  get fdp() {
    return this.f.jabatan_access as UntypedFormArray;
  }

  get isDatasetChange() {
    const data = this.datasetData;

    if (!data) {
      return false;
    }

    let change: boolean;
    if (
      (data.is_validate === 2 || data.is_validate === 3) &&
      data.is_active &&
      !data.is_deleted
    ) {
      change = true;
    } else {
      change = false;
    }
    return change;
  }

  initSpreadsheet() {
    this.spreadsheet = x_spreadsheet('#input-spreadsheet', {
      mode: this.spreadsheetMode,
      showToolbar: false,
      showGrid: true,
      showContextmenu: true,
      view: {
        height: () => document.documentElement.clientHeight - 22,
        width: () =>
          document.getElementById('form-dataset-update').clientWidth - 80,
      },
      row: {
        len: this.spreadsheetConfig.num_row,
        height: 25,
      },
      col: {
        len: this.spreadsheetConfig.num_col,
        width: 100,
        indexWidth: 60,
        minWidth: 60,
      },
      style: {
        bgcolor: '#ffffff',
        align: 'left',
        valign: 'middle',
        textwrap: false,
        strike: false,
        underline: false,
        color: '#0a0a0a',
        font: {
          name: 'Helvetica',
          size: 10,
          bold: false,
          italic: false,
        },
      },
    });
  }

  addTagFn = (name: string) => {
    if (size(name) >= 48) {
      this.tagInputValid = false;
      return null;
    }
    this.tagInputValid = true;
    return { name, tag: name };
  };

  getInputTag() {
    this.tagInputData$ = concat(
      this.tagInput$.pipe(
        debounceTime(1000),
        distinctUntilChanged(),
        tap(() => (this.tagInputIsLoading = true)),
        switchMap((term) =>
          this.datasetTagsService.searchItem(term).pipe(
            catchError(() => of([])),
            tap(() => (this.tagInputIsLoading = false)),
          ),
        ),
      ),
    );
  }

  getInputOrganization() {
    this.organizationInputData$ = concat(
      this.organizationInput$.pipe(
        debounceTime(1000),
        distinctUntilChanged(),
        tap(() => (this.organizationInputIsLoading = true)),
        switchMap((term) =>
          this.organizationStructureService
            .getSearchItem({
              search: term || '',
              sort: `satuan_kerja_nama:asc`,
              where: JSON.stringify({
                level: '0',
              }),
            })
            .pipe(
              catchError(() => of([])),
              tap(() => (this.organizationInputIsLoading = false)),
            ),
        ),
      ),
    );
  }

  getDataset() {
    this.datasetIsLoadingRead$ = this.store.pipe(
      select(selectIsLoadingReadDataset),
    );

    this.datasetData$ = this.store.pipe(select(selectDataset(this.id)));

    this.store
      .pipe(
        select(selectErrorDataset),
        filter((val) => val !== null && val.error),
      )
      .subscribe((result) => {
        if (!result.error) {
          this.datasetIsError = false;
        } else {
          this.datasetIsError = true;
          this.toastr.error(result.message, 'Dataset');
        }
      });

    this.store
      .pipe(
        takeUntil(this.unsubscribeDataset$),
        select(selectDataset(this.id)),
        filter((val) => val !== undefined),
        take(1),
      )
      .subscribe((result) => {
        this.datasetData = result;

        this.setFormDataset(result);
        this.setFormMetadataBusiness(result.metadata);
        this.setFormMetadataCustom(result.metadata);

        const { schema, table } = result;
        this.urlCoreData = `/${schema}/${table}`;
        this.urlFile = `${environment.backendURL}bigdata/${schema}/${table}`;

        const canAccess = result.can_access_new;

        if (canAccess) {
          this.accessBigData = true;
          this.getCoreData();
        } else {
          this.accessBigData = false;

          if (
            result.can_access_new_info.message === 'Schema and Table not found.'
          ) {
            this.accessinfoBigData = 'not_found';
          } else {
            this.accessinfoBigData = 'can_access';
          }
        }

        if (this.isDatasetChange) {
          this.myForm
            .get('change')
            .setValidators([Validators.required, Validators.minLength(15)]);
          this.myForm.get('change').updateValueAndValidity();
        }
      });
  }

  getCoreData() {
    const params1 = {
      count: true,
    };

    this.loadingAccess = true;
    this.coreDataService
      .getListV2(this.urlCoreData, params1)
      .toPromise()
      .then(
        async (result1) => {
          this.loadingAccess = false;
          this.accessBigData = true;
          this.totalRecord = result1.meta.total_record;

          const colOld = this.spreadsheetConfig.num_col;
          const colNew = result1.metadata.length;
          const rowOld = this.spreadsheetConfig.num_row;
          const rowNew = result1.meta.total_record + 1;

          this.spreadsheetConfig.num_col =
            colNew < colOld ? colOld : colNew + 1;
          this.spreadsheetConfig.num_row =
            rowNew < rowOld ? rowOld : rowNew + 1;

          const params2: any = {
            dataset_id: this.id,
            limit: this.totalRecord,
            download: 'csv',
            timestamp: new Date().valueOf(),
          };

          const merge = new URLSearchParams(params2);
          const url = `${this.urlFile}?${merge}`;

          const oReq = new XMLHttpRequest();
          oReq.open('GET', url, true);
          oReq.setRequestHeader('Cache-Control', 'no-cache');
          oReq.setRequestHeader(
            'Authorization',
            `Bearer ${this.satudataUser.jwt}`,
          );
          oReq.responseType = 'arraybuffer';

          oReq.onload = () => {
            const { response } = oReq;
            const data = new Uint8Array(response);
            const arr = [];
            for (let i = 0; i !== data.length; i += 1) {
              arr[i] = String.fromCharCode(data[i]);
            }
            const bstr = arr.join('');

            const out = [];
            const workbook = XLSX.read(bstr, {
              type: 'binary',
              raw: true,
              cellText: true,
              WTF: true,
            });
            workbook.SheetNames.forEach((name) => {
              const o = { name, rows: {} };
              const ws = workbook.Sheets[name];
              const aoa = XLSX.utils.sheet_to_json(ws, {
                raw: false,
                header: 1,
              });

              aoa.forEach((r, i) => {
                const cells = {};
                r.forEach((c, j) => {
                  cells[j] = { text: c };
                });
                o.rows[i] = { cells };
              });
              out.push(o);
            });

            this.spreadsheet.loadData(out);
          };

          oReq.send();
        },
        (error) => {
          this.loadingAccess = false;
          this.accessBigData = false;
          this.accessinfoBigData = 'not_found';
          throw new Error(error);
        },
      );
  }

  getAllLicense() {
    const pSort = `name:asc`;

    const params = {
      sort: pSort,
    };

    this.store.dispatch(
      fromLicenseActions.loadAllLicense({
        params,
        pagination: false,
        infinite: false,
      }),
    );

    this.licenseIsLoadingList$ = this.store.pipe(
      select(selectIsLoadingListLicense),
    );

    this.store
      .pipe(
        select(selectErrorLicense),
        filter((val) => val !== null && val.error),
      )
      .subscribe((result) => {
        if (!result.error) {
          this.licenseIsError = false;
        } else {
          this.licenseIsError = true;
          this.toastr.error(result.message, 'License');
        }
      });

    this.licenseData$ = this.store.pipe(
      select(selectAllLicense),
      filter((val) => val.length !== 0),
    );
  }

  getAllTopic() {
    const pSort = `name:asc`;

    const pWhere = {
      is_deleted: false,
    };

    const params = {
      sort: pSort,
      where: JSON.stringify(pWhere),
    };

    this.store.dispatch(
      fromTopicActions.loadAllTopic({
        params,
        pagination: false,
      }),
    );

    this.topicIsLoadingList$ = this.store.pipe(
      select(selectIsLoadingListTopic),
    );

    this.store
      .pipe(
        select(selectErrorTopic),
        filter((val) => val !== null && val.error),
      )
      .subscribe((result) => {
        if (!result.error) {
          this.topicIsError = false;
        } else {
          this.topicIsError = true;
          this.toastr.error(result.message, 'Topic');
        }
      });

    this.topicData$ = this.store.pipe(
      select(selectAllTopic),
      filter((val) => val.length !== 0),
    );
  }

  getAllOrganization() {
    const pSort = `nama_skpd:asc`;

    const pWhere = {
      is_satudata: true,
      is_deleted: false,
    };

    const params = {
      sort: pSort,
      where: JSON.stringify(pWhere),
    };

    this.store.dispatch(
      fromOrganizationActions.loadAllOrganization({
        params,
        pagination: false,
        infinite: false,
      }),
    );

    this.organizationIsLoadingList$ = this.store.pipe(
      select(selectIsLoadingListOrganization),
    );

    this.store
      .pipe(
        select(selectErrorOrganization),
        filter((val) => val !== null && val.error),
      )
      .subscribe((result) => {
        if (!result.error) {
          this.organizationIsError = false;
        } else {
          this.organizationIsError = true;
          this.toastr.error(result.message, 'Organization');
        }
      });

    this.organizationData$ = this.store.pipe(
      select(selectAllOrganization),
      filter((val) => val.length !== 0),
    );
  }

  getAllOrganizationSub(kodeSkpd: string) {
    const pSort = `nama_skpdsub:asc`;

    const pWhere = {
      kode_skpd: kodeSkpd,
    };

    const params = {
      sort: pSort,
      where: JSON.stringify(pWhere),
    };

    this.store.dispatch(
      fromOrganizationSubActions.loadAllOrganizationSub({
        params,
        pagination: false,
        infinite: false,
      }),
    );

    this.organizationSubIsLoadingList$ = this.store.pipe(
      select(selectIsLoadingListOrganizationSub),
    );

    this.store
      .pipe(
        select(selectErrorOrganizationSub),
        filter((val) => val !== null && val.error),
      )
      .subscribe((result) => {
        if (!result.error) {
          this.organizationSubIsError = false;
        } else {
          this.organizationSubIsError = true;
          this.toastr.error(result.message, 'Organization Sub');
        }
      });

    this.organizationSubData$ = this.store.pipe(
      select(selectAllOrganizationSub),
      filter((val) => val.length !== 0),
    );
  }

  getAllOrganizationUnit(kodeSkpdSub: string) {
    const pSort = `nama_skpdunit:asc`;

    const pWhere = {
      kode_skpdsub: kodeSkpdSub,
    };

    const params = {
      sort: pSort,
      where: JSON.stringify(pWhere),
    };

    this.store.dispatch(
      fromOrganizationUnitActions.loadAllOrganizationUnit({
        params,
        pagination: false,
        infinite: false,
      }),
    );

    this.organizationUnitIsLoadingList$ = this.store.pipe(
      select(selectIsLoadingListOrganizationUnit),
    );

    this.store
      .pipe(
        select(selectErrorOrganizationUnit),
        filter((val) => val !== null && val.error),
      )
      .subscribe((result) => {
        if (!result.error) {
          this.organizationUnitIsError = false;
        } else {
          this.organizationUnitIsError = true;
          this.toastr.error(result.message, 'Organization Unit');
        }
      });

    this.organizationUnitData$ = this.store.pipe(
      select(selectAllOrganizationUnit),
      filter((val) => val.length !== 0),
    );
  }

  getAllClassificationDataset() {
    const pSort = `name:asc`;

    const params = {
      sort: pSort,
    };

    this.store.dispatch(
      fromClassificationDatasetActions.loadAllClassificationDataset({
        params,
        pagination: false,
        infinite: false,
      }),
    );

    this.classificationDatasetIsLoadingList$ = this.store.pipe(
      select(selectIsLoadingListClassificationDataset),
    );

    this.store
      .pipe(
        select(selectErrorClassificationDataset),
        filter((val) => val !== null && val.error),
      )
      .subscribe((result) => {
        if (!result.error) {
          this.classificationDatasetIsError = false;
        } else {
          this.classificationDatasetIsError = true;
          this.toastr.error(result.message, 'Classification Dataset');
        }
      });

    this.classificationDatasetData$ = this.store.pipe(
      select(selectAllClassificationDataset),
      filter((val) => val.length !== 0),
    );
  }

  getAllBusinessField() {
    const pSort = `nama_bidang_urusan:asc`;

    const params = {
      sort: pSort,
    };

    this.businessFieldFacade.loadAllBusinessField(params, false, false);

    this.businessFieldIsLoadingList$ =
      this.businessFieldFacade.isLoadingListBusinessField$;

    this.businessFieldFacade.errorBusinessField$.subscribe((result) => {
      if (!result.error) {
        this.businessFieldIsError = false;
      } else {
        this.businessFieldIsError = true;
        this.toastr.error(result.message, 'Business Field');
      }
    });

    this.businessFieldData$ =
      this.businessFieldFacade.optionsBusinessFieldByKode$;
  }

  getUserPermissionAccessV2(data) {
    this.oldDataAccess = data;

    const unique = [
      ...new Set(
        data.map((item) => {
          return {
            satuan_kerja_id: item.satuan_kerja_id,
            satuan_kerja_nama: item.satuan_kerja_nama,
          };
        }),
      ),
    ];

    const group = unique.filter(
      (thing, index, self) =>
        index ===
        self.findIndex(
          (t) =>
            t['satuan_kerja_id'] === thing['satuan_kerja_id'] &&
            t['satuan_kerja_nama'] === thing['satuan_kerja_nama'],
        ),
    );

    this.myForm.controls.access_control.setValue(group);

    each(group, (res) => {
      this.getAllPosition(res);
    });
  }

  async mappingPosition(data, organization) {
    if (this.isDataAccessFirst) {
      this.oldData = lfilter(this.oldDataAccess, (res) => {
        return res.satuan_kerja_id === organization.satuan_kerja_id;
      });
    }

    let org0 = lfilter(data, { level: '0' });

    if (org0.length === 0) {
      const { dataPosition } = await this.getPosition(0, {
        satuan_kerja_id: organization.satuan_kerja_id,
      });
      org0 = dataPosition as any;
    }

    const getLevel = (allOrganisasi) => {
      const level = map(allOrganisasi, (res) => Number(res.level));
      return Math.max(...level);
    };
    const allLevel = getLevel(data);

    const setMapping = async (allOrganization, level = 1) => {
      const filterLevel = { level: level.toString() };

      return Promise.all(
        allOrganization.map(async (result) => {
          let done = false;
          switch (level) {
            case 1:
              assign(filterLevel, {
                satuan_kerja_id: result.satuan_kerja_id,
              });
              break;
            case 2:
              assign(filterLevel, {
                satuan_kerja_id: result.satuan_kerja_id,
              });
              assign(filterLevel, {
                lv1_unit_kerja_id: result.lv1_unit_kerja_id,
              });
              break;
            case 3:
              assign(filterLevel, {
                satuan_kerja_id: result.satuan_kerja_id,
              });
              assign(filterLevel, {
                lv1_unit_kerja_id: result.lv1_unit_kerja_id,
              });
              assign(filterLevel, {
                lv2_unit_kerja_id: result.lv2_unit_kerja_id,
              });
              break;
            case 4:
              assign(filterLevel, {
                satuan_kerja_id: result.satuan_kerja_id,
              });
              assign(filterLevel, {
                lv1_unit_kerja_id: result.lv1_unit_kerja_id,
              });
              assign(filterLevel, {
                lv2_unit_kerja_id: result.lv2_unit_kerja_id,
              });
              assign(filterLevel, {
                lv3_unit_kerja_id: result.lv3_unit_kerja_id,
              });
              break;
            default:
              done = true;
              break;
          }

          let checked: boolean;
          if (this.isDataAccessFirst) {
            const findCheck = find(
              this.oldData,
              (x) => x.jabatan_id === result.jabatan_id,
            );

            if (findCheck || findCheck !== undefined) {
              checked = true;
            } else {
              checked = false;
            }
          } else {
            checked = false;
          }

          if (level > allLevel || done) {
            return { ...result, checked };
          }

          let organizationFiltered = lfilter(data, filter);

          if (organizationFiltered.length === 0) {
            const { data: dataPosition } = await this.getPosition(
              level,
              filter,
            );
            organizationFiltered = dataPosition as any;
          }

          let populate: any;
          if (organizationFiltered.length > 0) {
            populate = await setMapping(organizationFiltered, level + 1);
            return { ...result, checked, populate };
          }

          return { ...result, checked };
        }),
      );
    };

    return setMapping(org0);
  }

  getPosition(level: number, filterLevel = null) {
    const pSort = `jabatan_nama:asc`;

    const pWhere = {
      level: level.toString(),
    };

    assign(pWhere, filterLevel);

    const params = {
      sort: pSort,
      where: JSON.stringify(pWhere),
    };

    return this.organizationStructureService.getList(params).toPromise();
  }

  getAllPosition(organization) {
    const pSort = `jabatan_nama:asc`;

    const pWhere = {
      satuan_kerja_id: organization.satuan_kerja_id,
    };

    const params = {
      sort: pSort,
      where: JSON.stringify(pWhere),
    };

    this.organizationStructureIsLoadingList = true;
    this.organizationStructureService.getList(params).subscribe((result) => {
      this.organizationStructureIsLoadingList = false;

      this.mappingPosition(result.data, organization).then((mapping) => {
        this.organizationStructureData.push({
          satuan_kerja_id: organization.satuan_kerja_id,
          satuan_kerja_nama: organization.satuan_kerja_nama,
          data: mapping,
          positions: result.data,
        });

        this.addPosition(mapping);
      });
    });
  }

  getAllSchema() {
    const params = {};

    this.schemaService.getList(params).subscribe((result) => {
      this.schemaData = result.data as any as SchemaData[];
    });
  }

  getAllTable(schema: string, table = null) {
    const params = {
      schema,
    };

    this.tableService.getList(params).subscribe((result) => {
      result.data.map((res) => {
        if (res.table === table) {
          return (res.disabled = false);
        }

        return (res.disabled = res.is_used);
      });
      this.tableData = result.data as any as TableData[];
    });
  }

  // SET FORM ==================================================================
  async setFormDataset(data: DatasetData) {
    this.isPeriod = !data.is_permanent;
    this.isRealtime = data.is_realtime;

    const tags = map(data.dataset_tags, (res) => res);

    if (data.dataset_class.id === 4) {
      this.isShowAccess = true;
      this.getUserPermissionAccessV2(data.dataset_permission);
    } else {
      this.myForm.controls.access_control.setValue([]);
    }

    if (this.isPeriod) {
      this.myForm.controls['period'].setValidators([Validators.required]);
    }

    if (this.isRealtime) {
      this.getAllSchema();
      this.getAllTable(data.schema, data.table);

      if (this.satudataUser.role_name === 'opd') {
        this.spreadsheetMode = 'read';
      }
    }

    if (this.satudataUser.role_name === 'opd') {
      this.getAllOrganizationSub(this.satudataUser.kode_skpd);
      this.getAllOrganizationUnit(data.skpdsub.kode_skpdsub);
    } else {
      this.getAllOrganizationSub(data.skpd.kode_skpd);
      this.getAllOrganizationUnit(data.skpdsub.kode_skpdsub);
    }

    this.myForm.patchValue({
      name: data.name,
      description: data.description,
      tags,
      license_id: data.license.id,
      sektoral_id: data.sektoral.id,
      category: data.category,
      kode_skpd: data.skpd.kode_skpd,
      kode_skpdsub: data.skpdsub.kode_skpdsub,
      kode_skpdunit: data.skpdunit.kode_skpdunit,
      kode_indikator: data.kode_indikator,
      kode_bidang_urusan: data.kode_bidang_urusan,
      dataset_class_id: data.dataset_class.id,
      is_realtime: String(this.isRealtime),
      is_permanent: String(data.is_permanent),
      period: data.period ? moment(data.period).format('YYYY-MM') : null,
      schema: data.schema,
      table: data.table,
    });
  }

  async setFormMetadataBusiness(metadata) {
    const data = lfilter(metadata, (res) => {
      return res.metadata_type_id === 1;
    });

    const mappingData = map(this.metadataData, (res) => {
      const findKey = find(data, ['key', res.key]);

      if (findKey) {
        if (res.field === 'frekuensi_dataset') {
          return {
            ...res,
            value: this.frequencyDatasetData.includes(findKey.value)
              ? findKey.value
              : null,
          };
        }

        return { ...res, value: findKey.value || null };
      }

      return { ...res, value: null };
    });

    // Dimension Dataset Start Option
    const dimensiDatasetAwal = find(mappingData, [
      'key',
      'Dimensi Dataset Awal',
    ]);
    if (!isNil(dimensiDatasetAwal)) {
      this.onSelectedDimensionStart(dimensiDatasetAwal.value);
    }

    const mappingPatch = {};
    forEach(mappingData, (res) => {
      assign(mappingPatch, { [res.field]: res.value });
    });

    this.myForm.patchValue(mappingPatch);
  }

  async setFormMetadataCustom(metadata) {
    const data = lfilter(metadata, (res) => {
      return res.metadata_type_id === 4;
    });

    forEach(this.metadataData, (res) => {
      const findKey = find(data, ['key', res.key]);

      if (findKey) {
        this.myForm.patchValue({
          [res.field]: findKey.value || null,
        });

        reject(data, ['key', res.key]);
      }
    });

    if (data.length > 0) {
      forEach(data, (item) => {
        const tmpDict = {};
        tmpDict['key'] = new UntypedFormControl(
          { value: item.key, disabled: false },
          [Validators.maxLength(255)],
        );
        tmpDict['value'] = new UntypedFormControl(
          { value: item.value, disabled: false },
          [Validators.maxLength(255)],
        );

        this.fdm.push(new UntypedFormGroup(tmpDict));
      });
    }
  }

  // DYNAMIC FORM ==============================================================
  addMetadata() {
    this.fdm.push(this.createMetadata());
    return false;
  }

  createMetadata(): UntypedFormGroup {
    return new UntypedFormGroup({
      key: new UntypedFormControl({ value: null, disabled: false }, [
        Validators.maxLength(255),
      ]),
      value: new UntypedFormControl({ value: null, disabled: false }, [
        Validators.maxLength(255),
      ]),
    });
  }

  removeMetadata(i) {
    if (this.fdm) {
      this.fdm.removeAt(i);
    }
    return false;
  }

  addPosition(data: any) {
    this.fdp.push(this.createPosition(data));
    return false;
  }

  createPosition(mapping: any) {
    const setForm = (allOrganization) => {
      return allOrganization.map((result) => {
        const temp = {
          position: new UntypedFormControl(result.checked || false),
          checked: new UntypedFormControl(false),
        };

        if (result.populate !== undefined && result.populate.length > 0) {
          const populate = setForm(result.populate);
          assign(temp, { level: new UntypedFormArray(populate) });
        }

        return new UntypedFormGroup(temp);
      });
    };

    return new UntypedFormArray(setForm(mapping));
  }

  removePosition(organization) {
    const findOrg = findIndex(this.organizationStructureData, {
      satuan_kerja_id: organization.value.satuan_kerja_id,
    });

    if (this.fdp) {
      this.fdp.removeAt(findOrg);
      this.organizationStructureData.splice(findOrg, 1);
    }
    return false;
  }

  removePositionAll() {
    this.myForm.controls.jabatan_access = new UntypedFormArray([]);
    this.organizationStructureData = [];
  }

  // SAVE ======================================================================
  getValuePosition() {
    const accessControl = this.f.jabatan_access['controls'];

    const getValue = (input, data) => {
      return map(input['controls'], (res, i) => {
        let organization: any;
        if (data.data) {
          organization = data.data[i];
        } else {
          organization = data.populate[i];
        }

        const position = res.get('position').value && organization.jabatan_id;

        if (res.get('level')) {
          const newArray = [
            position,
            ...getValue(res.get('level'), organization),
          ];
          return newArray;
        }

        return position;
      });
    };

    const selected = [];
    each(accessControl, (res, i) => {
      const value = getValue(res, this.organizationStructureData[i]);
      selected.push(value);
    });

    const allSelected = flattenDeep(selected);

    return lfilter(allSelected, (result) => result !== false);
  }

  setStatus() {
    const statusMap = {
      create: 'draft-active',
      draft: 'draft-active',
      verification: 'new-active',
      revision: 'new-active',
      'revision-new': 'new-active',
      'verification-edit': 'edit-active',
      'revision-edit': 'edit-active',
      approve: 'approve-active',
    };

    const status = STATUS_DATASET.get(
      statusMap[this.filter.from] || 'edit-active',
    );
    const action = this.filter.from in statusMap ? null : 'edit';

    return { action, status };
  }

  setSubmitter(event) {
    const submitterName = event.submitter.name;

    if (submitterName === 'draft') {
      return {
        submited: 'draft',
        status: STATUS_DATASET.get('draft-active'),
      };
    }

    if (submitterName === 'next') {
      const { action, status } = this.setStatus();
      return { action, submited: 'next', status };
    }

    return { submited: null };
  }

  redirectTo({ submited, status }) {
    const redirectMyData = () => {
      this.router.navigate(['/my-data'], {
        queryParams: { catalog: 'dataset', status: 'draft' },
      });
    };

    const redirectDetail = () => {
      this.router.navigate(['/dataset/detail', this.id], {
        state: { bypassFormGuard: true },
      });
    };

    const redirectPreview = () => {
      this.router.navigate(['/my-dataset/preview', this.id], {
        queryParams: { from: this.filter.from },
        state: { bypassFormGuard: true },
      });
    };

    const redirectDeskUploadChangeDraftNew = () => {
      this.router.navigate(['/desk'], {
        queryParams: {
          type: 'upload-change',
          status: 'verification',
          statusVerification: 'new',
        },
        state: { bypassFormGuard: true },
      });
    };

    const redirectDeskUploadChangeDraftEdit = () => {
      this.router.navigate(['/desk'], {
        queryParams: {
          type: 'upload-change',
          status: 'verification',
          statusVerification: 'edit',
        },
        state: { bypassFormGuard: true },
      });
    };

    if (submited === 'draft') {
      redirectMyData();
    } else if (submited === 'next') {
      switch (status.is_validate) {
        case 1:
          if (this.filter.from === 'verification') {
            if (this.satudataUser.role_name === 'walidata') {
              redirectDetail();
            } else {
              redirectDeskUploadChangeDraftNew();
            }
          } else if (
            this.filter.from === 'revision' ||
            this.filter.from === 'revision-new'
          ) {
            redirectDeskUploadChangeDraftNew();
          }
          break;
        case 3:
          redirectDeskUploadChangeDraftEdit();
          break;
        case 4:
          if (this.filter.from === 'verification-edit') {
            if (this.satudataUser.role_name === 'walidata') {
              redirectDetail();
            } else {
              redirectDeskUploadChangeDraftEdit();
            }
          } else if (this.filter.from === 'revision-edit') {
            redirectDeskUploadChangeDraftEdit();
          } else {
            redirectDeskUploadChangeDraftEdit();
          }
          break;
        default:
          redirectPreview();
          break;
      }
    }
  }

  getDataJsonTest() {
    this.dataJson = this.datasetService.getDataJson(
      this.spreadsheet,
      this.spreadsheetConfig,
    );
    console.info(this.dataJson);
  }

  getValueMetadata(input: any) {
    const bodyMetadata = [
      {
        key: 'Pengukuran Dataset',
        value: !isNil(input.pengukuran_dataset)
          ? input.pengukuran_dataset
          : null,
        metadata_type_id: 1,
      },
      {
        key: 'Tingkat Penyajian Dataset',
        value: !isNil(input.tingkat_penyajian_dataset)
          ? input.tingkat_penyajian_dataset
          : null,
        metadata_type_id: 1,
      },
      {
        key: 'Cakupan Dataset',
        value: !isNil(input.cakupan_dataset) ? input.cakupan_dataset : null,
        metadata_type_id: 1,
      },
      {
        key: 'Produsen',
        value: !isNil(input.produsen) ? input.produsen : null,
        metadata_type_id: 1,
      },
      {
        key: 'Bidang',
        value: !isNil(input.bidang) ? input.bidang : null,
        metadata_type_id: 1,
      },
      {
        key: 'Penanggung Jawab',
        value: !isNil(input.penanggung_jawab) ? input.penanggung_jawab : null,
        metadata_type_id: 1,
      },
      {
        key: 'Kontak Produsen',
        value: !isNil(input.kontak_produsen) ? input.kontak_produsen : null,
        metadata_type_id: 1,
      },
      {
        key: 'Kode Indikator',
        value: !isNil(input.kode_indikator) ? input.kode_indikator : null,
        metadata_type_id: 1,
      },
      {
        key: 'Bidang Urusan',
        value: !isNil(input.bidang_urusan) ? input.bidang_urusan : null,
        metadata_type_id: 1,
      },
      {
        key: 'Satuan Dataset',
        value: !isNil(input.satuan_dataset) ? input.satuan_dataset : null,
        metadata_type_id: 1,
      },
      {
        key: 'Frekuensi Dataset',
        value: !isNil(input.frekuensi_dataset) ? input.frekuensi_dataset : null,
        metadata_type_id: 1,
      },
      {
        key: 'Dimensi Dataset Awal',
        value: !isNil(input.dimensi_dataset_awal)
          ? input.dimensi_dataset_awal
          : null,
        metadata_type_id: 1,
      },
      {
        key: 'Dimensi Dataset Akhir',
        value: !isNil(input.dimensi_dataset_akhir)
          ? input.dimensi_dataset_akhir
          : null,
        metadata_type_id: 1,
      },
    ];

    if (!isNil(input.sumber_external)) {
      bodyMetadata.push({
        key: 'Sumber External',
        value: input.sumber_external,
        metadata_type_id: 1,
      });
    }

    map(input.metadata, (result) => {
      bodyMetadata.push({
        key: result.key,
        value: result.value,
        metadata_type_id: 4,
      });
    });

    return bodyMetadata;
  }

  onSubmit(event) {
    const { action, submited, status } = this.setSubmitter(event);

    const input = this.myForm.value;

    // Tags
    const bodyTags = input.tags;

    // Position Access
    const bodyPosition = this.getValuePosition();

    // Metadata
    const bodyMetadata = this.getValueMetadata(input);

    // Permanent
    const isPermanent = input.is_permanent !== 'false';

    let period = null;
    if (!isPermanent) {
      period = moment(input.period).format('YYYY-MM-01');
    }

    // Data Json
    this.dataJson = this.datasetService.getDataJson(
      this.spreadsheet,
      this.spreadsheetConfig,
    );

    // Change
    const bodyChange = input.change;

    // History
    let historyCategory: string;
    let historyNote = null;
    switch (this.filter.from) {
      case 'verification':
      case 'verification-edit':
        historyCategory = null;
        break;
      case 'revision-new':
        historyCategory = 'waiting';
        historyNote = bodyChange;
        break;
      case 'revision-edit':
        historyCategory = 'edit';
        historyNote = bodyChange;
        break;
      default:
        if (
          status.is_validate === 4 &&
          this.filter.from !== 'verification-edit'
        ) {
          historyCategory = 'edit';
          historyNote = bodyChange;
        } else {
          historyCategory = 'preview';
        }
        break;
    }

    let bodyHistory;
    if (historyCategory) {
      bodyHistory = {
        type: 'dataset',
        type_id: Number(this.id),
        category: historyCategory,
        notes: historyNote,
      };
    } else {
      bodyHistory = null;
    }

    // Dataset
    const bodyDataset = input;
    delete bodyDataset['tags'];
    delete bodyDataset['period_date'];
    delete bodyDataset['period_month'];
    delete bodyDataset['period_year'];
    delete bodyDataset['pengukuran_dataset'];
    delete bodyDataset['tingkat_penyajian_dataset'];
    delete bodyDataset['cakupan_dataset'];
    delete bodyDataset['produsen'];
    delete bodyDataset['bidang'];
    delete bodyDataset['penanggung_jawab'];
    delete bodyDataset['kontak_produsen'];
    delete bodyDataset['bidang_urusan'];
    delete bodyDataset['satuan_dataset'];
    delete bodyDataset['frekuensi_dataset'];
    delete bodyDataset['dimensi_dataset_awal'];
    delete bodyDataset['dimensi_dataset_akhir'];
    delete bodyDataset['sumber_external'];
    delete bodyDataset['metadata'];
    delete bodyDataset['access_control'];
    delete bodyDataset['jabatan_access'];
    delete bodyDataset['change'];
    if (!this.isRealtime) {
      delete bodyDataset['schema'];
      delete bodyDataset['table'];
    }

    // Final
    assign(bodyDataset, {
      id: Number(this.id),
      dataset_type_id: 3,
      regional_id: 1,
      is_permanent: isPermanent,
      period,
      is_realtime: this.isRealtime,
      tag: bodyTags,
      metadata: bodyMetadata,
    });

    if (!this.isRealtime) {
      assign(bodyDataset, { json: this.dataJson });
    }

    if (input.dataset_class_id === 4) {
      assign(bodyDataset, {
        jabatan_access: bodyPosition,
      });
    }

    if (bodyHistory) {
      assign(bodyDataset, { history_draft: bodyHistory });
    }

    assign(bodyDataset, status);

    this.datasetIsLoadingUpdate$ = this.store.pipe(
      select(selectIsLoadingUpdateDataset),
    );

    // Params
    const params = {};
    if (action && action === 'edit') {
      assign(params, { from: 'edit' });
    }

    this.store.dispatch(
      fromDatasetActions.updateDataset({
        update: bodyDataset,
        params,
      }),
    );

    this.actions$
      .pipe(
        delay(1000),
        ofType(fromDatasetActions.updateDatasetSuccess),
        takeUntil(this.unsubscribeDataset$),
      )
      .subscribe(() => {
        this.redirectTo({ submited, status });
      });

    this.actions$
      .pipe(
        ofType(fromDatasetActions.updateDatasetFailure),
        takeUntil(this.unsubscribeDataset$),
      )
      .subscribe((result) => {
        this.toastr.error(result.error.message, 'Dataset');
      });
  }

  // ONCHANGE ==================================================================
  onSelectedOrganization(event: any) {
    if (isNil(event)) {
      return;
    }

    this.myForm.controls['kode_skpdsub'].reset();
    this.myForm.controls['kode_skpdunit'].reset();
    this.getAllOrganizationSub(event.kode_skpd);

    this.myForm.controls['bidang'].reset();
    this.myForm.controls['penanggung_jawab'].reset();
    this.myForm.controls['produsen'].setValue(event.nama_skpd);
    this.myForm.controls['kontak_produsen'].setValue(
      this.getContact(event.phone, event.email),
    );
  }

  onClearOrganization() {
    this.organizationSubData$ = null;
    this.myForm.controls['kode_skpdsub'].reset();
    this.organizationUnitData$ = null;
    this.myForm.controls['kode_skpdunit'].reset();

    this.myForm.controls['produsen'].reset();
    this.myForm.controls['bidang'].reset();
    this.myForm.controls['penanggung_jawab'].reset();
    this.myForm.controls['kontak_produsen'].reset();
  }

  onSelectedOrganizationSub(event: any) {
    if (isNil(event)) {
      return;
    }

    this.myForm.controls['kode_skpdunit'].reset();
    this.getAllOrganizationUnit(event.kode_skpdsub);

    this.myForm.controls['penanggung_jawab'].reset();
    this.myForm.controls['bidang'].setValue(event?.nama_skpdsub);
  }

  onClearOrganizationSub() {
    this.organizationUnitData$ = null;
    this.myForm.controls['kode_skpdunit'].reset();

    this.myForm.controls['bidang'].reset();
    this.myForm.controls['penanggung_jawab'].reset();
  }

  onSelectedOrganizationUnit(event: any) {
    if (isNil(event)) {
      return;
    }

    this.myForm.controls['penanggung_jawab'].setValue(event.nama_skpdunit);
  }

  onClearOrganizationUnit() {
    this.myForm.controls['penanggung_jawab'].reset();
  }

  onSelectedClassificationDataset(value) {
    this.isClassificationDataset = true;

    if (value === 4) {
      this.isShowAccess = true;
    } else {
      this.isShowAccess = false;
      this.myForm.controls.access_control.setValue([]);
      this.removePositionAll();
    }
  }

  onClearClassificationDataset() {
    this.isClassificationDataset = false;
  }

  onAddAccessControl(value) {
    this.isDataAccessFirst = false;
    this.getAllPosition(value);
  }

  onRemoveAccessControl(value) {
    this.removePosition(value);
  }

  onSelectedIsPermanent(value: string) {
    const isTrueSet = value === 'true';
    if (!isTrueSet) {
      this.isPeriod = true;
      this.myForm.controls['period'].setValidators([Validators.required]);
    } else {
      this.isPeriod = false;
      this.myForm.controls['period'].clearValidators();
      this.myForm.controls['period'].reset();
    }

    this.myForm.controls['period'].updateValueAndValidity();
  }

  onSelectedSchema(value: string) {
    this.myForm.controls['table'].reset();
    this.getAllTable(value);
  }

  onClearSchema() {
    this.tableData = [];
    this.myForm.controls['table'].reset();
  }

  onSelectedDimensionStart(event: any) {
    if (isNil(event)) {
      return;
    }

    this.dimensionDatasetEndData = [];
    for (let i = new Date().getFullYear(); i >= event; i -= 1) {
      this.dimensionDatasetEndData = [...this.dimensionDatasetEndData, i];
    }
  }

  onSelectedBusinessField(event: any) {
    if (isNil(event)) {
      return;
    }

    this.myForm.controls['bidang_urusan'].setValue(event.label);
  }

  onClearBusinessField() {
    this.myForm.controls['bidang_urusan'].reset();
  }

  // ADDITIONAL ================================================================
  panelCollapse(to: string) {
    this.viewportScroller.setOffset([0, 100]);

    setTimeout(() => {
      this.viewportScroller.scrollToAnchor(to);
    }, 500);
  }

  get isDisabledButtonDraft(): boolean {
    const title = this.f.name?.valid;
    const organization = this.f.kode_skpd?.valid;
    const topic = this.f.sektoral_id?.valid;

    if (title && organization && topic) {
      return false;
    }

    return true;
  }

  getContact(phone: string, email: string) {
    let dash = '';
    if (phone && email) {
      dash = ' — ';
    }
    return `${phone ?? ''}${dash}${email ?? ''}`;
  }
}
