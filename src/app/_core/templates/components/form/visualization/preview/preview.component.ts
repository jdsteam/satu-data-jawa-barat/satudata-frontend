import {
  Component,
  OnDestroy,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';
import { Actions, ofType } from '@ngrx/effects';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';

// SERVICE
import { AuthenticationService } from '@services-v3';

// MODEL
import {
  LoginData,
  VisualizationData,
  MetadataVisualizationData,
} from '@models-v3';

// STORE
import {
  selectVisualization,
  selectIsLoadingRead as selectIsLoadingReadVisualization,
  selectIsLoadingUpdate as selectIsLoadingUpdateVisualization,
  selectError as selectErrorVisualization,
} from '@store-v3/visualization/visualization.selectors';
import { fromVisualizationActions } from '@store-v3/visualization/visualization.actions';

import {
  selectAllMetadataBusinessVisualization,
  selectError as selectErrorMetadataBusinessVisualization,
} from '@store-v3/metadata-business-visualization/metadata-business-visualization.selectors';
import { fromMetadataBusinessVisualizationActions } from '@store-v3/metadata-business-visualization/metadata-business-visualization.actions';

import {
  selectAllMetadataCustomVisualization,
  selectError as selectErrorMetadataCustomVisualization,
} from '@store-v3/metadata-custom-visualization/metadata-custom-visualization.selectors';
import { fromMetadataCustomVisualizationActions } from '@store-v3/metadata-custom-visualization/metadata-custom-visualization.actions';

import { assign, isNil } from 'lodash';
import moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { NgxLoadingModule } from 'ngx-loading';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

@Component({
  selector: 'app-form-visualization-preview',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    NgxLoadingModule,
    NgxSkeletonLoaderModule,
  ],
  templateUrl: './preview.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreviewComponent implements OnInit, OnDestroy {
  env = environment;
  satudataUser: LoginData;
  moment: any = moment;
  jds: any = jds;

  // Variable
  id: number;
  filter = {
    from: 'create',
  };

  defaultImage = 'assets/images/thumbnail.svg';

  // Data
  visualizationData$: Observable<VisualizationData>;
  visualizationIsLoadingRead$: Observable<boolean>;
  visualizationIsLoadingUpdate$: Observable<boolean>;
  visualizationIsError: boolean;

  metadataBusinessData$: Observable<MetadataVisualizationData[]>;
  metadataBusinessIsLoadingList$: Observable<boolean>;
  metadataBusinessIsError$: Observable<boolean>;
  metadataBusinessIsError: boolean;
  metadataBusinessErrorMessage: string;

  metadataCustomData$: Observable<MetadataVisualizationData[]>;
  metadataCustomIsLoadingList$: Observable<boolean>;
  metadataCustomIsError$: Observable<boolean>;
  metadataCustomIsError: boolean;
  metadataCustomErrorMessage: string;

  private unsubscribeVisualization$ = new Subject<void>();

  constructor(
    private cdRef: ChangeDetectorRef,
    private authenticationService: AuthenticationService,
    private router: Router,
    private route: ActivatedRoute,
    private actions$: Actions,
    private store: Store<any>,
    private toastr: ToastrService,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    // id
    this.id = this.route.snapshot.params.id;
  }

  ngOnInit() {
    this.queryParams();

    this.getVisualization();
    this.getAllMetadataBusinessVisualization();
    this.getAllMetadataCustomVisualization();
  }

  ngOnDestroy() {
    this.unsubscribeVisualization$.next();
    this.unsubscribeVisualization$.complete();
  }

  queryParams(): void {
    this.route.queryParams.subscribe((p) => {
      const pFrom = p.from;

      this.filter.from = !isNil(pFrom) ? pFrom : 'create';
    });
  }

  // GET =====================================================================================================
  getVisualization() {
    const params = {
      detail: true,
    };

    this.store.dispatch(
      fromVisualizationActions.loadVisualization({
        id: this.id,
        params,
      }),
    );

    this.visualizationIsLoadingRead$ = this.store.pipe(
      select(selectIsLoadingReadVisualization),
    );

    this.store
      .pipe(
        select(selectErrorVisualization),
        filter((val) => val !== null && val.error),
      )
      .subscribe((result) => {
        if (!result.error) {
          this.visualizationIsError = false;
        } else {
          this.visualizationIsError = true;
          this.toastr.error(result.message, 'Visualization');
        }
      });

    this.visualizationData$ = this.store.pipe(
      select(selectVisualization(this.id)),
    );
  }

  getAllMetadataBusinessVisualization(): void {
    const pWhere = {
      visualization_id: this.id,
      metadata_type_id: 1,
    };

    const params = {
      where: JSON.stringify(pWhere),
    };

    this.store.dispatch(
      fromMetadataBusinessVisualizationActions.loadAllMetadataBusinessVisualization(
        {
          params,
          pagination: false,
          infinite: false,
        },
      ),
    );

    this.store
      .pipe(
        select(selectErrorMetadataBusinessVisualization),
        filter((val) => val !== null && val.error),
      )
      .subscribe((result) => {
        if (!result.error) {
          this.metadataBusinessIsError = false;
        } else {
          this.metadataBusinessIsError = true;
          this.toastr.error(result.message, 'Metadata Business Dataset');
        }
      });

    this.metadataBusinessData$ = this.store.pipe(
      select(selectAllMetadataBusinessVisualization),
    );
  }

  getAllMetadataCustomVisualization(): void {
    const pWhere = {
      visualization_id: this.id,
      metadata_type_id: 4,
    };

    const params = {
      where: JSON.stringify(pWhere),
    };

    this.store.dispatch(
      fromMetadataCustomVisualizationActions.loadAllMetadataCustomVisualization(
        {
          params,
          pagination: false,
          infinite: false,
        },
      ),
    );

    this.store
      .pipe(
        select(selectErrorMetadataCustomVisualization),
        filter((val) => val !== null && val.error),
      )
      .subscribe((result) => {
        if (!result.error) {
          this.metadataCustomIsError = false;
        } else {
          this.metadataCustomIsError = true;
          this.toastr.error(result.message, 'Metadata Custom Dataset');
        }
      });

    this.metadataCustomData$ = this.store.pipe(
      select(selectAllMetadataCustomVisualization),
    );
  }

  // SAVE ====================================================================================================
  onSubmit() {
    const bodyVisualization = {
      id: Number(this.id),
    };

    switch (this.filter.from) {
      case 'create':
        assign(bodyVisualization, {
          is_active: true,
          is_deleted: false,
          // kode_skpd: this.dataVisualization[0].skpd.kode_skpd,
        });
        break;
      default:
        assign(bodyVisualization, {
          is_deleted: false,
          // kode_skpd: this.dataVisualization[0].skpd.kode_skpd,
        });
        break;
    }

    this.store.dispatch(
      fromVisualizationActions.updateVisualization({
        update: bodyVisualization,
      }),
    );

    this.visualizationIsLoadingUpdate$ = this.store.pipe(
      select(selectIsLoadingUpdateVisualization),
    );

    this.actions$
      .pipe(
        ofType(fromVisualizationActions.updateVisualizationSuccess),
        takeUntil(this.unsubscribeVisualization$),
      )
      .subscribe(() => {
        const from =
          this.filter.from === 'create' || this.filter.from === 'draft'
            ? 'ditambahkan'
            : 'diubah';

        Swal.fire({
          type: 'success',
          text: `Visualisasi berhasil ${from}.`,
          allowOutsideClick: false,
        }).then(() => {
          this.router.navigate(['/my-data'], {
            queryParams: { catalog: 'visualization' },
          });
        });
      });

    this.actions$
      .pipe(
        ofType(fromVisualizationActions.updateVisualizationFailure),
        takeUntil(this.unsubscribeVisualization$),
      )
      .subscribe((result) => {
        this.toastr.error(result.error.message, 'Visualization');
      });
  }

  errorImage(event) {
    // eslint-disable-next-line no-param-reassign
    event.target.src = this.defaultImage;
  }
}
