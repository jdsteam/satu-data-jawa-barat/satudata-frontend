import {
  Component,
  OnInit,
  OnChanges,
  OnDestroy,
  SimpleChanges,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

// STYLE
import * as jds from '@styles';

// MODEL
import { LicenseData } from '@models-v3';

// STORE
import {
  selectAllLicense,
  selectIsLoadingList as selectIsLoadingListLicense,
} from '@store-v3/license/license.selectors';
import { fromLicenseActions } from '@store-v3/license/license.actions';

// JDS-BI
import { JdsBiScrollbarModule } from '@jds-bi/core';

import {
  assign,
  filter as lfilter,
  isEmpty,
  map,
  merge,
  keyBy,
  values,
  orderBy,
  forEach,
  isNil,
  split,
} from 'lodash';
import { Store, select } from '@ngrx/store';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

@Component({
  selector: 'app-filter-license-v2',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    NgxSkeletonLoaderModule,
    JdsBiScrollbarModule,
  ],
  templateUrl: './license.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LicenseComponent implements OnInit, OnChanges, OnDestroy {
  jds: any = jds;

  @Input() from: any;
  @Input() filterInputLicense: any;
  @Input() filterData: any;
  @Output() filterOutputLicense = new EventEmitter<any>();

  // Variable
  classPanel: string;
  filter = {
    currentPage: 1,
    license: [],
  };

  // Data
  data: LicenseData[];
  data$: Observable<LicenseData[]>;
  isLoadingList$: Observable<boolean>;
  isError$: Observable<boolean>;
  isError = false;
  errorMessage: string;
  pagination: any;
  pages: any;

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<any>,
  ) {}

  ngOnInit(): void {
    this.getAllLicense();
  }

  ngOnChanges(changes: SimpleChanges): void {
    // eslint-disable-next-line no-restricted-syntax
    for (const propName in changes) {
      // eslint-disable-next-line no-prototype-builtins
      if (changes.hasOwnProperty(propName)) {
        if (propName === 'filterInputLicense') {
          const chng = changes[propName];
          this.filter.license = chng.currentValue;
          this.createChecklist();
        }
      }
    }

    this.classPanel = `${this.jds.borderTop(0)} ${this.jds.roundedBottom(8)}`;
  }

  ngOnDestroy(): void {
    this.filter.license = [];
  }

  queryParams(): void {
    this.route.queryParams.subscribe((p) => {
      const pLicense = p.license;

      this.filter.license = !isNil(pLicense)
        ? split(pLicense, ',').map(Number)
        : [];
    });
  }

  getAllLicense(): void {
    const pSort = 'name:asc';

    const params = {
      sort: pSort,
    };

    this.store.dispatch(
      fromLicenseActions.loadAllLicense({
        params,
        pagination: false,
        infinite: false,
      }),
    );

    this.isLoadingList$ = this.store.pipe(select(selectIsLoadingListLicense));

    this.store
      .pipe(
        select(selectAllLicense),
        filter((val) => val.length !== 0),
      )
      .subscribe((result) => {
        this.data = result;
        this.createChecklist();
      });
  }

  createChecklist(): void {
    if (this.data === undefined) {
      return;
    }

    const modify: any = [];

    forEach(this.data, (resultOrder) => {
      modify.push({
        ...resultOrder,
        checked: false,
      });
    });

    let result: any;

    if (this.filter.license.length) {
      const checked = [];
      map(this.filter.license, (resultLine) => {
        checked.push({ id: Number(resultLine), checked: true });
      });

      const merged = merge(keyBy(modify, 'id'), keyBy(checked, 'id'));
      const vals = values(merged);
      result = orderBy(vals, ['name'], ['asc']);
    } else {
      result = orderBy(modify, ['name'], ['asc']);
    }

    this.data = result;
  }

  filterLicense(isChecked: boolean, data: any) {
    if (isChecked) {
      map(this.data, (result) => {
        if (result.id === data.id) {
          assign(result, { checked: true });
        }
      });
    } else {
      map(this.data, (result) => {
        if (result.id === data.id) {
          // eslint-disable-next-line no-param-reassign
          delete result.checked;
        }
      });
    }

    const hasil = map(lfilter(this.data, 'checked'), 'id').join(',');
    const value = isEmpty(hasil) === false ? hasil : null;

    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        license: value,
        currentpage: null,
      },
      queryParamsHandling: 'merge',
    });

    const active = lfilter(this.data, 'checked');

    this.filter.license = active;
    this.filter.currentPage = 1;
    this.filterOutputLicense.emit(this.filter);
  }
}
