import {
  Component,
  OnInit,
  OnChanges,
  AfterViewInit,
  SimpleChanges,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { BehaviorSubject, Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';

// STYLE
import * as jds from '@styles';

// STORE
import {
  selectIsLoadingList as selectDatasetIsLoadingList,
  selectPagination as selectDatasetPagination,
  selectSuggestion as selectDatasetSuggestion,
} from '@store-v3/dataset/dataset.selectors';

import {
  selectIsLoadingList as selectIndicatorIsLoadingList,
  selectPagination as selectIndicatorPagination,
  selectSuggestion as selectIndicatorSuggestion,
} from '@store-v3/indicator/indicator.selectors';

import {
  selectIsLoadingList as selectVisualizationIsLoadingList,
  selectPagination as selectVisualizationPagination,
  selectSuggestion as selectVisualizationSuggestion,
} from '@store-v3/visualization/visualization.selectors';

import {
  selectIsLoadingList as selectOrganizationIsLoadingList,
  selectPagination as selectOrganizationPagination,
  selectSuggestion as selectOrganizationSuggestion,
} from '@store-v3/organization/organization.selectors';

import { assign, isEmpty, isNil } from 'lodash';
import { Store, select } from '@ngrx/store';

@Component({
  selector: 'app-filter-suggestion',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './suggestion.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SuggestionComponent implements OnInit, OnChanges, AfterViewInit {
  jds: any = jds;

  @Input() type: string;

  @Input() set filterInput(value: any) {
    this.filter = value;
  }
  get filterInput() {
    return this.filter;
  }

  @Output() filterOutputSearch = new EventEmitter<any>();
  defaultInputs$ = new BehaviorSubject<any>({
    type: null,
    filterInput: null,
  });

  // Variable
  filter: any;

  // Data
  isLoadingList$: Observable<boolean>;
  pagination$: Observable<any>;
  suggestion$: Observable<any>;

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<any>,
  ) {}

  ngOnInit(): void {
    this.defaultInputs$.next({
      ...this.defaultInputs$.getValue(),
      ...this.checkInputs(),
    });

    this.getPagination();
  }

  ngOnChanges(changes: SimpleChanges): void {
    const inputs = Object.keys(changes).reduce((result: any, item: any) => {
      if (item === 'filterInput') {
        this.filter = changes[item].currentValue;
        this.getPagination();
      }

      // eslint-disable-next-line no-param-reassign
      result[item] = changes[item].currentValue;
      return result;
    }, {});

    this.defaultInputs$.next({ ...this.defaultInputs$.getValue(), ...inputs });
  }

  ngAfterViewInit(): void {
    this.cdRef.detectChanges();
  }

  checkInputs(): any {
    const inputs = {};

    if (!isNil(this.type)) {
      assign(inputs, { type: this.type });
    }

    if (!isNil(this.filterInput)) {
      assign(inputs, { filterInput: this.filterInput });
    }

    return inputs;
  }

  getPagination() {
    switch (this.type) {
      case 'dataset':
        this.getPaginationDataset();
        break;
      case 'indicator':
        this.getPaginationIndicator();
        break;
      case 'visualization':
        this.getPaginationVisualization();
        break;
      case 'organization':
        this.getPaginationOrganization();
        break;
      default:
        break;
    }
  }

  getPaginationDataset() {
    this.isLoadingList$ = this.store.pipe(select(selectDatasetIsLoadingList));
    this.pagination$ = this.store.pipe(select(selectDatasetPagination));
    this.suggestion$ = this.store.pipe(select(selectDatasetSuggestion));
  }

  getPaginationIndicator() {
    this.isLoadingList$ = this.store.pipe(select(selectIndicatorIsLoadingList));
    this.pagination$ = this.store.pipe(select(selectIndicatorPagination));
    this.suggestion$ = this.store.pipe(select(selectIndicatorSuggestion));
  }

  getPaginationVisualization() {
    this.isLoadingList$ = this.store.pipe(
      select(selectVisualizationIsLoadingList),
    );
    this.pagination$ = this.store.pipe(select(selectVisualizationPagination));
    this.suggestion$ = this.store.pipe(select(selectVisualizationSuggestion));
  }

  getPaginationOrganization() {
    this.isLoadingList$ = this.store.pipe(
      select(selectOrganizationIsLoadingList),
    );
    this.pagination$ = this.store.pipe(select(selectOrganizationPagination));
    this.suggestion$ = this.store.pipe(select(selectOrganizationSuggestion));
  }

  getType(type: string): string {
    let name: string;
    switch (type) {
      case 'dataset':
        name = 'Dataset';
        break;
      case 'indicator':
        name = 'Indikator';
        break;
      case 'visualization':
        name = 'Visualisasi';
        break;
      case 'organization':
        name = 'Organisasi';
        break;
      default:
        break;
    }
    return name;
  }

  filterSearch(suggestion: string, event: string): void {
    const value = isEmpty(event) === false ? event : null;

    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        q: value,
        currentpage: null,
        suggestion: suggestion === 'on' ? null : suggestion,
      },
      queryParamsHandling: 'merge',
    });

    this.filter.search = value;
    this.filter.currentPage = 1;
    this.filter.suggestion = suggestion;
    this.filterOutputSearch.emit(this.filter);
  }
}
