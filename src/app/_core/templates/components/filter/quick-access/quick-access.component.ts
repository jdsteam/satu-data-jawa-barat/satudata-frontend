import {
  Component,
  OnChanges,
  SimpleChanges,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { filter } from 'rxjs/operators';

// STYLE
import * as jds from '@styles';

// CONSTANT
import { QUALITY_DATA } from '@constants-v4';

// COMPONENT
import { JdsBiIconsModule, JdsBiIconsService, iconXmark } from '@jds-bi/icons';

// STORE
import { selectMultipleClassificationDataset } from '@store-v3/classification-dataset/classification-dataset.selectors';
import { selectMultipleClassificationIndicatorByName } from '@store-v3/classification-indicator/classification-indicator.selectors';
import { selectMultipleOrganizationByCode } from '@store-v3/organization/organization.selectors';
import { selectMultipleTopic } from '@store-v3/topic/topic.selectors';
import { selectMultipleDatasetTag } from '@store-v3/dataset-tag/dataset-tag.selectors';
import { selectMultipleLicense } from '@store-v3/license/license.selectors';

import { isEmpty, map, orderBy, remove } from 'lodash';
import { Store, select } from '@ngrx/store';

@Component({
  selector: 'app-filter-quick-access',
  standalone: true,
  imports: [CommonModule, JdsBiIconsModule],
  templateUrl: './quick-access.component.html',
  styleUrls: ['./quick-access.component.scss'],
})
export class QuickAccessComponent implements OnChanges {
  jds: any = jds;

  @Input() filterInput: any;
  @Output() filterOutputClassificationDataset = new EventEmitter<any>();
  @Output() filterOutputClassificationIndicator = new EventEmitter<any>();
  @Output() filterOutputOrganization = new EventEmitter<any>();
  @Output() filterOutputTopic = new EventEmitter<any>();
  @Output() filterOutputDatasetTag = new EventEmitter<any>();
  @Output() filterOutputLicense = new EventEmitter<any>();
  @Output() filterOutputQualityData = new EventEmitter<any>();

  // Variable
  filter = {
    currentPage: 1,
    classification_dataset: [],
    classification_indicator: [],
    organization: [],
    topic: [],
    dataset_tag: [],
    license: [],
    quality_data: [],
  };

  // Data
  classificationDatasetData = [];
  classificationIndicatorData = [];
  organizationData = [];
  topicData = [];
  datasetTagData = [];
  licenseData = [];
  qualityData = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private iconService: JdsBiIconsService,
    private store: Store<any>,
  ) {
    this.iconService.registerIcons([iconXmark]);
  }

  ngOnChanges(changes: SimpleChanges): void {
    // eslint-disable-next-line no-restricted-syntax
    for (const propName in changes) {
      // eslint-disable-next-line no-prototype-builtins
      if (changes.hasOwnProperty(propName)) {
        const chng = changes[propName];
        this.filter = chng.currentValue;
        if (this.checkClassificationDataset) {
          this.getClassificationDataset();
        }
        this.getClassificationIndicator();
        this.getOrganization();
        this.getTopic();
        this.getDatasetTag();
        this.getLicense();
        this.getQualityData();
      }
    }
  }

  get accessPermission() {
    if (
      (this.checkClassificationDataset &&
        this.classificationDatasetData.length > 0) ||
      this.classificationIndicatorData.length > 0 ||
      this.organizationData.length > 0 ||
      this.topicData.length > 0 ||
      this.datasetTagData.length > 0 ||
      this.licenseData.length > 0 ||
      this.qualityData.length > 0
    ) {
      return true;
    }

    return false;
  }

  get checkClassificationDataset() {
    if (Array.isArray(this.filter.classification_dataset)) {
      return true;
    }

    return false;
  }

  getClassificationDataset(): void {
    this.store
      .pipe(
        select(
          selectMultipleClassificationDataset(
            this.filter.classification_dataset || [],
          ),
        ),
        filter((val) => val !== null),
      )
      .subscribe((result) => {
        this.classificationDatasetData = orderBy(result, ['name'], ['asc']);
      });
  }

  getClassificationIndicator(): void {
    this.store
      .pipe(
        select(
          selectMultipleClassificationIndicatorByName(
            this.filter.classification_indicator || [],
          ),
        ),
        filter((val) => val !== null),
      )
      .subscribe((result) => {
        this.classificationIndicatorData = orderBy(result, ['name'], ['asc']);
      });
  }

  getOrganization(): void {
    this.store
      .pipe(
        select(
          selectMultipleOrganizationByCode(this.filter.organization || []),
        ),
        filter((val) => val !== null),
      )
      .subscribe((result) => {
        this.organizationData = orderBy(result, ['nama_skpd'], ['asc']);
      });
  }

  getTopic(): void {
    this.store
      .pipe(
        select(selectMultipleTopic(this.filter.topic || [])),
        filter((val) => val !== null),
      )
      .subscribe((result) => {
        this.topicData = orderBy(result, ['name'], ['asc']);
      });
  }

  getDatasetTag(): void {
    this.store
      .pipe(
        select(selectMultipleDatasetTag(this.filter.dataset_tag || [])),
        filter((val) => val !== null),
      )
      .subscribe((result) => {
        this.datasetTagData = orderBy(result, ['tag'], ['asc']);
      });
  }

  getLicense(): void {
    this.store
      .pipe(
        select(selectMultipleLicense(this.filter.license || [])),
        filter((val) => val !== null),
      )
      .subscribe((result) => {
        this.licenseData = orderBy(result, ['name'], ['asc']);
      });
  }

  getQualityData(): void {
    this.qualityData = map(this.filter.quality_data, (result) => {
      const data = QUALITY_DATA.get(result);
      return data;
    });
  }

  clearClassificationDataset(id: number): void {
    remove(this.classificationDatasetData, (o) => o.id === id);
    const ids = map(this.classificationDatasetData, 'id').join(',');
    const value = isEmpty(ids) === false ? ids : null;

    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        classification_dataset: value,
        currentpage: null,
      },
      queryParamsHandling: 'merge',
    });

    const pick = map(this.classificationDatasetData, (object) => {
      return object.id.toString();
    });

    this.filter.classification_dataset = pick;
    this.filter.currentPage = 1;
    this.filterOutputClassificationDataset.emit(this.filter);
  }

  clearClassificationIndicator(id: number): void {
    remove(this.classificationIndicatorData, (o) => o.id === id);
    const ids = map(this.classificationIndicatorData, 'name').join(',');
    const value = isEmpty(ids) === false ? ids : null;

    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        classification_indicator: value,
        currentpage: null,
      },
      queryParamsHandling: 'merge',
    });

    const pick = map(this.classificationIndicatorData, (object) => {
      return object.name.toString();
    });

    this.filter.classification_indicator = pick;
    this.filter.currentPage = 1;
    this.filterOutputClassificationIndicator.emit(this.filter);
  }

  clearOrganization(id: number): void {
    remove(this.organizationData, (o) => o.id === id);
    const ids = map(this.organizationData, 'kode_skpd').join(',');
    const value = isEmpty(ids) === false ? ids : null;

    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        organization: value,
        currentpage: null,
      },
      queryParamsHandling: 'merge',
    });

    const pick = map(this.organizationData, (object) => {
      return object.kode_skpd.toString();
    });

    this.filter.organization = pick;
    this.filter.currentPage = 1;
    this.filterOutputOrganization.emit(this.filter);
  }

  clearTopic(id: number): void {
    remove(this.topicData, (o) => o.id === id);
    const ids = map(this.topicData, 'id').join(',');
    const value = isEmpty(ids) === false ? ids : null;

    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        topic: value,
        currentpage: null,
      },
      queryParamsHandling: 'merge',
    });

    const pick = map(this.topicData, (object) => {
      return object.id.toString();
    });

    this.filter.topic = pick;
    this.filter.currentPage = 1;
    this.filterOutputTopic.emit(this.filter);
  }

  clearDatasetTag(tag: number): void {
    remove(this.datasetTagData, (o) => o.tag === tag);
    const ids = map(this.datasetTagData, 'tag').join(',');
    const value = isEmpty(ids) === false ? ids : null;

    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        dataset_tag: value,
        currentpage: null,
      },
      queryParamsHandling: 'merge',
    });

    const pick = map(this.datasetTagData, (object) => {
      return object.tag.toString();
    });

    this.filter.dataset_tag = pick;
    this.filter.currentPage = 1;
    this.filterOutputDatasetTag.emit(this.filter);
  }

  clearLicense(id: number): void {
    remove(this.licenseData, (o) => o.id === id);
    const ids = map(this.licenseData, 'id').join(',');
    const value = isEmpty(ids) === false ? ids : null;

    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        license: value,
        currentpage: null,
      },
      queryParamsHandling: 'merge',
    });

    const pick = map(this.licenseData, (object) => {
      return object.id.toString();
    });

    this.filter.license = pick;
    this.filter.currentPage = 1;
    this.filterOutputLicense.emit(this.filter);
  }

  clearQualityData(slug: string): void {
    remove(this.qualityData, (o) => o.slug === slug);
    const ids = map(this.qualityData, 'slug').join(',');
    const value = isEmpty(ids) === false ? ids : null;

    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        quality_data: value,
        currentpage: null,
      },
      queryParamsHandling: 'merge',
    });

    const pick = map(this.qualityData, (object) => {
      return object.slug.toString();
    });

    this.filter.quality_data = pick;
    this.filter.currentPage = 1;
    this.filterOutputQualityData.emit(this.filter);
  }

  clearAll(): void {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        classification_dataset: null,
        classification_indicator: null,
        organization: null,
        topic: null,
        dataset_tag: null,
        license: null,
        quality_data: null,
        currentpage: null,
      },
      queryParamsHandling: 'merge',
    });

    this.filter.classification_dataset = null;
    this.filter.classification_indicator = null;
    this.filter.organization = null;
    this.filter.topic = null;
    this.filter.dataset_tag = null;
    this.filter.license = null;
    this.filter.quality_data = null;
    this.filter.currentPage = 1;
    this.filterOutputClassificationDataset.emit(this.filter);
    this.filterOutputClassificationIndicator.emit(this.filter);
    this.filterOutputOrganization.emit(this.filter);
    this.filterOutputTopic.emit(this.filter);
    this.filterOutputDatasetTag.emit(this.filter);
    this.filterOutputLicense.emit(this.filter);
    this.filterOutputQualityData.emit(this.filter);
  }
}
