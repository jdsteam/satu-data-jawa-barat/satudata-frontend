import {
  Component,
  OnInit,
  OnChanges,
  OnDestroy,
  SimpleChanges,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

// STYLE
import * as jds from '@styles';

// MODEL
import { LoginData, ClassificationIndicatorData } from '@models-v3';

// SERVICE
import { AuthenticationService } from '@services-v3';

// PLUGIN
import { Store, select } from '@ngrx/store';

// STORE
import {
  selectAllClassificationIndicator,
  selectIsLoadingList as selectIsLoadingListClassificationIndicator,
} from '@store-v3/classification-indicator/classification-indicator.selectors';
import { fromClassificationIndicatorActions } from '@store-v3/classification-indicator/classification-indicator.actions';

// JDS-BI
import { JdsBiScrollbarModule } from '@jds-bi/core';

import {
  assign,
  filter as lfilter,
  isEmpty,
  map,
  merge,
  keyBy,
  values,
  orderBy,
  forEach,
} from 'lodash';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

@Component({
  selector: 'app-filter-classification-indicator-v2',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    NgxSkeletonLoaderModule,
    JdsBiScrollbarModule,
  ],
  templateUrl: './classification-indicator.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ClassificationIndicatorComponent
  implements OnInit, OnChanges, OnDestroy
{
  satudataUser: LoginData;
  jds: any = jds;

  @Input() from: any;
  @Input() filterInputClassificationIndicator: any;
  @Input() filterData: any;
  @Output() filterOutputClassificationIndicator = new EventEmitter<any>();

  // Variable
  classPanel: string;
  filter = {
    currentPage: 1,
    classification_indicator: [],
  };

  // Data
  data: ClassificationIndicatorData[];
  data$: Observable<ClassificationIndicatorData[]>;
  isLoadingList$: Observable<boolean>;
  isError$: Observable<boolean>;
  isError = false;
  errorMessage: string;
  pagination: any;
  pages: any;

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private store: Store<any>,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
  }

  ngOnInit(): void {
    this.getAllClassificationIndicator();
  }

  ngOnChanges(changes: SimpleChanges): void {
    // eslint-disable-next-line no-restricted-syntax
    for (const propName in changes) {
      // eslint-disable-next-line no-prototype-builtins
      if (changes.hasOwnProperty(propName)) {
        if (propName === 'filterInputClassificationIndicator') {
          const chng = changes[propName];
          this.filter.classification_indicator = chng.currentValue;
          this.createChecklist();
        }
      }
    }

    switch (this.from) {
      case 'indicator-page':
        this.classPanel = `${this.jds.borderTop(0)} ${this.jds.roundedBottom(
          8,
        )}`;
        break;
      case 'organization-detail':
        this.classPanel = this.jds.rounded(8);
        break;
      case 'my-data-page':
        if (this.satudataUser.role_name === 'walidata') {
          this.classPanel = `${this.jds.borderTop(0)} ${this.jds.roundedBottom(
            8,
          )}`;
        } else if (
          this.satudataUser.role_name === 'opd' ||
          this.satudataUser.role_name === 'opd-view'
        ) {
          this.classPanel = this.jds.rounded(8);
        }
        break;
      default:
        this.classPanel = null;
        break;
    }
  }

  ngOnDestroy(): void {
    this.filter.classification_indicator = [];
  }

  getAllClassificationIndicator(): void {
    const pSort = 'name:asc';

    const params = {
      sort: pSort,
    };

    this.store.dispatch(
      fromClassificationIndicatorActions.loadAllClassificationIndicator({
        params,
        pagination: false,
        infinite: false,
      }),
    );

    this.isLoadingList$ = this.store.pipe(
      select(selectIsLoadingListClassificationIndicator),
    );

    this.store
      .pipe(
        select(selectAllClassificationIndicator),
        filter((val) => val.length !== 0),
      )
      .subscribe((result) => {
        this.data = result;
        this.createChecklist();
      });
  }

  createChecklist(): void {
    if (this.data === undefined) {
      return;
    }

    const modify: any = [];

    forEach(this.data, (resultOrder) => {
      modify.push({
        ...resultOrder,
        checked: false,
      });
    });

    let result: any;

    if (this.filter.classification_indicator.length) {
      const checked = [];
      map(this.filter.classification_indicator, (resultLine) => {
        checked.push({ name: resultLine, checked: true });
      });

      const merged = merge(keyBy(modify, 'name'), keyBy(checked, 'name'));
      const vals = values(merged);
      result = orderBy(vals, ['name'], ['asc']);
    } else {
      result = orderBy(modify, ['name'], ['asc']);
    }

    this.data = result;
  }

  filterClassificationIndicator(isChecked: boolean, data: any) {
    if (isChecked) {
      map(this.data, (result) => {
        if (result.name === data.name) {
          assign(result, { checked: true });
        }
      });
    } else {
      map(this.data, (result) => {
        if (result.name === data.name) {
          // eslint-disable-next-line no-param-reassign
          delete result.checked;
        }
      });
    }

    const hasil = map(lfilter(this.data, 'checked'), 'name').join(',');
    const value = isEmpty(hasil) === false ? hasil : null;

    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        classification_indicator: value,
        currentpage: null,
      },
      queryParamsHandling: 'merge',
    });

    const active = lfilter(this.data, 'checked');

    this.filter.classification_indicator = active;
    this.filter.currentPage = 1;
    this.filterOutputClassificationIndicator.emit(this.filter);
  }
}
