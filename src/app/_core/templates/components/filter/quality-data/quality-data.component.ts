import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { FormsModule } from '@angular/forms';

// STYLE
import * as jds from '@styles';

// CONSTANT
import { QUALITY_DATA } from '@constants-v4';

// JDS-BI
import { JdsBiScrollbarModule } from '@jds-bi/core';

// PLUGIN
import {
  assign,
  filter as _filter,
  forEach,
  isEmpty,
  keyBy,
  map as _map,
  merge,
  values,
} from 'lodash';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

@Component({
  selector: 'app-filter-quality-data',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    NgxSkeletonLoaderModule,
    JdsBiScrollbarModule,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './quality-data.component.html',
})
export class QualityDataComponent implements OnInit, OnChanges, OnDestroy {
  jds = jds;

  @Input() from: any;
  @Input() filterInputQualityData: any;
  @Input() filterData: any;
  @Output() filterOutputQualityData = new EventEmitter<any>();

  // Variable
  filter = {
    currentPage: 1,
    quality_data: [],
  };

  // Data
  data: any[];

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.getAllQualityData();
  }

  ngOnChanges(changes: SimpleChanges): void {
    // eslint-disable-next-line no-restricted-syntax
    for (const propName in changes) {
      // eslint-disable-next-line no-prototype-builtins
      if (changes.hasOwnProperty(propName)) {
        if (propName === 'filterInputQualityData') {
          const chng = changes[propName];
          this.filter.quality_data = chng.currentValue;
          this.createChecklist();
        }
      }
    }
  }

  ngOnDestroy(): void {
    this.filter.quality_data = [];
  }

  getAllQualityData(): void {
    this.data = Array.from(QUALITY_DATA, ([, value]) => ({
      id: value.id,
      name: value.name,
      slug: value.slug,
    }));

    this.createChecklist();
  }

  createChecklist(): void {
    if (this.data === undefined) {
      return;
    }

    const modify: any = [];

    forEach(this.data, (resultOrder) => {
      modify.push({
        ...resultOrder,
        checked: false,
      });
    });

    let result: any;

    if (this.filter.quality_data.length) {
      const checked: any[] = [];
      this.filter.quality_data.forEach((resultLine) => {
        checked.push({ slug: resultLine, checked: true });
      });

      const merged = merge(keyBy(modify, 'slug'), keyBy(checked, 'slug'));
      const value = values(merged);
      result = value;
    } else {
      result = modify;
    }

    this.data = result;
  }

  filterQualityData(isChecked: EventTarget | null, data: any): void {
    if (isChecked) {
      forEach(this.data, (result) => {
        if (result.slug === data.slug) {
          assign(result, { checked: true });
        }
      });
    } else {
      forEach(this.data, (result) => {
        if (result.slug === data.slug) {
          delete result['checked'];
        }
      });
    }

    const hasil = _map(_filter(this.data, 'checked'), 'slug').join(',');
    const value = isEmpty(hasil) === false ? hasil : null;

    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        quality_data: value,
        currentpage: null,
      },
      queryParamsHandling: 'merge',
    });

    const active = _filter(this.data, 'checked');

    this.filter.quality_data = active;
    this.filter.currentPage = 1;
    this.filterOutputQualityData.emit(this.filter);
  }
}
