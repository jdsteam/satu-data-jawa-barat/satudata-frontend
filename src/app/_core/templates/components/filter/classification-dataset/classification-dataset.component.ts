import {
  Component,
  OnInit,
  OnChanges,
  OnDestroy,
  SimpleChanges,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

// STYLE
import * as jds from '@styles';

// MODEL
import { ClassificationDatasetData } from '@models-v3';

// STORE
import {
  selectAllClassificationDataset,
  selectIsLoadingList as selectIsLoadingListClassificationDataset,
} from '@store-v3/classification-dataset/classification-dataset.selectors';
import { fromClassificationDatasetActions } from '@store-v3/classification-dataset/classification-dataset.actions';

// JDS-BI
import { JdsBiScrollbarModule } from '@jds-bi/core';

import {
  assign,
  filter as lfilter,
  isEmpty,
  map,
  merge,
  keyBy,
  values,
  orderBy,
  forEach,
} from 'lodash';
import { Store, select } from '@ngrx/store';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

@Component({
  selector: 'app-filter-classification-dataset-v2',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    NgxSkeletonLoaderModule,
    JdsBiScrollbarModule,
  ],
  templateUrl: './classification-dataset.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ClassificationDatasetComponent
  implements OnInit, OnChanges, OnDestroy
{
  jds: any = jds;

  @Input() from: any;
  @Input() filterInputClassificationDataset: any;
  @Input() filterData: any;
  @Output() filterOutputClassificationDataset = new EventEmitter<any>();

  // Variable
  classPanel: string;
  filter = {
    currentPage: 1,
    classification_dataset: [],
  };

  // Data
  data: ClassificationDatasetData[];
  data$: Observable<ClassificationDatasetData[]>;
  isLoadingList$: Observable<boolean>;
  isError$: Observable<boolean>;
  isError = false;
  errorMessage: string;
  pagination: any;
  pages: any;

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<any>,
  ) {}

  ngOnInit(): void {
    this.getAllClassificationDataset();
  }

  ngOnChanges(changes: SimpleChanges): void {
    // eslint-disable-next-line no-restricted-syntax
    for (const propName in changes) {
      // eslint-disable-next-line no-prototype-builtins
      if (changes.hasOwnProperty(propName)) {
        if (propName === 'filterInputClassificationDataset') {
          const chng = changes[propName];
          this.filter.classification_dataset = chng.currentValue;
          this.createChecklist();
        }
      }
    }

    switch (this.from) {
      case 'dataset-page':
        this.classPanel = `${this.jds.borderTop(0)} ${this.jds.rounded(0)}`;
        break;
      case 'visualization-page':
        this.classPanel = `${this.jds.borderTop(0)} ${this.jds.roundedBottom(
          8,
        )}`;
        break;
      case 'organization-detail':
      case 'my-data-page':
        if (this.filterData === 'dataset') {
          this.classPanel = `${this.jds.borderTop(0)} ${this.jds.rounded(0)}`;
        } else if (this.filterData === 'visualization') {
          this.classPanel = `${this.jds.borderTop(0)} ${this.jds.roundedBottom(
            8,
          )}`;
        }
        break;
      default:
        this.classPanel = null;
        break;
    }
  }

  ngOnDestroy(): void {
    this.filter.classification_dataset = [];
  }

  getAllClassificationDataset(): void {
    const pSort = 'name:asc';

    const params = {
      sort: pSort,
    };

    this.store.dispatch(
      fromClassificationDatasetActions.loadAllClassificationDataset({
        params,
        pagination: false,
        infinite: false,
      }),
    );

    this.isLoadingList$ = this.store.pipe(
      select(selectIsLoadingListClassificationDataset),
    );

    this.store
      .pipe(
        select(selectAllClassificationDataset),
        filter((val) => val.length !== 0),
      )
      .subscribe((result) => {
        this.data = result;
        this.createChecklist();
      });
  }

  createChecklist(): void {
    if (this.data === undefined) {
      return;
    }

    const modify: any = [];

    forEach(this.data, (resultOrder) => {
      modify.push({
        ...resultOrder,
        checked: false,
      });
    });

    let result: any;

    if (this.filter.classification_dataset.length) {
      const checked = [];
      map(this.filter.classification_dataset, (resultLine) => {
        checked.push({ id: Number(resultLine), checked: true });
      });

      const merged = merge(keyBy(modify, 'id'), keyBy(checked, 'id'));
      const vals = values(merged);
      result = orderBy(vals, ['name'], ['asc']);
    } else {
      result = orderBy(modify, ['name'], ['asc']);
    }

    this.data = result;
  }

  filterClassificationDataset(isChecked: boolean, data: any) {
    if (isChecked) {
      map(this.data, (result) => {
        if (result.id === data.id) {
          assign(result, { checked: true });
        }
      });
    } else {
      map(this.data, (result) => {
        if (result.id === data.id) {
          // eslint-disable-next-line no-param-reassign
          delete result.checked;
        }
      });
    }

    const hasil = map(lfilter(this.data, 'checked'), 'id').join(',');
    const value = isEmpty(hasil) === false ? hasil : null;

    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        classification_dataset: value,
        currentpage: null,
      },
      queryParamsHandling: 'merge',
    });

    const active = lfilter(this.data, 'checked');

    this.filter.classification_dataset = active;
    this.filter.currentPage = 1;
    this.filterOutputClassificationDataset.emit(this.filter);
  }
}
