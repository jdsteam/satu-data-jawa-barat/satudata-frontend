import {
  Component,
  OnInit,
  OnChanges,
  OnDestroy,
  SimpleChanges,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// MODEL
import { OrganizationData } from '@models-v3';

// STORE
import {
  selectAllOrganization,
  selectIsLoadingList as selectIsLoadingListOrganization,
} from '@store-v3/organization/organization.selectors';
import { fromOrganizationActions } from '@store-v3/organization/organization.actions';

// COMPONENT
import { JdsBiInputModule, JdsBiScrollbarModule } from '@jds-bi/core';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconMagnifyingGlass,
} from '@jds-bi/icons';

import {
  assign,
  filter as lfilter,
  isEmpty,
  map,
  merge,
  keyBy,
  values,
  orderBy,
  forEach,
} from 'lodash';
import { Store, select } from '@ngrx/store';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

@Component({
  selector: 'app-filter-organization-v2',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    JdsBiInputModule,
    JdsBiIconsModule,
    NgxSkeletonLoaderModule,
    JdsBiScrollbarModule,
  ],
  templateUrl: './organization.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrganizationComponent implements OnInit, OnChanges, OnDestroy {
  colorScheme = COLORSCHEME;
  jds: any = jds;

  @Input() from: any;
  @Input() filterInputOrganization: any;
  @Input() filterData: any;
  @Output() filterOutputOrganization = new EventEmitter<any>();

  // Variable
  classPanel: string;
  filter = {
    currentPage: 1,
    organization: [],
  };

  // Data
  data: OrganizationData[];
  dataTemp: OrganizationData[];
  data$: Observable<OrganizationData[]>;
  isLoadingList$: Observable<boolean>;
  isError$: Observable<boolean>;
  isError = false;
  errorMessage: string;
  pagination: any;
  pages: any;

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private iconService: JdsBiIconsService,
    private store: Store<any>,
  ) {
    this.iconService.registerIcons([iconMagnifyingGlass]);
  }

  ngOnInit(): void {
    this.getAllOrganization();
  }

  ngOnChanges(changes: SimpleChanges): void {
    // eslint-disable-next-line no-restricted-syntax
    for (const propName in changes) {
      // eslint-disable-next-line no-prototype-builtins
      if (changes.hasOwnProperty(propName)) {
        if (propName === 'filterInputOrganization') {
          const chng = changes[propName];
          this.filter.organization = chng.currentValue;
          this.createChecklist();
        }
      }
    }

    this.classPanel = this.jds.roundedTop(8);
  }

  ngOnDestroy(): void {
    this.filter.organization = [];
  }

  getAllOrganization(): void {
    const pSort = 'nama_skpd:asc';

    const pWhere = {
      is_satudata: true,
      is_deleted: false,
    };

    const params = {
      sort: pSort,
      where: JSON.stringify(pWhere),
    };

    this.store.dispatch(
      fromOrganizationActions.loadAllOrganization({
        params,
        pagination: false,
        infinite: false,
      }),
    );

    this.isLoadingList$ = this.store.pipe(
      select(selectIsLoadingListOrganization),
    );

    this.store
      .pipe(
        select(selectAllOrganization),
        filter((val) => val.length !== 0),
      )
      .subscribe((result) => {
        this.data = result;
        this.dataTemp = result;
        this.createChecklist();
      });
  }

  createChecklist(): void {
    if (this.data === undefined) {
      return;
    }

    const modify: any = [];

    forEach(this.data, (resultOrder) => {
      modify.push({
        ...resultOrder,
        checked: false,
      });
    });

    let result: any;

    if (this.filter.organization.length) {
      const checked = [];
      map(this.filter.organization, (resultLine) => {
        checked.push({ kode_skpd: resultLine, checked: true });
      });

      const merged = merge(
        keyBy(modify, 'kode_skpd'),
        keyBy(checked, 'kode_skpd'),
      );
      const vals = values(merged);
      result = orderBy(vals, ['nama_skpd'], ['asc']);
    } else {
      result = orderBy(modify, ['nama_skpd'], ['asc']);
    }

    this.data = result;
    this.dataTemp = result;
  }

  filterOrganization(isChecked: boolean, data: any) {
    if (isChecked) {
      map(this.data, (result) => {
        if (result.kode_skpd === data.kode_skpd) {
          assign(result, { checked: true });
        }
      });
    } else {
      map(this.data, (result) => {
        if (result.kode_skpd === data.kode_skpd) {
          // eslint-disable-next-line no-param-reassign
          delete result.checked;
        }
      });
    }

    const hasil = map(lfilter(this.data, 'checked'), 'kode_skpd').join(',');
    const value = isEmpty(hasil) === false ? hasil : null;

    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        organization: value,
        currentpage: null,
      },
      queryParamsHandling: 'merge',
    });

    const active = lfilter(this.data, 'checked');

    this.filter.organization = active;
    this.filter.currentPage = 1;
    this.filterOutputOrganization.emit(this.filter);
  }

  filterSearch(value: string): void {
    if (value === '') {
      this.dataTemp = this.data;
    }

    this.dataTemp = lfilter(this.data, (o) => {
      return o.nama_skpd.toLowerCase().indexOf(value) > -1;
    });
  }
}
