import {
  Component,
  OnInit,
  OnChanges,
  OnDestroy,
  SimpleChanges,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

// STYLE
import * as jds from '@styles';

// MODEL
import { DatasetTagData } from '@models-v3';

// STORE
import {
  selectAllDatasetTag,
  selectIsLoadingList as selectIsLoadingListDatasetTag,
} from '@store-v3/dataset-tag/dataset-tag.selectors';
import { fromDatasetTagActions } from '@store-v3/dataset-tag/dataset-tag.actions';

// JDS-BI
import { JdsBiScrollbarModule } from '@jds-bi/core';

import {
  assign,
  filter as lfilter,
  isEmpty,
  map,
  merge,
  keyBy,
  values,
  orderBy,
  forEach,
} from 'lodash';
import { Store, select } from '@ngrx/store';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

@Component({
  selector: 'app-filter-dataset-tag-v2',
  standalone: true,
  imports: [CommonModule, NgxSkeletonLoaderModule, JdsBiScrollbarModule],
  templateUrl: './dataset-tag.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DatasetTagComponent implements OnInit, OnChanges, OnDestroy {
  jds: any = jds;

  @Input() from: any;
  @Input() filterInputDatasetTag: any;
  @Input() filterData: any;
  @Output() filterOutputDatasetTag = new EventEmitter<any>();

  // Variable
  classPanel: string;
  filter = {
    currentPage: 1,
    dataset_tag: [],
  };

  // Data
  data: DatasetTagData[];
  data$: Observable<DatasetTagData[]>;
  isLoadingList$: Observable<boolean>;
  isError$: Observable<boolean>;
  isError = false;
  errorMessage: string;
  pagination: any;
  pages: any;

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<any>,
  ) {}

  ngOnInit(): void {
    this.getAllDatasetTag();
  }

  ngOnChanges(changes: SimpleChanges): void {
    // eslint-disable-next-line no-restricted-syntax
    for (const propName in changes) {
      // eslint-disable-next-line no-prototype-builtins
      if (changes.hasOwnProperty(propName)) {
        if (propName === 'filterInputDatasetTag') {
          const chng = changes[propName];
          this.filter.dataset_tag = chng.currentValue;
          this.createChecklist();
        }
      }
    }

    this.classPanel = `${this.jds.borderTop(0)} ${this.jds.rounded(0)}`;
  }

  ngOnDestroy(): void {
    this.filter.dataset_tag = [];
  }

  getAllDatasetTag(): void {
    const pSort = 'tag:asc';

    const pWhere = {};

    const params = {
      sort: pSort,
      distinct: true,
      where: JSON.stringify(pWhere),
    };

    this.store.dispatch(
      fromDatasetTagActions.loadAllDatasetTag({
        params,
        pagination: false,
        infinite: false,
      }),
    );

    this.isLoadingList$ = this.store.pipe(
      select(selectIsLoadingListDatasetTag),
    );

    this.store
      .pipe(
        select(selectAllDatasetTag),
        filter((val) => val.length !== 0),
      )
      .subscribe((result) => {
        this.data = result;
        this.createChecklist();
      });
  }

  createChecklist(): void {
    if (this.data === undefined) {
      return;
    }

    const modify: any = [];

    forEach(this.data, (resultOrder) => {
      modify.push({
        ...resultOrder,
        checked: false,
      });
    });

    let result: any;

    if (this.filter.dataset_tag.length) {
      const checked = [];
      map(this.filter.dataset_tag, (resultLine) => {
        checked.push({ tag: resultLine, checked: true });
      });

      const merged = merge(keyBy(modify, 'tag'), keyBy(checked, 'tag'));
      const vals = values(merged);
      result = orderBy(vals, ['tag'], ['asc']);
    } else {
      result = orderBy(modify, ['tag'], ['asc']);
    }

    this.data = result;
  }

  checkChecklist(checked: boolean): string {
    if (checked) {
      return 'active';
    }

    return '';
  }

  filterDatasetTag(data: any) {
    map(this.data, (result) => {
      if (result.tag === data.tag && result.checked) {
        // eslint-disable-next-line no-param-reassign
        delete result.checked;
      } else if (result.tag === data.tag) {
        assign(result, { checked: true });
      }
    });

    const hasil = map(lfilter(this.data, 'checked'), 'tag').join(',');
    const value = isEmpty(hasil) === false ? hasil : null;

    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        dataset_tag: value,
        currentpage: null,
      },
      queryParamsHandling: 'merge',
    });

    const active = lfilter(this.data, 'checked');

    this.filter.dataset_tag = active;
    this.filter.currentPage = 1;
    this.filterOutputDatasetTag.emit(this.filter);
  }
}
