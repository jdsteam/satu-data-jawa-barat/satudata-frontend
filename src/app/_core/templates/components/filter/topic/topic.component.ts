import {
  Component,
  OnInit,
  OnChanges,
  OnDestroy,
  SimpleChanges,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

// STYLE
import * as jds from '@styles';

// MODEL
import { LoginData, TopicData } from '@models-v3';

// SERVICE
import { AuthenticationService } from '@services-v3';

// STORE
import {
  selectAllTopic,
  selectIsLoadingList as selectIsLoadingListTopic,
} from '@store-v3/topic/topic.selectors';

// JDS-BI
import { JdsBiScrollbarModule } from '@jds-bi/core';

import {
  assign,
  filter as lfilter,
  isEmpty,
  map,
  merge,
  keyBy,
  values,
  orderBy,
  forEach,
} from 'lodash';
import { Store, select } from '@ngrx/store';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

@Component({
  selector: 'app-filter-topic-v2',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    NgxSkeletonLoaderModule,
    JdsBiScrollbarModule,
  ],
  templateUrl: './topic.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TopicComponent implements OnInit, OnChanges, OnDestroy {
  satudataUser: LoginData;
  jds: any = jds;

  @Input() from: any;
  @Input() filterInputTopic: any;
  @Input() filterData: any;
  @Output() filterOutputTopic = new EventEmitter<any>();

  // Variable
  classPanel: string;
  filter = {
    currentPage: 1,
    topic: [],
  };

  // Data
  data: TopicData[];
  data$: Observable<TopicData[]>;
  isLoadingList$: Observable<boolean>;
  isError$: Observable<boolean>;
  isError = false;
  errorMessage: string;
  pagination: any;
  pages: any;

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private store: Store<any>,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
  }

  ngOnInit(): void {
    this.getAllTopic();
  }

  ngOnChanges(changes: SimpleChanges): void {
    // eslint-disable-next-line no-restricted-syntax
    for (const propName in changes) {
      // eslint-disable-next-line no-prototype-builtins
      if (changes.hasOwnProperty(propName)) {
        if (propName === 'filterInputTopic') {
          const chng = changes[propName];
          this.filter.topic = chng.currentValue;
          this.createChecklist();
        }
      }
    }

    switch (this.from) {
      case 'dataset-page':
      case 'visualization-page':
        this.classPanel = `${this.jds.borderTop(0)} ${this.jds.rounded(0)}`;
        break;
      case 'organization-detail':
        if (this.filterData === 'dataset') {
          this.classPanel = this.jds.roundedTop(8);
        } else if (this.filterData === 'visualization') {
          this.classPanel = `${this.jds.roundedTop(8)} ${this.jds.roundedBottom(
            0,
          )}`;
        }
        break;
      case 'my-data-page':
        if (this.satudataUser.role_name === 'walidata') {
          this.classPanel = `${this.jds.borderTop(0)} ${this.jds.rounded(0)}`;
        } else if (
          this.satudataUser.role_name === 'opd' ||
          this.satudataUser.role_name === 'opd-view'
        ) {
          this.classPanel = this.jds.roundedTop(8);
        }
        break;
      default:
        this.classPanel = null;
        break;
    }
  }

  ngOnDestroy(): void {
    this.filter.topic = [];
  }

  getAllTopic(): void {
    this.isLoadingList$ = this.store.pipe(select(selectIsLoadingListTopic));

    this.store
      .pipe(
        select(selectAllTopic),
        filter((val) => val.length !== 0),
      )
      .subscribe((result) => {
        this.data = result;
        this.createChecklist();
      });
  }

  createChecklist(): void {
    if (this.data === undefined) {
      return;
    }

    const modify: any = [];

    forEach(this.data, (resultOrder) => {
      modify.push({
        ...resultOrder,
        checked: false,
      });
    });

    let result: any;

    if (this.filter.topic.length) {
      const checked = [];
      map(this.filter.topic, (resultLine) => {
        checked.push({ id: Number(resultLine), checked: true });
      });

      const merged = merge(keyBy(modify, 'id'), keyBy(checked, 'id'));
      const vals = values(merged);
      result = orderBy(vals, ['name'], ['asc']);
    } else {
      result = orderBy(modify, ['name'], ['asc']);
    }

    this.data = result;
  }

  filterTopic(isChecked: boolean, data: any) {
    if (isChecked) {
      map(this.data, (result) => {
        if (result.id === data.id) {
          assign(result, { checked: true });
        }
      });
    } else {
      map(this.data, (result) => {
        if (result.id === data.id) {
          // eslint-disable-next-line no-param-reassign
          delete result.checked;
        }
      });
    }

    const hasil = map(lfilter(this.data, 'checked'), 'id').join(',');
    const value = isEmpty(hasil) === false ? hasil : null;

    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        topic: value,
        currentpage: null,
      },
      queryParamsHandling: 'merge',
    });

    const active = lfilter(this.data, 'checked');

    this.filter.topic = active;
    this.filter.currentPage = 1;
    this.filterOutputTopic.emit(this.filter);
  }
}
