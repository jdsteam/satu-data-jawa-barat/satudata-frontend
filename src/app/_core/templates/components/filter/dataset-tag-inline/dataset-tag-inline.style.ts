import { Injectable } from '@angular/core';

// PACKAGE
import { css } from '@emotion/css';

@Injectable()
export class DatasetTagInlineStyle {
  getDynamicStyle(): any {
    const main = css`
      display: flex;
      align-items: center;
    `;

    const title = css`
      font-size: 14px;
      margin-right: 6px;
    `;

    const tags = css`
      flex-grow: 1;
      font-size: 14px;

      a {
        cursor: pointer;
      }
    `;

    const dropdown = css`
      ul {
        li {
          list-style: none;

          a {
            display: block;
            padding: 5px 15px;
            cursor: pointer;
            font-size: 14px;

            &:hover {
              color: #ffffff;
              background-color: #0753a6;
              background-image: unset;
            }
          }
        }
      }
    `;

    const more = css`
      margin-left: 6px;
      cursor: pointer;
    `;

    return {
      main,
      title,
      tags,
      dropdown,
      more,
    };
  }
}
