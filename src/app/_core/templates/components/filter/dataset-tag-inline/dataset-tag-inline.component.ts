import {
  Component,
  OnInit,
  OnChanges,
  OnDestroy,
  AfterViewInit,
  SimpleChanges,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  ViewChild,
  ViewChildren,
  ElementRef,
  QueryList,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { BehaviorSubject } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';

// STYLE
import * as jds from '@styles';

// COMPONENT
import { JdsBiDropdownModule, JdsBiHostedDropdownModule } from '@jds-bi/core';

import { assign, includes, isEmpty, isNil, map } from 'lodash';

import { DatasetTagInlineStyle } from './dataset-tag-inline.style';

@Component({
  selector: 'app-filter-dataset-tag-inline',
  standalone: true,
  imports: [CommonModule, JdsBiDropdownModule, JdsBiHostedDropdownModule],
  templateUrl: './dataset-tag-inline.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DatasetTagInlineStyle],
})
export class DatasetTagInlineComponent
  implements OnInit, OnChanges, OnDestroy, AfterViewInit
{
  jds: any = jds;

  @ViewChild('tags', { static: true }) tagsElement: ElementRef;
  @ViewChildren('text') textElements: QueryList<ElementRef>;

  className!: any;
  @Input() data: any;

  @Input() set filterInput(value: any) {
    this.filter = value;
  }
  get filterInput() {
    return this.filter;
  }

  @Output() filterOutputDatasetTag = new EventEmitter<any>();
  defaultInputs$ = new BehaviorSubject<any>({
    data: null,
  });

  // Variable
  public isDropdown = false;
  helper = true;
  dataHelper = {
    ori: null,
    show: null,
    hide: null,
  };

  filter: any;

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private datasetTagInlineStyle: DatasetTagInlineStyle,
  ) {}

  ngOnInit(): void {
    this.defaultInputs$.next({
      ...this.defaultInputs$.getValue(),
      ...this.checkInputs(),
    });
    this.className = this.datasetTagInlineStyle.getDynamicStyle();

    this.dataHelper.ori = this.data;
  }

  ngOnChanges(changes: SimpleChanges): void {
    const inputs = Object.keys(changes).reduce((result: any, item: any) => {
      // eslint-disable-next-line no-param-reassign
      result[item] = changes[item].currentValue;
      return result;
    }, {});

    this.defaultInputs$.next({ ...this.defaultInputs$.getValue(), ...inputs });
    this.className = this.datasetTagInlineStyle.getDynamicStyle();
  }

  ngOnDestroy(): void {
    this.filter.dataset_tag = [];
  }

  ngAfterViewInit(): void {
    const divWidth = this.tagsElement.nativeElement.clientWidth;

    let shouldSkip = false;
    let spanIndex = null;
    let spanAllWidth = 0;
    this.textElements.forEach((span: ElementRef, index: number) => {
      const spanWidth = span.nativeElement.clientWidth;
      spanAllWidth += spanWidth;

      if (shouldSkip) {
        return;
      }
      if (spanAllWidth > divWidth) {
        shouldSkip = true;
        return;
      }

      spanIndex = index;
    });

    if (this.textElements.length === spanIndex + 1) {
      this.dataHelper.show = this.data;
    } else {
      this.dataHelper.show = this.data.slice(0, spanIndex);
      this.dataHelper.hide = this.data.slice(spanIndex);
    }

    this.helper = false;

    this.cdRef.detectChanges();
  }

  checkInputs(): any {
    const inputs = {};

    if (!isNil(this.data)) {
      assign(inputs, { data: this.data });
    }

    return inputs;
  }

  filterDatasetTag(data: any) {
    const filter = this.filter.dataset_tag;
    if (includes(filter, data)) {
      return;
    }

    filter.push(data);
    const hasil = map(filter).join(',');
    const value = isEmpty(hasil) === false ? hasil : null;

    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        dataset_tag: value,
        currentpage: null,
      },
      queryParamsHandling: 'merge',
    });

    this.filter.dataset_tag = filter;
    this.filter.currentPage = 1;
    this.filterOutputDatasetTag.emit(this.filter);
  }
}
