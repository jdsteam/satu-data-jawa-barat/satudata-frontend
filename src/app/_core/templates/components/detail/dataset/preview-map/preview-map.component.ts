/* eslint-disable @typescript-eslint/dot-notation */
import {
  Component,
  OnInit,
  OnChanges,
  OnDestroy,
  AfterViewInit,
  Input,
  SimpleChanges,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  ViewRef,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Router, ActivatedRoute } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { Observable, Subject, of } from 'rxjs';
import { filter, takeUntil, take } from 'rxjs/operators';
import { select, Store } from '@ngrx/store';
import { Actions } from '@ngrx/effects';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';
// MODEL
import { LoginData, DatasetData } from '@models-v3';
// PIPE
import { SafeHtmlPipe } from '@pipes-v4';
// SERVICE
import {
  AuthenticationService,
  StateService,
  PaginationService,
  CoreDataService,
} from '@services-v3';

// COMPONENT
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconCircleInfo,
  iconMagnifyingGlass,
} from '@jds-bi/icons';

// STORE
import {
  selectDataset,
  selectIsLoadingRead,
} from '@store-v3/dataset/dataset.selectors';

import { orderBy, set } from 'lodash';
import moment from 'moment';
import * as L from 'leaflet';
import { fromWorker } from 'observable-webworker';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgSelectModule } from '@ng-select/ng-select';

import jsonKota from '@json/kota.json';
import { PreviewMapStyle } from './preview-map.style';

@Component({
  selector: 'app-detail-dataset-preview-map',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    JdsBiIconsModule,
    NgxSkeletonLoaderModule,
    NgSelectModule,
    SafeHtmlPipe,
  ],
  templateUrl: './preview-map.component.html',
  styleUrls: ['./preview-map.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [PreviewMapStyle],
})
export class PreviewMapComponent
  implements OnInit, OnChanges, OnDestroy, AfterViewInit
{
  satudataUser: LoginData;
  satudataToken: string;
  moment: any = moment;
  jds: any = jds;

  className!: any;

  @Input() accessCoreDataInput: any;
  @Input() accessInfoCoreDataInput: any;

  // Log
  log = null;

  datasetInfo = null;
  userInfo = null;

  // Variable
  id: number;
  title: string;
  accessCoreData: boolean;
  accessInfoCoreData: boolean;

  urlEndpoint: any;
  urlService: any;
  urlCoreData: string;
  schema: string;
  table: string;

  limit: number;

  filters = {
    map: 'polygon',
    axis1: null,
    axis2: null,
    group: null,
    group2: null,
  };

  mapOption: any[] = [
    { value: 'polygon', label: 'Polygon' },
    { value: 'marker', label: 'Marker' },
  ];
  axis1Option: any[] = [];
  axis2Option: any[] = [];
  groupOption: any[] = [];
  group2Option: any[] = [];

  private map: L.Map;
  geojsonKota = jsonKota;
  dataLayer = [];
  infoLegend = '';

  // Data
  data: DatasetData;
  jsonContent$: any;
  jsonContentMap$: any;
  coredata: any[];
  metadata: any[];
  isLoading = false;

  data$: Observable<DatasetData>;
  isLoadingRead$: Observable<boolean>;
  isLoadingUpdate$: Observable<boolean>;
  isError$: Observable<boolean>;
  isError = false;
  errorMessage: string;

  private unsubscribeDataset$ = new Subject<void>();

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private stateService: StateService,
    private paginationService: PaginationService,
    private coreDataService: CoreDataService,
    private iconService: JdsBiIconsService,
    private store: Store<any>,
    private actions$: Actions,
    private previewMapStyle: PreviewMapStyle,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
    this.authenticationService.satudataToken.subscribe(
      (x) => (this.satudataToken = x),
    );
    this.stateService.currentLogUser.subscribe((x) => (this.userInfo = x));

    this.id = this.route.snapshot.params.id;

    this.iconService.registerIcons([iconCircleInfo, iconMagnifyingGlass]);
  }

  ngOnInit(): void {
    this.className = this.previewMapStyle.getDynamicStyle();

    this.getDataset();
    this.logDatasetMap('init');
  }

  ngOnChanges(changes: SimpleChanges): void {
    // eslint-disable-next-line no-restricted-syntax
    for (const propName in changes) {
      // eslint-disable-next-line no-prototype-builtins
      if (changes.hasOwnProperty(propName)) {
        if (propName === 'accessCoreDataInput') {
          const chng = changes[propName];
          this.accessCoreData = chng.currentValue;
        }
        if (propName === 'accessInfoCoreDataInput') {
          const chng = changes[propName];
          this.accessInfoCoreData = chng.currentValue;
        }
      }
    }
  }

  ngOnDestroy() {
    this.logDatasetMap('destroy');
    this.unsubscribeDataset$.next();
    this.unsubscribeDataset$.complete();
  }

  ngAfterViewInit(): void {
    if (L) {
      this.setupMap();
    }
  }

  getDataset(): void {
    this.isLoadingRead$ = this.store.pipe(select(selectIsLoadingRead));

    this.store
      .pipe(
        takeUntil(this.unsubscribeDataset$),
        select(selectDataset(this.id)),
        filter((val) => val !== undefined),
        take(1),
      )
      .subscribe((result) => {
        this.data = result;
        this.schema = result.schema;
        this.table = result.table;
        this.urlCoreData = `/${this.schema}/${this.table}`;

        this.coreDataService
          .getListV2(this.urlCoreData, {})
          .subscribe((coreData) => {
            this.limit = coreData.meta.total_record;
            this.getOption();
          });
      });
  }

  setupMap(): void {
    // Create the map in the #map container
    this.map = L.map('map', {
      zoomControl: false,
      fullscreenControl: false,
    }).setView([-6.932694, 107.627449], 8);

    // Add a tilelayer
    L.tileLayer(
      'https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png',
      {
        attribution:
          '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy;' +
          '<a href="https://carto.com/attributions">CARTO</a>',
        maxZoom: 18,
        tileSize: 512,
        zoomOffset: -1,
      },
    ).addTo(this.map);

    L.icon({
      iconUrl: 'assets/images/marker-icon-2x.png',
      shadowUrl: 'assets/images/marker-shadow.png',
      iconSize: [25, 41],
      shadowSize: [20, 20],
      iconAnchor: [17, 41],
      shadowAnchor: [4, 20],
      popupAnchor: [-3, -41],
    });
  }

  setOption(data: any) {
    const option = [];
    // eslint-disable-next-line no-restricted-syntax
    for (const key of Object.keys(data)) {
      const temp = {
        value: key,
        label: this.titleCase(key),
        type: typeof data[key],
      };
      option.push(temp);
    }

    return orderBy(option, ['label'], ['asc']);
  }

  setOptionAxis1(type: string, options: any) {
    const axis1Option = [];

    if (type === 'marker') {
      axis1Option.push({ value: '', label: '-- Pilih --' });
      axis1Option.push({ value: 'latitude', label: 'Latitude' });

      this.filters.axis1 = 'latitude';
    } else if (type === 'polygon') {
      options.forEach((element) => {
        if (element.type === 'string') {
          if (element.value !== 'satuan') {
            axis1Option.push(element);
          }
        }
        if (element.value.includes('kode')) {
          axis1Option.push(element);
          this.filters.axis1 = element.value;
        }
      });
      axis1Option.unshift({ value: '', label: '-- Pilih --' });

      if (!this.filters.axis1) {
        this.filters.axis1 = axis1Option[1].value;
      }
    }

    this.axis1Option = axis1Option;
  }

  setOptionAxis2(type: string, options: any) {
    const axis2Option = [];

    if (type === 'marker') {
      axis2Option.push({ value: '', label: '-- Pilih --' });
      axis2Option.push({ value: 'longitude', label: 'Longitude' });

      this.filters.axis2 = 'longitude';
    } else if (type === 'polygon') {
      options.forEach((element) => {
        if (element.type === 'number') {
          axis2Option.push(element);
        }
      });
      axis2Option.unshift({ value: '', label: '-- Pilih --' });

      axis2Option.forEach((element) => {
        if (!element.value.indexOf('jumlah')) {
          this.filters.axis2 = element.value;
        }
      });

      if (!this.filters.axis2) {
        if (axis2Option[axis2Option.length - 1].value === 'tahun') {
          this.filters.axis2 = axis2Option[axis2Option.length - 2].value;
        } else {
          this.filters.axis2 = axis2Option[axis2Option.length - 1].value;
        }
      }
    }

    this.axis2Option = axis2Option;
  }

  setOptionGroup(options: any) {
    const groupOption = [];
    options.forEach((element) => {
      groupOption.push(element);
      if (element.value === 'tahun') {
        this.filters.group = 'tahun';
      }
    });
    groupOption.unshift({ value: '', label: '-- Pilih --' });
    this.groupOption = groupOption;
  }

  getOption() {
    const params = {
      limit: this.limit,
    };

    this.isLoading = true;
    this.coreDataService
      .getListV2(this.urlCoreData, params)
      .subscribe((result) => {
        this.jsonContent$ = result.data;

        // Option
        const options = this.setOption(result.data[0]);

        let isLat = false;
        let isLong = false;

        options.forEach((element) => {
          if (element.value.toLowerCase().includes('lat')) {
            isLat = true;
          }
          if (element.value.toLowerCase().includes('long')) {
            isLong = true;
          }
        });

        // Set Group
        this.setOptionGroup(options);

        // Set Filter
        const group2Option = [
          ...new Set(this.jsonContent$.map((item) => item[this.filters.group])),
        ];
        this.group2Option = group2Option;
        // eslint-disable-next-line prefer-destructuring
        this.filters.group2 = group2Option[0];

        if (isLat && isLong) {
          this.filters.map = 'marker';

          // Set Axis 1
          this.setOptionAxis1(this.filters.map, options);

          // Set Axis 2
          this.setOptionAxis2(this.filters.map, options);

          this.loadMapMarker();
        } else {
          this.filters.map = 'polygon';

          // Set Axis 1
          this.setOptionAxis1(this.filters.map, options);

          // Set Axis 2
          this.setOptionAxis2(this.filters.map, options);

          this.loadMapPolygon();
        }

        this.isLoading = false;
      });
  }

  loadMapPolygon() {
    const params = {
      schema: this.schema,
      table: this.table,
      filter_key: this.filters.group,
      filter_value: this.filters.group2,
      group_kode: this.filters.axis1,
      group_value: this.filters.axis2,
    };

    this.coreDataService.getGeoJSON(params).subscribe((result) => {
      this.jsonContentMap$ = result;

      this.previewMapPolygon();
      this.createLegendPolygon();
    });
  }

  previewMapPolygon() {
    // add layer to map
    L.geoJSON(this.geojsonKota, {
      onEachFeature: (feature, layer) => {
        // set style layer
        let styleRegionBorderLine = {
          fillOpacity: 0.2,
          fillColor: '#FFFFFF',
          weight: 1,
          opacity: 1,
          color: '#000000',
        };

        // set popup layer
        let popup = '';
        popup += `<b> ${this.titleCase(
          this.filters.group,
        )} </b> : ${this.titleCase(this.filters.group2.toString())} <br>`;
        popup += `<b> Kota/Kabupaten </b> : ${this.titleCase(
          feature.properties.kemendagri_kabupaten_nama,
        )} <br>`;
        popup += `<b> ${this.titleCase(this.filters.axis2)} </b> : `;

        // set data polygon
        // eslint-disable-next-line no-restricted-syntax
        for (const row of this.jsonContentMap$['data']['info']) {
          if (
            row[this.filters.axis1].toString() ===
            feature.properties.bps_kabupaten_kode.toString()
          ) {
            styleRegionBorderLine = {
              fillOpacity: 0.8,
              fillColor: row.color,
              weight: 1,
              opacity: 1,
              color: '#000000',
            };
            popup += this.numberThousand(row[this.filters.axis2].toString());
            popup += '<br>';
          }
        }
        layer.setStyle(styleRegionBorderLine);
        const polygon = layer.addTo(this.map);
        this.dataLayer.push(polygon);
        layer.bindTooltip(popup);
      },
    });

    if (
      this.cdRef !== null &&
      this.cdRef !== undefined &&
      !(this.cdRef as ViewRef).destroyed
    ) {
      this.cdRef.detectChanges();
    }
  }

  createLegendPolygon() {
    let detailLabel = '';
    // eslint-disable-next-line prefer-destructuring
    const range = this.jsonContentMap$['data']['range'];
    const labels = [
      `<label class="${jds.formLabel}">Informasi: </label>`,
      `<div class="${jds.row}">`,
    ];

    range.forEach((e) => {
      detailLabel = `<div class="${jds.col(3)} ${jds.mt(8)}">`;
      detailLabel += `<div class="${jds.d(
        'flex',
      )}" style="flex-direction: column">`;
      detailLabel += `<div class="${jds.d('flex')} ${jds.alignItems(
        'start',
      )}">`;
      detailLabel += `<i class="legend-box" style="background: ${e.color}; position: relative; top: 5px; opacity: 1; width: 13px; min-width: 13px; height: 13px; float: left; margin-right: 8px; border: black 1px solid;"></i>`;
      detailLabel += `${this.numberThousand(e.from)} - ${this.numberThousand(
        e.to,
      )}`;
      detailLabel += `</div>`;
      detailLabel += `<div class="${jds.fs(12)}" style="margin-left: 21px;">(${
        e.total_cluster
      } wilayah)</div>`;
      detailLabel += `</div>`;
      detailLabel += `</div>`;
      labels.push(detailLabel);
    });

    labels.push('</div>');
    this.infoLegend = labels.join('');

    if (
      this.cdRef !== null &&
      this.cdRef !== undefined &&
      !(this.cdRef as ViewRef).destroyed
    ) {
      this.cdRef.detectChanges();
    }
  }

  loadMapMarker() {
    setTimeout(() => {
      this.jsonContent$.forEach((row) => {
        if (
          row.latitude !== '' &&
          row.latitude !== 'null' &&
          row.latitude !== null &&
          row.longitude !== '' &&
          row.longitude !== 'null' &&
          row.longitude !== null
        ) {
          // icom
          const defaulticon = L.icon({
            iconUrl: 'assets/images/marker-icon-2x.png',
            shadowUrl: 'assets/images/marker-shadow.png',
            iconSize: [25, 41],
            shadowSize: [20, 20],
            iconAnchor: [17, 41],
            shadowAnchor: [4, 20],
            popupAnchor: [-3, -41],
          });
          const layerList = L.marker(
            [parseFloat(row.latitude), parseFloat(row.longitude)],
            {
              icon: defaulticon,
            },
          );

          // popup
          const maxHeight = 200;
          const maxWidth = 350;
          let popupHtml = `<style>
              table{
                border-collapse: collapse;
                width: 100%;
              }
              td, th {
                border: 1px solid #dddddd;
                text-align: left;
                padding: 8px;
              }
              tr:nth-child(even) {
                background-color: #dddddd;
              }
              </style>`;
          popupHtml += '<table>';
          const ordered = Object.keys(row)
            .sort((a, b) => a[0].localeCompare(b[0]))
            .reduce((obj, key) => {
              // eslint-disable-next-line no-param-reassign
              obj[key] = row[key];
              return obj;
            }, {});
          Object.keys(ordered).forEach((key) => {
            popupHtml +=
              `<tr><td style="width: 42%; vertical-align: top;"><b>${this.titleCase(
                key,
              )}</b></td>` +
              `<td style="vertical-align: top;">${row[key]}</td></tr>`;
          });
          popupHtml += '</table>';
          layerList.bindPopup(popupHtml, { maxHeight, maxWidth });

          // add to map
          layerList.addTo(this.map);
          this.dataLayer.push(layerList);
        }
      });

      if (
        this.cdRef !== null &&
        this.cdRef !== undefined &&
        !(this.cdRef as ViewRef).destroyed
      ) {
        this.cdRef.detectChanges();
      }
    }, 100);
  }

  changeMap(): void {
    this.removeLayer();
    setTimeout(() => {
      if (this.filters.map === 'polygon') {
        this.loadMapPolygon();
      } else if (this.filters.map === 'marker') {
        this.loadMapMarker();
      }
    }, 100);
  }

  changeGroup(): void {
    this.group2Option = [];
    this.group2Option = [
      ...new Set(this.jsonContent$.map((item) => item[this.filters.group])),
    ];
    // eslint-disable-next-line prefer-destructuring
    this.filters.group2 = this.group2Option[0];
  }

  removeLayer(): void {
    this.dataLayer.forEach((element) => {
      this.map.removeLayer(element);
    });
    this.dataLayer = [];
  }

  titleCase(str: string): string {
    const splitStr = str.toLowerCase().split('_');
    for (let i = 0; i < splitStr.length; i += 1) {
      splitStr[i] =
        splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    return splitStr.join(' ');
  }

  numberThousand(x): void {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  }

  logDatasetMap(type: string): void {
    if (type === 'init') {
      this.datasetInfo = {
        date_start: moment().format('YYYY-MM-DD HH:mm:ss'),
        dataset_id: this.data.id,
        dataset_title: this.data.title,
        dataset_name: this.data.name,
        dataset_organization_code: this.data.skpd.kode_skpd,
        dataset_organization_name: this.data.skpd.nama_skpd,
      };

      this.log = {
        log: { ...this.datasetInfo, ...this.userInfo },
        token: this.satudataToken,
      };
    } else if (type === 'destroy') {
      if (typeof Worker !== 'undefined') {
        set(this.log.log, 'date_end', moment().format('YYYY-MM-DD HH:mm:ss'));

        fromWorker<object, string>(
          () =>
            new Worker(
              new URL(
                'src/app/__v4/workers/log-dataset-preview-map.worker',
                import.meta.url,
              ),
              {
                type: 'module',
                name: 'dataset-preview-map',
              },
            ),
          of(this.log),
        ).subscribe((message) => {
          if (!environment.production) {
            console.info(`Log dataset preview map: ${message}`);
          }
        });
      }
    }
  }
}
