import {
  Component,
  OnInit,
  OnChanges,
  OnDestroy,
  Input,
  SimpleChanges,
  ViewChild,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  ViewRef,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Router, ActivatedRoute } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { Observable, Subject, of } from 'rxjs';
import { filter, takeUntil, take } from 'rxjs/operators';
import { select, Store } from '@ngrx/store';
import { Actions } from '@ngrx/effects';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';

// SERVICE
import {
  AuthenticationService,
  StateService,
  PaginationService,
  CoreDataService,
} from '@services-v3';

// MODEL
import { LoginData, DatasetData } from '@models-v3';

// STORE
import { selectDataset } from '@store-v3/dataset/dataset.selectors';

// COMPONENT
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconCircleInfo,
  iconMagnifyingGlass,
} from '@jds-bi/icons';

import { groupBy, mapValues, orderBy, set } from 'lodash';
import moment from 'moment';
import { ChartOptions, ChartType } from 'chart.js';
import { NgChartsModule, BaseChartDirective } from 'ng2-charts';
import { fromWorker } from 'observable-webworker';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxPopperjsModule } from 'ngx-popperjs';

import { PreviewGraphStyle } from './preview-graph.style';

@Component({
  selector: 'app-detail-dataset-preview-graph',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    NgChartsModule,
    JdsBiIconsModule,
    NgxSkeletonLoaderModule,
    NgSelectModule,
    NgxPopperjsModule,
  ],
  templateUrl: './preview-graph.component.html',
  styleUrls: ['./preview-graph.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [PreviewGraphStyle],
})
export class PreviewGraphComponent implements OnInit, OnChanges, OnDestroy {
  satudataUser: LoginData;
  satudataToken: string;
  moment: any = moment;
  jds: any = jds;

  className!: any;

  @ViewChild(BaseChartDirective) chart: BaseChartDirective;

  @Input() accessCoreDataInput: any;
  @Input() accessInfoCoreDataInput: any;

  // Log
  log = null;

  datasetInfo = null;
  userInfo = null;

  // Variable
  id: number;
  title: string;
  accessCoreData: boolean;
  accessInfoCoreData: boolean;
  urlCoreData: string;
  limit: number;

  filter = {
    graph: 'bar',
    axis1: null,
    axis2: null,
    group: null,
  };

  graphOption: any[] = [
    { value: 'bar', label: 'Bar Chart' },
    { value: 'line', label: 'Line Chart' },
  ];
  axis1Option: any[] = [];
  axis2Option: any[] = [];
  groupOption: any[] = [];

  backgroundColor = [
    '#BBCC32',
    '#EDDD88',
    '#75ABE1',
    '#EF8867',
    '#ABAA00',
    '#FEAABA',
    '#99DEFF',
    '#44BC99',
    '#BEC0C0',
    '#A78FDA',
  ];

  // Data
  data: DatasetData;
  coredata: any[];
  meta: any;
  metadata: any[];
  isLoading = false;

  data$: Observable<DatasetData>;
  isLoadingRead$: Observable<boolean>;
  isLoadingUpdate$: Observable<boolean>;
  isError$: Observable<boolean>;
  isError = false;
  errorMessage: string;

  private unsubscribeDataset$ = new Subject<void>();

  // Chart
  public myChartOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    plugins: {
      legend: {
        position: 'bottom',
      },
    },
    scales: {
      y: {
        title: {
          display: true,
          text: 'Axis B',
        },
        beginAtZero: true,
      },
      x: {
        title: {
          display: true,
          text: 'Axis A',
        },
        ticks: {
          callback(_, index) {
            let label = this.getLabelForValue(index);

            if (typeof label === 'string') {
              // eslint-disable-next-line no-param-reassign
              label = label.replace('KABUPATEN', 'KAB.');
              return label.substring(0, 20);
            }
            return label;
          },
        },
      },
    },
  };
  public myChartType: ChartType = 'bar';
  public myChartLegend = true;
  public myChartLabels = [];
  public myChartData: any[] = [{ data: [] }];
  public myChartHeight = '150px';

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private stateService: StateService,
    private paginationService: PaginationService,
    private coreDataService: CoreDataService,
    private iconService: JdsBiIconsService,
    private store: Store<any>,
    private actions$: Actions,
    private previewGraphStyle: PreviewGraphStyle,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
    this.authenticationService.satudataToken.subscribe(
      (x) => (this.satudataToken = x),
    );
    this.stateService.currentLogUser.subscribe((x) => (this.userInfo = x));

    this.id = this.route.snapshot.params.id;

    this.stateService.currentDatasetDetail.subscribe((result: any) => {
      this.urlCoreData = result.url;
      this.meta = result.meta;
    });

    this.iconService.registerIcons([iconCircleInfo, iconMagnifyingGlass]);
  }

  ngOnInit(): void {
    this.className = this.previewGraphStyle.getDynamicStyle();

    this.getOption();
    this.getDataset();
    this.logDatasetGraph('init');
  }

  ngOnChanges(changes: SimpleChanges): void {
    // eslint-disable-next-line no-restricted-syntax
    for (const propName in changes) {
      // eslint-disable-next-line no-prototype-builtins
      if (changes.hasOwnProperty(propName)) {
        if (propName === 'accessCoreDataInput') {
          const chng = changes[propName];
          this.accessCoreData = chng.currentValue;
        }
        if (propName === 'accessInfoCoreDataInput') {
          const chng = changes[propName];
          this.accessInfoCoreData = chng.currentValue;
        }
      }
    }
  }

  ngOnDestroy() {
    this.logDatasetGraph('destroy');
    this.unsubscribeDataset$.next();
    this.unsubscribeDataset$.complete();
  }

  setOption(data: any) {
    const option = [];
    // eslint-disable-next-line no-restricted-syntax
    for (const key of Object.keys(data)) {
      const temp = {
        value: key,
        label: this.titleCase(key),
        type: typeof data[key],
      };
      option.push(temp);
    }

    return orderBy(option, ['label'], ['asc']);
  }

  setOptionAxis1(options: any) {
    const axis1Option = [];
    options.forEach((element) => {
      if (element.type === 'string') {
        if (element.value !== 'satuan') {
          axis1Option.push(element);
        }
      }
    });
    axis1Option.unshift({ value: '', label: '-- Pilih --' });
    this.axis1Option = axis1Option;
    this.filter.axis1 = axis1Option[1].value;
  }

  setOptionAxis2(options: any) {
    const axis2Option = [];
    options.forEach((element) => {
      if (element.type === 'number') {
        axis2Option.push(element);
      }
    });
    axis2Option.unshift({ value: '', label: '-- Pilih --' });
    this.axis2Option = axis2Option;

    axis2Option.forEach((element) => {
      if (!element.value.indexOf('jumlah')) {
        this.filter.axis2 = element.value;
      }
    });

    if (!this.filter.axis2) {
      if (axis2Option[axis2Option.length - 1].value === 'tahun') {
        this.filter.axis2 = axis2Option[axis2Option.length - 2].value;
      } else {
        this.filter.axis2 = axis2Option[axis2Option.length - 1].value;
      }
    }
  }

  setOptionGroup(options: any) {
    const groupOption = [];
    options.forEach((element) => {
      groupOption.push(element);
      if (element.value === 'tahun') {
        this.filter.group = 'tahun';
      }
    });
    groupOption.unshift({ value: '', label: '-- Pilih --' });
    this.groupOption = groupOption;
  }

  getOption() {
    const params = {
      limit: this.meta.total_record,
    };

    this.isLoading = true;
    this.coreDataService
      .getListV2(this.urlCoreData, params)
      .subscribe((result) => {
        this.coredata = result.data;

        // Option
        const options = this.setOption(result.data[0]);

        // Set Axis 1
        this.setOptionAxis1(options);

        // Set Axis 2
        this.setOptionAxis2(options);

        // Set Group
        this.setOptionGroup(options);

        this.isLoading = false;
        this.generateChart();
      });
  }

  generateChart() {
    let datas: any;
    this.myChartData = [];
    this.myChartLabels = [];

    if (this.filter.group) {
      const grouped = mapValues(groupBy(this.coredata, this.filter.group));
      datas = grouped;

      let i = -1;
      // eslint-disable-next-line no-restricted-syntax
      for (const key of Object.keys(datas)) {
        i += 1;
        const tempLabel = [];
        const tempData = {
          data: [],
          label: key,
          fill: true,
          backgroundColor: this.backgroundColor[i % 10],
          hoverBackgroundColor: this.backgroundColor[i % 10],
          borderColor: this.backgroundColor[i % 10],
          hoverBorderColor: this.backgroundColor[i % 10],
        };

        if (this.filter.graph === 'line') {
          tempData.fill = false;
        }

        datas[key].forEach((element) => {
          tempLabel.push(element[this.filter.axis1]);
          tempData.data.push(element[this.filter.axis2]);
        });

        this.myChartData.push(tempData);
        this.myChartLabels = tempLabel;
      }
    } else {
      datas = this.coredata;

      const tempLabel = [];
      const tempData = {
        data: [],
        label: this.titleCase(this.filter.axis2),
        fill: true,
        backgroundColor: this.backgroundColor[0],
        hoverBackgroundColor: this.backgroundColor[0],
        borderColor: this.backgroundColor[0],
        hoverBorderColor: this.backgroundColor[0],
      };

      if (this.filter.graph === 'line') {
        tempData.fill = false;
      }

      datas.forEach((element) => {
        tempLabel.push(element[this.filter.axis1]);
        tempData.data.push(element[this.filter.axis2]);
      });

      this.myChartData.push(tempData);
      this.myChartLabels = tempLabel;
    }

    if (
      this.cdRef !== null &&
      this.cdRef !== undefined &&
      !(this.cdRef as ViewRef).destroyed
    ) {
      this.cdRef.detectChanges();
    }
  }

  changeChart() {
    this.generateChart();
    this.myChartType = this.filter.graph as ChartType;
    this.chart.update();
    this.cdRef.detectChanges();
  }

  titleCase(str: string): string {
    const splitStr = str.toLowerCase().split('_');
    for (let i = 0; i < splitStr.length; i += 1) {
      splitStr[i] =
        splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    return splitStr.join(' ');
  }

  getDataset() {
    this.store
      .pipe(
        takeUntil(this.unsubscribeDataset$),
        select(selectDataset(this.id)),
        filter((val) => val !== undefined),
        take(1),
      )
      .subscribe((result) => {
        this.data = result;
      });
  }

  logDatasetGraph(type: string): void {
    if (type === 'init') {
      this.datasetInfo = {
        date_start: moment().format('YYYY-MM-DD HH:mm:ss'),
        dataset_id: this.data.id,
        dataset_title: this.data.title,
        dataset_name: this.data.name,
        dataset_organization_code: this.data.skpd.kode_skpd,
        dataset_organization_name: this.data.skpd.nama_skpd,
      };

      this.log = {
        log: { ...this.datasetInfo, ...this.userInfo },
        token: this.satudataToken,
      };
    } else if (type === 'destroy') {
      if (typeof Worker !== 'undefined') {
        set(this.log.log, 'date_end', moment().format('YYYY-MM-DD HH:mm:ss'));

        fromWorker<object, string>(
          () =>
            new Worker(
              new URL(
                'src/app/__v4/workers/log-dataset-preview-graph.worker',
                import.meta.url,
              ),
              {
                type: 'module',
                name: 'dataset-preview-graph',
              },
            ),
          of(this.log),
        ).subscribe((message) => {
          if (!environment.production) {
            console.info(`Log dataset preview graph: ${message}`);
          }
        });
      }
    }
  }
}
