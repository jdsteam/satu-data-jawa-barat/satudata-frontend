import { Injectable } from '@angular/core';

// PACKAGE
import { css } from '@emotion/css';

@Injectable()
export class PreviewGraphStyle {
  getDynamicStyle(): any {
    const filter = css`
      .ng-select {
        .ng-select-container {
          height: 36px;
          min-height: 36px;

          .ng-value-container {
            .ng-input {
              top: 50%;
              transform: translateY(-50%);
            }
          }
        }
      }
    `;

    return {
      filter,
    };
  }
}
