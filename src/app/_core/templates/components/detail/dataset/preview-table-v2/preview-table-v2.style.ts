import { Injectable } from '@angular/core';

// PACKAGE
import { css } from '@emotion/css';

@Injectable()
export class PreviewTableV2Style {
  getDynamicStyle(): any {
    const main = css`
      .ng-dropdown-panel {
        min-width: 300px !important;

        .ng-dropdown-header {
          padding: 0;
          border-bottom: unset;

          .ng-dropdown-search {
            padding: 8px 10px;

            input[type='text'] {
              height: 30px;
              border: 1px solid #dedfe0;
              background: #fff;
            }
          }
        }

        .ng-dropdown-panel-items {
          .ng-option {
            white-space: unset;
          }
        }
      }
    `;

    const table = css`
      margin-bottom: 0;

      > :not(caption) > * > * {
        border-bottom-width: 0;
        padding: 8px 16px;
      }

      > thead {
        background-color: #0753a6;
        color: #ffffff;
      }
    `;

    const columnId = css`
      width: 10%;
    `;

    const column50 = css`
      min-width: 400px;
    `;

    const column40 = css`
      min-width: 300px;
    `;

    const column30 = css`
      min-width: 200px;
    `;

    const columnShort = css`
      min-width: 100px;
    `;

    const search = css`
      input {
        height: 30px;
        border: 1px solid #dedfe0;
        background: #fff;
      }
    `;

    const select = css`
      .ng-select.ng-select-multiple {
        .ng-select-container {
          height: 30px;
          min-height: 30px;

          .ng-value-container {
            flex-wrap: nowrap;
            position: unset;

            .ng-placeholder {
              top: unset;
            }

            .ng-input {
              top: 50%;
              transform: translateY(-50%);
            }
          }

          &.ng-has-value {
            height: 30px;

            .ng-value {
              line-height: 18px;
            }
          }
        }

        &.ng-select-disabled > .ng-select-container {
          background-color: #c1d4e8;
        }
      }
    `;

    return {
      main,
      table,
      columnId,
      column50,
      column40,
      column30,
      columnShort,
      search,
      select,
    };
  }
}
