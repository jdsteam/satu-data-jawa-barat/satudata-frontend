import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  ViewChild,
  ElementRef,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { FormsModule, NgModel } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { takeUntil, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { select, Store } from '@ngrx/store';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// SERVICE
import { AuthenticationService, StateService } from '@services-v3';

// MODEL
import { LoginData } from '@models-v3';

// COMPONENT
import { TableSelectPositioningDirective } from '@directives-v4';
import {
  JdsBiInputModule,
  JdsBiTableHeadModule,
  JdsBiPaginationModule,
} from '@jds-bi/core';

// STORE
import {
  selectAllCoreData,
  selectIsLoadingList,
  selectPagination,
} from '@store-v3/core-data/core-data.selectors';
import { fromCoreDataActions } from '@store-v3/core-data/core-data.actions';

// eslint-disable-next-line object-curly-newline
import { assign, findIndex, forEach, isEmpty, isEqual, map } from 'lodash';
import moment from 'moment';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgSelectModule } from '@ng-select/ng-select';

import { PreviewTableV2Style } from './preview-table-v2.style';

@Component({
  selector: 'app-detail-dataset-preview-table-v2',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    NgxSkeletonLoaderModule,
    NgSelectModule,
    TableSelectPositioningDirective,
    JdsBiInputModule,
    JdsBiTableHeadModule,
    JdsBiPaginationModule,
  ],
  templateUrl: './preview-table-v2.component.html',
  styleUrls: ['./preview-table-v2.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [PreviewTableV2Style],
})
export class PreviewTableV2Component implements OnInit, OnDestroy {
  colorScheme = COLORSCHEME;
  satudataUser: LoginData;
  moment: any = moment;
  jds: any = jds;

  @ViewChild('main', { read: ElementRef }) mainEl: ElementRef;
  @ViewChild('checkAll') checkAllEl: ElementRef;

  className!: any;

  // Variable
  id: number;
  urlCoreData: string;

  filter = {
    perPage: 10,
    currentPage: 1,
    search: [],
    select: [],
    selectAll: false,
    sort: null,
    directory: null,
  };
  searchTextChanged$: Subject<string> = new Subject<string>();

  perPageItems: any[] = [
    { value: 10, label: '10' },
    { value: 25, label: '25' },
  ];

  // Data
  coredata: any[];
  metadata = [];

  data$: Observable<any[]>;
  isLoadingList$: Observable<boolean>;
  isLoadingUpdate$: Observable<boolean>;
  isError$: Observable<boolean>;
  isError = false;
  errorMessage: string;
  pagination$: Observable<any>;

  private unsubscribeCoreData$ = new Subject<void>();

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private stateService: StateService,
    private store: Store<any>,
    private previewTableStyle: PreviewTableV2Style,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    this.id = this.route.snapshot.params.id;

    this.stateService.currentDatasetDetail.subscribe((result: any) => {
      this.urlCoreData = result.url;
      this.metadata = result.metadata;
      this.filter.search = result.search;
      this.filter.select = result.select;
    });

    this.searchTextChanged$
      .pipe(debounceTime(1000), distinctUntilChanged())
      .subscribe(() => {
        this.getCoreData();
      });
  }

  ngOnInit(): void {
    this.className = this.previewTableStyle.getDynamicStyle();

    this.getCoreData();
  }

  ngOnDestroy() {
    this.unsubscribeCoreData$.next();
    this.unsubscribeCoreData$.complete();
  }

  getCoreData() {
    const pCurrent =
      Number(this.filter.perPage) * Number(this.filter.currentPage) -
      Number(this.filter.perPage);

    const pWhere = {};

    if (this.filter.select) {
      forEach(this.filter.select, (res) => {
        if (Array.isArray(res.value) && !isEmpty(res.value) && !res.all) {
          assign(pWhere, { [res.column]: res.value });
        }
      });
    }

    const pWhereOr = {};

    if (this.filter.search) {
      forEach(this.filter.search, (res) => {
        if (res.value) {
          assign(pWhereOr, { [res.column]: res.value });
        }
      });
    }

    const params = {
      limit: this.filter.perPage,
      skip: pCurrent,
    };

    if (this.filter.sort) {
      assign(params, {
        sort: `${this.filter.sort}:${this.filter.directory}`,
      });
    }

    if (!isEmpty(pWhere)) {
      assign(params, { where: JSON.stringify(pWhere) });
    }

    if (!isEmpty(pWhereOr)) {
      assign(params, { where_or: JSON.stringify(pWhereOr) });
    }

    this.store.dispatch(
      fromCoreDataActions.loadAllCoreData({
        url: this.urlCoreData,
        params,
        pagination: true,
        infinite: false,
      }),
    );

    this.isLoadingList$ = this.store.pipe(select(selectIsLoadingList));

    this.pagination$ = this.store.pipe(select(selectPagination));

    this.data$ = this.store.pipe(
      takeUntil(this.unsubscribeCoreData$),
      select(selectAllCoreData),
    );
  }

  filterSort(disabled: boolean, event: any) {
    if (disabled) {
      return;
    }

    const value = isEmpty(event) === false ? event : null;

    let dir = '';
    if (this.filter.sort === value) {
      if (this.filter.directory === 'desc') {
        dir = 'asc';
      } else if (this.filter.directory === 'asc') {
        dir = 'desc';
      }
    } else {
      dir = 'asc';
    }

    this.filter.sort = value;
    this.filter.directory = dir;
    this.filter.currentPage = 1;
    this.getCoreData();
  }

  getFilterIndex(type: string, column: string) {
    let filter: any;
    if (type === 'search') {
      filter = this.filter.search;
    } else if (type === 'select') {
      filter = this.filter.select;
    }

    return findIndex(filter, (obj: any) => obj.column === column);
  }

  filterSearch(column: string, event: any) {
    const value = isEmpty(event) === false ? event.target.value : null;
    const objIndex = this.getFilterIndex('search', column);

    this.filter.search[objIndex].value = value;
    this.filter.currentPage = 1;
    this.searchTextChanged$.next(value);
  }

  filterSelect(column: string, options: any, event: any) {
    const objIndex = this.getFilterIndex('select', column);

    const original = map(options, (item) => item.value).sort((a, b) =>
      a[0].localeCompare(b[0]),
    );
    const selected = map(event, (item) => item.value).sort((a, b) =>
      a[0].localeCompare(b[0]),
    );

    this.filter.select[objIndex].all = isEqual(original, selected);
    this.filter.select[objIndex].value = selected;
    this.filter.currentPage = 1;
    this.getCoreData();
  }

  filterSelectAll(ngSelect: NgModel, column: string, options: any, event: any) {
    const objIndex = this.getFilterIndex('select', column);

    if (event) {
      const selected = options.map((item) => item.value);
      this.filter.select[objIndex].all = true;
      this.filter.select[objIndex].value = selected;
      ngSelect.update.emit(selected);
    } else {
      this.filter.select[objIndex].all = false;
      this.filter.select[objIndex].value = [];
      ngSelect.update.emit([]);
    }

    this.filter.currentPage = 1;
    this.getCoreData();
  }

  filterPerPage(perPage: number) {
    this.filter.perPage = perPage;
    this.filter.currentPage = 1;
    this.filter = { ...this.filter };
    this.getCoreData();
  }

  filterPage(page: number): void {
    this.filter.currentPage = page;
    this.filter = { ...this.filter };
    this.getCoreData();
  }
}
