import { Injectable } from '@angular/core';

// PACKAGE
import { css } from '@emotion/css';

@Injectable()
export class ReviewV2Style {
  getDynamicStyle(): any {
    const main = css`
      display: block;
    `;

    return {
      main,
    };
  }
}
