import {
  Component,
  OnInit,
  OnChanges,
  OnDestroy,
  Input,
  SimpleChanges,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';

// STYLE
import * as jds from '@styles';
import { jdsBiColor } from '@jds-bi/cdk';

// MODEL
import { LoginData, DatasetData, ReviewDatasetData } from '@models-v3';

// SERVICE
import { AuthenticationService } from '@services-v3';

// COMPONENT
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconComments,
  iconStar,
} from '@jds-bi/icons';

// STORE
import {
  selectDataset,
  selectIsLoadingRead,
} from '@store-v3/dataset/dataset.selectors';

import {
  selectAllReviewDataset,
  selectIsLoadingList as selectIsLoadingListReviewDataset,
} from '@store-v3/review-dataset/review-dataset.selectors';
import { fromReviewDatasetActions } from '@store-v3/review-dataset/review-dataset.actions';

import moment from 'moment';
import { Store, select } from '@ngrx/store';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

import { ReviewV2Style } from './review-v2.style';

@Component({
  selector: 'app-detail-dataset-review-v2',
  standalone: true,
  imports: [CommonModule, JdsBiIconsModule, NgxSkeletonLoaderModule],
  templateUrl: './review-v2.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [ReviewV2Style],
})
export class ReviewV2Component implements OnInit, OnChanges, OnDestroy {
  satudataUser: LoginData;
  moment: any = moment;
  jds: any = jds;
  jdsBiColor = jdsBiColor;

  className!: any;
  @Input() accessCoreDataInput!: string;
  @Input() accessInfoCoreDataInput!: string;
  defaultInputs$ = new BehaviorSubject<any>({
    accessCoreDataInput: null,
    accessInfoCoreDataInput: null,
  });

  // Variable
  id: number;
  title: string;
  tableauViz: any;
  accessCoreData: boolean;
  accessInfoCoreData: boolean;

  // Data
  data$: Observable<DatasetData>;
  isLoadingRead$: Observable<boolean>;
  isLoadingUpdate$: Observable<boolean>;
  isError$: Observable<boolean>;
  isError = false;
  errorMessage: string;

  reviewData$: Observable<ReviewDatasetData[]>;
  reviewIsLoadingList$: Observable<boolean>;
  reviewIsError$: Observable<boolean>;
  reviewIsError = false;
  reviewErrorMessage: string;

  private unsubscribeDataset$ = new Subject<void>();

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private iconService: JdsBiIconsService,
    private store: Store<any>,
    private reviewStyle: ReviewV2Style,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    this.id = this.route.snapshot.params.id;

    this.iconService.registerIcons([iconComments, iconStar]);
  }

  ngOnInit(): void {
    this.defaultInputs$.next({
      ...this.defaultInputs$.getValue(),
      ...this.checkInputs(),
    });
    this.className = this.reviewStyle.getDynamicStyle();

    this.getDataset();
    this.getAllReviewDataset();
  }

  ngOnChanges(changes: SimpleChanges): void {
    const inputs = Object.keys(changes).reduce((result: any, item: any) => {
      // eslint-disable-next-line no-param-reassign
      result[item] = changes[item].currentValue;
      return result;
    }, {});

    this.defaultInputs$.next({ ...this.defaultInputs$.getValue(), ...inputs });
    this.className = this.reviewStyle.getDynamicStyle();
  }

  ngOnDestroy() {
    this.unsubscribeDataset$.next();
    this.unsubscribeDataset$.complete();
  }

  checkInputs(): any {
    return {};
  }

  getDataset() {
    this.isLoadingRead$ = this.store.pipe(select(selectIsLoadingRead));

    this.data$ = this.store.pipe(select(selectDataset(this.id)));
  }

  getAllReviewDataset(): void {
    const pWhere = {
      type: 'dataset',
      type_id: this.id,
    };

    const params = {
      sort: 'cdate:desc',
      where: JSON.stringify(pWhere),
    };

    this.store.dispatch(
      fromReviewDatasetActions.loadAllReviewDataset({
        params,
        pagination: false,
        infinite: false,
      }),
    );

    this.reviewIsLoadingList$ = this.store.pipe(
      select(selectIsLoadingListReviewDataset),
    );

    this.reviewData$ = this.store.pipe(select(selectAllReviewDataset));
  }
}
