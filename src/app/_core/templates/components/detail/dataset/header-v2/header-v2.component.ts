import {
  Component,
  OnInit,
  OnChanges,
  OnDestroy,
  Input,
  SimpleChanges,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  ViewChild,
  viewChild,
  inject,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Router, ActivatedRoute } from '@angular/router';
import { BehaviorSubject, Observable, Subject, of } from 'rxjs';
import { filter, take, takeUntil } from 'rxjs/operators';
import { HttpEventType } from '@angular/common/http';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';
// COMPONENT
import { ModalModule, ModalService } from '@components-v3/modal';
import { DatasetActiveArchiveComponent } from '@components-v3/modal/dataset-active-archive/dataset-active-archive.component';
import {
  DropdownDatasetActionComponent,
  ModalDatasetStatusComponent,
} from '@components-v4';
// CONSTANT
import { COLORSCHEME, STATUS_DATASET } from '@constants-v4';
// MODEL
import { LoginData, DatasetData } from '@models-v3';
// SERVICE
import {
  AuthenticationService,
  StateService,
  HttpService,
  DatasetService as DatasetOldService,
  CoreDataService,
  NotificationService,
} from '@services-v3';
import { DatasetService } from '@services-v4';
// STORE
import {
  selectDataset,
  selectIsLoadingRead,
} from '@store-v3/dataset/dataset.selectors';
import { fromDatasetActions } from '@store-v3/dataset/dataset.actions';
// TYPE
import type {
  DropdownDatasetActionDelete,
  DropdownDatasetActionArchive,
  DropdownDatasetActionActive,
  DropdownDatasetActionRecover,
  DropdownDatasetActionDiscontinue,
} from '@components-v4';
import type { StatusDataset } from '@types-v4';
// UTIL
import { mappingStatusDataset } from '@utils-v4';
// JDS-BI
import { jdsBiColor } from '@jds-bi/cdk';
import {
  JdsBiBadgeModule,
  JdsBiDropdownModule,
  JdsBiHostedDropdownModule,
} from '@jds-bi/core';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconBook,
  iconCalendar,
  iconChevronDown,
  iconCircleCheck,
  iconClockRotateLeft,
  iconClockThree,
  iconEllipsis,
  iconFileTimer,
} from '@jds-bi/icons';

import { assign, isNil } from 'lodash';
import moment from 'moment';
import { Store, select } from '@ngrx/store';
import Swal from 'sweetalert2';
import { saveAs } from 'file-saver';
import { fromWorker } from 'observable-webworker';
import { NgxLoadingModule } from 'ngx-loading';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { SubscribeDirective } from '@ngneat/subscribe';
import { useMutationResult } from '@ngneat/query';

import { HeaderV2Style } from './header-v2.style';

@Component({
  selector: 'app-detail-dataset-header-v2',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    SubscribeDirective,
    JdsBiBadgeModule,
    JdsBiIconsModule,
    ModalModule,
    NgxLoadingModule,
    NgxSkeletonLoaderModule,
    DatasetActiveArchiveComponent,
    DropdownDatasetActionComponent,
    ModalDatasetStatusComponent,
    JdsBiDropdownModule,
    JdsBiHostedDropdownModule,
  ],
  templateUrl: './header-v2.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [HeaderV2Style],
})
export class HeaderV2Component implements OnInit, OnChanges, OnDestroy {
  satudataUser: LoginData;
  satudataToken: string;
  colorScheme = COLORSCHEME;
  moment = moment;
  jds = jds;
  jdsBiColor = jdsBiColor;

  @ViewChild(DatasetActiveArchiveComponent)
  datasetActiveArchive: DatasetActiveArchiveComponent;

  // Modal
  modalDatasetStatusComponent = viewChild(ModalDatasetStatusComponent);

  // Service
  private datasetService = inject(DatasetService);
  editDatasetMutation = useMutationResult();

  className!: any;
  @Input() accessCoreDataInput!: string;
  @Input() accessInfoCoreDataInput!: string;
  defaultInputs$ = new BehaviorSubject<any>({
    accessCoreDataInput: null,
    accessInfoCoreDataInput: null,
  });

  // Log
  datasetLog = null;

  datasetInfo = null;
  userInfo = null;

  // Variable
  public isDropdownDownload = false;
  public isDropdownAction = false;
  id: number;
  title: string;
  tableauViz: any;
  accessCoreData: boolean;
  accessInfoCoreData: boolean;

  // Data
  data: DatasetData;
  data$: Observable<DatasetData>;
  isLoadingRead$: Observable<boolean>;
  isLoadingUpdate$: Observable<boolean>;
  isError$: Observable<boolean>;
  isError = false;
  errorMessage: string;

  downloadIsLoading = false;
  downloadName: string;
  downloadProgress = 0;

  private unsubscribeDataset$ = new Subject<void>();

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private stateService: StateService,
    private httpService: HttpService,
    private datasetOldService: DatasetOldService,
    private coreDataService: CoreDataService,
    private notificationService: NotificationService,
    private modalService: ModalService,
    private iconService: JdsBiIconsService,
    private store: Store<any>,
    private headerStyle: HeaderV2Style,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
    this.authenticationService.satudataToken.subscribe(
      (x) => (this.satudataToken = x),
    );
    this.stateService.currentLogUser.subscribe((x) => (this.userInfo = x));

    this.id = this.route.snapshot.params.id;

    this.iconService.registerIcons([
      iconBook,
      iconCalendar,
      iconChevronDown,
      iconCircleCheck,
      iconClockRotateLeft,
      iconClockThree,
      iconEllipsis,
      iconFileTimer,
    ]);
  }

  ngOnInit(): void {
    this.defaultInputs$.next({
      ...this.defaultInputs$.getValue(),
      ...this.checkInputs(),
    });
    this.className = this.headerStyle.getDynamicStyle();

    this.getDataset();
  }

  ngOnChanges(changes: SimpleChanges): void {
    const inputs = Object.keys(changes).reduce((result: any, item: any) => {
      if (item === 'accessCoreDataInput') {
        this.accessCoreData = changes[item].currentValue;
      }

      if (item === 'accessInfoCoreDataInput') {
        this.accessInfoCoreData = changes[item].currentValue;
      }

      // eslint-disable-next-line no-param-reassign
      result[item] = changes[item].currentValue;
      return result;
    }, {});

    this.defaultInputs$.next({ ...this.defaultInputs$.getValue(), ...inputs });
    this.className = this.headerStyle.getDynamicStyle();
  }

  ngOnDestroy() {
    this.unsubscribeDataset$.next();
    this.unsubscribeDataset$.complete();
  }

  checkInputs(): any {
    const inputs = {};

    if (!isNil(this.accessCoreDataInput)) {
      assign(inputs, { accessCoreDataInput: this.accessCoreDataInput });
    }

    if (!isNil(this.accessInfoCoreDataInput)) {
      assign(inputs, {
        accessInfoCoreDataInput: this.accessInfoCoreDataInput,
      });
    }

    return inputs;
  }

  getDataset() {
    this.isLoadingRead$ = this.store.pipe(select(selectIsLoadingRead));

    this.data$ = this.store.pipe(select(selectDataset(this.id)));

    this.store
      .pipe(
        select(selectDataset(this.id)),
        filter((val) => val !== undefined),
        take(1),
      )
      .subscribe((result) => {
        this.data = result;
      });
  }

  getClassificationColor(id: number) {
    return this.datasetOldService.getClassificationColor(id);
  }

  getSource(isRealtime: boolean, isPermanent: boolean) {
    return this.datasetOldService.getSource(isRealtime, isPermanent);
  }

  getType(value: string) {
    let type: string;
    let blob: string;
    let extension: string;

    if (value === 'pdf') {
      type = 'pdf';
      blob = 'application/pdf';
      extension = 'pdf';
    } else if (value === 'csv' || value === 'excel') {
      if (value === 'csv') {
        type = 'csvzip';
      } else if (value === 'excel') {
        type = 'xlszip';
      }
      blob = 'application/octet-stream';
      extension = 'zip';
    }

    return {
      type,
      blob,
      extension,
    };
  }

  getDimension(metadata: any) {
    return this.datasetOldService.getMetadataDimension(metadata);
  }

  getQualityScoreColor(score: number) {
    return this.datasetOldService.getQualityScoreColor(score);
  }

  getStatusCatalog(isValidate: number, isActive: boolean) {
    let status: string;

    switch (isValidate) {
      case 0:
        status = 'draft';
        break;
      case 3:
        if (isActive) {
          status = 'active';
        } else {
          status = 'archive';
        }
        break;
      case 5:
        status = 'delete';
        break;
      default:
        break;
    }

    return status;
  }

  getAccessAction(statusCatalog: string) {
    switch (statusCatalog) {
      case 'draft':
      case 'active':
      case 'archive':
        return true;
      default:
        return false;
    }
  }

  onDownload(type: string) {
    const fileType = this.getType(type);

    this.store
      .pipe(
        takeUntil(this.unsubscribeDataset$),
        select(selectDataset(this.id)),
        filter((val) => val !== undefined),
        take(1),
      )
      .subscribe((result) => {
        this.downloadProgress = 0;

        this.openModal('modal-download');

        const { id, schema, table } = result;
        const urlService = `/${schema}/${table}`;
        const urlFile = `${environment.backendURL}bigdata/${schema}/${table}`;

        const params = {
          dataset_id: id,
          download: fileType.type,
          timestamp: new Date().valueOf(),
        };

        // Hit Backend Notification Trigger
        if (this.satudataUser.kode_skpd !== result.skpd.kode_skpd) {
          this.notificationService.getTrigger(id).toPromise();
        }

        const paramsCount = {
          count: true,
        };

        this.coreDataService
          .getListV2(urlService, paramsCount)
          .subscribe((limit) => {
            assign(params, { limit: limit.meta.total_record });
            this.httpService.getFile(urlFile, params).subscribe((response) => {
              if (response.type === HttpEventType.DownloadProgress) {
                this.downloadIsLoading = true;
                this.downloadName = result.name;
                this.downloadProgress = Math.round(
                  (100 * response.loaded) / response.total,
                );
                this.cdRef.markForCheck();
              } else if (response.type === HttpEventType.Response) {
                const newBlob = new Blob([response.body], {
                  type: fileType.blob,
                });

                setTimeout(() => {
                  this.onDownloadCounter();
                  this.closeModal('modal-download');
                  this.downloadIsLoading = false;

                  saveAs(newBlob, `${table}.${fileType.extension}`);

                  this.logDataset(type);

                  Swal.fire({
                    type: 'success',
                    title: 'Unduh dataset berhasil.',
                    text: 'Dataset yang telah diunduh dapat dilihat kembali di halaman riwayat.',
                    allowOutsideClick: false,
                    showCancelButton: true,
                    cancelButtonText: 'Tutup',
                    confirmButtonColor: '#217EBA',
                    confirmButtonText: 'Lihat Riwayat',
                  }).then((res) => {
                    if (res.value) {
                      this.router.navigate(['/history'], {
                        queryParams: {
                          status: 'access',
                        },
                      });
                    } else {
                      this.getDataset();
                    }
                  });
                }, 1000);
              }
            });
          });
      });
  }

  onAccessAPI() {
    this.store
      .pipe(
        takeUntil(this.unsubscribeDataset$),
        select(selectDataset(this.id)),
        filter((val) => val !== undefined),
        take(1),
      )
      .subscribe((result) => {
        const { id, schema, table } = result;
        const urlService = `/${schema}/${table}`;
        const urlFile = `${environment.backendURL}bigdata/${schema}/${table}`;

        const params = {
          dataset_id: id,
        };

        // Hit Backend Notification Trigger
        if (this.satudataUser.kode_skpd !== result.skpd.kode_skpd) {
          this.notificationService.getTrigger(id).toPromise();
        }

        this.onDownloadCounter();
        this.httpService.getAPI(urlService, params);

        this.logDataset('api');

        window.open(`${urlFile}/doc`, '_blank');
      });
  }

  onDownloadCounter() {
    this.store.dispatch(
      fromDatasetActions.counterDataset({
        id: this.id,
        params: {
          category: 'access_private',
        },
      }),
    );
  }

  onArchive(data: DropdownDatasetActionArchive) {
    this.modalDatasetStatusComponent().openModal(data.id);
  }

  onActive(data: DropdownDatasetActionActive) {
    this.modalDatasetStatusComponent().openModal(data.id);
  }

  onDelete(obj: DropdownDatasetActionDelete) {
    Swal.fire({
      type: 'warning',
      text: 'Apakah Anda yakin untuk menghapus Dataset ini?',
      allowOutsideClick: false,
      showCancelButton: true,
      cancelButtonText: 'Batal',
      confirmButtonColor: '#d33',
      confirmButtonText: 'Hapus',
    }).then((result) => {
      if (result.value) {
        const data = {
          type: 'dataset',
          type_id: Number(obj.id),
          category: obj.category,
          notes: null,
        };
        this.onUpdate(data);
      }
    });
  }

  onRecover(obj: DropdownDatasetActionRecover) {
    Swal.fire({
      type: 'warning',
      text: 'Apakah Anda yakin untuk memulihkan Dataset ini?',
      allowOutsideClick: false,
      showCancelButton: true,
      cancelButtonText: 'Batal',
      confirmButtonColor: jdsBiColor.blue['700'],
      confirmButtonText: 'Pulihkan',
    }).then((result) => {
      if (result.value) {
        const data = {
          type: 'dataset',
          type_id: Number(obj.id),
          category: 'recover' as const,
          lastCategory: obj.lastCategory,
          notes: null,
        };
        this.onUpdate(data);
      }
    });
  }

  onDiscontinue(obj: DropdownDatasetActionDiscontinue) {
    Swal.fire({
      type: 'warning',
      text: 'Apakah Anda yakin untuk mendiskontinu Dataset ini?',
      allowOutsideClick: false,
      showCancelButton: true,
      cancelButtonText: 'Batal',
      confirmButtonColor: jdsBiColor.blue['700'],
      confirmButtonText: 'Diskontinu',
    }).then((result) => {
      if (result.value) {
        const data = {
          type: 'dataset',
          type_id: Number(obj.id),
          category: obj.category,
          notes: null,
        };
        this.onUpdate(data);
      }
    });
  }

  onUpdate(formData: {
    type: string;
    type_id: number;
    category: StatusDataset | 'recover';
    lastCategory?: null | string;
    notes?: null | string;
  }) {
    const { category, lastCategory } = formData;

    let text;
    let status;
    let categoryHistory;
    switch (category) {
      case 'approve-active':
        text = 'diaktifkan';
        status = 'active';
        categoryHistory = 'approve';
        break;
      case 'approve-archive':
        text = 'diarsipkan';
        status = 'archive';
        categoryHistory = 'archive';
        break;
      case 'delete-active':
        text = 'dihapus';
        status = 'delete';
        categoryHistory = 'delete-active';
        break;
      case 'delete-delete':
        text = 'dihapus';
        status = 'delete';
        categoryHistory = 'delete-delete';
        break;
      case 'discontinue-active':
        text = 'didiskontinu';
        categoryHistory = 'discontinue-active';
        break;
      case 'discontinue-archive':
        text = 'didiskontinu';
        categoryHistory = 'discontinue-archive';
        break;
      case 'recover':
        text = 'dipulihkan';
        status = 'delete';
        categoryHistory = lastCategory;
        break;
      default:
        break;
    }

    const bodyDataset = {
      id: formData.type_id,
      history_draft: { ...formData, category: categoryHistory },
    };

    if (category !== 'recover') {
      assign(bodyDataset, STATUS_DATASET.get(category));
    } else {
      assign(
        bodyDataset,
        STATUS_DATASET.get(mappingStatusDataset(lastCategory)),
      );
    }

    if (
      category === 'discontinue-active' ||
      category === 'discontinue-archive'
    ) {
      // TODO: activate if backend discontinue date submit and approve ready
      // const bodyDataset = { is_discontinue: 1, discontinue_submit_date: new Date() };

      // TODO: change if backend discontinue date submit and approve ready
      assign(bodyDataset, { is_discontinue: 1 });
    }

    this.datasetService
      .updateItem(bodyDataset.id, bodyDataset)
      .pipe(this.editDatasetMutation.track())
      .subscribe(() => {
        Swal.fire({
          type: 'success',
          text: `Dataset berhasil ${text}`,
          allowOutsideClick: false,
        }).then(() => {
          if (
            category === 'discontinue-active' ||
            category === 'discontinue-archive'
          ) {
            this.router.navigate(['/desk'], {
              queryParams: { type: 'discontinue' },
            });
          } else {
            this.router.navigate(['/my-data'], {
              queryParams: { catalog: 'dataset', status },
            });
          }
        });
      });
  }

  openModal(id: string) {
    this.modalService.open(id);
  }

  closeModal(id: string) {
    this.modalService.close(id);
  }

  logDataset(type: string): void {
    if (typeof Worker !== 'undefined') {
      this.datasetInfo = {
        type,
        date_start: moment().format('YYYY-MM-DD HH:mm:ss'),
        date_end: moment().format('YYYY-MM-DD HH:mm:ss'),
        dataset_id: this.data.id,
        dataset_title: this.data.title,
        dataset_name: this.data.name,
        dataset_organization_code: this.data.skpd.kode_skpd,
        dataset_organization_name: this.data.skpd.nama_skpd,
      };

      this.datasetLog = {
        log: { ...this.datasetInfo, ...this.userInfo },
        token: this.satudataToken,
      };

      fromWorker<object, string>(
        () =>
          new Worker(
            new URL(
              'src/app/__v4/workers/log-download-dataset.worker',
              import.meta.url,
            ),
            {
              type: 'module',
              name: 'download-dataset',
            },
          ),
        of(this.datasetLog),
      ).subscribe((message) => {
        if (!environment.production) {
          console.info(`Log download dataset: ${message}`);
        }
      });
    }
  }
}
