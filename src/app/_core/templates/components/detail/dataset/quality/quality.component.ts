import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

// STYLE
import * as jds from '@styles';

// MODEL
import { DatasetData } from '@models-v3';

// SERVICE
import { DatasetService } from '@services-v3';

// COMPONENT
import { jdsBiColor } from '@jds-bi/cdk';
import { JdsBiAccordionModule } from '@jds-bi/core';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconCircleInfo,
} from '@jds-bi/icons';

// STORE
import { selectDataset } from '@store-v3/dataset/dataset.selectors';

import { Store, select } from '@ngrx/store';
import { NgxPopperjsModule } from 'ngx-popperjs';

@Component({
  selector: 'app-detail-dataset-quality',
  standalone: true,
  imports: [
    CommonModule,
    JdsBiAccordionModule,
    JdsBiIconsModule,
    NgxPopperjsModule,
  ],
  templateUrl: './quality.component.html',
  styleUrls: ['./quality.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class QualityComponent implements OnInit {
  jds: any = jds;
  jdsBiColor: any = jdsBiColor;

  // Variable
  id: number;

  // Data
  data$: Observable<DatasetData>;

  constructor(
    private route: ActivatedRoute,
    private iconService: JdsBiIconsService,
    private datasetService: DatasetService,
    private store: Store<any>,
  ) {
    this.id = this.route.snapshot.params.id;

    this.iconService.registerIcons([iconCircleInfo]);
  }

  ngOnInit(): void {
    this.getDataset();
  }

  getDataset() {
    this.data$ = this.store.pipe(select(selectDataset(this.id)));
  }

  getQualityScoreColor(score: number) {
    return this.datasetService.getQualityScoreColor(score);
  }

  getQualityScoreDetail(result: any) {
    return [
      { name: 'Conformity', score: result.conformity },
      { name: 'Uniqueness', score: result.uniqueness },
      { name: 'Consistency', score: result.consistency },
      { name: 'Timeliness', score: result.timeliness },
      { name: 'Completeness', score: result.completeness },
    ];
  }
}
