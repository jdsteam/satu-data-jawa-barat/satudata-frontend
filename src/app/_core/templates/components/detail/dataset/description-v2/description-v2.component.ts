import {
  Component,
  OnInit,
  OnChanges,
  OnDestroy,
  Input,
  SimpleChanges,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { filter } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';
import { jdsBiColor } from '@jds-bi/cdk';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// MODEL
import { LoginData, DatasetData, MetadataDatasetData } from '@models-v3';
// PIPE
import { SafeHtmlPipe } from '@pipes-v4';
// SERVICE
import { AuthenticationService, DatasetService } from '@services-v3';

// COMPONENT
import { QualityComponent } from '@components-v3/detail/dataset/quality/quality.component';
import { JdsBiNavModule } from '@jds-bi/core';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconEnvelope,
  iconPhone,
} from '@jds-bi/icons';

// STORE
import {
  selectDataset,
  selectIsLoadingRead,
} from '@store-v3/dataset/dataset.selectors';

import { selectAllMetadataBusinessDataset } from '@store-v3/metadata-business-dataset/metadata-business-dataset.selectors';
import { fromMetadataBusinessDatasetActions } from '@store-v3/metadata-business-dataset/metadata-business-dataset.actions';

import { selectAllMetadataCustomDataset } from '@store-v3/metadata-custom-dataset/metadata-custom-dataset.selectors';
import { fromMetadataCustomDatasetActions } from '@store-v3/metadata-custom-dataset/metadata-custom-dataset.actions';

import { findIndex, isEmpty } from 'lodash';
import moment from 'moment';
import { Store, select } from '@ngrx/store';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

import { DescriptionV2Style } from './description-v2.style';

@Component({
  selector: 'app-detail-dataset-description-v2',
  standalone: true,
  imports: [
    CommonModule,
    JdsBiNavModule,
    JdsBiIconsModule,
    NgxSkeletonLoaderModule,
    SafeHtmlPipe,
    QualityComponent,
  ],
  templateUrl: './description-v2.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DescriptionV2Style],
})
export class DescriptionV2Component implements OnInit, OnChanges, OnDestroy {
  env = environment;
  colorScheme = COLORSCHEME;
  satudataUser: LoginData;
  moment: any = moment;
  jds: any = jds;
  jdsBiColor = jdsBiColor;

  className!: any;
  @Input() accessCoreDataInput!: string;
  @Input() accessInfoCoreDataInput!: string;
  defaultInputs$ = new BehaviorSubject<any>({
    accessCoreDataInput: null,
    accessInfoCoreDataInput: null,
  });

  // Variable
  activeNav = 1;
  id: number;
  filter = {
    component: 'description',
  };

  // Data
  data$: Observable<DatasetData>;
  isLoadingRead$: Observable<boolean>;
  isLoadingUpdate$: Observable<boolean>;
  isError$: Observable<boolean>;
  isError = false;
  errorMessage: string;

  metadataBusinessData: MetadataDatasetData[];
  metadataBusinessData$: Observable<MetadataDatasetData[]>;
  metadataBusinessIsLoadingList$: Observable<boolean>;
  metadataBusinessIsError$: Observable<boolean>;
  metadataBusinessIsError = false;
  metadataBusinessErrorMessage: string;

  metadataCustomData$: Observable<MetadataDatasetData[]>;
  metadataCustomIsLoadingList$: Observable<boolean>;
  metadataCustomIsError$: Observable<boolean>;
  metadataCustomIsError = false;
  metadataCustomErrorMessage: string;

  private unsubscribeDataset$ = new Subject<void>();

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private datasetService: DatasetService,
    private iconService: JdsBiIconsService,
    private store: Store<any>,
    private descriptionV2Style: DescriptionV2Style,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    this.id = this.route.snapshot.params.id;

    this.iconService.registerIcons([iconEnvelope, iconPhone]);
  }

  ngOnInit(): void {
    this.defaultInputs$.next({
      ...this.defaultInputs$.getValue(),
      ...this.checkInputs(),
    });
    this.className = this.descriptionV2Style.getDynamicStyle();

    this.getDataset();
    this.getAllMetadataBusinessDataset();
    this.getAllMetadataCustomDataset();
  }

  ngOnChanges(changes: SimpleChanges): void {
    const inputs = Object.keys(changes).reduce((result: any, item: any) => {
      // eslint-disable-next-line no-param-reassign
      result[item] = changes[item].currentValue;
      return result;
    }, {});

    this.defaultInputs$.next({ ...this.defaultInputs$.getValue(), ...inputs });
    this.className = this.descriptionV2Style.getDynamicStyle();
  }

  ngOnDestroy() {
    this.unsubscribeDataset$.next();
    this.unsubscribeDataset$.complete();
  }

  checkInputs(): any {
    return {};
  }

  getDataset() {
    this.isLoadingRead$ = this.store.pipe(select(selectIsLoadingRead));

    this.data$ = this.store.pipe(select(selectDataset(this.id)));
  }

  getAllMetadataBusinessDataset(): void {
    const pWhere = {
      dataset_id: this.id,
      metadata_type_id: 1,
    };

    const params = {
      where: JSON.stringify(pWhere),
    };

    this.store.dispatch(
      fromMetadataBusinessDatasetActions.loadAllMetadataBusinessDataset({
        params,
        pagination: false,
        infinite: false,
      }),
    );

    this.store
      .pipe(
        select(selectAllMetadataBusinessDataset),
        filter((val) => val.length !== 0),
      )
      .subscribe((result) => {
        this.metadataBusinessData = result;

        const findVersionIndex = findIndex(this.metadataBusinessData, (res) => {
          return res.key === 'Versi' && !res.value;
        });

        if (findVersionIndex >= 0) {
          this.metadataBusinessData.splice(findVersionIndex, 1);
        }
      });
  }

  getAllMetadataCustomDataset(): void {
    const pWhere = {
      dataset_id: this.id,
      metadata_type_id: 4,
    };

    const params = {
      where: JSON.stringify(pWhere),
    };

    this.store.dispatch(
      fromMetadataCustomDatasetActions.loadAllMetadataCustomDataset({
        params,
        pagination: false,
        infinite: false,
      }),
    );

    this.metadataCustomData$ = this.store.pipe(
      select(selectAllMetadataCustomDataset),
    );
  }

  filterComponent(event: string): void {
    const value = isEmpty(event) === false ? event : null;
    this.filter.component = value;
  }

  getMetadata(metadata: any) {
    return this.datasetService.getMetadata(metadata);
  }
}
