import { Injectable } from '@angular/core';

// PACKAGE
import { css } from '@emotion/css';

@Injectable()
export class DescriptionV2Style {
  getDynamicStyle(): any {
    const navTabs = css`
      display: flex;
      flex-wrap: wrap;
      padding-left: 0;
      margin-bottom: 0;
      list-style: none;
    `;

    const navItem = css`
      list-style: none;
      height: 48px;
      padding: 0;
      margin-right: 24px;
      cursor: pointer;
      box-sizing: border-box;
      text-align: center;
      display: inline-flex;
      justify-content: center;
      align-items: center;
      white-space: nowrap;
      position: relative;
    `;

    const navItemActive = css`
      &:before {
        content: '';
        display: block;
        height: 3px;
        background: #0753a6;
        position: absolute;
        bottom: 0;
        left: 0;
        right: 0;
      }
    `;

    const navLink = css`
      display: inline-flex;
      justify-content: center;
      align-items: center;
      white-space: nowrap;
      font-size: 14px;
      color: #919294;

      &:focus,
      &:hover {
        color: #0753a6;
        text-decoration: none;
      }
    `;

    const navLinkActive = css`
      color: #0753a6;
    `;

    const description = css`
      font-size: 16px !important;
      color: #535353 !important;
      margin-bottom: 40px;
      white-space: pre-wrap;
    `;

    const frame = css`
      margin-right: 48px;
      width: 100px;
      height: 100px;
      white-space: nowrap;
      text-align: center;
      position: relative;
      vertical-align: top;
      display: inline-block;

      img {
        max-height: 100%;
        max-width: 100%;
        width: auto;
        height: auto;
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
      }
    `;

    return {
      navTabs,
      navItem,
      navItemActive,
      navLink,
      navLinkActive,
      description,
      frame,
    };
  }
}
