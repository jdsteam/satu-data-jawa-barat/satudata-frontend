import {
  Component,
  OnInit,
  OnChanges,
  OnDestroy,
  Input,
  SimpleChanges,
  ChangeDetectionStrategy,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Router, ActivatedRoute } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { select, Store } from '@ngrx/store';
import { ofType, Actions } from '@ngrx/effects';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { STATUS_DATASET } from '@constants-v4';
// MODEL
import { LoginData, DatasetData } from '@models-v3';
// SERVICE
import { AuthenticationService, DatasetService } from '@services-v3';
import { NotificationService } from '@services-v4';

// STORE
import {
  selectDataset,
  selectIsLoadingRead,
  selectIsLoadingUpdate,
} from '@store-v3/dataset/dataset.selectors';
import { fromDatasetActions } from '@store-v3/dataset/dataset.actions';

import { assign } from 'lodash';
import moment from 'moment';
import Swal from 'sweetalert2';
import { NgxLoadingModule } from 'ngx-loading';

@Component({
  selector: 'app-detail-dataset-draft-action',
  standalone: true,
  imports: [CommonModule, RouterModule, NgxLoadingModule],
  templateUrl: './draft-action.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DraftActionComponent implements OnInit, OnChanges, OnDestroy {
  satudataUser: LoginData;
  moment: any = moment;
  jds: any = jds;

  @Input() accessCoreDataInput: any;
  @Input() accessInfoCoreDataInput: any;

  // Variable
  id: number;
  title: string;
  accessCoreData: boolean;
  accessInfoCoreData: boolean;

  // Data
  data$: Observable<DatasetData>;
  isLoadingRead$: Observable<boolean>;
  isLoadingUpdate$: Observable<boolean>;
  isError$: Observable<boolean>;
  isError = false;
  errorMessage: string;

  bodyHistoryDecline: any;

  private unsubscribeDataset$ = new Subject<void>();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private notificationService: NotificationService,
    private datasetService: DatasetService,
    private store: Store<any>,
    private actions$: Actions,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    this.id = this.route.snapshot.params.id;
  }

  ngOnInit(): void {
    this.getDataset();
  }

  ngOnChanges(changes: SimpleChanges): void {
    // eslint-disable-next-line no-restricted-syntax
    for (const propName in changes) {
      // eslint-disable-next-line no-prototype-builtins
      if (changes.hasOwnProperty(propName)) {
        if (propName === 'accessCoreDataInput') {
          const chng = changes[propName];
          this.accessCoreData = chng.currentValue;
        }
        if (propName === 'accessInfoCoreDataInput') {
          const chng = changes[propName];
          this.accessInfoCoreData = chng.currentValue;
        }
      }
    }
  }

  ngOnDestroy() {
    this.unsubscribeDataset$.next();
    this.unsubscribeDataset$.complete();
  }

  getDataset(): void {
    this.isLoadingRead$ = this.store.pipe(select(selectIsLoadingRead));

    this.data$ = this.store.pipe(select(selectDataset(this.id)));
  }

  checkDraft() {
    let accessDraft: boolean;
    if (this.satudataUser.role_name === 'walidata') {
      accessDraft = true;
    } else if (
      this.satudataUser.role_name === 'opd' ||
      this.satudataUser.role_name === 'opd-view'
    ) {
      accessDraft = false;
    }
    return accessDraft;
  }

  onDecline(id: number, validate: number) {
    Swal.fire({
      text: 'Berikan catatan terkait dataset ini',
      allowOutsideClick: false,
      showCancelButton: true,
      cancelButtonText: 'Batal',
      confirmButtonColor: '#0753A6',
      input: 'textarea',
      inputAttributes: {
        'aria-label': 'Type your message here',
      },
      inputPlaceholder: 'Tuliskan catatan disini',
      confirmButtonText: 'Kirim',
      showLoaderOnConfirm: true,
      reverseButtons: true,
      preConfirm: async (inputText) => {
        if (inputText) {
          let verification: string;
          if (validate === 1) {
            verification = 'revision-new';
          } else if (validate === 4) {
            verification = 'revision-edit';
          }

          this.bodyHistoryDecline = {
            type: 'dataset',
            type_id: id,
            category: verification,
            notes: inputText,
          };
        } else {
          Swal.showValidationMessage('Catatan harus isi');
        }
      },
    }).then((result) => {
      if (result.value) {
        const bodyDataset = {
          id: Number(id),
        };

        // History
        const bodyHistory = this.bodyHistoryDecline;
        assign(bodyDataset, { history_draft: bodyHistory });

        assign(bodyDataset, STATUS_DATASET.get('revision-active'));

        this.store.dispatch(
          fromDatasetActions.updateDataset({
            update: bodyDataset,
            // history: bodyHistory,
            params: {},
          }),
        );

        this.isLoadingUpdate$ = this.store.pipe(select(selectIsLoadingUpdate));

        this.actions$
          .pipe(
            ofType(fromDatasetActions.updateDatasetSuccess),
            takeUntil(this.unsubscribeDataset$),
          )
          .subscribe(() => {
            const bodyNotification = { is_enable: false };
            const params = {
              type: 'dataset',
              type_id: id,
              receiver: this.satudataUser.id,
            };

            this.notificationService
              .updateItemBulk(bodyNotification, params)
              .subscribe(() => {
                Swal.fire({
                  type: 'success',
                  text: 'Catatan revisi dataset berhasil terkirim',
                  allowOutsideClick: false,
                }).then(() => {
                  this.router.navigate(['/desk'], {
                    queryParams: {
                      type: 'upload-change',
                      status: 'revision',
                    },
                  });
                });
              });
          });
      }
    });
  }

  onAccept(id: number) {
    Swal.fire({
      type: 'warning',
      text: 'Apakah Anda yakin dataset ini sudah layak dipublikasikan?',
      allowOutsideClick: false,
      showCancelButton: true,
      cancelButtonText: 'Batal',
      confirmButtonColor: '#0753A6',
      confirmButtonText: 'Setuju',
      reverseButtons: true,
    }).then((result) => {
      if (result.value) {
        const bodyDataset = {
          id: Number(id),
        };

        // History
        const bodyHistory = {
          type: 'dataset',
          type_id: Number(this.id),
          category: 'approve',
          notes: null,
        };
        assign(bodyDataset, { history_draft: bodyHistory });

        assign(bodyDataset, STATUS_DATASET.get('approve-active'));

        this.store.dispatch(
          fromDatasetActions.updateDataset({
            update: bodyDataset,
            params: {},
          }),
        );

        this.isLoadingUpdate$ = this.store.pipe(select(selectIsLoadingUpdate));

        this.actions$
          .pipe(
            ofType(fromDatasetActions.updateDatasetSuccess),
            takeUntil(this.unsubscribeDataset$),
          )
          .subscribe(() => {
            const bodyNotification = { is_enable: false };
            const params = {
              type: 'dataset',
              type_id: id,
              receiver: this.satudataUser.id,
            };

            this.notificationService
              .updateItemBulk(bodyNotification, params)
              .subscribe(() => {
                Swal.fire({
                  type: 'success',
                  text: 'Dataset berhasil dipublikasikan',
                  allowOutsideClick: false,
                }).then(() => {
                  this.router.navigate(['/desk'], {
                    queryParams: {
                      type: 'upload-change',
                      status: 'approve',
                    },
                  });
                });
              });
          });
      }
    });
  }
}
