import { Injectable } from '@angular/core';

// PACKAGE
import { css } from '@emotion/css';

@Injectable()
export class DescriptionStyle {
  getDynamicStyle(): any {
    const label = css`
      font-size: 14px;
      font-weight: normal;
      color: #7e8284;
    `;

    const description = css`
      font-size: 14px !important;
      color: #535353 !important;
    `;

    const formula = css`
      p {
        font-size: 14px;
        margin-bottom: 0;
      }
    `;

    const image = css`
      img {
        max-width: 100%;
        max-height: 100%;
      }
    `;

    const frame = css`
      margin-right: 48px;
      width: 100px;
      height: 100px;
      white-space: nowrap;
      text-align: center;
      position: relative;
      vertical-align: top;
      display: inline-block;

      img {
        max-height: 100%;
        max-width: 100%;
        width: auto;
        height: auto;
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
      }
    `;

    return {
      label,
      description,
      formula,
      image,
      frame,
    };
  }
}
