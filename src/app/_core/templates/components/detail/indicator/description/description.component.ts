import {
  Component,
  OnInit,
  OnChanges,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Observable } from 'rxjs';
import { RouterModule, Router, ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';
// MODEL
import { LoginData, IndicatorData } from '@models-v3';
// PIPE
import { SafeHtmlPipe } from '@pipes-v4';
// SERVICE
import { AuthenticationService } from '@services-v3';

import { jdsBiColor } from '@jds-bi/cdk';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconEnvelope,
  iconPhone,
} from '@jds-bi/icons';

// STORE
import {
  selectIndicator,
  selectIsLoadingRead,
} from '@store-v3/indicator/indicator.selectors';

import moment from 'moment';
import { Store, select } from '@ngrx/store';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

import { DescriptionStyle } from './description.style';

@Component({
  selector: 'app-detail-indicator-description',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    SafeHtmlPipe,
    JdsBiIconsModule,
    NgxSkeletonLoaderModule,
  ],
  templateUrl: './description.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DescriptionStyle],
})
export class DescriptionComponent implements OnInit, OnChanges {
  env = environment;
  satudataUser: LoginData;
  moment: any = moment;
  jds: any = jds;
  jdsBiColor = jdsBiColor;

  className!: any;

  // Variable
  id: number;
  filter = {
    component: 'description',
  };

  // Data
  data$: Observable<IndicatorData>;
  isLoadingRead$: Observable<boolean>;
  isLoadingUpdate$: Observable<boolean>;
  isError$: Observable<boolean>;
  isError = false;
  errorMessage: string;

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private iconService: JdsBiIconsService,
    private store: Store<any>,
    private descriptionStyle: DescriptionStyle,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    this.id = this.route.snapshot.params.id;

    this.iconService.registerIcons([iconEnvelope, iconPhone]);
  }

  ngOnInit(): void {
    this.className = this.descriptionStyle.getDynamicStyle();

    this.getIndicator();
  }

  ngOnChanges(): void {
    this.className = this.descriptionStyle.getDynamicStyle();
  }

  getIndicator() {
    this.isLoadingRead$ = this.store.pipe(select(selectIsLoadingRead));

    this.data$ = this.store.pipe(select(selectIndicator(this.id)));
  }
}
