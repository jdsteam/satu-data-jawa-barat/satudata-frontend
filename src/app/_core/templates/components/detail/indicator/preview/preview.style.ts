import { Injectable } from '@angular/core';

// PACKAGE
import { css } from '@emotion/css';

@Injectable()
export class PreviewStyle {
  getDynamicStyle(): any {
    const label = css`
      font-size: 14px;
      color: #7e8284;
      margin-bottom: 8px;
    `;

    const description = css`
      font-size: 16px;
      font-weight: bold;
      color: #535353;
    `;

    const action = css`
      display: flex;
      align-items: center;
      justify-content: center;
    `;

    const add = css`
      fill: #0753a6;
      cursor: pointer;
    `;

    const edit = css`
      fill: #3d4447;
      cursor: pointer;
    `;

    const final = css`
      fill: #67b662;
      cursor: pointer;
    `;

    const disabled = css`
      fill: #969696;
      cursor: not-allowed;
    `;

    return {
      label,
      description,
      action,
      add,
      edit,
      final,
      disabled,
    };
  }
}
