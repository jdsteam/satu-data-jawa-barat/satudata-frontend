import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  ViewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { filter } from 'rxjs/operators';

// STYLE
import * as jds from '@styles';

// MODEL
import { LoginData, IndicatorData, IndicatorProgressData } from '@models-v3';

// SERVICE
import { AuthenticationService } from '@services-v3';

// STORE
import {
  selectIndicator,
  selectIsLoadingRead,
} from '@store-v3/indicator/indicator.selectors';

import {
  selectAllIndicatorProgressInitial,
  selectIsLoadingList as selectIsLoadingListIndicatorProgressInitial,
} from '@store-v3/indicator-progress-initial/indicator-progress-initial.selectors';
import { fromIndicatorProgressInitialActions } from '@store-v3/indicator-progress-initial/indicator-progress-initial.actions';

import {
  selectAllIndicatorProgressFinal,
  selectIsLoadingList as selectIsLoadingListIndicatorProgressFinal,
  selectIsLoadingUpdate as selectIsLoadingUpdateIndicatorProgressFinal,
} from '@store-v3/indicator-progress-final/indicator-progress-final.selectors';
import { fromIndicatorProgressFinalActions } from '@store-v3/indicator-progress-final/indicator-progress-final.actions';

// COMPONENT
import { IndicatorProgressComponent as ModalIndicatorProgressComponent } from '@components-v3/modal/indicator-progress/indicator-progress.component';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconCircleInfo,
} from '@jds-bi/icons';

// PLUGIN
import { Store, select } from '@ngrx/store';

import { last, orderBy } from 'lodash';
import moment from 'moment';
import Swal from 'sweetalert2';
import { NgxLoadingModule } from 'ngx-loading';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgxPopperjsModule } from 'ngx-popperjs';

import { PreviewStyle } from './preview.style';

@Component({
  selector: 'app-detail-indicator-preview',
  standalone: true,
  imports: [
    CommonModule,
    JdsBiIconsModule,
    NgxLoadingModule,
    NgxSkeletonLoaderModule,
    ModalIndicatorProgressComponent,
    NgxPopperjsModule,
  ],
  templateUrl: './preview.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [PreviewStyle],
})
export class PreviewComponent implements OnInit {
  satudataUser: LoginData;
  moment: any = moment;
  jds: any = jds;

  @ViewChild(ModalIndicatorProgressComponent)
  modalIndicatorProgressComponent: ModalIndicatorProgressComponent;

  className!: any;

  // Variable
  id: number;
  lastAchievement: string;

  // Data
  data$: Observable<IndicatorData>;
  isLoadingRead$: Observable<boolean>;
  isLoadingUpdate$: Observable<boolean>;
  isError$: Observable<boolean>;
  isError = false;
  errorMessage: string;

  indicatorProgressInitialData$: Observable<IndicatorProgressData[]>;
  indicatorProgressInitialIsLoadingList$: Observable<boolean>;

  indicatorProgressFinalData: IndicatorProgressData[];
  indicatorProgressFinalData$: Observable<IndicatorProgressData[]>;
  indicatorProgressFinalIsLoadingList$: Observable<boolean>;
  indicatorProgressFinalIsLoadingUpdate$: Observable<boolean>;

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private store: Store<any>,
    private sanitizer: DomSanitizer,
    private iconService: JdsBiIconsService,
    private previewStyle: PreviewStyle,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    this.id = this.route.snapshot.params.id;

    this.iconService.registerIcons([iconCircleInfo]);
  }

  ngOnInit(): void {
    this.className = this.previewStyle.getDynamicStyle();

    this.getIndicator();
    this.getProgressInitial();
    this.getProgressFinal();
  }

  getIndicator() {
    this.isLoadingRead$ = this.store.pipe(select(selectIsLoadingRead));

    this.data$ = this.store.pipe(select(selectIndicator(this.id)));
  }

  getProgressInitial() {
    const pSort = 'index:asc';

    const pWhere = {
      indikator_id: this.id,
      index: 'awal',
    };

    const params = {
      sort: pSort,
      where: JSON.stringify(pWhere),
    };

    this.store.dispatch(
      fromIndicatorProgressInitialActions.loadAllIndicatorProgressInitial({
        params,
        pagination: false,
        infinite: false,
      }),
    );

    this.indicatorProgressInitialIsLoadingList$ = this.store.pipe(
      select(selectIsLoadingListIndicatorProgressInitial),
    );

    this.indicatorProgressInitialData$ = this.store.pipe(
      select(selectAllIndicatorProgressInitial),
    );
  }

  getProgressFinal() {
    const pSort = 'index:asc';

    const pWhere = {
      indikator_id: this.id,
      index: 'akhir',
    };

    const params = {
      sort: pSort,
      where: JSON.stringify(pWhere),
    };

    this.store.dispatch(
      fromIndicatorProgressFinalActions.loadAllIndicatorProgressFinal({
        params,
        pagination: false,
        infinite: false,
      }),
    );

    this.indicatorProgressFinalIsLoadingList$ = this.store.pipe(
      select(selectIsLoadingListIndicatorProgressFinal),
    );

    this.store
      .pipe(
        select(selectAllIndicatorProgressFinal),
        filter((val) => val.length !== 0),
      )
      .subscribe((result) => {
        this.indicatorProgressFinalData = result;
        this.getLastAchievement(result);
      });
  }

  getLastAchievement(data: IndicatorProgressData[]) {
    if (data.length) {
      const getAchievement = data.filter(
        (res) =>
          res.capaian !== null &&
          res.capaian !== '' &&
          res.capaian !== '-' &&
          res.is_final,
      );

      if (getAchievement.length > 0) {
        const sorAchievement = orderBy(
          getAchievement,
          (datas) => datas.year,
          'asc',
        );
        this.lastAchievement = last(sorAchievement).capaian;
      } else {
        this.lastAchievement = '-';
      }
    }
    this.cdRef.detectChanges();
  }

  addAchievement(dataIndicatorProgress: IndicatorProgressData) {
    if (
      dataIndicatorProgress.capaian ||
      dataIndicatorProgress.is_final === true
    ) {
      return;
    }

    this.modalIndicatorProgressComponent.showModal(
      'add',
      dataIndicatorProgress,
    );
  }

  editAchievement(dataIndicatorProgress: IndicatorProgressData) {
    if (
      !dataIndicatorProgress.capaian ||
      dataIndicatorProgress.is_final === true
    ) {
      return;
    }

    this.modalIndicatorProgressComponent.showModal(
      'edit',
      dataIndicatorProgress,
    );
  }

  finalization(dataIndicatorProgress: IndicatorProgressData) {
    if (
      !dataIndicatorProgress.capaian ||
      dataIndicatorProgress.is_final === true
    ) {
      return;
    }

    Swal.fire({
      type: 'warning',
      text: 'Apakah Anda yakin untuk melakukan finalisasi pencapaian ini',
      allowOutsideClick: false,
      showCancelButton: true,
      cancelButtonText: 'Batal',
      confirmButtonColor: '#0753a6',
      confirmButtonText: 'Finalisasi',
    }).then((result) => {
      if (result.value) {
        const bodyProgress = {
          id: dataIndicatorProgress.id,
          indikator_id: dataIndicatorProgress.indikator_id,
          is_final: true,
        };

        const pWhere = {
          id: dataIndicatorProgress.id,
          indikator_id: dataIndicatorProgress.indikator_id,
        };

        const params = {
          where: JSON.stringify(pWhere),
        };

        this.store.dispatch(
          fromIndicatorProgressFinalActions.updateIndicatorProgressFinal({
            indicatorId: this.id,
            update: bodyProgress,
            params,
          }),
        );

        this.indicatorProgressFinalIsLoadingUpdate$ = this.store.pipe(
          select(selectIsLoadingUpdateIndicatorProgressFinal),
        );
      }
    });
  }
}
