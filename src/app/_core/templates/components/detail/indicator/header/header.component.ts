import {
  Component,
  OnInit,
  OnChanges,
  OnDestroy,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  ViewChild,
  inject,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Router, ActivatedRoute } from '@angular/router';
import { Observable, Subject, of } from 'rxjs';
import { filter, take, takeUntil } from 'rxjs/operators';
import { HttpEventType } from '@angular/common/http';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { CLASSIFICATION_INDICATOR, STATUS_INDICATOR } from '@constants-v4';
// MODEL
import { LoginData, IndicatorData } from '@models-v3';

// SERVICE
import { AuthenticationService, StateService, HttpService } from '@services-v3';
import { NotificationService, IndicatorService } from '@services-v4';

// COMPONENT
import { ModalIndicatorStatusComponent } from '@components-v4';
import { ModalModule, ModalService } from '@components-v3/modal';
import {
  JdsBiBadgeModule,
  JdsBiDropdownModule,
  JdsBiHostedDropdownModule,
} from '@jds-bi/core';
import { jdsBiColor } from '@jds-bi/cdk';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconCalendar,
  iconCabinetFiling,
  iconChevronDown,
  iconEllipsis,
} from '@jds-bi/icons';

// STORE
import {
  selectIndicator,
  selectIsLoadingRead,
} from '@store-v3/indicator/indicator.selectors';
import { fromIndicatorActions } from '@store-v3/indicator/indicator.actions';

// PLUGIN

import moment from 'moment';
import Swal from 'sweetalert2';
import { assign } from 'lodash';
import { Store, select } from '@ngrx/store';
import { saveAs } from 'file-saver';
import { fromWorker } from 'observable-webworker';
import { NgxLoadingModule } from 'ngx-loading';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { SubscribeDirective } from '@ngneat/subscribe';
import { useMutationResult } from '@ngneat/query';

import { HeaderStyle } from './header.style';

@Component({
  selector: 'app-detail-indicator-header',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    ModalModule,
    JdsBiBadgeModule,
    JdsBiIconsModule,
    NgxLoadingModule,
    SubscribeDirective,
    NgxSkeletonLoaderModule,
    JdsBiDropdownModule,
    JdsBiHostedDropdownModule,
    ModalIndicatorStatusComponent,
  ],
  templateUrl: './header.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [HeaderStyle],
})
export class HeaderComponent implements OnInit, OnChanges, OnDestroy {
  satudataUser: LoginData;
  satudataToken: string;
  moment: any = moment;
  jds: any = jds;
  jdsBiColor = jdsBiColor;

  @ViewChild(ModalIndicatorStatusComponent)
  modalIndicatorStatusComponent: ModalIndicatorStatusComponent;

  // Service
  private notificationService = inject(NotificationService);
  private indicatorService = inject(IndicatorService);
  editIndicatorMutation = useMutationResult();

  className!: any;

  // Log
  indicatorLog = null;

  indicatorInfo = null;
  userInfo = null;

  // Variable
  public isDropdownDownload = false;
  public isDropdownAction = false;
  id: number;
  title: string;
  status: string = null;

  // Data
  data: IndicatorData;
  data$: Observable<IndicatorData>;
  isLoadingRead$: Observable<boolean>;
  isLoadingUpdate$: Observable<boolean>;
  isError$: Observable<boolean>;
  isError = false;
  errorMessage: string;

  downloadIsLoading = false;
  downloadName: string;
  downloadProgress = 0;

  private unsubscribeIndicator$ = new Subject<void>();

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private stateService: StateService,
    private httpService: HttpService,
    private modalService: ModalService,
    private iconService: JdsBiIconsService,
    private store: Store<any>,
    private headerStyle: HeaderStyle,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
    this.authenticationService.satudataToken.subscribe(
      (x) => (this.satudataToken = x),
    );
    this.stateService.currentLogUser.subscribe((x) => (this.userInfo = x));

    this.id = this.route.snapshot.params.id;

    this.iconService.registerIcons([
      iconCalendar,
      iconCabinetFiling,
      iconChevronDown,
      iconEllipsis,
    ]);
  }

  ngOnInit(): void {
    this.className = this.headerStyle.getDynamicStyle();

    this.getIndicator();
  }

  ngOnChanges(): void {
    this.className = this.headerStyle.getDynamicStyle();
  }

  ngOnDestroy() {
    this.unsubscribeIndicator$.next();
    this.unsubscribeIndicator$.complete();
  }

  getClassificationColor(id: number) {
    return CLASSIFICATION_INDICATOR.get(id);
  }

  getIndicator() {
    this.isLoadingRead$ = this.store.pipe(select(selectIsLoadingRead));

    this.data$ = this.store.pipe(select(selectIndicator(this.id)));

    this.store
      .pipe(
        select(selectIndicator(this.id)),
        filter((val) => val !== undefined),
        take(1),
      )
      .subscribe((result) => {
        this.data = result;
      });
  }

  getType(value: string) {
    let type: string;
    let blob: string;
    let extension: string;

    if (value === 'csv') {
      type = 'csv';
      blob = 'text/csv';
      extension = 'csv';
    } else if (value === 'excel') {
      type = 'xls';
      blob = 'application/vnd.ms-excel';
      extension = 'xlsx';
    }

    return {
      type,
      blob,
      extension,
    };
  }

  onDownload(type: string) {
    const fileType = this.getType(type);

    this.store
      .pipe(
        takeUntil(this.unsubscribeIndicator$),
        select(selectIndicator(this.id)),
        filter((val) => val !== undefined),
        take(1),
      )
      .subscribe((result) => {
        this.downloadProgress = 0;

        this.openModal('modal-download');

        const { id } = result;
        const urlService = `${environment.backendURL}indikator/${id}`;

        const params = {
          download: fileType.type,
          timestamp: new Date().valueOf(),
        };

        this.httpService.getFile(urlService, params).subscribe((response) => {
          if (response.type === HttpEventType.DownloadProgress) {
            this.downloadIsLoading = true;
            this.downloadName = result.name;
            this.downloadProgress = Math.round(
              (100 * response.loaded) / response.total,
            );
            this.cdRef.markForCheck();
          } else if (response.type === HttpEventType.Response) {
            const newBlob = new Blob([response.body], {
              type: fileType.blob,
            });

            setTimeout(() => {
              this.onDownloadCounter();
              this.closeModal('modal-download');
              this.downloadIsLoading = false;

              saveAs(newBlob, `${result.title}.${fileType.extension}`);

              this.logIndicator(type);

              Swal.fire({
                type: 'success',
                title: 'Permintaan unduh indikator berhasil',
                allowOutsideClick: false,
                cancelButtonText: 'Tutup',
                confirmButtonColor: '#217EBA',
              }).then(() => {
                this.getIndicator();
              });
            }, 1000);
          }
        });
      });
  }

  onDownloadCounter() {
    this.store.dispatch(
      fromIndicatorActions.counterIndicator({
        id: this.id,
        params: {
          category: 'access',
        },
      }),
    );
  }

  onDelete(id: number) {
    Swal.fire({
      type: 'warning',
      text: 'Apakah Anda yakin untuk menghapus Indikator ini?',
      allowOutsideClick: false,
      showCancelButton: true,
      cancelButtonText: 'Batal',
      confirmButtonColor: '#d33',
      confirmButtonText: 'Hapus',
    }).then((result) => {
      if (result.value) {
        const data = {
          type: 'indicator',
          type_id: Number(id),
          category: 'draft-delete',
          notes: null,
        };
        this.onUpdate(data);
      }
    });
  }

  onArchive(id: number) {
    this.status = 'active';
    this.modalIndicatorStatusComponent.openModal(id);
  }

  onActive(id: number) {
    this.status = 'archive';
    this.modalIndicatorStatusComponent.openModal(id);
  }

  onUpdate(formData: any) {
    let category;
    switch (this.status) {
      case 'active':
        category = 'approve-archive';
        break;
      case 'archive':
        category = 'approve-active';
        break;
      case 'revision-new':
        category = 'revision-active-new';
        break;
      case 'revision-edit':
        category = 'revision-active-edit';
        break;
      default:
        category = formData.category;
        break;
    }

    // TODO: activate if backend history_draft ready
    // const bodyIndicator = {
    //   id: formData.type_id,
    //   history_draft: { ...formData, category },
    // };

    // TODO: change if backend history_draft ready
    const bodyIndicator = { id: formData.type_id };

    assign(bodyIndicator, STATUS_INDICATOR.get(category));

    this.indicatorService
      .updateItem(bodyIndicator.id, bodyIndicator)
      .pipe(this.editIndicatorMutation.track())
      .subscribe(() => {
        let text;
        let stat;
        switch (category) {
          case 'draft-delete':
            text = 'dihapus';
            stat = 'draft';
            break;
          case 'approve-active':
            text = 'diaktifkan';
            stat = 'active';
            break;
          case 'approve-archive':
            text = 'diarsipkan';
            stat = 'archive';
            break;
          default:
            break;
        }

        Swal.fire({
          type: 'success',
          text: `Indikator berhasil ${text}`,
          allowOutsideClick: false,
        }).then(() => {
          this.router.navigate(['/my-data'], {
            queryParams: {
              catalog: 'indicator',
              status: stat,
            },
          });
        });
      });
  }

  openModal(id: string) {
    this.modalService.open(id);
  }

  closeModal(id: string) {
    this.modalService.close(id);
  }

  logIndicator(type: string): void {
    if (typeof Worker !== 'undefined') {
      this.indicatorInfo = {
        type,
        date_start: moment().format('YYYY-MM-DD HH:mm:ss'),
        date_end: moment().format('YYYY-MM-DD HH:mm:ss'),
        indicator_id: this.data.id,
        indicator_title: this.data.title,
        indicator_name: this.data.name,
        indicator_organization_code: this.data.kode_skpd,
        indicator_organization_name: this.data.nama_skpd,
      };

      this.indicatorLog = {
        log: { ...this.indicatorInfo, ...this.userInfo },
        token: this.satudataToken,
      };

      fromWorker<object, string>(
        () =>
          new Worker(
            new URL(
              'src/app/__v4/workers/log-download-indicator.worker',
              import.meta.url,
            ),
            {
              type: 'module',
              name: 'download-indicator',
            },
          ),
        of(this.indicatorLog),
      ).subscribe((message) => {
        if (!environment.production) {
          console.info(`Log download indicator: ${message}`);
        }
      });
    }
  }
}
