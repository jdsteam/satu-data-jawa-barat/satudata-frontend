import { Injectable } from '@angular/core';

// PACKAGE
import { css } from '@emotion/css';

@Injectable()
export class HeaderStyle {
  getDynamicStyle(): any {
    const header = css`
      display: flex;
      align-items: start;
      margin-bottom: 10px;
    `;

    const title = css`
      margin-bottom: 0;
      font-family: Roboto;
      font-weight: 700;
    `;

    const label = css`
      font-size: 14px;
      line-height: 20px;
      color: #7e8284;
      margin-bottom: 8px;
    `;

    const action = css`
      margin-left: auto;
      display: flex;
      align-items: center;

      .dropdown {
        margin-left: 10px;

        .dropdown-menu {
          padding: 10px 0;
          li {
            list-style: none;

            a {
              display: block;
              padding: 5px 15px;
              cursor: pointer;
              font-size: 13px;

              &:hover {
                color: #ffffff;
                background-color: #0753a6;
                background-image: unset;
              }
            }
          }
        }

        &.dropdown-download:not(.disabled),
        &.dropdown-action:not(.disabled) {
          &:hover > .dropdown-menu {
            display: block;
            margin: auto;
          }
        }
      }
    `;

    return {
      header,
      title,
      label,
      action,
    };
  }
}
