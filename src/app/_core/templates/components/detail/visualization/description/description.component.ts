import {
  Component,
  OnInit,
  OnChanges,
  OnDestroy,
  Input,
  SimpleChanges,
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { select, Store } from '@ngrx/store';
import { Actions } from '@ngrx/effects';

// SERVICE
import { AuthenticationService } from '@services-v3';

// MODEL
import { LoginData, VisualizationData } from '@models-v3';

// STORE
import {
  selectVisualization,
  selectIsLoadingRead,
} from '@store-v3/visualization/visualization.selectors';

import moment from 'moment';

@Component({
  selector: 'app-detail-visualization-description',
  templateUrl: './description.component.html',
  styleUrls: ['./description.component.scss'],
})
export class DescriptionComponent implements OnInit, OnChanges, OnDestroy {
  satudataUser: LoginData;
  moment: any = moment;

  @Input() accessTableauInput: any;
  @Input() accessInfoTableauInput: any;

  // Variable
  id: number;
  title: string;
  tableauViz: any;
  accessTableau: boolean;
  accessInfoTableau: boolean;

  // Data
  data$: Observable<VisualizationData>;
  isLoadingRead$: Observable<boolean>;
  isLoadingUpdate$: Observable<boolean>;
  isError$: Observable<boolean>;
  isError = false;
  errorMessage: string;

  private unsubscribeVisualization$ = new Subject<void>();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private store: Store<any>,
    private actions$: Actions,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    this.id = this.route.snapshot.params.id;
  }

  ngOnInit(): void {
    this.getVisualization();
  }

  ngOnChanges(changes: SimpleChanges): void {
    // eslint-disable-next-line no-restricted-syntax
    for (const propName in changes) {
      // eslint-disable-next-line no-prototype-builtins
      if (changes.hasOwnProperty(propName)) {
        if (propName === 'accessTableauInput') {
          const chng = changes[propName];
          this.accessTableau = chng.currentValue;
        }
        if (propName === 'accessInfoTableauInput') {
          const chng = changes[propName];
          this.accessInfoTableau = chng.currentValue;
        }
      }
    }
  }

  ngOnDestroy() {
    this.unsubscribeVisualization$.next();
    this.unsubscribeVisualization$.complete();
  }

  getVisualization(): void {
    this.isLoadingRead$ = this.store.pipe(select(selectIsLoadingRead));

    this.data$ = this.store.pipe(select(selectVisualization(this.id)));
  }
}
