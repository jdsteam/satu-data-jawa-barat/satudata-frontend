import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Router, ActivatedRoute } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { filter } from 'rxjs/operators';
import { select, Store } from '@ngrx/store';

// STYLE
import * as jds from '@styles';

// SERVICE
import { AuthenticationService } from '@services-v3';

// MODEL
import {
  LoginData,
  VisualizationData,
  MetadataVisualizationData,
} from '@models-v3';

// STORE
import {
  selectVisualization,
  selectIsLoadingRead,
} from '@store-v3/visualization/visualization.selectors';

// COMPONENT
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconCircleInfo,
} from '@jds-bi/icons';
import { jdsBiColor } from '@jds-bi/cdk';

import {
  selectAllMetadataBusinessVisualization,
  selectIsLoadingList as selectIsLoadingListMetadataBusinessVisualization,
} from '@store-v3/metadata-business-visualization/metadata-business-visualization.selectors';
import { fromMetadataBusinessVisualizationActions } from '@store-v3/metadata-business-visualization/metadata-business-visualization.actions';

import moment from 'moment';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgxPopperjsModule } from 'ngx-popperjs';

@Component({
  selector: 'app-detail-visualization-description-v2',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    JdsBiIconsModule,
    NgxSkeletonLoaderModule,
    NgxPopperjsModule,
  ],
  templateUrl: './description-v2.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DescriptionV2Component implements OnInit, OnDestroy {
  satudataUser: LoginData;
  moment: any = moment;
  jds: any = jds;
  jdsBiColor = jdsBiColor;

  // Variable
  id: number;

  // Data
  data$: Observable<VisualizationData>;
  isLoadingRead$: Observable<boolean>;
  isLoadingUpdate$: Observable<boolean>;
  isError$: Observable<boolean>;
  isError = false;
  errorMessage: string;

  metadataBusinessData: MetadataVisualizationData;
  metadataBusinessData$: Observable<MetadataVisualizationData>;
  metadataBusinessIsLoadingList$: Observable<boolean>;
  metadataBusinessIsError$: Observable<boolean>;
  metadataBusinessIsError = false;
  metadataBusinessErrorMessage: string;

  private unsubscribeVisualization$ = new Subject<void>();

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private iconService: JdsBiIconsService,
    private store: Store<any>,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    this.id = this.route.snapshot.params.id;

    this.iconService.registerIcons([iconCircleInfo]);
  }

  ngOnInit(): void {
    this.getVisualization();
    this.getMetadataBusiness();
  }

  ngOnDestroy() {
    this.unsubscribeVisualization$.next();
    this.unsubscribeVisualization$.complete();
  }

  getVisualization(): void {
    this.isLoadingRead$ = this.store.pipe(select(selectIsLoadingRead));

    this.data$ = this.store.pipe(select(selectVisualization(this.id)));
  }

  getMetadataBusiness(): void {
    const pWhere = {
      visualization_id: this.id,
      metadata_type_id: 1,
      key: 'Divisualisasikan Oleh',
    };

    const params = {
      where: JSON.stringify(pWhere),
    };

    this.store.dispatch(
      fromMetadataBusinessVisualizationActions.loadAllMetadataBusinessVisualization(
        {
          params,
          pagination: false,
          infinite: false,
        },
      ),
    );

    this.metadataBusinessIsLoadingList$ = this.store.pipe(
      select(selectIsLoadingListMetadataBusinessVisualization),
    );

    this.store
      .pipe(
        select(selectAllMetadataBusinessVisualization),
        filter((val) => val.length !== 0),
      )
      .subscribe((result) => {
        const index = 0;
        this.metadataBusinessData = result[index];
      });
  }
}
