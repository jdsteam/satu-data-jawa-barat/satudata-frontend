import {
  Component,
  OnInit,
  OnChanges,
  OnDestroy,
  Input,
  SimpleChanges,
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { filter } from 'rxjs/operators';
import { select, Store } from '@ngrx/store';
import { Actions } from '@ngrx/effects';
import { environment } from 'src/environments/environment';

// SERVICE
import { AuthenticationService } from '@services-v3';

// MODEL
import {
  LoginData,
  VisualizationData,
  MetadataVisualizationData,
} from '@models-v3';

// STORE
import {
  selectVisualization,
  selectIsLoadingRead,
} from '@store-v3/visualization/visualization.selectors';

import {
  selectAllMetadataBusinessVisualization,
  selectIsLoadingList as selectIsLoadingListMetadataBusinessVisualization,
} from '@store-v3/metadata-business-visualization/metadata-business-visualization.selectors';
import { fromMetadataBusinessVisualizationActions } from '@store-v3/metadata-business-visualization/metadata-business-visualization.actions';

import moment from 'moment';

@Component({
  selector: 'app-detail-visualization-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
})
export class AboutComponent implements OnInit, OnChanges, OnDestroy {
  env = environment;
  satudataUser: LoginData;
  moment: any = moment;

  @Input() accessTableauInput: any;
  @Input() accessInfoTableauInput: any;

  // Variable
  id: number;
  accessTableau: boolean;
  accessInfoTableau: boolean;

  // Data
  data$: Observable<VisualizationData>;
  isLoadingRead$: Observable<boolean>;
  isLoadingUpdate$: Observable<boolean>;
  isError$: Observable<boolean>;
  isError = false;
  errorMessage: string;

  metadataBusinessData: MetadataVisualizationData;
  metadataBusinessData$: Observable<MetadataVisualizationData>;
  metadataBusinessIsLoadingList$: Observable<boolean>;
  metadataBusinessIsError$: Observable<boolean>;
  metadataBusinessIsError = false;
  metadataBusinessErrorMessage: string;

  private unsubscribeVisualization$ = new Subject<void>();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private store: Store<any>,
    private actions$: Actions,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    this.id = this.route.snapshot.params.id;
  }

  ngOnInit(): void {
    this.getVisualization();
    this.getMetadataBusiness();
  }

  ngOnChanges(changes: SimpleChanges): void {
    // eslint-disable-next-line no-restricted-syntax
    for (const propName in changes) {
      // eslint-disable-next-line no-prototype-builtins
      if (changes.hasOwnProperty(propName)) {
        if (propName === 'accessTableauInput') {
          const chng = changes[propName];
          this.accessTableau = chng.currentValue;
        }
        if (propName === 'accessInfoTableauInput') {
          const chng = changes[propName];
          this.accessInfoTableau = chng.currentValue;
        }
      }
    }
  }

  ngOnDestroy() {
    this.unsubscribeVisualization$.next();
    this.unsubscribeVisualization$.complete();
  }

  getVisualization(): void {
    this.isLoadingRead$ = this.store.pipe(select(selectIsLoadingRead));

    this.data$ = this.store.pipe(select(selectVisualization(this.id)));
  }

  getMetadataBusiness(): void {
    const pWhere = {
      visualization_id: this.id,
      metadata_type_id: 1,
      key: 'Divisualisasikan Oleh',
    };

    const params = {
      where: JSON.stringify(pWhere),
    };

    this.store.dispatch(
      fromMetadataBusinessVisualizationActions.loadAllMetadataBusinessVisualization(
        {
          params,
          pagination: false,
          infinite: false,
        },
      ),
    );

    this.metadataBusinessIsLoadingList$ = this.store.pipe(
      select(selectIsLoadingListMetadataBusinessVisualization),
    );

    this.store
      .pipe(
        select(selectAllMetadataBusinessVisualization),
        filter((val) => val.length !== 0),
      )
      .subscribe((result) => {
        const index = 0;
        this.metadataBusinessData = result[index];
      });
  }
}
