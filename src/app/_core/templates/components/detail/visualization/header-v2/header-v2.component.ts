import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Router, ActivatedRoute } from '@angular/router';
import { Observable, Subject, of } from 'rxjs';
import { filter, take, takeUntil } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { WHATSAPP } from '@constants-v4';
// MODEL
import { LoginData, VisualizationData } from '@models-v3';
// SERVICE
import {
  AuthenticationService,
  StateService,
  DatasetService,
} from '@services-v3';
// JDS-BI
import { jdsBiColor } from '@jds-bi/cdk';
import {
  JdsBiBadgeModule,
  JdsBiDropdownModule,
  JdsBiHostedDropdownModule,
} from '@jds-bi/core';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconBan,
  iconBook,
  iconCalendar,
  iconChevronDown,
  iconEllipsis,
  iconUserLock,
} from '@jds-bi/icons';

// STORE
import {
  selectVisualization,
  selectIsLoadingRead,
  selectIsLoadingUpdate,
} from '@store-v3/visualization/visualization.selectors';
import { fromVisualizationActions } from '@store-v3/visualization/visualization.actions';

import moment from 'moment';
import Swal from 'sweetalert2';
import { select, Store } from '@ngrx/store';
import { ofType, Actions } from '@ngrx/effects';
import { fromWorker } from 'observable-webworker';
import { NgxLoadingModule } from 'ngx-loading';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

import { HeaderV2Style } from './header-v2.style';

declare const tableau: any;

@Component({
  selector: 'app-detail-visualization-header-v2',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    JdsBiBadgeModule,
    JdsBiIconsModule,
    NgxLoadingModule,
    NgxSkeletonLoaderModule,
    JdsBiDropdownModule,
    JdsBiHostedDropdownModule,
  ],
  templateUrl: './header-v2.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [HeaderV2Style],
})
export class HeaderV2Component implements OnInit, OnDestroy {
  satudataUser: LoginData;
  satudataToken: string;
  whatsapp = WHATSAPP;
  moment: any = moment;
  jds: any = jds;
  jdsBiColor = jdsBiColor;

  className!: any;

  // Log
  visualizationLog = null;

  visualizationInfo = null;
  userInfo = null;

  // Variable
  public isDropdownDownload = false;
  public isDropdownAction = false;
  id: number;
  tableauViz: any;
  vizInit = false;
  accessTableau: boolean;
  accessInfoTableau: string;
  errorTableau: string;

  // Data
  data$: Observable<VisualizationData>;
  data: VisualizationData;
  isLoadingRead$: Observable<boolean>;
  isLoadingUpdate$: Observable<boolean>;
  isError$: Observable<boolean>;
  isError = false;
  errorMessage: string;

  private unsubscribeVisualization$ = new Subject<void>();

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private stateService: StateService,
    private datasetService: DatasetService,
    private store: Store<any>,
    private actions$: Actions,
    private iconService: JdsBiIconsService,
    private headerStyle: HeaderV2Style,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
    this.authenticationService.satudataToken.subscribe(
      (x) => (this.satudataToken = x),
    );
    this.stateService.currentLogUser.subscribe((x) => (this.userInfo = x));

    this.id = this.route.snapshot.params.id;

    this.iconService.registerIcons([
      iconBan,
      iconBook,
      iconCalendar,
      iconChevronDown,
      iconEllipsis,
      iconUserLock,
    ]);
  }

  ngOnInit(): void {
    this.className = this.headerStyle.getDynamicStyle();

    this.getVisualization();
  }

  ngOnDestroy() {
    this.unsubscribeVisualization$.next();
    this.unsubscribeVisualization$.complete();
  }

  getVisualization(): void {
    this.isLoadingRead$ = this.store.pipe(select(selectIsLoadingRead));

    this.data$ = this.store.pipe(select(selectVisualization(this.id)));

    this.store
      .pipe(
        takeUntil(this.unsubscribeVisualization$),
        select(selectVisualization(this.id)),
        filter((val) => val !== undefined),
        take(1),
      )
      .subscribe((result) => {
        this.data = result;
        const canAccess = result.can_access_new;

        if (canAccess) {
          this.accessTableau = true;
          this.getTableau(result.url);
        } else {
          this.accessTableau = false;
          this.accessInfoTableau = 'can_access';
        }
      });
  }

  getTableau(url: string): void {
    if (this.vizInit) {
      this.tableauViz.dispose();
      this.vizInit = false;
    }

    const placeholderDiv = document.getElementById('vizContainer');
    const options = {
      hideToolbar: true,
      hideTabs: true,
      onFirstInteractive: () => {
        this.vizInit = true;
      },
    };

    try {
      this.tableauViz = new tableau.Viz(placeholderDiv, url, options);
    } catch (err) {
      this.accessTableau = false;
      this.accessInfoTableau = 'error_response';
      // eslint-disable-next-line no-underscore-dangle
      this.errorTableau = err._message;
    }
  }

  getClassificationColor(id: number) {
    return this.datasetService.getClassificationColor(id);
  }

  onArchive(id: number) {
    Swal.fire({
      type: 'warning',
      text: 'Apakah Anda yakin untuk mengarsipkan Visualisasi ini?',
      allowOutsideClick: false,
      showCancelButton: true,
      cancelButtonText: 'Batal',
      confirmButtonColor: '#d33',
      confirmButtonText: 'Arsipkan',
    }).then((result) => {
      if (result.value) {
        const bodyVisualization = {
          id: Number(id),
          is_active: false,
        };

        this.store.dispatch(
          fromVisualizationActions.updateVisualization({
            update: bodyVisualization,
          }),
        );

        this.isLoadingUpdate$ = this.store.pipe(select(selectIsLoadingUpdate));

        this.actions$
          .pipe(
            ofType(fromVisualizationActions.updateVisualizationSuccess),
            takeUntil(this.unsubscribeVisualization$),
          )
          .subscribe(() => {
            Swal.fire({
              type: 'success',
              text: 'Visualisasi berhasil diarsipkan',
              allowOutsideClick: false,
            }).then(() => {
              this.router.navigate(['/my-data'], {
                queryParams: {
                  status: 'archive',
                  catalog: 'visualization',
                },
              });
            });
          });
      }
    });
  }

  onActive(id: number) {
    Swal.fire({
      type: 'warning',
      text: 'Apakah Anda yakin untuk mengaktifkan Visualisasi ini?',
      allowOutsideClick: false,
      showCancelButton: true,
      cancelButtonText: 'Batal',
      confirmButtonColor: '#28a745',
      confirmButtonText: 'Aktifkan',
    }).then((result) => {
      if (result.value) {
        const bodyVisualization = {
          id: Number(id),
          is_active: true,
        };

        this.store.dispatch(
          fromVisualizationActions.updateVisualization({
            update: bodyVisualization,
          }),
        );

        this.isLoadingUpdate$ = this.store.pipe(select(selectIsLoadingUpdate));

        this.actions$
          .pipe(
            ofType(fromVisualizationActions.updateVisualizationSuccess),
            takeUntil(this.unsubscribeVisualization$),
          )
          .subscribe(() => {
            Swal.fire({
              type: 'success',
              text: 'Visualisasi berhasil diaktifkan',
              allowOutsideClick: false,
            }).then(() => {
              this.router.navigate(['/my-data'], {
                queryParams: {
                  status: 'active',
                  catalog: 'visualization',
                },
              });
            });
          });
      }
    });
  }

  exportImage(): void {
    this.tableauViz.showExportImageDialog();
    this.onDownloadCounter('image');
    this.logVisualization('image');
  }

  exportPDF(): void {
    this.tableauViz.showExportPDFDialog();
    this.onDownloadCounter('pdf');
    this.logVisualization('pdf');
  }

  onDownloadCounter(type: string) {
    this.store.dispatch(
      fromVisualizationActions.counterVisualization({
        id: this.id,
        params: {
          category: type === 'image' ? 'image_private' : 'pdf_private',
        },
      }),
    );
  }

  logVisualization(type: string): void {
    if (typeof Worker !== 'undefined') {
      this.visualizationInfo = {
        type,
        date_start: moment().format('YYYY-MM-DD HH:mm:ss'),
        date_end: moment().format('YYYY-MM-DD HH:mm:ss'),
        visualization_id: this.data.id,
        visualization_title: this.data.title,
        visualization_name: this.data.name,
        visualization_organization_code: this.data.skpd.kode_skpd,
        visualization_organization_name: this.data.skpd.nama_skpd,
      };

      this.visualizationLog = {
        log: { ...this.visualizationInfo, ...this.userInfo },
        token: this.satudataToken,
      };

      fromWorker<object, string>(
        () =>
          new Worker(
            new URL(
              'src/app/__v4/workers/log-download-visualization.worker',
              import.meta.url,
            ),
            {
              type: 'module',
              name: 'download-visualization',
            },
          ),
        of(this.visualizationLog),
      ).subscribe((message) => {
        if (!environment.production) {
          console.info(`Log download visualization: ${message}`);
        }
      });
    }
  }
}
