import { Injectable } from '@angular/core';

// PACKAGE
import { css } from '@emotion/css';

@Injectable()
export class HeaderV2Style {
  getDynamicStyle(): any {
    const header = css`
      display: flex;
      align-items: start;
      margin-bottom: 10px;
    `;

    const title = css`
      margin-right: 16px;
      margin-bottom: 0;
      font-family: Roboto;
      font-weight: 700;
    `;

    const action = css`
      margin-left: auto;
      display: flex;
      align-items: center;

      .dropdown {
        margin-left: 10px;

        .dropdown-menu {
          padding: 10px 0;
          li {
            list-style: none;

            a {
              display: block;
              padding: 5px 15px;
              cursor: pointer;
              font-size: 13px;

              &:hover {
                color: #ffffff;
                background-color: #0753a6;
                background-image: unset;
              }
            }
          }
        }

        &.dropdown-download:not(.disabled),
        &.dropdown-action:not(.disabled) {
          &:hover > .dropdown-menu {
            display: block;
            margin: auto;
          }
        }
      }
    `;

    const hiddenToolbar = css`
      background-color: white;
      width: 100%;
      height: 30px;
      margin-top: -30px;
      z-index: 99;
      position: sticky;
    `;

    return {
      header,
      title,
      action,
      hiddenToolbar,
    };
  }
}
