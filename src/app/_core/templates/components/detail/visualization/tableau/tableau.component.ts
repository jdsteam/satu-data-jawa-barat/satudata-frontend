import {
  Component,
  OnInit,
  OnDestroy,
  Output,
  EventEmitter,
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { filter, take, takeUntil } from 'rxjs/operators';
import { select, Store } from '@ngrx/store';
import { Actions } from '@ngrx/effects';

// SERVICE
import { AuthenticationService } from '@services-v3';

// MODEL
import { LoginData, VisualizationData } from '@models-v3';

// STORE
import {
  selectVisualization,
  selectIsLoadingRead,
} from '@store-v3/visualization/visualization.selectors';

import moment from 'moment';

declare const tableau: any;

@Component({
  selector: 'app-detail-visualization-tableau',
  templateUrl: './tableau.component.html',
  styleUrls: ['./tableau.component.scss'],
})
export class TableauComponent implements OnInit, OnDestroy {
  satudataUser: LoginData;
  moment: any = moment;

  @Output() tableauVizOutput = new EventEmitter<any>();
  @Output() accessTableauOutput = new EventEmitter<boolean>();
  @Output() accessInfoTableauOutput = new EventEmitter<string>();

  // Variable
  id: number;
  title: string;
  tableauViz: any;
  vizInit = false;
  accessTableau: boolean;
  accessInfoTableau: string;
  errorTableau: string;

  // Data
  data$: Observable<VisualizationData>;
  isLoadingRead$: Observable<boolean>;
  isLoadingUpdate$: Observable<boolean>;
  isError$: Observable<boolean>;
  isError = false;
  errorMessage: string;

  private unsubscribeVisualization$ = new Subject<void>();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private store: Store<any>,
    private actions$: Actions,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    this.id = this.route.snapshot.params.id;
  }

  ngOnInit(): void {
    this.getVisualization();
  }

  ngOnDestroy() {
    this.unsubscribeVisualization$.next();
    this.unsubscribeVisualization$.complete();
  }

  getVisualization(): void {
    this.isLoadingRead$ = this.store.pipe(select(selectIsLoadingRead));

    this.store.pipe(select(selectVisualization(this.id)));

    this.store
      .pipe(
        takeUntil(this.unsubscribeVisualization$),
        select(selectVisualization(this.id)),
        filter((val) => val !== undefined),
        take(1),
      )
      .subscribe((result) => {
        const canAccess = result.can_access;

        if (canAccess) {
          this.accessTableau = true;
          this.getTableau(result.url);
        } else {
          this.accessTableau = false;
          this.accessInfoTableau = 'can_access';
          this.accessTableauOutput.emit(false);
          this.accessInfoTableauOutput.emit('can_access');
        }
      });
  }

  getTableau(url: string): void {
    if (this.vizInit) {
      this.tableauViz.dispose();
      this.vizInit = false;
    }

    const placeholderDiv = document.getElementById('vizContainer');
    const options = {
      hideToolbar: true,
      hideTabs: true,
      onFirstInteractive: () => {
        this.vizInit = true;
      },
    };

    try {
      this.tableauViz = new tableau.Viz(placeholderDiv, url, options);
      this.tableauVizOutput.emit(this.tableauViz);
      this.accessTableauOutput.emit(true);
    } catch (err) {
      this.accessTableau = false;
      this.accessInfoTableau = 'error_response';
      // eslint-disable-next-line no-underscore-dangle
      this.errorTableau = err._message;
      this.accessTableauOutput.emit(false);
      this.accessInfoTableauOutput.emit('error_response');
    }
  }
}
