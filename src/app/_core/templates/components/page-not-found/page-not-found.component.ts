import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

// STYLE
import * as jds from '@styles';

@Component({
  selector: 'app-page-not-found',
  standalone: true,
  imports: [CommonModule],
  template: `
    <div
      [ngClass]="[
        jds.d('flex'),
        jds.alignItems('center'),
        jds.justifyContent('center')
      ]"
      [style.flexDirection]="'column'"
      [style.height.vh]="100"
    >
      <h1>404</h1>
      <em>Halaman tidak ditemukan</em>
      <p>Halaman yang Anda cari tidak ditemukan.</p>
    </div>
  `,
})
export class PageNotFoundComponent {
  jds: any = jds;
}
