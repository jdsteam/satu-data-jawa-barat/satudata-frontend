import { Injectable, Inject, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { Router, RouterStateSnapshot } from '@angular/router';

import { LOCATION, HISTORY } from '@ng-web-apis/common';

// COMPONENT
import { JdsBiModalService } from '@jds-bi/core';
import { DatasetLeaveComponent as ModalDatasetLeaveComponent } from '@components-v3/modal/dataset-leave/dataset-leave.component';

@Injectable({ providedIn: 'root' })
export class CanDeactivateGuard {
  @ViewChild(ModalDatasetLeaveComponent)
  modalDatasetLeaveComponent: ModalDatasetLeaveComponent;

  constructor(
    @Inject(LOCATION) readonly windowLocation,
    @Inject(HISTORY) readonly windowHistory,
    private location: Location,
    private router: Router,
    private modalService: JdsBiModalService,
  ) {}

  async canDeactivate(currentState: RouterStateSnapshot): Promise<boolean> {
    // Bypass can deactivate
    if (this.router.getCurrentNavigation()?.extras?.state?.bypassFormGuard) {
      return true;
    }

    // Show popup
    const modalRef = this.modalService.openComponent(
      ModalDatasetLeaveComponent,
    );

    const value = await modalRef
      .onResult()
      .toPromise()
      .then(
        (closed) => {
          return closed;
        },
        (dismissed) => {
          return dismissed;
        },
      );

    // Redirect
    let redirect: boolean;
    if (value) {
      redirect = true;
    } else {
      const currentUrl = currentState?.url;
      if (this.location.isCurrentPathEqualTo(currentUrl)) {
        this.router.navigate([currentUrl], { skipLocationChange: true });
      } else {
        this.windowHistory.pushState(null, null, this.windowLocation.href);
      }

      redirect = false;
    }

    return redirect;
  }
}
