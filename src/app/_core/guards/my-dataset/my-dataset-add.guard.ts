import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

// Service
import { AuthenticationService } from '@services-v3';

@Injectable({ providedIn: 'root' })
export class MyDatasetAddGuard {
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
  ) {}

  async canActivate() {
    const satudataUser = this.authenticationService.satudataUserValue;

    let access: boolean;
    switch (satudataUser.role_name) {
      case 'walidata':
      case 'opd':
        access = true;
        break;
      case 'opd-view':
        access = false;
        break;
      default:
        break;
    }

    if (!access) {
      this.router.navigate(['/404']);
      return false;
    }

    return true;
  }
}
