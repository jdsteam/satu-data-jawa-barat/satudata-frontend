import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot } from '@angular/router';

// SERVICE
import { AuthenticationService, DatasetService } from '@services-v3';

@Injectable({ providedIn: 'root' })
export class MyDatasetEditGuard {
  id: number;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private datasetService: DatasetService,
  ) {}

  async canActivate(route: ActivatedRouteSnapshot) {
    const user = this.authenticationService.satudataUserValue;

    this.id = route.params.id;

    const dataset = await this.datasetService
      .getSingle(this.id, {})
      .toPromise();
    const { data } = dataset;

    let access: boolean;
    switch (user.role_name) {
      case 'walidata':
        access = true;
        break;
      case 'opd':
        if (dataset.error === 1 || user.kode_skpd !== data.skpd.kode_skpd) {
          access = false;
        } else {
          access = true;
        }
        break;
      case 'opd-view':
        access = false;
        break;
      default:
        break;
    }

    if (!access) {
      this.router.navigate(['/404']);
      return false;
    }

    return true;
  }
}
