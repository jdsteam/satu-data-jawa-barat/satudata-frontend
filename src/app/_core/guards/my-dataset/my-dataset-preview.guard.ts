import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot } from '@angular/router';

// MODEL
import { LoginData } from '@models-v3';

// SERVICE
import { AuthenticationService } from '@services-v3';

@Injectable({ providedIn: 'root' })
export class MyDatasetPreviewGuard {
  satudataUser: LoginData;

  // Variable
  id: number;
  title: string;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
  }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    this.id = route.params.id;

    let access: boolean;
    switch (this.satudataUser.role_name) {
      case 'walidata':
      case 'opd':
        access = true;
        break;
      case 'opd-view':
        access = false;
        break;
      default:
        break;
    }

    if (!access) {
      this.router.navigate(['/404']);
      return false;
    }

    return true;
  }
}
