import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot } from '@angular/router';

// SERVICE
import { AuthenticationService, VisualizationService } from '@services-v3';

@Injectable({ providedIn: 'root' })
export class MyVisualizationEditGuard {
  id: number;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private visualizationService: VisualizationService,
  ) {}

  async canActivate(route: ActivatedRouteSnapshot) {
    const user = this.authenticationService.satudataUserValue;

    this.id = route.params.id;

    // const visualization = await this.visualizationService.getSingle(this.id, {}).toPromise();
    // const data = visualization.data;

    let access: boolean;
    switch (user.role_name) {
      case 'walidata':
        access = true;
        break;
      // case 'opd':
      //   if (visualization.error === 1 || user.kode_skpd !== data.skpd.kode_skpd) {
      //     access = false;
      //   } else {
      //     access = true;
      //   }
      //   break;
      case 'opd':
      case 'opd-view':
        access = false;
        break;
      default:
        break;
    }

    if (!access) {
      this.router.navigate(['/404']);
      return false;
    }

    return true;
  }
}
