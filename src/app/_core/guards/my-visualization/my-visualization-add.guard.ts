import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

// Service
import { AuthenticationService } from '@services-v3';

@Injectable({ providedIn: 'root' })
export class MyVisualizationAddGuard {
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
  ) {}

  async canActivate() {
    const user = this.authenticationService.satudataUserValue;

    let access: boolean;
    switch (user.role_name) {
      case 'walidata':
        access = true;
        break;
      case 'opd':
      case 'opd-view':
        access = false;
        break;
      default:
        break;
    }

    if (!access) {
      this.router.navigate(['/404']);
      return false;
    }

    return true;
  }
}
