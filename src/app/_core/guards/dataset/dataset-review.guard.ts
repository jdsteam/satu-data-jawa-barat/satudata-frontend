import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot } from '@angular/router';

// SERVICE
import { DatasetService } from '@services-v3';

@Injectable({ providedIn: 'root' })
export class DatasetReviewGuard {
  id: number;

  constructor(
    private router: Router,
    private datasetService: DatasetService,
  ) {}

  async canActivate(route: ActivatedRouteSnapshot) {
    this.id = route.params.id;

    const params = {
      review: true,
    };

    const response = await this.datasetService
      .getSingle(this.id, params)
      .toPromise();

    if (response.error === 1) {
      this.router.navigate(['/404']);
      return false;
    }
    return true;
  }
}
