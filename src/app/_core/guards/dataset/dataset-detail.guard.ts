import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot } from '@angular/router';

// SERVICE
import { AuthenticationService, DatasetService } from '@services-v3';

@Injectable({ providedIn: 'root' })
export class DatasetDetailGuard {
  id: number;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private datasetService: DatasetService,
  ) {}

  async canActivate(route: ActivatedRouteSnapshot) {
    const user = this.authenticationService.satudataUserValue;

    this.id = route.params.id;

    // Get Single Dataset
    const params = {
      where: JSON.stringify({
        is_deleted: false,
      }),
    };

    const dataset = await this.datasetService
      .getSingle(this.id, params)
      .toPromise();
    const { data } = dataset;

    if (dataset.error === 1) {
      this.router.navigate(['/404']);
      return false;
    }

    // Check Access
    let access: boolean;
    switch (user.role_name) {
      case 'walidata':
        access = true;
        break;
      case 'opd':
        if (user.kode_skpd === data.skpd.kode_skpd) {
          access = true;
        } else if (
          data.is_validate === 3 &&
          user.kode_skpd !== data.skpd.kode_skpd
        ) {
          access = true;
        } else {
          access = false;
        }
        break;
      case 'opd-view':
        if (data.is_validate === 3) {
          access = true;
        } else {
          access = false;
        }
        break;
      default:
        break;
    }

    if (!access) {
      this.router.navigate(['/404']);
      return false;
    }

    return true;
  }
}
