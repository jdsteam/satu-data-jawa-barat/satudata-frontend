import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

// SERVICE
import { AuthenticationService } from '@services-v3';

@Injectable({ providedIn: 'root' })
export class DeskPageGuard {
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
  ) {}

  async canActivate() {
    const user = this.authenticationService.satudataUserValue;

    let access: boolean;
    switch (user.role_name) {
      case 'walidata':
      case 'opd':
        access = true;
        break;
      case 'opd-view':
        access = false;
        break;
      default:
        break;
    }

    if (!access) {
      this.router.navigate(['/404']);
      return false;
    }

    return true;
  }
}
