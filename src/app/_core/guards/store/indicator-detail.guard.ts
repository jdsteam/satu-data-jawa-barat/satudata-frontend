import { Injectable, inject } from '@angular/core';
import { Router, ActivatedRouteSnapshot } from '@angular/router';
import { concat, Observable } from 'rxjs';
import { first, map, skip, take, tap } from 'rxjs/operators';

// MODEL
import { IndicatorData } from '@models-v3';

// SERVICE
import { AuthenticationService } from '@services-v3';

// PLUGIN
import { Store, select } from '@ngrx/store';
import { Actions, ofType } from '@ngrx/effects';

// STORE
import { selectIndicator } from '@store-v3/indicator/indicator.selectors';
import { fromIndicatorActions } from '@store-v3/indicator/indicator.actions';

@Injectable({ providedIn: 'root' })
export class StoreIndicatorDetailGuard {
  // Variable
  id: number;
  title: string;

  private router = inject(Router);
  private authenticationService = inject(AuthenticationService);

  constructor(
    private store: Store<any>,
    private actions$: Actions,
  ) {}

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    this.id = route.params.id;

    return concat(this.loadIfRequired(), this.hasIndicatorInStore()).pipe(
      skip(1),
    );
  }

  private get indicatorState$(): Observable<IndicatorData> {
    return this.store.pipe(select(selectIndicator(this.id)));
  }

  private loadIfRequired(): Observable<any> {
    return this.indicatorState$.pipe(
      map((state) => state !== undefined),
      take(1),
      tap((load) => {
        if (!load) {
          // Get Single Indicator
          const params = {
            detail: true,
            where: JSON.stringify({
              is_deleted: false,
            }),
          };

          this.store.dispatch(
            fromIndicatorActions.loadIndicator({
              id: this.id,
              params,
            }),
          );

          // Check Empty
          this.actions$
            .pipe(ofType(fromIndicatorActions.clearIndicator))
            .subscribe(() => {
              this.router.navigate(['/404']);
            });
        }
      }),
    );
  }

  private hasIndicatorInStore(): Observable<boolean> {
    const user = this.authenticationService.satudataUserValue;

    return this.indicatorState$.pipe(
      first((state) => state !== undefined),
      map((result) => {
        // eslint-disable-next-line no-extra-boolean-cast
        if (!!result) {
          // Check Access
          let access: boolean;
          switch (user.role_name) {
            case 'walidata':
              access = true;
              break;
            case 'opd':
              if (user.kode_skpd === result.kode_skpd) {
                access = true;
              } else if (
                result.is_validate === 3 &&
                user.kode_skpd !== result.kode_skpd
              ) {
                access = true;
              } else {
                access = false;
              }
              break;
            case 'opd-view':
              if (result.is_validate === 3) {
                access = true;
              } else {
                access = false;
              }
              break;
            default:
              break;
          }

          if (!access) {
            this.router.navigate(['/404']);
            return false;
          }

          // Get Counter Indicator
          this.store.dispatch(
            fromIndicatorActions.counterIndicator({
              id: this.id,
              params: {
                category: 'view',
              },
            }),
          );

          return true;
        }

        this.router.navigate(['/404']);
        return false;
      }),
    );
  }
}
