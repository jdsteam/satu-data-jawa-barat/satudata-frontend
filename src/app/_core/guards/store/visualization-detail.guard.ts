import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot } from '@angular/router';
import { concat, Observable } from 'rxjs';
import { first, map, skip, take, tap } from 'rxjs/operators';

// MODEL
import { VisualizationData } from '@models-v3';

// PLUGIN
import { Store, select } from '@ngrx/store';
import { Actions, ofType } from '@ngrx/effects';

// STORE
import { selectVisualization } from '@store-v3/visualization/visualization.selectors';
import { fromVisualizationActions } from '@store-v3/visualization/visualization.actions';

@Injectable({ providedIn: 'root' })
export class StoreVisualizationDetailGuard {
  // Variable
  id: number;
  title: string;

  constructor(
    private router: Router,
    private store: Store<any>,
    private actions$: Actions,
  ) {}

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    this.id = route.params.id;

    return concat(this.loadIfRequired(), this.hasVisualizationInStore()).pipe(
      skip(1),
    );
  }

  private get visualizationState$(): Observable<VisualizationData> {
    return this.store.pipe(select(selectVisualization(this.id)));
  }

  private loadIfRequired(): Observable<any> {
    return this.visualizationState$.pipe(
      map((state) => state !== undefined),
      take(1),
      tap((load) => {
        if (!load) {
          // Get Single Visualization
          const params = {
            detail: true,
            where: JSON.stringify({
              is_deleted: false,
            }),
          };

          this.store.dispatch(
            fromVisualizationActions.loadVisualization({
              id: this.id,
              params,
            }),
          );
        }

        // Check Empty
        this.actions$
          .pipe(ofType(fromVisualizationActions.clearVisualization))
          .subscribe(() => {
            this.router.navigate(['/404']);
          });
      }),
    );
  }

  private hasVisualizationInStore(): Observable<boolean> {
    return this.visualizationState$.pipe(
      first((state) => state !== undefined),
      map((result) => {
        // eslint-disable-next-line no-extra-boolean-cast
        if (!!result) {
          // Get Counter Visualization
          this.store.dispatch(
            fromVisualizationActions.counterVisualization({
              id: this.id,
              params: {
                category: 'view_private',
              },
            }),
          );

          return true;
        }

        this.router.navigate(['/404']);
        return false;
      }),
    );
  }
}
