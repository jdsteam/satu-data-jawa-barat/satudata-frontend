import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot } from '@angular/router';
import { concat, Observable, of } from 'rxjs';
import { first, map, skip, take, tap } from 'rxjs/operators';

// MODEL
import { DatasetData } from '@models-v3';

// SERVICE
import { AuthenticationService } from '@services-v3';

// PLUGIN
import { Store, select } from '@ngrx/store';
import { Actions, ofType } from '@ngrx/effects';

// STORE
import { selectDataset } from '@store-v3/dataset/dataset.selectors';
import { fromDatasetActions } from '@store-v3/dataset/dataset.actions';

@Injectable({ providedIn: 'root' })
export class StoreDatasetDetailGuard {
  // Variable
  id: number;
  title: string;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private store: Store<any>,
    private actions$: Actions,
  ) {}

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    this.id = route.params.id;

    return concat(this.loadRequired(), this.hasDatasetInStore()).pipe(skip(1));
  }

  private get datasetState$(): Observable<DatasetData> {
    return this.store.pipe(select(selectDataset(this.id)));
  }

  private loadRequired(): Observable<any> {
    const params = {
      detail: true,
      where: JSON.stringify({
        is_deleted: false,
      }),
    };

    this.store.dispatch(
      fromDatasetActions.loadDataset({
        id: this.id,
        params,
      }),
    );

    // Check Empty
    this.actions$
      .pipe(ofType(fromDatasetActions.clearDataset))
      .subscribe(() => {
        this.router.navigate(['/404']);
      });

    return of(true);
  }

  private loadIfRequired(): Observable<any> {
    return this.datasetState$.pipe(
      map((state) => state !== undefined),
      take(1),
      tap((load) => {
        if (!load) {
          // Get Single Dataset
          const params = {
            detail: true,
            where: JSON.stringify({
              is_deleted: false,
            }),
          };

          this.store.dispatch(
            fromDatasetActions.loadDataset({
              id: this.id,
              params,
            }),
          );

          // Check Empty
          this.actions$
            .pipe(ofType(fromDatasetActions.clearDataset))
            .subscribe(() => {
              this.router.navigate(['/404']);
            });
        }
      }),
    );
  }

  private hasDatasetInStore(): Observable<boolean> {
    const user = this.authenticationService.satudataUserValue;

    return this.datasetState$.pipe(
      first((state) => state !== undefined),
      map((result) => {
        // eslint-disable-next-line no-extra-boolean-cast
        if (!!result) {
          // Check Access
          let access: boolean;
          switch (user.role_name) {
            case 'walidata':
              access = true;
              break;
            case 'opd':
              if (user.kode_skpd === result.skpd.kode_skpd) {
                access = true;
              } else if (
                result.is_validate === 3 &&
                user.kode_skpd !== result.skpd.kode_skpd
              ) {
                access = true;
              } else {
                access = false;
              }
              break;
            case 'opd-view':
              if (result.is_validate === 3) {
                access = true;
              } else {
                access = false;
              }
              break;
            default:
              break;
          }

          if (!access) {
            this.router.navigate(['/404']);
            return false;
          }

          // Get Counter Dataset
          this.store.dispatch(
            fromDatasetActions.counterDataset({
              id: this.id,
              params: {
                category: 'view_private',
              },
            }),
          );

          return true;
        }

        this.router.navigate(['/404']);
        return false;
      }),
    );
  }
}
