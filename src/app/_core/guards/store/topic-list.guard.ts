import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { concat, Observable } from 'rxjs';
import { first, map, skip, take, tap } from 'rxjs/operators';

// MODEL
import { TopicData } from '@models-v3';

// PLUGIN
import { Store, select } from '@ngrx/store';

// STORE
import { selectAllTopic } from '@store-v3/topic/topic.selectors';
import { fromTopicActions } from '@store-v3/topic/topic.actions';

@Injectable({ providedIn: 'root' })
export class StoreTopicListGuard {
  constructor(
    private router: Router,
    private store: Store<any>,
  ) {}

  canActivate(): Observable<boolean> {
    return concat(this.loadIfRequired(), this.hasTopicInStore()).pipe(skip(1));
  }

  private get topicState$(): Observable<TopicData[]> {
    return this.store.pipe(select(selectAllTopic));
  }

  private loadIfRequired(): Observable<any> {
    return this.topicState$.pipe(
      map((state) => state.length !== 0),
      take(1),
      tap((load) => {
        if (!load) {
          const pWhere = {
            is_deleted: false,
          };

          const params = {
            sort: 'name:asc',
            where: JSON.stringify(pWhere),
          };

          this.store.dispatch(
            fromTopicActions.loadAllTopic({
              params,
              pagination: false,
            }),
          );
        }
      }),
    );
  }

  private hasTopicInStore(): Observable<boolean> {
    return this.topicState$.pipe(
      first((state) => state.length !== 0),
      map((result) => {
        // eslint-disable-next-line no-extra-boolean-cast
        if (!!result) {
          return true;
        }

        this.router.navigate(['/404']);
        return false;
      }),
    );
  }
}
