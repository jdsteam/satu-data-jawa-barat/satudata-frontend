import { Injectable } from '@angular/core';
import {
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';

// MODEL
import { LoginData } from '@models-v3';

// SERVICE
import { AuthenticationService, StateService } from '@services-v3';

// PLUGIN
import { JwtHelperService } from '@auth0/angular-jwt';
import { v4 as uuidv4 } from 'uuid';

@Injectable({ providedIn: 'root' })
export class AuthGuard {
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private stateService: StateService,
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const helper = new JwtHelperService();

    const satudataUser = this.authenticationService.satudataUserValue;
    const satudataToken = this.authenticationService.satudataTokenValue;

    if (satudataUser === null) {
      this.router.navigate(['/auth/login'], {
        queryParams: { returnUrl: state.url },
      });
      return false;
    }

    const isExpiredUser = helper.isTokenExpired(satudataUser.jwt);
    const isExpired = helper.isTokenExpired(satudataToken);

    if (satudataUser && (!isExpiredUser || !isExpired)) {
      this.setLogUser(satudataUser);
      return true;
    }

    this.router.navigate(['/auth/login'], {
      queryParams: { returnUrl: state.url },
    });
    return false;
  }

  setLogUser(user: LoginData) {
    const userInfo = {
      uuid: uuidv4(),
      user_id: user.id,
      user_username: user.username,
      user_name: user.name,
      user_role: user.role_name,
      user_organization_id: user.satuan_kerja_id,
      user_organization_name: user.satuan_kerja_nama,
      user_position_id: user.jabatan_id,
      user_position_name: user.jabatan_nama,
    };

    this.stateService.changeLogUser(userInfo);
  }
}
