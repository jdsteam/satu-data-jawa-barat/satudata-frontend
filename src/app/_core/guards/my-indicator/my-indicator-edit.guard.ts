import { Injectable, inject } from '@angular/core';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { lastValueFrom } from 'rxjs';

// SERVICE
import { AuthenticationService, IndicatorService } from '@services-v3';

@Injectable({ providedIn: 'root' })
export class MyIndicatorEditGuard {
  // Service
  private router = inject(Router);
  private authenticationService = inject(AuthenticationService);
  private indicatorService = inject(IndicatorService);

  // Variable
  id: number;

  async canActivate(route: ActivatedRouteSnapshot) {
    const user = this.authenticationService.satudataUserValue;

    this.id = route.params.id;

    const indicator = await lastValueFrom(
      this.indicatorService.getSingle(this.id, {}),
    );
    const { data } = indicator;

    const userSkpd = user.kode_skpd;
    const indicatorSkpd = data.kode_skpd;

    let access: boolean;
    switch (user.role_name) {
      case 'walidata':
        access = true;
        break;
      case 'opd':
        if (indicator.error === 1 || userSkpd !== indicatorSkpd) {
          access = false;
        } else {
          access = true;
        }
        break;
      case 'opd-view':
        access = false;
        break;
      default:
        break;
    }

    if (!access) {
      this.router.navigate(['/404']);
      return false;
    }

    return true;
  }
}
