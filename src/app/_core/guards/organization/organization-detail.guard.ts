import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot } from '@angular/router';
import { concat, Observable } from 'rxjs';
import { first, map, skip, take, tap } from 'rxjs/operators';

// MODEL
import { OrganizationData } from '@models-v3';

// PLUGIN
import { Store, select } from '@ngrx/store';

// STORE
import { selectOrganization } from '@store-v3/organization/organization.selectors';
import { fromOrganizationActions } from '@store-v3/organization/organization.actions';

@Injectable({ providedIn: 'root' })
export class OrganizationDetailGuard {
  // Variable
  id: number;
  title: string;

  constructor(
    private router: Router,
    private store: Store<any>,
  ) {}

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    this.id = route.params.id;

    return concat(this.loadIfRequired(), this.hasOrganizationInStore()).pipe(
      skip(1),
    );
  }

  private get organizationState$(): Observable<OrganizationData> {
    return this.store.pipe(select(selectOrganization(this.id)));
  }

  private loadIfRequired(): Observable<any> {
    return this.organizationState$.pipe(
      map((state) => state !== undefined),
      take(1),
      tap((load) => {
        if (!load) {
          const params = {
            detail: true,
          };

          this.store.dispatch(
            fromOrganizationActions.loadOrganization({
              id: this.id,
              params,
            }),
          );
        }
      }),
    );
  }

  private hasOrganizationInStore(): Observable<boolean> {
    return this.organizationState$.pipe(
      first((state) => state !== undefined),
      map((result) => {
        // eslint-disable-next-line no-extra-boolean-cast
        if (!!result) {
          return true;
        }

        this.router.navigate(['/404']);
        return false;
      }),
    );
  }
}
