export async function handlePromise(
  promise: any,
): Promise<{ data: any; error: any }> {
  try {
    const data = await promise;
    return { data, error: undefined };
  } catch (error) {
    return { data: undefined, error };
  }
}
