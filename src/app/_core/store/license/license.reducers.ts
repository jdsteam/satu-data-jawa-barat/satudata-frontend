import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { LicenseData } from '@models-v3';
import { fromLicenseActions } from '@store-v3/license/license.actions';

export const ENTITY_FEATURE_KEY = 'licenseV3';

export interface State extends EntityState<LicenseData> {
  isLoadingList: boolean;
  isLoadingCreate: boolean;
  isLoadingRead: boolean;
  isLoadingUpdate: boolean;
  isLoadingDelete: boolean;
  metadata: any;
  pagination: any;
  error?: Error | any;
}

export const adapter: EntityAdapter<LicenseData> =
  createEntityAdapter<LicenseData>({
    selectId: (item) => item.id,
  });

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  isLoadingList: false,
  isLoadingCreate: false,
  isLoadingRead: false,
  isLoadingUpdate: false,
  isLoadingDelete: false,
  metadata: null,
  pagination: null,
  error: null,
});

const reducer = createReducer(
  initialState,

  // Load All
  on(fromLicenseActions.loadAllLicense, (state) => {
    return {
      ...state,
      isLoadingList: true,
      error: null,
    };
  }),
  on(
    fromLicenseActions.loadAllLicenseSuccess,
    (state, { data, pagination }) => {
      if (!pagination.infinite) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          error: { error: false },
        });
      }

      if (pagination.current_page === 1) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          error: { error: false },
        });
      }

      return adapter.addMany(data, {
        ...state,
        isLoadingList: false,
        pagination,
        error: { error: false },
      });
    },
  ),
  on(fromLicenseActions.loadAllLicenseFailure, (state, { error }) => {
    return {
      ...state,
      isLoadingList: false,
      error,
    };
  }),

  // Load Single & Load Single By Title
  on(
    fromLicenseActions.loadLicense,
    fromLicenseActions.loadLicenseByTitle,
    (state) => {
      return {
        ...state,
        isLoadingRead: true,
        error: null,
      };
    },
  ),
  on(
    fromLicenseActions.loadLicenseSuccess,
    fromLicenseActions.loadLicenseByTitleSuccess,
    (state, { data }) => {
      return adapter.addOne(data, {
        ...state,
        isLoadingRead: false,
        error: { error: false },
      });
    },
  ),
  on(
    fromLicenseActions.loadLicenseFailure,
    fromLicenseActions.loadLicenseByTitleFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingRead: false,
        error,
      };
    },
  ),

  // Clear
  on(fromLicenseActions.clearLicense, (state) => {
    return adapter.removeAll({
      ...state,
      isLoadingList: false,
      pagination: { empty: true },
      error: { error: false },
    });
  }),
);

export function LicenseReducer(state: State | undefined, action: Action): any {
  return reducer(state, action);
}
