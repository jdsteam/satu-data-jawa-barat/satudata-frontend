import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';

import { fromLicenseActions } from '@store-v3/license/license.actions';
import { LicenseService, PaginationService } from '@services-v3';
import { handlePromise } from '@helpers-v3';

@Injectable()
export class LicenseEffects {
  name = 'License';

  loadAllLicense$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromLicenseActions.loadAllLicense),
      switchMap(async (action) => {
        const { data: result, error: resultErr } = await handlePromise(
          this.licenseService.getList(action.params).toPromise(),
        );

        if (resultErr) {
          return this.loadAllFailure(resultErr.message);
        }

        if (result.error === 1) {
          if (result.message === 'Data not found') {
            return fromLicenseActions.clearLicense();
          }

          return this.loadAllFailure(result.message);
        }

        let pagination = { empty: true, infinite: action.infinite };
        if (action.pagination) {
          const resultPagination = this.paginationService.getPagination(
            result.meta.total_record,
            action.params.skip,
            action.params.limit,
          );

          pagination = {
            empty: false,
            infinite: action.infinite,
            ...resultPagination,
          };
        }

        return this.loadAllSuccess(result.data, pagination);
      }),
    ),
  );

  loadLicense$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromLicenseActions.loadLicense),
      switchMap((action) => {
        return this.licenseService.getSingle(action.id, action.params).pipe(
          map((result: any) =>
            fromLicenseActions.loadLicenseSuccess({
              data: result.data,
            }),
          ),
          catchError((error: HttpErrorResponse) =>
            of(
              fromLicenseActions.loadLicenseFailure({
                error: {
                  name: this.name,
                  error: !error.ok,
                  message: error.message,
                },
              }),
            ),
          ),
        );
      }),
    ),
  );

  loadLicenseByTitle$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromLicenseActions.loadLicenseByTitle),
      switchMap((action) => {
        return this.licenseService
          .getSingleByTitle(action.title, action.params)
          .pipe(
            map((result: any) =>
              fromLicenseActions.loadLicenseByTitleSuccess({
                data: result.data,
              }),
            ),
            catchError((error: HttpErrorResponse) =>
              of(
                fromLicenseActions.loadLicenseByTitleFailure({
                  error: {
                    name: this.name,
                    error: !error.ok,
                    message: error.message,
                  },
                }),
              ),
            ),
          );
      }),
    ),
  );

  constructor(
    private actions$: Actions,
    private licenseService: LicenseService,
    private paginationService: PaginationService,
  ) {}

  loadAllSuccess(data: any, pagination: any): any {
    return fromLicenseActions.loadAllLicenseSuccess({
      data,
      pagination,
    });
  }

  loadAllFailure(error: string): any {
    return fromLicenseActions.loadAllLicenseFailure({
      error: {
        name: this.name,
        error: true,
        message: error,
      },
    });
  }
}
