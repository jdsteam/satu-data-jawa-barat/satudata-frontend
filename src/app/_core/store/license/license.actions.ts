import { createAction, props } from '@ngrx/store';
import { LicenseData } from '@models-v3';

export enum LicenseActionTypes {
  LoadAllLicense = '[LICENSE] Load All License',
  LoadAllLicenseSuccess = '[LICENSE] Load All License Success',
  LoadAllLicenseFailure = '[LICENSE] Load All License Failure',
  LoadLicense = '[LICENSE] Load License',
  LoadLicenseSuccess = '[LICENSE] Load License Success',
  LoadLicenseFailure = '[LICENSE] Load License Failure',
  LoadLicenseByTitle = '[LICENSE] Load License By Title',
  LoadLicenseByTitleSuccess = '[LICENSE] Load License By Title Success',
  LoadLicenseByTitleFailure = '[LICENSE] Load License By Title Failure',
  ClearLicense = '[LICENSE] Clear License',
}

// Load All
export const loadAllLicense = createAction(
  LicenseActionTypes.LoadAllLicense,
  props<{ params: any; pagination: boolean; infinite: boolean }>(),
);

export const loadAllLicenseSuccess = createAction(
  LicenseActionTypes.LoadAllLicenseSuccess,
  props<{ data: LicenseData[]; pagination: any }>(),
);

export const loadAllLicenseFailure = createAction(
  LicenseActionTypes.LoadAllLicenseFailure,
  props<{ error: Error | any }>(),
);

// Load Single
export const loadLicense = createAction(
  LicenseActionTypes.LoadLicense,
  props<{ id: number; params: any }>(),
);

export const loadLicenseSuccess = createAction(
  LicenseActionTypes.LoadLicenseSuccess,
  props<{ data: LicenseData }>(),
);

export const loadLicenseFailure = createAction(
  LicenseActionTypes.LoadLicenseFailure,
  props<{ error: Error | any }>(),
);

// Load Single By Title
export const loadLicenseByTitle = createAction(
  LicenseActionTypes.LoadLicenseByTitle,
  props<{ title: string; params: any }>(),
);

export const loadLicenseByTitleSuccess = createAction(
  LicenseActionTypes.LoadLicenseByTitleSuccess,
  props<{ data: LicenseData }>(),
);

export const loadLicenseByTitleFailure = createAction(
  LicenseActionTypes.LoadLicenseByTitleFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearLicense = createAction(LicenseActionTypes.ClearLicense);

export const fromLicenseActions = {
  loadAllLicense,
  loadAllLicenseSuccess,
  loadAllLicenseFailure,
  loadLicense,
  loadLicenseSuccess,
  loadLicenseFailure,
  loadLicenseByTitle,
  loadLicenseByTitleSuccess,
  loadLicenseByTitleFailure,
  clearLicense,
};
