import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';

import { fromIndicatorClassificationHistoryActions } from '@store-v3/indicator-classification-history/indicator-classification-history.actions';
import { IndicatorClassificationHistoryService } from '@services-v3';

import * as _ from 'lodash';

@Injectable()
export class IndicatorClassificationHistoryEffects {
  name = 'Indicator Classification History';

  createIndicatorClassificationHistory$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        fromIndicatorClassificationHistoryActions.createIndicatorClassificationHistory,
      ),
      switchMap((action) => {
        const indicatorId = action.indicator.id;

        const bodyClassificationHistory = [];
        _.map(action.create, (result) => {
          bodyClassificationHistory.push({
            indikator_id: indicatorId,
            indikator_class_id: result.id,
          });
        });

        return this.indicatorClassificationHistoryService
          .createItem(bodyClassificationHistory)
          .pipe(
            map((res: any) => {
              return fromIndicatorClassificationHistoryActions.createIndicatorClassificationHistorySuccess(
                {
                  data: res.data,
                },
              );
            }),
            catchError((error) => {
              return of(
                fromIndicatorClassificationHistoryActions.createIndicatorClassificationHistoryFailure(
                  {
                    error: {
                      name: this.name,
                      error: !error.ok,
                      message: error.message,
                    },
                  },
                ),
              );
            }),
          );
      }),
    ),
  );

  updateIndicatorClassificationHistoryBulk$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        fromIndicatorClassificationHistoryActions.updateIndicatorClassificationHistoryBulk,
      ),
      switchMap((action) =>
        this.indicatorClassificationHistoryService
          .updateItemBulk(action.indicatorId, action.update)
          .pipe(
            map((res: any) => {
              return fromIndicatorClassificationHistoryActions.updateIndicatorClassificationHistoryBulkSuccess(
                {
                  data: res.data,
                },
              );
            }),
            catchError((error) => {
              return of(
                fromIndicatorClassificationHistoryActions.updateIndicatorClassificationHistoryBulkFailure(
                  {
                    error: {
                      name: this.name,
                      error: !error.ok,
                      message: error.message,
                    },
                  },
                ),
              );
            }),
          ),
      ),
    ),
  );

  constructor(
    private actions$: Actions,
    private indicatorClassificationHistoryService: IndicatorClassificationHistoryService,
  ) {}
}
