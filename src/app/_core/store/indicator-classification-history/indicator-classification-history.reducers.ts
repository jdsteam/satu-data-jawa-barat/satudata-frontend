import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { IndicatorClassificationHistoryData } from '@models-v3';
import { fromIndicatorClassificationHistoryActions } from '@store-v3/indicator-classification-history/indicator-classification-history.actions';

export const ENTITY_FEATURE_KEY = 'indicatorClassificationHistoryV3';

export interface State extends EntityState<IndicatorClassificationHistoryData> {
  isLoadingList: boolean;
  isLoadingCreate: boolean;
  isLoadingRead: boolean;
  isLoadingUpdate: boolean;
  isLoadingDelete: boolean;
  isLoadingCounter: boolean;
  metadata: any;
  pagination: any;
  error?: Error | any;
}

export const adapter: EntityAdapter<IndicatorClassificationHistoryData> =
  createEntityAdapter<IndicatorClassificationHistoryData>({
    selectId: (item) => item.id,
  });

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  isLoadingList: false,
  isLoadingCreate: false,
  isLoadingRead: false,
  isLoadingUpdate: false,
  isLoadingDelete: false,
  isLoadingCounter: false,
  metadata: null,
  pagination: null,
  error: null,
});

const reducer = createReducer(
  initialState,

  // Create
  on(
    fromIndicatorClassificationHistoryActions.createIndicatorClassificationHistory,
    (state) => {
      return {
        ...state,
        isLoadingCreate: true,
      };
    },
  ),
  on(
    fromIndicatorClassificationHistoryActions.createIndicatorClassificationHistorySuccess,
    (state, { data }) => {
      return adapter.addMany(data, {
        ...state,
        isLoadingCreate: false,
        error: { error: false },
      });
    },
  ),
  on(
    fromIndicatorClassificationHistoryActions.createIndicatorClassificationHistoryFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingCreate: false,
        error,
      };
    },
  ),

  // Update Bulk
  on(
    fromIndicatorClassificationHistoryActions.updateIndicatorClassificationHistoryBulk,
    (state) => {
      return {
        ...state,
        isLoadingUpdate: true,
      };
    },
  ),
  on(
    fromIndicatorClassificationHistoryActions.updateIndicatorClassificationHistoryBulkSuccess,
    (state, { data }) => {
      return adapter.updateMany(
        data.map((res) => ({ id: res.id, changes: res })),
        {
          ...state,
          isLoadingUpdate: false,
          error: { error: false },
        },
      );
    },
  ),
  on(
    fromIndicatorClassificationHistoryActions.updateIndicatorClassificationHistoryBulkFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingUpdate: false,
        error,
      };
    },
  ),

  // Clear
  on(
    fromIndicatorClassificationHistoryActions.clearIndicatorClassificationHistory,
    (state) => {
      return adapter.removeAll({
        ...state,
        isLoadingList: false,
        pagination: { empty: true },
        error: { error: false },
      });
    },
  ),
);

export function IndicatorClassificationHistoryReducer(
  state: State | undefined,
  action: Action,
): any {
  return reducer(state, action);
}
