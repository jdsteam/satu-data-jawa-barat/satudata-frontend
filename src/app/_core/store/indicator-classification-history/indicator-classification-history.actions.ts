import { createAction, props } from '@ngrx/store';
import { IndicatorClassificationHistoryData } from '@models-v3';

export enum IndicatorClassificationHistoryActionTypes {
  CreateIndicatorClassificationHistory = '[INDICATOR CLASSIFICATION HISTORY] Create Indicator Classification History',
  CreateIndicatorClassificationHistorySuccess = '[INDICATOR CLASSIFICATION HISTORY] Create Indicator Classification History Success',
  CreateIndicatorClassificationHistoryFailure = '[INDICATOR CLASSIFICATION HISTORY] Create Indicator Classification History Failure',
  UpdateIndicatorClassificationHistoryBulk = '[INDICATOR CLASSIFICATION HISTORY] Update Indicator Classification History Bulk',
  UpdateIndicatorClassificationHistoryBulkSuccess = '[INDICATOR CLASSIFICATION HISTORY] Update Indicator Classification History Bulk Success',
  UpdateIndicatorClassificationHistoryBulkFailure = '[INDICATOR CLASSIFICATION HISTORY] Update Indicator Classification History Bulk Failure',
  ClearIndicatorClassificationHistory = '[INDICATOR CLASSIFICATION HISTORY] Clear Indicator Classification History',
}

// Create
export const createIndicatorClassificationHistory = createAction(
  IndicatorClassificationHistoryActionTypes.CreateIndicatorClassificationHistory,
  props<{ create: any; indicator: any }>(),
);

export const createIndicatorClassificationHistorySuccess = createAction(
  IndicatorClassificationHistoryActionTypes.CreateIndicatorClassificationHistorySuccess,
  props<{ data: IndicatorClassificationHistoryData[] }>(),
);

export const createIndicatorClassificationHistoryFailure = createAction(
  IndicatorClassificationHistoryActionTypes.CreateIndicatorClassificationHistoryFailure,
  props<{ error: Error | any }>(),
);

// Update Bulk
export const updateIndicatorClassificationHistoryBulk = createAction(
  IndicatorClassificationHistoryActionTypes.UpdateIndicatorClassificationHistoryBulk,
  props<{ indicatorId: number; update: any }>(),
);

export const updateIndicatorClassificationHistoryBulkSuccess = createAction(
  IndicatorClassificationHistoryActionTypes.UpdateIndicatorClassificationHistoryBulkSuccess,
  props<{ data: IndicatorClassificationHistoryData[] }>(),
);

export const updateIndicatorClassificationHistoryBulkFailure = createAction(
  IndicatorClassificationHistoryActionTypes.UpdateIndicatorClassificationHistoryBulkFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearIndicatorClassificationHistory = createAction(
  IndicatorClassificationHistoryActionTypes.ClearIndicatorClassificationHistory,
);

export const fromIndicatorClassificationHistoryActions = {
  createIndicatorClassificationHistory,
  createIndicatorClassificationHistorySuccess,
  createIndicatorClassificationHistoryFailure,
  updateIndicatorClassificationHistoryBulk,
  updateIndicatorClassificationHistoryBulkSuccess,
  updateIndicatorClassificationHistoryBulkFailure,
  clearIndicatorClassificationHistory,
};
