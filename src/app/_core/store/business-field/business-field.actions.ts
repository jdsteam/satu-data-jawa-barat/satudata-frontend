import { createAction, props } from '@ngrx/store';
import { BusinessFieldData } from '@models-v3';

export enum BusinessFieldActionTypes {
  LoadAllBusinessField = '[ORGANIZATION] Load All Business Field',
  LoadAllBusinessFieldSuccess = '[ORGANIZATION] Load All Business Field Success',
  LoadAllBusinessFieldFailure = '[ORGANIZATION] Load All Business Field Failure',
  LoadBusinessField = '[ORGANIZATION] Load Business Field',
  LoadBusinessFieldSuccess = '[ORGANIZATION] Load Business Field Success',
  LoadBusinessFieldFailure = '[ORGANIZATION] Load Business Field Failure',
  ClearBusinessField = '[ORGANIZATION] Clear Business Field',
}

// Load All
export const loadAllBusinessField = createAction(
  BusinessFieldActionTypes.LoadAllBusinessField,
  props<{ params: any; pagination: boolean; infinite: boolean }>(),
);

export const loadAllBusinessFieldSuccess = createAction(
  BusinessFieldActionTypes.LoadAllBusinessFieldSuccess,
  props<{ data: BusinessFieldData[]; pagination: any; suggestion: any }>(),
);

export const loadAllBusinessFieldFailure = createAction(
  BusinessFieldActionTypes.LoadAllBusinessFieldFailure,
  props<{ error: Error | any }>(),
);

// Load Single
export const loadBusinessField = createAction(
  BusinessFieldActionTypes.LoadBusinessField,
  props<{ id: number; params: any }>(),
);

export const loadBusinessFieldSuccess = createAction(
  BusinessFieldActionTypes.LoadBusinessFieldSuccess,
  props<{ data: BusinessFieldData }>(),
);

export const loadBusinessFieldFailure = createAction(
  BusinessFieldActionTypes.LoadBusinessFieldFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearBusinessField = createAction(
  BusinessFieldActionTypes.ClearBusinessField,
);

export const fromBusinessFieldActions = {
  loadAllBusinessField,
  loadAllBusinessFieldSuccess,
  loadAllBusinessFieldFailure,
  loadBusinessField,
  loadBusinessFieldSuccess,
  loadBusinessFieldFailure,
  clearBusinessField,
};
