import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { BusinessFieldData } from '@models-v3';
import { fromBusinessFieldActions } from '@store-v3/business-field/business-field.actions';

export const ENTITY_FEATURE_KEY = 'businessField';

export interface State extends EntityState<BusinessFieldData> {
  isLoadingList: boolean;
  isLoadingCreate: boolean;
  isLoadingRead: boolean;
  isLoadingUpdate: boolean;
  isLoadingDelete: boolean;
  metadata: any;
  suggestion: any;
  pagination: any;
  error?: Error | any;
}

export const adapter: EntityAdapter<BusinessFieldData> =
  createEntityAdapter<BusinessFieldData>({
    // selectId: (item) => item.id,
    selectId: (item) => String(item.kode_bidang_urusan),
    // selectId: (item) => String(item.title),
  });

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  isLoadingList: false,
  isLoadingCreate: false,
  isLoadingRead: false,
  isLoadingUpdate: false,
  isLoadingDelete: false,
  metadata: null,
  suggestion: null,
  pagination: null,
  error: null,
});

const reducer = createReducer(
  initialState,

  // Load All
  on(fromBusinessFieldActions.loadAllBusinessField, (state) => {
    return {
      ...state,
      isLoadingList: true,
      error: null,
    };
  }),
  on(
    fromBusinessFieldActions.loadAllBusinessFieldSuccess,
    (state, { data, pagination, suggestion }) => {
      if (!pagination.infinite) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          suggestion,
          error: { error: false },
        });
      }

      if (pagination.current_page === 1) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          suggestion,
          error: { error: false },
        });
      }

      return adapter.addMany(data, {
        ...state,
        isLoadingList: false,
        pagination,
        suggestion,
        error: { error: false },
      });
    },
  ),
  on(
    fromBusinessFieldActions.loadAllBusinessFieldFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingList: false,
        pagination: { empty: true },
        error,
      };
    },
  ),

  // Load Single & Load Single By Title
  on(fromBusinessFieldActions.loadBusinessField, (state) => {
    return {
      ...state,
      isLoadingRead: true,
      pagination: { empty: true },
      error: null,
    };
  }),
  on(fromBusinessFieldActions.loadBusinessFieldSuccess, (state, { data }) => {
    return adapter.addOne(data, {
      ...state,
      isLoadingRead: false,
      pagination: { empty: true },
      error: { error: false },
    });
  }),
  on(fromBusinessFieldActions.loadBusinessFieldFailure, (state, { error }) => {
    return {
      ...state,
      isLoadingRead: false,
      pagination: { empty: true },
      error,
    };
  }),

  // Clear
  on(fromBusinessFieldActions.clearBusinessField, (state) => {
    return adapter.removeAll({
      ...state,
      isLoadingList: false,
      pagination: { empty: true },
      suggestion: {},
      error: { error: false },
    });
  }),
);

export function BusinessFieldReducer(
  state: State | undefined,
  action: Action,
): any {
  return reducer(state, action);
}
