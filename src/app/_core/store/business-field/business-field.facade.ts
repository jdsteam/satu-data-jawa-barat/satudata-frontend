import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { filter, map } from 'rxjs/operators';

import { BusinessFieldData } from '@models-v3';
import * as BusinessFieldActions from './business-field.actions';
import { State as BusinessFieldState } from './business-field.reducers';
import * as BusinessFieldSelectors from './business-field.selectors';

@Injectable()
export class BusinessFieldFacade {
  allBusinessField$ = this.store.pipe(
    select(BusinessFieldSelectors.selectAllBusinessField),
    map((data: BusinessFieldData[]) => {
      return data.map((result: BusinessFieldData) =>
        new BusinessFieldData().deserialize(result),
      );
    }),
  );

  isLoadingListBusinessField$ = this.store.pipe(
    select(BusinessFieldSelectors.selectIsLoadingList),
  );
  isLoadingCreateBusinessField$ = this.store.pipe(
    select(BusinessFieldSelectors.selectIsLoadingCreate),
  );
  isLoadingReadBusinessField$ = this.store.pipe(
    select(BusinessFieldSelectors.selectIsLoadingRead),
  );
  isLoadingUpdateBusinessField$ = this.store.pipe(
    select(BusinessFieldSelectors.selectIsLoadingUpdate),
  );
  isLoadingDeleteBusinessField$ = this.store.pipe(
    select(BusinessFieldSelectors.selectIsLoadingDelete),
  );

  metadataBusinessField$ = this.store.pipe(
    select(BusinessFieldSelectors.selectMetadata),
  );
  paginationBusinessField$ = this.store.pipe(
    select(BusinessFieldSelectors.selectPagination),
  );
  errorBusinessField$ = this.store.pipe(
    select(BusinessFieldSelectors.selectError),
    filter((val) => val !== null && val.error),
  );

  optionsBusinessField$ = this.store.pipe(
    select(BusinessFieldSelectors.selectAllBusinessField),
    map((data: BusinessFieldData[]) => {
      const options: any[] = [];
      data.forEach((res) => {
        options.push({
          value: res.id,
          label: res.nama_bidang_urusan,
          checked: false,
        });
      });

      return options;
    }),
  );

  optionsBusinessFieldByKode$ = this.store.pipe(
    select(BusinessFieldSelectors.selectAllBusinessField),
    map((data: BusinessFieldData[]) => {
      const options: any[] = [];
      data.forEach((res) => {
        options.push({
          value: res.kode_bidang_urusan,
          label: res.nama_bidang_urusan,
          checked: false,
        });
      });

      return options;
    }),
  );

  constructor(private readonly store: Store<BusinessFieldState>) {}

  loadAllBusinessField(params: any, pagination: boolean, infinite: boolean) {
    this.store.dispatch(
      BusinessFieldActions.loadAllBusinessField({
        params,
        pagination,
        infinite,
      }),
    );
  }

  loadBusinessField(id: number, params = {}) {
    this.store.dispatch(
      BusinessFieldActions.loadBusinessField({
        id,
        params,
      }),
    );
  }

  selectBusinessField(id: number) {
    return this.store.pipe(
      select(BusinessFieldSelectors.selectBusinessField(id)),
    );
  }

  selectMultipleBusinessField(ids: string[]) {
    return this.store.pipe(
      select(BusinessFieldSelectors.selectMultipleBusinessField(ids)),
    );
  }
}
