import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';

import { fromBusinessFieldActions } from '@store-v3/business-field/business-field.actions';
import { BusinessFieldService, PaginationService } from '@services-v3';
import { handlePromise } from '@helpers-v3';

@Injectable()
export class BusinessFieldEffects {
  name = 'BusinessField';

  loadAllBusinessField$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromBusinessFieldActions.loadAllBusinessField),
      switchMap(async (action) => {
        const { data: result, error: resultErr } = await handlePromise(
          this.businessFieldService.getList(action.params).toPromise(),
        );

        if (resultErr) {
          return this.loadAllFailure(resultErr.message);
        }

        if (result.error === 1) {
          if (result.message === 'Data not found') {
            return fromBusinessFieldActions.clearBusinessField();
          }

          return this.loadAllFailure(result.message);
        }

        let pagination = { empty: true, infinite: action.infinite };
        if (action.pagination) {
          const resultPagination = this.paginationService.getPagination(
            result.meta.total_record,
            action.params.skip,
            action.params.limit,
          );

          pagination = {
            empty: false,
            infinite: action.infinite,
            ...resultPagination,
          };
        }

        const suggestion = {
          suggestion: result.suggestion,
          original_text: result.original_text,
          suggestion_text: result.suggestion_text,
        };

        return this.loadAllSuccess(result.data, pagination, suggestion);
      }),
    ),
  );

  loadBusinessField$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromBusinessFieldActions.loadBusinessField),
      switchMap((action) => {
        return this.businessFieldService
          .getSingle(action.id, action.params)
          .pipe(
            map((result: any) =>
              fromBusinessFieldActions.loadBusinessFieldSuccess({
                data: result.data,
              }),
            ),
            catchError((error: HttpErrorResponse) =>
              of(
                fromBusinessFieldActions.loadBusinessFieldFailure({
                  error: {
                    name: this.name,
                    error: !error.ok,
                    message: error.message,
                  },
                }),
              ),
            ),
          );
      }),
    ),
  );

  constructor(
    private actions$: Actions,
    private businessFieldService: BusinessFieldService,
    private paginationService: PaginationService,
  ) {}

  loadAllSuccess(data: any, pagination: any, suggestion: any): any {
    return fromBusinessFieldActions.loadAllBusinessFieldSuccess({
      data,
      pagination,
      suggestion,
    });
  }

  loadAllFailure(error: string): any {
    return fromBusinessFieldActions.loadAllBusinessFieldFailure({
      error: {
        name: this.name,
        error: true,
        message: error,
      },
    });
  }
}
