import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';

import { fromOrganizationActions } from '@store-v3/organization/organization.actions';
import { OrganizationService, PaginationService } from '@services-v3';
import { handlePromise } from '@helpers-v3';

@Injectable()
export class OrganizationEffects {
  name = 'Organization';

  loadAllOrganization$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromOrganizationActions.loadAllOrganization),
      switchMap(async (action) => {
        const { data: result, error: resultErr } = await handlePromise(
          this.organizationService.getList(action.params).toPromise(),
        );

        if (resultErr) {
          return this.loadAllFailure(resultErr.message);
        }

        if (result.error === 1) {
          if (result.message === 'Data not found') {
            return fromOrganizationActions.clearOrganization();
          }

          return this.loadAllFailure(result.message);
        }

        let pagination = { empty: true, infinite: action.infinite };
        if (action.pagination) {
          const resultPagination = this.paginationService.getPagination(
            result.meta.total_record,
            action.params.skip,
            action.params.limit,
          );

          pagination = {
            empty: false,
            infinite: action.infinite,
            ...resultPagination,
          };
        }

        const suggestion = {
          suggestion: result.suggestion,
          original_text: result.original_text,
          suggestion_text: result.suggestion_text,
        };

        return this.loadAllSuccess(result.data, pagination, suggestion);
      }),
    ),
  );

  loadOrganization$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromOrganizationActions.loadOrganization),
      switchMap((action) => {
        return this.organizationService
          .getSingle(action.id, action.params)
          .pipe(
            map((result: any) =>
              fromOrganizationActions.loadOrganizationSuccess({
                data: result.data,
              }),
            ),
            catchError((error: HttpErrorResponse) =>
              of(
                fromOrganizationActions.loadOrganizationFailure({
                  error: {
                    name: this.name,
                    error: !error.ok,
                    message: error.message,
                  },
                }),
              ),
            ),
          );
      }),
    ),
  );

  loadOrganizationByTitle$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromOrganizationActions.loadOrganizationByTitle),
      switchMap((action) => {
        return this.organizationService
          .getSingleByTitle(action.title, action.params)
          .pipe(
            map((result: any) =>
              fromOrganizationActions.loadOrganizationByTitleSuccess({
                data: result.data,
              }),
            ),
            catchError((error: HttpErrorResponse) =>
              of(
                fromOrganizationActions.loadOrganizationByTitleFailure({
                  error: {
                    name: this.name,
                    error: !error.ok,
                    message: error.message,
                  },
                }),
              ),
            ),
          );
      }),
    ),
  );

  constructor(
    private actions$: Actions,
    private organizationService: OrganizationService,
    private paginationService: PaginationService,
  ) {}

  loadAllSuccess(data: any, pagination: any, suggestion: any): any {
    return fromOrganizationActions.loadAllOrganizationSuccess({
      data,
      pagination,
      suggestion,
    });
  }

  loadAllFailure(error: string): any {
    return fromOrganizationActions.loadAllOrganizationFailure({
      error: {
        name: this.name,
        error: true,
        message: error,
      },
    });
  }
}
