import { createAction, props } from '@ngrx/store';
import { OrganizationData } from '@models-v3';

export enum OrganizationActionTypes {
  LoadAllOrganization = '[ORGANIZATION] Load All Organization',
  LoadAllOrganizationSuccess = '[ORGANIZATION] Load All Organization Success',
  LoadAllOrganizationFailure = '[ORGANIZATION] Load All Organization Failure',
  LoadOrganization = '[ORGANIZATION] Load Organization',
  LoadOrganizationSuccess = '[ORGANIZATION] Load Organization Success',
  LoadOrganizationFailure = '[ORGANIZATION] Load Organization Failure',
  LoadOrganizationByTitle = '[ORGANIZATION] Load Organization By Title',
  LoadOrganizationByTitleSuccess = '[ORGANIZATION] Load Organization By Title Success',
  LoadOrganizationByTitleFailure = '[ORGANIZATION] Load Organization By Title Failure',
  ClearOrganization = '[ORGANIZATION] Clear Organization',
}

// Load All
export const loadAllOrganization = createAction(
  OrganizationActionTypes.LoadAllOrganization,
  props<{ params: any; pagination: boolean; infinite: boolean }>(),
);

export const loadAllOrganizationSuccess = createAction(
  OrganizationActionTypes.LoadAllOrganizationSuccess,
  props<{ data: OrganizationData[]; pagination: any; suggestion: any }>(),
);

export const loadAllOrganizationFailure = createAction(
  OrganizationActionTypes.LoadAllOrganizationFailure,
  props<{ error: Error | any }>(),
);

// Load Single
export const loadOrganization = createAction(
  OrganizationActionTypes.LoadOrganization,
  props<{ id: number; params: any }>(),
);

export const loadOrganizationSuccess = createAction(
  OrganizationActionTypes.LoadOrganizationSuccess,
  props<{ data: OrganizationData }>(),
);

export const loadOrganizationFailure = createAction(
  OrganizationActionTypes.LoadOrganizationFailure,
  props<{ error: Error | any }>(),
);

// Load Single By Title
export const loadOrganizationByTitle = createAction(
  OrganizationActionTypes.LoadOrganizationByTitle,
  props<{ title: string; params: any }>(),
);

export const loadOrganizationByTitleSuccess = createAction(
  OrganizationActionTypes.LoadOrganizationByTitleSuccess,
  props<{ data: OrganizationData }>(),
);

export const loadOrganizationByTitleFailure = createAction(
  OrganizationActionTypes.LoadOrganizationByTitleFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearOrganization = createAction(
  OrganizationActionTypes.ClearOrganization,
);

export const fromOrganizationActions = {
  loadAllOrganization,
  loadAllOrganizationSuccess,
  loadAllOrganizationFailure,
  loadOrganization,
  loadOrganizationSuccess,
  loadOrganizationFailure,
  loadOrganizationByTitle,
  loadOrganizationByTitleSuccess,
  loadOrganizationByTitleFailure,
  clearOrganization,
};
