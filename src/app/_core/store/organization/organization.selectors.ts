import { createFeatureSelector, createSelector } from '@ngrx/store';
import {
  State,
  adapter,
  ENTITY_FEATURE_KEY,
} from '@store-v3/organization/organization.reducers';

// Lookup feature state managed by NgRx
const getState = createFeatureSelector<State>(ENTITY_FEATURE_KEY);

// get the selectors
const { selectIds, selectAll, selectEntities } = adapter.getSelectors();

// select the array of ids
export const selectOrganizationIds = createSelector(getState, selectIds);

// select the array
export const selectAllOrganization = createSelector(getState, selectAll);

export const getOrganizationEntities = createSelector(
  getState,
  (state: State) => selectEntities(state),
);

// select by id
export const selectOrganization = (id: number | string) =>
  createSelector(getOrganizationEntities, (entities) => {
    return entities[id];
  });

// select by title
export const selectOrganizationByTitle = (title: number | string) =>
  createSelector(getOrganizationEntities, (entities) => {
    return entities[title];
  });

// select the by multiple id
export const selectMultipleOrganization = (ids: any[]) =>
  createSelector(getOrganizationEntities, (entities) => {
    const filtered = Object.keys(entities)
      .filter((key) => ids.includes(key))
      .reduce((obj, key) => {
        // eslint-disable-next-line no-param-reassign
        obj[key] = entities[key];
        return obj;
      }, {});

    return Object.keys(filtered).map((key) => filtered[key]);
  });

// select the by multiple by kode_skpd
export const selectMultipleOrganizationByCode = (ids: any[]) =>
  createSelector(getOrganizationEntities, (entities) => {
    const convert = Object.keys(entities).map((key) => entities[key]);
    return convert.filter((i) => ids.includes(i.kode_skpd));
  });

// select loaded flag
export const selectIsLoadingList = createSelector(
  getState,
  (state) => state.isLoadingList,
);
export const selectIsLoadingCreate = createSelector(
  getState,
  (state) => state.isLoadingCreate,
);
export const selectIsLoadingRead = createSelector(
  getState,
  (state) => state.isLoadingRead,
);
export const selectIsLoadingUpdate = createSelector(
  getState,
  (state) => state.isLoadingUpdate,
);
export const selectIsLoadingDelete = createSelector(
  getState,
  (state) => state.isLoadingDelete,
);

// select metadata
export const selectMetadata = createSelector(
  getState,
  (state) => state.metadata,
);

// select pagination
export const selectPagination = createSelector(
  getState,
  (state) => state.pagination,
);

// select suggestion
export const selectSuggestion = createSelector(
  getState,
  (state) => state.suggestion,
);

// select error
export const selectError = createSelector(getState, (state) => state.error);
