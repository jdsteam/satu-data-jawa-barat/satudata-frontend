import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { OrganizationData } from '@models-v3';
import { fromOrganizationActions } from '@store-v3/organization/organization.actions';

export const ENTITY_FEATURE_KEY = 'organizationV3';

export interface State extends EntityState<OrganizationData> {
  isLoadingList: boolean;
  isLoadingCreate: boolean;
  isLoadingRead: boolean;
  isLoadingUpdate: boolean;
  isLoadingDelete: boolean;
  metadata: any;
  suggestion: any;
  pagination: any;
  error?: Error | any;
}

export const adapter: EntityAdapter<OrganizationData> =
  createEntityAdapter<OrganizationData>({
    selectId: (item) => item.id,
    // selectId: (item) => String(item.title),
  });

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  isLoadingList: false,
  isLoadingCreate: false,
  isLoadingRead: false,
  isLoadingUpdate: false,
  isLoadingDelete: false,
  metadata: null,
  suggestion: null,
  pagination: null,
  error: null,
});

const reducer = createReducer(
  initialState,

  // Load All
  on(fromOrganizationActions.loadAllOrganization, (state) => {
    return {
      ...state,
      isLoadingList: true,
      error: null,
    };
  }),
  on(
    fromOrganizationActions.loadAllOrganizationSuccess,
    (state, { data, pagination, suggestion }) => {
      if (!pagination.infinite) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          suggestion,
          error: { error: false },
        });
      }

      if (pagination.current_page === 1) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          suggestion,
          error: { error: false },
        });
      }

      return adapter.addMany(data, {
        ...state,
        isLoadingList: false,
        pagination,
        suggestion,
        error: { error: false },
      });
    },
  ),
  on(fromOrganizationActions.loadAllOrganizationFailure, (state, { error }) => {
    return {
      ...state,
      isLoadingList: false,
      pagination: { empty: true },
      error,
    };
  }),

  // Load Single & Load Single By Title
  on(
    fromOrganizationActions.loadOrganization,
    fromOrganizationActions.loadOrganizationByTitle,
    (state) => {
      return {
        ...state,
        isLoadingRead: true,
        pagination: { empty: true },
        error: null,
      };
    },
  ),
  on(
    fromOrganizationActions.loadOrganizationSuccess,
    fromOrganizationActions.loadOrganizationByTitleSuccess,
    (state, { data }) => {
      return adapter.addOne(data, {
        ...state,
        isLoadingRead: false,
        pagination: { empty: true },
        error: { error: false },
      });
    },
  ),
  on(
    fromOrganizationActions.loadOrganizationFailure,
    fromOrganizationActions.loadOrganizationByTitleFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingRead: false,
        pagination: { empty: true },
        error,
      };
    },
  ),

  // Clear
  on(fromOrganizationActions.clearOrganization, (state) => {
    return adapter.removeAll({
      ...state,
      isLoadingList: false,
      pagination: { empty: true },
      suggestion: {},
      error: { error: false },
    });
  }),
);

export function OrganizationReducer(
  state: State | undefined,
  action: Action,
): any {
  return reducer(state, action);
}
