import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';

import { fromClassificationDatasetActions } from '@store-v3/classification-dataset/classification-dataset.actions';
import { ClassificationDatasetService, PaginationService } from '@services-v3';
import { handlePromise } from '@helpers-v3';

@Injectable()
export class ClassificationDatasetEffects {
  name = 'Classification Dataset';

  loadAllClassificationDataset$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromClassificationDatasetActions.loadAllClassificationDataset),
      switchMap(async (action) => {
        const { data: result, error: resultErr } = await handlePromise(
          this.classificationDatasetService.getList(action.params).toPromise(),
        );

        if (resultErr) {
          return this.loadAllFailure(resultErr.message);
        }

        if (result.error === 1) {
          if (result.message === 'Data not found') {
            return fromClassificationDatasetActions.clearClassificationDataset();
          }

          return this.loadAllFailure(result.message);
        }

        let pagination = { empty: true, infinite: action.infinite };
        if (action.pagination) {
          const resultPagination = this.paginationService.getPagination(
            result.meta.total_record,
            action.params.skip,
            action.params.limit,
          );

          pagination = {
            empty: false,
            infinite: action.infinite,
            ...resultPagination,
          };
        }

        return this.loadAllSuccess(result.data, pagination);
      }),
    ),
  );

  loadClassificationDataset$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromClassificationDatasetActions.loadClassificationDataset),
      switchMap((action) => {
        return this.classificationDatasetService
          .getSingle(action.id, action.params)
          .pipe(
            map((result: any) =>
              fromClassificationDatasetActions.loadClassificationDatasetSuccess(
                {
                  data: result.data,
                },
              ),
            ),
            catchError((error: HttpErrorResponse) =>
              of(
                fromClassificationDatasetActions.loadClassificationDatasetFailure(
                  {
                    error: {
                      name: this.name,
                      error: !error.ok,
                      message: error.message,
                    },
                  },
                ),
              ),
            ),
          );
      }),
    ),
  );

  loadClassificationDatasetByTitle$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromClassificationDatasetActions.loadClassificationDatasetByTitle),
      switchMap((action) => {
        return this.classificationDatasetService
          .getSingleByTitle(action.title, action.params)
          .pipe(
            map((result: any) =>
              fromClassificationDatasetActions.loadClassificationDatasetByTitleSuccess(
                {
                  data: result.data,
                },
              ),
            ),
            catchError((error: HttpErrorResponse) =>
              of(
                fromClassificationDatasetActions.loadClassificationDatasetByTitleFailure(
                  {
                    error: {
                      name: this.name,
                      error: !error.ok,
                      message: error.message,
                    },
                  },
                ),
              ),
            ),
          );
      }),
    ),
  );

  constructor(
    private actions$: Actions,
    private classificationDatasetService: ClassificationDatasetService,
    private paginationService: PaginationService,
  ) {}

  loadAllSuccess(data: any, pagination: any): any {
    return fromClassificationDatasetActions.loadAllClassificationDatasetSuccess(
      {
        data,
        pagination,
      },
    );
  }

  loadAllFailure(error: string): any {
    return fromClassificationDatasetActions.loadAllClassificationDatasetFailure(
      {
        error: {
          name: this.name,
          error: true,
          message: error,
        },
      },
    );
  }
}
