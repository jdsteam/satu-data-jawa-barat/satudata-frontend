import { createAction, props } from '@ngrx/store';
import { ClassificationDatasetData } from '@models-v3';

export enum ClassificationDatasetActionTypes {
  LoadAllClassificationDataset = '[CLASSIFICATION DATASET] Load All Classification Dataset',
  LoadAllClassificationDatasetSuccess = '[CLASSIFICATION DATASET] Load All Classification Dataset Success',
  LoadAllClassificationDatasetFailure = '[CLASSIFICATION DATASET] Load All Classification Dataset Failure',
  LoadClassificationDataset = '[CLASSIFICATION DATASET] Load Classification Dataset',
  LoadClassificationDatasetSuccess = '[CLASSIFICATION DATASET] Load Classification Dataset Success',
  LoadClassificationDatasetFailure = '[CLASSIFICATION DATASET] Load Classification Dataset Failure',
  LoadClassificationDatasetByTitle = '[CLASSIFICATION DATASET] Load Classification Dataset By Title',
  LoadClassificationDatasetByTitleSuccess = '[CLASSIFICATION DATASET] Load Classification Dataset By Title Success',
  LoadClassificationDatasetByTitleFailure = '[CLASSIFICATION DATASET] Load Classification Dataset By Title Failure',
  ClearClassificationDataset = '[CLASSIFICATION DATASET] Clear Classification Dataset',
}

// Load All
export const loadAllClassificationDataset = createAction(
  ClassificationDatasetActionTypes.LoadAllClassificationDataset,
  props<{ params: any; pagination: boolean; infinite: boolean }>(),
);

export const loadAllClassificationDatasetSuccess = createAction(
  ClassificationDatasetActionTypes.LoadAllClassificationDatasetSuccess,
  props<{ data: ClassificationDatasetData[]; pagination: any }>(),
);

export const loadAllClassificationDatasetFailure = createAction(
  ClassificationDatasetActionTypes.LoadAllClassificationDatasetFailure,
  props<{ error: Error | any }>(),
);

// Load Single
export const loadClassificationDataset = createAction(
  ClassificationDatasetActionTypes.LoadClassificationDataset,
  props<{ id: number; params: any }>(),
);

export const loadClassificationDatasetSuccess = createAction(
  ClassificationDatasetActionTypes.LoadClassificationDatasetSuccess,
  props<{ data: ClassificationDatasetData }>(),
);

export const loadClassificationDatasetFailure = createAction(
  ClassificationDatasetActionTypes.LoadClassificationDatasetFailure,
  props<{ error: Error | any }>(),
);

// Load Single By Title
export const loadClassificationDatasetByTitle = createAction(
  ClassificationDatasetActionTypes.LoadClassificationDatasetByTitle,
  props<{ title: string; params: any }>(),
);

export const loadClassificationDatasetByTitleSuccess = createAction(
  ClassificationDatasetActionTypes.LoadClassificationDatasetByTitleSuccess,
  props<{ data: ClassificationDatasetData }>(),
);

export const loadClassificationDatasetByTitleFailure = createAction(
  ClassificationDatasetActionTypes.LoadClassificationDatasetByTitleFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearClassificationDataset = createAction(
  ClassificationDatasetActionTypes.ClearClassificationDataset,
);

export const fromClassificationDatasetActions = {
  loadAllClassificationDataset,
  loadAllClassificationDatasetSuccess,
  loadAllClassificationDatasetFailure,
  loadClassificationDataset,
  loadClassificationDatasetSuccess,
  loadClassificationDatasetFailure,
  loadClassificationDatasetByTitle,
  loadClassificationDatasetByTitleSuccess,
  loadClassificationDatasetByTitleFailure,
  clearClassificationDataset,
};
