import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { ClassificationDatasetData } from '@models-v3';
import { fromClassificationDatasetActions } from '@store-v3/classification-dataset/classification-dataset.actions';

export const ENTITY_FEATURE_KEY = 'classificationDatasetV3';

export interface State extends EntityState<ClassificationDatasetData> {
  isLoadingList: boolean;
  isLoadingCreate: boolean;
  isLoadingRead: boolean;
  isLoadingUpdate: boolean;
  isLoadingDelete: boolean;
  metadata: any;
  pagination: any;
  error?: Error | any;
}

export const adapter: EntityAdapter<ClassificationDatasetData> =
  createEntityAdapter<ClassificationDatasetData>({
    selectId: (item) => item.id,
  });

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  isLoadingList: false,
  isLoadingCreate: false,
  isLoadingRead: false,
  isLoadingUpdate: false,
  isLoadingDelete: false,
  metadata: null,
  pagination: null,
  error: null,
});

const reducer = createReducer(
  initialState,

  // Load All
  on(fromClassificationDatasetActions.loadAllClassificationDataset, (state) => {
    return {
      ...state,
      isLoadingList: true,
      error: null,
    };
  }),
  on(
    fromClassificationDatasetActions.loadAllClassificationDatasetSuccess,
    (state, { data, pagination }) => {
      if (!pagination.infinite) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          error: { error: false },
        });
      }

      if (pagination.current_page === 1) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          error: { error: false },
        });
      }

      return adapter.addMany(data, {
        ...state,
        isLoadingList: false,
        pagination,
        error: { error: false },
      });
    },
  ),
  on(
    fromClassificationDatasetActions.loadAllClassificationDatasetFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingList: false,
        error,
      };
    },
  ),

  // Load Single & Load Single By Title
  on(
    fromClassificationDatasetActions.loadClassificationDataset,
    fromClassificationDatasetActions.loadClassificationDatasetByTitle,
    (state) => {
      return {
        ...state,
        isLoadingRead: true,
        error: null,
      };
    },
  ),
  on(
    fromClassificationDatasetActions.loadClassificationDatasetSuccess,
    fromClassificationDatasetActions.loadClassificationDatasetByTitleSuccess,
    (state, { data }) => {
      return adapter.addOne(data, {
        ...state,
        isLoadingRead: false,
        error: { error: false },
      });
    },
  ),
  on(
    fromClassificationDatasetActions.loadClassificationDatasetFailure,
    fromClassificationDatasetActions.loadClassificationDatasetByTitleFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingRead: false,
        error,
      };
    },
  ),

  // Clear
  on(fromClassificationDatasetActions.clearClassificationDataset, (state) => {
    return adapter.removeAll({
      ...state,
      isLoadingList: false,
      pagination: { empty: true },
      error: { error: false },
    });
  }),
);

export function ClassificationDatasetReducer(
  state: State | undefined,
  action: Action,
): any {
  return reducer(state, action);
}
