import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';

import { fromRequestDatasetPrivateActions } from '@store-v3/request-dataset-private/request-dataset-private.actions';
import { RequestDatasetPrivateService, PaginationService } from '@services-v3';
import { handlePromise } from '@helpers-v3';

@Injectable()
export class RequestDatasetPrivateEffects {
  name = 'Request Dataset Private';

  loadAllRequestDatasetPrivate$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromRequestDatasetPrivateActions.loadAllRequestDatasetPrivate),
      switchMap(async (action) => {
        const { data: result, error: resultErr } = await handlePromise(
          this.requestDatasetPrivateService.getList(action.params).toPromise(),
        );

        if (resultErr) {
          return this.loadAllFailure(resultErr.message);
        }

        if (result.error === 1) {
          if (result.message === 'Data not found') {
            return fromRequestDatasetPrivateActions.clearRequestDatasetPrivate();
          }

          return this.loadAllFailure(result.message);
        }

        let pagination = { empty: true };
        if (action.pagination) {
          const { data: total, error: totalErr } = await handlePromise(
            this.requestDatasetPrivateService
              .getTotal(action.params)
              .toPromise(),
          );

          if (totalErr) {
            return this.loadAllFailure(totalErr.message);
          }

          const resultPagination = this.paginationService.getPagination(
            total.data.count,
            action.params.skip,
            action.params.limit,
          );

          pagination = { empty: false, ...resultPagination };
        }

        return this.loadAllSuccess(result.data, pagination);
      }),
    ),
  );

  loadRequestDatasetPrivate$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromRequestDatasetPrivateActions.loadRequestDatasetPrivate),
      switchMap((action) => {
        return this.requestDatasetPrivateService
          .getSingle(action.id, action.params)
          .pipe(
            map((result: any) =>
              fromRequestDatasetPrivateActions.loadRequestDatasetPrivateSuccess(
                {
                  data: result.data,
                },
              ),
            ),
            catchError((error: HttpErrorResponse) =>
              of(
                fromRequestDatasetPrivateActions.loadRequestDatasetPrivateFailure(
                  {
                    error: {
                      name: this.name,
                      error: !error.ok,
                      message: error.message,
                    },
                  },
                ),
              ),
            ),
          );
      }),
    ),
  );

  createRequestDatasetPrivate$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromRequestDatasetPrivateActions.createRequestDatasetPrivate),
      switchMap((action) =>
        this.requestDatasetPrivateService.createItem(action.create).pipe(
          map((res: any) => {
            return fromRequestDatasetPrivateActions.createRequestDatasetPrivateSuccess(
              {
                data: res.data,
              },
            );
          }),
          catchError((error) => {
            return of(
              fromRequestDatasetPrivateActions.createRequestDatasetPrivateFailure(
                {
                  error: {
                    name: this.name,
                    error: true,
                    message: error.message,
                  },
                },
              ),
            );
          }),
        ),
      ),
    ),
  );

  updateRequestDatasetPrivate$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromRequestDatasetPrivateActions.updateRequestDatasetPrivate),
      switchMap((action) =>
        this.requestDatasetPrivateService
          .updateItem(action.update.id, action.update)
          .pipe(
            map((res: any) => {
              return fromRequestDatasetPrivateActions.updateRequestDatasetPrivateSuccess(
                {
                  data: res.data,
                },
              );
            }),
            catchError((error) => {
              return of(
                fromRequestDatasetPrivateActions.updateRequestDatasetPrivateFailure(
                  {
                    error: {
                      name: this.name,
                      error: true,
                      message: error.message,
                    },
                  },
                ),
              );
            }),
          ),
      ),
    ),
  );

  constructor(
    private actions$: Actions,
    private requestDatasetPrivateService: RequestDatasetPrivateService,
    private paginationService: PaginationService,
  ) {}

  loadAllSuccess(data: any, pagination: any): any {
    return fromRequestDatasetPrivateActions.loadAllRequestDatasetPrivateSuccess(
      {
        data,
        pagination,
      },
    );
  }

  loadAllFailure(error: string): any {
    return fromRequestDatasetPrivateActions.loadAllRequestDatasetPrivateFailure(
      {
        error: {
          name: this.name,
          error: true,
          message: error,
        },
      },
    );
  }
}
