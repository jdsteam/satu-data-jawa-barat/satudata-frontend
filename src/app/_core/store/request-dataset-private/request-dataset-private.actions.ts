import { createAction, props } from '@ngrx/store';
import { RequestDatasetPrivateData } from '@models-v3';

export enum RequestDatasetPrivateActionTypes {
  LoadAllRequestDatasetPrivate = '[REQUEST DATASET PRIVATE] Load All Request Dataset Private',
  LoadAllRequestDatasetPrivateSuccess = '[REQUEST DATASET PRIVATE] Load All Request Dataset Private Success',
  LoadAllRequestDatasetPrivateFailure = '[REQUEST DATASET PRIVATE] Load All Request Dataset Private Failure',
  LoadRequestDatasetPrivate = '[REQUEST DATASET PRIVATE] Load Request Dataset Private',
  LoadRequestDatasetPrivateSuccess = '[REQUEST DATASET PRIVATE] Load Request Dataset Private Success',
  LoadRequestDatasetPrivateFailure = '[REQUEST DATASET PRIVATE] Load Request Dataset Private Failure',
  CreateRequestDatasetPrivate = '[REQUEST DATASET PRIVATE] Create Request Dataset Private',
  CreateRequestDatasetPrivateSuccess = '[REQUEST DATASET PRIVATE] Create Request Dataset Private Success',
  CreateRequestDatasetPrivateFailure = '[REQUEST DATASET PRIVATE] Create Request Dataset Private Failure',
  UpdateRequestDatasetPrivate = '[REQUEST DATASET PRIVATE] Update Request Dataset Private',
  UpdateRequestDatasetPrivateSuccess = '[REQUEST DATASET PRIVATE] Update Request Dataset Private Success',
  UpdateRequestDatasetPrivateFailure = '[REQUEST DATASET PRIVATE] Update Request Dataset Private Failure',
  ClearRequestDatasetPrivate = '[REQUEST DATASET PRIVATE] Clear Request Dataset Private',
}

// Load
export const loadAllRequestDatasetPrivate = createAction(
  RequestDatasetPrivateActionTypes.LoadAllRequestDatasetPrivate,
  props<{ params: any; pagination: boolean; infinite: boolean }>(),
);

export const loadAllRequestDatasetPrivateSuccess = createAction(
  RequestDatasetPrivateActionTypes.LoadAllRequestDatasetPrivateSuccess,
  props<{ data: RequestDatasetPrivateData[]; pagination: any }>(),
);

export const loadAllRequestDatasetPrivateFailure = createAction(
  RequestDatasetPrivateActionTypes.LoadAllRequestDatasetPrivateFailure,
  props<{ error: Error | any }>(),
);

// Load Single
export const loadRequestDatasetPrivate = createAction(
  RequestDatasetPrivateActionTypes.LoadRequestDatasetPrivate,
  props<{ id: number; params: any }>(),
);

export const loadRequestDatasetPrivateSuccess = createAction(
  RequestDatasetPrivateActionTypes.LoadRequestDatasetPrivateSuccess,
  props<{ data: RequestDatasetPrivateData }>(),
);

export const loadRequestDatasetPrivateFailure = createAction(
  RequestDatasetPrivateActionTypes.LoadRequestDatasetPrivateFailure,
  props<{ error: Error | any }>(),
);

// Create
export const createRequestDatasetPrivate = createAction(
  RequestDatasetPrivateActionTypes.CreateRequestDatasetPrivate,
  props<{ create: any }>(),
);

export const createRequestDatasetPrivateSuccess = createAction(
  RequestDatasetPrivateActionTypes.CreateRequestDatasetPrivateSuccess,
  props<{ data: RequestDatasetPrivateData }>(),
);

export const createRequestDatasetPrivateFailure = createAction(
  RequestDatasetPrivateActionTypes.CreateRequestDatasetPrivateFailure,
  props<{ error: Error | any }>(),
);

// Update
export const updateRequestDatasetPrivate = createAction(
  RequestDatasetPrivateActionTypes.UpdateRequestDatasetPrivate,
  props<{ update: any }>(),
);

export const updateRequestDatasetPrivateSuccess = createAction(
  RequestDatasetPrivateActionTypes.UpdateRequestDatasetPrivateSuccess,
  props<{ data: RequestDatasetPrivateData }>(),
);

export const updateRequestDatasetPrivateFailure = createAction(
  RequestDatasetPrivateActionTypes.UpdateRequestDatasetPrivateFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearRequestDatasetPrivate = createAction(
  RequestDatasetPrivateActionTypes.ClearRequestDatasetPrivate,
);

export const fromRequestDatasetPrivateActions = {
  loadAllRequestDatasetPrivate,
  loadAllRequestDatasetPrivateSuccess,
  loadAllRequestDatasetPrivateFailure,
  loadRequestDatasetPrivate,
  loadRequestDatasetPrivateSuccess,
  loadRequestDatasetPrivateFailure,
  createRequestDatasetPrivate,
  createRequestDatasetPrivateSuccess,
  createRequestDatasetPrivateFailure,
  updateRequestDatasetPrivate,
  updateRequestDatasetPrivateSuccess,
  updateRequestDatasetPrivateFailure,
  clearRequestDatasetPrivate,
};
