import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { RequestDatasetPrivateData } from '@models-v3';
import { fromRequestDatasetPrivateActions } from '@store-v3/request-dataset-private/request-dataset-private.actions';

export const ENTITY_FEATURE_KEY = 'requestDatasetPrivateV3';

export interface State extends EntityState<RequestDatasetPrivateData> {
  isLoadingList: boolean;
  isLoadingCreate: boolean;
  isLoadingRead: boolean;
  isLoadingUpdate: boolean;
  isLoadingDelete: boolean;
  isLoadingCounter: boolean;
  metadata: any;
  pagination: any;
  error?: Error | any;
}

export const adapter: EntityAdapter<RequestDatasetPrivateData> =
  createEntityAdapter<RequestDatasetPrivateData>({
    selectId: (item) => item.id,
    // selectId: (item) => String(item.title),
  });

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  isLoadingList: false,
  isLoadingCreate: false,
  isLoadingRead: false,
  isLoadingUpdate: false,
  isLoadingDelete: false,
  isLoadingCounter: false,
  metadata: null,
  pagination: null,
  error: null,
});

const reducer = createReducer(
  initialState,

  // Load All
  on(fromRequestDatasetPrivateActions.loadAllRequestDatasetPrivate, (state) => {
    return {
      ...state,
      isLoadingList: true,
      error: null,
    };
  }),
  on(
    fromRequestDatasetPrivateActions.loadAllRequestDatasetPrivateSuccess,
    (state, { data, pagination }) => {
      if (!pagination.infinite) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          error: { error: false },
        });
      }

      if (pagination.current_page === 1) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          error: { error: false },
        });
      }

      return adapter.addMany(data, {
        ...state,
        isLoadingList: false,
        pagination,
        error: { error: false },
      });
    },
  ),
  on(
    fromRequestDatasetPrivateActions.loadAllRequestDatasetPrivateFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingList: false,
        error,
      };
    },
  ),

  // Load Single
  on(fromRequestDatasetPrivateActions.loadRequestDatasetPrivate, (state) => {
    return {
      ...state,
      isLoadingRead: true,
      error: null,
    };
  }),
  on(
    fromRequestDatasetPrivateActions.loadRequestDatasetPrivateSuccess,
    (state, { data }) => {
      return adapter.addOne(data, {
        ...state,
        isLoadingRead: false,
        error: { error: false },
      });
    },
  ),
  on(
    fromRequestDatasetPrivateActions.loadRequestDatasetPrivateFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingRead: false,
        error,
      };
    },
  ),

  // Create
  on(fromRequestDatasetPrivateActions.createRequestDatasetPrivate, (state) => {
    return {
      ...state,
      isLoadingCreate: true,
      error: null,
    };
  }),
  on(
    fromRequestDatasetPrivateActions.createRequestDatasetPrivateSuccess,
    (state, { data }) => {
      return adapter.addOne(data, {
        ...state,
        isLoadingCreate: false,
        error: { error: false },
      });
    },
  ),
  on(
    fromRequestDatasetPrivateActions.createRequestDatasetPrivateFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingCreate: false,
        error,
      };
    },
  ),

  // Update
  on(fromRequestDatasetPrivateActions.updateRequestDatasetPrivate, (state) => {
    return {
      ...state,
      isLoadingUpdate: true,
      error: null,
    };
  }),
  on(
    fromRequestDatasetPrivateActions.updateRequestDatasetPrivateSuccess,
    (state, { data }) => {
      return adapter.updateOne(
        { id: data.id, changes: data },
        {
          ...state,
          isLoadingUpdate: false,
          error: { error: false },
        },
      );
    },
  ),
  on(
    fromRequestDatasetPrivateActions.updateRequestDatasetPrivateFailure,
    (state, { error }) => {
      return {
        ...state,
        error,
      };
    },
  ),

  // Clear
  on(fromRequestDatasetPrivateActions.clearRequestDatasetPrivate, (state) => {
    return adapter.removeAll({
      ...state,
      isLoadingList: false,
      pagination: { empty: true },
      error: { error: false },
    });
  }),
);

export function RequestDatasetPrivateReducer(
  state: State | undefined,
  action: Action,
): any {
  return reducer(state, action);
}
