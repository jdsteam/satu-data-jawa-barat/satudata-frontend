import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { fromHistoryRequestPublicActions } from '@store-v3/history-request-public/history-request-public.actions';
import { HistoryRequestPublicData } from '@models-v3';

export const ENTITY_FEATURE_KEY = 'historyRequestPublicV3';

export interface State extends EntityState<HistoryRequestPublicData> {
  total: number;
  isLoading: boolean;
  error?: Error | any;
}

export const adapter: EntityAdapter<HistoryRequestPublicData> =
  createEntityAdapter<HistoryRequestPublicData>({
    selectId: (item) => item.id,
  });

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  total: 0,
  isLoading: false,
  error: null,
});

const reducer = createReducer(
  initialState,
  on(fromHistoryRequestPublicActions.loadHistoryRequestPublics, (state) => {
    return {
      ...state,
      total: 0,
      isLoading: true,
    };
  }),
  on(
    fromHistoryRequestPublicActions.loadHistoryRequestPublicsSuccess,
    (state, { data, total }) => {
      return adapter.setAll(data, {
        ...state,
        total,
        isLoading: false,
      });
    },
  ),
  on(
    fromHistoryRequestPublicActions.loadHistoryRequestPublicsFailure,
    (state, { error }) => {
      return {
        ...state,
        error,
      };
    },
  ),
);

export function HistoryRequestPublicReducer(
  state: State | undefined,
  action: Action,
) {
  return reducer(state, action);
}
