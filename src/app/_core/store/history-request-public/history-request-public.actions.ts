import { createAction, props } from '@ngrx/store';
import { HistoryRequestPublicData } from '@models-v3';

export enum HistoryRequestActionTypes {
  LoadHistoryRequestPublics = '[HISTORY REQUEST PUBLIC] Load History Request Publics',
  LoadHistoryRequestPublicsSuccess = '[HISTORY REQUEST PUBLIC] Load History Request Publics Success',
  LoadHistoryRequestPublicsFailure = '[HISTORY REQUEST PUBLIC] Load History Request Publics Failure',
}

export const loadHistoryRequestPublics = createAction(
  HistoryRequestActionTypes.LoadHistoryRequestPublics,
  props<{ params: string; paramsTotal: string }>(),
);

export const loadHistoryRequestPublicsSuccess = createAction(
  HistoryRequestActionTypes.LoadHistoryRequestPublicsSuccess,
  props<{ data: HistoryRequestPublicData[]; total: number }>(),
);

export const loadHistoryRequestPublicsFailure = createAction(
  HistoryRequestActionTypes.LoadHistoryRequestPublicsFailure,
  props<{ error: Error | any }>(),
);

export const fromHistoryRequestPublicActions = {
  loadHistoryRequestPublics,
  loadHistoryRequestPublicsSuccess,
  loadHistoryRequestPublicsFailure,
};
