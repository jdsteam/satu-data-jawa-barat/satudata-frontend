import { createFeatureSelector, createSelector } from '@ngrx/store';
import {
  State,
  adapter,
  ENTITY_FEATURE_KEY,
} from '@views/_core/store/history-request-public/history-request-public.reducers';

// Lookup the 'HistoryRequest' feature state managed by NgRx
const getHistoryRequestPublicState =
  createFeatureSelector<State>(ENTITY_FEATURE_KEY);

// get the selectors
const { selectIds, selectAll, selectTotal } = adapter.getSelectors();

// select the array of HistoryRequest ids
export const selectHistoryRequestPublicIds = createSelector(
  getHistoryRequestPublicState,
  selectIds,
);

// select the array of HistoryRequests
export const selectAllHistoryRequestPublic = createSelector(
  getHistoryRequestPublicState,
  selectAll,
);

// select the array of HistoryRequestsTotal
export const selectAllHistoryRequestPublicTotal = createSelector(
  getHistoryRequestPublicState,
  (state) => state.total,
);

// select the total HistoryRequest count
export const selectHistoryRequestPublicCount = createSelector(
  getHistoryRequestPublicState,
  selectTotal,
);

// select HistoryRequest loaded flag
export const selectIsLoading = createSelector(
  getHistoryRequestPublicState,
  (state) => state.isLoading,
);

// select HistoryRequest error
export const selectError = createSelector(
  getHistoryRequestPublicState,
  (state) => state.error,
);
