import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, switchMap, catchError, mergeMap } from 'rxjs/operators';

import { fromHistoryRequestPublicActions } from '@store-v3/history-request-public/history-request-public.actions';
import { HistoryRequestPublicService } from '@services-v3';

@Injectable()
export class HistoryRequestPublicEffects {
  loadHistoryRequests$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromHistoryRequestPublicActions.loadHistoryRequestPublics),
      switchMap((action) =>
        this.historyRequestPublicService.getItemsTotal(action.paramsTotal).pipe(
          map((resTotal: any) => resTotal),
          mergeMap((resTotal) =>
            this.historyRequestPublicService.getItems(action.params).pipe(
              map((res: any) =>
                fromHistoryRequestPublicActions.loadHistoryRequestPublicsSuccess(
                  {
                    data: res.data,
                    total: resTotal.data.count,
                  },
                ),
              ),
              catchError((error) =>
                of(
                  fromHistoryRequestPublicActions.loadHistoryRequestPublicsFailure(
                    {
                      error,
                    },
                  ),
                ),
              ),
            ),
          ),
          catchError((error) =>
            of(
              fromHistoryRequestPublicActions.loadHistoryRequestPublicsFailure({
                error,
              }),
            ),
          ),
        ),
      ),
    ),
  );

  constructor(
    private actions$: Actions,
    private historyRequestPublicService: HistoryRequestPublicService,
  ) {}
}
