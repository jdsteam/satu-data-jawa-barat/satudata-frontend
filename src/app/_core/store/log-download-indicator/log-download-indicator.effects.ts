import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { switchMap } from 'rxjs/operators';

import { fromLogDownloadIndicatorActions } from '@store-v3/log-download-indicator/log-download-indicator.actions';
import { LogDownloadIndicatorService } from '@services-v3';
import { handlePromise } from '@helpers-v3';

@Injectable()
export class LogDownloadIndicatorEffects {
  name = 'Log Download Indicator';

  loadAllLogDownloadIndicator$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromLogDownloadIndicatorActions.loadAllLogDownloadIndicator),
      switchMap(async (action) => {
        const { data: result, error: resultErr } = await handlePromise(
          this.logDownloadIndicatorService.getList(action.params).toPromise(),
        );

        if (resultErr) {
          return this.loadAllFailure(resultErr.message);
        }

        if (result.error === 1) {
          if (result.message === 'Data not found') {
            return fromLogDownloadIndicatorActions.clearLogDownloadIndicator();
          }
          return this.loadAllFailure(result.message);
        }

        let pagination = { empty: true, infinite: action.infinite };
        if (action.pagination) {
          pagination = {
            empty: false,
            infinite: action.infinite,
            ...result.pagination,
          };
        }

        return this.loadAllSuccess(result.data, pagination);
      }),
    ),
  );

  constructor(
    private actions$: Actions,
    private logDownloadIndicatorService: LogDownloadIndicatorService,
  ) {}

  loadAllSuccess(data: any, pagination: any): any {
    return fromLogDownloadIndicatorActions.loadAllLogDownloadIndicatorSuccess({
      data,
      pagination,
    });
  }

  loadAllFailure(error: string): any {
    return fromLogDownloadIndicatorActions.loadAllLogDownloadIndicatorFailure({
      error: {
        name: this.name,
        error: true,
        message: error,
      },
    });
  }
}
