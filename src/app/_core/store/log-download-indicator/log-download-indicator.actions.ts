import { createAction, props } from '@ngrx/store';
import { LogDownloadIndicatorData } from '@models-v3';

export enum LogDownloadIndicatorActionTypes {
  LoadAllLogDownloadIndicator = '[LOG DOWNLOAD INDICATOR] Load All Log Download Indicator',
  LoadAllLogDownloadIndicatorSuccess = '[LOG DOWNLOAD INDICATOR] Load All Log Download Indicator Success',
  LoadAllLogDownloadIndicatorFailure = '[LOG DOWNLOAD INDICATOR] Load All Log Download Indicator Failure',
  ClearLogDownloadIndicator = '[LOG DOWNLOAD INDICATOR] Clear Log Download Indicator',
}

// Load All
export const loadAllLogDownloadIndicator = createAction(
  LogDownloadIndicatorActionTypes.LoadAllLogDownloadIndicator,
  props<{ params: any; pagination: boolean; infinite: boolean }>(),
);

export const loadAllLogDownloadIndicatorSuccess = createAction(
  LogDownloadIndicatorActionTypes.LoadAllLogDownloadIndicatorSuccess,
  props<{ data: LogDownloadIndicatorData[]; pagination: any }>(),
);

export const loadAllLogDownloadIndicatorFailure = createAction(
  LogDownloadIndicatorActionTypes.LoadAllLogDownloadIndicatorFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearLogDownloadIndicator = createAction(
  LogDownloadIndicatorActionTypes.ClearLogDownloadIndicator,
);

export const fromLogDownloadIndicatorActions = {
  loadAllLogDownloadIndicator,
  loadAllLogDownloadIndicatorSuccess,
  loadAllLogDownloadIndicatorFailure,
  clearLogDownloadIndicator,
};
