import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { LogDownloadIndicatorData } from '@models-v3';
import { fromLogDownloadIndicatorActions } from '@store-v3/log-download-indicator/log-download-indicator.actions';

export const ENTITY_FEATURE_KEY = 'logDownloadIndicatorV3';

export interface State extends EntityState<LogDownloadIndicatorData> {
  isLoadingList: boolean;
  isLoadingCreate: boolean;
  isLoadingRead: boolean;
  isLoadingUpdate: boolean;
  isLoadingDelete: boolean;
  metadata: any;
  pagination: any;
  error?: Error | any;
}

export const adapter: EntityAdapter<LogDownloadIndicatorData> =
  createEntityAdapter<LogDownloadIndicatorData>({
    selectId: (item) => String(item.id),
  });

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  isLoadingList: false,
  isLoadingCreate: true,
  isLoadingRead: true,
  isLoadingUpdate: true,
  isLoadingDelete: true,
  metadata: null,
  pagination: null,
  error: null,
});

const reducer = createReducer(
  initialState,

  // Load All
  on(fromLogDownloadIndicatorActions.loadAllLogDownloadIndicator, (state) => {
    return {
      ...state,
      isLoadingList: true,
      error: null,
    };
  }),
  on(
    fromLogDownloadIndicatorActions.loadAllLogDownloadIndicatorSuccess,
    (state, { data, pagination }) => {
      if (!pagination.infinite) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          error: { error: false },
        });
      }

      if (pagination.current_page === 1) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          error: { error: false },
        });
      }

      return adapter.addMany(data, {
        ...state,
        isLoadingList: false,
        pagination,
        error: { error: false },
      });
    },
  ),
  on(
    fromLogDownloadIndicatorActions.loadAllLogDownloadIndicatorFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingList: false,
        error,
      };
    },
  ),

  // Clear
  on(fromLogDownloadIndicatorActions.clearLogDownloadIndicator, (state) => {
    return adapter.removeAll({
      ...state,
      isLoadingList: false,
      pagination: { empty: true },
      error: { error: false },
    });
  }),
);

export function LogDownloadIndicatorReducer(
  state: State | undefined,
  action: Action,
): any {
  return reducer(state, action);
}
