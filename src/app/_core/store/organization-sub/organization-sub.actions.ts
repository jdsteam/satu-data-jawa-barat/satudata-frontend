import { createAction, props } from '@ngrx/store';
import { OrganizationSubData } from '@models-v3';

export enum OrganizationSubActionTypes {
  LoadAllOrganizationSub = '[ORGANIZATION SUB] Load All Organization Sub',
  LoadAllOrganizationSubSuccess = '[ORGANIZATION SUB] Load All Organization Sub Success',
  LoadAllOrganizationSubFailure = '[ORGANIZATION SUB] Load All Organization Sub Failure',
  LoadOrganizationSub = '[ORGANIZATION SUB] Load Organization Sub',
  LoadOrganizationSubSuccess = '[ORGANIZATION SUB] Load Organization Sub Success',
  LoadOrganizationSubFailure = '[ORGANIZATION SUB] Load Organization Sub Failure',
  ClearOrganizationSub = '[ORGANIZATION SUB] Clear Organization Sub',
}

// Load All
export const loadAllOrganizationSub = createAction(
  OrganizationSubActionTypes.LoadAllOrganizationSub,
  props<{ params: any; pagination: boolean; infinite: boolean }>(),
);

export const loadAllOrganizationSubSuccess = createAction(
  OrganizationSubActionTypes.LoadAllOrganizationSubSuccess,
  props<{ data: OrganizationSubData[]; pagination: any }>(),
);

export const loadAllOrganizationSubFailure = createAction(
  OrganizationSubActionTypes.LoadAllOrganizationSubFailure,
  props<{ error: Error | any }>(),
);

// Load Single
export const loadOrganizationSub = createAction(
  OrganizationSubActionTypes.LoadOrganizationSub,
  props<{ id: number; params: any }>(),
);

export const loadOrganizationSubSuccess = createAction(
  OrganizationSubActionTypes.LoadOrganizationSubSuccess,
  props<{ data: OrganizationSubData }>(),
);

export const loadOrganizationSubFailure = createAction(
  OrganizationSubActionTypes.LoadOrganizationSubFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearOrganizationSub = createAction(
  OrganizationSubActionTypes.ClearOrganizationSub,
);

export const fromOrganizationSubActions = {
  loadAllOrganizationSub,
  loadAllOrganizationSubSuccess,
  loadAllOrganizationSubFailure,
  loadOrganizationSub,
  loadOrganizationSubSuccess,
  loadOrganizationSubFailure,
  clearOrganizationSub,
};
