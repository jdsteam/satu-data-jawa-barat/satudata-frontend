import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';

import { fromOrganizationSubActions } from '@store-v3/organization-sub/organization-sub.actions';
import { OrganizationSubService, PaginationService } from '@services-v3';
import { handlePromise } from '@helpers-v3';

@Injectable()
export class OrganizationSubEffects {
  name = 'Organization Sub';

  loadAllOrganizationSub$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromOrganizationSubActions.loadAllOrganizationSub),
      switchMap(async (action) => {
        const { data: result, error: resultErr } = await handlePromise(
          this.organizationSubService.getList(action.params).toPromise(),
        );

        if (resultErr) {
          return this.loadAllFailure(resultErr.message);
        }

        if (result.error === 1) {
          if (result.message === 'Data not found') {
            return fromOrganizationSubActions.clearOrganizationSub();
          }

          return this.loadAllFailure(result.message);
        }

        let pagination = { empty: true, infinite: action.infinite };
        if (action.pagination) {
          const resultPagination = this.paginationService.getPagination(
            result.meta.total_record,
            action.params.skip,
            action.params.limit,
          );

          pagination = {
            empty: false,
            infinite: action.infinite,
            ...resultPagination,
          };
        }

        return this.loadAllSuccess(result.data, pagination);
      }),
    ),
  );

  loadOrganizationSub$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromOrganizationSubActions.loadOrganizationSub),
      switchMap((action) => {
        return this.organizationSubService
          .getSingle(action.id, action.params)
          .pipe(
            map((result: any) =>
              fromOrganizationSubActions.loadOrganizationSubSuccess({
                data: result.data,
              }),
            ),
            catchError((error: HttpErrorResponse) =>
              of(
                fromOrganizationSubActions.loadOrganizationSubFailure({
                  error: {
                    name: this.name,
                    error: !error.ok,
                    message: error.message,
                  },
                }),
              ),
            ),
          );
      }),
    ),
  );

  constructor(
    private actions$: Actions,
    private organizationSubService: OrganizationSubService,
    private paginationService: PaginationService,
  ) {}

  loadAllSuccess(data: any, pagination: any): any {
    return fromOrganizationSubActions.loadAllOrganizationSubSuccess({
      data,
      pagination,
    });
  }

  loadAllFailure(error: string): any {
    return fromOrganizationSubActions.loadAllOrganizationSubFailure({
      error: {
        name: this.name,
        error: true,
        message: error,
      },
    });
  }
}
