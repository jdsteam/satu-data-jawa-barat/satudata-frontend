import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';

import { fromClassificationIndicatorActions } from '@store-v3/classification-indicator/classification-indicator.actions';
import {
  ClassificationIndicatorService,
  PaginationService,
} from '@services-v3';
import { handlePromise } from '@helpers-v3';

@Injectable()
export class ClassificationIndicatorEffects {
  name = 'Classification Indicator';

  loadAllClassificationIndicator$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromClassificationIndicatorActions.loadAllClassificationIndicator),
      switchMap(async (action) => {
        const { data: result, error: resultErr } = await handlePromise(
          this.classificationIndicatorService
            .getList(action.params)
            .toPromise(),
        );

        if (resultErr) {
          return this.loadAllFailure(resultErr.message);
        }

        if (result.error === 1) {
          if (result.message === 'Data not found') {
            return fromClassificationIndicatorActions.clearClassificationIndicator();
          }

          return this.loadAllFailure(result.message);
        }

        let pagination = { empty: true, infinite: action.infinite };
        if (action.pagination) {
          const resultPagination = this.paginationService.getPagination(
            result.meta.total_record,
            action.params.skip,
            action.params.limit,
          );

          pagination = {
            empty: false,
            infinite: action.infinite,
            ...resultPagination,
          };
        }

        return this.loadAllSuccess(result.data, pagination);
      }),
    ),
  );

  loadClassificationIndicator$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromClassificationIndicatorActions.loadClassificationIndicator),
      switchMap((action) => {
        return this.classificationIndicatorService
          .getSingle(action.id, action.params)
          .pipe(
            map((result: any) =>
              fromClassificationIndicatorActions.loadClassificationIndicatorSuccess(
                {
                  data: result.data,
                },
              ),
            ),
            catchError((error: HttpErrorResponse) =>
              of(
                fromClassificationIndicatorActions.loadClassificationIndicatorFailure(
                  {
                    error: {
                      name: this.name,
                      error: !error.ok,
                      message: error.message,
                    },
                  },
                ),
              ),
            ),
          );
      }),
    ),
  );

  loadClassificationIndicatorByTitle$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        fromClassificationIndicatorActions.loadClassificationIndicatorByTitle,
      ),
      switchMap((action) => {
        return this.classificationIndicatorService
          .getSingleByTitle(action.title, action.params)
          .pipe(
            map((result: any) =>
              fromClassificationIndicatorActions.loadClassificationIndicatorByTitleSuccess(
                {
                  data: result.data,
                },
              ),
            ),
            catchError((error: HttpErrorResponse) =>
              of(
                fromClassificationIndicatorActions.loadClassificationIndicatorByTitleFailure(
                  {
                    error: {
                      name: this.name,
                      error: !error.ok,
                      message: error.message,
                    },
                  },
                ),
              ),
            ),
          );
      }),
    ),
  );

  constructor(
    private actions$: Actions,
    private classificationIndicatorService: ClassificationIndicatorService,
    private paginationService: PaginationService,
  ) {}

  loadAllSuccess(data: any, pagination: any): any {
    return fromClassificationIndicatorActions.loadAllClassificationIndicatorSuccess(
      {
        data,
        pagination,
      },
    );
  }

  loadAllFailure(error: string): any {
    return fromClassificationIndicatorActions.loadAllClassificationIndicatorFailure(
      {
        error: {
          name: this.name,
          error: true,
          message: error,
        },
      },
    );
  }
}
