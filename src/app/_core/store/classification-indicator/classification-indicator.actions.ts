import { createAction, props } from '@ngrx/store';
import { ClassificationIndicatorData } from '@models-v3';

export enum ClassificationIndicatorActionTypes {
  LoadAllClassificationIndicator = '[CLASSIFICATION INDICATOR] Load All Classification Indicator',
  LoadAllClassificationIndicatorSuccess = '[CLASSIFICATION INDICATOR] Load All Classification Indicator Success',
  LoadAllClassificationIndicatorFailure = '[CLASSIFICATION INDICATOR] Load All Classification Indicator Failure',
  LoadClassificationIndicator = '[CLASSIFICATION INDICATOR] Load Classification Indicator',
  LoadClassificationIndicatorSuccess = '[CLASSIFICATION INDICATOR] Load Classification Indicator Success',
  LoadClassificationIndicatorFailure = '[CLASSIFICATION INDICATOR] Load Classification Indicator Failure',
  LoadClassificationIndicatorByTitle = '[CLASSIFICATION INDICATOR] Load Classification Indicator By Title',
  LoadClassificationIndicatorByTitleSuccess = '[CLASSIFICATION INDICATOR] Load Classification Indicator By Title Success',
  LoadClassificationIndicatorByTitleFailure = '[CLASSIFICATION INDICATOR] Load Classification Indicator By Title Failure',
  ClearClassificationIndicator = '[CLASSIFICATION INDICATOR] Clear Classification Indicator',
}

// Load All
export const loadAllClassificationIndicator = createAction(
  ClassificationIndicatorActionTypes.LoadAllClassificationIndicator,
  props<{ params: any; pagination: boolean; infinite: boolean }>(),
);

export const loadAllClassificationIndicatorSuccess = createAction(
  ClassificationIndicatorActionTypes.LoadAllClassificationIndicatorSuccess,
  props<{ data: ClassificationIndicatorData[]; pagination: any }>(),
);

export const loadAllClassificationIndicatorFailure = createAction(
  ClassificationIndicatorActionTypes.LoadAllClassificationIndicatorFailure,
  props<{ error: Error | any }>(),
);

// Load Single
export const loadClassificationIndicator = createAction(
  ClassificationIndicatorActionTypes.LoadClassificationIndicator,
  props<{ id: number; params: any }>(),
);

export const loadClassificationIndicatorSuccess = createAction(
  ClassificationIndicatorActionTypes.LoadClassificationIndicatorSuccess,
  props<{ data: ClassificationIndicatorData }>(),
);

export const loadClassificationIndicatorFailure = createAction(
  ClassificationIndicatorActionTypes.LoadClassificationIndicatorFailure,
  props<{ error: Error | any }>(),
);

// Load Single By Title
export const loadClassificationIndicatorByTitle = createAction(
  ClassificationIndicatorActionTypes.LoadClassificationIndicatorByTitle,
  props<{ title: string; params: any }>(),
);

export const loadClassificationIndicatorByTitleSuccess = createAction(
  ClassificationIndicatorActionTypes.LoadClassificationIndicatorByTitleSuccess,
  props<{ data: ClassificationIndicatorData }>(),
);

export const loadClassificationIndicatorByTitleFailure = createAction(
  ClassificationIndicatorActionTypes.LoadClassificationIndicatorByTitleFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearClassificationIndicator = createAction(
  ClassificationIndicatorActionTypes.ClearClassificationIndicator,
);

export const fromClassificationIndicatorActions = {
  loadAllClassificationIndicator,
  loadAllClassificationIndicatorSuccess,
  loadAllClassificationIndicatorFailure,
  loadClassificationIndicator,
  loadClassificationIndicatorSuccess,
  loadClassificationIndicatorFailure,
  loadClassificationIndicatorByTitle,
  loadClassificationIndicatorByTitleSuccess,
  loadClassificationIndicatorByTitleFailure,
  clearClassificationIndicator,
};
