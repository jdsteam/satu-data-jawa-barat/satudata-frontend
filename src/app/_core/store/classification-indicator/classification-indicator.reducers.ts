import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { ClassificationIndicatorData } from '@models-v3';
import { fromClassificationIndicatorActions } from '@store-v3/classification-indicator/classification-indicator.actions';

export const ENTITY_FEATURE_KEY = 'classificationIndicatorV3';

export interface State extends EntityState<ClassificationIndicatorData> {
  isLoadingList: boolean;
  isLoadingCreate: boolean;
  isLoadingRead: boolean;
  isLoadingUpdate: boolean;
  isLoadingDelete: boolean;
  metadata: any;
  pagination: any;
  error?: Error | any;
}

export const adapter: EntityAdapter<ClassificationIndicatorData> =
  createEntityAdapter<ClassificationIndicatorData>({
    selectId: (item) => item.id,
  });

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  isLoadingList: false,
  isLoadingCreate: false,
  isLoadingRead: false,
  isLoadingUpdate: false,
  isLoadingDelete: false,
  metadata: null,
  pagination: null,
  error: null,
});

const reducer = createReducer(
  initialState,

  // Load All
  on(
    fromClassificationIndicatorActions.loadAllClassificationIndicator,
    (state) => {
      return {
        ...state,
        isLoadingList: true,
      };
    },
  ),
  on(
    fromClassificationIndicatorActions.loadAllClassificationIndicatorSuccess,
    (state, { data, pagination }) => {
      if (!pagination.infinite) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          error: { error: false },
        });
      }

      if (pagination.current_page === 1) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          error: { error: false },
        });
      }

      return adapter.addMany(data, {
        ...state,
        isLoadingList: false,
        pagination,
        error: { error: false },
      });
    },
  ),
  on(
    fromClassificationIndicatorActions.loadAllClassificationIndicatorFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingList: false,
        error,
      };
    },
  ),

  // Load Single & Load Single By Title
  on(
    fromClassificationIndicatorActions.loadClassificationIndicator,
    fromClassificationIndicatorActions.loadClassificationIndicatorByTitle,
    (state) => {
      return {
        ...state,
        isLoadingRead: true,
      };
    },
  ),
  on(
    fromClassificationIndicatorActions.loadClassificationIndicatorSuccess,
    fromClassificationIndicatorActions.loadClassificationIndicatorByTitleSuccess,
    (state, { data }) => {
      return adapter.addOne(data, {
        ...state,
        isLoadingRead: false,
        error: { error: false },
      });
    },
  ),
  on(
    fromClassificationIndicatorActions.loadClassificationIndicatorFailure,
    fromClassificationIndicatorActions.loadClassificationIndicatorByTitleFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingRead: false,
        error,
      };
    },
  ),

  // Clear
  on(
    fromClassificationIndicatorActions.clearClassificationIndicator,
    (state) => {
      return adapter.removeAll({
        ...state,
        isLoadingList: false,
        pagination: { empty: true },
        error: { error: false },
      });
    },
  ),
);

export function ClassificationIndicatorReducer(
  state: State | undefined,
  action: Action,
): any {
  return reducer(state, action);
}
