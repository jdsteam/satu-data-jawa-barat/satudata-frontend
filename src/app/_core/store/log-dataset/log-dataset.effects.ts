import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { switchMap } from 'rxjs/operators';

import { fromLogDatasetActions } from '@store-v3/log-dataset/log-dataset.actions';
import { LogDatasetService } from '@services-v3';
import { handlePromise } from '@helpers-v3';

@Injectable()
export class LogDatasetEffects {
  name = 'Log Dataset';

  loadAllLogDataset$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromLogDatasetActions.loadAllLogDataset),
      switchMap(async (action) => {
        const { data: result, error: resultErr } = await handlePromise(
          this.logDatasetService.getList(action.params).toPromise(),
        );

        if (resultErr) {
          return this.loadAllFailure(resultErr.message);
        }

        if (result.error === 1) {
          if (result.message === 'Data not found') {
            return fromLogDatasetActions.clearLogDataset();
          }
          return this.loadAllFailure(result.message);
        }

        let pagination = { empty: true, infinite: action.infinite };
        if (action.pagination) {
          pagination = {
            empty: false,
            infinite: action.infinite,
            ...result.pagination,
          };
        }

        return this.loadAllSuccess(result.data, pagination);
      }),
    ),
  );

  constructor(
    private actions$: Actions,
    private logDatasetService: LogDatasetService,
  ) {}

  loadAllSuccess(data: any, pagination: any): any {
    return fromLogDatasetActions.loadAllLogDatasetSuccess({
      data,
      pagination,
    });
  }

  loadAllFailure(error: string): any {
    return fromLogDatasetActions.loadAllLogDatasetFailure({
      error: {
        name: this.name,
        error: true,
        message: error,
      },
    });
  }
}
