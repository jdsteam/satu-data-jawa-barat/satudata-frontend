import { createAction, props } from '@ngrx/store';
import { LogDatasetData } from '@models-v3';

export enum LogDatasetActionTypes {
  LoadAllLogDataset = '[LOG DATASET] Load All Log Dataset',
  LoadAllLogDatasetSuccess = '[LOG DATASET] Load All Log Dataset Success',
  LoadAllLogDatasetFailure = '[LOG DATASET] Load All Log Dataset Failure',
  ClearLogDataset = '[LOG DATASET] Clear Log Dataset',
}

// Load All
export const loadAllLogDataset = createAction(
  LogDatasetActionTypes.LoadAllLogDataset,
  props<{ params: any; pagination: boolean; infinite: boolean }>(),
);

export const loadAllLogDatasetSuccess = createAction(
  LogDatasetActionTypes.LoadAllLogDatasetSuccess,
  props<{ data: LogDatasetData[]; pagination: any }>(),
);

export const loadAllLogDatasetFailure = createAction(
  LogDatasetActionTypes.LoadAllLogDatasetFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearLogDataset = createAction(
  LogDatasetActionTypes.ClearLogDataset,
);

export const fromLogDatasetActions = {
  loadAllLogDataset,
  loadAllLogDatasetSuccess,
  loadAllLogDatasetFailure,
  clearLogDataset,
};
