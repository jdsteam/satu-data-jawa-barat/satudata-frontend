import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { switchMap } from 'rxjs/operators';

import { fromApplicationActions } from '@store-v3/application/application.actions';
import { ApplicationService, PaginationService } from '@services-v3';
import { handlePromise } from '@helpers-v3';

@Injectable()
export class ApplicationEffects {
  name = 'Application';

  loadAllApplication$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromApplicationActions.loadAllApplication),
      switchMap(async (action) => {
        const { data: result, error: resultErr } = await handlePromise(
          this.applicationService.getList(action.params).toPromise(),
        );

        if (resultErr) {
          return this.loadAllFailure(resultErr.message);
        }

        if (result.error === 1) {
          if (result.message === 'Data not found') {
            return fromApplicationActions.clearApplication();
          }

          return this.loadAllFailure(result.message);
        }

        let pagination = { empty: true };
        if (action.pagination) {
          const { data: total, error: totalErr } = await handlePromise(
            this.applicationService.getTotal(action.params).toPromise(),
          );

          if (totalErr) {
            return this.loadAllFailure(totalErr.message);
          }

          const resultPagination = this.paginationService.getPagination(
            total.data.count,
            action.params.skip,
            action.params.limit,
          );

          pagination = { empty: false, ...resultPagination };
        }

        return this.loadAllSuccess(result.data, pagination);
      }),
    ),
  );

  constructor(
    private actions$: Actions,
    private applicationService: ApplicationService,
    private paginationService: PaginationService,
  ) {}

  loadAllSuccess(data: any, pagination: any): any {
    return fromApplicationActions.loadAllApplicationSuccess({
      data,
      pagination,
    });
  }

  loadAllFailure(error: string): any {
    return fromApplicationActions.loadAllApplicationFailure({
      error: {
        name: this.name,
        error: true,
        message: error,
      },
    });
  }
}
