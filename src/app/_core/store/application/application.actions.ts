import { createAction, props } from '@ngrx/store';
import { ApplicationData } from '@models-v3';

export enum ApplicationActionTypes {
  LoadAllApplication = '[APPLICATION] Load All Application',
  LoadAllApplicationSuccess = '[APPLICATION] Load All Application Success',
  LoadAllApplicationFailure = '[APPLICATION] Load All Application Failure',
  ClearApplication = '[APPLICATION] Clear Application',
}

// Load All
export const loadAllApplication = createAction(
  ApplicationActionTypes.LoadAllApplication,
  props<{ params: any; pagination: boolean }>(),
);

export const loadAllApplicationSuccess = createAction(
  ApplicationActionTypes.LoadAllApplicationSuccess,
  props<{ data: ApplicationData[]; pagination: any }>(),
);

export const loadAllApplicationFailure = createAction(
  ApplicationActionTypes.LoadAllApplicationFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearApplication = createAction(
  ApplicationActionTypes.ClearApplication,
);

export const fromApplicationActions = {
  loadAllApplication,
  loadAllApplicationSuccess,
  loadAllApplicationFailure,
  clearApplication,
};
