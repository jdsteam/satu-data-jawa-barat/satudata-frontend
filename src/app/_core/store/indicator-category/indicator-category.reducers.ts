import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { IndicatorCategoryData } from '@models-v3';
import { fromIndicatorCategoryActions } from '@store-v3/indicator-category/indicator-category.actions';

export const ENTITY_FEATURE_KEY = 'indicatorCategoryV3';

export interface State extends EntityState<IndicatorCategoryData> {
  isLoadingList: boolean;
  isLoadingCreate: boolean;
  isLoadingRead: boolean;
  isLoadingUpdate: boolean;
  isLoadingDelete: boolean;
  metadata: any;
  pagination: any;
  error?: Error | any;
}

export const adapter: EntityAdapter<IndicatorCategoryData> =
  createEntityAdapter<IndicatorCategoryData>({
    selectId: (item) => item.id,
  });

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  isLoadingList: false,
  isLoadingCreate: false,
  isLoadingRead: false,
  isLoadingUpdate: false,
  isLoadingDelete: false,
  metadata: null,
  pagination: null,
  error: null,
});

const reducer = createReducer(
  initialState,

  // Load All
  on(fromIndicatorCategoryActions.loadAllIndicatorCategory, (state) => {
    return {
      ...state,
      isLoadingList: true,
    };
  }),
  on(
    fromIndicatorCategoryActions.loadAllIndicatorCategorySuccess,
    (state, { data, pagination }) => {
      if (!pagination.infinite) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          error: { error: false },
        });
      }

      if (pagination.current_page === 1) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          error: { error: false },
        });
      }

      return adapter.addMany(data, {
        ...state,
        isLoadingList: false,
        pagination,
        error: { error: false },
      });
    },
  ),
  on(
    fromIndicatorCategoryActions.loadAllIndicatorCategoryFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingList: false,
        error,
      };
    },
  ),

  // Load Single
  on(fromIndicatorCategoryActions.loadIndicatorCategory, (state) => {
    return {
      ...state,
      isLoadingRead: true,
    };
  }),
  on(
    fromIndicatorCategoryActions.loadIndicatorCategorySuccess,
    (state, { data }) => {
      return adapter.addOne(data, {
        ...state,
        isLoadingRead: false,
        error: { error: false },
      });
    },
  ),
  on(
    fromIndicatorCategoryActions.loadIndicatorCategoryFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingRead: false,
        error,
      };
    },
  ),

  // Clear
  on(fromIndicatorCategoryActions.clearIndicatorCategory, (state) => {
    return adapter.removeAll({
      ...state,
      isLoadingList: false,
      pagination: { empty: true },
      error: { error: false },
    });
  }),
);

export function IndicatorCategoryReducer(
  state: State | undefined,
  action: Action,
): any {
  return reducer(state, action);
}
