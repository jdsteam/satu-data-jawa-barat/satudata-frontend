import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';

import { fromIndicatorCategoryActions } from '@store-v3/indicator-category/indicator-category.actions';
import { IndicatorCategoryService, PaginationService } from '@services-v3';
import { handlePromise } from '@helpers-v3';

@Injectable()
export class IndicatorCategoryEffects {
  name = 'Indicator Category';

  loadAllIndicatorCategory$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromIndicatorCategoryActions.loadAllIndicatorCategory),
      switchMap(async (action) => {
        const { data: result, error: resultErr } = await handlePromise(
          this.indicatorCategoryService.getList(action.params).toPromise(),
        );

        if (resultErr) {
          return this.loadAllFailure(resultErr.message);
        }

        if (result.error === 1) {
          if (result.message === 'Data not found') {
            return fromIndicatorCategoryActions.clearIndicatorCategory();
          }

          return this.loadAllFailure(result.message);
        }

        let pagination = { empty: true, infinite: action.infinite };
        if (action.pagination) {
          const resultPagination = this.paginationService.getPagination(
            result.meta.total_record,
            action.params.skip,
            action.params.limit,
          );

          pagination = {
            empty: false,
            infinite: action.infinite,
            ...resultPagination,
          };
        }

        return this.loadAllSuccess(result.data, pagination);
      }),
    ),
  );

  loadIndicatorCategory$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromIndicatorCategoryActions.loadIndicatorCategory),
      switchMap((action) => {
        return this.indicatorCategoryService
          .getSingle(action.id, action.params)
          .pipe(
            map((result: any) =>
              fromIndicatorCategoryActions.loadIndicatorCategorySuccess({
                data: result.data,
              }),
            ),
            catchError((error: HttpErrorResponse) =>
              of(
                fromIndicatorCategoryActions.loadIndicatorCategoryFailure({
                  error: {
                    name: this.name,
                    error: !error.ok,
                    message: error.message,
                  },
                }),
              ),
            ),
          );
      }),
    ),
  );

  constructor(
    private actions$: Actions,
    private indicatorCategoryService: IndicatorCategoryService,
    private paginationService: PaginationService,
  ) {}

  loadAllSuccess(data: any, pagination: any): any {
    return fromIndicatorCategoryActions.loadAllIndicatorCategorySuccess({
      data,
      pagination,
    });
  }

  loadAllFailure(error: string): any {
    return fromIndicatorCategoryActions.loadAllIndicatorCategoryFailure({
      error: {
        name: this.name,
        error: true,
        message: error,
      },
    });
  }
}
