import { createAction, props } from '@ngrx/store';
import { IndicatorCategoryData } from '@models-v3';

export enum IndicatorCategoryActionTypes {
  LoadAllIndicatorCategory = '[INDICATOR CATEGORY] Load All Indicator Category',
  LoadAllIndicatorCategorySuccess = '[INDICATOR CATEGORY] Load All Indicator Category Success',
  LoadAllIndicatorCategoryFailure = '[INDICATOR CATEGORY] Load All Indicator Category Failure',
  LoadIndicatorCategory = '[INDICATOR CATEGORY] Load Indicator Category',
  LoadIndicatorCategorySuccess = '[INDICATOR CATEGORY] Load Indicator Category Success',
  LoadIndicatorCategoryFailure = '[INDICATOR CATEGORY] Load Indicator Category Failure',
  ClearIndicatorCategory = '[INDICATOR CATEGORY] Clear Indicator Category',
}

// Load All
export const loadAllIndicatorCategory = createAction(
  IndicatorCategoryActionTypes.LoadAllIndicatorCategory,
  props<{ params: any; pagination: boolean; infinite: boolean }>(),
);

export const loadAllIndicatorCategorySuccess = createAction(
  IndicatorCategoryActionTypes.LoadAllIndicatorCategorySuccess,
  props<{ data: IndicatorCategoryData[]; pagination: any }>(),
);

export const loadAllIndicatorCategoryFailure = createAction(
  IndicatorCategoryActionTypes.LoadAllIndicatorCategoryFailure,
  props<{ error: Error | any }>(),
);

// Load Single
export const loadIndicatorCategory = createAction(
  IndicatorCategoryActionTypes.LoadIndicatorCategory,
  props<{ id: number; params: any }>(),
);

export const loadIndicatorCategorySuccess = createAction(
  IndicatorCategoryActionTypes.LoadIndicatorCategorySuccess,
  props<{ data: IndicatorCategoryData }>(),
);

export const loadIndicatorCategoryFailure = createAction(
  IndicatorCategoryActionTypes.LoadIndicatorCategoryFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearIndicatorCategory = createAction(
  IndicatorCategoryActionTypes.ClearIndicatorCategory,
);

export const fromIndicatorCategoryActions = {
  loadAllIndicatorCategory,
  loadAllIndicatorCategorySuccess,
  loadAllIndicatorCategoryFailure,
  loadIndicatorCategory,
  loadIndicatorCategorySuccess,
  loadIndicatorCategoryFailure,
  clearIndicatorCategory,
};
