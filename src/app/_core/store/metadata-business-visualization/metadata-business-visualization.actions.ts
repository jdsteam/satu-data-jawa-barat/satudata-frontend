import { createAction, props } from '@ngrx/store';
import { MetadataVisualizationData } from '@models-v3';

export enum MetadataBusinessVisualizationActionTypes {
  LoadAllMetadataBusinessVisualization = '[METADATA BUSINESS VISUALIZATION] Load All Metadata Business Visualization',
  LoadAllMetadataBusinessVisualizationSuccess = '[METADATA BUSINESS VISUALIZATION] Load All Metadata Business Visualization Success',
  LoadAllMetadataBusinessVisualizationFailure = '[METADATA BUSINESS VISUALIZATION] Load All Metadata Business Visualization Failure',
  ClearMetadataBusinessVisualization = '[METADATA BUSINESS VISUALIZATION] Clear Metadata Business Visualization',
}

// Load All
export const loadAllMetadataBusinessVisualization = createAction(
  MetadataBusinessVisualizationActionTypes.LoadAllMetadataBusinessVisualization,
  props<{ params: any; pagination: boolean; infinite: boolean }>(),
);

export const loadAllMetadataBusinessVisualizationSuccess = createAction(
  MetadataBusinessVisualizationActionTypes.LoadAllMetadataBusinessVisualizationSuccess,
  props<{ data: MetadataVisualizationData[]; pagination: any }>(),
);

export const loadAllMetadataBusinessVisualizationFailure = createAction(
  MetadataBusinessVisualizationActionTypes.LoadAllMetadataBusinessVisualizationFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearMetadataBusinessVisualization = createAction(
  MetadataBusinessVisualizationActionTypes.ClearMetadataBusinessVisualization,
);

export const fromMetadataBusinessVisualizationActions = {
  loadAllMetadataBusinessVisualization,
  loadAllMetadataBusinessVisualizationSuccess,
  loadAllMetadataBusinessVisualizationFailure,
  clearMetadataBusinessVisualization,
};
