import { createAction, props } from '@ngrx/store';
import { MetadataDatasetData } from '@models-v3';

export enum MetadataCustomDatasetActionTypes {
  LoadAllMetadataCustomDataset = '[METADATA CUSTOM DATASET] Load All Metadata Custom Dataset',
  LoadAllMetadataCustomDatasetSuccess = '[METADATA CUSTOM DATASET] Load All Metadata Custom Dataset Success',
  LoadAllMetadataCustomDatasetFailure = '[METADATA CUSTOM DATASET] Load All Metadata Custom Dataset Failure',
  ClearMetadataCustomDataset = '[METADATA CUSTOM DATASET] Clear Metadata Custom Dataset',
}

// Load All
export const loadAllMetadataCustomDataset = createAction(
  MetadataCustomDatasetActionTypes.LoadAllMetadataCustomDataset,
  props<{ params: any; pagination: boolean; infinite: boolean }>(),
);

export const loadAllMetadataCustomDatasetSuccess = createAction(
  MetadataCustomDatasetActionTypes.LoadAllMetadataCustomDatasetSuccess,
  props<{ data: MetadataDatasetData[]; pagination: any }>(),
);

export const loadAllMetadataCustomDatasetFailure = createAction(
  MetadataCustomDatasetActionTypes.LoadAllMetadataCustomDatasetFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearMetadataCustomDataset = createAction(
  MetadataCustomDatasetActionTypes.ClearMetadataCustomDataset,
);

export const fromMetadataCustomDatasetActions = {
  loadAllMetadataCustomDataset,
  loadAllMetadataCustomDatasetSuccess,
  loadAllMetadataCustomDatasetFailure,
  clearMetadataCustomDataset,
};
