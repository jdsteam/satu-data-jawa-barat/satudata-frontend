import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { switchMap } from 'rxjs/operators';

import { fromMetadataCustomDatasetActions } from '@store-v3/metadata-custom-dataset/metadata-custom-dataset.actions';
import { MetadataDatasetService, PaginationService } from '@services-v3';
import { handlePromise } from '@helpers-v3';

@Injectable()
export class MetadataCustomDatasetEffects {
  name = 'Metadata Custom Dataset';

  loadAllMetadataCustomDataset$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromMetadataCustomDatasetActions.loadAllMetadataCustomDataset),
      switchMap(async (action) => {
        const { data: result, error: resultErr } = await handlePromise(
          this.metadataDatasetService.getList(action.params).toPromise(),
        );

        if (resultErr) {
          return this.loadAllFailure(resultErr.message);
        }

        if (result.error === 1) {
          if (result.message === 'Data not found') {
            return fromMetadataCustomDatasetActions.clearMetadataCustomDataset();
          }

          return this.loadAllFailure(result.message);
        }

        let pagination = { empty: true };
        if (action.pagination) {
          const { data: total, error: totalErr } = await handlePromise(
            this.metadataDatasetService.getTotal(action.params).toPromise(),
          );

          if (totalErr) {
            return this.loadAllFailure(totalErr.message);
          }

          const resultPagination = this.paginationService.getPagination(
            total.data.count,
            action.params.skip,
            action.params.limit,
          );

          pagination = { empty: false, ...resultPagination };
        }

        return this.loadAllSuccess(result.data, pagination);
      }),
    ),
  );

  constructor(
    private actions$: Actions,
    private metadataDatasetService: MetadataDatasetService,
    private paginationService: PaginationService,
  ) {}

  loadAllSuccess(data: any, pagination: any): any {
    return fromMetadataCustomDatasetActions.loadAllMetadataCustomDatasetSuccess(
      {
        data,
        pagination,
      },
    );
  }

  loadAllFailure(error: string): any {
    return fromMetadataCustomDatasetActions.loadAllMetadataCustomDatasetFailure(
      {
        error: {
          name: this.name,
          error: true,
          message: error,
        },
      },
    );
  }
}
