import { createAction, props } from '@ngrx/store';
import { OrganizationUnitData } from '@models-v3';

export enum OrganizationUnitActionTypes {
  LoadAllOrganizationUnit = '[ORGANIZATION UNIT] Load All Organization Unit',
  LoadAllOrganizationUnitSuccess = '[ORGANIZATION UNIT] Load All Organization Unit Success',
  LoadAllOrganizationUnitFailure = '[ORGANIZATION UNIT] Load All Organization Unit Failure',
  LoadOrganizationUnit = '[ORGANIZATION UNIT] Load Organization Unit',
  LoadOrganizationUnitSuccess = '[ORGANIZATION UNIT] Load Organization Unit Success',
  LoadOrganizationUnitFailure = '[ORGANIZATION UNIT] Load Organization Unit Failure',
  ClearOrganizationUnit = '[ORGANIZATION UNIT] Clear Organization Unit',
}

// Load All
export const loadAllOrganizationUnit = createAction(
  OrganizationUnitActionTypes.LoadAllOrganizationUnit,
  props<{ params: any; pagination: boolean; infinite: boolean }>(),
);

export const loadAllOrganizationUnitSuccess = createAction(
  OrganizationUnitActionTypes.LoadAllOrganizationUnitSuccess,
  props<{ data: OrganizationUnitData[]; pagination: any }>(),
);

export const loadAllOrganizationUnitFailure = createAction(
  OrganizationUnitActionTypes.LoadAllOrganizationUnitFailure,
  props<{ error: Error | any }>(),
);

// Load Single
export const loadOrganizationUnit = createAction(
  OrganizationUnitActionTypes.LoadOrganizationUnit,
  props<{ id: number; params: any }>(),
);

export const loadOrganizationUnitSuccess = createAction(
  OrganizationUnitActionTypes.LoadOrganizationUnitSuccess,
  props<{ data: OrganizationUnitData }>(),
);

export const loadOrganizationUnitFailure = createAction(
  OrganizationUnitActionTypes.LoadOrganizationUnitFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearOrganizationUnit = createAction(
  OrganizationUnitActionTypes.ClearOrganizationUnit,
);

export const fromOrganizationUnitActions = {
  loadAllOrganizationUnit,
  loadAllOrganizationUnitSuccess,
  loadAllOrganizationUnitFailure,
  loadOrganizationUnit,
  loadOrganizationUnitSuccess,
  loadOrganizationUnitFailure,
  clearOrganizationUnit,
};
