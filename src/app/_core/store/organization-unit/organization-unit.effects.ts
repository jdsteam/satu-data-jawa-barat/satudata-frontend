import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';

import { fromOrganizationUnitActions } from '@store-v3/organization-unit/organization-unit.actions';
import { OrganizationUnitService, PaginationService } from '@services-v3';
import { handlePromise } from '@helpers-v3';

@Injectable()
export class OrganizationUnitEffects {
  name = 'Organization Unit';

  loadAllOrganizationUnit$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromOrganizationUnitActions.loadAllOrganizationUnit),
      switchMap(async (action) => {
        const { data: result, error: resultErr } = await handlePromise(
          this.organizationUnitService.getList(action.params).toPromise(),
        );

        if (resultErr) {
          return this.loadAllFailure(resultErr.message);
        }

        if (result.error === 1) {
          if (result.message === 'Data not found') {
            return fromOrganizationUnitActions.clearOrganizationUnit();
          }

          return this.loadAllFailure(result.message);
        }

        let pagination = { empty: true, infinite: action.infinite };
        if (action.pagination) {
          const resultPagination = this.paginationService.getPagination(
            result.meta.total_record,
            action.params.skip,
            action.params.limit,
          );

          pagination = {
            empty: false,
            infinite: action.infinite,
            ...resultPagination,
          };
        }

        return this.loadAllSuccess(result.data, pagination);
      }),
    ),
  );

  loadOrganizationUnit$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromOrganizationUnitActions.loadOrganizationUnit),
      switchMap((action) => {
        return this.organizationUnitService
          .getSingle(action.id, action.params)
          .pipe(
            map((result: any) =>
              fromOrganizationUnitActions.loadOrganizationUnitSuccess({
                data: result.data,
              }),
            ),
            catchError((error: HttpErrorResponse) =>
              of(
                fromOrganizationUnitActions.loadOrganizationUnitFailure({
                  error: {
                    name: this.name,
                    error: !error.ok,
                    message: error.message,
                  },
                }),
              ),
            ),
          );
      }),
    ),
  );

  constructor(
    private actions$: Actions,
    private organizationUnitService: OrganizationUnitService,
    private paginationService: PaginationService,
  ) {}

  loadAllSuccess(data: any, pagination: any): any {
    return fromOrganizationUnitActions.loadAllOrganizationUnitSuccess({
      data,
      pagination,
    });
  }

  loadAllFailure(error: string): any {
    return fromOrganizationUnitActions.loadAllOrganizationUnitFailure({
      error: {
        name: this.name,
        error: true,
        message: error,
      },
    });
  }
}
