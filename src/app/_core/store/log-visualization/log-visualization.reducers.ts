import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { LogVisualizationData } from '@models-v3';
import { fromLogVisualizationActions } from '@store-v3/log-visualization/log-visualization.actions';

export const ENTITY_FEATURE_KEY = 'logVisualizationV3';

export interface State extends EntityState<LogVisualizationData> {
  isLoadingList: boolean;
  isLoadingCreate: boolean;
  isLoadingRead: boolean;
  isLoadingUpdate: boolean;
  isLoadingDelete: boolean;
  metadata: any;
  pagination: any;
  error?: Error | any;
}

export const adapter: EntityAdapter<LogVisualizationData> =
  createEntityAdapter<LogVisualizationData>({
    selectId: (item) => String(item.id),
  });

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  isLoadingList: false,
  isLoadingCreate: true,
  isLoadingRead: true,
  isLoadingUpdate: true,
  isLoadingDelete: true,
  metadata: null,
  pagination: null,
  error: null,
});

const reducer = createReducer(
  initialState,

  // Load All
  on(fromLogVisualizationActions.loadAllLogVisualization, (state) => {
    return {
      ...state,
      isLoadingList: true,
      error: null,
    };
  }),
  on(
    fromLogVisualizationActions.loadAllLogVisualizationSuccess,
    (state, { data, pagination }) => {
      if (!pagination.infinite) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          error: { error: false },
        });
      }

      if (pagination.current_page === 1) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          error: { error: false },
        });
      }

      return adapter.addMany(data, {
        ...state,
        isLoadingList: false,
        pagination,
        error: { error: false },
      });
    },
  ),
  on(
    fromLogVisualizationActions.loadAllLogVisualizationFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingList: false,
        error,
      };
    },
  ),

  // Clear
  on(fromLogVisualizationActions.clearLogVisualization, (state) => {
    return adapter.removeAll({
      ...state,
      isLoadingList: false,
      pagination: { empty: true },
      error: { error: false },
    });
  }),
);

export function LogVisualizationReducer(
  state: State | undefined,
  action: Action,
): any {
  return reducer(state, action);
}
