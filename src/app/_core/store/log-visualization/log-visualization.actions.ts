import { createAction, props } from '@ngrx/store';
import { LogVisualizationData } from '@models-v3';

export enum LogVisualizationActionTypes {
  LoadAllLogVisualization = '[LOG VISUALIZATION] Load All Log Visualization',
  LoadAllLogVisualizationSuccess = '[LOG VISUALIZATION] Load All Log Visualization Success',
  LoadAllLogVisualizationFailure = '[LOG VISUALIZATION] Load All Log Visualization Failure',
  ClearLogVisualization = '[LOG VISUALIZATION] Clear Log Visualization',
}

// Load All
export const loadAllLogVisualization = createAction(
  LogVisualizationActionTypes.LoadAllLogVisualization,
  props<{ params: any; pagination: boolean; infinite: boolean }>(),
);

export const loadAllLogVisualizationSuccess = createAction(
  LogVisualizationActionTypes.LoadAllLogVisualizationSuccess,
  props<{ data: LogVisualizationData[]; pagination: any }>(),
);

export const loadAllLogVisualizationFailure = createAction(
  LogVisualizationActionTypes.LoadAllLogVisualizationFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearLogVisualization = createAction(
  LogVisualizationActionTypes.ClearLogVisualization,
);

export const fromLogVisualizationActions = {
  loadAllLogVisualization,
  loadAllLogVisualizationSuccess,
  loadAllLogVisualizationFailure,
  clearLogVisualization,
};
