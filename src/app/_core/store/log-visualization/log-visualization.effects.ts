import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { switchMap } from 'rxjs/operators';

import { fromLogVisualizationActions } from '@store-v3/log-visualization/log-visualization.actions';
import { LogVisualizationService } from '@services-v3';
import { handlePromise } from '@helpers-v3';

@Injectable()
export class LogVisualizationEffects {
  name = 'Log Visualization';

  loadAllLogVisualization$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromLogVisualizationActions.loadAllLogVisualization),
      switchMap(async (action) => {
        const { data: result, error: resultErr } = await handlePromise(
          this.logVisualizationService.getList(action.params).toPromise(),
        );

        if (resultErr) {
          return this.loadAllFailure(resultErr.message);
        }

        if (result.error === 1) {
          if (result.message === 'Data not found') {
            return fromLogVisualizationActions.clearLogVisualization();
          }
          return this.loadAllFailure(result.message);
        }

        let pagination = { empty: true, infinite: action.infinite };
        if (action.pagination) {
          pagination = {
            empty: false,
            infinite: action.infinite,
            ...result.pagination,
          };
        }

        return this.loadAllSuccess(result.data, pagination);
      }),
    ),
  );

  constructor(
    private actions$: Actions,
    private logVisualizationService: LogVisualizationService,
  ) {}

  loadAllSuccess(data: any, pagination: any): any {
    return fromLogVisualizationActions.loadAllLogVisualizationSuccess({
      data,
      pagination,
    });
  }

  loadAllFailure(error: string): any {
    return fromLogVisualizationActions.loadAllLogVisualizationFailure({
      error: {
        name: this.name,
        error: true,
        message: error,
      },
    });
  }
}
