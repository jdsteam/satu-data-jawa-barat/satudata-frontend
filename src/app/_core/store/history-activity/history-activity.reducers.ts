import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { HistoryActivityData } from '@models-v3';
import { fromHistoryActivityActions } from '@store-v3/history-activity/history-activity.actions';

export const ENTITY_FEATURE_KEY = 'historyActivityV3';

export interface State extends EntityState<HistoryActivityData> {
  isLoadingList: boolean;
  isLoadingCreate: boolean;
  isLoadingRead: boolean;
  isLoadingUpdate: boolean;
  isLoadingDelete: boolean;
  metadata: any;
  pagination: any;
  error?: Error | any;
}

export const adapter: EntityAdapter<HistoryActivityData> =
  createEntityAdapter<HistoryActivityData>({
    selectId: (item) => item.id,
  });

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  isLoadingList: false,
  isLoadingCreate: true,
  isLoadingRead: true,
  isLoadingUpdate: true,
  isLoadingDelete: true,
  metadata: null,
  pagination: null,
  error: null,
});

const reducer = createReducer(
  initialState,

  // Load All
  on(fromHistoryActivityActions.loadAllHistoryActivity, (state) => {
    return {
      ...state,
      isLoadingList: true,
    };
  }),
  on(
    fromHistoryActivityActions.loadAllHistoryActivitySuccess,
    (state, { data, pagination }) => {
      return adapter.setAll(data, {
        ...state,
        isLoadingList: false,
        pagination,
        error: { error: false },
      });
    },
  ),
  on(
    fromHistoryActivityActions.loadAllHistoryActivityFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingList: false,
        error,
      };
    },
  ),

  // Clear
  on(fromHistoryActivityActions.clearHistoryActivity, (state) => {
    return adapter.removeAll({
      ...state,
      isLoadingList: false,
      pagination: { empty: true },
      error: { error: false },
    });
  }),
);

export function HistoryActivityReducer(
  state: State | undefined,
  action: Action,
): any {
  return reducer(state, action);
}
