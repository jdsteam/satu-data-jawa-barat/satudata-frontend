import { createAction, props } from '@ngrx/store';
import { HistoryActivityData } from '@models-v3';

export enum HistoryActivityActionTypes {
  LoadAllHistoryActivity = '[HISTORY ACTIVITY] Load All History Activity',
  LoadAllHistoryActivitySuccess = '[HISTORY ACTIVITY] Load All History Activity Success',
  LoadAllHistoryActivityFailure = '[HISTORY ACTIVITY] Load All History Activity Failure',
  ClearHistoryActivity = '[HISTORY ACTIVITY] Clear History Activity',
}

// Load All
export const loadAllHistoryActivity = createAction(
  HistoryActivityActionTypes.LoadAllHistoryActivity,
  props<{ params: any; pagination: boolean }>(),
);

export const loadAllHistoryActivitySuccess = createAction(
  HistoryActivityActionTypes.LoadAllHistoryActivitySuccess,
  props<{ data: HistoryActivityData[]; pagination: any }>(),
);

export const loadAllHistoryActivityFailure = createAction(
  HistoryActivityActionTypes.LoadAllHistoryActivityFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearHistoryActivity = createAction(
  HistoryActivityActionTypes.ClearHistoryActivity,
);

export const fromHistoryActivityActions = {
  loadAllHistoryActivity,
  loadAllHistoryActivitySuccess,
  loadAllHistoryActivityFailure,
  clearHistoryActivity,
};
