import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { switchMap } from 'rxjs/operators';

import { fromHistoryActivityActions } from '@store-v3/history-activity/history-activity.actions';
import { HistoryActivityService, PaginationService } from '@services-v3';
import { handlePromise } from '@helpers-v3';

@Injectable()
export class HistoryActivityEffects {
  name = 'History Activity';

  loadAllHistoryActivity$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromHistoryActivityActions.loadAllHistoryActivity),
      switchMap(async (action) => {
        const { data: result, error: resultErr } = await handlePromise(
          this.historyActivityService.getList(action.params).toPromise(),
        );

        if (resultErr) {
          return this.loadAllFailure(resultErr.message);
        }

        if (result.error === 1) {
          if (result.message === 'Data not found') {
            return fromHistoryActivityActions.clearHistoryActivity();
          }

          return this.loadAllFailure(result.message);
        }

        let pagination = { empty: true };
        if (action.pagination) {
          const { data: total, error: totalErr } = await handlePromise(
            this.historyActivityService.getTotal(action.params).toPromise(),
          );

          if (totalErr) {
            return this.loadAllFailure(totalErr.message);
          }

          const resultPagination = this.paginationService.getPagination(
            total.data.count,
            action.params.skip,
            action.params.limit,
          );

          pagination = { empty: false, ...resultPagination };
        }

        return this.loadAllSuccess(result.data, pagination);
      }),
    ),
  );

  constructor(
    private actions$: Actions,
    private historyActivityService: HistoryActivityService,
    private paginationService: PaginationService,
  ) {}

  loadAllSuccess(data: any, pagination: any): any {
    return fromHistoryActivityActions.loadAllHistoryActivitySuccess({
      data,
      pagination,
    });
  }

  loadAllFailure(error: string): any {
    return fromHistoryActivityActions.loadAllHistoryActivityFailure({
      error: {
        name: this.name,
        error: true,
        message: error,
      },
    });
  }
}
