import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { switchMap } from 'rxjs/operators';

import { fromLogDownloadVisualizationActions } from '@store-v3/log-download-visualization/log-download-visualization.actions';
import { LogDownloadVisualizationService } from '@services-v3';
import { handlePromise } from '@helpers-v3';

@Injectable()
export class LogDownloadVisualizationEffects {
  name = 'Log Download Visualization';

  loadAllLogDownloadVisualization$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        fromLogDownloadVisualizationActions.loadAllLogDownloadVisualization,
      ),
      switchMap(async (action) => {
        const { data: result, error: resultErr } = await handlePromise(
          this.logDownloadVisualizationService
            .getList(action.params)
            .toPromise(),
        );

        if (resultErr) {
          return this.loadAllFailure(resultErr.message);
        }

        if (result.error === 1) {
          if (result.message === 'Data not found') {
            return fromLogDownloadVisualizationActions.clearLogDownloadVisualization();
          }
          return this.loadAllFailure(result.message);
        }

        let pagination = { empty: true, infinite: action.infinite };
        if (action.pagination) {
          pagination = {
            empty: false,
            infinite: action.infinite,
            ...result.pagination,
          };
        }

        return this.loadAllSuccess(result.data, pagination);
      }),
    ),
  );

  constructor(
    private actions$: Actions,
    private logDownloadVisualizationService: LogDownloadVisualizationService,
  ) {}

  loadAllSuccess(data: any, pagination: any): any {
    return fromLogDownloadVisualizationActions.loadAllLogDownloadVisualizationSuccess(
      {
        data,
        pagination,
      },
    );
  }

  loadAllFailure(error: string): any {
    return fromLogDownloadVisualizationActions.loadAllLogDownloadVisualizationFailure(
      {
        error: {
          name: this.name,
          error: true,
          message: error,
        },
      },
    );
  }
}
