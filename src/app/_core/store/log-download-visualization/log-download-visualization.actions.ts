import { createAction, props } from '@ngrx/store';
import { LogDownloadVisualizationData } from '@models-v3';

export enum LogDownloadVisualizationActionTypes {
  LoadAllLogDownloadVisualization = '[LOG DOWNLOAD VISUALIZATION] Load All Log Download Visualization',
  LoadAllLogDownloadVisualizationSuccess = '[LOG DOWNLOAD VISUALIZATION] Load All Log Download Visualization Success',
  LoadAllLogDownloadVisualizationFailure = '[LOG DOWNLOAD VISUALIZATION] Load All Log Download Visualization Failure',
  ClearLogDownloadVisualization = '[LOG DOWNLOAD VISUALIZATION] Clear Log Download Visualization',
}

// Load All
export const loadAllLogDownloadVisualization = createAction(
  LogDownloadVisualizationActionTypes.LoadAllLogDownloadVisualization,
  props<{ params: any; pagination: boolean; infinite: boolean }>(),
);

export const loadAllLogDownloadVisualizationSuccess = createAction(
  LogDownloadVisualizationActionTypes.LoadAllLogDownloadVisualizationSuccess,
  props<{ data: LogDownloadVisualizationData[]; pagination: any }>(),
);

export const loadAllLogDownloadVisualizationFailure = createAction(
  LogDownloadVisualizationActionTypes.LoadAllLogDownloadVisualizationFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearLogDownloadVisualization = createAction(
  LogDownloadVisualizationActionTypes.ClearLogDownloadVisualization,
);

export const fromLogDownloadVisualizationActions = {
  loadAllLogDownloadVisualization,
  loadAllLogDownloadVisualizationSuccess,
  loadAllLogDownloadVisualizationFailure,
  clearLogDownloadVisualization,
};
