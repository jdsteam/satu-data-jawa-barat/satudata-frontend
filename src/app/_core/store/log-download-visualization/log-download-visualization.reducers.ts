import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { LogDownloadVisualizationData } from '@models-v3';
import { fromLogDownloadVisualizationActions } from '@store-v3/log-download-visualization/log-download-visualization.actions';

export const ENTITY_FEATURE_KEY = 'logDownloadVisualizationV3';

export interface State extends EntityState<LogDownloadVisualizationData> {
  isLoadingList: boolean;
  isLoadingCreate: boolean;
  isLoadingRead: boolean;
  isLoadingUpdate: boolean;
  isLoadingDelete: boolean;
  metadata: any;
  pagination: any;
  error?: Error | any;
}

export const adapter: EntityAdapter<LogDownloadVisualizationData> =
  createEntityAdapter<LogDownloadVisualizationData>({
    selectId: (item) => String(item.id),
  });

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  isLoadingList: false,
  isLoadingCreate: true,
  isLoadingRead: true,
  isLoadingUpdate: true,
  isLoadingDelete: true,
  metadata: null,
  pagination: null,
  error: null,
});

const reducer = createReducer(
  initialState,

  // Load All
  on(
    fromLogDownloadVisualizationActions.loadAllLogDownloadVisualization,
    (state) => {
      return {
        ...state,
        isLoadingList: true,
        error: null,
      };
    },
  ),
  on(
    fromLogDownloadVisualizationActions.loadAllLogDownloadVisualizationSuccess,
    (state, { data, pagination }) => {
      if (!pagination.infinite) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          error: { error: false },
        });
      }

      if (pagination.current_page === 1) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          error: { error: false },
        });
      }

      return adapter.addMany(data, {
        ...state,
        isLoadingList: false,
        pagination,
        error: { error: false },
      });
    },
  ),
  on(
    fromLogDownloadVisualizationActions.loadAllLogDownloadVisualizationFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingList: false,
        error,
      };
    },
  ),

  // Clear
  on(
    fromLogDownloadVisualizationActions.clearLogDownloadVisualization,
    (state) => {
      return adapter.removeAll({
        ...state,
        isLoadingList: false,
        pagination: { empty: true },
        error: { error: false },
      });
    },
  ),
);

export function LogDownloadVisualizationReducer(
  state: State | undefined,
  action: Action,
): any {
  return reducer(state, action);
}
