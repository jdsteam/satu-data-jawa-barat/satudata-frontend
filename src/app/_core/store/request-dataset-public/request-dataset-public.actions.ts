import { createAction, props } from '@ngrx/store';
import { RequestDatasetPublicData } from '@models-v3';

export enum RequestDatasetPublicActionTypes {
  LoadAllRequestDatasetPublic = '[REQUEST DATASET PUBLIC] Load All Request Dataset Public',
  LoadAllRequestDatasetPublicSuccess = '[REQUEST DATASET PUBLIC] Load All Request Dataset Public Success',
  LoadAllRequestDatasetPublicFailure = '[REQUEST DATASET PUBLIC] Load All Request Dataset Public Failure',
  UpdateRequestDatasetPublic = '[REQUEST DATASET PUBLIC] Update Request Dataset Public',
  UpdateRequestDatasetPublicSuccess = '[REQUEST DATASET PUBLIC] Update Request Dataset Public Success',
  UpdateRequestDatasetPublicFailure = '[REQUEST DATASET PUBLIC] Update Request Dataset Public Failure',
  ClearRequestDatasetPublic = '[REQUEST DATASET PUBLIC] Clear Request Dataset Public',
}

// Load All
export const loadAllRequestDatasetPublic = createAction(
  RequestDatasetPublicActionTypes.LoadAllRequestDatasetPublic,
  props<{ params: any; pagination: boolean }>(),
);

export const loadAllRequestDatasetPublicSuccess = createAction(
  RequestDatasetPublicActionTypes.LoadAllRequestDatasetPublicSuccess,
  props<{ data: RequestDatasetPublicData[]; pagination: any }>(),
);

export const loadAllRequestDatasetPublicFailure = createAction(
  RequestDatasetPublicActionTypes.LoadAllRequestDatasetPublicFailure,
  props<{ error: Error | any }>(),
);

// Update
export const updateRequestDatasetPublic = createAction(
  RequestDatasetPublicActionTypes.UpdateRequestDatasetPublic,
  props<{ update: any }>(),
);

export const updateRequestDatasetPublicSuccess = createAction(
  RequestDatasetPublicActionTypes.UpdateRequestDatasetPublicSuccess,
  props<{ data: RequestDatasetPublicData }>(),
);

export const updateRequestDatasetPublicFailure = createAction(
  RequestDatasetPublicActionTypes.UpdateRequestDatasetPublicFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearRequestDatasetPublic = createAction(
  RequestDatasetPublicActionTypes.ClearRequestDatasetPublic,
);

export const fromRequestDatasetPublicActions = {
  loadAllRequestDatasetPublic,
  loadAllRequestDatasetPublicSuccess,
  loadAllRequestDatasetPublicFailure,
  updateRequestDatasetPublic,
  updateRequestDatasetPublicSuccess,
  updateRequestDatasetPublicFailure,
  clearRequestDatasetPublic,
};
