import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { RequestDatasetPublicData } from '@models-v3';
import { fromRequestDatasetPublicActions } from '@store-v3/request-dataset-public/request-dataset-public.actions';

export const ENTITY_FEATURE_KEY = 'requestDatasetPublicV3';

export interface State extends EntityState<RequestDatasetPublicData> {
  isLoadingList: boolean;
  isLoadingCreate: boolean;
  isLoadingRead: boolean;
  isLoadingUpdate: boolean;
  isLoadingDelete: boolean;
  metadata: any;
  pagination: any;
  error?: Error | any;
}

export const adapter: EntityAdapter<RequestDatasetPublicData> =
  createEntityAdapter<RequestDatasetPublicData>({
    selectId: (item) => item.id,
  });

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  isLoadingList: false,
  isLoadingCreate: true,
  isLoadingRead: true,
  isLoadingUpdate: true,
  isLoadingDelete: true,
  metadata: null,
  pagination: null,
  error: null,
});

const reducer = createReducer(
  initialState,

  // Load All
  on(fromRequestDatasetPublicActions.loadAllRequestDatasetPublic, (state) => {
    return {
      ...state,
      isLoadingList: true,
    };
  }),
  on(
    fromRequestDatasetPublicActions.loadAllRequestDatasetPublicSuccess,
    (state, { data, pagination }) => {
      return adapter.setAll(data, {
        ...state,
        isLoadingList: false,
        pagination,
        error: { error: false },
      });
    },
  ),
  on(
    fromRequestDatasetPublicActions.loadAllRequestDatasetPublicFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingList: false,
        error,
      };
    },
  ),

  // Update
  on(fromRequestDatasetPublicActions.updateRequestDatasetPublic, (state) => {
    return {
      ...state,
      isLoadingUpdate: true,
    };
  }),
  on(
    fromRequestDatasetPublicActions.updateRequestDatasetPublicSuccess,
    (state, { data }) => {
      return adapter.updateOne(
        { id: data.id, changes: data },
        {
          ...state,
          isLoadingUpdate: false,
          error: { error: false },
        },
      );
    },
  ),
  on(
    fromRequestDatasetPublicActions.updateRequestDatasetPublicFailure,
    (state, { error }) => {
      return {
        ...state,
        error,
      };
    },
  ),

  // Clear
  on(fromRequestDatasetPublicActions.clearRequestDatasetPublic, (state) => {
    return adapter.removeAll({
      ...state,
      isLoadingList: false,
      pagination: { empty: true },
      error: { error: false },
    });
  }),
);

export function RequestDatasetPublicReducer(
  state: State | undefined,
  action: Action,
): any {
  return reducer(state, action);
}
