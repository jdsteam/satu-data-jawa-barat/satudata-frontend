import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';

import { fromRequestDatasetPublicActions } from '@store-v3/request-dataset-public/request-dataset-public.actions';
import { RequestDatasetPublicService, PaginationService } from '@services-v3';
import { handlePromise } from '@helpers-v3';

@Injectable()
export class RequestDatasetPublicEffects {
  name = 'Request Dataset Public';

  loadAllRequestDatasetPublic$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromRequestDatasetPublicActions.loadAllRequestDatasetPublic),
      switchMap(async (action) => {
        const { data: result, error: resultErr } = await handlePromise(
          this.requestDatasetPublicService.getList(action.params).toPromise(),
        );

        if (resultErr) {
          return this.loadAllFailure(resultErr.message);
        }

        if (result.error === 1) {
          if (result.message === 'Data not found') {
            return fromRequestDatasetPublicActions.clearRequestDatasetPublic();
          }

          return this.loadAllFailure(result.message);
        }

        let pagination = { empty: true };
        if (action.pagination) {
          const { data: total, error: totalErr } = await handlePromise(
            this.requestDatasetPublicService
              .getTotal(action.params)
              .toPromise(),
          );

          if (totalErr) {
            return this.loadAllFailure(totalErr.message);
          }

          const resultPagination = this.paginationService.getPagination(
            total.data.count,
            action.params.skip,
            action.params.limit,
          );

          pagination = { empty: false, ...resultPagination };
        }

        return this.loadAllSuccess(result.data, pagination);
      }),
    ),
  );

  updateRequestDatasetPublic$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromRequestDatasetPublicActions.updateRequestDatasetPublic),
      switchMap((action) =>
        this.requestDatasetPublicService
          .updateItem(action.update.id, action.update)
          .pipe(
            map((res: any) => {
              return fromRequestDatasetPublicActions.updateRequestDatasetPublicSuccess(
                {
                  data: res.data,
                },
              );
            }),
            catchError((error) => {
              return of(
                fromRequestDatasetPublicActions.updateRequestDatasetPublicFailure(
                  {
                    error: {
                      name: this.name,
                      error: true,
                      message: error.message,
                    },
                  },
                ),
              );
            }),
          ),
      ),
    ),
  );

  constructor(
    private actions$: Actions,
    private requestDatasetPublicService: RequestDatasetPublicService,
    private paginationService: PaginationService,
  ) {}

  loadAllSuccess(data: any, pagination: any): any {
    return fromRequestDatasetPublicActions.loadAllRequestDatasetPublicSuccess({
      data,
      pagination,
    });
  }

  loadAllFailure(error: string): any {
    return fromRequestDatasetPublicActions.loadAllRequestDatasetPublicFailure({
      error: {
        name: this.name,
        error: true,
        message: error,
      },
    });
  }
}
