import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule as Store } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from 'src/environments/environment';

// V3
import { OrganizationReducer as OrganizationV3Reducer } from '@store-v3/organization/organization.reducers';
import { OrganizationEffects as OrganizationV3Effects } from '@store-v3/organization/organization.effects';

import { OrganizationSubReducer as OrganizationSubV3Reducer } from '@store-v3/organization-sub/organization-sub.reducers';
import { OrganizationSubEffects as OrganizationSubV3Effects } from '@store-v3/organization-sub/organization-sub.effects';

import { OrganizationUnitReducer as OrganizationUnitV3Reducer } from '@store-v3/organization-unit/organization-unit.reducers';
import { OrganizationUnitEffects as OrganizationUnitV3Effects } from '@store-v3/organization-unit/organization-unit.effects';

import { ApplicationReducer as ApplicationV3Reducer } from '@store-v3/application/application.reducers';
import { ApplicationEffects as ApplicationV3Effects } from '@store-v3/application/application.effects';

import { ApplicationServiceReducer as ApplicationServiceV3Reducer } from '@store-v3/application-service/application-service.reducers';
import { ApplicationServiceEffects as ApplicationServiceV3Effects } from '@store-v3/application-service/application-service.effects';

import { TopicReducer as TopicV3Reducer } from '@store-v3/topic/topic.reducers';
import { TopicEffects as TopicV3Effects } from '@store-v3/topic/topic.effects';

import { LicenseReducer as LicenseV3Reducer } from '@store-v3/license/license.reducers';
import { LicenseEffects as LicenseV3Effects } from '@store-v3/license/license.effects';

import { ClassificationDatasetReducer as ClassificationDatasetV3Reducer } from '@store-v3/classification-dataset/classification-dataset.reducers';
import { ClassificationDatasetEffects as ClassificationDatasetV3Effects } from '@store-v3/classification-dataset/classification-dataset.effects';

import { ClassificationIndicatorReducer as ClassificationIndicatorV3Reducer } from '@store-v3/classification-indicator/classification-indicator.reducers';
import { ClassificationIndicatorEffects as ClassificationIndicatorV3Effects } from '@store-v3/classification-indicator/classification-indicator.effects';

import { DatasetTagReducer as DatasetTagV3Reducer } from '@store-v3/dataset-tag/dataset-tag.reducers';
import { DatasetTagEffects as DatasetTagV3Effects } from '@store-v3/dataset-tag/dataset-tag.effects';

import { DatasetReducer as DatasetV3Reducer } from '@store-v3/dataset/dataset.reducers';
import { DatasetEffects as DatasetV3Effects } from '@store-v3/dataset/dataset.effects';

import { HistoryRequestPrivateReducer as HistoryRequestPrivateV3Reducer } from '@store-v3/history-request-private/history-request-private.reducers';
import { HistoryRequestPrivateEffects as HistoryRequestPrivateV3Effects } from '@store-v3/history-request-private/history-request-private.effects';

import { HistoryRequestPublicReducer as HistoryRequestPublicV3Reducer } from '@store-v3/history-request-public/history-request-public.reducers';
import { HistoryRequestPublicEffects as HistoryRequestPublicV3Effects } from '@store-v3/history-request-public/history-request-public.effects';

import { IndicatorReducer as IndicatorV3Reducer } from '@store-v3/indicator/indicator.reducers';
import { IndicatorEffects as IndicatorV3Effects } from '@store-v3/indicator/indicator.effects';

import { VisualizationReducer as VisualizationV3Reducer } from '@store-v3/visualization/visualization.reducers';
import { VisualizationEffects as VisualizationV3Effects } from '@store-v3/visualization/visualization.effects';

import { RequestDatasetReducer as RequestDatasetV3Reducer } from '@store-v3/request-dataset/request-dataset.reducers';
import { RequestDatasetEffects as RequestDatasetV3Effects } from '@store-v3/request-dataset/request-dataset.effects';

import { RequestDatasetPrivateReducer as RequestDatasetPrivateV3Reducer } from '@store-v3/request-dataset-private/request-dataset-private.reducers';
import { RequestDatasetPrivateEffects as RequestDatasetPrivateV3Effects } from '@store-v3/request-dataset-private/request-dataset-private.effects';

import { RequestDatasetPublicReducer as RequestDatasetPublicV3Reducer } from '@store-v3/request-dataset-public/request-dataset-public.reducers';
import { RequestDatasetPublicEffects as RequestDatasetPublicV3Effects } from '@store-v3/request-dataset-public/request-dataset-public.effects';

import { ReviewDatasetReducer as ReviewDatasetV3Reducer } from '@store-v3/review-dataset/review-dataset.reducers';
import { ReviewDatasetEffects as ReviewDatasetV3Effects } from '@store-v3/review-dataset/review-dataset.effects';

import { IndicatorCategoryReducer as IndicatorCategoryV3Reducer } from '@store-v3/indicator-category/indicator-category.reducers';
import { IndicatorCategoryEffects as IndicatorCategoryV3Effects } from '@store-v3/indicator-category/indicator-category.effects';

import { IndicatorClassificationHistoryReducer as IndicatorClassificationHistoryV3Reducer } from '@store-v3/indicator-classification-history/indicator-classification-history.reducers';
import { IndicatorClassificationHistoryEffects as IndicatorClassificationHistoryV3Effects } from '@store-v3/indicator-classification-history/indicator-classification-history.effects';

import { IndicatorProgressReducer as IndicatorProgressV3Reducer } from '@store-v3/indicator-progress/indicator-progress.reducers';
import { IndicatorProgressEffects as IndicatorProgressV3Effects } from '@store-v3/indicator-progress/indicator-progress.effects';

import { IndicatorProgressInitialReducer as IndicatorProgressInitialV3Reducer } from '@store-v3/indicator-progress-initial/indicator-progress-initial.reducers';
import { IndicatorProgressInitialEffects as IndicatorProgressInitialV3Effects } from '@store-v3/indicator-progress-initial/indicator-progress-initial.effects';

import { IndicatorProgressFinalReducer as IndicatorProgressFinalV3Reducer } from '@store-v3/indicator-progress-final/indicator-progress-final.reducers';
import { IndicatorProgressFinalEffects as IndicatorProgressFinalV3Effects } from '@store-v3/indicator-progress-final/indicator-progress-final.effects';

import { UnitReducer as UnitV3Reducer } from '@store-v3/unit/unit.reducers';
import { UnitEffects as UnitV3Effects } from '@store-v3/unit/unit.effects';

import { BusinessFieldReducer } from '@store-v3/business-field/business-field.reducers';
import { BusinessFieldEffects } from '@store-v3/business-field/business-field.effects';
import { BusinessFieldFacade } from '@store-v3/business-field/business-field.facade';

import { MetadataBusinessDatasetReducer as MetadataBusinessDatasetV3Reducer } from '@store-v3/metadata-business-dataset/metadata-business-dataset.reducers';
import { MetadataBusinessDatasetEffects as MetadataBusinessDatasetV3Effects } from '@store-v3/metadata-business-dataset/metadata-business-dataset.effects';

import { MetadataCustomDatasetReducer as MetadataCustomDatasetV3Reducer } from '@store-v3/metadata-custom-dataset/metadata-custom-dataset.reducers';
import { MetadataCustomDatasetEffects as MetadataCustomDatasetV3Effects } from '@store-v3/metadata-custom-dataset/metadata-custom-dataset.effects';

import { MetadataBusinessVisualizationReducer as MetadataBusinessVisualizationV3Reducer } from '@store-v3/metadata-business-visualization/metadata-business-visualization.reducers';
import { MetadataBusinessVisualizationEffects as MetadataBusinessVisualizationV3Effects } from '@store-v3/metadata-business-visualization/metadata-business-visualization.effects';

import { MetadataCustomVisualizationReducer as MetadataCustomVisualizationV3Reducer } from '@store-v3/metadata-custom-visualization/metadata-custom-visualization.reducers';
import { MetadataCustomVisualizationEffects as MetadataCustomVisualizationV3Effects } from '@store-v3/metadata-custom-visualization/metadata-custom-visualization.effects';

import { HistoryLoginReducer as HistoryLoginV3Reducer } from '@store-v3/history-login/history-login.reducers';
import { HistoryLoginEffects as HistoryLoginV3Effects } from '@store-v3/history-login/history-login.effects';

import { HistoryActivityReducer as HistoryActivityV3Reducer } from '@store-v3/history-activity/history-activity.reducers';
import { HistoryActivityEffects as HistoryActivityV3Effects } from '@store-v3/history-activity/history-activity.effects';

import { NotificationReducer as NotificationV3Reducer } from '@store-v3/notification/notification.reducers';
import { NotificationEffects as NotificationV3Effects } from '@store-v3/notification/notification.effects';

import { CoreDataReducer as CoreDataV3Reducer } from '@store-v3/core-data/core-data.reducers';
import { CoreDataEffects as CoreDataV3Effects } from '@store-v3/core-data/core-data.effects';

import { LogLoginReducer as LogLoginV3Reducer } from '@store-v3/log-login/log-login.reducers';
import { LogLoginEffects as LogLoginV3Effects } from '@store-v3/log-login/log-login.effects';

import { LogPageReducer as LogPageV3Reducer } from '@store-v3/log-page/log-page.reducers';
import { LogPageEffects as LogPageV3Effects } from '@store-v3/log-page/log-page.effects';

import { LogDatasetReducer as LogDatasetV3Reducer } from '@store-v3/log-dataset/log-dataset.reducers';
import { LogDatasetEffects as LogDatasetV3Effects } from '@store-v3/log-dataset/log-dataset.effects';

import { LogIndicatorReducer as LogIndicatorV3Reducer } from '@store-v3/log-indicator/log-indicator.reducers';
import { LogIndicatorEffects as LogIndicatorV3Effects } from '@store-v3/log-indicator/log-indicator.effects';

import { LogVisualizationReducer as LogVisualizationV3Reducer } from '@store-v3/log-visualization/log-visualization.reducers';
import { LogVisualizationEffects as LogVisualizationV3Effects } from '@store-v3/log-visualization/log-visualization.effects';

import { LogDownloadDatasetReducer as LogDownloadDatasetV3Reducer } from '@store-v3/log-download-dataset/log-download-dataset.reducers';
import { LogDownloadDatasetEffects as LogDownloadDatasetV3Effects } from '@store-v3/log-download-dataset/log-download-dataset.effects';

import { LogDownloadIndicatorReducer as LogDownloadIndicatorV3Reducer } from '@store-v3/log-download-indicator/log-download-indicator.reducers';
import { LogDownloadIndicatorEffects as LogDownloadIndicatorV3Effects } from '@store-v3/log-download-indicator/log-download-indicator.effects';

import { LogDownloadVisualizationReducer as LogDownloadVisualizationV3Reducer } from '@store-v3/log-download-visualization/log-download-visualization.reducers';
import { LogDownloadVisualizationEffects as LogDownloadVisualizationV3Effects } from '@store-v3/log-download-visualization/log-download-visualization.effects';

@NgModule({
  imports: [
    CommonModule,
    Store.forRoot(
      {
        // V3
        organizationV3: OrganizationV3Reducer,
        organizationSubV3: OrganizationSubV3Reducer,
        organizationUnitV3: OrganizationUnitV3Reducer,
        applicationV3: ApplicationV3Reducer,
        applicationServiceV3: ApplicationServiceV3Reducer,
        topicV3: TopicV3Reducer,
        licenseV3: LicenseV3Reducer,
        classificationDatasetV3: ClassificationDatasetV3Reducer,
        classificationIndicatorV3: ClassificationIndicatorV3Reducer,
        datasetTagV3: DatasetTagV3Reducer,
        datasetV3: DatasetV3Reducer,
        historyRequestPrivateV3: HistoryRequestPrivateV3Reducer,
        historyRequestPublicV3: HistoryRequestPublicV3Reducer,
        indicatorV3: IndicatorV3Reducer,
        indicatorCategoryV3: IndicatorCategoryV3Reducer,
        indicatorClassificationHistoryV3:
          IndicatorClassificationHistoryV3Reducer,
        indicatorProgressV3: IndicatorProgressV3Reducer,
        indicatorProgressInitialV3: IndicatorProgressInitialV3Reducer,
        indicatorProgressFinalV3: IndicatorProgressFinalV3Reducer,
        visualizationV3: VisualizationV3Reducer,
        requestDatasetV3: RequestDatasetV3Reducer,
        requestDatasetPrivateV3: RequestDatasetPrivateV3Reducer,
        requestDatasetPublicV3: RequestDatasetPublicV3Reducer,
        reviewDatasetV3: ReviewDatasetV3Reducer,
        unitV3: UnitV3Reducer,
        businessField: BusinessFieldReducer,
        metadataBusinessDatasetV3: MetadataBusinessDatasetV3Reducer,
        metadataCustomDatasetV3: MetadataCustomDatasetV3Reducer,
        metadataBusinessVisualizationV3: MetadataBusinessVisualizationV3Reducer,
        metadataCustomVisualizationV3: MetadataCustomVisualizationV3Reducer,
        historyLoginV3: HistoryLoginV3Reducer,
        historyActivityV3: HistoryActivityV3Reducer,
        notificationV3: NotificationV3Reducer,
        coreDataV3: CoreDataV3Reducer,
        logLoginV3: LogLoginV3Reducer,
        logPageV3: LogPageV3Reducer,
        logDatasetV3: LogDatasetV3Reducer,
        logIndicatorV3: LogIndicatorV3Reducer,
        logVisualizationV3: LogVisualizationV3Reducer,
        logDownloadDatasetV3: LogDownloadDatasetV3Reducer,
        logDownloadIndicatorV3: LogDownloadIndicatorV3Reducer,
        logDownloadVisualizationV3: LogDownloadVisualizationV3Reducer,
      },
      {
        metaReducers: [],
      },
    ),
    EffectsModule.forRoot([
      // V3
      OrganizationV3Effects,
      OrganizationSubV3Effects,
      OrganizationUnitV3Effects,
      ApplicationV3Effects,
      ApplicationServiceV3Effects,
      TopicV3Effects,
      LicenseV3Effects,
      ClassificationDatasetV3Effects,
      ClassificationIndicatorV3Effects,
      DatasetTagV3Effects,
      DatasetV3Effects,
      HistoryRequestPrivateV3Effects,
      HistoryRequestPublicV3Effects,
      IndicatorV3Effects,
      IndicatorCategoryV3Effects,
      IndicatorClassificationHistoryV3Effects,
      IndicatorProgressV3Effects,
      IndicatorProgressInitialV3Effects,
      IndicatorProgressFinalV3Effects,
      VisualizationV3Effects,
      RequestDatasetV3Effects,
      RequestDatasetPrivateV3Effects,
      RequestDatasetPublicV3Effects,
      ReviewDatasetV3Effects,
      UnitV3Effects,
      BusinessFieldEffects,
      MetadataBusinessDatasetV3Effects,
      MetadataCustomDatasetV3Effects,
      MetadataBusinessVisualizationV3Effects,
      MetadataCustomVisualizationV3Effects,
      HistoryLoginV3Effects,
      HistoryActivityV3Effects,
      NotificationV3Effects,
      CoreDataV3Effects,
      LogLoginV3Effects,
      LogPageV3Effects,
      LogDatasetV3Effects,
      LogIndicatorV3Effects,
      LogVisualizationV3Effects,
      LogDownloadDatasetV3Effects,
      LogDownloadIndicatorV3Effects,
      LogDownloadVisualizationV3Effects,
    ]),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production,
    }),
  ],
  exports: [Store, EffectsModule, StoreDevtoolsModule],
  providers: [BusinessFieldFacade],
})
export class NgrxModule {}
