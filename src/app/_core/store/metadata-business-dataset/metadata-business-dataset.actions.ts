import { createAction, props } from '@ngrx/store';
import { MetadataDatasetData } from '@models-v3';

export enum MetadataBusinessDatasetActionTypes {
  LoadAllMetadataBusinessDataset = '[METADATA BUSINESS DATASET] Load All Metadata Business Dataset',
  LoadAllMetadataBusinessDatasetSuccess = '[METADATA BUSINESS DATASET] Load All Metadata Business Dataset Success',
  LoadAllMetadataBusinessDatasetFailure = '[METADATA BUSINESS DATASET] Load All Metadata Business Dataset Failure',
  ClearMetadataBusinessDataset = '[METADATA BUSINESS DATASET] Clear Metadata Business Dataset',
}

// Load All
export const loadAllMetadataBusinessDataset = createAction(
  MetadataBusinessDatasetActionTypes.LoadAllMetadataBusinessDataset,
  props<{ params: any; pagination: boolean; infinite: boolean }>(),
);

export const loadAllMetadataBusinessDatasetSuccess = createAction(
  MetadataBusinessDatasetActionTypes.LoadAllMetadataBusinessDatasetSuccess,
  props<{ data: MetadataDatasetData[]; pagination: any }>(),
);

export const loadAllMetadataBusinessDatasetFailure = createAction(
  MetadataBusinessDatasetActionTypes.LoadAllMetadataBusinessDatasetFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearMetadataBusinessDataset = createAction(
  MetadataBusinessDatasetActionTypes.ClearMetadataBusinessDataset,
);

export const fromMetadataBusinessDatasetActions = {
  loadAllMetadataBusinessDataset,
  loadAllMetadataBusinessDatasetSuccess,
  loadAllMetadataBusinessDatasetFailure,
  clearMetadataBusinessDataset,
};
