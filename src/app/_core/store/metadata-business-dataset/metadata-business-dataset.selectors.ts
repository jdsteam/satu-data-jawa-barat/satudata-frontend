import { createFeatureSelector, createSelector } from '@ngrx/store';
import {
  State,
  adapter,
  ENTITY_FEATURE_KEY,
} from '@store-v3/metadata-business-dataset/metadata-business-dataset.reducers';

// Lookup feature state managed by NgRx
const getState = createFeatureSelector<State>(ENTITY_FEATURE_KEY);

// get the selectors
const { selectIds, selectAll, selectEntities } = adapter.getSelectors();

// select the array of ids
export const selectMetadataBusinessDatasetIds = createSelector(
  getState,
  selectIds,
);

// select the array
export const selectAllMetadataBusinessDataset = createSelector(
  getState,
  selectAll,
);

export const getMetadataBusinessDatasetEntities = createSelector(
  getState,
  (state: State) => selectEntities(state),
);

// select by id
export const selectMetadataBusinessDataset = (id: number | string) =>
  createSelector(getMetadataBusinessDatasetEntities, (entities) => {
    return entities[id];
  });

// select the by multiple id
export const selectMultipleMetadataBusinessDataset = (ids: any[]) =>
  createSelector(getMetadataBusinessDatasetEntities, (entities) => {
    const filtered = Object.keys(entities)
      .filter((key) => ids.includes(key))
      .reduce((obj, key) => {
        // eslint-disable-next-line no-param-reassign
        obj[key] = entities[key];
        return obj;
      }, {});

    return Object.keys(filtered).map((key) => filtered[key]);
  });

// select loaded flag
export const selectIsLoadingList = createSelector(
  getState,
  (state) => state.isLoadingList,
);
export const selectIsLoadingCreate = createSelector(
  getState,
  (state) => state.isLoadingCreate,
);
export const selectIsLoadingRead = createSelector(
  getState,
  (state) => state.isLoadingRead,
);
export const selectIsLoadingUpdate = createSelector(
  getState,
  (state) => state.isLoadingUpdate,
);
export const selectIsLoadingDelete = createSelector(
  getState,
  (state) => state.isLoadingDelete,
);

// select metadata
export const selectMetadata = createSelector(
  getState,
  (state) => state.metadata,
);

// select pagination
export const selectPagination = createSelector(
  getState,
  (state) => state.pagination,
);

// select error
export const selectError = createSelector(getState, (state) => state.error);
