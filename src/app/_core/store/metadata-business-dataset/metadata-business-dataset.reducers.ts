import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { MetadataDatasetData } from '@models-v3';
import { fromMetadataBusinessDatasetActions } from '@store-v3/metadata-business-dataset/metadata-business-dataset.actions';

export const ENTITY_FEATURE_KEY = 'metadataBusinessDatasetV3';

export interface State extends EntityState<MetadataDatasetData> {
  isLoadingList: boolean;
  isLoadingCreate: boolean;
  isLoadingRead: boolean;
  isLoadingUpdate: boolean;
  isLoadingDelete: boolean;
  metadata: any;
  pagination: any;
  error?: Error | any;
}

export const adapter: EntityAdapter<MetadataDatasetData> =
  createEntityAdapter<MetadataDatasetData>({
    selectId: (item) => item.id,
  });

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  isLoadingList: false,
  isLoadingCreate: false,
  isLoadingRead: false,
  isLoadingUpdate: false,
  isLoadingDelete: false,
  metadata: null,
  pagination: null,
  error: null,
});

const reducer = createReducer(
  initialState,

  // Load All
  on(
    fromMetadataBusinessDatasetActions.loadAllMetadataBusinessDataset,
    (state) => {
      return {
        ...state,
        isLoadingList: true,
        error: null,
      };
    },
  ),
  on(
    fromMetadataBusinessDatasetActions.loadAllMetadataBusinessDatasetSuccess,
    (state, { data, pagination }) => {
      if (!pagination.infinite) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          error: { error: false },
        });
      }

      if (pagination.current_page === 1) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          error: { error: false },
        });
      }

      return adapter.addMany(data, {
        ...state,
        isLoadingList: false,
        pagination,
        error: { error: false },
      });
    },
  ),
  on(
    fromMetadataBusinessDatasetActions.loadAllMetadataBusinessDatasetFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingList: false,
        error,
      };
    },
  ),

  // Clear
  on(
    fromMetadataBusinessDatasetActions.clearMetadataBusinessDataset,
    (state) => {
      return adapter.removeAll({
        ...state,
        isLoadingList: false,
        pagination: { empty: true },
        error: { error: false },
      });
    },
  ),
);

export function MetadataBusinessDatasetReducer(
  state: State | undefined,
  action: Action,
): any {
  return reducer(state, action);
}
