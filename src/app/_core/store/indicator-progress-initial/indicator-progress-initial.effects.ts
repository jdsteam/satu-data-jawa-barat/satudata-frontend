import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';

import { fromIndicatorProgressInitialActions } from '@store-v3/indicator-progress-initial/indicator-progress-initial.actions';
import { IndicatorProgressService, PaginationService } from '@services-v3';

import * as _ from 'lodash';
import { handlePromise } from '@helpers-v3';

@Injectable()
export class IndicatorProgressInitialEffects {
  name = 'Indicator Progress Initial';

  loadAllIndicatorProgressInitial$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        fromIndicatorProgressInitialActions.loadAllIndicatorProgressInitial,
      ),
      switchMap(async (action) => {
        const { data: result, error: resultErr } = await handlePromise(
          this.indicatorProgressService.getList(action.params).toPromise(),
        );

        if (resultErr) {
          return this.loadAllFailure(resultErr.message);
        }

        if (result.error === 1) {
          if (result.message === 'Data not found') {
            return fromIndicatorProgressInitialActions.clearIndicatorProgressInitial();
          }

          return this.loadAllFailure(result.message);
        }

        let pagination = { empty: true, infinite: action.infinite };
        if (action.pagination) {
          const resultPagination = this.paginationService.getPagination(
            result.meta.total_record,
            action.params.skip,
            action.params.limit,
          );

          pagination = {
            empty: false,
            infinite: action.infinite,
            ...resultPagination,
          };
        }

        return this.loadAllSuccess(result.data, pagination);
      }),
    ),
  );

  loadIndicatorProgressInitial$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromIndicatorProgressInitialActions.loadIndicatorProgressInitial),
      switchMap((action) => {
        return this.indicatorProgressService
          .getSingle(action.id, action.params)
          .pipe(
            map((result: any) =>
              fromIndicatorProgressInitialActions.loadIndicatorProgressInitialSuccess(
                {
                  data: result.data,
                },
              ),
            ),
            catchError((error: HttpErrorResponse) =>
              of(
                fromIndicatorProgressInitialActions.loadIndicatorProgressInitialFailure(
                  {
                    error: {
                      name: this.name,
                      error: !error.ok,
                      message: error.message,
                    },
                  },
                ),
              ),
            ),
          );
      }),
    ),
  );

  createIndicatorProgress$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        fromIndicatorProgressInitialActions.createIndicatorProgressInitial,
      ),
      switchMap((action) => {
        const indicatorId = action.indicator.id;
        const indexHelper = 1;

        const bodyProgress = [];
        _.map(action.create, (result, index) => {
          bodyProgress.push({
            indikator_id: indicatorId,
            year: result.year,
            target: result.target || null,
            capaian: result.capaian || null,
            notes: result.notes,
            index: index + indexHelper,
          });
        });

        return this.indicatorProgressService.createItem(bodyProgress).pipe(
          map((res: any) => {
            return fromIndicatorProgressInitialActions.createIndicatorProgressInitialSuccess(
              {
                data: res.data,
              },
            );
          }),
          catchError((error) => {
            return of(
              fromIndicatorProgressInitialActions.createIndicatorProgressInitialFailure(
                {
                  error: {
                    name: this.name,
                    error: !error.ok,
                    message: error.message,
                  },
                },
              ),
            );
          }),
        );
      }),
    ),
  );

  constructor(
    private actions$: Actions,
    private indicatorProgressService: IndicatorProgressService,
    private paginationService: PaginationService,
  ) {}

  loadAllSuccess(data: any, pagination: any): any {
    return fromIndicatorProgressInitialActions.loadAllIndicatorProgressInitialSuccess(
      {
        data,
        pagination,
      },
    );
  }

  loadAllFailure(error: string): any {
    return fromIndicatorProgressInitialActions.loadAllIndicatorProgressInitialFailure(
      {
        error: {
          name: this.name,
          error: true,
          message: error,
        },
      },
    );
  }
}
