import { createAction, props } from '@ngrx/store';
import { IndicatorProgressData } from '@models-v3';

export enum IndicatorProgressInitialActionTypes {
  LoadAllIndicatorProgressInitial = '[INDICATOR PROGRESS INITIAL] Load All Indicator Progress Initial',
  LoadAllIndicatorProgressInitialSuccess = '[INDICATOR PROGRESS INITIAL] Load All Indicator Progress Initial Success',
  LoadAllIndicatorProgressInitialFailure = '[INDICATOR PROGRESS INITIAL] Load All Indicator Progress Initial Failure',
  LoadIndicatorProgressInitial = '[INDICATOR PROGRESS INITIAL] Load Indicator Progress Initial',
  LoadIndicatorProgressInitialSuccess = '[INDICATOR PROGRESS INITIAL] Load Indicator Progress Initial Success',
  LoadIndicatorProgressInitialFailure = '[INDICATOR PROGRESS INITIAL] Load Indicator Progress Initial Failure',
  CreateIndicatorProgressInitial = '[INDICATOR PROGRESS INITIAL] Create Indicator Progress Initial',
  CreateIndicatorProgressInitialSuccess = '[INDICATOR PROGRESS INITIAL] Create Indicator Progress Initial Success',
  CreateIndicatorProgressInitialFailure = '[INDICATOR PROGRESS INITIAL] Create Indicator Progress Initial Failure',
  ClearIndicatorProgressInitial = '[INDICATOR PROGRESS INITIAL] Clear Indicator Progress Initial',
}

// Load All
export const loadAllIndicatorProgressInitial = createAction(
  IndicatorProgressInitialActionTypes.LoadAllIndicatorProgressInitial,
  props<{ params: any; pagination: boolean; infinite: boolean }>(),
);

export const loadAllIndicatorProgressInitialSuccess = createAction(
  IndicatorProgressInitialActionTypes.LoadAllIndicatorProgressInitialSuccess,
  props<{ data: IndicatorProgressData[]; pagination: any }>(),
);

export const loadAllIndicatorProgressInitialFailure = createAction(
  IndicatorProgressInitialActionTypes.LoadAllIndicatorProgressInitialFailure,
  props<{ error: Error | any }>(),
);

// Load Single
export const loadIndicatorProgressInitial = createAction(
  IndicatorProgressInitialActionTypes.LoadIndicatorProgressInitial,
  props<{ id: number; params: any }>(),
);

export const loadIndicatorProgressInitialSuccess = createAction(
  IndicatorProgressInitialActionTypes.LoadIndicatorProgressInitialSuccess,
  props<{ data: IndicatorProgressData }>(),
);

export const loadIndicatorProgressInitialFailure = createAction(
  IndicatorProgressInitialActionTypes.LoadIndicatorProgressInitialFailure,
  props<{ error: Error | any }>(),
);

// Create
export const createIndicatorProgressInitial = createAction(
  IndicatorProgressInitialActionTypes.CreateIndicatorProgressInitial,
  props<{ create: any; indicator: any }>(),
);

export const createIndicatorProgressInitialSuccess = createAction(
  IndicatorProgressInitialActionTypes.CreateIndicatorProgressInitialSuccess,
  props<{ data: IndicatorProgressData[] }>(),
);

export const createIndicatorProgressInitialFailure = createAction(
  IndicatorProgressInitialActionTypes.CreateIndicatorProgressInitialFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearIndicatorProgressInitial = createAction(
  IndicatorProgressInitialActionTypes.ClearIndicatorProgressInitial,
);

export const fromIndicatorProgressInitialActions = {
  loadAllIndicatorProgressInitial,
  loadAllIndicatorProgressInitialSuccess,
  loadAllIndicatorProgressInitialFailure,
  loadIndicatorProgressInitial,
  loadIndicatorProgressInitialSuccess,
  loadIndicatorProgressInitialFailure,
  createIndicatorProgressInitial,
  createIndicatorProgressInitialSuccess,
  createIndicatorProgressInitialFailure,
  clearIndicatorProgressInitial,
};
