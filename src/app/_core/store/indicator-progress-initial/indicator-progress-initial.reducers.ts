import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { IndicatorProgressData } from '@models-v3';
import { fromIndicatorProgressInitialActions } from '@store-v3/indicator-progress-initial/indicator-progress-initial.actions';

export const ENTITY_FEATURE_KEY = 'indicatorProgressInitialV3';

export interface State extends EntityState<IndicatorProgressData> {
  isLoadingList: boolean;
  isLoadingCreate: boolean;
  isLoadingRead: boolean;
  isLoadingUpdate: boolean;
  isLoadingDelete: boolean;
  metadata: any;
  pagination: any;
  error?: Error | any;
}

export const adapter: EntityAdapter<IndicatorProgressData> =
  createEntityAdapter<IndicatorProgressData>({
    selectId: (item) => item.id,
  });

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  isLoadingList: false,
  isLoadingCreate: false,
  isLoadingRead: false,
  isLoadingUpdate: false,
  isLoadingDelete: false,
  metadata: null,
  pagination: null,
  error: null,
});

const reducer = createReducer(
  initialState,

  // Load All
  on(
    fromIndicatorProgressInitialActions.loadAllIndicatorProgressInitial,
    (state) => {
      return {
        ...state,
        isLoadingList: true,
      };
    },
  ),
  on(
    fromIndicatorProgressInitialActions.loadAllIndicatorProgressInitialSuccess,
    (state, { data, pagination }) => {
      if (!pagination.infinite) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          error: { error: false },
        });
      }

      if (pagination.current_page === 1) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          error: { error: false },
        });
      }

      return adapter.addMany(data, {
        ...state,
        isLoadingList: false,
        pagination,
        error: { error: false },
      });
    },
  ),
  on(
    fromIndicatorProgressInitialActions.loadAllIndicatorProgressInitialFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingList: false,
        error,
      };
    },
  ),

  // Load Single
  on(
    fromIndicatorProgressInitialActions.loadIndicatorProgressInitial,
    (state) => {
      return {
        ...state,
        isLoadingRead: true,
      };
    },
  ),
  on(
    fromIndicatorProgressInitialActions.loadIndicatorProgressInitialSuccess,
    (state, { data }) => {
      return adapter.addOne(data, {
        ...state,
        isLoadingRead: false,
        error: { error: false },
      });
    },
  ),
  on(
    fromIndicatorProgressInitialActions.loadIndicatorProgressInitialFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingRead: false,
        error,
      };
    },
  ),

  // Create
  on(
    fromIndicatorProgressInitialActions.createIndicatorProgressInitial,
    (state) => {
      return {
        ...state,
        isLoadingCreate: true,
      };
    },
  ),
  on(
    fromIndicatorProgressInitialActions.createIndicatorProgressInitialSuccess,
    (state, { data }) => {
      return adapter.addMany(data, {
        ...state,
        isLoadingCreate: false,
        error: { error: false },
      });
    },
  ),
  on(
    fromIndicatorProgressInitialActions.createIndicatorProgressInitialFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingCreate: false,
        error,
      };
    },
  ),

  // Clear
  on(
    fromIndicatorProgressInitialActions.clearIndicatorProgressInitial,
    (state) => {
      return adapter.removeAll({
        ...state,
        isLoadingList: false,
        pagination: { empty: true },
        error: { error: false },
      });
    },
  ),
);

export function IndicatorProgressInitialReducer(
  state: State | undefined,
  action: Action,
): any {
  return reducer(state, action);
}
