import { createFeatureSelector, createSelector } from '@ngrx/store';
import {
  State,
  adapter,
  ENTITY_FEATURE_KEY,
} from '@store-v3/history-request-private/history-request-private.reducers';

// Lookup feature state managed by NgRx
const getState = createFeatureSelector<State>(ENTITY_FEATURE_KEY);

// get the selectors
const { selectIds, selectAll, selectEntities } = adapter.getSelectors();

// select the array of ids
export const selectHistoryRequestPrivateIds = createSelector(
  getState,
  selectIds,
);

// select the array
export const selectAllHistoryRequestPrivate = createSelector(
  getState,
  selectAll,
);

export const getHistoryRequestPrivateEntities = createSelector(
  getState,
  (state: State) => selectEntities(state),
);

// select by id
export const selectHistoryRequestPrivate = (id: number | string) =>
  createSelector(getHistoryRequestPrivateEntities, (entities) => {
    return entities[id];
  });

// select by Title
export const selectHistoryRequestPrivateByTitle = (title: number | string) =>
  createSelector(getHistoryRequestPrivateEntities, (entities) => {
    return entities[title];
  });

// select loaded flag
export const selectIsLoadingList = createSelector(
  getState,
  (state) => state.isLoadingList,
);
export const selectIsLoadingCreate = createSelector(
  getState,
  (state) => state.isLoadingCreate,
);
export const selectIsLoadingRead = createSelector(
  getState,
  (state) => state.isLoadingRead,
);
export const selectIsLoadingUpdate = createSelector(
  getState,
  (state) => state.isLoadingUpdate,
);
export const selectIsLoadingDelete = createSelector(
  getState,
  (state) => state.isLoadingDelete,
);
export const selectIsLoadingCounter = createSelector(
  getState,
  (state) => state.isLoadingCounter,
);

// select metadata
export const selectMetadata = createSelector(
  getState,
  (state) => state.metadata,
);

// select pagination
export const selectPagination = createSelector(
  getState,
  (state) => state.pagination,
);

// select error
export const selectError = createSelector(getState, (state) => state.error);
