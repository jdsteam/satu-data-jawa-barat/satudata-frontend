import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';

import { fromHistoryRequestPrivateActions } from '@store-v3/history-request-private/history-request-private.actions';
import { HistoryRequestPrivateService, PaginationService } from '@services-v3';
import { handlePromise } from '@helpers-v3';

@Injectable()
export class HistoryRequestPrivateEffects {
  name = 'History Request Private';

  loadAllHistoryRequestPrivate$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromHistoryRequestPrivateActions.loadAllHistoryRequestPrivate),
      switchMap(async (action) => {
        const { data: result, error: resultErr } = await handlePromise(
          this.historyRequestPrivateService.getList(action.params).toPromise(),
        );

        if (resultErr) {
          return this.loadAllFailure(resultErr.message);
        }

        if (result.error === 1) {
          if (result.message === 'Data not found') {
            return fromHistoryRequestPrivateActions.clearHistoryRequestPrivate();
          }

          return this.loadAllFailure(result.message);
        }

        let pagination = { empty: true };
        if (action.pagination) {
          const { data: total, error: totalErr } = await handlePromise(
            this.historyRequestPrivateService
              .getTotal(action.params)
              .toPromise(),
          );

          if (totalErr) {
            return this.loadAllFailure(totalErr.message);
          }

          const resultPagination = this.paginationService.getPagination(
            total.data.count,
            action.params.skip,
            action.params.limit,
          );

          pagination = { empty: false, ...resultPagination };
        }

        return this.loadAllSuccess(result.data, pagination);
      }),
    ),
  );

  loadHistoryRequestPrivate$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromHistoryRequestPrivateActions.loadHistoryRequestPrivate),
      switchMap((action) => {
        return this.historyRequestPrivateService
          .getSingle(action.id, action.params)
          .pipe(
            map((result: any) =>
              fromHistoryRequestPrivateActions.loadHistoryRequestPrivateSuccess(
                {
                  data: result.data,
                },
              ),
            ),
            catchError((error: HttpErrorResponse) =>
              of(
                fromHistoryRequestPrivateActions.loadHistoryRequestPrivateFailure(
                  {
                    error: {
                      name: this.name,
                      error: !error.ok,
                      message: error.message,
                    },
                  },
                ),
              ),
            ),
          );
      }),
    ),
  );

  createHistoryRequestPrivate$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromHistoryRequestPrivateActions.createHistoryRequestPrivate),
      switchMap((action) =>
        this.historyRequestPrivateService.createItem(action.create).pipe(
          map((res: any) => {
            return fromHistoryRequestPrivateActions.createHistoryRequestPrivateSuccess(
              {
                data: res.data,
              },
            );
          }),
          catchError((error) => {
            return of(
              fromHistoryRequestPrivateActions.createHistoryRequestPrivateFailure(
                {
                  error: {
                    name: this.name,
                    error: true,
                    message: error.message,
                  },
                },
              ),
            );
          }),
        ),
      ),
    ),
  );

  constructor(
    private actions$: Actions,
    private historyRequestPrivateService: HistoryRequestPrivateService,
    private paginationService: PaginationService,
  ) {}

  loadAllSuccess(data: any, pagination: any): any {
    return fromHistoryRequestPrivateActions.loadAllHistoryRequestPrivateSuccess(
      {
        data,
        pagination,
      },
    );
  }

  loadAllFailure(error: string): any {
    return fromHistoryRequestPrivateActions.loadAllHistoryRequestPrivateFailure(
      {
        error: {
          name: this.name,
          error: true,
          message: error,
        },
      },
    );
  }
}
