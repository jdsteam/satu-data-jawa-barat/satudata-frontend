import { createAction, props } from '@ngrx/store';
import { HistoryRequestPrivateData } from '@models-v3';

export enum HistoryRequestPrivateActionTypes {
  LoadAllHistoryRequestPrivate = '[HISTORY REQUEST PRIVATE] Load All History Request Private',
  LoadAllHistoryRequestPrivateSuccess = '[HISTORY REQUEST PRIVATE] Load All History Request Private Success',
  LoadAllHistoryRequestPrivateFailure = '[HISTORY REQUEST PRIVATE] Load All History Request Private Failure',
  LoadHistoryRequestPrivate = '[HISTORY REQUEST PRIVATE] Load History Request Private',
  LoadHistoryRequestPrivateSuccess = '[HISTORY REQUEST PRIVATE] Load History Request Private Success',
  LoadHistoryRequestPrivateFailure = '[HISTORY REQUEST PRIVATE] Load History Request Private Failure',
  CreateHistoryRequestPrivate = '[HISTORY REQUEST PRIVATE] Create History Request Private',
  CreateHistoryRequestPrivateSuccess = '[HISTORY REQUEST PRIVATE] Create History Request Private Success',
  CreateHistoryRequestPrivateFailure = '[HISTORY REQUEST PRIVATE] Create History Request Private Failure',
  ClearHistoryRequestPrivate = '[HISTORY REQUEST PRIVATE] Clear History Request Private',
}

// Load
export const loadAllHistoryRequestPrivate = createAction(
  HistoryRequestPrivateActionTypes.LoadAllHistoryRequestPrivate,
  props<{ params: any; pagination: boolean; infinite: boolean }>(),
);

export const loadAllHistoryRequestPrivateSuccess = createAction(
  HistoryRequestPrivateActionTypes.LoadAllHistoryRequestPrivateSuccess,
  props<{ data: HistoryRequestPrivateData[]; pagination: any }>(),
);

export const loadAllHistoryRequestPrivateFailure = createAction(
  HistoryRequestPrivateActionTypes.LoadAllHistoryRequestPrivateFailure,
  props<{ error: Error | any }>(),
);

// Load Single
export const loadHistoryRequestPrivate = createAction(
  HistoryRequestPrivateActionTypes.LoadHistoryRequestPrivate,
  props<{ id: number; params: any }>(),
);

export const loadHistoryRequestPrivateSuccess = createAction(
  HistoryRequestPrivateActionTypes.LoadHistoryRequestPrivateSuccess,
  props<{ data: HistoryRequestPrivateData }>(),
);

export const loadHistoryRequestPrivateFailure = createAction(
  HistoryRequestPrivateActionTypes.LoadHistoryRequestPrivateFailure,
  props<{ error: Error | any }>(),
);

// Create
export const createHistoryRequestPrivate = createAction(
  HistoryRequestPrivateActionTypes.CreateHistoryRequestPrivate,
  props<{ create: any }>(),
);

export const createHistoryRequestPrivateSuccess = createAction(
  HistoryRequestPrivateActionTypes.CreateHistoryRequestPrivateSuccess,
  props<{ data: HistoryRequestPrivateData }>(),
);

export const createHistoryRequestPrivateFailure = createAction(
  HistoryRequestPrivateActionTypes.CreateHistoryRequestPrivateFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearHistoryRequestPrivate = createAction(
  HistoryRequestPrivateActionTypes.ClearHistoryRequestPrivate,
);

export const fromHistoryRequestPrivateActions = {
  loadAllHistoryRequestPrivate,
  loadAllHistoryRequestPrivateSuccess,
  loadAllHistoryRequestPrivateFailure,
  loadHistoryRequestPrivate,
  loadHistoryRequestPrivateSuccess,
  loadHistoryRequestPrivateFailure,
  createHistoryRequestPrivate,
  createHistoryRequestPrivateSuccess,
  createHistoryRequestPrivateFailure,
  clearHistoryRequestPrivate,
};
