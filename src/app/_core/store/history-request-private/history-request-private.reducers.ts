import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { HistoryRequestPrivateData } from '@models-v3';
import { fromHistoryRequestPrivateActions } from '@store-v3/history-request-private/history-request-private.actions';

export const ENTITY_FEATURE_KEY = 'historyRequestPrivateV3';

export interface State extends EntityState<HistoryRequestPrivateData> {
  isLoadingList: boolean;
  isLoadingCreate: boolean;
  isLoadingRead: boolean;
  isLoadingUpdate: boolean;
  isLoadingDelete: boolean;
  isLoadingCounter: boolean;
  metadata: any;
  pagination: any;
  error?: Error | any;
}

export const adapter: EntityAdapter<HistoryRequestPrivateData> =
  createEntityAdapter<HistoryRequestPrivateData>({
    selectId: (item) => item.id,
    // selectId: (item) => String(item.title),
  });

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  isLoadingList: false,
  isLoadingCreate: false,
  isLoadingRead: false,
  isLoadingUpdate: false,
  isLoadingDelete: false,
  isLoadingCounter: false,
  metadata: null,
  pagination: null,
  error: null,
});

const reducer = createReducer(
  initialState,

  // Load All
  on(fromHistoryRequestPrivateActions.loadAllHistoryRequestPrivate, (state) => {
    return {
      ...state,
      isLoadingList: true,
      error: null,
    };
  }),
  on(
    fromHistoryRequestPrivateActions.loadAllHistoryRequestPrivateSuccess,
    (state, { data, pagination }) => {
      if (!pagination.infinite) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          error: { error: false },
        });
      }

      if (pagination.current_page === 1) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          error: { error: false },
        });
      }

      return adapter.addMany(data, {
        ...state,
        isLoadingList: false,
        pagination,
        error: { error: false },
      });
    },
  ),
  on(
    fromHistoryRequestPrivateActions.loadAllHistoryRequestPrivateFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingList: false,
        error,
      };
    },
  ),

  // Load Single
  on(fromHistoryRequestPrivateActions.loadHistoryRequestPrivate, (state) => {
    return {
      ...state,
      isLoadingRead: true,
      error: null,
    };
  }),
  on(
    fromHistoryRequestPrivateActions.loadHistoryRequestPrivateSuccess,
    (state, { data }) => {
      return adapter.addOne(data, {
        ...state,
        isLoadingRead: false,
        error: { error: false },
      });
    },
  ),
  on(
    fromHistoryRequestPrivateActions.loadHistoryRequestPrivateFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingRead: false,
        error,
      };
    },
  ),

  // Create
  on(fromHistoryRequestPrivateActions.createHistoryRequestPrivate, (state) => {
    return {
      ...state,
      isLoadingCreate: true,
      error: null,
    };
  }),
  on(
    fromHistoryRequestPrivateActions.createHistoryRequestPrivateSuccess,
    (state, { data }) => {
      return adapter.addOne(data, {
        ...state,
        isLoadingCreate: false,
        error: { error: false },
      });
    },
  ),
  on(
    fromHistoryRequestPrivateActions.createHistoryRequestPrivateFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingCreate: false,
        error,
      };
    },
  ),

  // Clear
  on(fromHistoryRequestPrivateActions.clearHistoryRequestPrivate, (state) => {
    return adapter.removeAll({
      ...state,
      isLoadingList: false,
      pagination: { empty: true },
      error: { error: false },
    });
  }),
);

export function HistoryRequestPrivateReducer(
  state: State | undefined,
  action: Action,
): any {
  return reducer(state, action);
}
