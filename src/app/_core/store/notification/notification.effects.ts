import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';

import { fromNotificationActions } from '@store-v3/notification/notification.actions';
import { NotificationService, PaginationService } from '@services-v3';
import { handlePromise } from '@helpers-v3';

@Injectable()
export class NotificationEffects {
  name = 'Notification';

  loadAllNotification$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromNotificationActions.loadAllNotification),
      switchMap(async (action) => {
        const { data: result, error: resultErr } = await handlePromise(
          this.notificationService.getList(action.params).toPromise(),
        );

        if (resultErr) {
          return this.loadAllFailure(resultErr.message);
        }

        if (result.error === 1) {
          if (result.message === 'Data not found') {
            return fromNotificationActions.clearNotification();
          }

          return this.loadAllFailure(result.message);
        }

        let pagination = { empty: true, infinite: action.infinite };
        if (action.pagination) {
          const { data: total, error: totalErr } = await handlePromise(
            this.notificationService.getTotal(action.params).toPromise(),
          );

          if (totalErr) {
            return this.loadAllFailure(totalErr.message);
          }

          const resultPagination = this.paginationService.getPagination(
            total.data.count,
            action.params.skip,
            action.params.limit,
          );

          pagination = {
            empty: false,
            infinite: action.infinite,
            ...resultPagination,
          };
        }

        return this.loadAllSuccess(result.data, pagination);
      }),
    ),
  );

  loadNotification$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromNotificationActions.loadNotification),
      switchMap((action) => {
        return this.notificationService
          .getSingle(action.id, action.params)
          .pipe(
            map((result: any) =>
              fromNotificationActions.loadNotificationSuccess({
                data: result.data,
              }),
            ),
            catchError((error: HttpErrorResponse) =>
              of(
                fromNotificationActions.loadNotificationFailure({
                  error: {
                    name: this.name,
                    error: !error.ok,
                    message: error.message,
                  },
                }),
              ),
            ),
          );
      }),
    ),
  );

  constructor(
    private actions$: Actions,
    private notificationService: NotificationService,
    private paginationService: PaginationService,
  ) {}

  loadAllSuccess(data: any, pagination: any): any {
    return fromNotificationActions.loadAllNotificationSuccess({
      data,
      pagination,
    });
  }

  loadAllFailure(error: string): any {
    return fromNotificationActions.loadAllNotificationFailure({
      error: {
        name: this.name,
        error: true,
        message: error,
      },
    });
  }
}
