import { createAction, props } from '@ngrx/store';
import { NotificationData } from '@models-v3';

export enum NotificationActionTypes {
  LoadAllNotification = '[NOTIFICATION] Load All Notification',
  LoadAllNotificationSuccess = '[NOTIFICATION] Load All Notification Success',
  LoadAllNotificationFailure = '[NOTIFICATION] Load All Notification Failure',
  LoadNotification = '[NOTIFICATION] Load Notification',
  LoadNotificationSuccess = '[NOTIFICATION] Load Notification Success',
  LoadNotificationFailure = '[NOTIFICATION] Load Notification Failure',
  ClearNotification = '[NOTIFICATION] Clear Notification',
}

// Load All
export const loadAllNotification = createAction(
  NotificationActionTypes.LoadAllNotification,
  props<{ params: any; pagination: boolean; infinite: boolean }>(),
);

export const loadAllNotificationSuccess = createAction(
  NotificationActionTypes.LoadAllNotificationSuccess,
  props<{ data: NotificationData[]; pagination: any }>(),
);

export const loadAllNotificationFailure = createAction(
  NotificationActionTypes.LoadAllNotificationFailure,
  props<{ error: Error | any }>(),
);

// Load Single
export const loadNotification = createAction(
  NotificationActionTypes.LoadNotification,
  props<{ id: number; params: any }>(),
);

export const loadNotificationSuccess = createAction(
  NotificationActionTypes.LoadNotificationSuccess,
  props<{ data: NotificationData }>(),
);

export const loadNotificationFailure = createAction(
  NotificationActionTypes.LoadNotificationFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearNotification = createAction(
  NotificationActionTypes.ClearNotification,
);

export const fromNotificationActions = {
  loadAllNotification,
  loadAllNotificationSuccess,
  loadAllNotificationFailure,
  loadNotification,
  loadNotificationSuccess,
  loadNotificationFailure,
  clearNotification,
};
