import { createAction, props } from '@ngrx/store';
import { LogIndicatorData } from '@models-v3';

export enum LogIndicatorActionTypes {
  LoadAllLogIndicator = '[LOG INDICATOR] Load All Log Indicator',
  LoadAllLogIndicatorSuccess = '[LOG INDICATOR] Load All Log Indicator Success',
  LoadAllLogIndicatorFailure = '[LOG INDICATOR] Load All Log Indicator Failure',
  ClearLogIndicator = '[LOG INDICATOR] Clear Log Indicator',
}

// Load All
export const loadAllLogIndicator = createAction(
  LogIndicatorActionTypes.LoadAllLogIndicator,
  props<{ params: any; pagination: boolean; infinite: boolean }>(),
);

export const loadAllLogIndicatorSuccess = createAction(
  LogIndicatorActionTypes.LoadAllLogIndicatorSuccess,
  props<{ data: LogIndicatorData[]; pagination: any }>(),
);

export const loadAllLogIndicatorFailure = createAction(
  LogIndicatorActionTypes.LoadAllLogIndicatorFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearLogIndicator = createAction(
  LogIndicatorActionTypes.ClearLogIndicator,
);

export const fromLogIndicatorActions = {
  loadAllLogIndicator,
  loadAllLogIndicatorSuccess,
  loadAllLogIndicatorFailure,
  clearLogIndicator,
};
