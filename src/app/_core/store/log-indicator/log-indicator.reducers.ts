import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { LogIndicatorData } from '@models-v3';
import { fromLogIndicatorActions } from '@store-v3/log-indicator/log-indicator.actions';

export const ENTITY_FEATURE_KEY = 'logIndicatorV3';

export interface State extends EntityState<LogIndicatorData> {
  isLoadingList: boolean;
  isLoadingCreate: boolean;
  isLoadingRead: boolean;
  isLoadingUpdate: boolean;
  isLoadingDelete: boolean;
  metadata: any;
  pagination: any;
  error?: Error | any;
}

export const adapter: EntityAdapter<LogIndicatorData> =
  createEntityAdapter<LogIndicatorData>({
    selectId: (item) => String(item.id),
  });

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  isLoadingList: false,
  isLoadingCreate: true,
  isLoadingRead: true,
  isLoadingUpdate: true,
  isLoadingDelete: true,
  metadata: null,
  pagination: null,
  error: null,
});

const reducer = createReducer(
  initialState,

  // Load All
  on(fromLogIndicatorActions.loadAllLogIndicator, (state) => {
    return {
      ...state,
      isLoadingList: true,
      error: null,
    };
  }),
  on(
    fromLogIndicatorActions.loadAllLogIndicatorSuccess,
    (state, { data, pagination }) => {
      if (!pagination.infinite) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          error: { error: false },
        });
      }

      if (pagination.current_page === 1) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          error: { error: false },
        });
      }

      return adapter.addMany(data, {
        ...state,
        isLoadingList: false,
        pagination,
        error: { error: false },
      });
    },
  ),
  on(fromLogIndicatorActions.loadAllLogIndicatorFailure, (state, { error }) => {
    return {
      ...state,
      isLoadingList: false,
      error,
    };
  }),

  // Clear
  on(fromLogIndicatorActions.clearLogIndicator, (state) => {
    return adapter.removeAll({
      ...state,
      isLoadingList: false,
      pagination: { empty: true },
      error: { error: false },
    });
  }),
);

export function LogIndicatorReducer(
  state: State | undefined,
  action: Action,
): any {
  return reducer(state, action);
}
