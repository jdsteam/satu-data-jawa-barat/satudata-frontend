import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { switchMap } from 'rxjs/operators';

import { fromLogIndicatorActions } from '@store-v3/log-indicator/log-indicator.actions';
import { LogIndicatorService } from '@services-v3';
import { handlePromise } from '@helpers-v3';

@Injectable()
export class LogIndicatorEffects {
  name = 'Log Indicator';

  loadAllLogIndicator$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromLogIndicatorActions.loadAllLogIndicator),
      switchMap(async (action) => {
        const { data: result, error: resultErr } = await handlePromise(
          this.logIndicatorService.getList(action.params).toPromise(),
        );

        if (resultErr) {
          return this.loadAllFailure(resultErr.message);
        }

        if (result.error === 1) {
          if (result.message === 'Data not found') {
            return fromLogIndicatorActions.clearLogIndicator();
          }
          return this.loadAllFailure(result.message);
        }

        let pagination = { empty: true, infinite: action.infinite };
        if (action.pagination) {
          pagination = {
            empty: false,
            infinite: action.infinite,
            ...result.pagination,
          };
        }

        return this.loadAllSuccess(result.data, pagination);
      }),
    ),
  );

  constructor(
    private actions$: Actions,
    private logIndicatorService: LogIndicatorService,
  ) {}

  loadAllSuccess(data: any, pagination: any): any {
    return fromLogIndicatorActions.loadAllLogIndicatorSuccess({
      data,
      pagination,
    });
  }

  loadAllFailure(error: string): any {
    return fromLogIndicatorActions.loadAllLogIndicatorFailure({
      error: {
        name: this.name,
        error: true,
        message: error,
      },
    });
  }
}
