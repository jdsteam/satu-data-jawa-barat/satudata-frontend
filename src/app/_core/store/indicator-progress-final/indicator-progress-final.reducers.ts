import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { IndicatorProgressData } from '@models-v3';
import { fromIndicatorProgressFinalActions } from '@store-v3/indicator-progress-final/indicator-progress-final.actions';

export const ENTITY_FEATURE_KEY = 'indicatorProgressFinalV3';

export interface State extends EntityState<IndicatorProgressData> {
  isLoadingList: boolean;
  isLoadingCreate: boolean;
  isLoadingRead: boolean;
  isLoadingUpdate: boolean;
  isLoadingDelete: boolean;
  metadata: any;
  pagination: any;
  error?: Error | any;
}

export const adapter: EntityAdapter<IndicatorProgressData> =
  createEntityAdapter<IndicatorProgressData>({
    selectId: (item) => item.id,
  });

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  isLoadingList: false,
  isLoadingCreate: false,
  isLoadingRead: false,
  isLoadingUpdate: false,
  isLoadingDelete: false,
  metadata: null,
  pagination: null,
  error: null,
});

const reducer = createReducer(
  initialState,

  // Load All
  on(
    fromIndicatorProgressFinalActions.loadAllIndicatorProgressFinal,
    (state) => {
      return {
        ...state,
        isLoadingList: true,
      };
    },
  ),
  on(
    fromIndicatorProgressFinalActions.loadAllIndicatorProgressFinalSuccess,
    (state, { data, pagination }) => {
      if (!pagination.infinite) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          error: { error: false },
        });
      }

      if (pagination.current_page === 1) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          error: { error: false },
        });
      }

      return adapter.addMany(data, {
        ...state,
        isLoadingList: false,
        pagination,
        error: { error: false },
      });
    },
  ),
  on(
    fromIndicatorProgressFinalActions.loadAllIndicatorProgressFinalFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingList: false,
        error,
      };
    },
  ),

  // Load Single
  on(fromIndicatorProgressFinalActions.loadIndicatorProgressFinal, (state) => {
    return {
      ...state,
      isLoadingRead: true,
    };
  }),
  on(
    fromIndicatorProgressFinalActions.loadIndicatorProgressFinalSuccess,
    (state, { data }) => {
      return adapter.addOne(data, {
        ...state,
        isLoadingRead: false,
        error: { error: false },
      });
    },
  ),
  on(
    fromIndicatorProgressFinalActions.loadIndicatorProgressFinalFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingRead: false,
        error,
      };
    },
  ),

  // Create
  on(
    fromIndicatorProgressFinalActions.createIndicatorProgressFinal,
    (state) => {
      return {
        ...state,
        isLoadingCreate: true,
      };
    },
  ),
  on(
    fromIndicatorProgressFinalActions.createIndicatorProgressFinalSuccess,
    (state, { data }) => {
      return adapter.addMany(data, {
        ...state,
        isLoadingCreate: false,
        error: { error: false },
      });
    },
  ),
  on(
    fromIndicatorProgressFinalActions.createIndicatorProgressFinalFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingCreate: false,
        error,
      };
    },
  ),

  // Update Bulk
  on(
    fromIndicatorProgressFinalActions.updateIndicatorProgressFinal,
    (state) => {
      return {
        ...state,
        isLoadingUpdate: true,
      };
    },
  ),
  on(
    fromIndicatorProgressFinalActions.updateIndicatorProgressFinalSuccess,
    (state, { data }) => {
      return adapter.updateOne(
        { id: data.id, changes: data },
        {
          ...state,
          isLoadingUpdate: false,
          error: { error: false },
        },
      );
    },
  ),
  on(
    fromIndicatorProgressFinalActions.updateIndicatorProgressFinalFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingUpdate: false,
        error,
      };
    },
  ),

  // Clear
  on(fromIndicatorProgressFinalActions.clearIndicatorProgressFinal, (state) => {
    return adapter.removeAll({
      ...state,
      isLoadingList: false,
      pagination: { empty: true },
      error: { error: false },
    });
  }),
);

export function IndicatorProgressFinalReducer(
  state: State | undefined,
  action: Action,
): any {
  return reducer(state, action);
}
