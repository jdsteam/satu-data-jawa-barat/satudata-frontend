import { createAction, props } from '@ngrx/store';
import { IndicatorProgressData } from '@models-v3';

export enum IndicatorProgressFinalActionTypes {
  LoadAllIndicatorProgressFinal = '[INDICATOR PROGRESS FINAL] Load All Indicator Progress Final',
  LoadAllIndicatorProgressFinalSuccess = '[INDICATOR PROGRESS FINAL] Load All Indicator Progress Final Success',
  LoadAllIndicatorProgressFinalFailure = '[INDICATOR PROGRESS FINAL] Load All Indicator Progress Final Failure',
  LoadIndicatorProgressFinal = '[INDICATOR PROGRESS FINAL] Load Indicator Progress Final',
  LoadIndicatorProgressFinalSuccess = '[INDICATOR PROGRESS FINAL] Load Indicator Progress Final Success',
  LoadIndicatorProgressFinalFailure = '[INDICATOR PROGRESS FINAL] Load Indicator Progress Final Failure',
  CreateIndicatorProgressFinal = '[INDICATOR PROGRESS FINAL] Create Indicator Progress Final',
  CreateIndicatorProgressFinalSuccess = '[INDICATOR PROGRESS FINAL] Create Indicator Progress Final Success',
  CreateIndicatorProgressFinalFailure = '[INDICATOR PROGRESS FINAL] Create Indicator Progress Final Failure',
  UpdateIndicatorProgressFinal = '[INDICATOR PROGRESS FINAL] Update Indicator Progress Final',
  UpdateIndicatorProgressFinalSuccess = '[INDICATOR PROGRESS FINAL] Update Indicator Progress Final Success',
  UpdateIndicatorProgressFinalFailure = '[INDICATOR PROGRESS FINAL] Update Indicator Progress Final Failure',
  ClearIndicatorProgressFinal = '[INDICATOR PROGRESS FINAL] Clear Indicator Progress Final',
}

// Load All
export const loadAllIndicatorProgressFinal = createAction(
  IndicatorProgressFinalActionTypes.LoadAllIndicatorProgressFinal,
  props<{ params: any; pagination: boolean; infinite: boolean }>(),
);

export const loadAllIndicatorProgressFinalSuccess = createAction(
  IndicatorProgressFinalActionTypes.LoadAllIndicatorProgressFinalSuccess,
  props<{ data: IndicatorProgressData[]; pagination: any }>(),
);

export const loadAllIndicatorProgressFinalFailure = createAction(
  IndicatorProgressFinalActionTypes.LoadAllIndicatorProgressFinalFailure,
  props<{ error: Error | any }>(),
);

// Load Single
export const loadIndicatorProgressFinal = createAction(
  IndicatorProgressFinalActionTypes.LoadIndicatorProgressFinal,
  props<{ id: number; params: any }>(),
);

export const loadIndicatorProgressFinalSuccess = createAction(
  IndicatorProgressFinalActionTypes.LoadIndicatorProgressFinalSuccess,
  props<{ data: IndicatorProgressData }>(),
);

export const loadIndicatorProgressFinalFailure = createAction(
  IndicatorProgressFinalActionTypes.LoadIndicatorProgressFinalFailure,
  props<{ error: Error | any }>(),
);

// Create
export const createIndicatorProgressFinal = createAction(
  IndicatorProgressFinalActionTypes.CreateIndicatorProgressFinal,
  props<{ create: any; indicator: any }>(),
);

export const createIndicatorProgressFinalSuccess = createAction(
  IndicatorProgressFinalActionTypes.CreateIndicatorProgressFinalSuccess,
  props<{ data: IndicatorProgressData[] }>(),
);

export const createIndicatorProgressFinalFailure = createAction(
  IndicatorProgressFinalActionTypes.CreateIndicatorProgressFinalFailure,
  props<{ error: Error | any }>(),
);

// Update
export const updateIndicatorProgressFinal = createAction(
  IndicatorProgressFinalActionTypes.UpdateIndicatorProgressFinal,
  props<{ indicatorId: number; update: any; params: any }>(),
);

export const updateIndicatorProgressFinalSuccess = createAction(
  IndicatorProgressFinalActionTypes.UpdateIndicatorProgressFinalSuccess,
  props<{ data: IndicatorProgressData }>(),
);

export const updateIndicatorProgressFinalFailure = createAction(
  IndicatorProgressFinalActionTypes.UpdateIndicatorProgressFinalFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearIndicatorProgressFinal = createAction(
  IndicatorProgressFinalActionTypes.ClearIndicatorProgressFinal,
);

export const fromIndicatorProgressFinalActions = {
  loadAllIndicatorProgressFinal,
  loadAllIndicatorProgressFinalSuccess,
  loadAllIndicatorProgressFinalFailure,
  loadIndicatorProgressFinal,
  loadIndicatorProgressFinalSuccess,
  loadIndicatorProgressFinalFailure,
  createIndicatorProgressFinal,
  createIndicatorProgressFinalSuccess,
  createIndicatorProgressFinalFailure,
  updateIndicatorProgressFinal,
  updateIndicatorProgressFinalSuccess,
  updateIndicatorProgressFinalFailure,
  clearIndicatorProgressFinal,
};
