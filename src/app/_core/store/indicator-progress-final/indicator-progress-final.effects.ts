import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';

import { fromIndicatorProgressFinalActions } from '@store-v3/indicator-progress-final/indicator-progress-final.actions';
import { IndicatorProgressService, PaginationService } from '@services-v3';

import * as _ from 'lodash';
import { handlePromise } from '@helpers-v3';

@Injectable()
export class IndicatorProgressFinalEffects {
  name = 'Indicator Progress Final';

  loadAllIndicatorProgressFinal$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromIndicatorProgressFinalActions.loadAllIndicatorProgressFinal),
      switchMap(async (action) => {
        const { data: result, error: resultErr } = await handlePromise(
          this.indicatorProgressService.getList(action.params).toPromise(),
        );

        if (resultErr) {
          return this.loadAllFailure(resultErr.message);
        }

        if (result.error === 1) {
          if (result.message === 'Data not found') {
            return fromIndicatorProgressFinalActions.clearIndicatorProgressFinal();
          }

          return this.loadAllFailure(result.message);
        }

        let pagination = { empty: true, infinite: action.infinite };
        if (action.pagination) {
          const resultPagination = this.paginationService.getPagination(
            result.meta.total_record,
            action.params.skip,
            action.params.limit,
          );

          pagination = {
            empty: false,
            infinite: action.infinite,
            ...resultPagination,
          };
        }

        return this.loadAllSuccess(result.data, pagination);
      }),
    ),
  );

  loadIndicatorProgressFinal$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromIndicatorProgressFinalActions.loadIndicatorProgressFinal),
      switchMap((action) => {
        return this.indicatorProgressService
          .getSingle(action.id, action.params)
          .pipe(
            map((result: any) =>
              fromIndicatorProgressFinalActions.loadIndicatorProgressFinalSuccess(
                {
                  data: result.data,
                },
              ),
            ),
            catchError((error: HttpErrorResponse) =>
              of(
                fromIndicatorProgressFinalActions.loadIndicatorProgressFinalFailure(
                  {
                    error: {
                      name: this.name,
                      error: !error.ok,
                      message: error.message,
                    },
                  },
                ),
              ),
            ),
          );
      }),
    ),
  );

  createIndicatorProgress$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromIndicatorProgressFinalActions.createIndicatorProgressFinal),
      switchMap((action) => {
        const indicatorId = action.indicator.id;
        const indexHelper = 99;

        const bodyProgress = [];
        _.map(action.create, (result, index) => {
          bodyProgress.push({
            indikator_id: indicatorId,
            year: result.year,
            target: result.target || null,
            capaian: result.capaian || null,
            notes: result.notes,
            index: index + indexHelper,
          });
        });

        return this.indicatorProgressService.createItem(bodyProgress).pipe(
          map((res: any) => {
            return fromIndicatorProgressFinalActions.createIndicatorProgressFinalSuccess(
              {
                data: res.data,
              },
            );
          }),
          catchError((error) => {
            return of(
              fromIndicatorProgressFinalActions.createIndicatorProgressFinalFailure(
                {
                  error: {
                    name: this.name,
                    error: !error.ok,
                    message: error.message,
                  },
                },
              ),
            );
          }),
        );
      }),
    ),
  );

  updateIndicatorProgressFinal$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromIndicatorProgressFinalActions.updateIndicatorProgressFinal),
      switchMap((action) =>
        this.indicatorProgressService
          .updateItemBulk(action.indicatorId, action.update, action.params)
          .pipe(
            map((res: any) => {
              return fromIndicatorProgressFinalActions.updateIndicatorProgressFinalSuccess(
                {
                  data: res.data,
                },
              );
            }),
            catchError((error) => {
              return of(
                fromIndicatorProgressFinalActions.updateIndicatorProgressFinalFailure(
                  {
                    error: {
                      name: this.name,
                      error: !error.ok,
                      message: error.message,
                    },
                  },
                ),
              );
            }),
          ),
      ),
    ),
  );

  constructor(
    private actions$: Actions,
    private indicatorProgressService: IndicatorProgressService,
    private paginationService: PaginationService,
  ) {}

  loadAllSuccess(data: any, pagination: any): any {
    return fromIndicatorProgressFinalActions.loadAllIndicatorProgressFinalSuccess(
      {
        data,
        pagination,
      },
    );
  }

  loadAllFailure(error: string): any {
    return fromIndicatorProgressFinalActions.loadAllIndicatorProgressFinalFailure(
      {
        error: {
          name: this.name,
          error: true,
          message: error,
        },
      },
    );
  }
}
