import { createAction, props } from '@ngrx/store';
import { LogDownloadDatasetData } from '@models-v3';

export enum LogDownloadDatasetActionTypes {
  LoadAllLogDownloadDataset = '[LOG DOWNLOAD DATASET] Load All Log Download Dataset',
  LoadAllLogDownloadDatasetSuccess = '[LOG DOWNLOAD DATASET] Load All Log Download Dataset Success',
  LoadAllLogDownloadDatasetFailure = '[LOG DOWNLOAD DATASET] Load All Log Download Dataset Failure',
  ClearLogDownloadDataset = '[LOG DOWNLOAD DATASET] Clear Log Download Dataset',
}

// Load All
export const loadAllLogDownloadDataset = createAction(
  LogDownloadDatasetActionTypes.LoadAllLogDownloadDataset,
  props<{ params: any; pagination: boolean; infinite: boolean }>(),
);

export const loadAllLogDownloadDatasetSuccess = createAction(
  LogDownloadDatasetActionTypes.LoadAllLogDownloadDatasetSuccess,
  props<{ data: LogDownloadDatasetData[]; pagination: any }>(),
);

export const loadAllLogDownloadDatasetFailure = createAction(
  LogDownloadDatasetActionTypes.LoadAllLogDownloadDatasetFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearLogDownloadDataset = createAction(
  LogDownloadDatasetActionTypes.ClearLogDownloadDataset,
);

export const fromLogDownloadDatasetActions = {
  loadAllLogDownloadDataset,
  loadAllLogDownloadDatasetSuccess,
  loadAllLogDownloadDatasetFailure,
  clearLogDownloadDataset,
};
