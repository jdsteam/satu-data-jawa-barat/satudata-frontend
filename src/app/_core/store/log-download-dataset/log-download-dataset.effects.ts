import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { switchMap } from 'rxjs/operators';

import { fromLogDownloadDatasetActions } from '@store-v3/log-download-dataset/log-download-dataset.actions';
import { LogDownloadDatasetService } from '@services-v3';
import { handlePromise } from '@helpers-v3';

@Injectable()
export class LogDownloadDatasetEffects {
  name = 'Log Download Dataset';

  loadAllLogDownloadDataset$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromLogDownloadDatasetActions.loadAllLogDownloadDataset),
      switchMap(async (action) => {
        const { data: result, error: resultErr } = await handlePromise(
          this.logDownloadDatasetService.getList(action.params).toPromise(),
        );

        if (resultErr) {
          return this.loadAllFailure(resultErr.message);
        }

        if (result.error === 1) {
          if (result.message === 'Data not found') {
            return fromLogDownloadDatasetActions.clearLogDownloadDataset();
          }
          return this.loadAllFailure(result.message);
        }

        let pagination = { empty: true, infinite: action.infinite };
        if (action.pagination) {
          pagination = {
            empty: false,
            infinite: action.infinite,
            ...result.pagination,
          };
        }

        return this.loadAllSuccess(result.data, pagination);
      }),
    ),
  );

  constructor(
    private actions$: Actions,
    private logDownloadDatasetService: LogDownloadDatasetService,
  ) {}

  loadAllSuccess(data: any, pagination: any): any {
    return fromLogDownloadDatasetActions.loadAllLogDownloadDatasetSuccess({
      data,
      pagination,
    });
  }

  loadAllFailure(error: string): any {
    return fromLogDownloadDatasetActions.loadAllLogDownloadDatasetFailure({
      error: {
        name: this.name,
        error: true,
        message: error,
      },
    });
  }
}
