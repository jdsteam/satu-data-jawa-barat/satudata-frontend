import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { switchMap } from 'rxjs/operators';

import { fromLogLoginActions } from '@store-v3/log-login/log-login.actions';
import { LogLoginService } from '@services-v3';
import { handlePromise } from '@helpers-v3';

@Injectable()
export class LogLoginEffects {
  name = 'Log Login';

  loadAllLogLogin$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromLogLoginActions.loadAllLogLogin),
      switchMap(async (action) => {
        const { data: result, error: resultErr } = await handlePromise(
          this.logLoginService.getList(action.params).toPromise(),
        );

        if (resultErr) {
          return this.loadAllFailure(resultErr.message);
        }

        if (result.error === 1) {
          if (result.message === 'Data not found') {
            return fromLogLoginActions.clearLogLogin();
          }
          return this.loadAllFailure(result.message);
        }

        let pagination = { empty: true };
        if (action.pagination) {
          pagination = { empty: false, ...result.pagination };
        }

        return this.loadAllSuccess(result.data, pagination);
      }),
    ),
  );

  constructor(
    private actions$: Actions,
    private logLoginService: LogLoginService,
  ) {}

  loadAllSuccess(data: any, pagination: any): any {
    return fromLogLoginActions.loadAllLogLoginSuccess({
      data,
      pagination,
    });
  }

  loadAllFailure(error: string): any {
    return fromLogLoginActions.loadAllLogLoginFailure({
      error: {
        name: this.name,
        error: true,
        message: error,
      },
    });
  }
}
