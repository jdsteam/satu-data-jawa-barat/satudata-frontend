import { createAction, props } from '@ngrx/store';
import { LogLoginData } from '@models-v3';

export enum LogLoginActionTypes {
  LoadAllLogLogin = '[LOG LOGIN] Load All Log Login',
  LoadAllLogLoginSuccess = '[LOG LOGIN] Load All Log Login Success',
  LoadAllLogLoginFailure = '[LOG LOGIN] Load All Log Login Failure',
  ClearLogLogin = '[LOG LOGIN] Clear Log Login',
}

// Load All
export const loadAllLogLogin = createAction(
  LogLoginActionTypes.LoadAllLogLogin,
  props<{ params: any; pagination: boolean; infinite: boolean }>(),
);

export const loadAllLogLoginSuccess = createAction(
  LogLoginActionTypes.LoadAllLogLoginSuccess,
  props<{ data: LogLoginData[]; pagination: any }>(),
);

export const loadAllLogLoginFailure = createAction(
  LogLoginActionTypes.LoadAllLogLoginFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearLogLogin = createAction(LogLoginActionTypes.ClearLogLogin);

export const fromLogLoginActions = {
  loadAllLogLogin,
  loadAllLogLoginSuccess,
  loadAllLogLoginFailure,
  clearLogLogin,
};
