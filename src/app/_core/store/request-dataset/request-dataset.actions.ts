import { createAction, props } from '@ngrx/store';
import { RequestDatasetData } from '@models-v3';

export enum RequestDatasetActionTypes {
  LoadAllRequestDataset = '[REQUEST DATASET] Load All Request Dataset',
  LoadAllRequestDatasetSuccess = '[REQUEST DATASET] Load All Request Dataset Success',
  LoadAllRequestDatasetFailure = '[REQUEST DATASET] Load All Request Dataset Failure',
  ClearRequestDataset = '[REQUEST DATASET] Clear Request Dataset',
}

// Load All
export const loadAllRequestDataset = createAction(
  RequestDatasetActionTypes.LoadAllRequestDataset,
  props<{ params: any; pagination: boolean }>(),
);

export const loadAllRequestDatasetSuccess = createAction(
  RequestDatasetActionTypes.LoadAllRequestDatasetSuccess,
  props<{ data: RequestDatasetData[]; pagination: any }>(),
);

export const loadAllRequestDatasetFailure = createAction(
  RequestDatasetActionTypes.LoadAllRequestDatasetFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearRequestDataset = createAction(
  RequestDatasetActionTypes.ClearRequestDataset,
);

export const fromRequestDatasetActions = {
  loadAllRequestDataset,
  loadAllRequestDatasetSuccess,
  loadAllRequestDatasetFailure,
  clearRequestDataset,
};
