import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { switchMap } from 'rxjs/operators';

import { fromRequestDatasetActions } from '@store-v3/request-dataset/request-dataset.actions';
import { RequestDatasetService, PaginationService } from '@services-v3';
import { handlePromise } from '@helpers-v3';

@Injectable()
export class RequestDatasetEffects {
  name = 'Request Dataset';

  loadAllRequestDataset$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromRequestDatasetActions.loadAllRequestDataset),
      switchMap(async (action) => {
        const { data: result, error: resultErr } = await handlePromise(
          this.requestDatasetService.getList(action.params).toPromise(),
        );

        if (resultErr) {
          return this.loadAllFailure(resultErr.message);
        }

        if (result.error === 1) {
          if (result.message === 'Data not found') {
            return fromRequestDatasetActions.clearRequestDataset();
          }

          return this.loadAllFailure(result.message);
        }

        let pagination = { empty: true };
        if (action.pagination) {
          const { data: total, error: totalErr } = await handlePromise(
            this.requestDatasetService.getTotal(action.params).toPromise(),
          );

          if (totalErr) {
            return this.loadAllFailure(totalErr.message);
          }

          const resultPagination = this.paginationService.getPagination(
            total.data.count,
            action.params.skip,
            action.params.limit,
          );

          pagination = { empty: false, ...resultPagination };
        }

        return this.loadAllSuccess(result.data, pagination);
      }),
    ),
  );

  constructor(
    private actions$: Actions,
    private requestDatasetService: RequestDatasetService,
    private paginationService: PaginationService,
  ) {}

  loadAllSuccess(data: any, pagination: any): any {
    return fromRequestDatasetActions.loadAllRequestDatasetSuccess({
      data,
      pagination,
    });
  }

  loadAllFailure(error: string): any {
    return fromRequestDatasetActions.loadAllRequestDatasetFailure({
      error: {
        name: this.name,
        error: true,
        message: error,
      },
    });
  }
}
