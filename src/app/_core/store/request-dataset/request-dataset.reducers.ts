import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { RequestDatasetData } from '@models-v3';
import { fromRequestDatasetActions } from '@store-v3/request-dataset/request-dataset.actions';

export const ENTITY_FEATURE_KEY = 'requestDatasetV3';

export interface State extends EntityState<RequestDatasetData> {
  isLoadingList: boolean;
  isLoadingCreate: boolean;
  isLoadingRead: boolean;
  isLoadingUpdate: boolean;
  isLoadingDelete: boolean;
  metadata: any;
  pagination: any;
  error?: Error | any;
}

export const adapter: EntityAdapter<RequestDatasetData> =
  createEntityAdapter<RequestDatasetData>({
    selectId: (item) => item.id,
  });

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  isLoadingList: false,
  isLoadingCreate: true,
  isLoadingRead: true,
  isLoadingUpdate: true,
  isLoadingDelete: true,
  metadata: null,
  pagination: null,
  error: null,
});

const reducer = createReducer(
  initialState,

  // Load All
  on(fromRequestDatasetActions.loadAllRequestDataset, (state) => {
    return {
      ...state,
      isLoadingList: true,
    };
  }),
  on(
    fromRequestDatasetActions.loadAllRequestDatasetSuccess,
    (state, { data, pagination }) => {
      return adapter.setAll(data, {
        ...state,
        isLoadingList: false,
        pagination,
        error: { error: false },
      });
    },
  ),
  on(
    fromRequestDatasetActions.loadAllRequestDatasetFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingList: false,
        error,
      };
    },
  ),

  // Clear
  on(fromRequestDatasetActions.clearRequestDataset, (state) => {
    return adapter.removeAll({
      ...state,
      isLoadingList: false,
      pagination: { empty: true },
      error: { error: false },
    });
  }),
);

export function RequestDatasetReducer(
  state: State | undefined,
  action: Action,
): any {
  return reducer(state, action);
}
