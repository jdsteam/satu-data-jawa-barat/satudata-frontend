import { createAction, props } from '@ngrx/store';
import { DatasetData } from '@models-v3';

export enum DatasetActionTypes {
  LoadAllDataset = '[DATASET] Load All Dataset',
  LoadAllDatasetSuccess = '[DATASET] Load All Dataset Success',
  LoadAllDatasetFailure = '[DATASET] Load All Dataset Failure',
  LoadDataset = '[DATASET] Load Dataset',
  LoadDatasetSuccess = '[DATASET] Load Dataset Success',
  LoadDatasetFailure = '[DATASET] Load Dataset Failure',
  LoadDatasetByTitle = '[DATASET] Load Dataset By Title',
  LoadDatasetByTitleSuccess = '[DATASET] Load Dataset By Title Success',
  LoadDatasetByTitleFailure = '[DATASET] Load Dataset By Title Failure',
  CreateDataset = '[DATASET] Create Dataset',
  CreateDatasetSuccess = '[DATASET] Create Dataset Success',
  CreateDatasetFailure = '[DATASET] Create Dataset Failure',
  UpdateDataset = '[DATASET] Update Dataset',
  UpdateDatasetSuccess = '[DATASET] Update Dataset Success',
  UpdateDatasetFailure = '[DATASET] Update Dataset Failure',
  CounterDataset = '[DATASET] Counter Dataset',
  CounterDatasetSuccess = '[DATASET] Counter Dataset Success',
  CounterDatasetFailure = '[DATASET] Counter Dataset Failure',
  CounterDatasetByTitle = '[DATASET] Counter Dataset By Title',
  CounterDatasetByTitleSuccess = '[DATASET] Counter Dataset By Title Success',
  CounterDatasetByTitleFailure = '[DATASET] Counter Dataset By Title Failure',
  ClearDataset = '[DATASET] Clear Dataset',
}

// Load
export const loadAllDataset = createAction(
  DatasetActionTypes.LoadAllDataset,
  props<{ params: any; pagination: boolean; infinite: boolean }>(),
);

export const loadAllDatasetSuccess = createAction(
  DatasetActionTypes.LoadAllDatasetSuccess,
  props<{ data: DatasetData[]; pagination: any; suggestion: any }>(),
);

export const loadAllDatasetFailure = createAction(
  DatasetActionTypes.LoadAllDatasetFailure,
  props<{ error: Error | any }>(),
);

// Load Single
export const loadDataset = createAction(
  DatasetActionTypes.LoadDataset,
  props<{ id: number; params: any }>(),
);

export const loadDatasetSuccess = createAction(
  DatasetActionTypes.LoadDatasetSuccess,
  props<{ data: DatasetData }>(),
);

export const loadDatasetFailure = createAction(
  DatasetActionTypes.LoadDatasetFailure,
  props<{ error: Error | any }>(),
);

// Load Single By Title
export const loadDatasetByTitle = createAction(
  DatasetActionTypes.LoadDatasetByTitle,
  props<{ title: string; params: any }>(),
);

export const loadDatasetByTitleSuccess = createAction(
  DatasetActionTypes.LoadDatasetByTitleSuccess,
  props<{ data: DatasetData }>(),
);

export const loadDatasetByTitleFailure = createAction(
  DatasetActionTypes.LoadDatasetByTitleFailure,
  props<{ error: Error | any }>(),
);

// Create
export const createDataset = createAction(
  DatasetActionTypes.CreateDataset,
  props<{ create: any }>(),
);

export const createDatasetSuccess = createAction(
  DatasetActionTypes.CreateDatasetSuccess,
  props<{ data: DatasetData }>(),
);

export const createDatasetFailure = createAction(
  DatasetActionTypes.CreateDatasetFailure,
  props<{ error: Error | any }>(),
);

// Update
export const updateDataset = createAction(
  DatasetActionTypes.UpdateDataset,
  props<{ update: any; params: any }>(),
);

export const updateDatasetSuccess = createAction(
  DatasetActionTypes.UpdateDatasetSuccess,
  props<{ data: DatasetData }>(),
);

export const updateDatasetFailure = createAction(
  DatasetActionTypes.UpdateDatasetFailure,
  props<{ error: Error | any }>(),
);

// Counter
export const counterDataset = createAction(
  DatasetActionTypes.CounterDataset,
  props<{ id: number; params: any }>(),
);

export const counterDatasetSuccess = createAction(
  DatasetActionTypes.CounterDatasetSuccess,
  props<{ data: any }>(),
);

export const counterDatasetFailure = createAction(
  DatasetActionTypes.CounterDatasetFailure,
  props<{ error: Error | any }>(),
);

// Counter By Title
export const counterDatasetByTitle = createAction(
  DatasetActionTypes.CounterDatasetByTitle,
  props<{ title: string; params: any }>(),
);

export const counterDatasetByTitleSuccess = createAction(
  DatasetActionTypes.CounterDatasetByTitleSuccess,
  props<{ data: any }>(),
);

export const counterDatasetByTitleFailure = createAction(
  DatasetActionTypes.CounterDatasetByTitleFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearDataset = createAction(DatasetActionTypes.ClearDataset);

export const fromDatasetActions = {
  loadAllDataset,
  loadAllDatasetSuccess,
  loadAllDatasetFailure,
  loadDataset,
  loadDatasetSuccess,
  loadDatasetFailure,
  loadDatasetByTitle,
  loadDatasetByTitleSuccess,
  loadDatasetByTitleFailure,
  createDataset,
  createDatasetSuccess,
  createDatasetFailure,
  updateDataset,
  updateDatasetSuccess,
  updateDatasetFailure,
  counterDataset,
  counterDatasetSuccess,
  counterDatasetFailure,
  counterDatasetByTitle,
  counterDatasetByTitleSuccess,
  counterDatasetByTitleFailure,
  clearDataset,
};
