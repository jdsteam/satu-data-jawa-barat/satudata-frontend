import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';

import { fromDatasetActions } from '@store-v3/dataset/dataset.actions';
import { DatasetService, PaginationService } from '@services-v3';

// PLUGIN
import { handlePromise } from '@helpers-v3';

@Injectable()
export class DatasetEffects {
  name = 'Dataset';

  loadAllDataset$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromDatasetActions.loadAllDataset),
      switchMap(async (action) => {
        const { data: result, error: resultErr } = await handlePromise(
          this.datasetService.getList(action.params).toPromise(),
        );

        if (resultErr) {
          return this.loadAllFailure(resultErr.message);
        }

        if (result.error === 1) {
          if (result.message === 'Data not found') {
            return fromDatasetActions.clearDataset();
          }
          return this.loadAllFailure(result.message);
        }

        let pagination = { empty: true, infinite: action.infinite };
        if (action.pagination) {
          const resultPagination = this.paginationService.getPagination(
            result.meta.total_record,
            action.params.skip,
            action.params.limit,
          );

          pagination = {
            empty: false,
            infinite: action.infinite,
            ...resultPagination,
          };
        }

        const suggestion = {
          suggestion: result.suggestion,
          original_text: result.original_text,
          suggestion_text: result.suggestion_text,
        };

        return this.loadAllSuccess(result.data, pagination, suggestion);
      }),
    ),
  );

  loadDataset$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromDatasetActions.loadDataset),
      switchMap((action) => {
        return this.datasetService.getSingle(action.id, action.params).pipe(
          map((result: any) => {
            if (result.error === 1) {
              if (result.message === 'Data not found') {
                return fromDatasetActions.clearDataset();
              }
              of(
                fromDatasetActions.loadDatasetFailure({
                  error: {
                    name: this.name,
                    error: true,
                    message: result.message,
                  },
                }),
              );
            }

            return fromDatasetActions.loadDatasetSuccess({
              data: result.data,
            });
          }),
          catchError((error: HttpErrorResponse) =>
            of(
              fromDatasetActions.loadDatasetFailure({
                error: {
                  name: this.name,
                  error: !error.ok,
                  message: error.message,
                },
              }),
            ),
          ),
        );
      }),
    ),
  );

  loadDatasetByTitle$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromDatasetActions.loadDatasetByTitle),
      switchMap((action) => {
        return this.datasetService
          .getSingleByTitle(action.title, action.params)
          .pipe(
            map((result: any) =>
              fromDatasetActions.loadDatasetByTitleSuccess({
                data: result.data,
              }),
            ),
            catchError((error: HttpErrorResponse) =>
              of(
                fromDatasetActions.loadDatasetByTitleFailure({
                  error: {
                    name: this.name,
                    error: !error.ok,
                    message: error.message,
                  },
                }),
              ),
            ),
          );
      }),
    ),
  );

  createDataset$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromDatasetActions.createDataset),
      switchMap((action) =>
        this.datasetService.createItem(action.create).pipe(
          map((res: any) => {
            return fromDatasetActions.createDatasetSuccess({
              data: res.data,
            });
          }),
          catchError((error) => {
            return of(
              fromDatasetActions.createDatasetFailure({
                error: {
                  name: this.name,
                  error: true,
                  message: error.message,
                },
              }),
            );
          }),
        ),
      ),
    ),
  );

  updateDataset$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromDatasetActions.updateDataset),
      switchMap((action) =>
        this.datasetService
          .updateItem(action.update.id, action.update, action.params)
          .pipe(
            map((res: any) => {
              return fromDatasetActions.updateDatasetSuccess({
                data: res.data,
              });
            }),
            catchError((error) => {
              return of(
                fromDatasetActions.updateDatasetFailure({
                  error: {
                    name: this.name,
                    error: true,
                    message: error.message,
                  },
                }),
              );
            }),
          ),
      ),
    ),
  );

  counterDataset$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromDatasetActions.counterDataset),
      switchMap((action) =>
        this.datasetService.getCounter(action.id, action.params).pipe(
          map((res: any) => {
            const result = {
              id: res.data.id,
              count_access_private: res.data.count_access_private,
              count_view_private: res.data.count_view_private,
              count_download_api_private: res.data.count_download_api_private,
              count_download_csv_private: res.data.count_download_csv_private,
              count_download_xls_private: res.data.count_download_xls_private,
              count_column: res.data.count_column,
              count_row: res.data.count_row,
            };

            if (res.error === 1) {
              return fromDatasetActions.counterDatasetFailure({
                error: {
                  name: this.name,
                  error: true,
                  message: res.message,
                },
              });
            }

            return fromDatasetActions.counterDatasetSuccess({
              data: result,
            });
          }),
          catchError((error: HttpErrorResponse) =>
            of(
              fromDatasetActions.counterDatasetFailure({
                error: {
                  name: this.name,
                  error: !error.ok,
                  message: error.message,
                },
              }),
            ),
          ),
        ),
      ),
    ),
  );

  counterDatasetByTitle$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromDatasetActions.counterDatasetByTitle),
      switchMap((action) =>
        this.datasetService.getCounterByTitle(action.title, action.params).pipe(
          map((res: any) =>
            fromDatasetActions.counterDatasetByTitleSuccess({
              data: res.data,
            }),
          ),
          catchError((error: HttpErrorResponse) =>
            of(
              fromDatasetActions.counterDatasetByTitleFailure({
                error: {
                  name: this.name,
                  error: !error.ok,
                  message: error.message,
                },
              }),
            ),
          ),
        ),
      ),
    ),
  );

  constructor(
    private actions$: Actions,
    private datasetService: DatasetService,
    private paginationService: PaginationService,
  ) {}

  loadAllSuccess(data: any, pagination: any, suggestion: any): any {
    return fromDatasetActions.loadAllDatasetSuccess({
      data,
      pagination,
      suggestion,
    });
  }

  loadAllFailure(error: string): any {
    return fromDatasetActions.loadAllDatasetFailure({
      error: {
        name: this.name,
        error: true,
        message: error,
      },
    });
  }
}
