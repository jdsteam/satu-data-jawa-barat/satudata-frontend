import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { DatasetData } from '@models-v3';
import { fromDatasetActions } from '@store-v3/dataset/dataset.actions';

export const ENTITY_FEATURE_KEY = 'datasetV3';

export interface State extends EntityState<DatasetData> {
  isLoadingList: boolean;
  isLoadingCreate: boolean;
  isLoadingRead: boolean;
  isLoadingUpdate: boolean;
  isLoadingDelete: boolean;
  isLoadingCounter: boolean;
  metadata: any;
  suggestion: any;
  pagination: any;
  error?: Error | any;
}

export const adapter: EntityAdapter<DatasetData> =
  createEntityAdapter<DatasetData>({
    selectId: (item) => item.id,
    // selectId: (item) => String(item.title),
  });

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  isLoadingList: false,
  isLoadingCreate: false,
  isLoadingRead: false,
  isLoadingUpdate: false,
  isLoadingDelete: false,
  isLoadingCounter: false,
  metadata: null,
  suggestion: null,
  pagination: null,
  error: null,
});

const reducer = createReducer(
  initialState,

  // Load All
  on(fromDatasetActions.loadAllDataset, (state) => {
    return {
      ...state,
      isLoadingList: true,
      error: null,
    };
  }),
  on(
    fromDatasetActions.loadAllDatasetSuccess,
    (state, { data, pagination, suggestion }) => {
      if (!pagination.infinite) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          suggestion,
          error: { error: false },
        });
      }

      if (pagination.current_page === 1) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          suggestion,
          error: { error: false },
        });
      }

      return adapter.addMany(data, {
        ...state,
        isLoadingList: false,
        pagination,
        suggestion,
        error: { error: false },
      });
    },
  ),
  on(fromDatasetActions.loadAllDatasetFailure, (state, { error }) => {
    return {
      ...state,
      isLoadingList: false,
      error,
    };
  }),

  // Load Single & Load Single By Title
  on(
    fromDatasetActions.loadDataset,
    fromDatasetActions.loadDatasetByTitle,
    (state) => {
      return {
        ...state,
        isLoadingRead: true,
        error: null,
      };
    },
  ),
  on(
    fromDatasetActions.loadDatasetSuccess,
    fromDatasetActions.loadDatasetByTitleSuccess,
    (state, { data }) => {
      return adapter.upsertOne(data, {
        ...state,
        isLoadingRead: false,
        error: { error: false },
      });
    },
  ),
  on(
    fromDatasetActions.loadDatasetFailure,
    fromDatasetActions.loadDatasetByTitleFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingRead: false,
        error,
      };
    },
  ),

  // Create
  on(fromDatasetActions.createDataset, (state) => {
    return {
      ...state,
      isLoadingCreate: true,
      error: null,
    };
  }),
  on(fromDatasetActions.createDatasetSuccess, (state, { data }) => {
    return adapter.addOne(data, {
      ...state,
      isLoadingCreate: false,
      error: { error: false },
    });
  }),
  on(fromDatasetActions.createDatasetFailure, (state, { error }) => {
    return {
      ...state,
      isLoadingCreate: false,
      error,
    };
  }),

  // Update
  on(fromDatasetActions.updateDataset, (state) => {
    return {
      ...state,
      isLoadingUpdate: true,
      error: null,
    };
  }),
  on(fromDatasetActions.updateDatasetSuccess, (state, { data }) => {
    return adapter.updateOne(
      { id: data.id, changes: data },
      {
        ...state,
        isLoadingUpdate: false,
        error: { error: false },
      },
    );
  }),
  on(fromDatasetActions.updateDatasetFailure, (state, { error }) => {
    return {
      ...state,
      error,
    };
  }),

  // Counter
  on(
    fromDatasetActions.counterDataset,
    fromDatasetActions.counterDatasetByTitle,
    (state) => {
      return {
        ...state,
        isLoadingCounter: true,
        error: null,
      };
    },
  ),
  on(fromDatasetActions.counterDatasetSuccess, (state, { data }) => {
    return adapter.updateOne(
      { id: data.id, changes: data },
      {
        ...state,
        isLoadingCounter: false,
        error: { error: false },
      },
    );
  }),
  on(
    fromDatasetActions.counterDatasetFailure,
    fromDatasetActions.counterDatasetByTitleFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingCounter: false,
        error,
      };
    },
  ),

  // Counter By Title
  on(fromDatasetActions.counterDatasetByTitleSuccess, (state, { data }) => {
    return adapter.updateOne(
      { id: data.title, changes: data },
      {
        ...state,
        isLoadingCounter: false,
        error: { error: false },
      },
    );
  }),

  // Clear
  on(fromDatasetActions.clearDataset, (state) => {
    return adapter.removeAll({
      ...state,
      isLoadingList: false,
      pagination: { empty: true },
      suggestion: {},
      error: { error: false },
    });
  }),
);

export function DatasetReducer(state: State | undefined, action: Action): any {
  return reducer(state, action);
}
