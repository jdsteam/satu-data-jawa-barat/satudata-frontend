import { createAction, props } from '@ngrx/store';
import { ReviewDatasetData } from '@models-v3';

export enum ReviewDatasetActionTypes {
  LoadAllReviewDataset = '[REVIEW DATASET] Load All Review Dataset',
  LoadAllReviewDatasetSuccess = '[REVIEW DATASET] Load All Review Dataset Success',
  LoadAllReviewDatasetFailure = '[REVIEW DATASET] Load All Review Dataset Failure',
  ClearReviewDataset = '[REVIEW DATASET] Clear Review Dataset',
}

// Load All
export const loadAllReviewDataset = createAction(
  ReviewDatasetActionTypes.LoadAllReviewDataset,
  props<{ params: any; pagination: boolean; infinite: boolean }>(),
);

export const loadAllReviewDatasetSuccess = createAction(
  ReviewDatasetActionTypes.LoadAllReviewDatasetSuccess,
  props<{ data: ReviewDatasetData[]; pagination: any }>(),
);

export const loadAllReviewDatasetFailure = createAction(
  ReviewDatasetActionTypes.LoadAllReviewDatasetFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearReviewDataset = createAction(
  ReviewDatasetActionTypes.ClearReviewDataset,
);

export const fromReviewDatasetActions = {
  loadAllReviewDataset,
  loadAllReviewDatasetSuccess,
  loadAllReviewDatasetFailure,
  clearReviewDataset,
};
