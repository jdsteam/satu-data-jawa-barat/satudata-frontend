import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { switchMap } from 'rxjs/operators';

import { fromReviewDatasetActions } from '@store-v3/review-dataset/review-dataset.actions';
import { ReviewDatasetService, PaginationService } from '@services-v3';
import { handlePromise } from '@helpers-v3';

@Injectable()
export class ReviewDatasetEffects {
  name = 'Review Dataset';

  loadAllReviewDataset$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromReviewDatasetActions.loadAllReviewDataset),
      switchMap(async (action) => {
        const { data: result, error: resultErr } = await handlePromise(
          this.reviewDatasetService.getList(action.params).toPromise(),
        );

        if (resultErr) {
          return this.loadAllFailure(resultErr.message);
        }

        if (result.error === 1) {
          if (result.message === 'Data not found') {
            return fromReviewDatasetActions.clearReviewDataset();
          }

          return this.loadAllFailure(result.message);
        }

        let pagination = { empty: true, infinite: action.infinite };
        if (action.pagination) {
          const resultPagination = this.paginationService.getPagination(
            result.meta.total_record,
            action.params.skip,
            action.params.limit,
          );

          pagination = {
            empty: false,
            infinite: action.infinite,
            ...resultPagination,
          };
        }

        return this.loadAllSuccess(result.data, pagination);
      }),
    ),
  );

  constructor(
    private actions$: Actions,
    private reviewDatasetService: ReviewDatasetService,
    private paginationService: PaginationService,
  ) {}

  loadAllSuccess(data: any, pagination: any): any {
    return fromReviewDatasetActions.loadAllReviewDatasetSuccess({
      data,
      pagination,
    });
  }

  loadAllFailure(error: string): any {
    return fromReviewDatasetActions.loadAllReviewDatasetFailure({
      error: {
        name: this.name,
        error: true,
        message: error,
      },
    });
  }
}
