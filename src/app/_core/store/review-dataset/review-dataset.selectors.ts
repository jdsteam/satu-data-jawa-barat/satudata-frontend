import { createFeatureSelector, createSelector } from '@ngrx/store';
import {
  State,
  adapter,
  ENTITY_FEATURE_KEY,
} from '@store-v3/review-dataset/review-dataset.reducers';

// Lookup feature state managed by NgRx
const getState = createFeatureSelector<State>(ENTITY_FEATURE_KEY);

// get the selectors
const { selectIds, selectAll, selectEntities } = adapter.getSelectors();

// select the array of ids
export const selectReviewDatasetIds = createSelector(getState, selectIds);

// select the array
export const selectAllReviewDataset = createSelector(getState, selectAll);

export const getReviewDatasetEntities = createSelector(
  getState,
  (state: State) => selectEntities(state),
);

// select by id
export const selectReviewDataset = (id: number | string) =>
  createSelector(getReviewDatasetEntities, (entities) => {
    return entities[id];
  });

// select the by multiple id
export const selectMultipleReviewDataset = (ids: any[]) =>
  createSelector(getReviewDatasetEntities, (entities) => {
    const filtered = Object.keys(entities)
      .filter((key) => ids.includes(key))
      .reduce((obj, key) => {
        // eslint-disable-next-line no-param-reassign
        obj[key] = entities[key];
        return obj;
      }, {});

    return Object.keys(filtered).map((key) => filtered[key]);
  });

// select loaded flag
export const selectIsLoadingList = createSelector(
  getState,
  (state) => state.isLoadingList,
);
export const selectIsLoadingCreate = createSelector(
  getState,
  (state) => state.isLoadingCreate,
);
export const selectIsLoadingRead = createSelector(
  getState,
  (state) => state.isLoadingRead,
);
export const selectIsLoadingUpdate = createSelector(
  getState,
  (state) => state.isLoadingUpdate,
);
export const selectIsLoadingDelete = createSelector(
  getState,
  (state) => state.isLoadingDelete,
);

// select metadata
export const selectMetadata = createSelector(
  getState,
  (state) => state.metadata,
);

// select pagination
export const selectPagination = createSelector(
  getState,
  (state) => state.pagination,
);

// select error
export const selectError = createSelector(getState, (state) => state.error);
