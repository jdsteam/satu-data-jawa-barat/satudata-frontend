import { createAction, props } from '@ngrx/store';
import { ApplicationServiceData } from '@models-v3';

export enum ApplicationServiceActionTypes {
  LoadAllApplicationService = '[APPLICATION SERVICE] Load All Application Service',
  LoadAllApplicationServiceSuccess = '[APPLICATION SERVICE] Load All Application Service Success',
  LoadAllApplicationServiceFailure = '[APPLICATION SERVICE] Load All Application Service Failure',
  ClearApplicationService = '[APPLICATION SERVICE] Clear Application Service',
}

// Load All
export const loadAllApplicationService = createAction(
  ApplicationServiceActionTypes.LoadAllApplicationService,
  props<{ params: any; pagination: boolean }>(),
);

export const loadAllApplicationServiceSuccess = createAction(
  ApplicationServiceActionTypes.LoadAllApplicationServiceSuccess,
  props<{ data: ApplicationServiceData[]; pagination: any }>(),
);

export const loadAllApplicationServiceFailure = createAction(
  ApplicationServiceActionTypes.LoadAllApplicationServiceFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearApplicationService = createAction(
  ApplicationServiceActionTypes.ClearApplicationService,
);

export const fromApplicationServiceActions = {
  loadAllApplicationService,
  loadAllApplicationServiceSuccess,
  loadAllApplicationServiceFailure,
  clearApplicationService,
};
