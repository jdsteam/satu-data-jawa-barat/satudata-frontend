import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { switchMap } from 'rxjs/operators';

import { fromApplicationServiceActions } from '@store-v3/application-service/application-service.actions';
import { ApplicationServiceService, PaginationService } from '@services-v3';
import { handlePromise } from '@helpers-v3';

@Injectable()
export class ApplicationServiceEffects {
  name = 'Application Service';

  loadAllApplicationService$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromApplicationServiceActions.loadAllApplicationService),
      switchMap(async (action) => {
        const { data: result, error: resultErr } = await handlePromise(
          this.applicationServiceService.getList(action.params).toPromise(),
        );

        if (resultErr) {
          return this.loadAllFailure(resultErr.message);
        }

        if (result.error === 1) {
          if (result.message === 'Data not found') {
            return fromApplicationServiceActions.clearApplicationService();
          }

          return this.loadAllFailure(result.message);
        }

        let pagination = { empty: true };
        if (action.pagination) {
          const { data: total, error: totalErr } = await handlePromise(
            this.applicationServiceService.getTotal(action.params).toPromise(),
          );

          if (totalErr) {
            return this.loadAllFailure(totalErr.message);
          }

          const resultPagination = this.paginationService.getPagination(
            total.data.count,
            action.params.skip,
            action.params.limit,
          );

          pagination = { empty: false, ...resultPagination };
        }

        return this.loadAllSuccess(result.data, pagination);
      }),
    ),
  );

  constructor(
    private actions$: Actions,
    private applicationServiceService: ApplicationServiceService,
    private paginationService: PaginationService,
  ) {}

  loadAllSuccess(data: any, pagination: any): any {
    return fromApplicationServiceActions.loadAllApplicationServiceSuccess({
      data,
      pagination,
    });
  }

  loadAllFailure(error: string): any {
    return fromApplicationServiceActions.loadAllApplicationServiceFailure({
      error: {
        name: this.name,
        error: true,
        message: error,
      },
    });
  }
}
