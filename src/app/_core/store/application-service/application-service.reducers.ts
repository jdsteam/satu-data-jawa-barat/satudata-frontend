import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { ApplicationServiceData } from '@models-v3';
import { fromApplicationServiceActions } from '@store-v3/application-service/application-service.actions';

export const ENTITY_FEATURE_KEY = 'applicationServiceV3';

export interface State extends EntityState<ApplicationServiceData> {
  isLoadingList: boolean;
  isLoadingCreate: boolean;
  isLoadingRead: boolean;
  isLoadingUpdate: boolean;
  isLoadingDelete: boolean;
  metadata: any;
  pagination: any;
  error?: Error | any;
}

export const adapter: EntityAdapter<ApplicationServiceData> =
  createEntityAdapter<ApplicationServiceData>({
    selectId: (item) => item.id,
  });

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  isLoadingList: false,
  isLoadingCreate: true,
  isLoadingRead: true,
  isLoadingUpdate: true,
  isLoadingDelete: true,
  metadata: null,
  pagination: null,
  error: null,
});

const reducer = createReducer(
  initialState,

  // Load All
  on(fromApplicationServiceActions.loadAllApplicationService, (state) => {
    return {
      ...state,
      isLoadingList: true,
      error: null,
    };
  }),
  on(
    fromApplicationServiceActions.loadAllApplicationServiceSuccess,
    (state, { data, pagination }) => {
      return adapter.setAll(data, {
        ...state,
        isLoadingList: false,
        pagination,
        error: { error: false },
      });
    },
  ),
  on(
    fromApplicationServiceActions.loadAllApplicationServiceFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingList: false,
        error,
      };
    },
  ),

  // Clear
  on(fromApplicationServiceActions.clearApplicationService, (state) => {
    return adapter.removeAll({
      ...state,
      isLoadingList: false,
      pagination: { empty: true },
      error: { error: false },
    });
  }),
);

export function ApplicationServiceReducer(
  state: State | undefined,
  action: Action,
): any {
  return reducer(state, action);
}
