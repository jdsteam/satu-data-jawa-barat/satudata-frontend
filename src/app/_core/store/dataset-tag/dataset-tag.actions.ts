import { createAction, props } from '@ngrx/store';
import { DatasetTagData } from '@models-v3';

export enum DatasetTagActionTypes {
  LoadAllDatasetTag = '[DATASET TAG] Load All Dataset Tag',
  LoadAllDatasetTagSuccess = '[DATASET TAG] Load All Dataset Tag Success',
  LoadAllDatasetTagFailure = '[DATASET TAG] Load All Dataset Tag Failure',
  LoadDatasetTag = '[DATASET TAG] Load Dataset Tag',
  LoadDatasetTagSuccess = '[DATASET TAG] Load Dataset Tag Success',
  LoadDatasetTagFailure = '[DATASET TAG] Load Dataset Tag Failure',
  ClearDatasetTag = '[DATASET TAG] Clear Dataset Tag',
}

// Load All
export const loadAllDatasetTag = createAction(
  DatasetTagActionTypes.LoadAllDatasetTag,
  props<{ params: any; pagination: boolean; infinite: boolean }>(),
);

export const loadAllDatasetTagSuccess = createAction(
  DatasetTagActionTypes.LoadAllDatasetTagSuccess,
  props<{ data: DatasetTagData[]; pagination: any }>(),
);

export const loadAllDatasetTagFailure = createAction(
  DatasetTagActionTypes.LoadAllDatasetTagFailure,
  props<{ error: Error | any }>(),
);

// Load Single
export const loadDatasetTag = createAction(
  DatasetTagActionTypes.LoadDatasetTag,
  props<{ id: number; params: any }>(),
);

export const loadDatasetTagSuccess = createAction(
  DatasetTagActionTypes.LoadDatasetTagSuccess,
  props<{ data: DatasetTagData }>(),
);

export const loadDatasetTagFailure = createAction(
  DatasetTagActionTypes.LoadDatasetTagFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearDatasetTag = createAction(
  DatasetTagActionTypes.ClearDatasetTag,
);

export const fromDatasetTagActions = {
  loadAllDatasetTag,
  loadAllDatasetTagSuccess,
  loadAllDatasetTagFailure,
  loadDatasetTag,
  loadDatasetTagSuccess,
  loadDatasetTagFailure,
  clearDatasetTag,
};
