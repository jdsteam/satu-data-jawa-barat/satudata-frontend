import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { DatasetTagData } from '@models-v3';
import { fromDatasetTagActions } from '@store-v3/dataset-tag/dataset-tag.actions';

export const ENTITY_FEATURE_KEY = 'datasetTagV3';

export interface State extends EntityState<DatasetTagData> {
  isLoadingList: boolean;
  isLoadingCreate: boolean;
  isLoadingRead: boolean;
  isLoadingUpdate: boolean;
  isLoadingDelete: boolean;
  metadata: any;
  pagination: any;
  error?: Error | any;
}

export const adapter: EntityAdapter<DatasetTagData> =
  createEntityAdapter<DatasetTagData>({
    selectId: (item) => String(item.tag),
  });

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  isLoadingList: false,
  isLoadingCreate: false,
  isLoadingRead: false,
  isLoadingUpdate: false,
  isLoadingDelete: false,
  metadata: null,
  pagination: null,
  error: null,
});

const reducer = createReducer(
  initialState,

  // Load All
  on(fromDatasetTagActions.loadAllDatasetTag, (state) => {
    return {
      ...state,
      isLoadingList: true,
    };
  }),
  on(
    fromDatasetTagActions.loadAllDatasetTagSuccess,
    (state, { data, pagination }) => {
      if (!pagination.infinite) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          error: { error: false },
        });
      }

      if (pagination.current_page === 1) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          error: { error: false },
        });
      }

      return adapter.addMany(data, {
        ...state,
        isLoadingList: false,
        pagination,
        error: { error: false },
      });
    },
  ),
  on(fromDatasetTagActions.loadAllDatasetTagFailure, (state, { error }) => {
    return {
      ...state,
      isLoadingList: false,
      error,
    };
  }),

  // Load Single
  on(fromDatasetTagActions.loadDatasetTag, (state) => {
    return {
      ...state,
      isLoadingRead: true,
    };
  }),
  on(fromDatasetTagActions.loadDatasetTagSuccess, (state, { data }) => {
    return adapter.addOne(data, {
      ...state,
      isLoadingRead: false,
      error: { error: false },
    });
  }),
  on(fromDatasetTagActions.loadDatasetTagFailure, (state, { error }) => {
    return {
      ...state,
      isLoadingRead: false,
      error,
    };
  }),

  // Clear
  on(fromDatasetTagActions.clearDatasetTag, (state) => {
    return adapter.removeAll({
      ...state,
      isLoadingList: false,
      pagination: { empty: true },
      error: { error: false },
    });
  }),
);

export function DatasetTagReducer(
  state: State | undefined,
  action: Action,
): any {
  return reducer(state, action);
}
