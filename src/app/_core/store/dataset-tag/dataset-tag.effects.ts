import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';

import { fromDatasetTagActions } from '@store-v3/dataset-tag/dataset-tag.actions';
import { DatasetTagService, PaginationService } from '@services-v3';
import { handlePromise } from '@helpers-v3';

@Injectable()
export class DatasetTagEffects {
  name = 'Dataset Tag';

  loadAllDatasetTag$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromDatasetTagActions.loadAllDatasetTag),
      switchMap(async (action) => {
        const { data: result, error: resultErr } = await handlePromise(
          this.datasetTagService.getList(action.params).toPromise(),
        );

        if (resultErr) {
          return this.loadAllFailure(resultErr.message);
        }

        if (result.error === 1) {
          if (result.message === 'Data not found') {
            return fromDatasetTagActions.clearDatasetTag();
          }

          return this.loadAllFailure(result.message);
        }

        let pagination = { empty: true, infinite: action.infinite };
        if (action.pagination) {
          const resultPagination = this.paginationService.getPagination(
            result.meta.total_record,
            action.params.skip,
            action.params.limit,
          );

          pagination = {
            empty: false,
            infinite: action.infinite,
            ...resultPagination,
          };
        }

        return this.loadAllSuccess(result.data, pagination);
      }),
    ),
  );

  loadDatasetTag$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromDatasetTagActions.loadDatasetTag),
      switchMap((action) => {
        return this.datasetTagService.getSingle(action.id, action.params).pipe(
          map((result: any) =>
            fromDatasetTagActions.loadDatasetTagSuccess({
              data: result.data,
            }),
          ),
          catchError((error: HttpErrorResponse) =>
            of(
              fromDatasetTagActions.loadDatasetTagFailure({
                error: {
                  name: this.name,
                  error: !error.ok,
                  message: error.message,
                },
              }),
            ),
          ),
        );
      }),
    ),
  );

  constructor(
    private actions$: Actions,
    private datasetTagService: DatasetTagService,
    private paginationService: PaginationService,
  ) {}

  loadAllSuccess(data: any, pagination: any): any {
    return fromDatasetTagActions.loadAllDatasetTagSuccess({
      data,
      pagination,
    });
  }

  loadAllFailure(error: string): any {
    return fromDatasetTagActions.loadAllDatasetTagFailure({
      error: {
        name: this.name,
        error: true,
        message: error,
      },
    });
  }
}
