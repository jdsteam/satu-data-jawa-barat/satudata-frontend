import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { TopicData } from '@models-v3';
import { fromTopicActions } from '@store-v3/topic/topic.actions';

export const ENTITY_FEATURE_KEY = 'topicV3';

export interface State extends EntityState<TopicData> {
  isLoadingList: boolean;
  isLoadingCreate: boolean;
  isLoadingRead: boolean;
  isLoadingUpdate: boolean;
  isLoadingDelete: boolean;
  metadata: any;
  pagination: any;
  error?: Error | any;
}

export const adapter: EntityAdapter<TopicData> = createEntityAdapter<TopicData>(
  {
    selectId: (item) => item.id,
  },
);

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  isLoadingList: false,
  isLoadingCreate: true,
  isLoadingRead: true,
  isLoadingUpdate: true,
  isLoadingDelete: true,
  metadata: null,
  pagination: null,
  error: null,
});

const reducer = createReducer(
  initialState,

  // Load All
  on(fromTopicActions.loadAllTopic, (state) => {
    return {
      ...state,
      isLoadingList: true,
      error: null,
    };
  }),
  on(fromTopicActions.loadAllTopicSuccess, (state, { data, pagination }) => {
    return adapter.setAll(data, {
      ...state,
      isLoadingList: false,
      pagination,
      error: { error: false },
    });
  }),
  on(fromTopicActions.loadAllTopicFailure, (state, { error }) => {
    return {
      ...state,
      isLoadingList: false,
      error,
    };
  }),

  // Clear
  on(fromTopicActions.clearTopic, (state) => {
    return adapter.removeAll({
      ...state,
      isLoadingList: false,
      pagination: { empty: true },
      error: { error: false },
    });
  }),
);

export function TopicReducer(state: State | undefined, action: Action): any {
  return reducer(state, action);
}
