import { createAction, props } from '@ngrx/store';
import { TopicData } from '@models-v3';

export enum TopicActionTypes {
  LoadAllTopic = '[TOPIC] Load All Topic',
  LoadAllTopicSuccess = '[TOPIC] Load All Topic Success',
  LoadAllTopicFailure = '[TOPIC] Load All Topic Failure',
  ClearTopic = '[TOPIC] Clear Topic',
}

// Load All
export const loadAllTopic = createAction(
  TopicActionTypes.LoadAllTopic,
  props<{ params: any; pagination: boolean }>(),
);

export const loadAllTopicSuccess = createAction(
  TopicActionTypes.LoadAllTopicSuccess,
  props<{ data: TopicData[]; pagination: any }>(),
);

export const loadAllTopicFailure = createAction(
  TopicActionTypes.LoadAllTopicFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearTopic = createAction(TopicActionTypes.ClearTopic);

export const fromTopicActions = {
  loadAllTopic,
  loadAllTopicSuccess,
  loadAllTopicFailure,
  clearTopic,
};
