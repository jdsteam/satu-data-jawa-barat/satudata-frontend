import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { switchMap } from 'rxjs/operators';

import { fromTopicActions } from '@store-v3/topic/topic.actions';
import { TopicService, PaginationService } from '@services-v3';
import { handlePromise } from '@helpers-v3';

@Injectable()
export class TopicEffects {
  name = 'Topic';

  loadAllTopic$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromTopicActions.loadAllTopic),
      switchMap(async (action) => {
        const { data: result, error: resultErr } = await handlePromise(
          this.topicService.getList(action.params).toPromise(),
        );

        if (resultErr) {
          return this.loadAllFailure(resultErr.message);
        }

        if (result.error === 1) {
          if (result.message === 'Data not found') {
            return fromTopicActions.clearTopic();
          }

          return this.loadAllFailure(result.message);
        }

        let pagination = { empty: true };
        if (action.pagination) {
          const { data: total, error: totalErr } = await handlePromise(
            this.topicService.getTotal(action.params).toPromise(),
          );

          if (totalErr) {
            return this.loadAllFailure(totalErr.message);
          }

          const resultPagination = this.paginationService.getPagination(
            total.data.count,
            action.params.skip,
            action.params.limit,
          );

          pagination = { empty: false, ...resultPagination };
        }

        return this.loadAllSuccess(result.data, pagination);
      }),
    ),
  );

  constructor(
    private actions$: Actions,
    private topicService: TopicService,
    private paginationService: PaginationService,
  ) {}

  loadAllSuccess(data: any, pagination: any): any {
    return fromTopicActions.loadAllTopicSuccess({
      data,
      pagination,
    });
  }

  loadAllFailure(error: string): any {
    return fromTopicActions.loadAllTopicFailure({
      error: {
        name: this.name,
        error: true,
        message: error,
      },
    });
  }
}
