import { createAction, props } from '@ngrx/store';
import { LogPageData } from '@models-v3';

export enum LogPageActionTypes {
  LoadAllLogPage = '[LOG PAGE] Load All Log Page',
  LoadAllLogPageSuccess = '[LOG PAGE] Load All Log Page Success',
  LoadAllLogPageFailure = '[LOG PAGE] Load All Log Page Failure',
  ClearLogPage = '[LOG PAGE] Clear Log Page',
}

// Load All
export const loadAllLogPage = createAction(
  LogPageActionTypes.LoadAllLogPage,
  props<{ params: any; pagination: boolean; infinite: boolean }>(),
);

export const loadAllLogPageSuccess = createAction(
  LogPageActionTypes.LoadAllLogPageSuccess,
  props<{ data: LogPageData[]; pagination: any }>(),
);

export const loadAllLogPageFailure = createAction(
  LogPageActionTypes.LoadAllLogPageFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearLogPage = createAction(LogPageActionTypes.ClearLogPage);

export const fromLogPageActions = {
  loadAllLogPage,
  loadAllLogPageSuccess,
  loadAllLogPageFailure,
  clearLogPage,
};
