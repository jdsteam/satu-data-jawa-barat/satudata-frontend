import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { switchMap } from 'rxjs/operators';

import { fromLogPageActions } from '@store-v3/log-page/log-page.actions';
import { LogPageService } from '@services-v3';
import { handlePromise } from '@helpers-v3';

@Injectable()
export class LogPageEffects {
  name = 'Log Page';

  loadAllLogPage$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromLogPageActions.loadAllLogPage),
      switchMap(async (action) => {
        const { data: result, error: resultErr } = await handlePromise(
          this.logPageService.getList(action.params).toPromise(),
        );

        if (resultErr) {
          return this.loadAllFailure(resultErr.message);
        }

        if (result.error === 1) {
          if (result.message === 'Data not found') {
            return fromLogPageActions.clearLogPage();
          }
          return this.loadAllFailure(result.message);
        }

        let pagination = { empty: true, infinite: action.infinite };
        if (action.pagination) {
          pagination = {
            empty: false,
            infinite: action.infinite,
            ...result.pagination,
          };
        }

        return this.loadAllSuccess(result.data, pagination);
      }),
    ),
  );

  constructor(
    private actions$: Actions,
    private logPageService: LogPageService,
  ) {}

  loadAllSuccess(data: any, pagination: any): any {
    return fromLogPageActions.loadAllLogPageSuccess({
      data,
      pagination,
    });
  }

  loadAllFailure(error: string): any {
    return fromLogPageActions.loadAllLogPageFailure({
      error: {
        name: this.name,
        error: true,
        message: error,
      },
    });
  }
}
