import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { IndicatorData } from '@models-v3';
import { fromIndicatorActions } from '@store-v3/indicator/indicator.actions';

export const ENTITY_FEATURE_KEY = 'indicatorV3';

export interface State extends EntityState<IndicatorData> {
  isLoadingList: boolean;
  isLoadingCreate: boolean;
  isLoadingRead: boolean;
  isLoadingUpdate: boolean;
  isLoadingDelete: boolean;
  isLoadingCounter: boolean;
  metadata: any;
  suggestion: any;
  pagination: any;
  error?: Error | any;
}

export const adapter: EntityAdapter<IndicatorData> =
  createEntityAdapter<IndicatorData>({
    selectId: (item) => item.id,
  });

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  isLoadingList: false,
  isLoadingCreate: false,
  isLoadingRead: false,
  isLoadingUpdate: false,
  isLoadingDelete: false,
  isLoadingCounter: false,
  metadata: null,
  suggestion: null,
  pagination: null,
  error: null,
});

const reducer = createReducer(
  initialState,

  // Load All
  on(fromIndicatorActions.loadAllIndicator, (state) => {
    return {
      ...state,
      isLoadingList: true,
      error: null,
    };
  }),
  on(
    fromIndicatorActions.loadAllIndicatorSuccess,
    (state, { data, pagination, suggestion }) => {
      if (!pagination.infinite) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          suggestion,
          error: { error: false },
        });
      }

      if (pagination.current_page === 1) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          suggestion,
          error: { error: false },
        });
      }

      return adapter.addMany(data, {
        ...state,
        isLoadingList: false,
        pagination,
        suggestion,
        error: { error: false },
      });
    },
  ),
  on(fromIndicatorActions.loadAllIndicatorFailure, (state, { error }) => {
    return {
      ...state,
      isLoadingList: false,
      error,
    };
  }),

  // Load Single & Load Single By Title
  on(
    fromIndicatorActions.loadIndicator,
    fromIndicatorActions.loadIndicatorByTitle,
    (state) => {
      return {
        ...state,
        isLoadingRead: true,
        error: null,
      };
    },
  ),
  on(
    fromIndicatorActions.loadIndicatorSuccess,
    fromIndicatorActions.loadIndicatorByTitleSuccess,
    (state, { data }) => {
      return adapter.addOne(data, {
        ...state,
        isLoadingRead: false,
        error: { error: false },
      });
    },
  ),
  on(
    fromIndicatorActions.loadIndicatorFailure,
    fromIndicatorActions.loadIndicatorByTitleFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingRead: false,
        error,
      };
    },
  ),

  // Create
  on(fromIndicatorActions.createIndicator, (state) => {
    return {
      ...state,
      isLoadingCreate: true,
      error: null,
    };
  }),
  on(fromIndicatorActions.createIndicatorSuccess, (state, { data }) => {
    return adapter.addOne(data, {
      ...state,
      isLoadingCreate: false,
      error: { error: false },
    });
  }),
  on(fromIndicatorActions.createIndicatorFailure, (state, { error }) => {
    return {
      ...state,
      isLoadingCreate: false,
      error,
    };
  }),

  // Update
  on(fromIndicatorActions.updateIndicator, (state) => {
    return {
      ...state,
      isLoadingUpdate: true,
      error: null,
    };
  }),
  on(fromIndicatorActions.updateIndicatorSuccess, (state, { data }) => {
    return adapter.updateOne(
      { id: data.id, changes: data },
      {
        ...state,
        isLoadingUpdate: false,
        error: { error: false },
      },
    );
  }),
  on(fromIndicatorActions.updateIndicatorFailure, (state, { error }) => {
    return {
      ...state,
      error,
    };
  }),

  // Counter
  on(
    fromIndicatorActions.counterIndicator,
    fromIndicatorActions.counterIndicatorByTitle,
    (state) => {
      return {
        ...state,
        isLoadingCounter: true,
        error: null,
      };
    },
  ),
  on(fromIndicatorActions.counterIndicatorSuccess, (state, { data }) => {
    return adapter.updateOne(
      { id: data.id, changes: data },
      {
        ...state,
        isLoadingCounter: false,
        error: { error: false },
      },
    );
  }),
  on(
    fromIndicatorActions.counterIndicatorFailure,
    fromIndicatorActions.counterIndicatorByTitleFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingCounter: false,
        error,
      };
    },
  ),

  // Counter By Title
  on(fromIndicatorActions.counterIndicatorByTitleSuccess, (state, { data }) => {
    return adapter.updateOne(
      { id: data.title, changes: data },
      {
        ...state,
        isLoadingCounter: false,
        error: { error: false },
      },
    );
  }),

  // Clear
  on(fromIndicatorActions.clearIndicator, (state) => {
    return adapter.removeAll({
      ...state,
      isLoadingList: false,
      pagination: { empty: true },
      suggestion: {},
      error: { error: false },
    });
  }),
);

export function IndicatorReducer(
  state: State | undefined,
  action: Action,
): any {
  return reducer(state, action);
}
