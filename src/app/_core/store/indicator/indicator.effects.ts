import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';

import { fromIndicatorActions } from '@store-v3/indicator/indicator.actions';
import { fromIndicatorClassificationHistoryActions } from '@store-v3/indicator-classification-history/indicator-classification-history.actions';
import { fromIndicatorProgressInitialActions } from '@store-v3/indicator-progress-initial/indicator-progress-initial.actions';
import { fromIndicatorProgressFinalActions } from '@store-v3/indicator-progress-final/indicator-progress-final.actions';
import { IndicatorService, PaginationService } from '@services-v3';
import { handlePromise } from '@helpers-v3';

@Injectable()
export class IndicatorEffects {
  name = 'Indicator';

  loadAllIndicator$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromIndicatorActions.loadAllIndicator),
      switchMap(async (action) => {
        const { data: result, error: resultErr } = await handlePromise(
          this.indicatorService.getList(action.params).toPromise(),
        );

        if (resultErr) {
          return this.loadAllFailure(resultErr.message);
        }

        if (result.error === 1) {
          if (result.message === 'Data not found') {
            return fromIndicatorActions.clearIndicator();
          }
          return this.loadAllFailure(result.message);
        }

        let pagination = { empty: true, infinite: action.infinite };
        if (action.pagination) {
          const resultPagination = this.paginationService.getPagination(
            result.meta.total_record,
            action.params.skip,
            action.params.limit,
          );

          pagination = {
            empty: false,
            infinite: action.infinite,
            ...resultPagination,
          };
        }

        const suggestion = {
          suggestion: result.suggestion,
          original_text: result.original_text,
          suggestion_text: result.suggestion_text,
        };

        return this.loadAllSuccess(result.data, pagination, suggestion);
      }),
    ),
  );

  loadIndicator$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromIndicatorActions.loadIndicator),
      switchMap((action) => {
        return this.indicatorService.getSingle(action.id, action.params).pipe(
          map((result: any) => {
            if (result.error === 1) {
              if (result.message === 'Data not found') {
                return fromIndicatorActions.clearIndicator();
              }
              of(
                fromIndicatorActions.loadIndicatorFailure({
                  error: {
                    name: this.name,
                    error: true,
                    message: result.message,
                  },
                }),
              );
            }

            return fromIndicatorActions.loadIndicatorSuccess({
              data: result.data,
            });
          }),
          catchError((error: HttpErrorResponse) =>
            of(
              fromIndicatorActions.loadIndicatorFailure({
                error: {
                  name: this.name,
                  error: !error.ok,
                  message: error.message,
                },
              }),
            ),
          ),
        );
      }),
    ),
  );

  loadIndicatorByTitle$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromIndicatorActions.loadIndicatorByTitle),
      switchMap((action) => {
        return this.indicatorService
          .getSingle(action.title, action.params)
          .pipe(
            map((result: any) =>
              fromIndicatorActions.loadIndicatorByTitleSuccess({
                data: result.data,
              }),
            ),
            catchError((error: HttpErrorResponse) =>
              of(
                fromIndicatorActions.loadIndicatorByTitleFailure({
                  error: {
                    name: this.name,
                    error: !error.ok,
                    message: error.message,
                  },
                }),
              ),
            ),
          );
      }),
    ),
  );

  createIndicator$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromIndicatorActions.createIndicator),
      switchMap((action) => {
        return this.indicatorService.createItem(action.create).pipe(
          switchMap((res) => [
            fromIndicatorActions.createIndicatorSuccess({
              data: res.data,
            }),
            fromIndicatorClassificationHistoryActions.createIndicatorClassificationHistory(
              {
                indicator: res.data,
                create: action.createClassification,
              },
            ),
            fromIndicatorProgressInitialActions.createIndicatorProgressInitial({
              indicator: res.data,
              create: action.createInitialCondition,
            }),
            fromIndicatorProgressFinalActions.createIndicatorProgressFinal({
              indicator: res.data,
              create: action.createFinalCondition,
            }),
          ]),
          catchError((error) => {
            return of(
              fromIndicatorActions.createIndicatorFailure({
                error: {
                  name: this.name,
                  error: true,
                  message: error.message,
                },
              }),
            );
          }),
        );
      }),
    ),
  );

  updateIndicator$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromIndicatorActions.updateIndicator),
      switchMap((action) =>
        this.indicatorService.updateItem(action.update.id, action.update).pipe(
          map((res: any) => {
            return fromIndicatorActions.updateIndicatorSuccess({
              data: res.data,
            });
          }),
          catchError((error) => {
            return of(
              fromIndicatorActions.updateIndicatorFailure({
                error: {
                  name: this.name,
                  error: true,
                  message: error.message,
                },
              }),
            );
          }),
        ),
      ),
    ),
  );

  counterIndicator$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromIndicatorActions.counterIndicator),
      switchMap((action) =>
        this.indicatorService.getCounter(action.id, action.params).pipe(
          map((res: any) =>
            fromIndicatorActions.counterIndicatorSuccess({
              data: res.data,
            }),
          ),
          catchError((error: HttpErrorResponse) =>
            of(
              fromIndicatorActions.counterIndicatorFailure({
                error: {
                  name: this.name,
                  error: !error.ok,
                  message: error.message,
                },
              }),
            ),
          ),
        ),
      ),
    ),
  );

  counterIndicatorByTitle$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromIndicatorActions.counterIndicatorByTitle),
      switchMap((action) =>
        this.indicatorService.getCounter(action.title, action.params).pipe(
          map((res: any) =>
            fromIndicatorActions.counterIndicatorByTitleSuccess({
              data: res.data,
            }),
          ),
          catchError((error: HttpErrorResponse) =>
            of(
              fromIndicatorActions.counterIndicatorByTitleFailure({
                error: {
                  name: this.name,
                  error: !error.ok,
                  message: error.message,
                },
              }),
            ),
          ),
        ),
      ),
    ),
  );

  constructor(
    private actions$: Actions,
    private indicatorService: IndicatorService,
    private paginationService: PaginationService,
  ) {}

  loadAllSuccess(data: any, pagination: any, suggestion: any): any {
    return fromIndicatorActions.loadAllIndicatorSuccess({
      data,
      pagination,
      suggestion,
    });
  }

  loadAllFailure(error: string): any {
    return fromIndicatorActions.loadAllIndicatorFailure({
      error: {
        name: this.name,
        error: true,
        message: error,
      },
    });
  }
}
