import { createAction, props } from '@ngrx/store';
import { IndicatorData } from '@models-v3';

export enum IndicatorActionTypes {
  LoadAllIndicator = '[INDICATOR] Load All Indicator',
  LoadAllIndicatorSuccess = '[INDICATOR] Load All Indicator Success',
  LoadAllIndicatorFailure = '[INDICATOR] Load All Indicator Failure',
  LoadIndicator = '[INDICATOR] Load Indicator',
  LoadIndicatorSuccess = '[INDICATOR] Load Indicator Success',
  LoadIndicatorFailure = '[INDICATOR] Load Indicator Failure',
  LoadIndicatorByTitle = '[INDICATOR] Load Indicator By Title',
  LoadIndicatorByTitleSuccess = '[INDICATOR] Load Indicator By Title Success',
  LoadIndicatorByTitleFailure = '[INDICATOR] Load Indicator By Title Failure',
  CreateIndicator = '[INDICATOR] Create Indicator',
  CreateIndicatorSuccess = '[INDICATOR] Create Indicator Success',
  CreateIndicatorFailure = '[INDICATOR] Create Indicator Failure',
  UpdateIndicator = '[INDICATOR] Update Indicator',
  UpdateIndicatorSuccess = '[INDICATOR] Update Indicator Success',
  UpdateIndicatorFailure = '[INDICATOR] Update Indicator Failure',
  CounterIndicator = '[INDICATOR] Counter Indicator',
  CounterIndicatorSuccess = '[INDICATOR] Counter Indicator Success',
  CounterIndicatorFailure = '[INDICATOR] Counter Indicator Failure',
  CounterIndicatorByTitle = '[INDICATOR] Counter Indicator By Title',
  CounterIndicatorByTitleSuccess = '[INDICATOR] Counter Indicator By Title Success',
  CounterIndicatorByTitleFailure = '[INDICATOR] Counter Indicator By Title Failure',
  ClearIndicator = '[INDICATOR] Clear Indicator',
}

// Load All
export const loadAllIndicator = createAction(
  IndicatorActionTypes.LoadAllIndicator,
  props<{ params: any; pagination: boolean; infinite: boolean }>(),
);

export const loadAllIndicatorSuccess = createAction(
  IndicatorActionTypes.LoadAllIndicatorSuccess,
  props<{ data: IndicatorData[]; pagination: any; suggestion: any }>(),
);

export const loadAllIndicatorFailure = createAction(
  IndicatorActionTypes.LoadAllIndicatorFailure,
  props<{ error: Error | any }>(),
);

// Load Single
export const loadIndicator = createAction(
  IndicatorActionTypes.LoadIndicator,
  props<{ id: number; params: any }>(),
);

export const loadIndicatorSuccess = createAction(
  IndicatorActionTypes.LoadIndicatorSuccess,
  props<{ data: IndicatorData }>(),
);

export const loadIndicatorFailure = createAction(
  IndicatorActionTypes.LoadIndicatorFailure,
  props<{ error: Error | any }>(),
);

// Load Single By Title
export const loadIndicatorByTitle = createAction(
  IndicatorActionTypes.LoadIndicatorByTitle,
  props<{ title: string; params: any }>(),
);

export const loadIndicatorByTitleSuccess = createAction(
  IndicatorActionTypes.LoadIndicatorByTitleSuccess,
  props<{ data: IndicatorData }>(),
);

export const loadIndicatorByTitleFailure = createAction(
  IndicatorActionTypes.LoadIndicatorByTitleFailure,
  props<{ error: Error | any }>(),
);

// Create
export const createIndicator = createAction(
  IndicatorActionTypes.CreateIndicator,
  props<{
    create: any;
    createClassification: any;
    createInitialCondition: any;
    createFinalCondition: any;
  }>(),
);

export const createIndicatorSuccess = createAction(
  IndicatorActionTypes.CreateIndicatorSuccess,
  props<{ data: IndicatorData }>(),
);

export const createIndicatorFailure = createAction(
  IndicatorActionTypes.CreateIndicatorFailure,
  props<{ error: Error | any }>(),
);

// Update
export const updateIndicator = createAction(
  IndicatorActionTypes.UpdateIndicator,
  props<{ update: any }>(),
);

export const updateIndicatorSuccess = createAction(
  IndicatorActionTypes.UpdateIndicatorSuccess,
  props<{ data: IndicatorData }>(),
);

export const updateIndicatorFailure = createAction(
  IndicatorActionTypes.UpdateIndicatorFailure,
  props<{ error: Error | any }>(),
);

// Counter
export const counterIndicator = createAction(
  IndicatorActionTypes.CounterIndicator,
  props<{ id: number; params: any }>(),
);

export const counterIndicatorSuccess = createAction(
  IndicatorActionTypes.CounterIndicatorSuccess,
  props<{ data: IndicatorData }>(),
);

export const counterIndicatorFailure = createAction(
  IndicatorActionTypes.CounterIndicatorFailure,
  props<{ error: Error | any }>(),
);

// Counter By Title
export const counterIndicatorByTitle = createAction(
  IndicatorActionTypes.CounterIndicatorByTitle,
  props<{ title: string; params: any }>(),
);

export const counterIndicatorByTitleSuccess = createAction(
  IndicatorActionTypes.CounterIndicatorByTitleSuccess,
  props<{ data: IndicatorData }>(),
);

export const counterIndicatorByTitleFailure = createAction(
  IndicatorActionTypes.CounterIndicatorByTitleFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearIndicator = createAction(IndicatorActionTypes.ClearIndicator);

export const fromIndicatorActions = {
  loadAllIndicator,
  loadAllIndicatorSuccess,
  loadAllIndicatorFailure,
  loadIndicator,
  loadIndicatorSuccess,
  loadIndicatorFailure,
  loadIndicatorByTitle,
  loadIndicatorByTitleSuccess,
  loadIndicatorByTitleFailure,
  createIndicator,
  createIndicatorSuccess,
  createIndicatorFailure,
  updateIndicator,
  updateIndicatorSuccess,
  updateIndicatorFailure,
  counterIndicator,
  counterIndicatorSuccess,
  counterIndicatorFailure,
  counterIndicatorByTitle,
  counterIndicatorByTitleSuccess,
  counterIndicatorByTitleFailure,
  clearIndicator,
};
