import { createAction, props } from '@ngrx/store';

export enum CoreDataActionTypes {
  LoadAllCoreData = '[CORE DATA] Load All Core Data',
  LoadAllCoreDataSuccess = '[CORE DATA] Load All Core Data Success',
  LoadAllCoreDataFailure = '[CORE DATA] Load All Core Data Failure',
  ClearCoreData = '[CORE DATA] Clear Core Data',
}

// Load
export const loadAllCoreData = createAction(
  CoreDataActionTypes.LoadAllCoreData,
  props<{ url: string; params: any; pagination: boolean; infinite: boolean }>(),
);

export const loadAllCoreDataSuccess = createAction(
  CoreDataActionTypes.LoadAllCoreDataSuccess,
  props<{ data: any[]; pagination: any }>(),
);

export const loadAllCoreDataFailure = createAction(
  CoreDataActionTypes.LoadAllCoreDataFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearCoreData = createAction(CoreDataActionTypes.ClearCoreData);

export const fromCoreDataActions = {
  loadAllCoreData,
  loadAllCoreDataSuccess,
  loadAllCoreDataFailure,
  clearCoreData,
};
