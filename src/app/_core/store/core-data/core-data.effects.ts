import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { switchMap } from 'rxjs/operators';

import { fromCoreDataActions } from '@store-v3/core-data/core-data.actions';
import { CoreDataService, PaginationService } from '@services-v3';
import { handlePromise } from '@helpers-v3';

@Injectable()
export class CoreDataEffects {
  name = 'CoreData';

  loadAllCoreData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromCoreDataActions.loadAllCoreData),
      switchMap(async (action) => {
        const { data: result, error: resultErr } = await handlePromise(
          this.coreDataService.getListV2(action.url, action.params).toPromise(),
        );

        if (resultErr) {
          return this.loadAllFailure(resultErr.message);
        }

        if (result.error === 1) {
          if (result.message === 'Data not found') {
            return fromCoreDataActions.clearCoreData();
          }
          return this.loadAllFailure(result.message);
        }

        let pagination = { empty: true, infinite: action.infinite };
        if (action.pagination) {
          const resultPagination = this.paginationService.getPagination(
            result.meta.total_record,
            action.params.skip,
            action.params.limit,
          );

          pagination = {
            empty: false,
            infinite: action.infinite,
            ...resultPagination,
          };
        }

        return this.loadAllSuccess(result.data, pagination);
      }),
    ),
  );

  constructor(
    private actions$: Actions,
    private coreDataService: CoreDataService,
    private paginationService: PaginationService,
  ) {}

  loadAllSuccess(data: any, pagination: any): any {
    return fromCoreDataActions.loadAllCoreDataSuccess({
      data,
      pagination,
    });
  }

  loadAllFailure(error: string): any {
    return fromCoreDataActions.loadAllCoreDataFailure({
      error: {
        name: this.name,
        error: true,
        message: error,
      },
    });
  }
}
