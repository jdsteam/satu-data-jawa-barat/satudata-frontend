import { createFeatureSelector, createSelector } from '@ngrx/store';
import {
  State,
  adapter,
  ENTITY_FEATURE_KEY,
} from '@store-v3/core-data/core-data.reducers';

// Lookup feature state managed by NgRx
const getState = createFeatureSelector<State>(ENTITY_FEATURE_KEY);

// get the selectors
const { selectIds, selectAll, selectEntities } = adapter.getSelectors();

// select the array of ids
export const selectCoreDataIds = createSelector(getState, selectIds);

// select the array
export const selectAllCoreData = createSelector(getState, selectAll);

export const getCoreDataEntities = createSelector(getState, (state: State) =>
  selectEntities(state),
);

// select by id
export const selectCoreData = (id: number | string) =>
  createSelector(getCoreDataEntities, (entities) => {
    return entities[id];
  });

// select loaded flag
export const selectIsLoadingList = createSelector(
  getState,
  (state) => state.isLoadingList,
);

// select metadata
export const selectMetadata = createSelector(
  getState,
  (state) => state.metadata,
);

// select pagination
export const selectPagination = createSelector(
  getState,
  (state) => state.pagination,
);

// select error
export const selectError = createSelector(getState, (state) => state.error);
