import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { MetadataVisualizationData } from '@models-v3';
import { fromMetadataCustomVisualizationActions } from '@store-v3/metadata-custom-visualization/metadata-custom-visualization.actions';

export const ENTITY_FEATURE_KEY = 'metadataCustomVisualizationV3';

export interface State extends EntityState<MetadataVisualizationData> {
  isLoadingList: boolean;
  isLoadingCreate: boolean;
  isLoadingRead: boolean;
  isLoadingUpdate: boolean;
  isLoadingDelete: boolean;
  metadata: any;
  pagination: any;
  error?: Error | any;
}

export const adapter: EntityAdapter<MetadataVisualizationData> =
  createEntityAdapter<MetadataVisualizationData>({
    selectId: (item) => item.id,
  });

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  isLoadingList: false,
  isLoadingCreate: false,
  isLoadingRead: false,
  isLoadingUpdate: false,
  isLoadingDelete: false,
  metadata: null,
  pagination: null,
  error: null,
});

const reducer = createReducer(
  initialState,

  // Load All
  on(
    fromMetadataCustomVisualizationActions.loadAllMetadataCustomVisualization,
    (state) => {
      return {
        ...state,
        isLoadingList: true,
        error: null,
      };
    },
  ),
  on(
    fromMetadataCustomVisualizationActions.loadAllMetadataCustomVisualizationSuccess,
    (state, { data, pagination }) => {
      if (!pagination.infinite) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          error: { error: false },
        });
      }

      if (pagination.current_page === 1) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          error: { error: false },
        });
      }

      return adapter.addMany(data, {
        ...state,
        isLoadingList: false,
        pagination,
        error: { error: false },
      });
    },
  ),
  on(
    fromMetadataCustomVisualizationActions.loadAllMetadataCustomVisualizationFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingList: false,
        error,
      };
    },
  ),

  // Clear
  on(
    fromMetadataCustomVisualizationActions.clearMetadataCustomVisualization,
    (state) => {
      return adapter.removeAll({
        ...state,
        isLoadingList: false,
        pagination: { empty: true },
        error: { error: false },
      });
    },
  ),
);

export function MetadataCustomVisualizationReducer(
  state: State | undefined,
  action: Action,
): any {
  return reducer(state, action);
}
