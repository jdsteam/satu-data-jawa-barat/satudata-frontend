import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { switchMap } from 'rxjs/operators';

import { fromMetadataCustomVisualizationActions } from '@store-v3/metadata-custom-visualization/metadata-custom-visualization.actions';
import { MetadataVisualizationService, PaginationService } from '@services-v3';
import { handlePromise } from '@helpers-v3';

@Injectable()
export class MetadataCustomVisualizationEffects {
  name = 'Metadata Custom Visualization';

  loadAllMetadataCustomVisualization$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        fromMetadataCustomVisualizationActions.loadAllMetadataCustomVisualization,
      ),
      switchMap(async (action) => {
        const { data: result, error: resultErr } = await handlePromise(
          this.metadataVisualizationService.getList(action.params).toPromise(),
        );

        if (resultErr) {
          return this.loadAllFailure(resultErr.message);
        }

        if (result.error === 1) {
          if (result.message === 'Data not found') {
            return fromMetadataCustomVisualizationActions.clearMetadataCustomVisualization();
          }
          return this.loadAllFailure(result.message);
        }

        let pagination = { empty: true };
        if (action.pagination) {
          const { data: total, error: totalErr } = await handlePromise(
            this.metadataVisualizationService
              .getTotal(action.params)
              .toPromise(),
          );

          if (totalErr) {
            return this.loadAllFailure(totalErr.message);
          }

          const resultPagination = this.paginationService.getPagination(
            total.data.count,
            action.params.skip,
            action.params.limit,
          );

          pagination = { empty: false, ...resultPagination };
        }

        return this.loadAllSuccess(result.data, pagination);
      }),
    ),
  );

  constructor(
    private actions$: Actions,
    private metadataVisualizationService: MetadataVisualizationService,
    private paginationService: PaginationService,
  ) {}

  loadAllSuccess(data: any, pagination: any): any {
    return fromMetadataCustomVisualizationActions.loadAllMetadataCustomVisualizationSuccess(
      {
        data,
        pagination,
      },
    );
  }

  loadAllFailure(error: string): any {
    return fromMetadataCustomVisualizationActions.loadAllMetadataCustomVisualizationFailure(
      {
        error: {
          name: this.name,
          error: true,
          message: error,
        },
      },
    );
  }
}
