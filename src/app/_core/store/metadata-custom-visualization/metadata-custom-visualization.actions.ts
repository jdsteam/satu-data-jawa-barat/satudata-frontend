import { createAction, props } from '@ngrx/store';
import { MetadataVisualizationData } from '@models-v3';

export enum MetadataCustomVisualizationActionTypes {
  LoadAllMetadataCustomVisualization = '[METADATA CUSTOM VISUALIZATION] Load All Metadata Custom Visualization',
  LoadAllMetadataCustomVisualizationSuccess = '[METADATA CUSTOM VISUALIZATION] Load All Metadata Custom Visualization Success',
  LoadAllMetadataCustomVisualizationFailure = '[METADATA CUSTOM VISUALIZATION] Load All Metadata Custom Visualization Failure',
  ClearMetadataCustomVisualization = '[METADATA CUSTOM VISUALIZATION] Clear Metadata Custom Visualization',
}

// Load All
export const loadAllMetadataCustomVisualization = createAction(
  MetadataCustomVisualizationActionTypes.LoadAllMetadataCustomVisualization,
  props<{ params: any; pagination: boolean; infinite: boolean }>(),
);

export const loadAllMetadataCustomVisualizationSuccess = createAction(
  MetadataCustomVisualizationActionTypes.LoadAllMetadataCustomVisualizationSuccess,
  props<{ data: MetadataVisualizationData[]; pagination: any }>(),
);

export const loadAllMetadataCustomVisualizationFailure = createAction(
  MetadataCustomVisualizationActionTypes.LoadAllMetadataCustomVisualizationFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearMetadataCustomVisualization = createAction(
  MetadataCustomVisualizationActionTypes.ClearMetadataCustomVisualization,
);

export const fromMetadataCustomVisualizationActions = {
  loadAllMetadataCustomVisualization,
  loadAllMetadataCustomVisualizationSuccess,
  loadAllMetadataCustomVisualizationFailure,
  clearMetadataCustomVisualization,
};
