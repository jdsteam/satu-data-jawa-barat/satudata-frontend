import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';

import { fromVisualizationActions } from '@store-v3/visualization/visualization.actions';
import { VisualizationService, PaginationService } from '@services-v3';
import { handlePromise } from '@helpers-v3';

@Injectable()
export class VisualizationEffects {
  name = 'Visualization';

  loadAllVisualization$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromVisualizationActions.loadAllVisualization),
      switchMap(async (action) => {
        const { data: result, error: resultErr } = await handlePromise(
          this.visualizationService.getList(action.params).toPromise(),
        );

        if (resultErr) {
          return this.loadAllFailure(resultErr.message);
        }

        if (result.error === 1) {
          if (result.message === 'Data not found') {
            return fromVisualizationActions.clearVisualization();
          }
          return this.loadAllFailure(result.message);
        }

        let pagination = { empty: true, infinite: action.infinite };
        if (action.pagination) {
          const resultPagination = this.paginationService.getPagination(
            result.meta.total_record,
            action.params.skip,
            action.params.limit,
          );

          pagination = {
            empty: false,
            infinite: action.infinite,
            ...resultPagination,
          };
        }

        const suggestion = {
          suggestion: result.suggestion,
          original_text: result.original_text,
          suggestion_text: result.suggestion_text,
        };

        return this.loadAllSuccess(result.data, pagination, suggestion);
      }),
    ),
  );

  loadVisualization$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromVisualizationActions.loadVisualization),
      switchMap((action) => {
        return this.visualizationService
          .getSingle(action.id, action.params)
          .pipe(
            map((result: any) => {
              if (result.error === 1) {
                if (result.message === 'Data not found') {
                  return fromVisualizationActions.clearVisualization();
                }
                of(
                  fromVisualizationActions.loadVisualizationFailure({
                    error: {
                      name: this.name,
                      error: true,
                      message: result.message,
                    },
                  }),
                );
              }

              return fromVisualizationActions.loadVisualizationSuccess({
                data: result.data,
              });
            }),
            catchError((error: HttpErrorResponse) =>
              of(
                fromVisualizationActions.loadVisualizationFailure({
                  error: {
                    name: this.name,
                    error: !error.ok,
                    message: error.message,
                  },
                }),
              ),
            ),
          );
      }),
    ),
  );

  loadVisualizationByTitle$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromVisualizationActions.loadVisualizationByTitle),
      switchMap((action) => {
        return this.visualizationService
          .getSingle(action.title, action.params)
          .pipe(
            map((result: any) =>
              fromVisualizationActions.loadVisualizationByTitleSuccess({
                data: result.data,
              }),
            ),
            catchError((error: HttpErrorResponse) =>
              of(
                fromVisualizationActions.loadVisualizationByTitleFailure({
                  error: {
                    name: this.name,
                    error: !error.ok,
                    message: error.message,
                  },
                }),
              ),
            ),
          );
      }),
    ),
  );

  updateVisualization$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromVisualizationActions.updateVisualization),
      switchMap((action) =>
        this.visualizationService
          .updateItem(action.update.id, action.update)
          .pipe(
            map((res: any) => {
              return fromVisualizationActions.updateVisualizationSuccess({
                data: res.data,
              });
            }),
            catchError((error) => {
              return of(
                fromVisualizationActions.updateVisualizationFailure({
                  error: {
                    name: this.name,
                    error: true,
                    message: error.message,
                  },
                }),
              );
            }),
          ),
      ),
    ),
  );

  counterVisualization$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromVisualizationActions.counterVisualization),
      switchMap((action) =>
        this.visualizationService.getCounter(action.id, action.params).pipe(
          map((res: any) =>
            fromVisualizationActions.counterVisualizationSuccess({
              data: res.data,
            }),
          ),
          catchError((error: HttpErrorResponse) =>
            of(
              fromVisualizationActions.counterVisualizationFailure({
                error: {
                  name: this.name,
                  error: !error.ok,
                  message: error.message,
                },
              }),
            ),
          ),
        ),
      ),
    ),
  );

  counterVisualizationByTitle$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromVisualizationActions.counterVisualizationByTitle),
      switchMap((action) =>
        this.visualizationService.getCounter(action.title, action.params).pipe(
          map((res: any) =>
            fromVisualizationActions.counterVisualizationByTitleSuccess({
              data: res.data,
            }),
          ),
          catchError((error: HttpErrorResponse) =>
            of(
              fromVisualizationActions.counterVisualizationByTitleFailure({
                error: {
                  name: this.name,
                  error: !error.ok,
                  message: error.message,
                },
              }),
            ),
          ),
        ),
      ),
    ),
  );

  constructor(
    private actions$: Actions,
    private visualizationService: VisualizationService,
    private paginationService: PaginationService,
  ) {}

  loadAllSuccess(data: any, pagination: any, suggestion: any): any {
    return fromVisualizationActions.loadAllVisualizationSuccess({
      data,
      pagination,
      suggestion,
    });
  }

  loadAllFailure(error: string): any {
    return fromVisualizationActions.loadAllVisualizationFailure({
      error: {
        name: this.name,
        error: true,
        message: error,
      },
    });
  }
}
