import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { VisualizationData } from '@models-v3';
import { fromVisualizationActions } from '@store-v3/visualization/visualization.actions';

export const ENTITY_FEATURE_KEY = 'visualizationV3';

export interface State extends EntityState<VisualizationData> {
  isLoadingList: boolean;
  isLoadingCreate: boolean;
  isLoadingRead: boolean;
  isLoadingUpdate: boolean;
  isLoadingDelete: boolean;
  isLoadingCounter: boolean;
  metadata: any;
  suggestion: any;
  pagination: any;
  error?: Error | any;
}

export const adapter: EntityAdapter<VisualizationData> =
  createEntityAdapter<VisualizationData>({
    selectId: (item) => item.id,
    // selectId: (item) => String(item.title),
  });

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  isLoadingList: false,
  isLoadingCreate: false,
  isLoadingRead: false,
  isLoadingUpdate: false,
  isLoadingDelete: false,
  isLoadingCounter: false,
  metadata: null,
  suggestion: null,
  pagination: null,
  error: null,
});

const reducer = createReducer(
  initialState,

  // Load All
  on(fromVisualizationActions.loadAllVisualization, (state) => {
    return {
      ...state,
      isLoadingList: true,
      error: null,
    };
  }),
  on(
    fromVisualizationActions.loadAllVisualizationSuccess,
    (state, { data, pagination, suggestion }) => {
      if (!pagination.infinite) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          suggestion,
          error: { error: false },
        });
      }

      if (pagination.current_page === 1) {
        return adapter.setAll(data, {
          ...state,
          isLoadingList: false,
          pagination,
          suggestion,
          error: { error: false },
        });
      }

      return adapter.addMany(data, {
        ...state,
        isLoadingList: false,
        pagination,
        suggestion,
        error: { error: false },
      });
    },
  ),
  on(
    fromVisualizationActions.loadAllVisualizationFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingList: false,
        error,
      };
    },
  ),

  // Load Single & Load Single By Title
  on(
    fromVisualizationActions.loadVisualization,
    fromVisualizationActions.loadVisualizationByTitle,
    (state) => {
      return {
        ...state,
        isLoadingRead: true,
        error: null,
      };
    },
  ),
  on(
    fromVisualizationActions.loadVisualizationSuccess,
    fromVisualizationActions.loadVisualizationByTitleSuccess,
    (state, { data }) => {
      return adapter.addOne(data, {
        ...state,
        isLoadingRead: false,
        error: { error: false },
      });
    },
  ),
  on(
    fromVisualizationActions.loadVisualizationFailure,
    fromVisualizationActions.loadVisualizationByTitleFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingRead: false,
        error,
      };
    },
  ),

  // Update
  on(fromVisualizationActions.updateVisualization, (state) => {
    return {
      ...state,
      isLoadingUpdate: true,
      error: null,
    };
  }),
  on(fromVisualizationActions.updateVisualizationSuccess, (state, { data }) => {
    return adapter.updateOne(
      { id: data.id, changes: data },
      {
        ...state,
        isLoadingUpdate: false,
        error: { error: false },
      },
    );
  }),
  on(
    fromVisualizationActions.updateVisualizationFailure,
    (state, { error }) => {
      return {
        ...state,
        error,
      };
    },
  ),

  // Counter
  on(
    fromVisualizationActions.counterVisualization,
    fromVisualizationActions.counterVisualizationByTitle,
    (state) => {
      return {
        ...state,
        isLoadingCounter: true,
        error: null,
      };
    },
  ),
  on(
    fromVisualizationActions.counterVisualizationSuccess,
    (state, { data }) => {
      return adapter.updateOne(
        { id: data.id, changes: data },
        {
          ...state,
          isLoadingCounter: false,
          error: { error: false },
        },
      );
    },
  ),
  on(
    fromVisualizationActions.counterVisualizationFailure,
    fromVisualizationActions.counterVisualizationByTitleFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingCounter: false,
        error,
      };
    },
  ),

  // Counter By Title
  on(
    fromVisualizationActions.counterVisualizationByTitleSuccess,
    (state, { data }) => {
      return adapter.updateOne(
        { id: data.title, changes: data },
        {
          ...state,
          isLoadingCounter: false,
          error: { error: false },
        },
      );
    },
  ),

  // Clear
  on(fromVisualizationActions.clearVisualization, (state) => {
    return adapter.removeAll({
      ...state,
      isLoadingList: false,
      pagination: { empty: true },
      suggestion: {},
      error: { error: false },
    });
  }),
);

export function VisualizationReducer(
  state: State | undefined,
  action: Action,
): any {
  return reducer(state, action);
}
