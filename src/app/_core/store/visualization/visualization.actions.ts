import { createAction, props } from '@ngrx/store';
import { VisualizationData } from '@models-v3';

export enum VisualizationActionTypes {
  LoadAllVisualization = '[VISUALIZATION] Load All Visualization',
  LoadAllVisualizationSuccess = '[VISUALIZATION] Load All Visualization Success',
  LoadAllVisualizationFailure = '[VISUALIZATION] Load All Visualization Failure',
  LoadVisualization = '[VISUALIZATION] Load Visualization',
  LoadVisualizationSuccess = '[VISUALIZATION] Load Visualization Success',
  LoadVisualizationFailure = '[VISUALIZATION] Load Visualization Failure',
  LoadVisualizationByTitle = '[VISUALIZATION] Load Visualization By Title',
  LoadVisualizationByTitleSuccess = '[VISUALIZATION] Load Visualization By Title Success',
  LoadVisualizationByTitleFailure = '[VISUALIZATION] Load Visualization By Title Failure',
  UpdateVisualization = '[VISUALIZATION] Update Visualization',
  UpdateVisualizationSuccess = '[VISUALIZATION] Update Visualization Success',
  UpdateVisualizationFailure = '[VISUALIZATION] Update Visualization Failure',
  CounterVisualization = '[VISUALIZATION] Counter Visualization',
  CounterVisualizationSuccess = '[VISUALIZATION] Counter Visualization Success',
  CounterVisualizationFailure = '[VISUALIZATION] Counter Visualization Failure',
  CounterVisualizationByTitle = '[VISUALIZATION] Counter Visualization By Title',
  CounterVisualizationByTitleSuccess = '[VISUALIZATION] Counter Visualization By Title Success',
  CounterVisualizationByTitleFailure = '[VISUALIZATION] Counter Visualization By Title Failure',
  ClearVisualization = '[VISUALIZATION] Clear Visualization',
}

// Load All
export const loadAllVisualization = createAction(
  VisualizationActionTypes.LoadAllVisualization,
  props<{ params: any; pagination: boolean; infinite: boolean }>(),
);

export const loadAllVisualizationSuccess = createAction(
  VisualizationActionTypes.LoadAllVisualizationSuccess,
  props<{ data: VisualizationData[]; pagination: any; suggestion: any }>(),
);

export const loadAllVisualizationFailure = createAction(
  VisualizationActionTypes.LoadAllVisualizationFailure,
  props<{ error: Error | any }>(),
);

// Load Single
export const loadVisualization = createAction(
  VisualizationActionTypes.LoadVisualization,
  props<{ id: number; params: any }>(),
);

export const loadVisualizationSuccess = createAction(
  VisualizationActionTypes.LoadVisualizationSuccess,
  props<{ data: VisualizationData }>(),
);

export const loadVisualizationFailure = createAction(
  VisualizationActionTypes.LoadVisualizationFailure,
  props<{ error: Error | any }>(),
);

// Load Single By Title
export const loadVisualizationByTitle = createAction(
  VisualizationActionTypes.LoadVisualizationByTitle,
  props<{ title: string; params: any }>(),
);

export const loadVisualizationByTitleSuccess = createAction(
  VisualizationActionTypes.LoadVisualizationByTitleSuccess,
  props<{ data: VisualizationData }>(),
);

export const loadVisualizationByTitleFailure = createAction(
  VisualizationActionTypes.LoadVisualizationByTitleFailure,
  props<{ error: Error | any }>(),
);

// Update
export const updateVisualization = createAction(
  VisualizationActionTypes.UpdateVisualization,
  props<{ update: any }>(),
);

export const updateVisualizationSuccess = createAction(
  VisualizationActionTypes.UpdateVisualizationSuccess,
  props<{ data: VisualizationData }>(),
);

export const updateVisualizationFailure = createAction(
  VisualizationActionTypes.UpdateVisualizationFailure,
  props<{ error: Error | any }>(),
);

// Counter
export const counterVisualization = createAction(
  VisualizationActionTypes.CounterVisualization,
  props<{ id: number; params: any }>(),
);

export const counterVisualizationSuccess = createAction(
  VisualizationActionTypes.CounterVisualizationSuccess,
  props<{ data: VisualizationData }>(),
);

export const counterVisualizationFailure = createAction(
  VisualizationActionTypes.CounterVisualizationFailure,
  props<{ error: Error | any }>(),
);

// Counter By Title
export const counterVisualizationByTitle = createAction(
  VisualizationActionTypes.CounterVisualizationByTitle,
  props<{ title: string; params: any }>(),
);

export const counterVisualizationByTitleSuccess = createAction(
  VisualizationActionTypes.CounterVisualizationByTitleSuccess,
  props<{ data: VisualizationData }>(),
);

export const counterVisualizationByTitleFailure = createAction(
  VisualizationActionTypes.CounterVisualizationByTitleFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearVisualization = createAction(
  VisualizationActionTypes.ClearVisualization,
);

export const fromVisualizationActions = {
  loadAllVisualization,
  loadAllVisualizationSuccess,
  loadAllVisualizationFailure,
  loadVisualization,
  loadVisualizationSuccess,
  loadVisualizationFailure,
  loadVisualizationByTitle,
  loadVisualizationByTitleSuccess,
  loadVisualizationByTitleFailure,
  updateVisualization,
  updateVisualizationSuccess,
  updateVisualizationFailure,
  counterVisualization,
  counterVisualizationSuccess,
  counterVisualizationFailure,
  counterVisualizationByTitle,
  counterVisualizationByTitleSuccess,
  counterVisualizationByTitleFailure,
  clearVisualization,
};
