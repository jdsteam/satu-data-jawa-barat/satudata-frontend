import { createAction, props } from '@ngrx/store';
import { UnitData } from '@models-v3';

export enum UnitActionTypes {
  LoadAllUnit = '[UNIT] Load All Unit',
  LoadAllUnitSuccess = '[UNIT] Load All Unit Success',
  LoadAllUnitFailure = '[UNIT] Load All Unit Failure',
  LoadUnit = '[UNIT] Load Unit',
  LoadUnitSuccess = '[UNIT] Load Unit Success',
  LoadUnitFailure = '[UNIT] Load Unit Failure',
  CreateUnit = '[UNIT] Create Unit',
  CreateUnitSuccess = '[UNIT] Create Unit Success',
  CreateUnitFailure = '[UNIT] Create Unit Failure',
  ClearUnit = '[UNIT] Clear Unit',
}

// Load All
export const loadAllUnit = createAction(
  UnitActionTypes.LoadAllUnit,
  props<{ params: any; pagination: boolean; infinite: boolean }>(),
);

export const loadAllUnitSuccess = createAction(
  UnitActionTypes.LoadAllUnitSuccess,
  props<{ data: UnitData[]; pagination: any }>(),
);

export const loadAllUnitFailure = createAction(
  UnitActionTypes.LoadAllUnitFailure,
  props<{ error: Error | any }>(),
);

// Load Single
export const loadUnit = createAction(
  UnitActionTypes.LoadUnit,
  props<{ id: number; params: any }>(),
);

export const loadUnitSuccess = createAction(
  UnitActionTypes.LoadUnitSuccess,
  props<{ data: UnitData }>(),
);

export const loadUnitFailure = createAction(
  UnitActionTypes.LoadUnitFailure,
  props<{ error: Error | any }>(),
);

// Create
export const createUnit = createAction(
  UnitActionTypes.CreateUnit,
  props<{ create: any }>(),
);

export const createUnitSuccess = createAction(
  UnitActionTypes.CreateUnitSuccess,
  props<{ data: UnitData }>(),
);

export const createUnitFailure = createAction(
  UnitActionTypes.CreateUnitFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearUnit = createAction(UnitActionTypes.ClearUnit);

export const fromUnitActions = {
  loadAllUnit,
  loadAllUnitSuccess,
  loadAllUnitFailure,
  loadUnit,
  loadUnitSuccess,
  loadUnitFailure,
  createUnit,
  createUnitSuccess,
  createUnitFailure,
  clearUnit,
};
