import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { UnitData } from '@models-v3';
import { fromUnitActions } from '@store-v3/unit/unit.actions';

export const ENTITY_FEATURE_KEY = 'unitV3';

export interface State extends EntityState<UnitData> {
  isLoadingList: boolean;
  isLoadingCreate: boolean;
  isLoadingRead: boolean;
  isLoadingUpdate: boolean;
  isLoadingDelete: boolean;
  metadata: any;
  pagination: any;
  error?: Error | any;
}

export const adapter: EntityAdapter<UnitData> = createEntityAdapter<UnitData>({
  selectId: (item) => item.id,
});

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  isLoadingList: false,
  isLoadingCreate: false,
  isLoadingRead: false,
  isLoadingUpdate: false,
  isLoadingDelete: false,
  metadata: null,
  pagination: null,
  error: null,
});

const reducer = createReducer(
  initialState,

  // Load All
  on(fromUnitActions.loadAllUnit, (state) => {
    return {
      ...state,
      isLoadingList: true,
    };
  }),
  on(fromUnitActions.loadAllUnitSuccess, (state, { data, pagination }) => {
    if (!pagination.infinite) {
      return adapter.setAll(data, {
        ...state,
        isLoadingList: false,
        pagination,
        error: { error: false },
      });
    }

    if (pagination.current_page === 1) {
      return adapter.setAll(data, {
        ...state,
        isLoadingList: false,
        pagination,
        error: { error: false },
      });
    }

    return adapter.addMany(data, {
      ...state,
      isLoadingList: false,
      pagination,
      error: { error: false },
    });
  }),
  on(fromUnitActions.loadAllUnitFailure, (state, { error }) => {
    return {
      ...state,
      isLoadingList: false,
      error,
    };
  }),

  // Load Single
  on(fromUnitActions.loadUnit, (state) => {
    return {
      ...state,
      isLoadingRead: true,
    };
  }),
  on(fromUnitActions.loadUnitSuccess, (state, { data }) => {
    return adapter.addOne(data, {
      ...state,
      isLoadingRead: false,
      error: { error: false },
    });
  }),
  on(fromUnitActions.loadUnitFailure, (state, { error }) => {
    return {
      ...state,
      isLoadingRead: false,
      error,
    };
  }),

  // Create
  on(fromUnitActions.createUnit, (state) => {
    return {
      ...state,
      isLoadingCreate: true,
    };
  }),
  on(fromUnitActions.createUnitSuccess, (state, { data }) => {
    return adapter.addOne(data, {
      ...state,
      isLoadingCreate: false,
      error: { error: false },
    });
  }),
  on(fromUnitActions.createUnitFailure, (state, { error }) => {
    return {
      ...state,
      isLoadingCreate: false,
      error,
    };
  }),

  // Clear
  on(fromUnitActions.clearUnit, (state) => {
    return adapter.removeAll({
      ...state,
      isLoadingList: false,
      pagination: { empty: true },
      error: { error: false },
    });
  }),
);

export function UnitReducer(state: State | undefined, action: Action): any {
  return reducer(state, action);
}
