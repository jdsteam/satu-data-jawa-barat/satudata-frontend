import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';

import { fromUnitActions } from '@store-v3/unit/unit.actions';
import { UnitService, PaginationService } from '@services-v3';
import { handlePromise } from '@helpers-v3';

@Injectable()
export class UnitEffects {
  name = 'Unit';

  loadAllUnit$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromUnitActions.loadAllUnit),
      switchMap(async (action) => {
        const { data: result, error: resultErr } = await handlePromise(
          this.unitService.getList(action.params).toPromise(),
        );

        if (resultErr) {
          return this.loadAllFailure(resultErr.message);
        }

        if (result.error === 1) {
          if (result.message === 'Data not found') {
            return fromUnitActions.clearUnit();
          }

          return this.loadAllFailure(result.message);
        }

        let pagination = { empty: true, infinite: action.infinite };
        if (action.pagination) {
          const resultPagination = this.paginationService.getPagination(
            result.meta.total_record,
            action.params.skip,
            action.params.limit,
          );

          pagination = {
            empty: false,
            infinite: action.infinite,
            ...resultPagination,
          };
        }

        return this.loadAllSuccess(result.data, pagination);
      }),
    ),
  );

  loadUnit$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromUnitActions.loadUnit),
      switchMap((action) => {
        return this.unitService.getSingle(action.id, action.params).pipe(
          map((result: any) =>
            fromUnitActions.loadUnitSuccess({
              data: result.data,
            }),
          ),
          catchError((error: HttpErrorResponse) =>
            of(
              fromUnitActions.loadUnitFailure({
                error: {
                  name: this.name,
                  error: !error.ok,
                  message: error.message,
                },
              }),
            ),
          ),
        );
      }),
    ),
  );

  createUnit$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromUnitActions.createUnit),
      switchMap((action) =>
        this.unitService.createItem(action.create).pipe(
          map((res: any) => {
            return fromUnitActions.createUnitSuccess({
              data: res.data,
            });
          }),
          catchError((error) => {
            return of(
              fromUnitActions.createUnitFailure({
                error: {
                  name: this.name,
                  error: !error.ok,
                  message: error.message,
                },
              }),
            );
          }),
        ),
      ),
    ),
  );

  constructor(
    private actions$: Actions,
    private unitService: UnitService,
    private paginationService: PaginationService,
  ) {}

  loadAllSuccess(data: any, pagination: any): any {
    return fromUnitActions.loadAllUnitSuccess({
      data,
      pagination,
    });
  }

  loadAllFailure(error: string): any {
    return fromUnitActions.loadAllUnitFailure({
      error: {
        name: this.name,
        error: true,
        message: error,
      },
    });
  }
}
