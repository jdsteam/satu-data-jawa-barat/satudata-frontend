import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { switchMap } from 'rxjs/operators';

import { fromHistoryLoginActions } from '@store-v3/history-login/history-login.actions';
import { HistoryLoginService, PaginationService } from '@services-v3';
import { handlePromise } from '@helpers-v3';

@Injectable()
export class HistoryLoginEffects {
  name = 'History Login';

  loadAllHistoryLogin$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromHistoryLoginActions.loadAllHistoryLogin),
      switchMap(async (action) => {
        const { data: result, error: resultErr } = await handlePromise(
          this.historyLoginService.getList(action.params).toPromise(),
        );

        if (resultErr) {
          return this.loadAllFailure(resultErr.message);
        }

        if (result.error === 1) {
          if (result.message === 'Data not found') {
            return fromHistoryLoginActions.clearHistoryLogin();
          }

          return this.loadAllFailure(result.message);
        }

        let pagination = { empty: true };
        if (action.pagination) {
          const { data: total, error: totalErr } = await handlePromise(
            this.historyLoginService.getTotal(action.params).toPromise(),
          );

          if (totalErr) {
            return this.loadAllFailure(totalErr.message);
          }

          const resultPagination = this.paginationService.getPagination(
            total.data.count,
            action.params.skip,
            action.params.limit,
          );

          pagination = { empty: false, ...resultPagination };
        }

        return this.loadAllSuccess(result.data, pagination);
      }),
    ),
  );

  constructor(
    private actions$: Actions,
    private historyLoginService: HistoryLoginService,
    private paginationService: PaginationService,
  ) {}

  loadAllSuccess(data: any, pagination: any): any {
    return fromHistoryLoginActions.loadAllHistoryLoginSuccess({
      data,
      pagination,
    });
  }

  loadAllFailure(error: string): any {
    return fromHistoryLoginActions.loadAllHistoryLoginFailure({
      error: {
        name: this.name,
        error: true,
        message: error,
      },
    });
  }
}
