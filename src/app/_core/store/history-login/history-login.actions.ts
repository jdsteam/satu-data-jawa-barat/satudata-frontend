import { createAction, props } from '@ngrx/store';
import { HistoryLoginData } from '@models-v3';

export enum HistoryLoginActionTypes {
  LoadAllHistoryLogin = '[HISTORY LOGIN] Load All History Login',
  LoadAllHistoryLoginSuccess = '[HISTORY LOGIN] Load All History Login Success',
  LoadAllHistoryLoginFailure = '[HISTORY LOGIN] Load All History Login Failure',
  ClearHistoryLogin = '[HISTORY LOGIN] Clear History Login',
}

// Load All
export const loadAllHistoryLogin = createAction(
  HistoryLoginActionTypes.LoadAllHistoryLogin,
  props<{ params: any; pagination: boolean }>(),
);

export const loadAllHistoryLoginSuccess = createAction(
  HistoryLoginActionTypes.LoadAllHistoryLoginSuccess,
  props<{ data: HistoryLoginData[]; pagination: any }>(),
);

export const loadAllHistoryLoginFailure = createAction(
  HistoryLoginActionTypes.LoadAllHistoryLoginFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearHistoryLogin = createAction(
  HistoryLoginActionTypes.ClearHistoryLogin,
);

export const fromHistoryLoginActions = {
  loadAllHistoryLogin,
  loadAllHistoryLoginSuccess,
  loadAllHistoryLoginFailure,
  clearHistoryLogin,
};
