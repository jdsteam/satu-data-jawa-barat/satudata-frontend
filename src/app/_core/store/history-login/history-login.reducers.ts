import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { HistoryLoginData } from '@models-v3';
import { fromHistoryLoginActions } from '@store-v3/history-login/history-login.actions';

export const ENTITY_FEATURE_KEY = 'historyLoginV3';

export interface State extends EntityState<HistoryLoginData> {
  isLoadingList: boolean;
  isLoadingCreate: boolean;
  isLoadingRead: boolean;
  isLoadingUpdate: boolean;
  isLoadingDelete: boolean;
  metadata: any;
  pagination: any;
  error?: Error | any;
}

export const adapter: EntityAdapter<HistoryLoginData> =
  createEntityAdapter<HistoryLoginData>({
    selectId: (item) => item.id,
  });

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  isLoadingList: false,
  isLoadingCreate: true,
  isLoadingRead: true,
  isLoadingUpdate: true,
  isLoadingDelete: true,
  metadata: null,
  pagination: null,
  error: null,
});

const reducer = createReducer(
  initialState,

  // Load All
  on(fromHistoryLoginActions.loadAllHistoryLogin, (state) => {
    return {
      ...state,
      isLoadingList: true,
    };
  }),
  on(
    fromHistoryLoginActions.loadAllHistoryLoginSuccess,
    (state, { data, pagination }) => {
      return adapter.setAll(data, {
        ...state,
        isLoadingList: false,
        pagination,
        error: { error: false },
      });
    },
  ),
  on(fromHistoryLoginActions.loadAllHistoryLoginFailure, (state, { error }) => {
    return {
      ...state,
      isLoadingList: false,
      error,
    };
  }),

  // Clear
  on(fromHistoryLoginActions.clearHistoryLogin, (state) => {
    return adapter.removeAll({
      ...state,
      isLoadingList: false,
      pagination: { empty: true },
      error: { error: false },
    });
  }),
);

export function HistoryLoginReducer(
  state: State | undefined,
  action: Action,
): any {
  return reducer(state, action);
}
