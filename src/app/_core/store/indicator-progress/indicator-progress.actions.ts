import { createAction, props } from '@ngrx/store';
import { IndicatorProgressData } from '@models-v3';

export enum IndicatorProgressActionTypes {
  CreateIndicatorProgress = '[INDICATOR PROGRESS] Create Indicator Progress',
  CreateIndicatorProgressSuccess = '[INDICATOR PROGRESS] Create Indicator Progress Success',
  CreateIndicatorProgressFailure = '[INDICATOR PROGRESS] Create Indicator Progress Failure',
  UpdateIndicatorProgress = '[INDICATOR PROGRESS] Update Indicator Progress',
  UpdateIndicatorProgressSuccess = '[INDICATOR PROGRESS] Update Indicator Progress Success',
  UpdateIndicatorProgressFailure = '[INDICATOR PROGRESS] Update Indicator Progress Failure',
  UpdateIndicatorProgressBulk = '[INDICATOR PROGRESS] Update Indicator Progress Bulk',
  UpdateIndicatorProgressBulkSuccess = '[INDICATOR PROGRESS] Update Indicator Progress Bulk Success',
  UpdateIndicatorProgressBulkFailure = '[INDICATOR PROGRESS] Update Indicator Progress Bulk Failure',
  ClearIndicatorProgress = '[INDICATOR PROGRESS] Clear Indicator Progress',
}

// Create
export const createIndicatorProgress = createAction(
  IndicatorProgressActionTypes.CreateIndicatorProgress,
  props<{ create: any; indicator: any; progress: any }>(),
);

export const createIndicatorProgressSuccess = createAction(
  IndicatorProgressActionTypes.CreateIndicatorProgressSuccess,
  props<{ data: IndicatorProgressData }>(),
);

export const createIndicatorProgressFailure = createAction(
  IndicatorProgressActionTypes.CreateIndicatorProgressFailure,
  props<{ error: Error | any }>(),
);

// Update
export const updateIndicatorProgress = createAction(
  IndicatorProgressActionTypes.UpdateIndicatorProgress,
  props<{ indicatorId: number; update: any; params: any }>(),
);

export const updateIndicatorProgressSuccess = createAction(
  IndicatorProgressActionTypes.UpdateIndicatorProgressSuccess,
  props<{ data: IndicatorProgressData }>(),
);

export const updateIndicatorProgressFailure = createAction(
  IndicatorProgressActionTypes.UpdateIndicatorProgressFailure,
  props<{ error: Error | any }>(),
);

// Update Bulk
export const updateIndicatorProgressBulk = createAction(
  IndicatorProgressActionTypes.UpdateIndicatorProgressBulk,
  props<{ indicatorId: number; update: any; params: any }>(),
);

export const updateIndicatorProgressBulkSuccess = createAction(
  IndicatorProgressActionTypes.UpdateIndicatorProgressBulkSuccess,
  props<{ data: IndicatorProgressData[] }>(),
);

export const updateIndicatorProgressBulkFailure = createAction(
  IndicatorProgressActionTypes.UpdateIndicatorProgressBulkFailure,
  props<{ error: Error | any }>(),
);

// Clear
export const clearIndicatorProgress = createAction(
  IndicatorProgressActionTypes.ClearIndicatorProgress,
);

export const fromIndicatorProgressActions = {
  createIndicatorProgress,
  createIndicatorProgressSuccess,
  createIndicatorProgressFailure,
  updateIndicatorProgress,
  updateIndicatorProgressSuccess,
  updateIndicatorProgressFailure,
  updateIndicatorProgressBulk,
  updateIndicatorProgressBulkSuccess,
  updateIndicatorProgressBulkFailure,
  clearIndicatorProgress,
};
