import { createFeatureSelector, createSelector } from '@ngrx/store';
import {
  State,
  adapter,
  ENTITY_FEATURE_KEY,
} from '@store-v3/indicator-progress/indicator-progress.reducers';

// Lookup feature state managed by NgRx
const getState = createFeatureSelector<State>(ENTITY_FEATURE_KEY);

// get the selectors
const { selectIds, selectAll, selectEntities } = adapter.getSelectors();

// select the array of ids
export const selectIndicatorProgressIds = createSelector(getState, selectIds);

// select the array
export const selectAllIndicatorProgress = createSelector(getState, selectAll);

export const getIndicatorProgressEntities = createSelector(
  getState,
  (state: State) => selectEntities(state),
);

// select by id
export const selectIndicatorProgress = (id: number | string) =>
  createSelector(getIndicatorProgressEntities, (entities) => {
    return entities[id];
  });

// select the by title
export const selectIndicatorProgressByTitle = (title: number | string) =>
  createSelector(getIndicatorProgressEntities, (entities) => {
    return entities[title];
  });

// select the by multiple id
export const selectMultipleIndicatorProgress = (ids: any[]) =>
  createSelector(getIndicatorProgressEntities, (entities) => {
    const filtered = Object.keys(entities)
      .filter((key) => ids.includes(key))
      .reduce((obj, key) => {
        // eslint-disable-next-line no-param-reassign
        obj[key] = entities[key];
        return obj;
      }, {});

    return Object.keys(filtered).map((key) => filtered[key]);
  });

// select loaded flag
export const selectIsLoadingList = createSelector(
  getState,
  (state) => state.isLoadingList,
);
export const selectIsLoadingCreate = createSelector(
  getState,
  (state) => state.isLoadingCreate,
);
export const selectIsLoadingRead = createSelector(
  getState,
  (state) => state.isLoadingRead,
);
export const selectIsLoadingUpdate = createSelector(
  getState,
  (state) => state.isLoadingUpdate,
);
export const selectIsLoadingDelete = createSelector(
  getState,
  (state) => state.isLoadingDelete,
);
export const selectIsLoadingCounter = createSelector(
  getState,
  (state) => state.isLoadingCounter,
);

// select metadata
export const selectMetadata = createSelector(
  getState,
  (state) => state.metadata,
);

// select pagination
export const selectPagination = createSelector(
  getState,
  (state) => state.pagination,
);

// select error
export const selectError = createSelector(getState, (state) => state.error);
