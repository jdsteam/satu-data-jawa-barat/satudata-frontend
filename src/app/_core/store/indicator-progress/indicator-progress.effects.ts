import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';

import { fromIndicatorProgressActions } from '@store-v3/indicator-progress/indicator-progress.actions';
import { IndicatorProgressService } from '@services-v3';

import * as _ from 'lodash';

@Injectable()
export class IndicatorProgressEffects {
  name = 'Indicator Progress';

  createIndicatorProgress$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromIndicatorProgressActions.createIndicatorProgress),
      switchMap((action) => {
        const indicatorId = action.indicator.id;
        const indexHelper = action.progress === 'initial' ? 1 : 99;

        const bodyProgress = [];
        _.map(action.create, (result, index) => {
          bodyProgress.push({
            indikator_id: indicatorId,
            year: result.year,
            target: result.target || null,
            capaian: result.capaian || null,
            notes: result.notes,
            index: index + indexHelper,
          });
        });

        return this.indicatorProgressService.createItem(bodyProgress).pipe(
          map((res: any) => {
            return fromIndicatorProgressActions.createIndicatorProgressSuccess({
              data: res.data,
            });
          }),
          catchError((error) => {
            return of(
              fromIndicatorProgressActions.createIndicatorProgressFailure({
                error: {
                  name: this.name,
                  error: !error.ok,
                  message: error.message,
                },
              }),
            );
          }),
        );
      }),
    ),
  );

  updateIndicatorProgress$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromIndicatorProgressActions.updateIndicatorProgress),
      switchMap((action) =>
        this.indicatorProgressService
          .updateItemBulk(action.indicatorId, action.update, action.params)
          .pipe(
            map((res: any) => {
              return fromIndicatorProgressActions.updateIndicatorProgressSuccess(
                {
                  data: res.data,
                },
              );
            }),
            catchError((error) => {
              return of(
                fromIndicatorProgressActions.updateIndicatorProgressFailure({
                  error: {
                    name: this.name,
                    error: !error.ok,
                    message: error.message,
                  },
                }),
              );
            }),
          ),
      ),
    ),
  );

  updateIndicatorProgressBulk$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromIndicatorProgressActions.updateIndicatorProgressBulk),
      switchMap((action) =>
        this.indicatorProgressService
          .updateItemBulk(action.indicatorId, action.update, action.params)
          .pipe(
            map((res: any) => {
              return fromIndicatorProgressActions.updateIndicatorProgressBulkSuccess(
                {
                  data: res.data,
                },
              );
            }),
            catchError((error) => {
              return of(
                fromIndicatorProgressActions.updateIndicatorProgressBulkFailure(
                  {
                    error: {
                      name: this.name,
                      error: !error.ok,
                      message: error.message,
                    },
                  },
                ),
              );
            }),
          ),
      ),
    ),
  );

  constructor(
    private actions$: Actions,
    private indicatorProgressService: IndicatorProgressService,
  ) {}
}
