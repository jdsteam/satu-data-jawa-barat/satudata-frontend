import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { IndicatorProgressData } from '@models-v3';
import { fromIndicatorProgressActions } from '@store-v3/indicator-progress/indicator-progress.actions';

export const ENTITY_FEATURE_KEY = 'indicatorProgressV3';

export interface State extends EntityState<IndicatorProgressData> {
  isLoadingList: boolean;
  isLoadingCreate: boolean;
  isLoadingRead: boolean;
  isLoadingUpdate: boolean;
  isLoadingDelete: boolean;
  isLoadingCounter: boolean;
  metadata: any;
  pagination: any;
  error?: Error | any;
}

export const adapter: EntityAdapter<IndicatorProgressData> =
  createEntityAdapter<IndicatorProgressData>({
    selectId: (item) => item.id,
  });

export interface EntityPartialState {
  readonly [ENTITY_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  isLoadingList: false,
  isLoadingCreate: false,
  isLoadingRead: false,
  isLoadingUpdate: false,
  isLoadingDelete: false,
  isLoadingCounter: false,
  metadata: null,
  pagination: null,
  error: null,
});

const reducer = createReducer(
  initialState,

  // Create
  on(fromIndicatorProgressActions.createIndicatorProgress, (state) => {
    return {
      ...state,
      isLoadingCreate: true,
    };
  }),
  on(
    fromIndicatorProgressActions.createIndicatorProgressSuccess,
    (state, { data }) => {
      return adapter.addOne(data, {
        ...state,
        isLoadingCreate: false,
        error: { error: false },
      });
    },
  ),
  on(
    fromIndicatorProgressActions.createIndicatorProgressFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingCreate: false,
        error,
      };
    },
  ),

  // Update
  on(
    fromIndicatorProgressActions.updateIndicatorProgress,
    fromIndicatorProgressActions.updateIndicatorProgressBulk,
    (state) => {
      return {
        ...state,
        isLoadingUpdate: true,
        error: null,
      };
    },
  ),
  on(
    fromIndicatorProgressActions.updateIndicatorProgressSuccess,
    (state, { data }) => {
      return adapter.updateOne(
        { id: data.id, changes: data },
        {
          ...state,
          isLoadingUpdate: false,
          error: { error: false },
        },
      );
    },
  ),
  on(
    fromIndicatorProgressActions.updateIndicatorProgressFailure,
    fromIndicatorProgressActions.updateIndicatorProgressBulkFailure,
    (state, { error }) => {
      return {
        ...state,
        isLoadingUpdate: false,
        error,
      };
    },
  ),

  // Update Bulk
  on(
    fromIndicatorProgressActions.updateIndicatorProgressBulkSuccess,
    (state, { data }) => {
      return adapter.updateMany(
        data.map((res) => ({ id: res.id, changes: res })),
        {
          ...state,
          isLoadingUpdate: false,
          error: { error: false },
        },
      );
    },
  ),

  // Clear
  on(fromIndicatorProgressActions.clearIndicatorProgress, (state) => {
    return adapter.removeAll({
      ...state,
      isLoadingList: false,
      pagination: { empty: true },
      error: { error: false },
    });
  }),
);

export function IndicatorProgressReducer(
  state: State | undefined,
  action: Action,
): any {
  return reducer(state, action);
}
