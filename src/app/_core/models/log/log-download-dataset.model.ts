/* eslint-disable @typescript-eslint/no-use-before-define */
import { Deserializable, PaginationData } from '@models-v3';

import moment from 'moment';

export class LogDownloadDataset implements Deserializable {
  [x: string]: any;
  message: string;
  data: LogDownloadDatasetData;
  error: number;
  pagination: PaginationData;

  deserialize(input: any): this {
    Object.assign(this, input);

    // eslint-disable-next-line @typescript-eslint/no-use-before-define
    this.data = input.data.map((res: any) =>
      new LogDownloadDatasetData().deserialize(res),
    );

    return this;
  }
}

export class LogDownloadDatasetData implements Deserializable {
  [x: string]: any;
  id: string;
  uuid: string;
  type: string;
  date_start: Date;
  date_end: Date;
  ip: string;
  user_agent: string;
  os: string;
  browser: string;
  device: string;
  os_version: string;
  browser_version: string;
  device_type: string;
  orientation: string;
  is_desktop: string;
  is_mobile: boolean;
  is_tablet: boolean;
  country_code: string;
  country_name: string;
  region_code: string;
  region_name: string;
  city_code: string;
  city_name: string;
  latitude: number;
  longitude: number;
  user_id: string;
  user_username: string;
  user_name: string;
  user_role: string;
  user_organization_id: string;
  user_organization_code: string;
  user_organization_name: string;
  user_position_id: string;
  user_position_code: string;
  user_position_name: string;
  dataset_id: number;
  dataset_title: string;
  dataset_name: string;
  dataset_organization_id: string;
  dataset_organization_code: string;
  dataset_organization_name: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }

  getRole() {
    let role: string;
    switch (this.user_role) {
      case 'walidata':
        role = 'Walidata';
        break;
      case 'opd':
        role = 'OPD Pengelola';
        break;
      case 'opd-view':
        role = 'OPD View';
        break;
      default:
        break;
    }

    return role;
  }

  getDateStart() {
    return moment(this.date_start).locale('id').format('HH:mm DD/MM/YYYY');
  }

  getDateEnd() {
    return moment(this.date_end).locale('id').format('HH:mm DD/MM/YYYY');
  }
}
