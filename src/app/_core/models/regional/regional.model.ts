import { Deserializable, RegionalLevel } from '@models-v3';

export class Regional implements Deserializable {
  [x: string]: any;
  message: string;
  data: RegionalData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class RegionalData implements Deserializable {
  [x: string]: any;
  id: number;
  regional_level: RegionalLevel;
  kode_bps: string;
  nama_bps: string;
  kode_kemendagri: string;
  nama_kemendagri: string;
  image: string;
  notes: string;
  is_active: boolean;
  is_deleted: boolean;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
