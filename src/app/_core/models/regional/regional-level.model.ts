import { Deserializable } from '@models-v3';

export class RegionalLevel implements Deserializable {
  [x: string]: any;
  message: string;
  data: RegionalLevelData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class RegionalLevelData implements Deserializable {
  [x: string]: any;
  id: number;
  name: string;
  notes: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
