import { Deserializable } from '@models-v3';

export class InfographicOwner implements Deserializable {
  [x: string]: any;
  message: string;
  data: InfographicOwnerData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
export class InfographicOwnerData implements Deserializable {
  [x: string]: any;
  owner: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
