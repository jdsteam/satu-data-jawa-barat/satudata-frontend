import { Deserializable } from '@models-v3';

export class InfographicDetail implements Deserializable {
  [x: string]: any;
  message: string;
  data: InfographicDetailData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
export class InfographicDetailData implements Deserializable {
  [x: string]: any;
  id: number;
  infographic_id: number;
  title: string;
  name: string;
  description: string;
  image: string;
  is_active: boolean;
  is_deleted: boolean;
  cuid: number;
  cdate: string;
  muid: number;
  mdate: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
