import {
  Deserializable,
  RegionalData,
  TopicData,
  ClassificationDatasetData,
  LicenseData,
  InfographicDetailData,
} from '@models-v3';

export class Infographic implements Deserializable {
  [x: string]: any;
  message: string;
  data: InfographicData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
export class InfographicData implements Deserializable {
  [x: string]: any;
  id: number;
  name: string;
  title: string;
  description: string;
  image: string;
  source: string;
  owner: string;
  owner_email: string;
  maintainer: string;
  maintainer_email: string;
  is_active: boolean;
  is_deleted: boolean;
  is_validate: boolean;
  count_view: number;
  count_rating: number;
  cuid: number;
  cdate: string;
  muid: number;
  mdate: string;
  regional: RegionalData;
  sektoral: TopicData;
  dataset_class: ClassificationDatasetData;
  license: LicenseData;
  infographic_detail: InfographicDetailData;
  keywords: any[];

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
