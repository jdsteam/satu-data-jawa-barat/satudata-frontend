import { Deserializable } from '@models-v3';

export class InfographicSource implements Deserializable {
  [x: string]: any;
  message: string;
  data: InfographicSourceData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
export class InfographicSourceData implements Deserializable {
  [x: string]: any;
  source: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
