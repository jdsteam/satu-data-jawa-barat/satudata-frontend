import { Deserializable } from '@models-v3';

export class Notification implements Deserializable {
  [x: string]: any;
  message: string;
  data: NotificationData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class NotificationData implements Deserializable {
  [x: string]: any;
  id: number;
  type: string;
  type_id: number;
  sender: number;
  reciver: number;
  title: string;
  content: string;
  is_read: boolean;
  is_enable: boolean;
  feature: number;
  cdate: Date;
  mdate: Date;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
