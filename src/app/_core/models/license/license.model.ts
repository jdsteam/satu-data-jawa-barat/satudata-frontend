import { Deserializable } from '@models-v3';

export class License implements Deserializable {
  [x: string]: any;
  message: string;
  data: LicenseData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class LicenseData implements Deserializable {
  [x: string]: any;
  id: number;
  name: string;
  notes: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
