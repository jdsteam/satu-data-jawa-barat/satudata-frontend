import { Deserializable } from '@models-v3';

export class Agreement implements Deserializable {
  [x: string]: any;
  message: string;
  data: AgreementData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class AgreementData implements Deserializable {
  [x: string]: any;
  id: number;
  version: string;
  agreement_text: string;
  notes: string;
  is_active: boolean;
  is_deleted: boolean;
  cdate: string;
  cuid: number;
  mdate: string;
  muid: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
