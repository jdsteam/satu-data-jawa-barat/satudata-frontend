import { Deserializable, RoleData } from '@models-v3';

export class Login implements Deserializable {
  [x: string]: any;
  message: string;
  data: LoginData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class LoginData implements Deserializable {
  [x: string]: any;
  id: number;
  email: string;
  username: string;
  nip: string;
  name: string;
  profile_pic: string;
  notes: string;
  last_login: Date;
  cdate: Date;
  cuid: number;
  is_active: boolean;
  is_deleted: boolean;
  logo_skpd: string;
  agreement_id: number;
  satuan_kerja_id: number;
  lv1_unit_kerja_id: string;
  lv2_unit_kerja_id: string;
  lv3_unit_kerja_id: string;
  lv4_unit_kerja_id: string;
  satuan_kerja_nama: string;
  lv1_unit_kerja_nama: string;
  lv2_unit_kerja_nama: string;
  lv3_unit_kerja_nama: string;
  lv4_unit_kerja_nama: string;
  level_unit_kerja: number;
  jabatan_id: string;
  jabatan_nama: string;
  role_id: number;
  role_name: string;
  role_title: string;
  regional_level: string;
  kode_kemendagri: string;
  nama_kemendagri: string;
  role: RoleData;
  kode_skpd: string;
  nama_skpd: string;
  iat: number;
  exp: number;
  jwt?: string;
  is_agree: boolean;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
