import { Deserializable } from '@models-v3';

export class Role implements Deserializable {
  [x: string]: any;
  message: string;
  data: RoleData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class RoleData implements Deserializable {
  [x: string]: any;
  id: number;
  name: string;
  title: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
