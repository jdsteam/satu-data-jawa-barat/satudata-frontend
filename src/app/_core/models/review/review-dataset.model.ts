import { Deserializable } from '@models-v3';

export class ReviewDataset implements Deserializable {
  [x: string]: any;
  message: string;
  data: ReviewDatasetData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class ReviewDatasetData implements Deserializable {
  [x: string]: any;
  id: number;
  type: string;
  type_id: number;
  user_id: number;
  rating: number;
  review: string;
  cuid: number;
  muid: number;
  cdate: Date;
  mdate: Date;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
