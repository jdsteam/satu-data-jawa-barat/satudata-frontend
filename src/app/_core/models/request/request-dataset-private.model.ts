import { Deserializable } from '@models-v3';

export class RequestDatasetPrivate implements Deserializable {
  [x: string]: any;
  message: string;
  data: RequestDatasetPrivateData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class RequestDatasetPrivateData implements Deserializable {
  [x: string]: any;
  id: number;
  user_id: number;
  nama: string;
  telp: string;
  email: string;
  judul_data: string;
  tau_skpd: boolean;
  nama_skpd: string;
  kebutuhan_data: string;
  tujuan_data: string;
  datetime: Date;
  status: number;
  notes: string;
  cdate: Date;
  mdate: Date;
  is_active: boolean;
  is_deleted: boolean;
  history: any;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
