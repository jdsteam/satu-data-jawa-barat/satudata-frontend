import { Deserializable } from '@models-v3';

export class RequestDatasetPublic implements Deserializable {
  [x: string]: any;
  message: string;
  data: RequestDatasetPublicData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class RequestDatasetPublicData implements Deserializable {
  [x: string]: any;
  id: number;
  nama: string;
  telp: string;
  email: string;
  pekerjaan: string;
  bidang: string;
  instansi: string;
  judul_data: string;
  tau_skpd: boolean;
  nama_skpd: string;
  kebutuhan_data: string;
  tujuan_data: string;
  datetime: Date;
  status: number;
  notes: string;
  cdate: Date;
  mdate: Date;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
