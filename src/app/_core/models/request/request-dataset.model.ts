import { Deserializable } from '@models-v3';

export class RequestDataset implements Deserializable {
  [x: string]: any;
  message: string;
  data: RequestDatasetData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class RequestDatasetData implements Deserializable {
  [x: string]: any;
  skpd_request: string;
  mdate: Date;
  username: string;
  cdate: Date;
  notes: string;
  id: number;
  nama_skpd: string;
  title: string;
  nama_skpd_alias: string;
  history_request: any;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
