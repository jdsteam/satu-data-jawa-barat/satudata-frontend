import { Deserializable } from '@models-v3';

export class BusinessField implements Deserializable {
  [x: string]: any;
  message: string;
  data: BusinessFieldData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class BusinessFieldData implements Deserializable {
  [x: string]: any;
  id!: number;
  kode_bidang_urusan!: string;
  nama_bidang_urusan!: string;
  notes!: string;
  cuid!: number;
  cdate!: Date;
  muid!: number;
  mdate!: Date;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
