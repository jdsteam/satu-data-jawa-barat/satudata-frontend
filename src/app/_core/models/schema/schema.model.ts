import { Deserializable } from '@models-v3';

export class Schema implements Deserializable {
  [x: string]: any;
  message: string;
  data: SchemaData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class SchemaData implements Deserializable {
  [x: string]: any;
  schema: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
