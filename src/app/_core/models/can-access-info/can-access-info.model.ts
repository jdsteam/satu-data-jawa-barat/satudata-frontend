import { Deserializable } from '@models-v3';

export class CanAccessInfo implements Deserializable {
  [x: string]: any;
  message: string;
  data: CanAccessInfoData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class CanAccessInfoData implements Deserializable {
  [x: string]: any;
  id: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
