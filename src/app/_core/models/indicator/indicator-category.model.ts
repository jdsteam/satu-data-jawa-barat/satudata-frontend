import { Deserializable } from '@models-v3';

export class IndicatorCategory implements Deserializable {
  [x: string]: any;
  message: string;
  data: IndicatorCategoryData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class IndicatorCategoryData implements Deserializable {
  [x: string]: any;
  name: string;
  notes: string;
  id: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
