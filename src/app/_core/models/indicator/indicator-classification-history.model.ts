import { Deserializable } from '@models-v3';

export class IndicatorClassificationHistory implements Deserializable {
  [x: string]: any;
  message: string;
  data: IndicatorClassificationHistoryData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class IndicatorClassificationHistoryData implements Deserializable {
  [x: string]: any;
  id: number;
  indikator_class_id: number;
  indikator_id: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
