import { Deserializable } from '@models-v3';

export class Classification implements Deserializable {
  [x: string]: any;
  message: string;
  data: ClassificationData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class ClassificationData implements Deserializable {
  [x: string]: any;
  id: number;
  name: string;
  notes: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
