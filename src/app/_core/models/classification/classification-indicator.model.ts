import { Deserializable } from '@models-v3';

export class ClassificationIndicator implements Deserializable {
  [x: string]: any;
  message: string;
  data: ClassificationIndicatorData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class ClassificationIndicatorData implements Deserializable {
  [x: string]: any;
  id: number;
  name: string;
  notes: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
