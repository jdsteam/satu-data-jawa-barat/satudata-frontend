import { Deserializable } from '@models-v3';

export class ClassificationDataset implements Deserializable {
  [x: string]: any;
  message: string;
  data: ClassificationDatasetData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class ClassificationDatasetData implements Deserializable {
  [x: string]: any;
  id: number;
  name: string;
  notes: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
