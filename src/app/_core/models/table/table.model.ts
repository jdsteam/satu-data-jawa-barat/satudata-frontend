import { Deserializable } from '@models-v3';

export class Table implements Deserializable {
  [x: string]: any;
  message: string;
  data: TableData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class TableData implements Deserializable {
  [x: string]: any;
  schema: string;
  table: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
