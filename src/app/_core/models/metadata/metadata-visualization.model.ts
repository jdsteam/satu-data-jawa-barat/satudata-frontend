import { Deserializable, MetadataType } from '@models-v3';

export class MetadataVisualization implements Deserializable {
  [x: string]: any;
  message: string;
  data: MetadataVisualizationData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class MetadataVisualizationData implements Deserializable {
  [x: string]: any;
  id: number;
  visualization_id: number;
  metadata_type_id: number;
  key: string;
  value: string;
  description: string;
  metadata_type: MetadataType;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
