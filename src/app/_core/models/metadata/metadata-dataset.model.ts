import { Deserializable, MetadataType } from '@models-v3';

export class MetadataDataset implements Deserializable {
  [x: string]: any;
  message: string;
  data: MetadataDatasetData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class MetadataDatasetData implements Deserializable {
  [x: string]: any;
  id: number;
  dataset_id: number;
  metadata_type_id: number;
  key: string;
  value: string;
  description: string;
  metadata_type: MetadataType;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
