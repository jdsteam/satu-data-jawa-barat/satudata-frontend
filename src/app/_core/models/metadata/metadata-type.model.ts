import { Deserializable } from '@models-v3';

export class MetadataType implements Deserializable {
  [x: string]: any;
  message: string;
  data: MetadataTypeData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class MetadataTypeData implements Deserializable {
  [x: string]: any;
  id: number;
  name: string;
  notes: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
