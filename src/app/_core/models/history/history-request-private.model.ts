import { Deserializable } from '@models-v3';

export class HistoryRequestPrivate implements Deserializable {
  [x: string]: any;
  message: string;
  data: HistoryRequestPrivateData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class HistoryRequestPrivateData implements Deserializable {
  [x: string]: any;
  request_private_id: number;
  status: number;
  id: number;
  notes: string;
  datetime: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
