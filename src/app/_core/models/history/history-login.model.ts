import { Deserializable } from '@models-v3';

export class HistoryLogin implements Deserializable {
  [x: string]: any;
  message: string;
  data: HistoryLoginData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class HistoryLoginData implements Deserializable {
  [x: string]: any;
  id: number;
  user_id: number;
  username: string;
  kode_skpd: string;
  nama_skpd: string;
  datetime: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
