import { Deserializable } from '@models-v3';

export class HistoryRequestPublic implements Deserializable {
  [x: string]: any;
  message: string;
  data: HistoryRequestPublicData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class HistoryRequestPublicData implements Deserializable {
  [x: string]: any;
  request_public_id: number;
  status: number;
  id: number;
  notes: string;
  datetime: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
