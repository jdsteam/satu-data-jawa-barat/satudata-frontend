import { Deserializable } from '@models-v3';

export class HistoryDraft implements Deserializable {
  [x: string]: any;
  message: string;
  data: HistoryDraftData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class HistoryDraftData implements Deserializable {
  [x: string]: any;
  id: number;
  type: string;
  type_id: number;
  category: string;
  notes: string;
  user_id: number;
  datetime: string;
  history_id: number;
  dataset_id: number;
  name: string;
  nama_skpd_alias: string;
  cdare: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
