import { Deserializable } from '@models-v3';

export class HistoryActivity implements Deserializable {
  [x: string]: any;
  message: string;
  data: HistoryActivityData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class HistoryActivityData implements Deserializable {
  [x: string]: any;
  id: number;
  category: string;
  type: string;
  type_id: number;
  user_id: number;
  username: string;
  name: string;
  origin_opd: string;
  owner_opd: string;
  datetime: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
