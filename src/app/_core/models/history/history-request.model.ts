import { Deserializable } from '@models-v3';

export class HistoryRequest implements Deserializable {
  [x: string]: any;
  message: string;
  data: HistoryRequestData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class HistoryRequestData implements Deserializable {
  [x: string]: any;
  id: number;
  type: string;
  dataset_id: number;
  category: number;
  notes: string;
  user_id: string;
  datetime: string;
  request_id: number;
  history_id: number;
  username: string;
  title: string;
  kode_skpd: string;
  nama_skpd: string;
  nama_skpd_alias: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
