import { Deserializable } from '@models-v3';

export class OrganizationStructure implements Deserializable {
  [x: string]: any;
  message: string;
  data: OrganizationStructureData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class OrganizationStructureData implements Deserializable {
  [x: string]: any;
  id: string;
  eselon_id: string;
  eselon_nm: string;
  jabatan_id: string;
  jabatan_nama: string;
  level: string;
  gol_id_minimum: string;
  parent_id: string;
  kode_skpd: string;
  satuan_kerja_id: string;
  satuan_kerja_nama: string;
  lv1_unit_kerja_id: string;
  lv1_unit_kerja_nama: string;
  lv2_unit_kerja_id: string;
  lv2_unit_kerja_nama: string;
  lv3_unit_kerja_id: string;
  lv3_unit_kerja_nama: string;
  lv4_unit_kerja_id: string;
  lv4_unit_kerja_nama: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
