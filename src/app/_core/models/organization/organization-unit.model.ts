import {
  Deserializable,
  RegionalData,
  OrganizationData,
  OrganizationSubData,
} from '@models-v3';

export class OrganizationUnit implements Deserializable {
  [x: string]: any;
  message: string;
  data: OrganizationUnitData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class OrganizationUnitData implements Deserializable {
  [x: string]: any;
  id: number;
  regional_id: number;
  skpd_id: number;
  skpdsub_id: number;
  kode_skpd: string;
  kode_skpdsub: string;
  kode_skpdunit: string;
  nama_skpd: string;
  nama_skpdsub: string;
  nama_skpdunit: string;
  notes: string;
  cdate: string;
  cuid: number;
  regional: RegionalData;
  skpd: OrganizationData;
  skpdsub: OrganizationSubData;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
