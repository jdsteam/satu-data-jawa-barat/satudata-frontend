import { Deserializable, RegionalData, OrganizationData } from '@models-v3';

export class OrganizationSub implements Deserializable {
  [x: string]: any;
  message: string;
  data: OrganizationSubData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class OrganizationSubData implements Deserializable {
  [x: string]: any;
  id: number;
  regional_id: number;
  skpd_id: number;
  kode_skpd: string;
  kode_skpdsub: string;
  nama_skpd: string;
  nama_skpdsub: string;
  notes: string;
  cdate: string;
  cuid: number;
  regional: RegionalData;
  skpd: OrganizationData;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
