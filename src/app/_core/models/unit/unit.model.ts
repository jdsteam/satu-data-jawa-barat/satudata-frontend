import { Deserializable } from '@models-v3';

export class Unit implements Deserializable {
  [x: string]: any;
  message: string;
  data: UnitData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class UnitData implements Deserializable {
  [x: string]: any;
  id: number;
  name: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
