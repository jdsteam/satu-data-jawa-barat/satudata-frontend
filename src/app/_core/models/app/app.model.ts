import { Deserializable, OrganizationData } from '@models-v3';

export class Application implements Deserializable {
  [x: string]: any;
  message: string;
  data: ApplicationData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class ApplicationData implements Deserializable {
  [x: string]: any;
  id: number;
  url: string;
  name: string;
  notes: string;
  code: string;
  kode_skpd: string;
  is_active: boolean;
  is_deleted: boolean;
  cdate: string;
  cuid: number;
  mdate: string;
  muid: number;
  skpd: OrganizationData;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
