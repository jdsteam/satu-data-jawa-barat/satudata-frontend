import { Deserializable, ClassificationDatasetData } from '@models-v3';

export class ApplicationService implements Deserializable {
  [x: string]: any;
  message: string;
  data: ApplicationServiceData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class ApplicationServiceData implements Deserializable {
  [x: string]: any;
  id: number;
  app_id: number;
  controller: string;
  notes: string;
  action: string;
  dataset_class_id: number;
  is_backend: boolean;
  is_active: boolean;
  is_deleted: boolean;
  cdate: string;
  cuid: number;
  mdate: string;
  muid: number;
  dataset_class: ClassificationDatasetData;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
