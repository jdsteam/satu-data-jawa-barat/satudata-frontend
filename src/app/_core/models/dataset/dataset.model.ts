import {
  Deserializable,
  ApplicationData,
  ApplicationServiceData,
  HistoryDraftData,
  DatasetTagData,
  DataTypeData,
  TopicData,
  LicenseData,
  OrganizationData,
  OrganizationSubData,
  OrganizationUnitData,
  RegionalData,
  ClassificationDatasetData,
  CanAccessInfo,
} from '@models-v3';

export class Dataset implements Deserializable {
  [x: string]: any;
  message: string;
  data: DatasetData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class DatasetData implements Deserializable {
  [x: string]: any;
  id: number;
  dataset_type_id: number;
  dataset_class_id: number;
  regional_id: number;
  license_id: number;
  sektoral_id: number;
  kode_skpd: string;
  kode_skpdsub: string;
  kode_skpdunit: string;
  app_id: number;
  app_service_id: number;
  name: string;
  title: string;
  year: string;
  image: string;
  description: string;
  owner: string;
  owner_email: string;
  maintainer: string;
  maintainer_email: string;
  notes: string;
  is_active: boolean;
  is_deleted: boolean;
  is_permanent: boolean;
  is_validate: number;
  cdate: string;
  cuid: number;
  mdate: string;
  muid: number;
  count_column: number;
  count_row: number;
  count_view: number;
  count_access: number;
  count_rating: string;
  app: ApplicationData;
  app_service: ApplicationServiceData;
  history_draft: HistoryDraftData;
  dataset_tag: DatasetTagData;
  dataset_tags: DatasetTagData;
  dataset_type: DataTypeData;
  sektoral: TopicData;
  license: LicenseData;
  skpd: OrganizationData;
  skpdsub: OrganizationSubData;
  skpdunit: OrganizationUnitData;
  regional: RegionalData;
  dataset_class: ClassificationDatasetData;
  can_access_info: CanAccessInfo;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
