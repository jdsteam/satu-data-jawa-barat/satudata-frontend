import { Deserializable } from '@models-v3';

export class DatasetTag implements Deserializable {
  [x: string]: any;
  message: string;
  data: DatasetTagData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class DatasetTagData implements Deserializable {
  [x: string]: any;
  id: number;
  dataset_id: number;
  tag: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
