import { Deserializable } from '@models-v3';

export class DatasetFile implements Deserializable {
  [x: string]: any;
  message: string;
  metadata: any[];
  data: any[];
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
