import { Deserializable } from '@models-v3';

export class DataType implements Deserializable {
  [x: string]: any;
  message: string;
  data: DataTypeData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class DataTypeData implements Deserializable {
  [x: string]: any;
  id: number;
  name: string;
  notes: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
