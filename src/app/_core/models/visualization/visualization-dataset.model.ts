import { Deserializable, DatasetData, DatasetTagData } from '@models-v3';

export class VisualizationDataset implements Deserializable {
  [x: string]: any;
  message: string;
  data: VisualizationDatasetData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class VisualizationDatasetData implements Deserializable {
  [x: string]: any;
  id: number;
  dataset_id: number;
  dataset: DatasetData;
  dataset_tag: DatasetTagData;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
