/* eslint-disable @typescript-eslint/no-use-before-define */
import { isArray } from 'lodash';
import {
  Deserializable,
  RegionalData,
  TopicData,
  ClassificationDatasetData,
  LicenseData,
  OrganizationData,
  OrganizationSubData,
  OrganizationUnitData,
  VisualizationDatasetData,
} from '@models-v3';

export class Visualization implements Deserializable {
  [x: string]: any;
  message: string;
  data: VisualizationData;
  error: number;

  // deserialize(input: any): this {
  //   return Object.assign(this, input);
  // }

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new VisualizationData().deserialize(res),
      );
    } else {
      this.data = new VisualizationData().deserialize(input.data);
    }

    return this;
  }
}

export class VisualizationData implements Deserializable {
  [x: string]: any;
  id: number;
  name: string;
  title: string;
  year: string;
  description: string;
  image: string;
  url: string;
  kode_skpd: string;
  kode_skpdsub: string;
  kode_skpdunit: string;
  owner: string;
  owner_email: string;
  maintainer: string;
  maintainer_email: string;
  can_access: boolean;
  is_active: boolean;
  is_deleted: boolean;
  is_validate: boolean;
  count_view: number;
  count_rating: number;
  cuid: number;
  cdate: string;
  muid: number;
  mdate: string;
  regional: RegionalData;
  sektoral: TopicData;
  dataset_class: ClassificationDatasetData;
  license: LicenseData;
  skpd: OrganizationData;
  skpdsub: OrganizationSubData;
  skpdunit: OrganizationUnitData;
  visualization_dataset: VisualizationDatasetData;
  keywords: any[];

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
