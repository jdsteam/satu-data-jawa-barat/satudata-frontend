/* eslint-disable @typescript-eslint/no-use-before-define */
import { Deserializable } from '@models-v3';

// PLUGIN
import { isArray } from 'lodash';

export class MapsetType implements Deserializable {
  [x: string]: any;
  message: string;
  data: MapsetTypeData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new MapsetTypeData().deserialize(res),
      );
    } else {
      this.data = new MapsetTypeData().deserialize(input.data);
    }

    return this;
  }
}

export class MapsetTypeData implements Deserializable {
  [x: string]: any;
  id: number;
  name: string;
  notes: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
