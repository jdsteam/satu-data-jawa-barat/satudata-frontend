/* eslint-disable @typescript-eslint/no-use-before-define */
import { Deserializable } from '@models-v3';

// PLUGIN
import { isArray } from 'lodash';

export class MapsetSource implements Deserializable {
  [x: string]: any;
  message: string;
  data: MapsetSourceData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new MapsetSourceData().deserialize(res),
      );
    } else {
      this.data = new MapsetSourceData().deserialize(input.data);
    }

    return this;
  }
}

export class MapsetSourceData implements Deserializable {
  [x: string]: any;
  id: number;
  name: string;
  notes: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
