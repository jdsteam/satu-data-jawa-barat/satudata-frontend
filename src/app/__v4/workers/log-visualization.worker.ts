import { DoWork, runWorker } from 'observable-webworker';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import API from 'fetch-worker';

export class LogVisualizationWorker implements DoWork<object, string> {
  // API URL
  apiUrl = `${environment.fluentdURL}satudata/`;

  public work(data$: Observable<any>): Observable<any> {
    return data$.pipe(
      map((res) => {
        API.fetch(`${this.apiUrl}visualization`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            // Authorization: `Bearer ${res.token}`,
          },
          body: JSON.stringify(res.log),
        });

        return res.log.visualization_id;
      }),
    );
  }
}

runWorker(LogVisualizationWorker);
