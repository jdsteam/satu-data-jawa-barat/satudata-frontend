// PACKAGE
import { css } from '@emotion/css';
import { gray50, gray200, gray300 } from './variables.style';

const getTable = () => {
  return css`
    --jds-table-bg: transparent;
    --jds-table-accent-bg: transparent;
    --jds-table-striped-color: #212529;
    --jds-table-striped-bg: #fafafa;
    --jds-table-active-color: #212529;
    --jds-table-active-bg: rgba(0, 0, 0, 0.1);
    --jds-table-hover-color: #212529;
    --jds-table-hover-bg: rgba(0, 0, 0, 0.075);
    width: 100%;
    margin-bottom: 16px;
    color: #212529;
    vertical-align: top;
    border-color: #dee2e6;

    > :not(caption) > * > * {
      padding: 10px 16px;
      background-color: var(--jds-table-bg);
      border-bottom-width: 1px;
      box-shadow: inset 0 0 0 9999px var(--jds-table-accent-bg);
    }

    > tbody {
      vertical-align: inherit;
    }

    > thead {
      vertical-align: bottom;
    }

    > :not(:last-child) > :last-child > * {
      border-bottom-color: currentColor;
    }
  `;
};

const getTableSm = () => {
  return css`
    > :not(caption) > * > * {
      padding: 6px 16px;
    }
  `;
};

const getTableBordered = () => {
  return css`
    > :not(caption) > * {
      border-width: 1px 0;

      > * {
        border-width: 0 1px;
      }
    }
  `;
};

const getTableBorderless = () => {
  return css`
    > :not(caption) > * > * {
      border-bottom-width: 0;
      padding: 6px 0;
    }
  `;
};

const getTableStriped = () => {
  return css`
    > tbody > tr:nth-of-type(odd) {
      --jds-table-accent-bg: var(--jds-table-striped-bg);
      color: var(--jds-table-striped-color);
    }
  `;
};

const getTableResponsive = () => {
  return css`
    overflow-x: auto;
  `;
};

const getTableSortable = () => {
  return css`
    position: relative;
    cursor: pointer;
    padding-right: 38px !important;

    &:after {
      font-family: 'Font Awesome 5 Free';
      position: absolute;
      top: 50%;
      right: 16px;
      font-size: 12px;
      margin-top: -5px;
      display: inline-block;
      line-height: 1;
      -webkit-font-smoothing: antialiased;

      content: '\\f0dc';
      opacity: 0.5;
    }

    &.disabled:after {
      content: '';
      opacity: 0;
    }

    &.asc:after {
      content: '\\f0de';
      opacity: 1;
      margin-top: -5px;
    }

    &.desc:after {
      content: '\\f0dd';
      opacity: 1;
      margin-top: -8px;
    }
  `;
};

const getTablePaging = () => {
  const table = getTable().toString();

  return css`
    ${`.${table}`} {
      margin-bottom: 0;
    }

    .paging {
      font-size: 14px;
      background-color: ${gray50};
      border: 1px solid ${gray300};
      border-top: 0;

      .paging-info {
        .paging-per-page {
          display: flex;
          align-items: center;
        }

        .paging-records {
          display: flex;
          align-items: center;
        }
      }

      .paging-nav {
        display: flex;
        align-items: center;

        .paging-page {
          display: flex;
          align-items: center;
        }

        .paging-button-end {
          display: flex;
        }
      }

      @media (max-width: 767.98px) {
        .paging-info {
          display: none;
        }
      }

      @media (max-width: 991.98px) {
        .paging-nav {
          justify-content: space-between;

          .paging-button-start {
            display: flex;

            .paging-prev {
              border-right: 1px solid ${gray200};
            }
          }

          .paging-button-end {
            .paging-prev {
              display: none;
            }

            .paging-next {
              border-left: 1px solid ${gray200};
            }
          }
        }
      }

      @media (min-width: 768px) {
        .paging-info {
          display: flex;
          justify-content: center;
        }
      }

      @media (min-width: 992px) {
        .paging-info {
          display: flex;
          justify-content: flex-start;

          .paging-per-page {
            border-right: 1px solid ${gray200};
          }

          .paging-records {
            border-right: 1px solid ${gray200};
          }
        }

        .paging-nav {
          justify-content: flex-end;

          .paging-button-start {
            display: none;
          }

          .paging-page {
            border-right: 1px solid ${gray200};
          }

          .paging-button-end {
            .paging-prev {
              display: block;
              border-right: 1px solid ${gray200};
            }
          }
        }
      }
    }
  `;
};

export const table = getTable();
export const tableSm = getTableSm();
export const tableBordered = getTableBordered();
export const tableBorderless = getTableBorderless();
export const tableStriped = getTableStriped();
export const tableResponsive = getTableResponsive();
export const tableSortable = getTableSortable();
export const tablePaging = getTablePaging();
