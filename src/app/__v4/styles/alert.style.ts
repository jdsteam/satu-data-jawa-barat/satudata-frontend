// PACKAGE
import { css } from '@emotion/css';
import * as Color from 'color';
import { themeColors, primary } from './variables.style';
import { getHexToRGB } from './functions.style';
import { buttonClose } from './close.style';

const getAlert = () => {
  return css`
    position: relative;
    padding: 16px 16px;
    margin-bottom: 16px;
    border: 1px solid transparent;
    border-radius: 8px;
  `;
};

const getAlertHeading = () => {
  return css`
    color: inherit;
  `;
};

const getAlertIcon = () => {
  return css`
    top: 2px;
    position: relative;
  `;
};

const getAlertLink = () => {
  return css`
    font-weight: 700;
  `;
};

const getAlertDismissible = () => {
  const button = buttonClose.toString();

  return css`
    padding-right: 48px;

    ${`.${button}`} {
      position: absolute;
      top: 0;
      right: 0;
      z-index: 2;
      padding: 20px 16px;
    }
  `;
};

const getAlertVariant = (value: string = null) => {
  const link = getAlertLink().toString();

  const arr = Object.entries(themeColors).map(([key, val]) => ({ key, val }));
  const obj = arr.find((x) => x.key === value);

  let res: string;
  if (obj !== undefined) {
    res = obj.val;
  } else {
    res = value;
  }

  const original = Color.rgb(getHexToRGB(res));

  let alertBackground: string;
  let alertBorder: string;

  if (value === 'primary') {
    alertBackground = '#e3f2fd';
    alertBorder = primary;
  } else {
    alertBackground = original.lighten(0.6).hex();
    alertBorder = original.lighten(0.5).hex();
  }

  const alertColor = original.darken(0.4).hex();
  const alertLinkColor = original.darken(0.2).hex();

  return css`
    color: ${alertColor};
    background-color: ${alertBackground};
    border-color: ${alertBorder};

    ${`.${link}`} {
      color: ${alertLinkColor};
    }
  `;
};

export const alert = getAlert();
export const alertHeading = getAlertHeading();
export const alertIcon = getAlertIcon();
export const alertLink = getAlertLink();
export const alertDismissible = getAlertDismissible();

export const alertPrimary = getAlertVariant('primary');
export const alertSecondary = getAlertVariant('secondary');
export const alertSuccess = getAlertVariant('success');
export const alertInfo = getAlertVariant('info');
export const alertWarning = getAlertVariant('warning');
export const alertDanger = getAlertVariant('danger');
export const alertLight = getAlertVariant('light');
export const alertDark = getAlertVariant('dark');
export const alertWhite = getAlertVariant('white');
export const alertBlack = getAlertVariant('black');
export const alertCustom = (value: string) => getAlertVariant(value);
