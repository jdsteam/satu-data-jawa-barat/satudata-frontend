// PACKAGE
import { css } from '@emotion/css';

const getAccordion = () => {
  return css`
    display: flex;
    flex-direction: column;
    width: 100%;
    height: auto;
  `;
};

const getAccordionButtonComponent = () => {
  const contentDown = '\\e945';
  const contentRight = '\\e947';

  return css`
    padding: 15px;

    a {
      text-transform: uppercase;
      position: relative;
      display: block;
      text-decoration: none;
      outline: none;
      font-weight: bold;
      font-size: 14px;

      &:hover,
      &:focus {
        color: #555555;
      }

      &:before {
        content: '${contentDown}';
        position: absolute;
        right: 0px;
        top: 0;
        font-size: 16px;
        color: #cbcbcb;
        font-family: 'icomoon' !important;
      }

      &.collapsed:before {
        content: '${contentRight}';
        position: absolute;
        right: 0px;
        top: 0;
        font-size: 16px;
        color: #cbcbcb;
        font-family: 'icomoon' !important;
      }
    }
  `;
};

const getAccordionButton = () => {
  return css`
    position: relative;
    display: flex;
    align-items: center;
    width: 100%;
    padding: 20px 20px;
    font-size: 14px;
    color: #333333;
    text-align: left;
    background-color: #ffffff;
    border: 0;
    border-radius: 0;
    overflow-anchor: none;

    &:hover {
      z-index: 2;
    }
  `;
};

const getAccordionHeader = () => {
  return css`
    margin-bottom: 0;
  `;
};

const getAccordionItem = () => {
  const button = getAccordionButton().toString();

  return css`
    background-color: #ffffff;
    border: 1px solid #e7e8e9;

    &:first-of-type {
      border-top-left-radius: 8px;
      border-top-right-radius: 8px;

      ${`.${button}`} {
        border-top-left-radius: 8px;
        border-top-right-radius: 8px;
      }
    }

    &:not(:first-of-type) {
      border-top: 0;
    }

    &:last-of-type {
      border-bottom-right-radius: 8px;
      border-bottom-left-radius: 8px;

      ${`.${button}`} {
        &.collapsed {
          border-bottom-right-radius: 8px;
          border-bottom-left-radius: 8px;
        }
      }

      .accordion-collapse {
        border-bottom-right-radius: 8px;
        border-bottom-left-radius: 8px;
      }
    }
  `;
};

const getAccordionItemComponent = () => {
  return css`
    background-color: #ffffff;
    border: 1px solid #e7e8e9;
  `;
};

const getAccordionBody = () => {
  return css`
    padding: 20px 20px;
    transition: all 0.2s ease-in-out;
  `;
};

const getAccordionBodyComponent = () => {
  return css`
    padding: 0 20px;

    ul {
      li {
        list-style: none;
        display: inline-block;
        width: 100%;
        padding-bottom: 5px;
      }
    }
  `;
};

export const accordion = getAccordion();
export const accordionButton = getAccordionButton();
export const accordionButtonComponent = getAccordionButtonComponent();
export const accordionHeader = getAccordionHeader();
export const accordionItem = getAccordionItem();
export const accordionItemComponent = getAccordionItemComponent();
export const accordionBody = getAccordionBody();
export const accordionBodyComponent = getAccordionBodyComponent();
