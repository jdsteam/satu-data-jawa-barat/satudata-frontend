// PACKAGE
import { css } from '@emotion/css';
import { jdsBiColor } from '@jds-bi/cdk';

const getOffcanvasBackdrop = () => {
  return css`
    position: fixed;
    top: 0;
    left: 0;
    z-index: 1040;
    width: 100vw;
    height: 100vh;
    background-color: #000;

    &.show {
      opacity: 0.5;
    }

    &.fade {
      opacity: 0;
    }
  `;
};

const getOffcanvas = () => {
  return css`
    position: fixed;
    bottom: 0;
    z-index: 1045;
    display: flex;
    flex-direction: column;
    max-width: 100%;
    visibility: hidden;
    background-color: #ffffff;
    background-clip: padding-box;
    outline: 0;
    transition: transform 0.3s ease-in-out;

    &.show {
      visibility: visible;

      &:not(.hiding) {
        transform: none;
      }
    }
  `;
};

const getOffcanvasBottom = () => {
  return css`
    right: 0;
    left: 0;
    height: 30vh;
    max-height: 100%;
    border-top: 1px solid ${jdsBiColor.gray['100']};
    transform: translateY(100%);
  `;
};

export const offcanvasBackdrop = getOffcanvasBackdrop();
export const offcanvas = getOffcanvas();
export const offcanvasBottom = getOffcanvasBottom();
