// PACKAGE
import { css } from '@emotion/css';
import * as Color from 'color';
import {
  themeColors,
  colorContrastLight,
  white,
  primary,
  gray400,
  gray600,
  gray800,
} from './variables.style';
import { getHexToRGB } from './functions.style';

const getButton = () => {
  return css`
    display: inline-block;
    font-weight: 400;
    line-height: 16px;
    color: #212529;
    text-align: center;
    text-decoration: none !important;
    vertical-align: middle;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    user-select: none;
    background-color: transparent;
    border: 1px solid transparent;
    padding: 10px 16px;
    font-size: 14px;
    border-radius: 8px;
    transition:
      color 0.15s ease-in-out,
      background-color 0.15s ease-in-out,
      border-color 0.15s ease-in-out,
      box-shadow 0.15s ease-in-out;

    &:hover {
      color: #212529;
    }

    &:focus {
      outline: 0;
    }

    &:disabled,
    &.disabled,
    fieldset:disabled & {
      pointer-events: none;
      opacity: 0.65;
    }
  `;
};

const getButtonVariant = (value: string = null) => {
  const arr = Object.entries(themeColors).map(([key, val]) => ({ key, val }));
  const obj = arr.find((x) => x.key === value);

  let res: string;
  if (obj !== undefined) {
    res = obj.val;
  } else {
    res = value;
  }

  const original = Color.rgb(getHexToRGB(res));
  const light = colorContrastLight;

  const hex = original.hex();

  const background = hex;
  const border = hex;
  const color = original.isDark() ? '#ffffff' : '#000000';

  const hoverBackground =
    color === light
      ? original.darken(0.15).hex()
      : original.lighten(0.15).hex();
  const originalHoverBackground = Color.rgb(getHexToRGB(hoverBackground));
  const hoverBorder =
    color === light ? original.darken(0.2).hex() : original.lighten(0.1).hex();
  const hoverColor = originalHoverBackground.isDark() ? '#ffffff' : '#000000';

  // const activeBackground = color === light ? original.darken(0.2).hex() : original.lighten(0.2).hex();
  // const originalActiveBackground = Color.rgb(getHexToRGB(activeBackground));
  // const activeBorder = color === light ? original.darken(0.25).hex() : original.lighten(0.1).hex();
  // const activeColor = originalActiveBackground.isDark() ? '#ffffff' : '#000000';

  const disabledBackground = background;
  const originalDisabledBackground = Color.rgb(getHexToRGB(disabledBackground));
  const disabledBorder = border;
  const disabledColor = originalDisabledBackground.isDark()
    ? '#ffffff'
    : '#000000';

  return css`
    color: ${color};
    background-color: ${background};
    border-color: ${border};

    &:hover {
      color: ${hoverColor};
      background-color: ${hoverBackground};
      border-color: ${hoverBorder};
    }

    &:focus {
      color: ${hoverColor};
    }

    &:disabled,
    &.disabled {
      color: ${disabledColor};
      background-color: ${disabledBackground};
      border-color: ${disabledBorder};
    }
  `;
};

const getButtonLink = () => {
  return css`
    font-weight: normal;
    color: ${primary};
    text-decoration: none;

    &:hover {
      color: ${primary};
      text-decoration: none;
    }

    &:focus {
      text-decoration: none;
    }

    &:disabled,
    &.disabled {
      color: #333333;
    }
  `;
};

const getButtonDefault = () => {
  return css`
    color: #333333;
    background-color: #ffffff;
    border-color: #ccc;

    &:hover {
      color: #333333;
      background-color: #e0e0e0;
      border-color: #ccc;
    }

    &:disabled,
    &.disabled {
      color: #333333;
      background-color: #e0e0e0;
      border-color: #ccc;
    }
  `;
};

const getButtonTag = () => {
  return css`
    color: #555555;
    background-color: #f9f9f9;
    border-color: #dedfe0;
    border-radius: 20px;

    &:hover {
      color: #ffffff;
      background-color: ${primary};
      border-color: ${primary};
    }

    &:disabled,
    &.disabled {
      color: #555555;
      background-color: #f9f9f9;
      border-color: #dedfe0;
    }

    &.active {
      color: #fff;
      background-color: ${primary};
      border-color: ${primary};
    }
  `;
};

const getButtonStatus = () => {
  return css`
    color: ${gray600};
    background-color: ${white};
    border-color: ${gray400};

    &:hover {
      color: ${primary};
      background-color: #d6eafc;
      border-color: #d6eafc;
    }

    &:disabled,
    &.disabled {
      color: #333333;
      background-color: #e0e0e0;
      border-color: #ccc;
    }

    &.active {
      color: ${primary};
      background-color: #d6eafc;
      border-color: #d6eafc;
    }
  `;
};

const getButtonRequest = (value = 'default') => {
  let color: string;
  let bgColor: string;
  let borderColor: string;
  let hover: string;
  switch (value) {
    case 'default':
      color = gray800;
      bgColor = 'white';
      borderColor = gray400;
      hover = gray400;
      break;
    case 'process':
      color = '#ff9500';
      bgColor = '#fff9e1';
      borderColor = '#fff9e1';
      hover = '#ffeeb4';
      break;
    case 'finish':
      color = '#069550';
      bgColor = '#e6f6ec';
      borderColor = '#e6f6ec';
      hover = '#c3e9d0';
      break;
    case 'decline':
      color = '#d32f2f';
      bgColor = '#ffebee';
      borderColor = '#ffebee';
      hover = '#ffcdd2';
      break;
    default:
      break;
  }

  return css`
    padding: 5px 9px;
    color: ${color};
    background-color: ${bgColor};
    border-color: ${borderColor};

    &:hover {
      color: ${color};
      background-color: ${hover};
      border-color: ${hover};
    }

    &:disabled,
    &.disabled {
      color: #333333;
      background-color: #e0e0e0;
      border-color: #ccc;
    }

    &.active {
      color: ${color};
      background-color: ${hover};
      border-color: ${hover};
    }

    svg {
      top: 0.125em;
      position: relative;
    }
  `;
};

const getButtonOutlineVariant = (value: string = null) => {
  const arr = Object.entries(themeColors).map(([key, val]) => ({ key, val }));
  const obj = arr.find((x) => x.key === value);

  let res: string;
  if (obj !== undefined) {
    res = obj.val;
  } else {
    res = value;
  }

  const original = Color.rgb(getHexToRGB(res));

  const hex = original.hex();

  const color = hex;
  const colorHover = original.isDark() ? '#ffffff' : '#000000';

  const activeBackground = color;
  // const originalActiveBackground = Color.rgb(getHexToRGB(activeBackground));
  const activeBorder = color;
  // const activeColor = originalActiveBackground.isDark() ? '#ffffff' : '#000000';

  return css`
    color: ${color};
    border-color: ${color};

    &:hover {
      color: ${colorHover};
      background-color: ${activeBackground};
      border-color: ${activeBorder};
    }

    &:disabled,
    &.disabled {
      color: ${color};
      background-color: transparent;
    }
  `;
};

const getButtonSize = (value: string = null) => {
  let paddingY: string;
  let paddingX: string;
  let fontSize: string;
  switch (value) {
    case 'lg':
      paddingY = '12px';
      paddingX = '18px';
      fontSize = '16px';
      break;
    case 'sm':
      paddingY = '8px';
      paddingX = '14px';
      fontSize = '12px';
      break;
    default:
      break;
  }

  return css`
    padding: ${paddingY} ${paddingX};
    font-size: ${fontSize};
  `;
};

export const button = getButton();

export const buttonPrimary = getButtonVariant('primary');
export const buttonSecondary = getButtonVariant('secondary');
export const buttonSuccess = getButtonVariant('success');
export const buttonInfo = getButtonVariant('info');
export const buttonWarning = getButtonVariant('warning');
export const buttonDanger = getButtonVariant('danger');
export const buttonLight = getButtonVariant('light');
export const buttonDark = getButtonVariant('dark');
export const buttonWhite = getButtonVariant('white');
export const buttonBlack = getButtonVariant('black');
export const buttonLink = getButtonLink();
export const buttonDefault = getButtonDefault();
export const buttonTag = getButtonTag();
export const buttonStatus = getButtonStatus();
export const buttonRequest = (value: string) => getButtonRequest(value);
export const buttonCustom = (value: string) => getButtonVariant(value);

export const buttonOutlinePrimary = getButtonOutlineVariant('primary');
export const buttonOutlineSecondary = getButtonOutlineVariant('secondary');
export const buttonOutlineSuccess = getButtonOutlineVariant('success');
export const buttonOutlineInfo = getButtonOutlineVariant('info');
export const buttonOutlineWarning = getButtonOutlineVariant('warning');
export const buttonOutlineDanger = getButtonOutlineVariant('danger');
export const buttonOutlineLight = getButtonOutlineVariant('light');
export const buttonOutlineDark = getButtonOutlineVariant('dark');
export const buttonOutlineWhite = getButtonOutlineVariant('white');
export const buttonOutlineBlack = getButtonOutlineVariant('black');
export const buttonOutlineCustom = (value: string) =>
  getButtonOutlineVariant(value);

export const buttonLg = getButtonSize('lg');
export const buttonSm = getButtonSize('sm');
