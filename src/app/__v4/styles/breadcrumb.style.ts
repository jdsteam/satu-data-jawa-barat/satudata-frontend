// PACKAGE
import { css } from '@emotion/css';

import { jdsBiColor } from '@jds-bi/cdk';

const getBreadcrumb = () => {
  return css`
    display: inline-flex;
    flex-wrap: wrap;
    padding: 0 0;
    margin-bottom: 0;
    font-size: 16px;
    list-style: none;
    background-color: null;
    border-radius: null;
  `;
};

const getBreadcrumbItem = () => {
  return css`
    display: inline-flex;
    align-items: center;
    color: ${jdsBiColor.gray['300']};
    margin-right: 4px;

    a {
      color: ${jdsBiColor.blue['600']};
      font-weight: 700;
      text-decoration: underline;

      &:hover {
        color: ${jdsBiColor.blue['800']};
      }
    }

    span,
    svg {
      margin-left: 8px;
      margin-right: 8px;
    }

    span {
      color: ${jdsBiColor.gray['300']};
    }

    &.active {
      color: ${jdsBiColor.gray['800']};
      font-weight: 700;
    }
  `;
};

export const breadcrumb = getBreadcrumb();
export const breadcrumbItem = getBreadcrumbItem();
