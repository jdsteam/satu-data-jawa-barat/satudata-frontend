// PACKAGE
import { css } from '@emotion/css';
import { jdsBiColor } from '@jds-bi/cdk';

const getTimeline = () => {
  return css`
    list-style-type: none;
    position: relative;
    padding-left: 0;

    > li {
      position: relative;
      border-left: 4px solid ${jdsBiColor.blue['600']};
      padding-left: 34px;
      padding-bottom: 20px;
      margin-left: 10px;
      list-style: none;

      &:not(:last-child) {
        padding-left: 30px;
      }

      &:first-child {
        &:before {
          background: ${jdsBiColor.blue['600']};
        }
      }

      &:last-child {
        border: 0px;
        padding-bottom: 10px;

        &:before {
          left: -8px;
        }
      }

      &:before {
        content: ' ';
        background: white;
        display: inline-block;
        position: absolute;
        border-radius: 50%;
        border: 3px solid ${jdsBiColor.blue['600']};
        left: -12px;
        width: 20px;
        height: 20px;
        z-index: 400;
      }
    }
  `;
};

export const timeline = getTimeline();
