// PACKAGE
import { css } from '@emotion/css';

const getButtonClose = () => {
  return css`
    box-sizing: content-box;
    width: 16px;
    height: 16px;
    padding: 4px 4px;
    color: #000000;
    background: transparent
      url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16' fill='%23000'%3e%3cpath d='M.293.293a1 1 0 011.414 0L8 6.586 14.293.293a1 1 0 111.414 1.414L9.414 8l6.293 6.293a1 1 0 01-1.414 1.414L8 9.414l-6.293 6.293a1 1 0 01-1.414-1.414L6.586 8 .293 1.707a1 1 0 010-1.414z'/%3e%3c/svg%3e")
      center / 16px auto no-repeat;
    border: 0;
    border-radius: 8px;
    opacity: 0.5;

    &:hover {
      color: #000000;
      text-decoration: none;
      opacity: 0.75;
    }

    &:disabled,
    &.disabled {
      pointer-events: none;
      user-select: none;
      opacity: 0.25;
    }
  `;
};

const getButtonCloseWhite = () => {
  return css`
    filter: #ffffff;
  `;
};

export const buttonClose = getButtonClose();
export const buttonCloseWhite = getButtonCloseWhite();
