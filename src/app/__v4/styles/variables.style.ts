/* eslint-disable no-underscore-dangle */
/* eslint-disable @typescript-eslint/naming-convention */
const _white = '#ffffff';
const _gray_50 = '#fafafa';
const _gray_100 = '#F5F5F5';
const _gray_200 = '#eee';
const _gray_300 = '#e0e0e0';
const _gray_400 = '#BDBDBD';
const _gray_500 = '#9e9e9e';
const _gray_600 = '#6c757d';
const _gray_700 = '#616161';
const _gray_800 = '#424242';
const _gray_900 = '#212529';
const _black = '#000000';
export const white = _white;
export const gray50 = _gray_50;
export const gray100 = _gray_100;
export const gray200 = _gray_200;
export const gray300 = _gray_300;
export const gray400 = _gray_400;
export const gray500 = _gray_500;
export const gray600 = _gray_600;
export const gray700 = _gray_700;
export const gray800 = _gray_800;
export const gray900 = _gray_900;
export const black = _black;

// Red
const _red_50 = '#ffebee';
const _red_100 = '#ffcdd2';
const _red_200 = '#ef9a9a';
const _red_300 = '#e57373';
const _red_400 = '#ef5350';
const _red_500 = '#f44336';
const _red_600 = '#e53935';
const _red_700 = '#d32f2f';
const _red_800 = '#c62828';
const _red_900 = '#b71b1c';
export const red50 = _red_50;
export const red100 = _red_100;
export const red200 = _red_200;
export const red300 = _red_300;
export const red400 = _red_400;
export const red500 = _red_500;
export const red600 = _red_600;
export const red700 = _red_700;
export const red800 = _red_800;
export const red900 = _red_900;

const _blue = '#1E88E5';
const _purple = '#AB47BC';
const _pink = '#FF4D77';
const _red = '#F44336';
const _orange = '#fd7e14';
const _yellow = '#FFD026';
const _green = '#16A75C';
const _cyan = '#0dcaf0';
const _cobalt = '#0753a6';
const _shuttle_grey = '#5C6265';
const _solitude = '#e7e8e9';
export const blue = _blue;
export const purple = _purple;
export const pink = _pink;
export const red = _red;
export const orange = _orange;
export const yellow = _yellow;
export const green = _green;
export const cyan = _cyan;
export const cobalt = _cobalt;
export const shuttleGrey = _shuttle_grey;
export const solitude = _solitude;

const _primary = _cobalt;
const _secondary = _shuttle_grey;
const _success = _green;
const _info = _cyan;
const _warning = _yellow;
const _danger = _red;
const _light = _gray_600;
const _dark = _gray_900;
export const primary = _primary;
export const secondary = _secondary;
export const success = _success;
export const info = _info;
export const warning = _warning;
export const danger = _danger;
export const light = _light;
export const dark = _dark;

const _theme_colors = {
  primary,
  secondary,
  success,
  info,
  warning,
  danger,
  light,
  dark,
  white,
  black,
  muted: '#777777',
  transparent: 'transparent',
  gray50,
  gray100,
  gray200,
  gray300,
  gray400,
  gray500,
  gray600,
  gray700,
  gray800,
  gray900,
  red50,
  red100,
  red200,
  red300,
  red400,
  red500,
  red600,
  red700,
  red800,
  red900,
};
export const themeColors = _theme_colors;

const _min_contrast_ratio = 2.5;
export const minContrastRatio = _min_contrast_ratio;

const _color_contrast_dark = _black;
export const colorContrastDark = _color_contrast_dark;

const _color_contrast_light = _white;
export const colorContrastLight = _color_contrast_light;
