// PACKAGE
import { css } from '@emotion/css';

const getCard = () => {
  return css`
    position: relative;
    display: flex;
    flex-direction: column;
    min-width: 0;
    height: $card-height;
    word-wrap: break-word;
    background-color: #ffffff;
    background-clip: border-box;
    border: 1px solid #e7e8e9;
    border-radius: 8px;

    > hr {
      margin-right: 0;
      margin-left: 0;
    }

    > .list-group {
      border-top: inherit;
      border-bottom: inherit;

      &:first-child {
        border-top-width: 0;
        border-top-left-radius: 8px;
        border-top-right-radius: 8px;
      }

      &:last-child {
        border-bottom-width: 0;
        border-bottom-right-radius: 8px;
        border-bottom-left-radius: 8px;
      }
    }

    > .card-header + .list-group,
    > .list-group + .card-footer {
      border-top: 0;
    }
  `;
};

const getCardHeader = () => {
  return css`
    padding: 24px 24px 18px 24px;
    margin-bottom: 0;
    background-color: #ffffff;
    border-bottom: 1px solid #e7e8e9;

    &:first-child {
      border-radius: 8px 8px 0 0;
    }
  `;
};

const getCardTitle = () => {
  return css`
    font-size: 16px;
    font-weight: bold;
    line-height: 19px;
    color: #4c4c4c;
    margin-bottom: 0;
  `;
};

const getCardBody = () => {
  return css`
    flex: 1 1 auto;
    padding: 20px 20px;
  `;
};

const getCardFooter = () => {
  return css`
    padding: 20px 20px;
    background-color: #ffffff;
    border-top: 1px solid #e7e8e9;

    &:last-child {
      border-radius: 0 0 8px 8px;
    }
  `;
};

export const card = getCard();
export const cardHeader = getCardHeader();
export const cardTitle = getCardTitle();
export const cardBody = getCardBody();
export const cardFooter = getCardFooter();
