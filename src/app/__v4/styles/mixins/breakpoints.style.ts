// PACKAGE
import { css } from '@emotion/css';

const gridBreakpoints = {
  xs: 0,
  sm: 576,
  md: 768,
  lg: 992,
  xl: 1200,
  xxl: 1400,
};

const getBreakpointMin = (name: string) => {
  const min = gridBreakpoints[name];
  return min !== 0 ? min : null;
};

const getBreakpointMax = (name: string) => {
  const max = gridBreakpoints[name];
  return max && max > 0 ? max - 0.02 : null;
};

const getBreakpointNext = (name: string) => {
  const arr = Object.entries(gridBreakpoints).map(([key, value]) => ({
    key,
    value,
  }));
  const index = arr.findIndex((x) => x.key === name);
  return index < arr.length ? arr[index + 1].key : null;
};

export const getMediaBreakpointUp = (name: string, value: any) => {
  const min = getBreakpointMin(name);

  return css`
    @media (min-width: ${min}px) {
      ${value}
    }
  `;
};

export const getMediaBreakpointDown = (name: string, value: any) => {
  const max = getBreakpointMax(name);

  return css`
    @media (max-width: ${max}px) {
      ${value}
    }
  `;
};

export const getMediaBreakpointOnly = (name: string, value: any) => {
  const min = getBreakpointMin(name);
  const next = getBreakpointNext(name);
  const max = getBreakpointMax(next);

  let result: string;
  if (min !== null && max !== null) {
    result = css`
      @media (min-width: ${min}px) and (max-width: ${max}px) {
        ${value}
      }
    `;
  } else if (max === null) {
    result = getMediaBreakpointUp(name, value);
  } else if (min === null) {
    result = getMediaBreakpointDown(name, value);
  } else {
    result = null;
  }

  return result;
};

export const getMediaBreakpointBetween = (
  lower: string,
  upper: string,
  value: any,
) => {
  const min = getBreakpointMin(lower);
  const max = getBreakpointMax(upper);

  let result: string;
  if (min !== null && max !== null) {
    result = css`
      @media (min-width: ${min}px) and (max-width: ${max}px) {
        ${value}
      }
    `;
  } else if (max === null) {
    result = getMediaBreakpointUp(lower, value);
  } else if (min === null) {
    result = getMediaBreakpointDown(upper, value);
  } else {
    result = null;
  }

  return result;
};
