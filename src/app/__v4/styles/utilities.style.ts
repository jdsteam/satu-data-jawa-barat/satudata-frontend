// TYPE
import {
  AlignItems,
  BorderColor,
  Display,
  JustifyContent,
  List,
  ObjectFit,
  Position,
  TextAlign,
  VerticalAlign,
} from '@types-v4';

import { css } from '@emotion/css';

import { themeColors, solitude } from './variables.style';
import { getMediaBreakpointUp } from './mixins/breakpoints.style';

// Display
export const d = (value: Display) => {
  return css`
    display: ${value} !important;
  `;
};
export const dSm = (value: Display) => getMediaBreakpointUp('sm', d(value));
export const dMd = (value: Display) => getMediaBreakpointUp('md', d(value));
export const dLg = (value: Display) => getMediaBreakpointUp('lg', d(value));
export const dXl = (value: Display) => getMediaBreakpointUp('xl', d(value));
export const dXxl = (value: Display) => getMediaBreakpointUp('xxl', d(value));

// Border
export const border = (value = 0) => {
  return value === 0
    ? css`
        border: 0 !important;
      `
    : css`
        border: ${value}px solid ${solitude} !important;
      `;
};

export const borderTop = (value = 0) => {
  return value === 0
    ? css`
        border-top: 0 !important;
      `
    : css`
        border-top: ${value}px solid ${solitude} !important;
      `;
};

export const borderEnd = (value = 0) => {
  return value === 0
    ? css`
        border-right: 0 !important;
      `
    : css`
        border-right: ${value}px solid ${solitude} !important;
      `;
};

export const borderBottom = (value = 0) => {
  return value === 0
    ? css`
        border-bottom: 0 !important;
      `
    : css`
        border-bottom: ${value}px solid ${solitude} !important;
      `;
};

export const borderStart = (value = 0) => {
  return value === 0
    ? css`
        border-left: 0 !important;
      `
    : css`
        border-left: ${value}px solid ${solitude} !important;
      `;
};

// Border Color
export const borderColor = (value: BorderColor | string) => {
  let res: string;
  switch (value) {
    case 'current':
      res = 'currentColor';
      break;
    default:
      res = value;
      break;
  }

  return css`
    border-color: ${res} !important;
  `;
};

// Sizing
const sizing = (value: number | string) => {
  let res: string;
  switch (value) {
    case 'none':
      res = `none`;
      break;
    case 'auto':
      res = `auto`;
      break;
    case 'full':
      res = '100%';
      break;
    case 'screen':
      res = '100vw';
      break;
    case 'min':
      res = 'min-content';
      break;
    case 'max':
      res = 'max-content';
      break;
    case 'fit':
      res = 'fit-content';
      break;
    case '1/2':
      res = '50%';
      break;
    case '1/3':
      res = '33.333333%';
      break;
    case '2/3':
      res = '66.666667%';
      break;
    case '1/4':
      res = '25%';
      break;
    case '2/4':
      res = '50%';
      break;
    case '3/4':
      res = '75%';
      break;
    case '1/5':
      res = '20%';
      break;
    case '2/5':
      res = '40%';
      break;
    case '3/5':
      res = '60%';
      break;
    case '4/5':
      res = '80%';
      break;
    default:
      res = `${value}px`;
      break;
  }

  return res;
};

// Width
export const w = (value: number | string) => {
  const res = sizing(value);

  return css`
    width: ${res} !important;
  `;
};

// Min Width
export const minW = (value: number | string) => {
  const res = sizing(value);

  return css`
    min-width: ${res} !important;
  `;
};

// Max Width
export const maxW = (value: number | string) => {
  const res = sizing(value);

  return css`
    max-width: ${res} !important;
  `;
};

// Height
export const h = (value: number | string) => {
  const res = sizing(value);

  return css`
    height: ${res} !important;
  `;
};

// Min Height
export const minH = (value: number | string) => {
  const res = sizing(value);

  return css`
    min-height: ${res} !important;
  `;
};

// Min Height
export const maxH = (value: number | string) => {
  const res = sizing(value);

  return css`
    max-height: ${res} !important;
  `;
};

// Justify Content
export const justifyContent = (value: JustifyContent) => {
  let res: string;
  switch (value) {
    case 'start':
      res = 'flex-start';
      break;
    case 'end':
      res = 'flex-end';
      break;
    case 'center':
      res = 'center';
      break;
    case 'between':
      res = 'space-between';
      break;
    case 'around':
      res = 'space-around';
      break;
    case 'evenly':
      res = 'space-evenly';
      break;
    default:
      res = value;
      break;
  }

  return css`
    justify-content: ${res} !important;
  `;
};
export const justifyContentSm = (value: JustifyContent) =>
  getMediaBreakpointUp('sm', justifyContent(value));
export const justifyContentMd = (value: JustifyContent) =>
  getMediaBreakpointUp('md', justifyContent(value));
export const justifyContentLg = (value: JustifyContent) =>
  getMediaBreakpointUp('lg', justifyContent(value));
export const justifyContentXl = (value: JustifyContent) =>
  getMediaBreakpointUp('xl', justifyContent(value));
export const justifyContentXxl = (value: JustifyContent) =>
  getMediaBreakpointUp('xxl', justifyContent(value));

// Align Items
export const alignItems = (value: AlignItems) => {
  let res: string;
  switch (value) {
    case 'start':
      res = 'flex-start';
      break;
    case 'end':
      res = 'flex-end';
      break;
    case 'center':
      res = 'center';
      break;
    case 'baseline':
      res = 'baseline';
      break;
    case 'stretch':
      res = 'stretch';
      break;
    default:
      break;
  }

  return css`
    align-items: ${res} !important;
  `;
};
export const alignItemsSm = (value: AlignItems) =>
  getMediaBreakpointUp('sm', alignItems(value));
export const alignItemsMd = (value: AlignItems) =>
  getMediaBreakpointUp('md', alignItems(value));
export const alignItemsLg = (value: AlignItems) =>
  getMediaBreakpointUp('lg', alignItems(value));
export const alignItemsXl = (value: AlignItems) =>
  getMediaBreakpointUp('xl', alignItems(value));
export const alignItemsXxl = (value: AlignItems) =>
  getMediaBreakpointUp('xxl', alignItems(value));

// Margin
export const m = (value: number | string) => {
  return value === 'auto'
    ? css`
        margin: auto !important;
      `
    : css`
        margin: ${value}px !important;
      `;
};
export const mSm = (value: number | string) =>
  getMediaBreakpointUp('sm', m(value));
export const mMd = (value: number | string) =>
  getMediaBreakpointUp('md', m(value));
export const mLg = (value: number | string) =>
  getMediaBreakpointUp('lg', m(value));
export const mXl = (value: number | string) =>
  getMediaBreakpointUp('xl', m(value));
export const mXxl = (value: number | string) =>
  getMediaBreakpointUp('xxl', m(value));

export const mx = (value: number | string) => {
  return value === 'auto'
    ? css`
        margin-right: auto !important;
        margin-left: auto !important;
      `
    : css`
        margin-right: ${value}px !important;
        margin-left: ${value}px !important;
      `;
};
export const mxSm = (value: number | string) =>
  getMediaBreakpointUp('sm', mx(value));
export const mxMd = (value: number | string) =>
  getMediaBreakpointUp('md', mx(value));
export const mxLg = (value: number | string) =>
  getMediaBreakpointUp('lg', mx(value));
export const mxXl = (value: number | string) =>
  getMediaBreakpointUp('xl', mx(value));
export const mxXxl = (value: number | string) =>
  getMediaBreakpointUp('xxl', mx(value));

export const my = (value: number | string) => {
  return value === 'auto'
    ? css`
        margin-top: auto !important;
        margin-bottom: auto !important;
      `
    : css`
        margin-top: ${value}px !important;
        margin-bottom: ${value}px !important;
      `;
};
export const mySm = (value: number | string) =>
  getMediaBreakpointUp('sm', my(value));
export const myMd = (value: number | string) =>
  getMediaBreakpointUp('md', my(value));
export const myLg = (value: number | string) =>
  getMediaBreakpointUp('lg', my(value));
export const myXl = (value: number | string) =>
  getMediaBreakpointUp('xl', my(value));
export const myXxl = (value: number | string) =>
  getMediaBreakpointUp('xxl', my(value));

export const mt = (value: number | string) => {
  return value === 'auto'
    ? css`
        margin-top: auto !important;
      `
    : css`
        margin-top: ${value}px !important;
      `;
};
export const mtSm = (value: number | string) =>
  getMediaBreakpointUp('sm', mt(value));
export const mtMd = (value: number | string) =>
  getMediaBreakpointUp('md', mt(value));
export const mtLg = (value: number | string) =>
  getMediaBreakpointUp('lg', mt(value));
export const mtXl = (value: number | string) =>
  getMediaBreakpointUp('xl', mt(value));
export const mtXxl = (value: number | string) =>
  getMediaBreakpointUp('xxl', mt(value));

export const me = (value: number | string) => {
  return value === 'auto'
    ? css`
        margin-right: auto !important;
      `
    : css`
        margin-right: ${value}px !important;
      `;
};
export const meSm = (value: number | string) =>
  getMediaBreakpointUp('sm', me(value));
export const meMd = (value: number | string) =>
  getMediaBreakpointUp('md', me(value));
export const meLg = (value: number | string) =>
  getMediaBreakpointUp('lg', me(value));
export const meXl = (value: number | string) =>
  getMediaBreakpointUp('xl', me(value));
export const meXxl = (value: number | string) =>
  getMediaBreakpointUp('xxl', me(value));

export const mb = (value: number | string) => {
  return value === 'auto'
    ? css`
        margin-bottom: auto !important;
      `
    : css`
        margin-bottom: ${value}px !important;
      `;
};
export const mbSm = (value: number | string) =>
  getMediaBreakpointUp('sm', mb(value));
export const mbMd = (value: number | string) =>
  getMediaBreakpointUp('md', mb(value));
export const mbLg = (value: number | string) =>
  getMediaBreakpointUp('lg', mb(value));
export const mbXl = (value: number | string) =>
  getMediaBreakpointUp('xl', mb(value));
export const mbXxl = (value: number | string) =>
  getMediaBreakpointUp('xxl', mb(value));

export const ms = (value: number | string) => {
  return value === 'auto'
    ? css`
        margin-left: auto !important;
      `
    : css`
        margin-left: ${value}px !important;
      `;
};
export const msSm = (value: number | string) =>
  getMediaBreakpointUp('sm', ms(value));
export const msMd = (value: number | string) =>
  getMediaBreakpointUp('md', ms(value));
export const msLg = (value: number | string) =>
  getMediaBreakpointUp('lg', ms(value));
export const msXl = (value: number | string) =>
  getMediaBreakpointUp('xl', ms(value));
export const msXxl = (value: number | string) =>
  getMediaBreakpointUp('xxl', ms(value));

// Padding
export const p = (value: number | string) => {
  return value === 'auto'
    ? css`
        padding: auto !important;
      `
    : css`
        padding: ${value}px !important;
      `;
};
export const pSm = (value: number | string) =>
  getMediaBreakpointUp('sm', p(value));
export const pMd = (value: number | string) =>
  getMediaBreakpointUp('md', p(value));
export const pLg = (value: number | string) =>
  getMediaBreakpointUp('lg', p(value));
export const pXl = (value: number | string) =>
  getMediaBreakpointUp('xl', p(value));
export const pXxl = (value: number | string) =>
  getMediaBreakpointUp('xxl', p(value));

export const px = (value: number | string) => {
  return value === 'auto'
    ? css`
        padding-right: auto !important;
        padding-left: auto !important;
      `
    : css`
        padding-right: ${value}px !important;
        padding-left: ${value}px !important;
      `;
};
export const pxSm = (value: number | string) =>
  getMediaBreakpointUp('sm', px(value));
export const pxMd = (value: number | string) =>
  getMediaBreakpointUp('md', px(value));
export const pxLg = (value: number | string) =>
  getMediaBreakpointUp('lg', px(value));
export const pxXl = (value: number | string) =>
  getMediaBreakpointUp('xl', px(value));
export const pxXxl = (value: number | string) =>
  getMediaBreakpointUp('xxl', px(value));

export const py = (value: number | string) => {
  return value === 'auto'
    ? css`
        padding-top: auto !important;
        padding-bottom: auto !important;
      `
    : css`
        padding-top: ${value}px !important;
        padding-bottom: ${value}px !important;
      `;
};
export const pySm = (value: number | string) =>
  getMediaBreakpointUp('sm', py(value));
export const pyMd = (value: number | string) =>
  getMediaBreakpointUp('md', py(value));
export const pyLg = (value: number | string) =>
  getMediaBreakpointUp('lg', py(value));
export const pyXl = (value: number | string) =>
  getMediaBreakpointUp('xl', py(value));
export const pyXxl = (value: number | string) =>
  getMediaBreakpointUp('xxl', py(value));

export const pt = (value: number | string) => {
  return value === 'auto'
    ? css`
        padding-top: auto !important;
      `
    : css`
        padding-top: ${value}px !important;
      `;
};
export const ptSm = (value: number | string) =>
  getMediaBreakpointUp('sm', pt(value));
export const ptMd = (value: number | string) =>
  getMediaBreakpointUp('md', pt(value));
export const ptLg = (value: number | string) =>
  getMediaBreakpointUp('lg', pt(value));
export const ptXl = (value: number | string) =>
  getMediaBreakpointUp('xl', pt(value));
export const ptXxl = (value: number | string) =>
  getMediaBreakpointUp('xxl', pt(value));

export const pe = (value: number | string) => {
  return value === 'auto'
    ? css`
        padding-right: auto !important;
      `
    : css`
        padding-right: ${value}px !important;
      `;
};
export const peSm = (value: number | string) =>
  getMediaBreakpointUp('sm', pe(value));
export const peMd = (value: number | string) =>
  getMediaBreakpointUp('md', pe(value));
export const peLg = (value: number | string) =>
  getMediaBreakpointUp('lg', pe(value));
export const peXl = (value: number | string) =>
  getMediaBreakpointUp('xl', pe(value));
export const peXxl = (value: number | string) =>
  getMediaBreakpointUp('xxl', pe(value));

export const pb = (value: number | string) => {
  return value === 'auto'
    ? css`
        padding-bottom: auto !important;
      `
    : css`
        padding-bottom: ${value}px !important;
      `;
};
export const pbSm = (value: number | string) =>
  getMediaBreakpointUp('sm', pb(value));
export const pbMd = (value: number | string) =>
  getMediaBreakpointUp('md', pb(value));
export const pbLg = (value: number | string) =>
  getMediaBreakpointUp('lg', pb(value));
export const pbXl = (value: number | string) =>
  getMediaBreakpointUp('xl', pb(value));
export const pbXxl = (value: number | string) =>
  getMediaBreakpointUp('xxl', pb(value));

export const ps = (value: number | string) => {
  return value === 'auto'
    ? css`
        padding-left: auto !important;
      `
    : css`
        padding-left: ${value}px !important;
      `;
};
export const psSm = (value: number | string) =>
  getMediaBreakpointUp('sm', ps(value));
export const psMd = (value: number | string) =>
  getMediaBreakpointUp('md', ps(value));
export const psLg = (value: number | string) =>
  getMediaBreakpointUp('lg', ps(value));
export const psXl = (value: number | string) =>
  getMediaBreakpointUp('xl', ps(value));
export const psXxl = (value: number | string) =>
  getMediaBreakpointUp('xxl', ps(value));

// Font Size
export const fs = (value: number | string) => {
  return css`
    font-size: ${value}px !important;
  `;
};

// Font Weight
function getFontWeight(value: number | string) {
  let res: number | string;
  switch (value) {
    case 'light':
      res = 'lighter';
      break;
    case 'lighter':
      res = '300';
      break;
    case 'normal':
      res = '400';
      break;
    case 'bold':
      res = '700';
      break;
    case 'bolder':
      res = 'bolder';
      break;
    default:
      res = value;
      break;
  }

  return css`
    font-weight: ${res} !important;
  `;
}

export const fontWeight = (value: number | string) => getFontWeight(value);
export const fw = (value: number | string) => getFontWeight(value);

// Text Align
export const textAlign = (value: TextAlign) => {
  let res: string;
  switch (value) {
    case 'start':
      res = 'left';
      break;
    case 'end':
      res = 'right';
      break;
    case 'center':
      res = 'center';
      break;
    default:
      res = value;
      break;
  }

  return css`
    text-align: ${res} !important;
  `;
};
export const textAlignSm = (value: TextAlign) =>
  getMediaBreakpointUp('sm', textAlign(value));
export const textAlignMd = (value: TextAlign) =>
  getMediaBreakpointUp('md', textAlign(value));
export const textAlignLg = (value: TextAlign) =>
  getMediaBreakpointUp('lg', textAlign(value));
export const textAlignXl = (value: TextAlign) =>
  getMediaBreakpointUp('xl', textAlign(value));
export const textAlignXxl = (value: TextAlign) =>
  getMediaBreakpointUp('xxl', textAlign(value));

// Text Decoration
export const textDecoration = (value: string) => {
  return css`
    text-decoration: ${value} !important;
  `;
};

// Color
export const text = (value: string) => {
  const arr = Object.entries(themeColors).map(([key, val]) => ({ key, val }));
  const color = arr.find((x) => x.key === value);

  let res: string;
  if (color !== undefined) {
    res = color.val;
  } else {
    res = value;
  }

  return css`
    color: ${res} !important;
  `;
};

// Background Color
export const bg = (value: string) => {
  const arr = Object.entries(themeColors).map(([key, val]) => ({ key, val }));
  const color = arr.find((x) => x.key === value);

  let res: string;
  if (color !== undefined) {
    res = color.val;
  } else {
    res = value;
  }

  return css`
    background-color: ${res} !important;
  `;
};

// Rounded
export const rounded = (value: number | string = 0) => {
  let res: string;
  switch (value) {
    case 0:
      res = '0';
      break;
    case 'circle':
      res = '50%';
      break;
    case 'pill':
      res = '50rem';
      break;
    default:
      res = `${value}px`;
      break;
  }

  return css`
    border-radius: ${res} !important;
  `;
};

export const roundedTop = (value = 0) => {
  return value === 0
    ? css`
        border-top-left-radius: 0 !important;
        border-top-right-radius: 0 !important;
      `
    : css`
        border-top-left-radius: ${value}px !important;
        border-top-right-radius: ${value}px !important;
      `;
};

export const roundedEnd = (value = 0) => {
  return value === 0
    ? css`
        border-top-right-radius: 0 !important;
        border-bottom-right-radius: 0 !important;
      `
    : css`
        border-top-right-radius: ${value}px !important;
        border-bottom-right-radius: ${value}px !important;
      `;
};

export const roundedBottom = (value = 0) => {
  return value === 0
    ? css`
        border-bottom-right-radius: 0 !important;
        border-bottom-left-radius: 0 !important;
      `
    : css`
        border-bottom-right-radius: ${value}px !important;
        border-bottom-left-radius: ${value}px !important;
      `;
};

export const roundedStart = (value = 0) => {
  return value === 0
    ? css`
        border-bottom-left-radius: 0 !important;
        border-top-left-radius: 0 !important;
      `
    : css`
        border-bottom-left-radius: ${value}px !important;
        border-top-left-radius: ${value}px !important;
      `;
};

// Gap
export const gap = (value: number) => {
  return css`
    gap: ${value}px !important;
  `;
};
export const gapSm = (value: number) => getMediaBreakpointUp('sm', gap(value));
export const gapMd = (value: number) => getMediaBreakpointUp('md', gap(value));
export const gapLg = (value: number) => getMediaBreakpointUp('lg', gap(value));
export const gapXl = (value: number) => getMediaBreakpointUp('xl', gap(value));
export const gapXxl = (value: number) =>
  getMediaBreakpointUp('xxl', gap(value));

export const gapCol = (value: number) => {
  return css`
    column-gap: ${value}px !important;
  `;
};
export const gapColSm = (value: number) =>
  getMediaBreakpointUp('sm', gapCol(value));
export const gapColMd = (value: number) =>
  getMediaBreakpointUp('md', gapCol(value));
export const gapColLg = (value: number) =>
  getMediaBreakpointUp('lg', gapCol(value));
export const gapColXl = (value: number) =>
  getMediaBreakpointUp('xl', gapCol(value));
export const gapColXxl = (value: number) =>
  getMediaBreakpointUp('xxl', gapCol(value));

export const gapRow = (value: number) => {
  return css`
    row-gap: ${value}px !important;
  `;
};
export const gapRowSm = (value: number) =>
  getMediaBreakpointUp('sm', gapRow(value));
export const gapRowMd = (value: number) =>
  getMediaBreakpointUp('md', gapRow(value));
export const gapRowLg = (value: number) =>
  getMediaBreakpointUp('lg', gapRow(value));
export const gapRowXl = (value: number) =>
  getMediaBreakpointUp('xl', gapRow(value));
export const gapRowXxl = (value: number) =>
  getMediaBreakpointUp('xxl', gapRow(value));

// Position
export const pos = (value: Position) => {
  return css`
    position: ${value} !important;
  `;
};

// List Style Type
export const list = (value: List) => {
  return css`
    list-style-type: ${value} !important;
  `;
};

// Object Fit
export const object = (value: ObjectFit) => {
  return css`
    object-fit: ${value} !important;
  `;
};

// Top / Right / Bottom / Left
export const inset = (value: number) => {
  return css`
    inset: ${value}px !important;
  `;
};
export const insetX = (value: number) => {
  return css`
    left: ${value}px !important;
    right: ${value}px !important;
  `;
};
export const insetY = (value: number) => {
  return css`
    top: ${value}px !important;
    bottom: ${value}px !important;
  `;
};
export const start = (value: number) => {
  return css`
    inset-inline-start: ${value}px !important;
  `;
};
export const end = (value: number) => {
  return css`
    inset-inline-end: ${value}px !important;
  `;
};
export const top = (value: number) => {
  return css`
    top: ${value}px !important;
  `;
};
export const right = (value: number) => {
  return css`
    right: ${value}px !important;
  `;
};
export const bottom = (value: number) => {
  return css`
    bottom: ${value}px !important;
  `;
};
export const left = (value: number) => {
  return css`
    left: ${value}px !important;
  `;
};

// Line Height
export const leading = (value: number) => {
  return css`
    line-height: ${value}px !important;
  `;
};

// Vertical Align
export const align = (value: VerticalAlign) => {
  return css`
    vertical-align: ${value} !important;
  `;
};
