// PACKAGE
import { css } from '@emotion/css';

const getFade = () => {
  return css`
    transition: opacity 0.15s linear;

    &:not(.show) {
      opacity: 0;
    }
  `;
};

const getCollapse = () => {
  return css`
    &:not(.show) {
      display: none;
    }
  `;
};

const getCollapsing = () => {
  return css`
    height: 0;
    overflow: hidden;
    transition: height 0.35s ease;
  `;
};

export const fade = getFade();
export const collapse = getCollapse();
export const collapsing = getCollapsing();
