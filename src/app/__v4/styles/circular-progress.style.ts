// PACKAGE
import { css } from '@emotion/css';
import { primary } from './variables.style';

const getCircularProgress = (value: number, size = 48, color = primary) => {
  return css`
    .pie {
      .left-side {
        transform: rotate(calc(${value} * 3.6deg));
      }

      ${value <= 50
        ? `
        clip: rect(
          0,
          ${size}px,
          ${size}px,
          calc(${size}px / 2)
        );

        .right-side {
          display: none;
        }
      `
        : `
        clip: rect(auto, auto, auto, auto);

        .right-side {
          transform: rotate(180deg);
        }
      `}
    }

    .pie-wrapper {
      width: ${size}px;
      height: ${size}px;
      float: left;
      position: relative;

      &:nth-child(3n + 1) {
        clear: both;
      }

      .pie {
        width: 100%;
        height: 100%;
        left: 0;
        position: absolute;
        top: 0;

        .half-circle {
          width: 100%;
          height: 100%;
          border: calc(${size}px / 7) solid ${color};
          border-radius: 50%;
          clip: rect(0, calc(${size}px / 2), ${size}px, 0);
          left: 0;
          position: absolute;
          top: 0;
        }
      }

      .shadow {
        width: 100%;
        height: 100%;
        border: calc(${size}px / 7) solid #e0e0e0;
        border-radius: 50%;
      }
    }
  `;
};

export const circularProgress = getCircularProgress;
