// Color Contrast
export const getColorContrast = (hexcolor: string) => {
  let hex = hexcolor;
  if (hex.slice(0, 1) === '#') {
    hex = hex.slice(1);
  }

  if (hex.length === 3) {
    hex = hex
      .split('')
      .map((res) => res + res)
      .join('');
  }

  const r = parseInt(hex.substr(0, 2), 16);
  const g = parseInt(hex.substr(2, 2), 16);
  const b = parseInt(hex.substr(4, 2), 16);

  const yiq = (r * 299 + g * 587 + b * 114) / 1000;

  return yiq >= 128 ? '#000000' : 'ffffff';
};

// RGB to Hex
export const getRGBToHex = (r, g, b) =>
  // eslint-disable-next-line prefer-template
  '#' +
  [r, g, b]
    .map((x) => {
      const hex = x.toString(16);
      return hex.length === 1 ? `0${hex}` : hex;
    })
    .join('');

// Hex to RGB
export const getHexToRGB = (hex) =>
  hex
    .replace(
      /^#?([a-f\d])([a-f\d])([a-f\d])$/i,
      (m, r, g, b) => `#${r}${r}${g}${g}${b}${b}`,
    )
    .substring(1)
    .match(/.{2}/g)
    .map((x) => parseInt(x, 16));
