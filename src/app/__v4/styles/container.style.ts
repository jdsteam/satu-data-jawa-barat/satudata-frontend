// PACKAGE
import { css, cx } from '@emotion/css';

const getContainer = (value: string = null) => {
  const main = css`
    width: 100%;
    padding-right: 20px;
    padding-left: 20px;
    margin-right: auto;
    margin-left: auto;
  `;

  const sm = css`
    @media (min-width: 576px) {
      max-width: 540px;
    }
  `;

  const md = css`
    @media (min-width: 768px) {
      max-width: 760px;
    }
  `;

  const lg = css`
    @media (min-width: 992px) {
      max-width: 980px;
    }
  `;

  const xl = css`
    @media (min-width: 1200px) {
      max-width: 1240px;
    }
  `;

  const xxl = css`
    @media (min-width: 1400px) {
      max-width: 1320px;
    }
  `;

  switch (value) {
    case 'sm':
      return cx(main, sm, md, lg, xl, xxl);
    case 'md':
      return cx(main, md, lg, xl, xxl);
    case 'lg':
      return cx(main, lg, xl, xxl);
    case 'xl':
      return cx(main, xl, xxl);
    case 'xxl':
      return cx(main, xxl);
    case 'fluid':
      return main;
    default:
      return cx(main, sm, md, lg, xl, xxl);
  }
};

export const container = getContainer();
export const containerFluid = getContainer('fluid');
export const containerXxl = getContainer('xxl');
export const containerXl = getContainer('xl');
export const containerLg = getContainer('lg');
export const containerMd = getContainer('md');
export const containerSm = getContainer('sm');
