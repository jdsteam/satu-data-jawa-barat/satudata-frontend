// PACKAGE
import { css } from '@emotion/css';

const getNav = () => {
  return css`
    display: flex;
    flex-wrap: wrap;
    padding-left: 0;
    margin-bottom: 0;
    list-style: none;
  `;
};

const getNavLink = () => {
  return css`
    display: block;
    padding: 8px 16px;
    font-size: 14px;
    font-weight: bold;
    color: #7e8284;
    text-decoration: none;

    &:hover,
    &:focus {
      color: #0753a6;
      text-decoration: none;
    }
  `;
};

const getNavPills = () => {
  const link = getNavLink().toString();

  return css`
    ${`.${link}`} {
      background: none;
      border: 0;
      border-radius: 8px;
    }

    ${`.${link}`}.active {
      color: #ffffff;
      background-color: #0753a6;
    }
  `;
};

const getNavFlush = () => {
  const link = getNavLink().toString();

  return css`
    ${`.${link}`} {
      background: none;
      border: 0;
    }

    ${`.${link}`}.active {
      color: #0753a6;
      border-bottom: 2px solid #0753a6;
    }
  `;
};

const getTabContent = () => {
  return css`
    > .tab-pane {
      display: none;
    }

    > .active {
      display: block;
    }
  `;
};

export const nav = getNav();
export const navLink = getNavLink();
export const navPills = getNavPills();
export const navFlush = getNavFlush();
export const tabContent = getTabContent();
