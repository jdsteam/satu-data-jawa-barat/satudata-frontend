// PACKAGE
import { css } from '@emotion/css';

const getNavbarLink = () => {
  return css`
    display: block;
    font-size: 14px;
    text-decoration: none;
    cursor: pointer;

    &:hover {
      text-decoration: none;
    }

    &:focus {
      outline: none;
      text-decoration: none;
    }

    &.active {
      color: #0753a6;
    }
  `;
};

const getDropdownMenu = () => {
  return css`
    padding: 10px 8px;
    margin-bottom: 0;
  `;
};

const getDropdownNavbarItem = () => {
  return css`
    list-style: none;
  `;
};

const getDropdownNavbarLink = () => {
  return css`
    display: block;
    width: 100%;
    padding: 8px 10px;
    font-size: 16px;
    text-decoration: none;
    text-align: inherit;
    border-radius: 6px;
    cursor: pointer;

    &:hover {
      background-color: #eee;
      text-decoration: none;
      color: unset;
    }

    &:focus {
      outline: none;
      text-decoration: none;
    }
  `;
};

export const navbarLink = getNavbarLink();
export const dropdownMenu = getDropdownMenu();
export const dropdownNavbarItem = getDropdownNavbarItem();
export const dropdownNavbarLink = getDropdownNavbarLink();
