// PACKAGE
import { css } from '@emotion/css';
import { primary, gray200 } from './variables.style';

const getProgress = () => {
  return css`
    display: flex;
    height: 16px;
    overflow: hidden;
    font-size: 12px;
    background-color: ${gray200};
    border-radius: 8px;
  `;
};

const getProgressBar = () => {
  return css`
    display: flex;
    flex-direction: column;
    justify-content: center;
    overflow: hidden;
    color: #fff;
    text-align: center;
    white-space: nowrap;
    background-color: ${primary};
    transition: width 0.6s ease;
  `;
};

const getProgressBarStriped = () => {
  return css`
    background-image: linear-gradient(
      45deg,
      rgba(255 255 255 / 30%) 25%,
      transparent 25%,
      transparent 50%,
      rgba(255 255 255 / 30%) 50%,
      rgba(255 255 255 / 30%) 75%,
      transparent 75%,
      transparent
    );
    background-size: 16px 16px;
  `;
};

const getProgressBarAnimated = () => {
  return css`
    animation: 1s linear infinite progress-bar-stripes;

    @media (prefers-reduced-motion: reduce) {
      animation: none;
    }

    @keyframes progress-bar-stripes {
      0% {
        background-position-x: 16px;
      }
    }
  `;
};

export const progress = getProgress();
export const progressBar = getProgressBar();
export const progressBarStriped = getProgressBarStriped();
export const progressBarAnimated = getProgressBarAnimated();
