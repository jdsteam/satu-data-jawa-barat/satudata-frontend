// PACKAGE
import { css } from '@emotion/css';

const getInputSearch = (value) => {
  return css`
    position: relative;

    i {
      position: absolute;
      color: #bbb;
      top: 50%;
      transform: translateY(-50%);

      ${value === 'sm' &&
      css`
        left: 10px;
      `}

      ${value === 'md' &&
      css`
        left: 16px;
      `}
    }

    input {
      ${value === 'sm' &&
      css`
        padding-left: 32px !important;
        height: 30px;
        font-size: 12px;
        border: 1px solid #dedfe0;
      `}

      ${value === 'md' &&
      css`
        padding-left: 42px !important;
      `}
    }
  `;
};

export const inputSearch = getInputSearch('md');
export const inputSearchSm = getInputSearch('sm');
export const inputSearchMd = getInputSearch('md');
