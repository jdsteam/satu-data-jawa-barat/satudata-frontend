// PACKAGE
import { css } from '@emotion/css';

const getformLabel = () => {
  return css`
    margin-bottom: 4px;
    font-size: 16px;
    font-style: normal;
    font-weight: 700;
  `;
};

export const formLabel = getformLabel();
