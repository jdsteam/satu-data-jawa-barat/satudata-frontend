// PACKAGE
import { css } from '@emotion/css';
import { jdsBiColor, jdsBiColorScheme } from '@jds-bi/cdk';

const getformCheckLabel = () => {
  return css`
    color: null;
    cursor: null;

    display: unset;
    max-width: unset;
    margin-bottom: unset;
    font-weight: unset;
    font-size: 14px;
  `;
};

const getformCheckInput = () => {
  const label = getformCheckLabel().toString();

  return css`
    width: 16px;
    height: 16px;
    margin-top: 4px;
    vertical-align: top;
    background-color: #fff;
    background-repeat: no-repeat;
    background-position: center;
    background-size: contain;
    border: 1px solid #9e9e9e;
    appearance: none;
    color-adjust: exact;

    &[type='checkbox'] {
      border-radius: 4px;
    }

    &[type='radio'] {
      border-radius: 50%;
    }

    &:checked {
      background-color: #0d6efd;
      border-color: #0d6efd;

      &[type='checkbox'] {
        background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 20 20'%3e%3cpath fill='none' stroke='%23fff' stroke-linecap='round' stroke-linejoin='round' stroke-width='3' d='M6 10l3 3l6-6'/%3e%3c/svg%3e");
      }

      &[type='radio'] {
        background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='-4 -4 8 8'%3e%3ccircle r='2' fill='%23fff'/%3e%3c/svg%3e");
      }
    }

    &:disabled {
      pointer-events: none;
      filter: none;
      opacity: 0.5;
    }

    &[disabled],
    &:disabled {
      ~ ${`.${label}`} {
        opacity: 0.5;
      }
    }
  `;
};

const getformCheck = () => {
  const input = getformCheckInput().toString();

  return css`
    display: block;
    min-height: 16px;
    padding-left: 26px;
    margin-bottom: 0;

    ${`.${input}`} {
      float: left;
      margin-left: -26px;
    }
  `;
};

const getformSwitch = () => {
  const { primary } = jdsBiColorScheme('blue');
  const input = getformCheckInput().toString();

  return css`
    padding-left: 40px;

    ${`.${input}`} {
      border: 2px solid ${jdsBiColor.gray['300']};
      width: 48px;
      height: 24px;
      margin-left: -40px;
      background-color: ${jdsBiColor.gray['300']};
      background-image: url("data:image/svg+xml,<svg width='20' height='20' viewBox='0 0 20 20' fill='white' xmlns='http://www.w3.org/2000/svg'><circle cx='10' cy='10' r='10'/></svg>");
      background-position: left center;
      border-radius: 48px;
      transition: background-position 0.15s ease-in-out;

      &:focus {
        background-image: url("data:image/svg+xml,<svg width='20' height='20' viewBox='0 0 20 20' fill='white' xmlns='http://www.w3.org/2000/svg'><circle cx='10' cy='10' r='10'/></svg>");
        border-color: ${primary};
        outline: 0;
        box-shadow: inset 0 0 0 1px ${jdsBiColor.yellow['500']};
      }

      &:checked {
        border-color: ${primary};
        background-color: ${primary};
        background-position: right center;
        background-image: url("data:image/svg+xml,<svg width='20' height='20' viewBox='0 0 20 20' fill='white' xmlns='http://www.w3.org/2000/svg'><circle cx='10' cy='10' r='10'/></svg>");
      }
    }
  `;
};

const getformCheckInline = () => {
  return css`
    display: inline-block;
    margin-right: 16px;
  `;
};

export const formCheck = getformCheck();
export const formSwitch = getformSwitch();
export const formCheckInput = getformCheckInput();
export const formCheckLabel = getformCheckLabel();
export const formCheckInline = getformCheckInline();
