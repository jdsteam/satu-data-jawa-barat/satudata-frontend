// PACKAGE
import { css } from '@emotion/css';

const getformText = () => {
  return css`
    margin-top: 4px;
    font-size: 14px;
    font-style: normal;
    font-weight: 400;
    color: #757575;
  `;
};

export const formText = getformText();
