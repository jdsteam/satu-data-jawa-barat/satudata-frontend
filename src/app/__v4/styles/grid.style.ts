// PACKAGE
import { css } from '@emotion/css';
import { getMediaBreakpointUp } from './mixins/breakpoints.style';

// Row
const getRow = () => {
  return css`
    --jds-gutter-x: 40px;
    --jds-gutter-y: 0;
    display: flex;
    flex-wrap: wrap;
    margin-top: calc(var(--jds-gutter-y) * -1);
    margin-right: calc(var(--jds-gutter-x) / -2);
    margin-left: calc(var(--jds-gutter-x) / -2);

    > * {
      box-sizing: border-box;
      flex-shrink: 0;
      width: 100%;
      max-width: 100%;
      padding-right: calc(var(--jds-gutter-x) / 2);
      padding-left: calc(var(--jds-gutter-x) / 2);
      margin-top: var(--jds-gutter-y);
    }
  `;
};

export const row = getRow();

// Row Cols
const getRowCols = (infix: string = null, i: number | string = null) => {
  let main: string;

  if (i === 'auto') {
    main = css`
      > * {
        flex: 0 0 auto;
        width: auto;
      }
    `;
  } else {
    const percentage = 100 / Number(i);
    main = css`
      > * {
        flex: 0 0 auto;
        width: ${percentage}%;
      }
    `;
  }

  return infix === null ? main : getMediaBreakpointUp(infix, main);
};

export const rowCols = (i: number | string = null) => getRowCols(null, i);
export const rowColsSm = (i: number | string = null) => getRowCols('sm', i);
export const rowColsMd = (i: number | string = null) => getRowCols('md', i);
export const rowColsLg = (i: number | string = null) => getRowCols('lg', i);
export const rowColsXl = (i: number | string = null) => getRowCols('xl', i);
export const rowColsXxl = (i: number | string = null) => getRowCols('xxl', i);

// Columns
const getCol = (infix: string = null, i: number | string = null) => {
  let main: string;

  main = css`
    flex: 1 0 0%;
  `;

  if (i !== null) {
    if (i === 'auto') {
      main = css`
        flex: 0 0 auto;
        width: auto;
      `;
    } else {
      const percentage = (Number(i) / 12) * 100;
      main = css`
        flex: 0 0 auto;
        width: ${percentage}%;
      `;
    }
  }

  return infix === null ? main : getMediaBreakpointUp(infix, main);
};

export const col = (i: number | string = null) => getCol(null, i);
export const colSm = (i: number | string = null) => getCol('sm', i);
export const colMd = (i: number | string = null) => getCol('md', i);
export const colLg = (i: number | string = null) => getCol('lg', i);
export const colXl = (i: number | string = null) => getCol('xl', i);
export const colXxl = (i: number | string = null) => getCol('xxl', i);

// Offset
const getOffset = (infix: string = null, i: number | string = null) => {
  const percentage = (Number(i) / 12) * 100;
  const off = css`
    margin-left: ${percentage}%;
  `;

  return infix === null ? off : getMediaBreakpointUp(infix, off);
};

export const offset = (i: number | string = null) => getOffset(null, i);
export const offsetSm = (i: number | string = null) => getOffset('sm', i);
export const offsetMd = (i: number | string = null) => getOffset('md', i);
export const offsetLg = (i: number | string = null) => getOffset('lg', i);
export const offsetXl = (i: number | string = null) => getOffset('xl', i);
export const offsetXxl = (i: number | string = null) => getOffset('xxl', i);
