// PACKAGE
import { css } from '@emotion/css';

const getBadge = () => {
  return css`
    display: inline-block;
    padding: 0.35em 0.65em;
    font-size: 0.75em;
    font-weight: 700;
    line-height: 1;
    color: #fff;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: 4px;

    &:empty {
      display: none;
    }
  `;
};

export const badge = getBadge();
