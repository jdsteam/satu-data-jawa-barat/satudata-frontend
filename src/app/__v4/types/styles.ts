export type AlignItems = 'start' | 'end' | 'center' | 'baseline' | 'stretch';
export type BorderColor =
  | 'inherit'
  | 'current'
  | 'transparent'
  | 'black'
  | 'white';
export type Display =
  | 'block'
  | 'inline-block'
  | 'inline'
  | 'flex'
  | 'inline-flex'
  | 'table'
  | 'inline-table'
  | 'table-caption'
  | 'table-cell'
  | 'table-column'
  | 'table-column-group'
  | 'table-footer-group'
  | 'table-header-group'
  | 'table-row-group'
  | 'table-row'
  | 'table-row'
  | 'flow-root'
  | 'grid'
  | 'inline-grid'
  | 'contents'
  | 'list-item'
  | 'none';
export type JustifyContent =
  | 'normal'
  | 'start'
  | 'end'
  | 'center'
  | 'between'
  | 'around'
  | 'evenly'
  | 'stretch';
export type List = 'none' | 'disc' | 'decimal';
export type ObjectFit = 'contain' | 'cover' | 'fill' | 'none' | 'scale-down';
export type Position = 'static' | 'fixed' | 'absolute' | 'relative' | 'sticky';
export type TextAlign =
  | 'left'
  | 'center'
  | 'right'
  | 'justify'
  | 'start'
  | 'end';
export type VerticalAlign =
  | 'baseline'
  | 'top'
  | 'middle'
  | 'bottom'
  | 'text-top'
  | 'text-bottom'
  | 'sub'
  | 'super';
