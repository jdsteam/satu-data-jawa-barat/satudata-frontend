/* eslint-disable @typescript-eslint/no-redeclare */
export const Catalog = [
  'dataset',
  'mapset',
  'indicator',
  'visualization',
  'organization',
] as const;

export type Catalog = (typeof Catalog)[number];
