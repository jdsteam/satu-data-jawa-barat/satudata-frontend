/* eslint-disable @typescript-eslint/no-redeclare */
export const StatusDataset = [
  'draft-active',
  'draft-archive',
  'draft-delete',
  'new-active',
  'new-archive',
  'new-delete',
  'revision-active',
  'revision-archive',
  'revision-delete',
  'approve-active',
  'approve-archive',
  'approve-delete',
  'edit-active',
  'edit-archive',
  'edit-delete',
  'delete-active',
  'delete-archive',
  'delete-delete',
  'discontinue-active',
  'discontinue-archive',
  'discontinue-delete',
] as const;

export type StatusDataset = (typeof StatusDataset)[number];
