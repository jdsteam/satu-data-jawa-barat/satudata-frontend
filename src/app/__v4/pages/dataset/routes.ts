// import { inject } from '@angular/core';
import { Routes } from '@angular/router';

// GUARDS
import { StoreDatasetDetailGuard, DatasetReviewGuard } from '@guards-v3';

// import { DatasetDetailService } from './containers/dataset-detail/dataset-detail.service';

// const canActivateDatasetDetail: CanActivateFn = (
//   route: ActivatedRouteSnapshot,
// ) => {
//   return inject(DatasetDetailService).canActivate(route.params.id);
// };

export const DATASET_ROUTES: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        loadComponent: () =>
          import('./containers/dataset-page/dataset-page.component').then(
            (m) => m.DatasetPageComponent,
          ),
      },
      // {
      //   path: 'detail/:id',
      //   canActivate: [canActivateDatasetDetail],
      //   loadComponent: () =>
      //     import('./containers/dataset-detail/dataset-detail.component').then(
      //       (m) => m.DatasetDetailComponent,
      //     ),
      // },
      {
        path: 'detail/:id',
        canActivate: [StoreDatasetDetailGuard],
        loadChildren: () =>
          import(
            '../../../dataset/containers/dataset-detail/dataset-detail.module'
          ).then((m) => m.DatasetDetailModule),
      },
      {
        path: 'review/:id',
        canActivate: [DatasetReviewGuard],
        loadChildren: () =>
          import(
            '../../../dataset/containers/dataset-review/dataset-review.module'
          ).then((m) => m.DatasetReviewModule),
      },
    ],
  },
];
