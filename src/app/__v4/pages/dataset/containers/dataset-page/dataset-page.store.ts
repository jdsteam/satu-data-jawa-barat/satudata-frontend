import { computed, inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs';
import {
  patchState,
  signalStore,
  signalStoreFeature,
  withComputed,
  withHooks,
  withMethods,
  withState,
} from '@ngrx/signals';
import { rxMethod } from '@ngrx/signals/rxjs-interop';
import { tapResponse } from '@ngrx/operators';

// CONSTANT
import { QUALITY_DATA, SORT_DATASET, STATUS_DATASET } from '@constants-v4';
// FEATURE
import {
  withClassification,
  withDatasetLicense,
  withDatasetQuality,
  withDatasetTags,
  withOrganizationSatuData,
  withTopicSatuData,
} from '@features-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';
import { DatasetService } from '@services-v4';

import { assign, isEmpty } from 'lodash';

type DatasetPageState = {
  satudataUser: LoginData;
  filterSearch: string | null;
  filterSort: string;
  filterOrganization: string[];
  filterTopic: string[];
  filterClassification: number[] | string[];
  filterQuality: string[];
  filterTags: string[];
  filterLicense: string[];
  filterPerPage: number;
  filterCurrentPage: number;
  listDataset: any;
};

export const initialState: DatasetPageState = {
  satudataUser: null,
  filterSearch: null,
  filterSort: 'newest',
  filterOrganization: [],
  filterTopic: [],
  filterClassification: [],
  filterQuality: [],
  filterTags: [],
  filterLicense: [],
  filterPerPage: 5,
  filterCurrentPage: 1,
  listDataset: null,
};

export function withDatasetPage() {
  return signalStoreFeature(
    withOrganizationSatuData(),
    withTopicSatuData(),
    withClassification(),
    withDatasetQuality(),
    withDatasetTags(),
    withDatasetLicense(),
  );
}

export const DatasetPageStore = signalStore(
  withState(initialState),
  withHooks((store) => {
    const authenticationService = inject(AuthenticationService);

    return {
      onInit() {
        authenticationService.satudataUser.subscribe((x) => {
          patchState(store, { satudataUser: x });
        });
      },
    };
  }),
  withComputed((store) => ({
    hasFilterQuickAccess: computed(() => {
      if (
        store.filterOrganization().length > 0 ||
        store.filterTopic().length > 0 ||
        store.filterClassification().length > 0 ||
        store.filterQuality().length > 0 ||
        store.filterTags().length > 0 ||
        store.filterLicense().length > 0
      ) {
        return true;
      }

      return false;
    }),
    hasFilterSearch: computed(() => {
      if (
        store.filterSearch() ||
        store.filterOrganization().length ||
        store.filterTopic().length ||
        store.filterClassification().length ||
        store.filterQuality().length ||
        store.filterTags().length ||
        store.filterLicense().length
      ) {
        return true;
      }

      return false;
    }),
  })),
  withDatasetPage(),
  withMethods(
    (
      store,
      router = inject(Router),
      route = inject(ActivatedRoute),
      datasetService = inject(DatasetService),
    ) => ({
      handleFilterSearch(search: string | null): void {
        const { filterSearch: previous } = store;
        const value = isEmpty(search) === false ? search : null;

        if (previous() !== value) {
          router.navigate([], {
            relativeTo: route,
            queryParams: {
              q: value,
              page: null,
            },
            queryParamsHandling: 'merge',
          });

          patchState(store, () => ({
            filterSearch: value,
            filterCurrentPage: initialState.filterCurrentPage,
          }));

          this.getAllDataset();
        }
      },
      handleFilterSort(sort: string): void {
        router.navigate([], {
          relativeTo: route,
          queryParams: {
            sort,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterSort: sort,
          filterCurrentPage: initialState.filterCurrentPage,
        }));

        this.getAllDataset();
      },
      handleFilterOrganization(organization: string[]): void {
        const value = !isEmpty(organization) ? organization.join(',') : null;

        router.navigate([], {
          relativeTo: route,
          queryParams: {
            organization: value,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterOrganization: organization,
          filterCurrentPage: initialState.filterCurrentPage,
        }));

        this.getAllDataset();
      },
      handleFilterTopic(topic: string[]): void {
        const value = !isEmpty(topic) ? topic.join(',') : null;

        router.navigate([], {
          relativeTo: route,
          queryParams: {
            topic: value,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterTopic: topic,
          filterCurrentPage: initialState.filterCurrentPage,
        }));

        this.getAllDataset();
      },
      handleFilterClassification(classification: number[] | string[]): void {
        const value = !isEmpty(classification)
          ? classification.join(',')
          : null;

        router.navigate([], {
          relativeTo: route,
          queryParams: {
            classification: value,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterClassification: classification,
          filterCurrentPage: initialState.filterCurrentPage,
        }));

        this.getAllDataset();
      },
      handleFilterQuality(quality: string[]): void {
        const value = !isEmpty(quality) ? quality.join(',') : null;

        router.navigate([], {
          relativeTo: route,
          queryParams: {
            quality: value,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterQuality: quality,
          filterCurrentPage: initialState.filterCurrentPage,
        }));

        this.getAllDataset();
      },
      handleFilterTags(tags: string[]): void {
        const value = !isEmpty(tags) ? tags.join(',') : null;

        router.navigate([], {
          relativeTo: route,
          queryParams: {
            tags: value,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterTags: tags,
          filterCurrentPage: initialState.filterCurrentPage,
        }));

        this.getAllDataset();
      },
      handleFilterLicense(license: string[]): void {
        const value = !isEmpty(license) ? license.join(',') : null;

        router.navigate([], {
          relativeTo: route,
          queryParams: {
            license: value,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterLicense: license,
          filterCurrentPage: initialState.filterCurrentPage,
        }));

        this.getAllDataset();
      },
      handleFilterPerPage(perPage: number): void {
        router.navigate([], {
          relativeTo: route,
          queryParams: {
            perPage,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterPerPage: perPage,
          filterCurrentPage: initialState.filterCurrentPage,
        }));

        this.getAllDataset();
      },
      handleFilterCurrentPage(currentPage: number): void {
        router.navigate([], {
          relativeTo: route,
          queryParams: {
            page: currentPage,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterCurrentPage: currentPage,
        }));

        this.getAllDataset();
      },
      handleFilterClear(): void {
        router.navigate([], {
          relativeTo: route,
          queryParams: {
            organization: [],
            topic: [],
            classification: [],
            quality: [],
            tags: [],
            license: [],
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterOrganization: initialState.filterOrganization,
          filterTopic: initialState.filterTopic,
          filterClassification: initialState.filterClassification,
          filterQuality: initialState.filterQuality,
          filterTags: initialState.filterTags,
          filterLicense: initialState.filterLicense,
          filterCurrentPage: initialState.filterCurrentPage,
        }));

        this.getAllDataset();
      },
      getAllDataset: rxMethod<void>(
        switchMap(() => {
          const {
            filterSearch: search,
            filterOrganization: organization,
            filterTopic: topic,
            filterClassification: classification,
            filterQuality: quality,
            filterTags: tags,
            filterLicense: license,
            filterSort: sort,
            filterPerPage: perPage,
            filterCurrentPage: currentPage,
          } = store;

          const pWhere = {};

          assign(pWhere, STATUS_DATASET.get('approve-active'));

          if (!isEmpty(organization())) {
            assign(pWhere, { kode_skpd: organization() });
          }

          if (!isEmpty(topic())) {
            assign(pWhere, { sektoral_id: topic() });
          }

          if (!isEmpty(classification())) {
            assign(pWhere, { dataset_class_id: classification() });
          }

          if (!isEmpty(quality())) {
            const pQuality = quality().map((item) => {
              const data = QUALITY_DATA.get(item);
              return data.name;
            });

            assign(pWhere, {
              category: 'Data Agregat',
              quality_score_label: pQuality,
            });
          }

          if (!isEmpty(tags())) {
            assign(pWhere, { tags: tags() });
          }

          if (!isEmpty(license())) {
            assign(pWhere, { license_id: license() });
          }

          const params = {
            search: search() || '',
            sort: SORT_DATASET.get(sort()).paramSort,
            skip: perPage() * currentPage() - perPage(),
            limit: perPage(),
            where: JSON.stringify(pWhere),
          };

          return datasetService.getQueryList(params).result$.pipe(
            tapResponse({
              next: (result) => {
                patchState(store, { listDataset: result });
              },
              error: console.error,
            }),
          );
        }),
      ),
    }),
  ),
);
