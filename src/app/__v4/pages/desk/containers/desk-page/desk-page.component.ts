import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
  inject,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Title } from '@angular/platform-browser';
import { RouterModule, Router, ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { STATUS_DATASET, STATUS_MAPSET } from '@constants-v4';
// INTERFACE
import { Styles, Breadcrumb } from '@interfaces-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService, StateService } from '@services-v3';
import { GlobalService, DatasetService, MapsetService } from '@services-v4';
// TYPE
import type { StatusDataset } from '@types-v4';

import { assign, isNil, set } from 'lodash';
import moment from 'moment';
import { fromWorker } from 'observable-webworker';
import Swal from 'sweetalert2';
import { useMutationResult } from '@ngneat/query';
import { SubscribeDirective } from '@ngneat/subscribe';
import { NgxLoadingModule } from 'ngx-loading';

import {
  DeskHeaderComponent,
  DeskTabTypeComponent,
  DeskStatusCatalogComponent,
  DeskTabStatusVerificationComponent,
  DeskListUploadChangeComponent,
  DeskListDiscontinueComponent,
  DeskApplicationRequestComponent,
  DeskStatusRequestSatuDataComponent,
  DeskStatusRequestOpenDataComponent,
  DeskListRequestSatuDataComponent,
  DeskListRequestOpenDataComponent,
} from '../../components';

import { DEFAULT_STATE, DeskPageStore } from './desk-page.store';

@Component({
  selector: 'app-desk-page',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    DeskHeaderComponent,
    DeskTabTypeComponent,
    DeskStatusCatalogComponent,
    DeskTabStatusVerificationComponent,
    DeskListUploadChangeComponent,
    DeskListDiscontinueComponent,
    DeskApplicationRequestComponent,
    DeskStatusRequestSatuDataComponent,
    DeskStatusRequestOpenDataComponent,
    DeskListRequestSatuDataComponent,
    DeskListRequestOpenDataComponent,
    SubscribeDirective,
    NgxLoadingModule,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './desk-page.component.html',
  providers: [DeskPageStore],
})
export class DeskPageComponent implements OnInit, OnDestroy {
  satudataUser: LoginData;
  satudataToken: string;
  jds: Styles = jds;
  moment = moment;

  // Log
  log = null;
  pageInfo = null;
  deviceInfo = null;
  userInfo = null;

  // Settings
  title: string;
  label: string;
  breadcrumb: Breadcrumb[] = [{ label: 'Desk', link: '/desk' }];

  // Service
  private router = inject(Router);
  private route = inject(ActivatedRoute);
  private globalService = inject(GlobalService);
  private authenticationService = inject(AuthenticationService);
  private stateService = inject(StateService);
  private titleService = inject(Title);
  private myDataPageStore = inject(DeskPageStore);

  private datasetService = inject(DatasetService);
  private mapsetService = inject(MapsetService);
  editDatasetMutation = useMutationResult();
  editMapsetMutation = useMutationResult();

  // Component Store
  readonly vm$ = this.myDataPageStore.vm$;

  constructor() {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
    this.authenticationService.satudataToken.subscribe(
      (x) => (this.satudataToken = x),
    );
    this.stateService.currentLogUser.subscribe((x) => (this.userInfo = x));
  }

  ngOnInit(): void {
    this.logPage('init');
    this.settingsAll();
    this.queryParams();
    this.getAllDeskData();
  }

  ngOnDestroy(): void {
    this.logPage('destroy');
  }

  settingsAll(): void {
    if (this.satudataUser.role_name === 'walidata') {
      this.label = 'Desk Walidata';
    } else if (this.satudataUser.role_name === 'opd') {
      this.label = 'Desk OPD';
    }

    this.title = this.globalService.title;
    this.titleService.setTitle(`${this.label} - ${this.title}`);
    this.globalService.changeLabel(this.label);
    this.globalService.changeBreadcrumb(this.breadcrumb);
  }

  queryParams(): void {
    this.route.queryParams.subscribe((p) => {
      const {
        filterSearch: search,
        filterType: type,
        filterStatus: status,
        filterApplication: application,
        filterStatusVerification: statusVerification,
        filterStatusRequest: statusRequest,
        filterSort: sort,
        filterPerPage: perPage,
        filterCurrentPage: currentPage,
      } = DEFAULT_STATE;

      const pSearch = !isNil(p['q']) ? p['q'] : search;
      const pType = !isNil(p['type']) ? p['type'] : type;
      const pStatus = !isNil(p['status']) ? p['status'] : status;
      const pApplication = !isNil(p['application'])
        ? p['application']
        : application;
      const pStatusVerification = !isNil(p['statusVerification'])
        ? p['statusVerification']
        : statusVerification;
      const pStatusRequest = !isNil(p['statusRequest'])
        ? p['statusRequest']
        : statusRequest;
      const pPerPage = !isNil(p['perPage']) ? p['perPage'] : perPage;
      const pCurrentPage = !isNil(p['page']) ? p['page'] : currentPage;

      let pSort = sort;
      switch (pType) {
        case 'upload-change':
          if (pStatus === 'verification') {
            if (pStatusVerification === 'edit') {
              pSort = 'mdate';
            }
          } else if (pStatus === 'revision' || pStatus === 'approve') {
            pSort = 'mdate';
          }
          break;
        case 'discontinue':
          if (pStatus === 'verification') {
            // TODO: change value discontinue_submit_date
            pSort = 'mdate';
          } else if (pStatus === 'approve') {
            // TODO: change value discontinue_approve_date
            pSort = 'mdate';
          }
          break;
        case 'request-dataset':
          pSort = 'newest';
          break;
        default:
          pSort = sort;
          break;
      }

      this.myDataPageStore.setFilterSearch(pSearch);
      this.myDataPageStore.setFilterType(pType);
      this.myDataPageStore.setFilterStatus(pStatus);
      this.myDataPageStore.setFilterApplication(pApplication);
      this.myDataPageStore.setFilterStatusVerification(pStatusVerification);
      this.myDataPageStore.setFilterStatusRequest(pStatusRequest);
      this.myDataPageStore.setFilterSort(pSort);
      this.myDataPageStore.setFilterPerPage(pPerPage);
      this.myDataPageStore.setFilterCurrentPage(pCurrentPage);
    });
  }

  // Get =======================================================================
  getAllDeskData() {
    this.myDataPageStore.getAllDeskData();
  }

  // Action ====================================================================
  onDelete(obj: any) {
    if (obj.catalog === 'dataset') {
      this.onDeleteDataset(obj);
    } else if (obj.catalog === 'mapset') {
      this.onDeleteMapset(obj);
    }
  }

  onDeleteDataset(obj: any) {
    Swal.fire({
      type: 'warning',
      text: 'Apakah Anda yakin untuk menghapus Dataset ini?',
      allowOutsideClick: false,
      showCancelButton: true,
      cancelButtonText: 'Batal',
      confirmButtonColor: '#d33',
      confirmButtonText: 'Hapus',
    }).then((result) => {
      if (result.value) {
        const data = {
          type: 'dataset',
          type_id: Number(obj.id),
          category: obj.category,
          notes: null,
        };
        this.onDatasetUpdate(data);
      }
    });
  }

  onDatasetUpdate(formData: {
    type: string;
    type_id: number;
    category: StatusDataset;
    notes?: null | string;
  }) {
    const { category } = formData;

    let text;
    let categoryHistory;
    switch (category) {
      case 'delete-active':
        text = 'dihapus';
        categoryHistory = 'delete-active';
        break;
      default:
        break;
    }

    const bodyDataset = {
      id: formData.type_id,
      history_draft: { ...formData, category: categoryHistory },
    };

    assign(bodyDataset, STATUS_DATASET.get(category));

    this.datasetService
      .updateItem(bodyDataset.id, bodyDataset)
      .pipe(this.editDatasetMutation.track())
      .subscribe(() => {
        Swal.fire({
          type: 'success',
          text: `Dataset berhasil ${text}`,
          allowOutsideClick: false,
        }).then(() => {
          this.myDataPageStore.getAllDeskData();
        });
      });
  }

  onDeleteMapset(obj: any) {
    Swal.fire({
      type: 'warning',
      text: 'Apakah Anda yakin untuk menghapus Mapset ini?',
      allowOutsideClick: false,
      showCancelButton: true,
      cancelButtonText: 'Batal',
      confirmButtonColor: '#d33',
      confirmButtonText: 'Hapus',
    }).then((result) => {
      if (result.value) {
        const data = {
          type: 'mapset',
          type_id: Number(obj.id),
          category: obj.category,
          notes: null,
        };
        this.onMapsetUpdate(data);
      }
    });
  }

  onMapsetUpdate(formData: any) {
    const { category } = formData;
    const bodyMapset = { id: formData.type_id, history_draft: formData };

    assign(bodyMapset, STATUS_MAPSET.get(category));

    this.mapsetService
      .updateItem(bodyMapset.id, bodyMapset)
      .pipe(this.editMapsetMutation.track())
      .subscribe(() => {
        let text;
        switch (category) {
          case 'revision-delete':
            text = 'dihapus';
            break;
          default:
            break;
        }

        Swal.fire({
          type: 'success',
          text: `Mapset berhasil ${text}`,
          allowOutsideClick: false,
        }).then(() => {
          this.myDataPageStore.getAllDeskData();
        });
      });
  }

  // Log =======================================================================
  logPage(type: string): void {
    if (type === 'init') {
      this.pageInfo = {
        page: this.router.url,
        date_start: moment().format('YYYY-MM-DD HH:mm:ss'),
      };

      this.log = {
        log: { ...this.pageInfo, ...this.userInfo },
        token: this.satudataToken,
      };
    } else if (type === 'destroy') {
      if (typeof Worker !== 'undefined') {
        set(this.log.log, 'date_end', moment().format('YYYY-MM-DD HH:mm:ss'));

        const log = of(this.log);

        fromWorker<object, string>(
          () =>
            new Worker(
              new URL('src/app/__v4/workers/log-page.worker', import.meta.url),
              { type: 'module' },
            ),
          log,
        ).subscribe((message) => {
          if (!environment.production) {
            console.info(`Log page: ${message}`);
          }
        });
      }
    }
  }
}
