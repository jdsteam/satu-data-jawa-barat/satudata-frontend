import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ComponentStore } from '@ngrx/component-store';
import { Observable, of, zip } from 'rxjs';
import { tap, withLatestFrom, switchMap, map } from 'rxjs/operators';
import {
  UntypedFormArray,
  UntypedFormControl,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';

// CONSTANT
import {
  STATUS_REQUEST_SATUDATA,
  STATUS_REQUEST_OPENDATA,
  STATUS_REQUEST_OPENDATA_REASON_DECLINE,
  SORT_REQUEST,
} from '@constants-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';
import {
  DeskCatalogService,
  HistoryCatalogStatusService,
  RequestDatasetSatuDataService,
  HistoryRequestDatasetSatuDataService,
  RequestDatasetOpenDataService,
  HistoryRequestDatasetOpenDataService,
  TopicService,
  OrganizationService,
} from '@services-v4';

import { assign, isEmpty } from 'lodash';

export type TType = 'upload-change' | 'discontinue' | 'request-dataset';
export type TStatus = 'verification' | 'revision' | 'approve';
export type TApplication = 'satudata' | 'opendata';
export type TStatusVerification = 'new' | 'edit';
export type TStatusRequest =
  | 'all'
  | 'submitted'
  | 'processed'
  | 'accommodated'
  | 'rejected';

export interface DeskPageState {
  filterSearch: string | null;
  filterType: TType;
  filterStatus: TStatus;
  filterApplication: TApplication;
  filterStatusVerification: TStatusVerification;
  filterStatusRequest: TStatusRequest;
  filterCatalog: string[];
  filterSort: string;
  filterDirection: string;
  filterPerPage: number;
  filterCurrentPage: number;
  listDeskCatalog: any;
  listDeskCatalogDiscontinue: any;
  historyDataset: any;
  historyMapset: any;
  historyIndicator: any;
  countRequestDatasetSatuData: any;
  countRequestDatasetSatuDataSubmitted: any;
  countRequestDatasetSatuDataProcessed: any;
  countRequestDatasetSatuDataAccommodated: any;
  countRequestDatasetSatuDataRejected: any;
  countRequestDatasetOpenData: any;
  countRequestDatasetOpenDataSubmitted: any;
  countRequestDatasetOpenDataProcessed: any;
  countRequestDatasetOpenDataAccommodated: any;
  countRequestDatasetOpenDataRejected: any;
  listRequestDatasetSatuData: any;
  detailRequestDatasetSatuData: any;
  historyRequestDatasetSatuData: any;
  formGroupHistoryRequestDatasetSatuDataDecline: UntypedFormGroup;
  formGroupHistoryRequestDatasetSatuDataFinish: UntypedFormGroup;
  listRequestDatasetOpenData: any;
  detailRequestDatasetOpenData: any;
  historyRequestDatasetOpenData: any;
  formGroupHistoryRequestDatasetOpenDataDecline: UntypedFormGroup;
  formGroupHistoryRequestDatasetOpenDataFinish: UntypedFormGroup;
  listRequestDatasetReasonDecline: any;
  listTopicSatuData: any;
  listOrganizationSatuData: any;
}

export const DEFAULT_STATE: DeskPageState = {
  filterSearch: null,
  filterType: 'upload-change',
  filterStatus: 'verification',
  filterApplication: 'satudata',
  filterStatusVerification: 'new',
  filterStatusRequest: 'all',
  filterCatalog: [],
  filterSort: 'cdate',
  filterDirection: 'desc',
  filterPerPage: 10,
  filterCurrentPage: 1,
  listDeskCatalog: null,
  listDeskCatalogDiscontinue: null,
  historyDataset: null,
  historyMapset: null,
  historyIndicator: null,
  countRequestDatasetSatuData: null,
  countRequestDatasetSatuDataSubmitted: null,
  countRequestDatasetSatuDataProcessed: null,
  countRequestDatasetSatuDataAccommodated: null,
  countRequestDatasetSatuDataRejected: null,
  countRequestDatasetOpenData: null,
  countRequestDatasetOpenDataSubmitted: null,
  countRequestDatasetOpenDataProcessed: null,
  countRequestDatasetOpenDataAccommodated: null,
  countRequestDatasetOpenDataRejected: null,
  listRequestDatasetSatuData: null,
  detailRequestDatasetSatuData: null,
  historyRequestDatasetSatuData: null,
  formGroupHistoryRequestDatasetSatuDataDecline: new UntypedFormGroup({
    request_private_id: new UntypedFormControl(),
    status: new UntypedFormControl(),
    notes: new UntypedFormControl(null, [Validators.required]),
  }),
  formGroupHistoryRequestDatasetSatuDataFinish: new UntypedFormGroup({
    request_private_id: new UntypedFormControl(),
    status: new UntypedFormControl(),
    notes: new UntypedFormControl(null, [Validators.required]),
  }),
  listRequestDatasetOpenData: null,
  detailRequestDatasetOpenData: null,
  historyRequestDatasetOpenData: null,
  formGroupHistoryRequestDatasetOpenDataDecline: new UntypedFormGroup({
    request_public_id: new UntypedFormControl(),
    status: new UntypedFormControl(),
    reason_decline: new UntypedFormControl(null, [Validators.required]),
    notes: new UntypedFormControl(null, [Validators.required]),
  }),
  formGroupHistoryRequestDatasetOpenDataFinish: new UntypedFormGroup({
    request_public_id: new UntypedFormControl(),
    status: new UntypedFormControl(),
    source: new UntypedFormControl(null, [Validators.required]),
    link: new UntypedFormArray([]),
    title: new UntypedFormControl(),
    link_drive: new UntypedFormControl(),
    topic: new UntypedFormControl(),
    year: new UntypedFormControl(),
    authority: new UntypedFormControl(),
    organization: new UntypedFormControl(),
    organization_manual: new UntypedFormControl(),
    organization_phone: new UntypedFormControl(),
    notes: new UntypedFormControl(null, [Validators.required]),
  }),
  listRequestDatasetReasonDecline: null,
  listTopicSatuData: null,
  listOrganizationSatuData: null,
};

@Injectable()
export class DeskPageStore extends ComponentStore<DeskPageState> {
  satudataUser: LoginData;

  // Variable
  statusRequestSatuData = Array.from(STATUS_REQUEST_SATUDATA, ([, value]) => ({
    id: value.id,
    name: value.name,
    alert: value.alert,
  }));
  statusRequestOpenData = Array.from(STATUS_REQUEST_OPENDATA, ([, value]) => ({
    id: value.id,
    name: value.name,
    alert: value.alert,
  }));

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private deskCatalogService: DeskCatalogService,
    private historyCatalogStatusService: HistoryCatalogStatusService,
    private requestDatasetSatuDataService: RequestDatasetSatuDataService,
    private historyRequestDatasetSatuDataService: HistoryRequestDatasetSatuDataService,
    private requestDatasetOpenDataService: RequestDatasetOpenDataService,
    private historyRequestDatasetOpenDataService: HistoryRequestDatasetOpenDataService,
    private topicService: TopicService,
    private organizationService: OrganizationService,
  ) {
    super(DEFAULT_STATE);

    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
  }

  // *********** Updaters *********** //
  readonly setFilterSearch = this.updater((state, value: string | null) => ({
    ...state,
    filterSearch: value,
  }));

  readonly setFilterType = this.updater((state, value: TType) => ({
    ...state,
    filterType: value,
  }));

  readonly setFilterStatus = this.updater((state, value: TStatus) => ({
    ...state,
    filterStatus: value,
  }));

  readonly setFilterApplication = this.updater(
    (state, value: TApplication) => ({
      ...state,
      filterApplication: value,
    }),
  );

  readonly setFilterStatusVerification = this.updater(
    (state, value: TStatusVerification) => ({
      ...state,
      filterStatusVerification: value,
    }),
  );

  readonly setFilterStatusRequest = this.updater(
    (state, value: TStatusRequest) => ({
      ...state,
      filterStatusRequest: value,
    }),
  );

  readonly setFilterCatalog = this.updater((state, value: string[]) => ({
    ...state,
    filterCatalog: value,
  }));

  readonly setFilterSort = this.updater((state, value: string) => ({
    ...state,
    filterSort: value,
  }));

  readonly setFilterDirection = this.updater((state, value: string) => ({
    ...state,
    filterDirection: value,
  }));

  readonly setFilterPerPage = this.updater((state, value: number) => ({
    ...state,
    filterPerPage: Number(value),
  }));

  readonly setFilterCurrentPage = this.updater((state, value: number) => ({
    ...state,
    filterCurrentPage: Number(value),
  }));

  readonly setListDeskCatalog = this.updater((state, value: any) => ({
    ...state,
    listDeskCatalog: value,
  }));

  readonly setListDeskCatalogDiscontinue = this.updater(
    (state, value: any) => ({
      ...state,
      listDeskCatalogDiscontinue: value,
    }),
  );

  readonly setHistoryDataset = this.updater((state, value: any) => ({
    ...state,
    historyDataset: value,
  }));

  readonly setHistoryMapset = this.updater((state, value: any) => ({
    ...state,
    historyMapset: value,
  }));

  readonly setHistoryIndicator = this.updater((state, value: any) => ({
    ...state,
    historyIndicator: value,
  }));

  readonly setCountRequestDatasetSatuData = this.updater(
    (state, value: any) => ({
      ...state,
      countRequestDatasetSatuData: value,
    }),
  );

  readonly setCountRequestDatasetSatuDataSubmitted = this.updater(
    (state, value: any) => ({
      ...state,
      countRequestDatasetSatuDataSubmitted: value,
    }),
  );

  readonly setCountRequestDatasetSatuDataProcessed = this.updater(
    (state, value: any) => ({
      ...state,
      countRequestDatasetSatuDataProcessed: value,
    }),
  );

  readonly setCountRequestDatasetSatuDataAccommodated = this.updater(
    (state, value: any) => ({
      ...state,
      countRequestDatasetSatuDataAccommodated: value,
    }),
  );

  readonly setCountRequestDatasetSatuDataRejected = this.updater(
    (state, value: any) => ({
      ...state,
      countRequestDatasetSatuDataRejected: value,
    }),
  );

  readonly setCountRequestDatasetOpenData = this.updater(
    (state, value: any) => ({
      ...state,
      countRequestDatasetOpenData: value,
    }),
  );

  readonly setCountRequestDatasetOpenDataSubmitted = this.updater(
    (state, value: any) => ({
      ...state,
      countRequestDatasetOpenDataSubmitted: value,
    }),
  );

  readonly setCountRequestDatasetOpenDataProcessed = this.updater(
    (state, value: any) => ({
      ...state,
      countRequestDatasetOpenDataProcessed: value,
    }),
  );

  readonly setCountRequestDatasetOpenDataAccommodated = this.updater(
    (state, value: any) => ({
      ...state,
      countRequestDatasetOpenDataAccommodated: value,
    }),
  );

  readonly setCountRequestDatasetOpenDataRejected = this.updater(
    (state, value: any) => ({
      ...state,
      countRequestDatasetOpenDataRejected: value,
    }),
  );

  readonly setListRequestDatasetSatuData = this.updater(
    (state, value: any) => ({
      ...state,
      listRequestDatasetSatuData: value,
    }),
  );

  readonly setDetailRequestDatasetSatuData = this.updater(
    (state, value: any) => ({
      ...state,
      detailRequestDatasetSatuData: value,
    }),
  );

  readonly setHistoryRequestDatasetSatuData = this.updater(
    (state, value: any) => ({
      ...state,
      historyRequestDatasetSatuData: value,
    }),
  );

  readonly setFormGroupHistoryRequestDatasetSatuDataDecline = this.updater(
    (state, value: any) => ({
      ...state,
      formGroupHistoryRequestDatasetSatuDataDecline: value,
    }),
  );

  readonly setFormGroupHistoryRequestDatasetSatuDataFinish = this.updater(
    (state, value: any) => ({
      ...state,
      formGroupHistoryRequestDatasetSatuDataFinish: value,
    }),
  );

  readonly setListRequestDatasetOpenData = this.updater(
    (state, value: any) => ({
      ...state,
      listRequestDatasetOpenData: value,
    }),
  );

  readonly setDetailRequestDatasetOpenData = this.updater(
    (state, value: any) => ({
      ...state,
      detailRequestDatasetOpenData: value,
    }),
  );

  readonly setHistoryRequestDatasetOpenData = this.updater(
    (state, value: any) => ({
      ...state,
      historyRequestDatasetOpenData: value,
    }),
  );

  readonly setFormGroupHistoryRequestDatasetOpenDataDecline = this.updater(
    (state, value: any) => ({
      ...state,
      formGroupHistoryRequestDatasetOpenDataDecline: value,
    }),
  );

  readonly setFormGroupHistoryRequestDatasetOpenDataFinish = this.updater(
    (state, value: any) => ({
      ...state,
      formGroupHistoryRequestDatasetOpenDataFinish: value,
    }),
  );

  readonly setListRequestDatasetReasonDecline = this.updater(
    (state, value: any) => ({
      ...state,
      listRequestDatasetReasonDecline: value,
    }),
  );

  readonly setListTopicSatuData = this.updater((state, value: any) => ({
    ...state,
    listTopicSatuData: value,
  }));

  readonly setListOrganizationSatuData = this.updater((state, value: any) => ({
    ...state,
    listOrganizationSatuData: value,
  }));

  // *********** Selectors *********** //
  // Filter
  readonly getFilterSearch$ = this.select(({ filterSearch }) => filterSearch);
  readonly getFilterType$ = this.select(({ filterType }) => filterType);
  readonly getFilterStatus$ = this.select(({ filterStatus }) => filterStatus);
  readonly getFilterApplication$ = this.select(
    ({ filterApplication }) => filterApplication,
  );
  readonly getFilterStatusVerification$ = this.select(
    ({ filterStatusVerification }) => filterStatusVerification,
  );
  readonly getFilterStatusRequest$ = this.select(
    ({ filterStatusRequest }) => filterStatusRequest,
  );
  readonly getFilterCatalog$ = this.select(
    ({ filterCatalog }) => filterCatalog,
  );
  readonly getFilterSort$ = this.select(({ filterSort }) => filterSort);
  readonly getFilterDirection$ = this.select(
    ({ filterDirection }) => filterDirection,
  );
  readonly getFilterPerPage$ = this.select(
    ({ filterPerPage }) => filterPerPage,
  );
  readonly getFilterCurrentPage$ = this.select(
    ({ filterCurrentPage }) => filterCurrentPage,
  );
  readonly getListDeskCatalog$ = this.select(
    ({ listDeskCatalog }) => listDeskCatalog,
  );
  readonly getListDeskCatalogDiscontinue$ = this.select(
    ({ listDeskCatalogDiscontinue }) => listDeskCatalogDiscontinue,
  );
  readonly getHistoryDataset$ = this.select(
    ({ historyDataset }) => historyDataset,
  );
  readonly getHistoryMapset$ = this.select(
    ({ historyMapset }) => historyMapset,
  );
  readonly getHistoryIndicator$ = this.select(
    ({ historyIndicator }) => historyIndicator,
  );
  // Count
  readonly getCountRequestDatasetSatuData$ = this.select(
    ({ countRequestDatasetSatuData }) => countRequestDatasetSatuData,
  );
  readonly getCountRequestDatasetSatuDataSubmitted$ = this.select(
    ({ countRequestDatasetSatuDataSubmitted }) =>
      countRequestDatasetSatuDataSubmitted,
  );
  readonly getCountRequestDatasetSatuDataProcessed$ = this.select(
    ({ countRequestDatasetSatuDataProcessed }) =>
      countRequestDatasetSatuDataProcessed,
  );
  readonly getCountRequestDatasetSatuDataAccommodated$ = this.select(
    ({ countRequestDatasetSatuDataAccommodated }) =>
      countRequestDatasetSatuDataAccommodated,
  );
  readonly getCountRequestDatasetSatuDataRejected$ = this.select(
    ({ countRequestDatasetSatuDataRejected }) =>
      countRequestDatasetSatuDataRejected,
  );
  readonly getCountRequestDatasetOpenData$ = this.select(
    ({ countRequestDatasetOpenData }) => countRequestDatasetOpenData,
  );
  readonly getCountRequestDatasetOpenDataSubmitted$ = this.select(
    ({ countRequestDatasetOpenDataSubmitted }) =>
      countRequestDatasetOpenDataSubmitted,
  );
  readonly getCountRequestDatasetOpenDataProcessed$ = this.select(
    ({ countRequestDatasetOpenDataProcessed }) =>
      countRequestDatasetOpenDataProcessed,
  );
  readonly getCountRequestDatasetOpenDataAccommodated$ = this.select(
    ({ countRequestDatasetOpenDataAccommodated }) =>
      countRequestDatasetOpenDataAccommodated,
  );
  readonly getCountRequestDatasetOpenDataRejected$ = this.select(
    ({ countRequestDatasetOpenDataRejected }) =>
      countRequestDatasetOpenDataRejected,
  );
  // List
  readonly getListRequestDatasetSatuData$ = this.select(
    ({ listRequestDatasetSatuData }) => listRequestDatasetSatuData,
  );
  readonly getDetailRequestDatasetSatuData$ = this.select(
    ({ detailRequestDatasetSatuData }) => detailRequestDatasetSatuData,
  );
  readonly getHistoryRequestDatasetSatuData$ = this.select(
    ({ historyRequestDatasetSatuData }) => historyRequestDatasetSatuData,
  );
  readonly getFormGroupHistoryRequestDatasetSatuDataDecline$ = this.select(
    ({ formGroupHistoryRequestDatasetSatuDataDecline }) =>
      formGroupHistoryRequestDatasetSatuDataDecline,
  );
  readonly getFormGroupHistoryRequestDatasetSatuDataFinish$ = this.select(
    ({ formGroupHistoryRequestDatasetSatuDataFinish }) =>
      formGroupHistoryRequestDatasetSatuDataFinish,
  );
  readonly getListRequestDatasetOpenData$ = this.select(
    ({ listRequestDatasetOpenData }) => listRequestDatasetOpenData,
  );
  readonly getDetailRequestDatasetOpenData$ = this.select(
    ({ detailRequestDatasetOpenData }) => detailRequestDatasetOpenData,
  );
  readonly getHistoryRequestDatasetOpenData$ = this.select(
    ({ historyRequestDatasetOpenData }) => historyRequestDatasetOpenData,
  );
  readonly getFormGroupHistoryRequestDatasetOpenDataDecline$ = this.select(
    ({ formGroupHistoryRequestDatasetOpenDataDecline }) =>
      formGroupHistoryRequestDatasetOpenDataDecline,
  );
  readonly getFormGroupHistoryRequestDatasetOpenDataFinish$ = this.select(
    ({ formGroupHistoryRequestDatasetOpenDataFinish }) =>
      formGroupHistoryRequestDatasetOpenDataFinish,
  );
  readonly getListRequestDatasetReasonDecline$ = this.select(
    ({ listRequestDatasetReasonDecline }) => listRequestDatasetReasonDecline,
  );
  readonly getListTopicSatuData$ = this.select(
    ({ listTopicSatuData }) => listTopicSatuData,
  );
  readonly getListOrganizationSatuData$ = this.select(
    ({ listOrganizationSatuData }) => listOrganizationSatuData,
  );

  // ViewModel of Paginator component
  readonly vm$ = this.select(
    this.state$,
    this.getFilterSearch$,
    this.getFilterType$,
    this.getFilterStatus$,
    this.getFilterApplication$,
    this.getFilterStatusVerification$,
    this.getFilterStatusRequest$,
    this.getFilterCatalog$,
    this.getFilterSort$,
    this.getFilterDirection$,
    this.getFilterPerPage$,
    this.getFilterCurrentPage$,
    this.getListDeskCatalog$,
    this.getListDeskCatalogDiscontinue$,
    this.getHistoryDataset$,
    this.getHistoryMapset$,
    this.getHistoryIndicator$,
    this.getCountRequestDatasetSatuData$,
    this.getCountRequestDatasetSatuDataSubmitted$,
    this.getCountRequestDatasetSatuDataProcessed$,
    this.getCountRequestDatasetSatuDataAccommodated$,
    this.getCountRequestDatasetSatuDataRejected$,
    this.getCountRequestDatasetOpenData$,
    this.getCountRequestDatasetOpenDataSubmitted$,
    this.getCountRequestDatasetOpenDataProcessed$,
    this.getCountRequestDatasetOpenDataAccommodated$,
    this.getCountRequestDatasetOpenDataRejected$,
    this.getListRequestDatasetSatuData$,
    this.getDetailRequestDatasetSatuData$,
    this.getHistoryRequestDatasetSatuData$,
    this.getFormGroupHistoryRequestDatasetSatuDataDecline$,
    this.getFormGroupHistoryRequestDatasetSatuDataFinish$,
    this.getListRequestDatasetOpenData$,
    this.getDetailRequestDatasetOpenData$,
    this.getHistoryRequestDatasetOpenData$,
    this.getFormGroupHistoryRequestDatasetOpenDataDecline$,
    this.getFormGroupHistoryRequestDatasetOpenDataFinish$,
    this.getListRequestDatasetReasonDecline$,
    this.getListTopicSatuData$,
    this.getListOrganizationSatuData$,
    (
      state,
      getFilterSearch,
      getFilterType,
      getFilterStatus,
      getFilterApplication,
      getFilterStatusVerification,
      getFilterStatusRequest,
      getFilterCatalog,
      getFilterSort,
      getFilterDirection,
      getFilterPerPage,
      getFilterCurrentPage,
      getListDeskCatalog,
      getListDeskCatalogDiscontinue,
      getHistoryDataset,
      getHistoryMapset,
      getHistoryIndicator,
      getCountRequestDatasetSatuData,
      getCountRequestDatasetSatuDataSubmitted,
      getCountRequestDatasetSatuDataProcessed,
      getCountRequestDatasetSatuDataAccommodated,
      getCountRequestDatasetSatuDataRejected,
      getCountRequestDatasetOpenData,
      getCountRequestDatasetOpenDataSubmitted,
      getCountRequestDatasetOpenDataProcessed,
      getCountRequestDatasetOpenDataAccommodated,
      getCountRequestDatasetOpenDataRejected,
      getListRequestDatasetSatuData,
      getDetailRequestDatasetSatuData,
      getHistoryRequestDatasetSatuData,
      getFormGroupHistoryRequestDatasetSatuDataDecline,
      getFormGroupHistoryRequestDatasetSatuDataFinish,
      getListRequestDatasetOpenData,
      getDetailRequestDatasetOpenData,
      getHistoryRequestDatasetOpenData,
      getFormGroupHistoryRequestDatasetOpenDataDecline,
      getFormGroupHistoryRequestDatasetOpenDataFinish,
      getListRequestDatasetReasonDecline,
      getListTopicSatuData,
      getListOrganizationSatuData,
    ) => ({
      filterSearch: state.filterSearch,
      filterType: state.filterType,
      filterStatus: state.filterStatus,
      filterApplication: state.filterApplication,
      filterStatusVerification: state.filterStatusVerification,
      filterStatusRequest: state.filterStatusRequest,
      filterCatalog: state.filterCatalog,
      filterSort: state.filterSort,
      filterDirection: state.filterDirection,
      filterPerPage: state.filterPerPage,
      filterCurrentPage: state.filterCurrentPage,
      listDeskCatalog: state.listDeskCatalog,
      listDeskCatalogDiscontinue: state.listDeskCatalogDiscontinue,
      historyDataset: state.historyDataset,
      historyMapset: state.historyMapset,
      historyIndicator: state.historyIndicator,
      countRequestDatasetSatuData: state.countRequestDatasetSatuData,
      countRequestDatasetSatuDataSubmitted:
        state.countRequestDatasetSatuDataSubmitted,
      countRequestDatasetSatuDataProcessed:
        state.countRequestDatasetSatuDataProcessed,
      countRequestDatasetSatuDataAccommodated:
        state.countRequestDatasetSatuDataAccommodated,
      countRequestDatasetSatuDataRejected:
        state.countRequestDatasetSatuDataRejected,
      countRequestDatasetOpenData: state.countRequestDatasetOpenData,
      countRequestDatasetOpenDataSubmitted:
        state.countRequestDatasetOpenDataSubmitted,
      countRequestDatasetOpenDataProcessed:
        state.countRequestDatasetOpenDataProcessed,
      countRequestDatasetOpenDataAccommodated:
        state.countRequestDatasetOpenDataAccommodated,
      countRequestDatasetOpenDataRejected:
        state.countRequestDatasetOpenDataRejected,
      listRequestDatasetSatuData: state.listRequestDatasetSatuData,
      detailRequestDatasetSatuData: state.detailRequestDatasetSatuData,
      historyRequestDatasetSatuData: state.historyRequestDatasetSatuData,
      formGroupHistoryRequestDatasetSatuDataDecline:
        state.formGroupHistoryRequestDatasetSatuDataDecline,
      formGroupHistoryRequestDatasetSatuDataFinish:
        state.formGroupHistoryRequestDatasetSatuDataFinish,
      listRequestDatasetOpenData: state.listRequestDatasetOpenData,
      detailRequestDatasetOpenData: state.detailRequestDatasetOpenData,
      historyRequestDatasetOpenData: state.historyRequestDatasetOpenData,
      formGroupHistoryRequestDatasetOpenDataDecline:
        state.formGroupHistoryRequestDatasetOpenDataDecline,
      formGroupHistoryRequestDatasetOpenDataFinish:
        state.formGroupHistoryRequestDatasetOpenDataFinish,
      listRequestDatasetReasonDecline: state.listRequestDatasetReasonDecline,
      listTopicSatuData: state.listTopicSatuData,
      listOrganizationSatuData: state.listOrganizationSatuData,
      getFilterSearch,
      getFilterType,
      getFilterStatus,
      getFilterApplication,
      getFilterStatusVerification,
      getFilterStatusRequest,
      getFilterCatalog,
      getFilterSort,
      getFilterDirection,
      getFilterPerPage,
      getFilterCurrentPage,
      getListDeskCatalog,
      getListDeskCatalogDiscontinue,
      getHistoryDataset,
      getHistoryMapset,
      getHistoryIndicator,
      getCountRequestDatasetSatuData,
      getCountRequestDatasetSatuDataSubmitted,
      getCountRequestDatasetSatuDataProcessed,
      getCountRequestDatasetSatuDataAccommodated,
      getCountRequestDatasetSatuDataRejected,
      getCountRequestDatasetOpenData,
      getCountRequestDatasetOpenDataSubmitted,
      getCountRequestDatasetOpenDataProcessed,
      getCountRequestDatasetOpenDataAccommodated,
      getCountRequestDatasetOpenDataRejected,
      getListRequestDatasetSatuData,
      getDetailRequestDatasetSatuData,
      getHistoryRequestDatasetSatuData,
      getFormGroupHistoryRequestDatasetSatuDataDecline,
      getFormGroupHistoryRequestDatasetSatuDataFinish,
      getListRequestDatasetOpenData,
      getDetailRequestDatasetOpenData,
      getHistoryRequestDatasetOpenData,
      getFormGroupHistoryRequestDatasetOpenDataDecline,
      getFormGroupHistoryRequestDatasetOpenDataFinish,
      getListRequestDatasetReasonDecline,
      getListTopicSatuData,
      getListOrganizationSatuData,
    }),
  );

  // *********** Actions *********** //
  readonly filterSearch = this.effect((search$: Observable<string>) => {
    return search$.pipe(
      withLatestFrom(this.getFilterSearch$),
      tap<[string, string]>(([search, previous]) => {
        const value = isEmpty(search) === false ? search : null;

        if (previous !== value) {
          this.router.navigate([], {
            relativeTo: this.route,
            queryParams: {
              q: value,
              page: null,
            },
            queryParamsHandling: 'merge',
          });

          this.setFilterSearch(value);
          this.setFilterCurrentPage(DEFAULT_STATE.filterCurrentPage);
        }
      }),
      map(() => {
        this.getAllDeskData();
      }),
    );
  });

  readonly filterType = this.effect((type$: Observable<TType>) => {
    return type$.pipe(
      withLatestFrom(this.getFilterType$),
      tap<[TType, TType]>(([type]) => {
        this.router.navigate([], {
          relativeTo: this.route,
          queryParams: {
            type,
            status: null,
            application: null,
            statusVerification: null,
            statusRequest: null,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        this.setFilterType(type);
        this.setFilterStatus(DEFAULT_STATE.filterStatus);
        this.setFilterApplication(DEFAULT_STATE.filterApplication);
        this.setFilterStatusVerification(
          DEFAULT_STATE.filterStatusVerification,
        );
        this.setFilterStatusRequest(DEFAULT_STATE.filterStatusRequest);
        this.setFilterSort(
          type === 'upload-change' || type === 'discontinue'
            ? DEFAULT_STATE.filterSort
            : 'newest',
        );
        this.setFilterCurrentPage(DEFAULT_STATE.filterCurrentPage);
      }),
      map(() => {
        this.getAllDeskData();
      }),
    );
  });

  readonly filterStatus = this.effect((status$: Observable<TStatus>) => {
    return status$.pipe(
      withLatestFrom(this.getFilterStatus$),
      tap<[TStatus, TStatus]>(([status]) => {
        this.router.navigate([], {
          relativeTo: this.route,
          queryParams: {
            status,
            statusVerification: null,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        this.setFilterStatus(status);
        this.setFilterStatusVerification(
          DEFAULT_STATE.filterStatusVerification,
        );
        this.setFilterSort(
          status === 'verification' ? DEFAULT_STATE.filterSort : 'mdate',
        );
        this.setFilterCurrentPage(DEFAULT_STATE.filterCurrentPage);
      }),
      map(() => {
        this.getAllDeskData();
      }),
    );
  });

  readonly filterApplication = this.effect(
    (application$: Observable<TApplication>) => {
      return application$.pipe(
        withLatestFrom(this.getFilterApplication$),
        tap<[TApplication, TApplication]>(([application]) => {
          this.router.navigate([], {
            relativeTo: this.route,
            queryParams: {
              application,
              statusRequest: null,
              page: null,
            },
            queryParamsHandling: 'merge',
          });

          this.setFilterApplication(application);
          this.setFilterStatusRequest(DEFAULT_STATE.filterStatusRequest);
          this.setFilterCurrentPage(DEFAULT_STATE.filterCurrentPage);
        }),
        map(() => {
          this.getAllDeskData();
        }),
      );
    },
  );

  readonly filterStatusVerification = this.effect(
    (statusVerification$: Observable<TStatusVerification>) => {
      return statusVerification$.pipe(
        withLatestFrom(this.getFilterStatusVerification$),
        tap<[TStatusVerification, TStatusVerification]>(
          ([statusVerification]) => {
            this.router.navigate([], {
              relativeTo: this.route,
              queryParams: {
                statusVerification,
                page: null,
              },
              queryParamsHandling: 'merge',
            });

            this.setFilterStatusVerification(statusVerification);
            this.setFilterSort(
              statusVerification === 'new' ? DEFAULT_STATE.filterSort : 'mdate',
            );
            this.setFilterCurrentPage(DEFAULT_STATE.filterCurrentPage);
          },
        ),
        map(() => {
          this.getAllDeskData();
        }),
      );
    },
  );

  readonly filterStatusRequest = this.effect(
    (statusRequest$: Observable<TStatusRequest>) => {
      return statusRequest$.pipe(
        withLatestFrom(this.getFilterStatusRequest$),
        tap<[TStatusRequest, TStatusRequest]>(([statusRequest]) => {
          this.router.navigate([], {
            relativeTo: this.route,
            queryParams: {
              statusRequest,
              page: null,
            },
            queryParamsHandling: 'merge',
          });

          this.setFilterStatusRequest(statusRequest);
          this.setFilterCurrentPage(DEFAULT_STATE.filterCurrentPage);
        }),
        map(() => {
          this.getAllDeskData();
        }),
      );
    },
  );

  readonly filterCatalog = this.effect((catalog$: Observable<string[]>) => {
    return catalog$.pipe(
      withLatestFrom(this.getFilterCatalog$),
      tap<[string[], string[]]>(([catalog]) => {
        this.setFilterCatalog(catalog);
        this.setFilterCurrentPage(DEFAULT_STATE.filterCurrentPage);
      }),
      map(() => {
        this.getAllDeskData();
      }),
    );
  });

  readonly filterSort = this.effect((sort$: Observable<string>) => {
    return sort$.pipe(
      withLatestFrom(this.getFilterSort$),
      tap<[string, string]>(([sort]) => {
        this.setFilterSort(sort);
        this.setFilterCurrentPage(DEFAULT_STATE.filterCurrentPage);
      }),
      map(() => {
        this.getAllDeskData();
      }),
    );
  });

  readonly filterSortTable = this.effect((sort$: Observable<string>) => {
    return sort$.pipe(
      tap((sort) => {
        const obj = sort.split(':');

        this.setFilterSort(obj[0]);
        this.setFilterDirection(obj[1]);
        this.setFilterCurrentPage(DEFAULT_STATE.filterCurrentPage);
      }),
      map(() => {
        this.getAllDeskData();
      }),
    );
  });

  readonly filterPerPage = this.effect((perPage$: Observable<number>) => {
    return perPage$.pipe(
      withLatestFrom(this.getFilterPerPage$),
      tap<[number, number]>(([perPage]) => {
        this.router.navigate([], {
          relativeTo: this.route,
          queryParams: {
            perPage,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        this.setFilterPerPage(perPage);
        this.setFilterCurrentPage(DEFAULT_STATE.filterCurrentPage);
      }),
      map(() => {
        this.getAllDeskData();
      }),
    );
  });

  readonly filterCurrentPage = this.effect(
    (currentPage$: Observable<number>) => {
      return currentPage$.pipe(
        withLatestFrom(this.getFilterCurrentPage$),
        tap<[number, number]>(([currentPage]) => {
          this.router.navigate([], {
            relativeTo: this.route,
            queryParams: {
              page: currentPage,
            },
            queryParamsHandling: 'merge',
          });

          this.setFilterCurrentPage(currentPage);
        }),
        map(() => {
          this.getAllDeskData();
        }),
      );
    },
  );

  readonly getAllDeskData = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      withLatestFrom(this.vm$),
      tap(([, vm]) => {
        switch (vm.filterType) {
          case 'upload-change':
            this.getAllDeskCatalog();
            break;
          case 'discontinue':
            this.getAllDeskCatalogDiscontinue();
            break;
          case 'request-dataset':
            switch (vm.filterApplication) {
              case 'satudata':
                this.getCountRequestDatasetSatuData();
                this.getAllRequestDatasetSatuData();
                break;
              case 'opendata':
                this.getCountRequestDatasetOpenData();
                this.getAllRequestDatasetOpenData();
                break;
              default:
                break;
            }
          // eslint-disable-next-line no-fallthrough
          default:
            break;
        }

        return null;
      }),
    );
  });

  // Desk Catalog
  readonly getAllDeskCatalog = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      withLatestFrom(this.vm$),
      switchMap(([, vm]) => {
        const {
          filterStatus: status,
          filterStatusVerification: statusVerification,
          filterSearch: search,
          filterCatalog: catalog,
          filterSort: sort,
          filterDirection: direction,
          filterPerPage: perPage,
          filterCurrentPage: currentPage,
        } = vm;

        const pWhere = {
          is_active: true,
          is_deleted: false,
        };

        if (status === 'verification') {
          if (statusVerification === 'new') {
            assign(pWhere, { is_validate: 1 });
          } else if (statusVerification === 'edit') {
            assign(pWhere, { is_validate: 4 });
          }
        } else if (status === 'revision') {
          assign(pWhere, { is_validate: 2 });
        } else if (status === 'approve') {
          assign(pWhere, { is_validate: 3 });
        }

        if (this.satudataUser.role_name === 'opd') {
          assign(pWhere, { kode_skpd: this.satudataUser.kode_skpd });
        }

        if (!isEmpty(catalog)) {
          const arr = catalog.map((item) => `${item}`);
          assign(pWhere, { type: arr });
        }

        const params = {
          search: search || '',
          sort: `${sort}:${direction}`,
          skip: perPage * currentPage - perPage,
          limit: perPage,
          where: JSON.stringify(pWhere),
        };

        return this.deskCatalogService.getQueryList(params).result$.pipe(
          tap({
            next: (result) => {
              this.setListDeskCatalog(result);
            },
          }),
        );
      }),
    );
  });

  // Desk Catalog Discontinue
  readonly getAllDeskCatalogDiscontinue = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      withLatestFrom(this.vm$),
      switchMap(([, vm]) => {
        const {
          filterStatus: status,
          filterSearch: search,
          filterCatalog: catalog,
          filterSort: sort,
          filterDirection: direction,
          filterPerPage: perPage,
          filterCurrentPage: currentPage,
        } = vm;

        const pWhere = {};

        if (status === 'verification') {
          assign(pWhere, {
            is_validate: 6,
            is_deleted: false,
            is_discontinue: 1,
          });
        } else if (status === 'approve') {
          assign(pWhere, {
            is_validate: 3,
            is_deleted: false,
            is_discontinue: 3,
          });
        }

        if (this.satudataUser.role_name === 'opd') {
          assign(pWhere, { kode_skpd: this.satudataUser.kode_skpd });
        }

        if (!isEmpty(catalog)) {
          const arr = catalog.map((item) => `${item}`);
          assign(pWhere, { type: arr });
        }

        const params = {
          search: search || '',
          sort: `${sort}:${direction}`,
          skip: perPage * currentPage - perPage,
          limit: perPage,
          where: JSON.stringify(pWhere),
        };

        return this.deskCatalogService.getQueryList(params).result$.pipe(
          tap({
            next: (result) => {
              this.setListDeskCatalogDiscontinue(result);
            },
          }),
        );
      }),
    );
  });

  // History Desk Catalog
  readonly getHistoryDataset = this.effect((id$: Observable<number>) => {
    return id$.pipe(
      switchMap((id) => {
        const pWhere = {
          type: 'dataset',
          type_id: id,
        };

        const params = {
          sort: 'datetime:desc',
          where: JSON.stringify(pWhere),
        };

        const options = {
          pagination: false,
        };

        return this.historyCatalogStatusService
          .getQueryList(id, params, options)
          .result$.pipe(
            tap({
              next: (result) => {
                this.setHistoryDataset(result);
              },
            }),
          );
      }),
    );
  });

  readonly getHistoryMapset = this.effect((id$: Observable<number>) => {
    return id$.pipe(
      switchMap((id) => {
        const pWhere = {
          type: 'mapset',
          type_id: id,
        };

        const params = {
          sort: 'datetime:desc',
          where: JSON.stringify(pWhere),
        };

        const options = {
          pagination: false,
        };

        return this.historyCatalogStatusService
          .getQueryList(id, params, options)
          .result$.pipe(
            tap({
              next: (result) => {
                this.setHistoryMapset(result);
              },
            }),
          );
      }),
    );
  });

  readonly getHistoryIndicator = this.effect((id$: Observable<number>) => {
    return id$.pipe(
      switchMap((id) => {
        const pWhere = {
          type: 'indicator',
          type_id: id,
        };

        const params = {
          sort: 'datetime:desc',
          where: JSON.stringify(pWhere),
        };

        const options = {
          pagination: false,
        };

        return this.historyCatalogStatusService
          .getQueryList(id, params, options)
          .result$.pipe(
            tap({
              next: (result) => {
                this.setHistoryIndicator(result);
              },
            }),
          );
      }),
    );
  });

  // Request Dataset Satu Data
  readonly getCountRequestDatasetSatuData = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      withLatestFrom(this.vm$),
      switchMap(([, vm]) => {
        const { filterSearch: search } = vm;

        const params = {
          search: search || '',
          where: JSON.stringify({
            is_deleted: false,
          }),
        };

        const options = {};

        const getAll =
          this.requestDatasetSatuDataService.getQueryTotal(params).result$;

        const getSubmitted = this.requestDatasetSatuDataService.getQueryTotal(
          params,
          options,
          'submitted',
        ).result$;

        const getProcessed = this.requestDatasetSatuDataService.getQueryTotal(
          params,
          options,
          'processed',
        ).result$;

        const getAccommodated =
          this.requestDatasetSatuDataService.getQueryTotal(
            params,
            options,
            'accommodated',
          ).result$;

        const getRejected = this.requestDatasetSatuDataService.getQueryTotal(
          params,
          options,
          'rejected',
        ).result$;

        return zip(
          getAll,
          getSubmitted,
          getProcessed,
          getAccommodated,
          getRejected,
        ).pipe(
          tap({
            next: ([all, submitted, processed, accommodated, rejected]) => {
              this.setCountRequestDatasetSatuData(all);
              this.setCountRequestDatasetSatuDataSubmitted(submitted);
              this.setCountRequestDatasetSatuDataProcessed(processed);
              this.setCountRequestDatasetSatuDataAccommodated(accommodated);
              this.setCountRequestDatasetSatuDataRejected(rejected);
            },
          }),
        );
      }),
    );
  });

  readonly getAllRequestDatasetSatuData = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      withLatestFrom(this.vm$),
      switchMap(([, vm]) => {
        const {
          filterSearch: search,
          filterStatusRequest: statusRequest,
          filterSort: sort,
          filterPerPage: perPage,
          filterCurrentPage: currentPage,
        } = vm;

        const pWhere = {
          is_deleted: false,
        };

        if (statusRequest !== 'all') {
          assign(pWhere, {
            status: STATUS_REQUEST_SATUDATA.get(statusRequest).id,
          });
        }

        const params = {
          search: search || '',
          sort: SORT_REQUEST.get(sort).paramSort,
          skip: perPage * currentPage - perPage,
          limit: perPage,
          where: JSON.stringify(pWhere),
        };

        return this.requestDatasetSatuDataService
          .getQueryList(params)
          .result$.pipe(
            tap({
              next: (result) => {
                this.setListRequestDatasetSatuData(result);
              },
            }),
          );
      }),
    );
  });

  readonly getRequestDatasetSatuData = this.effect(
    (id$: Observable<number>) => {
      return id$.pipe(
        switchMap((id) => {
          return this.requestDatasetSatuDataService
            .getQuerySingle(id, {})
            .result$.pipe(
              tap({
                next: (result) => {
                  this.setDetailRequestDatasetSatuData(result);
                },
              }),
            );
        }),
      );
    },
  );

  // History Request Dataset Satu Data
  readonly getHistoryRequestDatasetSatuData = this.effect(
    (id$: Observable<number>) => {
      return id$.pipe(
        switchMap((id) => {
          const pWhere = {
            request_private_id: id,
          };

          const params = {
            sort: 'datetime:desc',
            where: JSON.stringify(pWhere),
          };

          const options = {
            pagination: false,
          };

          return this.historyRequestDatasetSatuDataService
            .getQueryList(id, params, options)
            .result$.pipe(
              tap({
                next: (result) => {
                  this.setHistoryRequestDatasetSatuData(result);
                },
              }),
            );
        }),
      );
    },
  );

  // Request Dataset Open Data
  readonly getCountRequestDatasetOpenData = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      withLatestFrom(this.vm$),
      switchMap(([, vm]) => {
        const { filterSearch: search } = vm;

        const params = {
          search: search || '',
          where: JSON.stringify({
            is_deleted: false,
          }),
        };

        const options = {};

        const getAll =
          this.requestDatasetOpenDataService.getQueryTotal(params).result$;

        const getSubmitted = this.requestDatasetOpenDataService.getQueryTotal(
          params,
          options,
          'submitted',
        ).result$;

        const getProcessed = this.requestDatasetOpenDataService.getQueryTotal(
          params,
          options,
          'processed',
        ).result$;

        const getAccommodated =
          this.requestDatasetOpenDataService.getQueryTotal(
            params,
            options,
            'accommodated',
          ).result$;

        const getRejected = this.requestDatasetOpenDataService.getQueryTotal(
          params,
          options,
          'rejected',
        ).result$;

        return zip(
          getAll,
          getSubmitted,
          getProcessed,
          getAccommodated,
          getRejected,
        ).pipe(
          tap({
            next: ([all, submitted, processed, accommodated, rejected]) => {
              this.setCountRequestDatasetOpenData(all);
              this.setCountRequestDatasetOpenDataSubmitted(submitted);
              this.setCountRequestDatasetOpenDataProcessed(processed);
              this.setCountRequestDatasetOpenDataAccommodated(accommodated);
              this.setCountRequestDatasetOpenDataRejected(rejected);
            },
          }),
        );
      }),
    );
  });

  readonly getAllRequestDatasetOpenData = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      withLatestFrom(this.vm$),
      switchMap(([, vm]) => {
        const {
          filterSearch: search,
          filterStatusRequest: statusRequest,
          filterSort: sort,
          filterPerPage: perPage,
          filterCurrentPage: currentPage,
        } = vm;

        const pWhere = {
          is_deleted: false,
        };

        if (statusRequest !== 'all') {
          assign(pWhere, {
            status: STATUS_REQUEST_OPENDATA.get(statusRequest).id,
          });
        }

        const params = {
          search: search || '',
          sort: SORT_REQUEST.get(sort).paramSort,
          skip: perPage * currentPage - perPage,
          limit: perPage,
          where: JSON.stringify(pWhere),
        };

        return this.requestDatasetOpenDataService
          .getQueryList(params)
          .result$.pipe(
            tap({
              next: (result) => {
                this.setListRequestDatasetOpenData(result);
              },
            }),
          );
      }),
    );
  });

  readonly getRequestDatasetOpenData = this.effect(
    (id$: Observable<number>) => {
      return id$.pipe(
        switchMap((id) => {
          return this.requestDatasetOpenDataService
            .getQuerySingle(id, {})
            .result$.pipe(
              tap({
                next: (result) => {
                  this.setDetailRequestDatasetOpenData(result);
                },
              }),
            );
        }),
      );
    },
  );

  // History Request Dataset Open Data
  readonly getHistoryRequestDatasetOpenData = this.effect(
    (id$: Observable<number>) => {
      return id$.pipe(
        switchMap((id) => {
          const pWhere = {
            request_public_id: id,
          };

          const params = {
            sort: 'datetime:desc',
            where: JSON.stringify(pWhere),
          };

          const options = {
            pagination: false,
          };

          return this.historyRequestDatasetOpenDataService
            .getQueryList(id, params, options)
            .result$.pipe(
              tap({
                next: (result) => {
                  this.setHistoryRequestDatasetOpenData(result);
                },
              }),
            );
        }),
      );
    },
  );

  // Request Dataset Action
  readonly getAllRequestDatasetReasonDecline = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      withLatestFrom(this.vm$),
      switchMap(() => {
        const status = Array.from(
          STATUS_REQUEST_OPENDATA_REASON_DECLINE,
          ([, value]) => ({
            id: value.id,
            name: value.name,
          }),
        );

        const data = of({
          isSuccess: true,
          data: {
            list: status,
          },
        });

        return data.pipe(
          tap({
            next: (result) => {
              this.setListRequestDatasetReasonDecline(result);
            },
          }),
        );
      }),
    );
  });

  readonly getAllTopicSatuData = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      switchMap(() => {
        const params = {
          sort: `name:asc`,
          where: JSON.stringify({
            is_deleted: false,
          }),
        };

        const options = {
          pagination: false,
        };
        const app = 'satudata';

        return this.topicService
          .getQueryList(params, options, app)
          .result$.pipe(
            tap({
              next: (result) => {
                this.setListTopicSatuData(result);
              },
            }),
          );
      }),
    );
  });

  readonly getAllOrganizationSatuData = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      switchMap(() => {
        const params = {
          sort: `nama_skpd:asc`,
          where: JSON.stringify({
            is_deleted: false,
          }),
        };

        const options = {
          pagination: false,
        };
        const app = 'satudata';

        return this.organizationService
          .getQueryList(params, options, app)
          .result$.pipe(
            tap({
              next: (result) => {
                this.setListOrganizationSatuData(result);
              },
            }),
          );
      }),
    );
  });
}
