import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  FormsModule,
  ReactiveFormsModule,
  UntypedFormGroup,
} from '@angular/forms';
import { zip } from 'rxjs';

// STYLE
import * as jds from '@styles';
// INTERFACE
import { Styles } from '@interfaces-v4';
// CONSTANT
import { COLORSCHEME, STATUS_REQUEST_OPENDATA } from '@constants-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';
import {
  RequestDatasetOpenDataService,
  HistoryRequestDatasetOpenDataService,
  NotificationService,
} from '@services-v4';
// JDS-BI
import {
  JdsBiButtonModule,
  JdsBiModalModule,
  JdsBiModalService,
  JdsBiInputModule,
} from '@jds-bi/core';
import { JdsBiIconsModule, JdsBiIconsService, iconXmark } from '@jds-bi/icons';

import { find } from 'lodash';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { LetDirective } from '@ngrx/component';
import { useMutationResult } from '@ngneat/query';
import { SubscribeDirective } from '@ngneat/subscribe';
import { NgxLoadingModule } from 'ngx-loading';
import Swal from 'sweetalert2';
import { NgSelectModule } from '@ng-select/ng-select';

import { DeskPageStore } from '../../containers/desk-page/desk-page.store';

@Component({
  selector: 'app-desk-modal-request-opendata-decline',
  standalone: true,
  imports: [
    CommonModule,
    JdsBiButtonModule,
    JdsBiIconsModule,
    JdsBiModalModule,
    JdsBiInputModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSkeletonLoaderModule,
    LetDirective,
    NgxLoadingModule,
    SubscribeDirective,
    NgSelectModule,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './desk-modal-request-opendata-decline.component.html',
})
export class DeskModalRequestOpenDataDeclineComponent {
  satudataUser: LoginData;
  colorScheme = COLORSCHEME;
  jds: Styles = jds;

  // Service
  private jdsBiModalService = inject(JdsBiModalService);
  private deskPageStore = inject(DeskPageStore);

  private authenticationService = inject(AuthenticationService);
  private notificationService = inject(NotificationService);
  private requestDatasetService = inject(RequestDatasetOpenDataService);
  private historyRequestDatasetService = inject(
    HistoryRequestDatasetOpenDataService,
  );
  updateRequestDatasetMutation = useMutationResult();

  // Variable
  formGroup: UntypedFormGroup;
  statusRequest = Array.from(STATUS_REQUEST_OPENDATA, ([, value]) => ({
    id: value.id,
    name: value.name,
    color: value.color,
    alert: value.alert,
  }));

  // Component Store
  readonly vm$ = this.deskPageStore.vm$;

  constructor(private JdsBiIconService: JdsBiIconsService) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    this.deskPageStore.getFormGroupHistoryRequestDatasetOpenDataDecline$.subscribe(
      (value: UntypedFormGroup) => {
        this.formGroup = value;
      },
    );

    this.JdsBiIconService.registerIcons([iconXmark]);
  }

  get f() {
    return this.formGroup.controls;
  }

  openModal() {
    this.jdsBiModalService.open('desk-modal-request-opendata-decline');
  }

  closeModal() {
    this.jdsBiModalService.close('desk-modal-request-opendata-decline');
  }

  onSubmit() {
    this.closeModal();

    const {
      request_public_id: id,
      status,
      reason_decline: reasonDecline,
      notes,
    } = this.formGroup.value;

    // Notification
    const bodyNotification = { is_enable: false, is_read: true };
    const paramsNotification = {
      type: 'request_public',
      type_id: id,
      receiver: this.satudataUser.id,
    };

    const updateNotification = this.notificationService.updateItemBulk(
      bodyNotification,
      paramsNotification,
    );

    // Request Dataset
    const bodyRequestDataset = {
      id,
      status,
      notes,
    };

    const updateRequestDataset = this.requestDatasetService.updateItem(
      id,
      bodyRequestDataset,
    );

    // History Request Dataset
    const bodyHistoryRequestDataset = {
      request_public_id: id,
      status,
      notes_type: reasonDecline,
      notes,
    };

    const createHistoryRequestDataset =
      this.historyRequestDatasetService.createItem(bodyHistoryRequestDataset);

    zip(updateNotification, createHistoryRequestDataset).subscribe(() => {
      updateRequestDataset
        .pipe(this.updateRequestDatasetMutation.track())
        .subscribe(({ data: requestDataset }) => {
          const { alert } = find(this.statusRequest, {
            id: requestDataset.status,
          });

          Swal.fire({
            type: 'success',
            text: alert,
            allowOutsideClick: false,
          }).then(() => {
            this.formGroup.reset();

            this.deskPageStore.setFormGroupHistoryRequestDatasetOpenDataDecline(
              this.formGroup,
            );
          });
        });
    });
  }
}
