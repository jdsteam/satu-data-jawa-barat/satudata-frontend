import { Component, ChangeDetectionStrategy } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// STYLE
import * as jds from '@styles';
// INTERFACE
import { Styles } from '@interfaces-v4';
// CONSTANT
import {
  COLORSCHEME,
  STATUS_REQUEST_OPENDATA as STATUS_REQUEST,
} from '@constants-v4';
// JDS-BI
import { JdsBiBadgeModule, JdsBiNavModule } from '@jds-bi/core';

import { NgSelectModule } from '@ng-select/ng-select';

import { DeskSortRequestComponent } from '../desk-sort-request/desk-sort-request.component';
import {
  DeskPageStore,
  TStatusRequest,
} from '../../containers/desk-page/desk-page.store';

@Component({
  selector: 'app-desk-status-request-opendata',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    NgSelectModule,
    JdsBiBadgeModule,
    JdsBiNavModule,
    DeskSortRequestComponent,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './desk-status-request-opendata.component.html',
})
export class DeskStatusRequestOpenDataComponent {
  colorScheme = COLORSCHEME;
  jds: Styles = jds;

  // Variable
  statusRequest = Array.from(STATUS_REQUEST, ([, value]) => ({
    id: value.id,
    name: value.name,
    slug: value.slug,
  }));

  sortStatus: any[] = [
    { value: 'newest', label: 'Terbaru' },
    { value: 'oldest', label: 'Terlama' },
    { value: 'ticket', label: 'No. Tiket' },
  ];

  readonly vm$ = this.deskPageStore.vm$;

  constructor(private deskPageStore: DeskPageStore) {}

  getCount(slug: string, alias: string) {
    if (slug !== 'all') {
      return `countRequestDatasetOpenData${alias}`;
    }

    return 'countRequestDatasetOpenData';
  }

  onStatusRequest(value: TStatusRequest): void {
    this.deskPageStore.filterStatusRequest(value);
  }
}
