import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';

// STYLE
import * as jds from '@styles';
// INTERFACE
import { Styles } from '@interfaces-v4';
// CONSTANT
import { COLORSCHEME, STATUS_REQUEST_SATUDATA } from '@constants-v4';
// JDS-BI
import {
  JdsBiButtonModule,
  JdsBiModalModule,
  JdsBiModalService,
} from '@jds-bi/core';
import { JdsBiIconsModule, JdsBiIconsService, iconXmark } from '@jds-bi/icons';

import { find } from 'lodash';
import moment from 'moment';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

import { DeskPageStore } from '../../containers/desk-page/desk-page.store';

@Component({
  selector: 'app-desk-modal-request-satudata-detail',
  standalone: true,
  imports: [
    CommonModule,
    JdsBiButtonModule,
    JdsBiIconsModule,
    JdsBiModalModule,
    NgxSkeletonLoaderModule,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './desk-modal-request-satudata-detail.component.html',
})
export class DeskModalRequestSatuDataDetailComponent {
  colorScheme = COLORSCHEME;
  jds: Styles = jds;
  moment = moment;

  // Service
  private jdsBiModalService = inject(JdsBiModalService);
  private deskPageStore = inject(DeskPageStore);

  // Variable
  statusRequest = Array.from(STATUS_REQUEST_SATUDATA, ([, value]) => ({
    id: value.id,
    name: value.name,
    color: value.color,
  }));

  // Component Store
  readonly vm$ = this.deskPageStore.vm$;

  constructor(private JdsBiIconService: JdsBiIconsService) {
    this.JdsBiIconService.registerIcons([iconXmark]);
  }

  openModal() {
    this.jdsBiModalService.open('desk-modal-request-satudata-detail');
  }

  closeModal() {
    this.jdsBiModalService.close('desk-modal-request-satudata-detail');
  }

  getStatusName(value: number) {
    return find(this.statusRequest, { id: value }).name;
  }
}
