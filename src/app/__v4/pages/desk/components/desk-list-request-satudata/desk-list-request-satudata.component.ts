import {
  ChangeDetectionStrategy,
  Component,
  ViewChild,
  inject,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { UntypedFormGroup } from '@angular/forms';
import { zip } from 'rxjs';

// STYLE
import * as jds from '@styles';
// INTERFACE
import { Styles } from '@interfaces-v4';
// COMPONENT
import { ModalRequestSatuDataTrackingComponent } from '@components-v4';
// CONSTANT
import { COLORSCHEME, STATUS_REQUEST_SATUDATA } from '@constants-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';
import {
  RequestDatasetSatuDataService,
  HistoryRequestDatasetSatuDataService,
  NotificationService,
} from '@services-v4';
// JDS-BI
import {
  JdsBiBadgeModule,
  JdsBiButtonModule,
  JdsBiPaginationModule,
} from '@jds-bi/core';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconEye,
  iconCircleInfo,
  iconCheck,
  iconCheckDouble,
  iconXmark,
} from '@jds-bi/icons';

import { find } from 'lodash';
import moment from 'moment';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { useMutationResult } from '@ngneat/query';
import { SubscribeDirective } from '@ngneat/subscribe';
import { NgxLoadingModule } from 'ngx-loading';
import Swal from 'sweetalert2';

import { DeskModalRequestSatuDataDetailComponent } from '../desk-modal-request-satudata-detail/desk-modal-request-satudata-detail.component';
import { DeskModalRequestSatuDataDeclineComponent } from '../desk-modal-request-satudata-decline/desk-modal-request-satudata-decline.component';
import { DeskModalRequestSatuDataFinishComponent } from '../desk-modal-request-satudata-finish/desk-modal-request-satudata-finish.component';

import { DeskPageStore } from '../../containers/desk-page/desk-page.store';

@Component({
  selector: 'app-desk-list-request-satudata',
  standalone: true,
  imports: [
    CommonModule,
    JdsBiBadgeModule,
    JdsBiButtonModule,
    JdsBiIconsModule,
    JdsBiPaginationModule,
    NgxSkeletonLoaderModule,
    NgxLoadingModule,
    SubscribeDirective,
    DeskModalRequestSatuDataDetailComponent,
    ModalRequestSatuDataTrackingComponent,
    DeskModalRequestSatuDataDeclineComponent,
    DeskModalRequestSatuDataFinishComponent,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './desk-list-request-satudata.component.html',
})
export class DeskListRequestSatuDataComponent {
  satudataUser: LoginData;
  colorScheme = COLORSCHEME;
  jds: Styles = jds;
  moment: any = moment;

  @ViewChild(DeskModalRequestSatuDataDetailComponent)
  modalDetail: DeskModalRequestSatuDataDetailComponent;

  @ViewChild(ModalRequestSatuDataTrackingComponent)
  modalTracking: ModalRequestSatuDataTrackingComponent;

  @ViewChild(DeskModalRequestSatuDataDeclineComponent)
  modalDecline: DeskModalRequestSatuDataDeclineComponent;

  @ViewChild(DeskModalRequestSatuDataFinishComponent)
  modalFinish: DeskModalRequestSatuDataFinishComponent;

  // Service
  private authenticationService = inject(AuthenticationService);
  private notificationService = inject(NotificationService);
  private requestDatasetService = inject(RequestDatasetSatuDataService);
  private historyRequestDatasetService = inject(
    HistoryRequestDatasetSatuDataService,
  );
  updateRequestDatasetMutation = useMutationResult();

  // Variable
  perPageItems: any[] = [
    { value: 10, label: '10' },
    { value: 25, label: '25' },
  ];
  statusRequest = Array.from(STATUS_REQUEST_SATUDATA, ([, value]) => ({
    id: value.id,
    name: value.name,
    color: value.color,
    alert: value.alert,
  }));
  formGroupDecline: UntypedFormGroup;
  formGroupFinish: UntypedFormGroup;

  // Data
  requestDataset$ = null;

  // Component Store
  readonly vm$ = this.deskPageStore.vm$;

  constructor(
    private iconService: JdsBiIconsService,
    private deskPageStore: DeskPageStore,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    this.deskPageStore.getFormGroupHistoryRequestDatasetSatuDataDecline$.subscribe(
      (value: UntypedFormGroup) => {
        this.formGroupDecline = value;
      },
    );

    this.deskPageStore.getFormGroupHistoryRequestDatasetSatuDataFinish$.subscribe(
      (value: UntypedFormGroup) => {
        this.formGroupFinish = value;
      },
    );

    this.iconService.registerIcons([
      iconEye,
      iconCircleInfo,
      iconCheck,
      iconCheckDouble,
      iconXmark,
    ]);
  }

  getBadgeColor(value: number) {
    return find(this.statusRequest, { id: value }).color;
  }

  getBadgeName(value: number) {
    return find(this.statusRequest, { id: value }).name;
  }

  filterPerPage(value: number) {
    this.deskPageStore.filterPerPage(value);
  }

  filterPage(value: number) {
    this.deskPageStore.filterCurrentPage(value);
  }

  onDetail(value: number) {
    this.modalDetail.openModal();
    this.deskPageStore.getRequestDatasetSatuData(value);
  }

  onTracking(value: number) {
    this.modalTracking.openModal();
    this.deskPageStore.getHistoryRequestDatasetSatuData(value);
  }

  onProcess(value: number) {
    Swal.fire({
      type: 'warning',
      text: 'Apakah Anda yakin?',
      allowOutsideClick: false,
      showCancelButton: true,
      cancelButtonText: 'Batal',
      confirmButtonColor: '#ffa500',
      confirmButtonText: 'Proses',
      reverseButtons: true,
    }).then((result) => {
      if (result.value) {
        // Notification
        const bodyNotification = { is_enable: false, is_read: true };
        const paramsNotification = {
          type: 'request',
          type_id: value,
          receiver: this.satudataUser.id,
        };

        const updateNotification = this.notificationService.updateItemBulk(
          bodyNotification,
          paramsNotification,
        );

        // Request Dataset
        const bodyRequestDataset = {
          id: value,
          status: 2,
          notes: null,
        };

        const updateRequestDataset = this.requestDatasetService.updateItem(
          value,
          bodyRequestDataset,
        );

        // History Request Dataset
        const bodyHistoryRequestDataset = {
          request_private_id: value,
          status: 2,
          notes: null,
        };

        const createHistoryRequestDataset =
          this.historyRequestDatasetService.createItem(
            bodyHistoryRequestDataset,
          );

        zip(updateNotification, createHistoryRequestDataset).subscribe(() => {
          updateRequestDataset
            .pipe(this.updateRequestDatasetMutation.track())
            .subscribe(({ data: requestDataset }) => {
              const { alert } = find(this.statusRequest, {
                id: requestDataset.status,
              });

              Swal.fire({
                type: 'success',
                text: alert,
                allowOutsideClick: false,
              });
            });
        });
      }
    });
  }

  onFinish(value: number) {
    this.modalFinish.openModal();

    this.formGroupFinish.patchValue({
      request_private_id: value,
      status: 3,
    });

    this.deskPageStore.setFormGroupHistoryRequestDatasetSatuDataFinish(
      this.formGroupFinish,
    );
  }

  onDecline(value: number) {
    this.modalDecline.openModal();

    this.formGroupDecline.patchValue({
      request_private_id: value,
      status: 4,
    });

    this.deskPageStore.setFormGroupHistoryRequestDatasetSatuDataDecline(
      this.formGroupDecline,
    );
  }
}
