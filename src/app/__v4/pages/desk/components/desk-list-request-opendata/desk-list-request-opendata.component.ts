import {
  ChangeDetectionStrategy,
  Component,
  ViewChild,
  inject,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { UntypedFormGroup } from '@angular/forms';
import { zip } from 'rxjs';

// STYLE
import * as jds from '@styles';
// INTERFACE
import { Styles } from '@interfaces-v4';
// COMPONENT
import { ModalRequestOpenDataTrackingComponent } from '@components-v4';
// CONSTANT
import { COLORSCHEME, STATUS_REQUEST_OPENDATA } from '@constants-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';
import {
  RequestDatasetOpenDataService,
  HistoryRequestDatasetOpenDataService,
  NotificationService,
} from '@services-v4';
// UTIL
import { hasOwnProperty, isJson } from '@utils-v4';
// JDS-BI
import { jdsBiColor } from '@jds-bi/cdk';
import {
  JdsBiBadgeModule,
  JdsBiButtonModule,
  JdsBiPaginationModule,
} from '@jds-bi/core';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconEye,
  iconCircleInfo,
  iconCheck,
  iconCheckDouble,
  iconXmark,
} from '@jds-bi/icons';

import { find, isNil } from 'lodash';
import moment from 'moment';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { useMutationResult } from '@ngneat/query';
import { SubscribeDirective } from '@ngneat/subscribe';
import { NgxLoadingModule } from 'ngx-loading';
import Swal from 'sweetalert2';

import { DeskModalRequestOpenDataDetailComponent } from '../desk-modal-request-opendata-detail/desk-modal-request-opendata-detail.component';
import { DeskModalRequestOpenDataDeclineComponent } from '../desk-modal-request-opendata-decline/desk-modal-request-opendata-decline.component';
import { DeskModalRequestOpenDataFinishComponent } from '../desk-modal-request-opendata-finish/desk-modal-request-opendata-finish.component';

import { DeskPageStore } from '../../containers/desk-page/desk-page.store';

@Component({
  selector: 'app-desk-list-request-opendata',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    JdsBiBadgeModule,
    JdsBiButtonModule,
    JdsBiIconsModule,
    JdsBiPaginationModule,
    NgxSkeletonLoaderModule,
    NgxLoadingModule,
    SubscribeDirective,
    DeskModalRequestOpenDataDetailComponent,
    ModalRequestOpenDataTrackingComponent,
    DeskModalRequestOpenDataDeclineComponent,
    DeskModalRequestOpenDataFinishComponent,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './desk-list-request-opendata.component.html',
})
export class DeskListRequestOpenDataComponent {
  satudataUser: LoginData;
  colorScheme = COLORSCHEME;
  jds: Styles = jds;
  jdsBiColor = jdsBiColor;
  moment: any = moment;

  @ViewChild(DeskModalRequestOpenDataDetailComponent)
  modalDetail: DeskModalRequestOpenDataDetailComponent;

  @ViewChild(ModalRequestOpenDataTrackingComponent)
  modalTracking: ModalRequestOpenDataTrackingComponent;

  @ViewChild(DeskModalRequestOpenDataDeclineComponent)
  modalDecline: DeskModalRequestOpenDataDeclineComponent;

  @ViewChild(DeskModalRequestOpenDataFinishComponent)
  modalFinish: DeskModalRequestOpenDataFinishComponent;

  // Service
  private authenticationService = inject(AuthenticationService);
  private notificationService = inject(NotificationService);
  private requestDatasetService = inject(RequestDatasetOpenDataService);
  private historyRequestDatasetService = inject(
    HistoryRequestDatasetOpenDataService,
  );
  updateRequestDatasetMutation = useMutationResult();

  // Variable
  perPageItems: any[] = [
    { value: 10, label: '10' },
    { value: 25, label: '25' },
  ];
  statusRequest = Array.from(STATUS_REQUEST_OPENDATA, ([, value]) => ({
    id: value.id,
    name: value.name,
    color: value.color,
    alert: value.alert,
  }));
  formGroupDecline: UntypedFormGroup;
  formGroupFinish: UntypedFormGroup;

  // Data
  requestDataset$ = null;

  // Component Store
  readonly vm$ = this.deskPageStore.vm$;

  constructor(
    private iconService: JdsBiIconsService,
    private deskPageStore: DeskPageStore,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    this.deskPageStore.getFormGroupHistoryRequestDatasetOpenDataDecline$.subscribe(
      (value: UntypedFormGroup) => {
        this.formGroupDecline = value;
      },
    );

    this.deskPageStore.getFormGroupHistoryRequestDatasetOpenDataFinish$.subscribe(
      (value: UntypedFormGroup) => {
        this.formGroupFinish = value;
      },
    );

    this.iconService.registerIcons([
      iconEye,
      iconCircleInfo,
      iconCheck,
      iconCheckDouble,
      iconXmark,
    ]);
  }

  getBadgeColor(value: number) {
    return find(this.statusRequest, { id: value }).color;
  }

  getBadgeName(value: number) {
    return find(this.statusRequest, { id: value }).name;
  }

  getAuthority(data: any) {
    if (!isNil(data) && isJson(data)) {
      const json = JSON.parse(data);
      const isAuthority = hasOwnProperty(json, 'authority');

      if (isAuthority) {
        return true;
      }

      return false;
    }

    return false;
  }

  getAuthorityValue(data: any) {
    const json = JSON.parse(data);
    return json.authority;
  }

  getAuthorityName(data: any) {
    const isAuthority = this.getAuthorityValue(data);

    if (isAuthority) {
      return 'Dalam Wewenang';
    }

    return 'Luar Wewenang';
  }

  filterPerPage(value: number) {
    this.deskPageStore.filterPerPage(value);
  }

  filterPage(value: number) {
    this.deskPageStore.filterCurrentPage(value);
  }

  onDetail(value: number) {
    this.modalDetail.openModal();
    this.deskPageStore.getRequestDatasetOpenData(value);
  }

  onTracking(value: number) {
    this.modalTracking.openModal();
    this.deskPageStore.getHistoryRequestDatasetOpenData(value);
  }

  onProcess(value: number) {
    Swal.fire({
      type: 'warning',
      text: 'Apakah Anda yakin?',
      allowOutsideClick: false,
      showCancelButton: true,
      cancelButtonText: 'Batal',
      confirmButtonColor: '#ffa500',
      confirmButtonText: 'Proses',
      reverseButtons: true,
    }).then((result) => {
      if (result.value) {
        // Notification
        const bodyNotification = { is_enable: false, is_read: true };
        const paramsNotification = {
          type: 'request_public',
          type_id: value,
          receiver: this.satudataUser.id,
        };

        const updateNotification = this.notificationService.updateItemBulk(
          bodyNotification,
          paramsNotification,
        );

        // Request Dataset
        const bodyRequestDataset = {
          id: value,
          status: 2,
          notes: null,
        };

        const updateRequestDataset = this.requestDatasetService.updateItem(
          value,
          bodyRequestDataset,
        );

        // History Request Dataset
        const bodyHistoryRequestDataset = {
          request_public_id: value,
          status: 2,
          notes: null,
        };

        const createHistoryRequestDataset =
          this.historyRequestDatasetService.createItem(
            bodyHistoryRequestDataset,
          );

        zip(updateNotification, createHistoryRequestDataset).subscribe(() => {
          updateRequestDataset
            .pipe(this.updateRequestDatasetMutation.track())
            .subscribe(({ data: requestDataset }) => {
              const { alert } = find(this.statusRequest, {
                id: requestDataset.status,
              });

              Swal.fire({
                type: 'success',
                text: alert,
                allowOutsideClick: false,
              });
            });
        });
      }
    });
  }

  onFinish(value: number) {
    this.modalFinish.openModal();

    this.formGroupFinish.patchValue({
      request_public_id: value,
      status: 4,
    });

    this.deskPageStore.setFormGroupHistoryRequestDatasetOpenDataFinish(
      this.formGroupFinish,
    );
  }

  onDecline(value: number) {
    this.modalDecline.openModal();

    this.formGroupDecline.patchValue({
      request_public_id: value,
      status: 3,
    });

    this.deskPageStore.getAllRequestDatasetReasonDecline();
    this.deskPageStore.setFormGroupHistoryRequestDatasetOpenDataDecline(
      this.formGroupDecline,
    );
  }
}
