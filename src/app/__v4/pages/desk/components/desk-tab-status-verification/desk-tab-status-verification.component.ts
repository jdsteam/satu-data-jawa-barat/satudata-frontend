import { Component, ChangeDetectionStrategy } from '@angular/core';
import { CommonModule } from '@angular/common';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// JDS-BI
import { JdsBiNavModule } from '@jds-bi/core';

import {
  DeskPageStore,
  TStatusVerification,
} from '../../containers/desk-page/desk-page.store';

@Component({
  selector: 'app-desk-tab-status-verification',
  standalone: true,
  imports: [CommonModule, JdsBiNavModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './desk-tab-status-verification.component.html',
})
export class DeskTabStatusVerificationComponent {
  colorScheme = COLORSCHEME;
  jds: Styles = jds;

  readonly vm$ = this.deskPageStore.vm$;

  constructor(private deskPageStore: DeskPageStore) {}

  onStatusVerification(value: TStatusVerification): void {
    this.deskPageStore.filterStatusVerification(value);
  }
}
