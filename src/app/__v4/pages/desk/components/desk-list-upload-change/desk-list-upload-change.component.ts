import {
  Component,
  ChangeDetectionStrategy,
  ViewChild,
  EventEmitter,
  Output,
  inject,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// COMPONENT
import {
  ModalDatasetTrackingComponent,
  ModalMapsetTrackingComponent,
  ModalIndicatorTrackingComponent,
} from '@components-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';
// TYPE
import type { StatusDataset } from '@types-v4';
// JDS-BI
import { jdsBiColor } from '@jds-bi/cdk';
import {
  JdsBiTableHeadModule,
  JdsBiDropdownModule,
  JdsBiHostedDropdownModule,
  JdsBiPaginationModule,
  JdsBiBadgeModule,
  JdsBiButtonModule,
} from '@jds-bi/core';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconPencil,
  iconTrash,
} from '@jds-bi/icons';

import { filter, map } from 'lodash';
import moment from 'moment';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

import { DeskPageStore } from '../../containers/desk-page/desk-page.store';

@Component({
  selector: 'app-desk-list-upload-change',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    JdsBiTableHeadModule,
    JdsBiDropdownModule,
    JdsBiHostedDropdownModule,
    JdsBiPaginationModule,
    JdsBiBadgeModule,
    JdsBiButtonModule,
    JdsBiIconsModule,
    NgxSkeletonLoaderModule,
    ModalDatasetTrackingComponent,
    ModalMapsetTrackingComponent,
    ModalIndicatorTrackingComponent,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './desk-list-upload-change.component.html',
})
export class DeskListUploadChangeComponent {
  satudataUser: LoginData;
  colorScheme = COLORSCHEME;
  jds: Styles = jds;
  jdsBiColor = jdsBiColor;
  moment: any = moment;

  @ViewChild(ModalDatasetTrackingComponent)
  modalDatasetTracking: ModalDatasetTrackingComponent;
  @ViewChild(ModalMapsetTrackingComponent)
  modalMapsetTracking: ModalMapsetTrackingComponent;
  @ViewChild(ModalIndicatorTrackingComponent)
  modalIndicatorTracking: ModalIndicatorTrackingComponent;

  @Output() delete = new EventEmitter<number | object>();

  // Service
  private authenticationService = inject(AuthenticationService);
  private deskPageStore = inject(DeskPageStore);
  private iconService = inject(JdsBiIconsService);

  // Variable
  typeOptions = [
    { value: 'dataset', label: 'Dataset', checked: false },
    { value: 'mapset', label: 'Mapset', checked: false },
  ];
  perPageItems = [
    { value: 10, label: '10' },
    { value: 25, label: '25' },
  ];
  isOpenTypeData = false;
  direction = 'asc';

  readonly vm$ = this.deskPageStore.vm$;

  constructor() {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    this.iconService.registerIcons([iconPencil, iconTrash]);
  }

  getSkeleton(verification: string) {
    let draft: any[];
    let revision: any[];
    let approve: any[];
    if (this.satudataUser.role_name === 'walidata') {
      if (verification === 'new') {
        draft = new Array(6);
      } else if (verification === 'edit') {
        draft = new Array(7);
      }
      revision = new Array(7);
      approve = new Array(7);
    } else if (this.satudataUser.role_name === 'opd') {
      if (verification === 'new') {
        draft = new Array(5);
      } else if (verification === 'edit') {
        draft = new Array(6);
      }
      revision = new Array(6);
      approve = new Array(6);
    }

    return {
      draft,
      revision,
      approve,
    };
  }

  getEditQueryParams(catalog: string, category: string) {
    let params;
    if (catalog === 'dataset') {
      let value;
      switch (category) {
        case 'waiting':
          value = 'verification';
          break;
        case 'edit':
          value = 'verification-edit';
          break;
        default:
          value = category;
          break;
      }

      params = {
        from: value,
      };
    } else {
      params = {
        page: 'desk',
        status: category,
      };
    }

    return params;
  }

  onPerPage(value: number) {
    this.deskPageStore.filterPerPage(value);
  }

  onPage(value: number) {
    this.deskPageStore.filterCurrentPage(value);
  }

  onFilter(code: string, isChecked: boolean) {
    const items = this.typeOptions;

    items.forEach((item) => {
      if (item.value === code) {
        item.checked = isChecked;
      }

      return item;
    });

    const value = map(filter(items, 'checked'), 'value');
    this.deskPageStore.filterCatalog(value);
  }

  onSort(value: any, direction: string) {
    let dir = '';
    if (direction === 'desc') {
      dir = 'asc';
    } else if (direction === 'asc') {
      dir = 'desc';
    }

    this.deskPageStore.filterSortTable(`${value}:${dir}`);
  }

  onTracking(catalog: string, id: number) {
    if (catalog === 'dataset') {
      this.modalDatasetTracking.openModal();
      this.deskPageStore.getHistoryDataset(id);
    } else if (catalog === 'mapset') {
      this.modalMapsetTracking.openModal();
      this.deskPageStore.getHistoryMapset(id);
    } else if (catalog === 'indicator') {
      this.modalIndicatorTracking.openModal();
      this.deskPageStore.getHistoryIndicator(id);
    }
  }

  onDelete(catalog: string, id: number, category: StatusDataset) {
    this.delete.emit({ id, category, catalog });
  }
}
