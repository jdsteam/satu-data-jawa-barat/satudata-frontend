import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  FormsModule,
  ReactiveFormsModule,
  UntypedFormArray,
  UntypedFormControl,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { zip } from 'rxjs';

// STYLE
import * as jds from '@styles';
// INTERFACE
import { Styles } from '@interfaces-v4';
// CONSTANT
import { COLORSCHEME, STATUS_REQUEST_OPENDATA } from '@constants-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';
import {
  RequestDatasetOpenDataService,
  HistoryRequestDatasetOpenDataService,
  NotificationService,
} from '@services-v4';
// UTIL
import { arrayRange, businessDays } from '@utils-v4';
// JDS-BI
import {
  JdsBiButtonModule,
  JdsBiModalModule,
  JdsBiModalService,
  JdsBiInputModule,
} from '@jds-bi/core';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconXmark,
  iconCirclePlus,
  iconTrash,
} from '@jds-bi/icons';

import { assign, find, map } from 'lodash';
import moment from 'moment';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { LetDirective } from '@ngrx/component';
import { useMutationResult } from '@ngneat/query';
import { SubscribeDirective } from '@ngneat/subscribe';
import { NgxLoadingModule } from 'ngx-loading';
import Swal from 'sweetalert2';
import { NgSelectModule } from '@ng-select/ng-select';

import { DeskPageStore } from '../../containers/desk-page/desk-page.store';

@Component({
  selector: 'app-desk-modal-request-opendata-finish',
  standalone: true,
  imports: [
    CommonModule,
    JdsBiButtonModule,
    JdsBiIconsModule,
    JdsBiModalModule,
    JdsBiInputModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSkeletonLoaderModule,
    LetDirective,
    NgxLoadingModule,
    SubscribeDirective,
    NgSelectModule,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './desk-modal-request-opendata-finish.component.html',
})
export class DeskModalRequestOpenDataFinishComponent {
  satudataUser: LoginData;
  colorScheme = COLORSCHEME;
  jds: Styles = jds;
  moment = moment;

  // Service
  private jdsBiModalService = inject(JdsBiModalService);
  private deskPageStore = inject(DeskPageStore);

  private authenticationService = inject(AuthenticationService);
  private notificationService = inject(NotificationService);
  private requestDatasetService = inject(RequestDatasetOpenDataService);
  private historyRequestDatasetService = inject(
    HistoryRequestDatasetOpenDataService,
  );
  updateRequestDatasetMutation = useMutationResult();

  // Variable
  formGroup: UntypedFormGroup;
  statusRequest = Array.from(STATUS_REQUEST_OPENDATA, ([, value]) => ({
    id: value.id,
    name: value.name,
    color: value.color,
    alert: value.alert,
  }));
  currentYear = new Date().getFullYear();
  yearOptions = arrayRange(this.currentYear, this.currentYear - 10, -1);

  // Component Store
  readonly vm$ = this.deskPageStore.vm$;

  constructor(private JdsBiIconService: JdsBiIconsService) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    this.deskPageStore.getFormGroupHistoryRequestDatasetOpenDataFinish$.subscribe(
      (value: UntypedFormGroup) => {
        this.formGroup = value;
      },
    );

    this.JdsBiIconService.registerIcons([iconXmark, iconCirclePlus, iconTrash]);
  }

  get f() {
    return this.formGroup.controls;
  }

  get fl() {
    return this.f.link as UntypedFormArray;
  }

  openModal() {
    this.jdsBiModalService.open('desk-modal-request-opendata-finish');
  }

  closeModal() {
    this.jdsBiModalService.close('desk-modal-request-opendata-finish');
  }

  onSelectedSource(value: string) {
    switch (value) {
      case 'open_data_jabar':
        this.addLink();
        this.f.title.clearValidators();
        this.f.title.reset();
        this.f.link_drive.clearValidators();
        this.f.link_drive.reset();
        this.f.topic.clearValidators();
        this.f.topic.reset();
        this.f.year.clearValidators();
        this.f.year.reset();
        this.f.authority.clearValidators();
        this.f.authority.reset();
        this.f.organization.clearValidators();
        this.f.organization.reset();
        this.f.organization_manual.clearValidators();
        this.f.organization_manual.reset();
        this.f.organization_phone.clearValidators();
        this.f.organization_phone.reset();
        break;
      case 'jabar_drive':
        this.fl.clear();
        this.f.title.setValidators([
          Validators.required,
          Validators.maxLength(255),
        ]);
        this.f.title.updateValueAndValidity();
        this.f.link_drive.setValidators([Validators.required]);
        this.f.link_drive.updateValueAndValidity();
        this.deskPageStore.getAllTopicSatuData();
        this.f.topic.setValidators([Validators.required]);
        this.f.topic.updateValueAndValidity();
        this.f.year.setValidators([Validators.required]);
        this.f.year.updateValueAndValidity();
        this.f.authority.setValidators([Validators.required]);
        this.f.authority.updateValueAndValidity();
        break;
      case 'sumber_lain':
        this.fl.clear();
        this.f.title.clearValidators();
        this.f.title.reset();
        this.f.link_drive.clearValidators();
        this.f.link_drive.reset();
        this.f.topic.clearValidators();
        this.f.topic.reset();
        this.f.year.clearValidators();
        this.f.year.reset();
        this.f.authority.clearValidators();
        this.f.authority.reset();
        this.f.organization.clearValidators();
        this.f.organization.reset();
        this.f.organization_manual.clearValidators();
        this.f.organization_manual.reset();
        this.f.organization_phone.clearValidators();
        this.f.organization_phone.reset();
        break;
      default:
        break;
    }
  }

  onSelectedAuthority(value: string) {
    const auth = value === 'true';

    if (auth) {
      this.deskPageStore.getAllOrganizationSatuData();
      this.f.organization.setValidators([Validators.required]);
      this.f.organization.updateValueAndValidity();

      this.f.organization_manual.clearValidators();
      this.f.organization_manual.reset();
      this.f.organization_phone.clearValidators();
      this.f.organization_phone.reset();
    } else {
      this.f.organization.clearValidators();
      this.f.organization.reset();

      this.f.organization_manual.setValidators([
        Validators.required,
        Validators.maxLength(255),
      ]);
      this.f.organization_manual.updateValueAndValidity();
      this.f.organization_phone.setValidators([
        Validators.required,
        Validators.maxLength(255),
      ]);
      this.f.organization_phone.updateValueAndValidity();
    }
  }

  addLink() {
    this.fl.push(this.createLink());
    return false;
  }

  createLink(): UntypedFormGroup {
    return new UntypedFormGroup({
      url: new UntypedFormControl(null, [Validators.required]),
    });
  }

  removeLink(i) {
    if (this.fl) {
      this.fl.removeAt(i);
    }
    return false;
  }

  getUpdateDate() {
    const date = businessDays(new Date(), 14);

    return moment(date).locale('id').format('DD MMMM YYYY');
  }

  onSubmit() {
    this.closeModal();

    const {
      request_public_id: id,
      status,
      source,
      link,
      title,
      link_drive: linkDrive,
      topic,
      year,
      authority,
      organization,
      organization_manual: organizationManual,
      organization_phone: organizationPhone,
      notes,
    } = this.formGroup.value;

    const isAuthority = authority === 'true';

    // Notification
    const bodyNotification = { is_enable: false, is_read: true };
    const paramsNotification = {
      type: 'request_public',
      type_id: id,
      receiver: this.satudataUser.id,
    };

    const updateNotification = this.notificationService.updateItemBulk(
      bodyNotification,
      paramsNotification,
    );

    // Request Dataset
    const bodyRequestDataset = {
      id,
      status,
    };

    if (source === 'jabar_drive') {
      const lastUpdate = {
        notes,
        authority: isAuthority,
      };

      if (isAuthority) {
        assign(lastUpdate, {
          authority_date: businessDays(new Date(), 14),
        });
      }

      assign(bodyRequestDataset, {
        notes: JSON.stringify(lastUpdate),
      });
    } else {
      assign(bodyRequestDataset, {
        notes,
      });
    }

    const updateRequestDataset = this.requestDatasetService.updateItem(
      id,
      bodyRequestDataset,
    );

    // History Request Dataset
    const bodyHistoryRequestDataset = {
      request_public_id: id,
      status,
      notes,
    };

    switch (source) {
      case 'open_data_jabar':
        assign(bodyHistoryRequestDataset, {
          dataset_source: 'Open Data Jabar',
          odj_link: JSON.stringify(map(link, 'url')),
          odj_notes: notes,
        });
        break;
      case 'jabar_drive':
        assign(bodyHistoryRequestDataset, {
          dataset_source: 'Jabar Drive',
          jd_judul: title,
          jd_link: linkDrive,
          jd_topik: topic.name,
          jd_tahun: year,
          jd_wewenang_prov: isAuthority,
          jd_notes: notes,
        });

        if (isAuthority) {
          assign(bodyHistoryRequestDataset, {
            jd_organisasi: organization.nama_skpd,
            jd_organisasi_kontak: organization.phone,
          });
        } else {
          assign(bodyHistoryRequestDataset, {
            jd_organisasi: organizationManual,
            jd_organisasi_kontak: organizationPhone,
          });
        }
        break;
      case 'sumber_lain':
        assign(bodyHistoryRequestDataset, {
          dataset_source: 'Sumber Lain',
          sl_notes: notes,
        });
        break;
      default:
        break;
    }

    const createHistoryRequestDataset =
      this.historyRequestDatasetService.createItem(bodyHistoryRequestDataset);

    zip(updateNotification, createHistoryRequestDataset).subscribe(() => {
      updateRequestDataset
        .pipe(this.updateRequestDatasetMutation.track())
        .subscribe(({ data: requestDataset }) => {
          const { alert } = find(this.statusRequest, {
            id: requestDataset.status,
          });

          Swal.fire({
            type: 'success',
            text: alert,
            allowOutsideClick: false,
          }).then(() => {
            this.formGroup.reset();

            this.deskPageStore.setFormGroupHistoryRequestDatasetOpenDataFinish(
              this.formGroup,
            );
          });
        });
    });
  }
}
