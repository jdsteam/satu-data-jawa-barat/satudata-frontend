import {
  Component,
  ChangeDetectionStrategy,
  OnInit,
  inject,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';
// JDS-BI
import { jdsBiColor } from '@jds-bi/cdk';
import { JdsBiInputModule, JdsBiButtonModule } from '@jds-bi/core';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconMagnifyingGlass,
  iconCircleXmark,
} from '@jds-bi/icons';

import { DeskPageStore } from '../../containers/desk-page/desk-page.store';

@Component({
  selector: 'app-desk-header',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    JdsBiInputModule,
    JdsBiButtonModule,
    JdsBiIconsModule,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './desk-header.component.html',
})
export class DeskHeaderComponent implements OnInit {
  satudataUser: LoginData;
  colorScheme = COLORSCHEME;
  jds: Styles = jds;
  jdsBiColor = jdsBiColor;

  // Settings
  label: string;

  // Service
  private authenticationService = inject(AuthenticationService);

  // Component Store
  readonly vm$ = this.deskPageStore.vm$;

  constructor(
    private iconService: JdsBiIconsService,
    private deskPageStore: DeskPageStore,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    this.iconService.registerIcons([iconMagnifyingGlass, iconCircleXmark]);
  }

  ngOnInit(): void {
    if (this.satudataUser.role_name === 'walidata') {
      this.label = 'Desk Walidata';
    } else if (this.satudataUser.role_name === 'opd') {
      this.label = 'Desk OPD';
    }
  }

  getPlaceholder(value: string) {
    let placeholder;
    switch (value) {
      case 'upload-change':
        placeholder = 'Cari dataset, mapset, atau organisasi';
        break;
      case 'discontinue':
        placeholder = 'Cari dataset atau organisasi';
        break;
      case 'request-dataset':
        placeholder = 'Cari dataset';
        break;
      default:
        break;
    }

    return placeholder;
  }

  onSearch(value: string): void {
    this.deskPageStore.filterSearch(value);
  }
}
