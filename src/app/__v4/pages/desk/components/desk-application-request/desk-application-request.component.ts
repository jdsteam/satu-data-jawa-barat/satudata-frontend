import { Component, ChangeDetectionStrategy } from '@angular/core';
import { CommonModule } from '@angular/common';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// JDS-BI
import { JdsBiButtonModule } from '@jds-bi/core';

import {
  DeskPageStore,
  TApplication,
} from '../../containers/desk-page/desk-page.store';

@Component({
  selector: 'app-desk-application-request',
  standalone: true,
  imports: [CommonModule, JdsBiButtonModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './desk-application-request.component.html',
})
export class DeskApplicationRequestComponent {
  colorScheme = COLORSCHEME;
  jds: Styles = jds;

  readonly vm$ = this.deskPageStore.vm$;

  constructor(private deskPageStore: DeskPageStore) {}

  onApplication(value: TApplication): void {
    this.deskPageStore.filterApplication(value);
  }
}
