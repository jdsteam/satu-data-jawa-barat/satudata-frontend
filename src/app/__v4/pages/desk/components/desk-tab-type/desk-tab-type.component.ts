import { Component, ChangeDetectionStrategy } from '@angular/core';
import { CommonModule } from '@angular/common';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';
// COMPONENT
import { JdsBiNavModule } from '@jds-bi/core';

import {
  DeskPageStore,
  TType,
} from '../../containers/desk-page/desk-page.store';

@Component({
  selector: 'app-desk-tab-type',
  standalone: true,
  imports: [CommonModule, JdsBiNavModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './desk-tab-type.component.html',
})
export class DeskTabTypeComponent {
  colorScheme = COLORSCHEME;
  satudataUser: LoginData;
  jds: Styles = jds;

  readonly vm$ = this.deskPageStore.vm$;

  constructor(
    private authenticationService: AuthenticationService,
    private deskPageStore: DeskPageStore,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
  }

  onType(value: TType): void {
    this.deskPageStore.filterType(value);
  }
}
