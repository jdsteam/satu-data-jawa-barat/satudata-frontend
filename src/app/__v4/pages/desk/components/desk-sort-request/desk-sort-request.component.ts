import { ChangeDetectionStrategy, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// STYLE
import * as jds from '@styles';
// INTERFACE
import { Styles } from '@interfaces-v4';

import { NgSelectModule } from '@ng-select/ng-select';

import { DeskPageStore } from '../../containers/desk-page/desk-page.store';

@Component({
  selector: 'app-desk-sort-request',
  standalone: true,
  imports: [CommonModule, FormsModule, NgSelectModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './desk-sort-request.component.html',
  styleUrls: ['./desk-sort-request.component.scss'],
})
export class DeskSortRequestComponent {
  jds: Styles = jds;

  // Variable
  sortOption: any[] = [
    { value: 'newest', label: 'Terbaru' },
    { value: 'oldest', label: 'Terlama' },
    { value: 'ticket', label: 'No. Tiket' },
  ];

  readonly vm$ = this.deskPageStore.vm$;

  constructor(private deskPageStore: DeskPageStore) {}

  onSort(value: string): void {
    this.deskPageStore.filterSort(value);
  }
}
