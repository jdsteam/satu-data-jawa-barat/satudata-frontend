import {
  Component,
  ChangeDetectionStrategy,
  ViewChild,
  inject,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// COMPONENT
import {
  ModalDatasetTrackingComponent,
  ModalMapsetTrackingComponent,
} from '@components-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';
// JDS-BI
import { jdsBiColor } from '@jds-bi/cdk';
import {
  JdsBiTableHeadModule,
  JdsBiDropdownModule,
  JdsBiHostedDropdownModule,
  JdsBiPaginationModule,
  JdsBiBadgeModule,
  JdsBiButtonModule,
} from '@jds-bi/core';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconPencil,
  iconTrash,
} from '@jds-bi/icons';

import moment from 'moment';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

import { DeskPageStore } from '../../containers/desk-page/desk-page.store';

@Component({
  selector: 'app-desk-list-discontinue',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    JdsBiTableHeadModule,
    JdsBiDropdownModule,
    JdsBiHostedDropdownModule,
    JdsBiPaginationModule,
    JdsBiBadgeModule,
    JdsBiButtonModule,
    JdsBiIconsModule,
    NgxSkeletonLoaderModule,
    ModalDatasetTrackingComponent,
    ModalMapsetTrackingComponent,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './desk-list-discontinue.component.html',
})
export class DeskListDiscontinueComponent {
  satudataUser: LoginData;
  colorScheme = COLORSCHEME;
  jds: Styles = jds;
  jdsBiColor = jdsBiColor;
  moment: any = moment;

  @ViewChild(ModalDatasetTrackingComponent)
  modalDatasetTracking: ModalDatasetTrackingComponent;

  // Service
  private authenticationService = inject(AuthenticationService);
  private deskPageStore = inject(DeskPageStore);
  private iconService = inject(JdsBiIconsService);

  // Variable
  perPageItems = [
    { value: 10, label: '10' },
    { value: 25, label: '25' },
  ];
  isOpenTypeData = false;
  direction = 'asc';

  readonly vm$ = this.deskPageStore.vm$;

  constructor() {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    this.iconService.registerIcons([iconPencil, iconTrash]);
  }

  getSkeleton() {
    let draft: any[];
    let approve: any[];
    if (this.satudataUser.role_name === 'walidata') {
      draft = new Array(7);
      approve = new Array(8);
    } else if (this.satudataUser.role_name === 'opd') {
      draft = new Array(6);
      approve = new Array(7);
    }

    return {
      draft,
      approve,
    };
  }

  getEditQueryParams(catalog: string, category: string) {
    let params;
    if (catalog === 'dataset') {
      let value;
      switch (category) {
        case 'waiting':
          value = 'verification';
          break;
        case 'edit':
          value = 'verification-edit';
          break;
        default:
          value = category;
          break;
      }

      params = {
        from: value,
      };
    } else if (catalog === 'mapset') {
      params = {
        page: 'desk',
        status: category,
      };
    }

    return params;
  }

  onPerPage(value: number) {
    this.deskPageStore.filterPerPage(value);
  }

  onPage(value: number) {
    this.deskPageStore.filterCurrentPage(value);
  }

  onSort(value: any, direction: string) {
    let dir = '';
    if (direction === 'desc') {
      dir = 'asc';
    } else if (direction === 'asc') {
      dir = 'desc';
    }

    this.deskPageStore.filterSortTable(`${value}:${dir}`);
  }

  onTracking(catalog: string, id: number) {
    if (catalog === 'dataset') {
      this.modalDatasetTracking.openModal();
      this.deskPageStore.getHistoryDataset(id);
    }
  }
}
