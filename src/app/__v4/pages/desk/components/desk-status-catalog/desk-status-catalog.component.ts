import { Component, ChangeDetectionStrategy } from '@angular/core';
import { CommonModule } from '@angular/common';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';
// JDS-BI
import { JdsBiButtonModule } from '@jds-bi/core';

import {
  DeskPageStore,
  TStatus,
} from '../../containers/desk-page/desk-page.store';

@Component({
  selector: 'app-desk-status-catalog',
  standalone: true,
  imports: [CommonModule, JdsBiButtonModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './desk-status-catalog.component.html',
})
export class DeskStatusCatalogComponent {
  satudataUser: LoginData;
  colorScheme = COLORSCHEME;
  jds: Styles = jds;

  readonly vm$ = this.deskPageStore.vm$;

  constructor(
    private deskPageStore: DeskPageStore,
    private authenticationService: AuthenticationService,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
  }

  onStatus(value: TStatus): void {
    this.deskPageStore.filterStatus(value);
  }
}
