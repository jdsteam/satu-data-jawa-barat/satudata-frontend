import { inject } from '@angular/core';
import { CanActivateFn, Routes } from '@angular/router';

import { DeskPageService } from './containers/desk-page/desk-page.service';

const canActivateDeskPage: CanActivateFn = () => {
  return inject(DeskPageService).canActivate();
};

export const DESK_ROUTES: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        canActivate: [canActivateDeskPage],
        loadComponent: () =>
          import('./containers/desk-page/desk-page.component').then(
            (m) => m.DeskPageComponent,
          ),
      },
    ],
  },
];
