import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ComponentStore } from '@ngrx/component-store';
import { assign, isEmpty } from 'lodash';
import { Observable } from 'rxjs';
import { tap, withLatestFrom, switchMap, map } from 'rxjs/operators';

// CONSTANT
import { NOTIFICATION } from '@constants-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';
import {
  NotificationService,
  HistoryRequestDatasetSatuDataService,
} from '@services-v4';

const Notification = Array.from(NOTIFICATION, ([, value]) => value.id);
export type TType = (typeof Notification)[number];

export interface NotificationPageState {
  filterType: TType;
  filterCatalog: string[];
  filterSort: string;
  filterDirection: string;
  filterPerPage: number;
  filterCurrentPage: number;
  listNotification: any;
  detailNotification: any;
  historyRequestDatasetSatuData: any;
}

export const DEFAULT_STATE: NotificationPageState = {
  filterType: 'upload-change',
  filterCatalog: [],
  filterSort: 'cdate',
  filterDirection: 'desc',
  filterPerPage: 10,
  filterCurrentPage: 1,
  listNotification: null,
  detailNotification: null,
  historyRequestDatasetSatuData: null,
};

@Injectable()
export class NotificationPageStore extends ComponentStore<NotificationPageState> {
  satudataUser: LoginData;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private notificationService: NotificationService,
    private historyRequestDatasetSatuDataService: HistoryRequestDatasetSatuDataService,
  ) {
    super(DEFAULT_STATE);

    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
  }

  // *********** Updaters *********** //
  readonly setFilterType = this.updater((state, value: TType) => ({
    ...state,
    filterType: value,
  }));

  readonly setFilterCatalog = this.updater((state, value: string[]) => ({
    ...state,
    filterCatalog: value,
  }));

  readonly setFilterSort = this.updater((state, value: string) => ({
    ...state,
    filterSort: value,
  }));

  readonly setFilterDirection = this.updater((state, value: string) => ({
    ...state,
    filterDirection: value,
  }));

  readonly setFilterPerPage = this.updater((state, value: number) => ({
    ...state,
    filterPerPage: Number(value),
  }));

  readonly setFilterCurrentPage = this.updater((state, value: number) => ({
    ...state,
    filterCurrentPage: Number(value),
  }));

  readonly setListNotification = this.updater((state, value: any) => ({
    ...state,
    listNotification: value,
  }));

  readonly setDetailNotification = this.updater((state, value: any) => ({
    ...state,
    detailNotification: value,
  }));

  readonly setHistoryRequestDatasetSatuData = this.updater(
    (state, value: any) => ({
      ...state,
      historyRequestDatasetSatuData: value,
    }),
  );

  // *********** Selectors *********** //
  readonly getFilterType$ = this.select(({ filterType }) => filterType);
  readonly getFilterCatalog$ = this.select(
    ({ filterCatalog }) => filterCatalog,
  );
  readonly getFilterSort$ = this.select(({ filterSort }) => filterSort);
  readonly getFilterDirection$ = this.select(
    ({ filterDirection }) => filterDirection,
  );
  readonly getFilterPerPage$ = this.select(
    ({ filterPerPage }) => filterPerPage,
  );
  readonly getFilterCurrentPage$ = this.select(
    ({ filterCurrentPage }) => filterCurrentPage,
  );
  readonly getListNotification$ = this.select(
    ({ listNotification }) => listNotification,
  );
  readonly getDetailNotification$ = this.select(
    ({ detailNotification }) => detailNotification,
  );
  readonly getHistoryRequestDatasetSatuData$ = this.select(
    ({ historyRequestDatasetSatuData }) => historyRequestDatasetSatuData,
  );

  // ViewModel of Paginator component
  readonly vm$ = this.select(
    this.state$,
    this.getFilterType$,
    this.getFilterCatalog$,
    this.getFilterSort$,
    this.getFilterDirection$,
    this.getFilterPerPage$,
    this.getFilterCurrentPage$,
    this.getListNotification$,
    this.getDetailNotification$,
    this.getHistoryRequestDatasetSatuData$,
    (
      state,
      getFilterType,
      getFilterCatalog,
      getFilterSort,
      getFilterDirection,
      getFilterPerPage,
      getFilterCurrentPage,
      getListNotification,
      getDetailNotification,
      getHistoryRequestDatasetSatuData,
    ) => ({
      filterType: state.filterType,
      filterCatalog: state.filterCatalog,
      filterSort: state.filterSort,
      filterDirection: state.filterDirection,
      filterPerPage: state.filterPerPage,
      filterCurrentPage: state.filterCurrentPage,
      listNotification: state.listNotification,
      detailNotification: state.detailNotification,
      historyRequestDatasetSatuData: state.historyRequestDatasetSatuData,
      getFilterType,
      getFilterCatalog,
      getFilterSort,
      getFilterDirection,
      getFilterPerPage,
      getFilterCurrentPage,
      getListNotification,
      getDetailNotification,
      getHistoryRequestDatasetSatuData,
    }),
  );

  // *********** Actions *********** //
  readonly filterType = this.effect((type$: Observable<TType>) => {
    return type$.pipe(
      withLatestFrom(this.getFilterType$),
      tap<[TType, TType]>(([type]) => {
        this.router.navigate([], {
          relativeTo: this.route,
          queryParams: {
            type,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        this.setFilterType(type);
        this.setFilterSort(DEFAULT_STATE.filterSort);
        this.setFilterCurrentPage(DEFAULT_STATE.filterCurrentPage);
      }),
      map(() => {
        this.getAllNotification();
      }),
    );
  });

  readonly filterCatalog = this.effect((catalog$: Observable<string[]>) => {
    return catalog$.pipe(
      withLatestFrom(this.getFilterCatalog$),
      tap<[string[], string[]]>(([catalog]) => {
        this.setFilterCatalog(catalog);
        this.setFilterCurrentPage(DEFAULT_STATE.filterCurrentPage);
      }),
      map(() => {
        this.getAllNotification();
      }),
    );
  });

  readonly filterSortTable = this.effect((sort$: Observable<string>) => {
    return sort$.pipe(
      tap((sort) => {
        const obj = sort.split(':');

        this.setFilterSort(obj[0]);
        this.setFilterDirection(obj[1]);
        this.setFilterCurrentPage(DEFAULT_STATE.filterCurrentPage);
      }),
      map(() => {
        this.getAllNotification();
      }),
    );
  });

  readonly filterPerPage = this.effect((perPage$: Observable<number>) => {
    return perPage$.pipe(
      withLatestFrom(this.getFilterPerPage$),
      tap<[number, number]>(([perPage]) => {
        this.router.navigate([], {
          relativeTo: this.route,
          queryParams: {
            perPage,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        this.setFilterPerPage(perPage);
        this.setFilterCurrentPage(DEFAULT_STATE.filterCurrentPage);
      }),
      map(() => {
        this.getAllNotification();
      }),
    );
  });

  readonly filterCurrentPage = this.effect(
    (currentPage$: Observable<number>) => {
      return currentPage$.pipe(
        withLatestFrom(this.getFilterCurrentPage$),
        tap<[number, number]>(([currentPage]) => {
          this.router.navigate([], {
            relativeTo: this.route,
            queryParams: {
              page: currentPage,
            },
            queryParamsHandling: 'merge',
          });

          this.setFilterCurrentPage(currentPage);
        }),
        map(() => {
          this.getAllNotification();
        }),
      );
    },
  );

  // Notification
  readonly getAllNotification = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      withLatestFrom(this.vm$),
      switchMap(([, vm]) => {
        const {
          filterType: type,
          filterCatalog: catalog,
          filterSort: sort,
          filterDirection: direction,
          filterPerPage: perPage,
          filterCurrentPage: currentPage,
        } = vm;

        const pWhere = {
          receiver: this.satudataUser.id,
        };

        if (!isEmpty(type)) {
          assign(pWhere, { feature: NOTIFICATION.get(type).paramWhere });
        }

        if (!isEmpty(catalog)) {
          const arr = catalog.map((item) => `${item}`);
          assign(pWhere, { type: arr });
        }

        const params = {
          sort: `${sort}:${direction}`,
          skip: perPage * currentPage - perPage,
          limit: perPage,
          where: JSON.stringify(pWhere),
        };
        const options = {
          pagination: true,
        };

        return this.notificationService
          .getQueryList(params, options, type)
          .result$.pipe(
            tap({
              next: (result) => {
                this.setListNotification(result);
              },
            }),
          );
      }),
    );
  });

  readonly getLastNotification = this.effect((id$: Observable<number>) => {
    return id$.pipe(
      switchMap((id) => {
        const pWhere = {
          receiver: this.satudataUser.id,
          type: 'request_private',
          type_id: id,
        };

        const params = {
          sort: 'cdate:desc',
          skip: 0,
          limit: 1,
          where: JSON.stringify(pWhere),
        };

        const options = {
          pagination: false,
        };

        return this.notificationService
          .getQueryLast(id, params, options)
          .result$.pipe(
            tap({
              next: (result) => {
                this.setDetailNotification(result);
              },
            }),
          );
      }),
    );
  });

  readonly getHistoryRequestDatasetSatuData = this.effect(
    (id$: Observable<number>) => {
      return id$.pipe(
        switchMap((id) => {
          const pWhere = {
            request_private_id: id,
          };

          const params = {
            sort: 'datetime:desc',
            where: JSON.stringify(pWhere),
          };

          const options = {
            pagination: false,
          };

          return this.historyRequestDatasetSatuDataService
            .getQueryList(id, params, options)
            .result$.pipe(
              tap({
                next: (result) => {
                  this.setHistoryRequestDatasetSatuData(result);
                },
              }),
            );
        }),
      );
    },
  );
}
