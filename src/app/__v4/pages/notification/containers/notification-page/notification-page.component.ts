import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
  inject,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Title } from '@angular/platform-browser';
import { RouterModule, Router, ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Styles, Breadcrumb } from '@interfaces-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { StateService } from '@services-v3';
import { GlobalService } from '@services-v4';

import { isNil, set } from 'lodash';
import moment from 'moment';
import { fromWorker } from 'observable-webworker';

import {
  NotificationTabTypeComponent,
  NotificationListComponent,
} from '../../components';

import {
  DEFAULT_STATE,
  NotificationPageStore,
} from './notification-page.store';

@Component({
  selector: 'app-notification-page',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    NotificationTabTypeComponent,
    NotificationListComponent,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './notification-page.component.html',
  providers: [NotificationPageStore],
})
export class NotificationPageComponent implements OnInit, OnDestroy {
  satudataUser: LoginData;
  satudataToken: string;
  colorScheme = COLORSCHEME;
  jds: Styles = jds;
  moment = moment;

  // Log
  log = null;
  pageInfo = null;
  deviceInfo = null;
  userInfo = null;

  // Setting
  title: string;
  label = 'Notifikasi';
  breadcrumb: Breadcrumb[] = [{ label: 'Notifikasi', link: '/notification' }];

  // Service
  private router = inject(Router);
  private route = inject(ActivatedRoute);
  private globalService = inject(GlobalService);
  private stateService = inject(StateService);
  private titleService = inject(Title);
  private notificationPageStore = inject(NotificationPageStore);

  // Component Store
  readonly vm$ = this.notificationPageStore.vm$;

  // Variable
  perPageItems: any[] = [
    { value: 5, label: '5' },
    { value: 10, label: '10' },
  ];

  constructor() {
    this.stateService.currentLogUser.subscribe((x) => (this.userInfo = x));
  }

  ngOnInit(): void {
    this.logPage('init');
    this.settingsAll();
    this.queryParams();
    this.getAllNotification();
  }

  ngOnDestroy(): void {
    this.logPage('destroy');
  }

  settingsAll(): void {
    this.title = this.globalService.title;
    this.titleService.setTitle(`${this.label} - ${this.title}`);
    this.globalService.changeLabel(this.label);
    this.globalService.changeBreadcrumb(this.breadcrumb);
  }

  queryParams(): void {
    this.route.queryParams.subscribe((p) => {
      const {
        filterType: type,
        filterSort: sort,
        filterPerPage: perPage,
        filterCurrentPage: currentPage,
      } = DEFAULT_STATE;

      const pType = !isNil(p['type']) ? p['type'] : type;
      const pSort = !isNil(p['sort']) ? p['sort'] : sort;
      const pPerPage = !isNil(p['perPage']) ? p['perPage'] : perPage;
      const pCurrentPage = !isNil(p['page']) ? p['page'] : currentPage;

      this.notificationPageStore.setFilterType(pType);
      this.notificationPageStore.setFilterSort(pSort);
      this.notificationPageStore.setFilterPerPage(pPerPage);
      this.notificationPageStore.setFilterCurrentPage(pCurrentPage);
    });
  }

  // Get =======================================================================
  getAllNotification() {
    this.notificationPageStore.getAllNotification();
  }

  // Log =======================================================================
  logPage(type: string): void {
    if (type === 'init') {
      this.pageInfo = {
        page: this.router.url,
        date_start: moment().format('YYYY-MM-DD HH:mm:ss'),
      };

      this.log = {
        log: { ...this.pageInfo, ...this.userInfo },
        token: this.satudataToken,
      };
    } else if (type === 'destroy') {
      if (typeof Worker !== 'undefined') {
        set(this.log.log, 'date_end', moment().format('YYYY-MM-DD HH:mm:ss'));

        const log = of(this.log);

        fromWorker<object, string>(
          () =>
            new Worker(
              new URL('src/app/__v4/workers/log-page.worker', import.meta.url),
              { type: 'module' },
            ),
          log,
        ).subscribe((message) => {
          if (!environment.production) {
            console.info(`Log page: ${message}`);
          }
        });
      }
    }
  }
}
