import { Routes } from '@angular/router';

export const NOTIFICATION_ROUTES: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        loadComponent: () =>
          import(
            './containers/notification-page/notification-page.component'
          ).then((m) => m.NotificationPageComponent),
      },
    ],
  },
];
