import { Component, ChangeDetectionStrategy } from '@angular/core';
import { CommonModule } from '@angular/common';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';
// COMPONENT
import { JdsBiNavModule } from '@jds-bi/core';

import {
  NotificationPageStore,
  TType,
} from '../../containers/notification-page/notification-page.store';

@Component({
  selector: 'app-notification-tab-type',
  standalone: true,
  imports: [CommonModule, JdsBiNavModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './notification-tab-type.component.html',
})
export class NotificationTabTypeComponent {
  colorScheme = COLORSCHEME;
  satudataUser: LoginData;
  jds: Styles = jds;

  readonly vm$ = this.notificationPageStore.vm$;

  constructor(
    private authenticationService: AuthenticationService,
    private notificationPageStore: NotificationPageStore,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
  }

  onType(value: TType): void {
    this.notificationPageStore.filterType(value);
  }
}
