import {
  Component,
  ChangeDetectionStrategy,
  ViewChild,
  inject,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

// STYLE
import * as jds from '@styles';
// COMPONENT
import { ModalRequestSatuDataTrackingComponent } from '@components-v4';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';
import { NotificationService } from '@services-v4';
// JDS-BI
import {
  JdsBiTableHeadModule,
  JdsBiDropdownModule,
  JdsBiHostedDropdownModule,
  JdsBiPaginationModule,
  JdsBiBadgeModule,
  JdsBiButtonModule,
} from '@jds-bi/core';

import { filter, includes, map } from 'lodash';
import moment from 'moment';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

import { NotificationPageStore } from '../../containers/notification-page/notification-page.store';

@Component({
  selector: 'app-notification-list',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    JdsBiTableHeadModule,
    JdsBiDropdownModule,
    JdsBiHostedDropdownModule,
    JdsBiPaginationModule,
    JdsBiBadgeModule,
    JdsBiButtonModule,
    NgxSkeletonLoaderModule,
    ModalRequestSatuDataTrackingComponent,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './notification-list.component.html',
})
export class NotificationListComponent {
  satudataUser: LoginData;
  colorScheme = COLORSCHEME;
  jds: Styles = jds;
  moment: any = moment;

  @ViewChild(ModalRequestSatuDataTrackingComponent)
  modalTracking: ModalRequestSatuDataTrackingComponent;

  // Service
  private router = inject(Router);
  private authenticationService = inject(AuthenticationService);
  private notificationPageStore = inject(NotificationPageStore);
  private notificationService = inject(NotificationService);

  // Variable
  typeOptions = [
    { value: 'dataset', label: 'Dataset', checked: false },
    { value: 'mapset', label: 'Mapset', checked: false },
  ];
  perPageItems = [
    { value: 10, label: '10' },
    { value: 25, label: '25' },
  ];
  isOpenTypeData = false;
  direction = 'asc';

  readonly vm$ = this.notificationPageStore.vm$;

  constructor() {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
  }

  updateNotification(type: string, typeId: number) {
    const body = { is_enable: false, is_read: true };
    const params = {
      type,
      type_id: typeId,
      receiver: this.satudataUser.id,
    };

    this.notificationService.updateItemBulk(body, params).subscribe();
  }

  onPerPage(value: number) {
    this.notificationPageStore.filterPerPage(value);
  }

  onPage(value: number) {
    this.notificationPageStore.filterCurrentPage(value);
  }

  onFilter(code: string, isChecked: boolean) {
    const items = this.typeOptions;

    items.forEach((item) => {
      if (item.value === code) {
        item.checked = isChecked;
      }

      return item;
    });

    const value = map(filter(items, 'checked'), 'value');
    this.notificationPageStore.filterCatalog(value);
  }

  onSort(value: any, direction: string) {
    let dir = '';
    if (direction === 'desc') {
      dir = 'asc';
    } else if (direction === 'asc') {
      dir = 'desc';
    }

    this.notificationPageStore.filterSortTable(`${value}:${dir}`);
  }

  onRead(
    feature: number,
    type: string,
    typeId: number,
    title: string = null,
    notes: string = null,
  ) {
    this.updateNotification(type, typeId);

    if (feature === 1) {
      this.router.navigate(['/dataset/review', typeId]);
    } else if (feature === 2) {
      if (this.satudataUser.role_name === 'walidata') {
        this.router.navigate(['/desk'], {
          queryParams: {
            type: 'upload-change',
            status: 'draft',
            statusVerification: notes,
          },
        });
      } else if (this.satudataUser.role_name === 'opd') {
        if (title === 'Walidata melakukan publikasi terhadap dataset ') {
          this.router.navigate(['/dataset/detail', typeId]);
        } else {
          this.router.navigate(['/desk'], {
            queryParams: {
              type: 'upload-change',
              status: 'revision',
            },
          });
        }
      }
    } else if (feature === 4) {
      if (this.satudataUser.role_name === 'walidata') {
        this.router.navigate(['/desk'], {
          queryParams: {
            type: 'request-dataset',
            application: 'opendata',
          },
        });
      }
    } else if (feature === 5) {
      this.router.navigate(['/e-walidata-2/assign']);
    }
  }

  onRequestDatasetPrivate(type: string, typeId: number): void {
    this.updateNotification(type, typeId);

    if (this.satudataUser.role_name === 'walidata') {
      this.router.navigate(['/desk'], {
        queryParams: {
          type: 'request-dataset',
          application: 'satudata',
        },
      });
    } else if (
      this.satudataUser.role_name === 'opd' ||
      this.satudataUser.role_name === 'opd-view'
    ) {
      this.modalTracking.openModal();
      this.notificationPageStore.getLastNotification(typeId);
      this.notificationPageStore.getHistoryRequestDatasetSatuData(typeId);
    }
  }

  getSkeleton() {
    return {
      reviewDataset: new Array(3),
      uploadChange: new Array(4),
      requestDataset: new Array(3),
      eWalidata: new Array(3),
    };
  }

  getCatalog(catalog: string) {
    let title = '';
    if (catalog === 'dataset') {
      title = 'Dataset';
    } else if (catalog === 'indicator') {
      title = 'Indikator';
    } else if (catalog === 'visualization') {
      title = 'Visualisasi';
    }
    return title;
  }

  getRequestDatasetPrivateContent(title: string, content: string) {
    let request: string;
    if (includes(content, 'Nomor ticket')) {
      const splitted = content.split('Nomor ticket');
      request = `${title} ${splitted[0]}<br>Nomor Tiket${splitted[1]}`;
    } else if (includes(content, 'Nomor tiket')) {
      const splitted = content.split('Nomor tiket');
      request = `${title} ${splitted[0]}<br>Nomor Tiket${splitted[1]}`;
    } else {
      request = `${title} ${content}`;
    }

    return request;
  }
}
