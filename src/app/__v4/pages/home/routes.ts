import { Routes } from '@angular/router';

export const HOME_ROUTES: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        loadComponent: () =>
          import('./containers/home-page/home-page.component').then(
            (m) => m.HomePageComponent,
          ),
      },
    ],
  },
];
