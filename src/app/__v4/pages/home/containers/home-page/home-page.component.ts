import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
  inject,
  viewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { of } from 'rxjs';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';
// INTERFACE
import { Breadcrumb, Styles } from '@interfaces-v4';
// SERVICE
import { AuthenticationService, StateService } from '@services-v3';
import { GlobalService } from '@services-v4';
// COMPONENT
import { ModalSurveyComponent } from '@components-v4';

import { set } from 'lodash';
import moment from 'moment';
import { fromWorker } from 'observable-webworker';

import {
  HomeBannerComponent,
  HomeCarouselTopicComponent,
  HomeStatisticComponent,
  HomeTabTrendingComponent,
  HomeListTrendingDatasetComponent,
  HomeListTrendingVisualizationComponent,
  HomeListTrandingTopicComponent,
  HomeListTrandingOrganizationComponent,
} from '../../components';

import { HomePageStore } from './home-page.store';

@Component({
  selector: 'app-home-page',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    HomeBannerComponent,
    HomeCarouselTopicComponent,
    HomeStatisticComponent,
    HomeTabTrendingComponent,
    HomeListTrendingDatasetComponent,
    HomeListTrendingVisualizationComponent,
    HomeListTrandingTopicComponent,
    HomeListTrandingOrganizationComponent,
    ModalSurveyComponent,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './home-page.component.html',
  providers: [HomePageStore],
})
export class HomePageComponent implements OnInit, OnDestroy {
  satudataToken: string;
  jds: Styles = jds;
  moment = moment;

  modalSurvey = viewChild(ModalSurveyComponent);

  // Log
  log = null;
  pageInfo = null;
  deviceInfo = null;
  userInfo = null;

  // Setting
  title: string;
  label = 'Beranda';
  breadcrumb: Breadcrumb[] = [{ label: 'Beranda', link: '/home' }];

  // Service
  private router = inject(Router);
  private route = inject(ActivatedRoute);
  private globalService = inject(GlobalService);
  private authenticationService = inject(AuthenticationService);
  private stateService = inject(StateService);
  private titleService = inject(Title);
  private homePageStore = inject(HomePageStore);

  // Component Store View Model
  readonly vm = this.homePageStore;

  constructor() {
    this.authenticationService.satudataToken.subscribe(
      (x) => (this.satudataToken = x),
    );
    this.stateService.currentLogUser.subscribe((x) => (this.userInfo = x));
  }

  ngOnInit(): void {
    this.logPage('init');
    this.settingsAll();

    this.homePageStore.getAllTopicSatuData();
    this.homePageStore.getCountDataset();
    this.homePageStore.getCountIndicator();
    this.homePageStore.getCountVisualization();
    this.homePageStore.getCountOrganization();
    this.homePageStore.getAllTrending();

    // TODO: modal
    // setTimeout(() => {
    //   this.modalSurvey().openModal();
    // }, 1000);
  }

  ngOnDestroy(): void {
    this.logPage('destroy');
  }

  settingsAll(): void {
    this.title = this.globalService.title;
    this.titleService.setTitle(`${this.label} - ${this.title}`);
    this.globalService.changeLabel(this.label);
    this.globalService.changeBreadcrumb(this.breadcrumb);
  }

  // Log =======================================================================
  logPage(type: string): void {
    if (type === 'init') {
      this.pageInfo = {
        page: this.router.url,
        date_start: moment().format('YYYY-MM-DD HH:mm:ss'),
      };

      this.log = {
        log: { ...this.pageInfo, ...this.userInfo },
        token: this.satudataToken,
      };
    } else if (type === 'destroy') {
      if (typeof Worker !== 'undefined') {
        set(this.log.log, 'date_end', moment().format('YYYY-MM-DD HH:mm:ss'));

        const log = of(this.log);

        fromWorker<object, string>(
          () =>
            new Worker(
              new URL('src/app/__v4/workers/log-page.worker', import.meta.url),
              { type: 'module' },
            ),
          log,
        ).subscribe((message) => {
          if (!environment.production) {
            console.info(`Log page: ${message}`);
          }
        });
      }
    }
  }
}
