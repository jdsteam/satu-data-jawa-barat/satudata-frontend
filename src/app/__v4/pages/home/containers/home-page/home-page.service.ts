import { inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { environment } from 'src/environments/environment';

// CONSTANT
import { APPLICATION } from '@constants-v4';
// MODEL
import { Dataset, Organization, Topic, Visualization } from '@models-v4';
// SERVICE
import { PaginationService } from '@services-v3';
// UTIL
import { hasOwnProperty } from '@utils-v4';

import { assign } from 'lodash';
import { UseQuery } from '@ngneat/query';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root',
})
export class HomePageService {
  title = 'Home Page';

  // API URL
  apiUrlDataset = `${environment.backendURL}dataset`;
  apiUrlVisualization = `${environment.backendURL}visualization`;
  apiUrlTopic = `${environment.backendURL}sektoral`;
  apiUrlOrganization = `${environment.backendURL}skpd`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  private http = inject(HttpClient);
  private useQuery = inject(UseQuery);
  private paginationService = inject(PaginationService);
  private toastr = inject(ToastrService);

  modifyWhereParams(app: any, params: any) {
    if (app) {
      const where = JSON.parse(params.where);
      assign(where, { [app.paramWhere]: true });
      params.where = JSON.stringify(where);
    }

    return params;
  }

  // Trending Dataset
  getTrendingDataset(params: any, options: any) {
    const merge = new URLSearchParams(params);

    const queryName = `List ${this.title} Trending Dataset`;

    return this.http
      .get<any>(`${this.apiUrlDataset}?${merge}`, this.httpOptions)
      .pipe(
        map((response) => {
          if (response.error === 1 && response.message !== 'Data not found') {
            throw new Error(response.message);
          }

          return new Dataset().deserialize(response);
        }),
        map((response) => {
          const hasPagination = hasOwnProperty(options, 'pagination');
          const hasInfinite = hasOwnProperty(options, 'infinite');

          const isPagination = hasPagination ? options.pagination : true;
          const isInfinite = hasInfinite ? options.infinite : false;

          let pagination = { empty: true, infinite: isInfinite };
          if (isPagination) {
            const resultPagination = this.paginationService.getPagination(
              response.meta.total_record,
              params.skip,
              params.limit,
            );

            pagination = {
              empty: false,
              infinite: isInfinite,
              ...resultPagination,
            };
          }

          return {
            pagination,
            list: response.data,
          };
        }),
        catchError((e) => {
          this.toastr.error(e.message, queryName);
          return throwError(() => e);
        }),
      );
  }

  getQueryTrendingDataset(params: any, options: any = {}) {
    return this.useQuery(
      ['trendingDataset'],
      () => {
        return this.getTrendingDataset(params, options);
      },
      options,
    );
  }

  // Trending Visualization
  getTrendingVisualization(params: any, options: any) {
    const merge = new URLSearchParams(params);

    const queryName = `List ${this.title} Trending Visualization`;

    return this.http
      .get<any>(`${this.apiUrlVisualization}?${merge}`, this.httpOptions)
      .pipe(
        map((response) => {
          if (response.error === 1 && response.message !== 'Data not found') {
            throw new Error(response.message);
          }

          return new Visualization().deserialize(response);
        }),
        map((response) => {
          const hasPagination = hasOwnProperty(options, 'pagination');
          const hasInfinite = hasOwnProperty(options, 'infinite');

          const isPagination = hasPagination ? options.pagination : true;
          const isInfinite = hasInfinite ? options.infinite : false;

          let pagination = { empty: true, infinite: isInfinite };
          if (isPagination) {
            const resultPagination = this.paginationService.getPagination(
              response.meta.total_record,
              params.skip,
              params.limit,
            );

            pagination = {
              empty: false,
              infinite: isInfinite,
              ...resultPagination,
            };
          }

          return {
            pagination,
            list: response.data,
          };
        }),
        catchError((e) => {
          this.toastr.error(e.message, queryName);
          return throwError(() => e);
        }),
      );
  }

  getQueryTrendingVisualization(params: any, options: any = {}) {
    return this.useQuery(
      ['trendingVisualization'],
      () => {
        return this.getTrendingVisualization(params, options);
      },
      options,
    );
  }

  // Trending Topic
  getTrendingTopic(params: any, options: any, app = null) {
    const appData = APPLICATION.get(app);
    const newParams = this.modifyWhereParams(appData, params);
    const merge = new URLSearchParams(newParams);

    const queryName = appData
      ? `List ${this.title} Trending Topic ${appData.name}`
      : `List ${this.title} Trending Topic`;

    return this.http
      .get<any>(`${this.apiUrlTopic}/trending?${merge}`, this.httpOptions)
      .pipe(
        map((response) => {
          if (response.error === 1 && response.message !== 'Data not found') {
            throw new Error(response.message);
          }

          return new Topic().deserialize(response);
        }),
        map((response) => {
          const pagination = { empty: true, infinite: false };

          // eslint-disable-next-line array-callback-return
          response.data.map((result) => {
            if (result.datecount === 0) {
              result.datecount = [1, 1];
            } else {
              result.datecount = result.datecount.split(',').map(Number);
            }
          });

          return {
            pagination,
            list: response.data,
          };
        }),
        catchError((e) => {
          this.toastr.error(e.message, queryName);
          return throwError(() => e);
        }),
      );
  }

  getQueryTrendingTopic(params: any, options: any = {}, app = null) {
    const appData = APPLICATION.get(app);

    const queryKey = appData
      ? `trendingTopic${appData.alias}`
      : `trendingTopic`;

    return this.useQuery(
      [queryKey],
      () => {
        return this.getTrendingTopic(params, options, app);
      },
      options,
    );
  }

  // Trending Organization
  getTrendingOrganization(params: any, options: any, app = null) {
    const appData = APPLICATION.get(app);
    const newParams = this.modifyWhereParams(appData, params);
    const merge = new URLSearchParams(newParams);

    const queryName = appData
      ? `List ${this.title} Trending Organization ${appData.name}`
      : `List ${this.title} Trending Organization`;

    return this.http
      .get<any>(
        `${this.apiUrlOrganization}/trending?${merge}`,
        this.httpOptions,
      )
      .pipe(
        map((response) => {
          if (response.error === 1 && response.message !== 'Data not found') {
            throw new Error(response.message);
          }

          return new Organization().deserialize(response);
        }),
        map((response) => {
          const pagination = { empty: true, infinite: false };

          // eslint-disable-next-line array-callback-return
          response.data.map((result) => {
            if (result.datecount === 0) {
              result.datecount = [1, 1];
            } else {
              result.datecount = result.datecount.split(',').map(Number);
            }
          });

          return {
            pagination,
            list: response.data,
          };
        }),
        catchError((e) => {
          this.toastr.error(e.message, queryName);
          return throwError(() => e);
        }),
      );
  }

  getQueryTrendingOrganization(params: any, options: any = {}, app = null) {
    const appData = APPLICATION.get(app);

    const queryKey = appData
      ? `trendingOrganization${appData.alias}`
      : `trendingOrganization`;

    return this.useQuery(
      [queryKey],
      () => {
        return this.getTrendingOrganization(params, options, app);
      },
      options,
    );
  }
}
