import { inject } from '@angular/core';
import { switchMap } from 'rxjs';
import {
  patchState,
  signalStore,
  signalStoreFeature,
  withMethods,
  withState,
} from '@ngrx/signals';
import { rxMethod } from '@ngrx/signals/rxjs-interop';
import { tapResponse } from '@ngrx/operators';

// FEATURE
import { withTopicSatuData } from '@features-v4';
// SERVICE
import {
  DatasetService,
  IndicatorService,
  OrganizationService,
  VisualizationService,
} from '@services-v4';

import { HomePageService } from './home-page.service';

type HomePageState = {
  filterCatalog: string;
  filterSort: string;
  filterDirection: string;
  listTopicSatuData: any;
  countDataset: any;
  countIndicator: any;
  countVisualization: any;
  countOrganization: any;
  listTrendingDataset: any;
  listTrendingVisualization: any;
  listTrendingTopicSatuData: any;
  listTrendingOrganizationSatuData: any;
};

const initialState: HomePageState = {
  filterCatalog: 'dataset',
  filterSort: 'access',
  filterDirection: 'desc',
  listTopicSatuData: null,
  countDataset: null,
  countIndicator: null,
  countVisualization: null,
  countOrganization: null,
  listTrendingDataset: null,
  listTrendingVisualization: null,
  listTrendingTopicSatuData: null,
  listTrendingOrganizationSatuData: null,
};

export function withHomePage() {
  return signalStoreFeature(withTopicSatuData());
}

export const HomePageStore = signalStore(
  withState(initialState),
  withHomePage(),
  withMethods(
    (
      store,
      datasetService = inject(DatasetService),
      indicatorService = inject(IndicatorService),
      visualizationService = inject(VisualizationService),
      organizationService = inject(OrganizationService),
      homePageService = inject(HomePageService),
    ) => ({
      handleFilterCatalog(catalog: string): void {
        let sort: string;
        switch (catalog) {
          case 'dataset':
            sort = 'access';
            break;
          case 'visualization':
            sort = 'see';
            break;
          default:
            sort = 'count';
            break;
        }

        patchState(store, () => ({
          filterCatalog: catalog,
          filterSort: sort,
          filterDirection: initialState.filterDirection,
        }));

        this.getAllTrending();
      },
      handleFilterSort(sort: string): void {
        patchState(store, () => ({
          filterSort: sort,
        }));

        this.getAllTrending();
      },
      handleFilterDirection(direction: string): void {
        patchState(store, () => ({
          filterDirection: direction,
        }));

        this.getAllTrending();
      },
      getCountDataset: rxMethod<void>(
        switchMap(() => {
          const params = {
            where: JSON.stringify({
              is_active: true,
              is_deleted: false,
              is_validate: 3,
            }),
          };

          return datasetService.getQueryTotal(params).result$.pipe(
            tapResponse({
              next: (result) => {
                patchState(store, { countDataset: result });
              },
              error: console.error,
            }),
          );
        }),
      ),
      getCountIndicator: rxMethod<void>(
        switchMap(() => {
          const params = {
            where: JSON.stringify({
              is_active: true,
              is_deleted: false,
            }),
          };

          return indicatorService.getQueryTotal(params).result$.pipe(
            tapResponse({
              next: (result) => {
                patchState(store, { countIndicator: result });
              },
              error: console.error,
            }),
          );
        }),
      ),
      getCountVisualization: rxMethod<void>(
        switchMap(() => {
          const params = {
            where: JSON.stringify({
              is_active: true,
              is_deleted: false,
            }),
          };

          return visualizationService.getQueryTotal(params).result$.pipe(
            tapResponse({
              next: (result) => {
                patchState(store, { countVisualization: result });
              },
              error: console.error,
            }),
          );
        }),
      ),
      getCountOrganization: rxMethod<void>(
        switchMap(() => {
          const params = {
            where: JSON.stringify({
              is_deleted: false,
              is_external: false,
            }),
          };

          return organizationService.getQueryTotal(params).result$.pipe(
            tapResponse({
              next: (result) => {
                patchState(store, { countOrganization: result });
              },
              error: console.error,
            }),
          );
        }),
      ),
      getAllTrending(): void {
        const { filterCatalog: catalog } = store;

        switch (catalog()) {
          case 'dataset':
            return this.getTrendingDataset();
          case 'visualization':
            return this.getTrendingVisualization();
          case 'topic':
            return this.getTrendingTopic();
          case 'organization':
            return this.getTrendingOrganization();
          default:
            return null;
        }
      },
      getTrendingDataset: rxMethod<void>(
        switchMap(() => {
          const { filterSort: sort } = store;

          let pSort: string;
          switch (sort()) {
            case 'newest':
              pSort = `mdate:desc`;
              break;
            case 'access':
              pSort = `count_access_private:desc`;
              break;
            default:
              break;
          }

          const params = {
            sort: pSort,
            limit: 5,
            where: JSON.stringify({
              is_validate: 3,
              is_active: true,
              is_deleted: false,
            }),
          };

          const options = {
            pagination: false,
          };

          return homePageService
            .getQueryTrendingDataset(params, options)
            .result$.pipe(
              tapResponse({
                next: (result) => {
                  patchState(store, { listTrendingDataset: result });
                },
                error: console.error,
              }),
            );
        }),
      ),
      getTrendingVisualization: rxMethod<void>(
        switchMap(() => {
          const { filterSort: sort } = store;

          let pSort: string;
          switch (sort()) {
            case 'newest':
              pSort = `mdate:desc`;
              break;
            case 'see':
              pSort = `count_view_private:desc`;
              break;
            default:
              break;
          }

          const params = {
            sort: pSort,
            limit: 5,
            where: JSON.stringify({
              is_active: true,
              is_deleted: false,
            }),
          };

          const options = {
            pagination: false,
          };

          return homePageService
            .getQueryTrendingVisualization(params, options)
            .result$.pipe(
              tapResponse({
                next: (result) => {
                  patchState(store, { listTrendingVisualization: result });
                },
                error: console.error,
              }),
            );
        }),
      ),
      getTrendingTopic: rxMethod<void>(
        switchMap(() => {
          const { filterDirection: direction } = store;

          const pSort = `count:${direction()}`;

          const params = {
            sort: pSort,
            where: JSON.stringify({
              is_deleted: false,
            }),
          };

          const options = {
            pagination: false,
          };
          const app = 'satudata';

          return homePageService
            .getQueryTrendingTopic(params, options, app)
            .result$.pipe(
              tapResponse({
                next: (result) => {
                  patchState(store, { listTrendingTopicSatuData: result });
                },
                error: console.error,
              }),
            );
        }),
      ),
      getTrendingOrganization: rxMethod<void>(
        switchMap(() => {
          const { filterDirection: direction } = store;

          const pSort = `count:${direction()}`;

          const params = {
            sort: pSort,
            where: JSON.stringify({
              is_active: true,
              is_deleted: false,
            }),
          };

          const options = {
            pagination: false,
          };
          const app = 'satudata';

          return homePageService
            .getQueryTrendingOrganization(params, options, app)
            .result$.pipe(
              tapResponse({
                next: (result) => {
                  patchState(store, {
                    listTrendingOrganizationSatuData: result,
                  });
                },
                error: console.error,
              }),
            );
        }),
      ),
    }),
  ),
);
