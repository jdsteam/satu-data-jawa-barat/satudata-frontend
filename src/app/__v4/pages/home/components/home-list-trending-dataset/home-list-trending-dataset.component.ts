import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// STYLE
import * as jds from '@styles';
// INTERFACE
import { Styles } from '@interfaces-v4';
// JDS-Bi
import { jdsBiColor } from '@jds-bi/cdk';
import { JdsBiBadgeModule } from '@jds-bi/core';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconBook,
  iconBuilding,
  iconCalendar,
  iconCircleCheck,
  iconCircleDown,
  iconClockRotateLeft,
  iconClockThree,
  iconFileTimer,
} from '@jds-bi/icons';

import moment from 'moment';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgxPopperjsModule } from 'ngx-popperjs';

import { HomePageStore } from '../../containers/home-page/home-page.store';

@Component({
  selector: 'app-home-list-trending-dataset',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    NgxSkeletonLoaderModule,
    JdsBiBadgeModule,
    JdsBiIconsModule,
    NgxPopperjsModule,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './home-list-trending-dataset.component.html',
})
export class HomeListTrendingDatasetComponent {
  jds: Styles = jds;
  jdsBiColor = jdsBiColor;
  moment = moment;

  // Service
  private iconService = inject(JdsBiIconsService);
  private homePageStore = inject(HomePageStore);

  // Component Store View Model
  readonly vm = this.homePageStore;

  constructor() {
    this.iconService.registerIcons([
      iconBook,
      iconBuilding,
      iconCalendar,
      iconCircleCheck,
      iconCircleDown,
      iconClockRotateLeft,
      iconClockThree,
      iconFileTimer,
    ]);
  }
}
