import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';
// INTERFACE
import { Styles } from '@interfaces-v4';
// JDS-BI
import { JdsBiTableHeadModule } from '@jds-bi/core';

import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { TrendModule } from 'ngx-trend';

import { HomePageStore } from '../../containers/home-page/home-page.store';

@Component({
  selector: 'app-home-list-trending-organization',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    CommonModule,
    RouterModule,
    JdsBiTableHeadModule,
    NgxSkeletonLoaderModule,
    TrendModule,
  ],
  templateUrl: './home-list-trending-organization.component.html',
})
export class HomeListTrandingOrganizationComponent {
  env = environment;
  jds: Styles = jds;

  // Service
  private homePageStore = inject(HomePageStore);

  // Component Store
  readonly vm = this.homePageStore;

  handleSort(event: any, direction: string) {
    let dir = '';
    if (direction === 'desc') {
      dir = 'asc';
    } else if (direction === 'asc') {
      dir = 'desc';
    }

    this.homePageStore.handleFilterDirection(dir);
  }
}
