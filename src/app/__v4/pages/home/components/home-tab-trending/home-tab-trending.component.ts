import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// JDS-BI
import { JdsBiNavModule } from '@jds-bi/core';

import { isEmpty } from 'lodash';
import { NgSelectModule } from '@ng-select/ng-select';

import { HomePageStore } from '../../containers/home-page/home-page.store';

@Component({
  selector: 'app-home-tab-trending',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CommonModule, FormsModule, JdsBiNavModule, NgSelectModule],
  templateUrl: './home-tab-trending.component.html',
  styleUrls: ['./home-tab-trending.component.scss'],
})
export class HomeTabTrendingComponent {
  colorScheme = COLORSCHEME;
  jds: Styles = jds;

  // Service
  private homePageStore = inject(HomePageStore);

  // Variable
  sortDatasetOption: any[] = [
    { value: 'access', label: 'Diunduh' },
    { value: 'newest', label: 'Terbaru' },
  ];
  sortIndicatorOption: any[] = [
    { value: 'see', label: 'Dilihat' },
    { value: 'access', label: 'Diunduh' },
    { value: 'newest', label: 'Terbaru' },
  ];
  sortVisualizationOption: any[] = [
    { value: 'see', label: 'Dilihat' },
    { value: 'newest', label: 'Terbaru' },
  ];

  // Component Store View Model
  readonly vm = this.homePageStore;

  handleFilterCatalog(event: string): void {
    const value = isEmpty(event) === false ? event : null;

    this.homePageStore.handleFilterCatalog(value);
  }

  handleFilterSort(event: any): void {
    const value = isEmpty(event) === false ? event.value : null;

    this.homePageStore.handleFilterSort(value);
  }
}
