import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';
// INTERFACE
import { Styles } from '@interfaces-v4';
// JDS-BI
import { jdsBiColor } from '@jds-bi/cdk';

import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

import { HomePageStore } from '../../containers/home-page/home-page.store';

@Component({
  selector: 'app-home-carousel-topic',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CommonModule, RouterModule, NgxSkeletonLoaderModule],
  templateUrl: './home-carousel-topic.component.html',
  styleUrls: ['./home-carousel-topic.component.scss'],
})
export class HomeCarouselTopicComponent {
  env = environment;
  jds: Styles = jds;
  jdsBiColor = jdsBiColor;

  // Service
  private homePageStore = inject(HomePageStore);

  // Component Store View Model
  readonly vm = this.homePageStore;
}
