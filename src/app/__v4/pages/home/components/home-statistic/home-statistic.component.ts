import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// STYLE
import * as jds from '@styles';
// INTERFACE
import { Styles } from '@interfaces-v4';
// JDS-BI
import { jdsBiColor } from '@jds-bi/cdk';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconArrowRight,
} from '@jds-bi/icons';

import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

import { HomePageStore } from '../../containers/home-page/home-page.store';

@Component({
  selector: 'app-home-statistic',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    NgxSkeletonLoaderModule,
    JdsBiIconsModule,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './home-statistic.component.html',
  styleUrls: ['./home-statistic.component.scss'],
})
export class HomeStatisticComponent {
  jds: Styles = jds;
  jdsBiColor = jdsBiColor;

  // Service
  private iconService = inject(JdsBiIconsService);
  private homePageStore = inject(HomePageStore);

  // Component Store View Model
  readonly vm = this.homePageStore;

  constructor() {
    this.iconService.registerIcons([iconArrowRight]);
  }
}
