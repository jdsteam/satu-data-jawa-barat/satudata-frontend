import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  inject,
} from '@angular/core';
import { CommonModule, NgOptimizedImage } from '@angular/common';
import { Router, RouterModule } from '@angular/router';
import { Subject, concat, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';
// INTERFACE
import { Styles } from '@interfaces-v4';
// SERVICE
import { AuthenticationService, StateService } from '@services-v3';
// JDS-BI
import { jdsBiColor } from '@jds-bi/cdk';

import moment from 'moment';
import { fromWorker } from 'observable-webworker';
import { NgSelectModule } from '@ng-select/ng-select';

@Component({
  selector: 'app-home-banner',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CommonModule, RouterModule, NgOptimizedImage, NgSelectModule],
  templateUrl: './home-banner.component.html',
  styleUrls: ['./home-banner.component.scss'],
})
export class HomeBannerComponent implements OnInit {
  satudataToken: string;
  jds: Styles = jds;
  jdsBiColor = jdsBiColor;
  moment = moment;

  // Log
  log = null;
  searchInfo = null;
  userInfo = null;

  // Service
  private router = inject(Router);
  private authenticationService = inject(AuthenticationService);
  private stateService = inject(StateService);

  // Data
  dataSearch$: any;
  isLoading = false;
  inputSearch$ = new Subject<string>();

  constructor() {
    this.authenticationService.satudataToken.subscribe(
      (x) => (this.satudataToken = x),
    );
    this.stateService.currentLogUser.subscribe((x) => (this.userInfo = x));
  }

  ngOnInit(): void {
    this.getSearch();
  }

  getSearch() {
    this.dataSearch$ = concat(
      this.inputSearch$.pipe(
        debounceTime(1000),
        distinctUntilChanged(),
        tap(() => (this.isLoading = true)),
        map((value) => {
          this.isLoading = false;

          return [
            { search: value, content: ' di Dataset', type: 'dataset' },
            { search: value, content: ' di Mapset', type: 'mapset' },
            { search: value, content: ' di Indikator', type: 'indicator' },
            {
              search: value,
              content: ' di Visualisasi',
              type: 'visualization',
            },
          ];
        }),
      ),
    );
  }

  onSelectedSearch(event) {
    this.router.navigate([event.type], {
      queryParams: {
        q: event.search,
        currentpage: null,
      },
      queryParamsHandling: 'merge',
    });

    // Log
    this.logSearch(event);
  }

  // Log =======================================================================
  logSearch(event: any): void {
    if (typeof Worker !== 'undefined') {
      this.searchInfo = {
        page: this.router.url,
        date_start: moment().format('YYYY-MM-DD HH:mm:ss'),
        date_end: moment().format('YYYY-MM-DD HH:mm:ss'),
        search_type: event.type,
        search_keyword: event.search,
      };

      this.searchInfo = {
        log: { ...this.searchInfo, ...this.userInfo },
        token: this.satudataToken,
      };

      fromWorker<object, string>(
        () =>
          new Worker(
            new URL('src/app/__v4/workers/log-search.worker', import.meta.url),
            {
              type: 'module',
              name: 'search',
            },
          ),
        of(this.searchInfo),
      ).subscribe((message) => {
        if (!environment.production) {
          console.info(`Log search: ${message}`);
        }
      });
    }
  }
}
