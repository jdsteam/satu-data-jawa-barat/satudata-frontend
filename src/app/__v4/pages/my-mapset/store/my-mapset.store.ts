import { Injectable, inject } from '@angular/core';
import { ComponentStore } from '@ngrx/component-store';
import {
  UntypedFormControl,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { filter, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { Observable } from 'rxjs';

// CONSTANT
import { SOURCE_MAPSET } from '@constants-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';
import {
  TopicService,
  ClassificationService,
  MapsetProgramService,
  RegionalService,
  OrganizationService,
  OrganizationSubService,
  OrganizationUnitService,
  MapsetSourceService,
  MapsetTypeService,
  MapsetAreaCoverageService,
  MapsetGeoserverService,
  MapsetGeonetworkMetadataService,
  MapsetService,
} from '@services-v4';
// UTIL
import { mapserviceNamespaceLayer } from '@utils-v4';

import { assign, forEach } from 'lodash';
import Swal from 'sweetalert2';

export interface MyMapsetState {
  mapsetFormGroup: UntypedFormGroup;
  mapsetAttributeFormGroup: UntypedFormGroup;
  mapsetAttribute: string[];
  isEditMode: boolean;
  isNote: boolean;
  listOrganizationSelf: any;
  listTopicSatuPeta: any;
  listClassification: any;
  listMapsetProgram: any;
  listRegional: any;
  listOrganizationSatuPeta: any;
  listOrganizationSub: any;
  listOrganizationUnit: any;
  listMapsetSource: any;
  listMapsetType: any;
  listMapsetAreaCoverage: any;
  listMapsetGeoserver: any;
  listMapsetGeonetworkMetadata: any;
}

export const DEFAULT_STATE: MyMapsetState = {
  mapsetFormGroup: new UntypedFormGroup({
    info_name: new UntypedFormControl(null, [
      Validators.required,
      Validators.maxLength(255),
    ]),
    info_description: new UntypedFormControl('', [Validators.required]),
    info_scale: new UntypedFormControl(null, [
      Validators.required,
      Validators.maxLength(255),
    ]),
    info_topic: new UntypedFormControl(null, [Validators.required]),
    info_classification: new UntypedFormControl(null, [Validators.required]),
    info_classification_access: new UntypedFormControl(null),
    info_status_data: new UntypedFormControl(null, [Validators.required]),
    info_program: new UntypedFormControl(null),
    info_regional_type: new UntypedFormControl(null),
    info_regional: new UntypedFormControl(null, [Validators.required]),
    info_organization: new UntypedFormControl(null),
    info_organization_sub: new UntypedFormControl(null),
    info_organization_unit: new UntypedFormControl(null),
    info_organization_county: new UntypedFormControl(null, [
      Validators.maxLength(255),
    ]),
    info_organization_address: new UntypedFormControl(null),
    info_organization_phone: new UntypedFormControl(null, [
      Validators.maxLength(255),
    ]),
    info_organization_email: new UntypedFormControl(null, [
      Validators.maxLength(255),
    ]),
    info_status: new UntypedFormControl(true),
    info_popular: new UntypedFormControl(false),
    meta_mapset_source: new UntypedFormControl(null, [Validators.required]),
    meta_mapservice: new UntypedFormControl(null),
    meta_mapservice_url: new UntypedFormControl(null),
    meta_file_xml: new UntypedFormControl(null),
    meta_file_zip: new UntypedFormControl(null),
    meta_mapset_type: new UntypedFormControl(null, [Validators.required]),
    meta_area_coverage: new UntypedFormControl(null),
    meta_period_update: new UntypedFormControl(null, [
      Validators.maxLength(255),
    ]),
    meta_version: new UntypedFormControl(null, [Validators.maxLength(255)]),
    change: new UntypedFormControl(null),
  }),
  mapsetAttributeFormGroup: new UntypedFormGroup({
    lcode: new UntypedFormControl(null, [Validators.maxLength(255)]),
    conjin: new UntypedFormControl(null, [Validators.maxLength(255)]),
    kdprv: new UntypedFormControl('Kode Provinsi'),
    shape: new UntypedFormControl('SHAPE'),
    objectid: new UntypedFormControl('Object ID'),
  }),
  mapsetAttribute: [],
  isEditMode: false,
  isNote: false,
  listOrganizationSelf: null,
  listTopicSatuPeta: null,
  listClassification: null,
  listMapsetProgram: null,
  listRegional: null,
  listOrganizationSatuPeta: null,
  listOrganizationSub: null,
  listOrganizationUnit: null,
  listMapsetSource: null,
  listMapsetType: null,
  listMapsetAreaCoverage: null,
  listMapsetGeoserver: null,
  listMapsetGeonetworkMetadata: null,
};

@Injectable()
export class MyMapsetStore extends ComponentStore<MyMapsetState> {
  satudataUser: LoginData;

  private authenticationService = inject(AuthenticationService);
  private topicService = inject(TopicService);
  private classificationService = inject(ClassificationService);
  private mapsetProgramService = inject(MapsetProgramService);
  private regionalService = inject(RegionalService);
  private organizationService = inject(OrganizationService);
  private organizationSubService = inject(OrganizationSubService);
  private organizationUnitService = inject(OrganizationUnitService);
  private mapsetSourceService = inject(MapsetSourceService);
  private mapsetTypeService = inject(MapsetTypeService);
  private mapsetAreaCoverageService = inject(MapsetAreaCoverageService);
  private mapsetGeoserverService = inject(MapsetGeoserverService);
  private mapsetGeonetworkMetadataService = inject(
    MapsetGeonetworkMetadataService,
  );
  private mapsetService = inject(MapsetService);

  constructor() {
    super(DEFAULT_STATE);

    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
  }

  // *********** Updaters *********** //
  readonly setMapsetFormGroup = this.updater((state, value: any) => ({
    ...state,
    mapsetFormGroup: value || new UntypedFormGroup({}),
  }));

  readonly setMapsetAttributeFormGroup = this.updater((state, value: any) => ({
    ...state,
    mapsetAttributeFormGroup: value || new UntypedFormGroup({}),
  }));

  readonly setMapsetAttribute = this.updater((state, value: any) => ({
    ...state,
    mapsetAttribute: value || [],
  }));

  readonly setIsEditMode = this.updater((state, value: boolean) => ({
    ...state,
    isEditMode: value || false,
  }));

  readonly setIsNote = this.updater((state, value: boolean) => ({
    ...state,
    isNote: value || false,
  }));

  readonly setListOrganizationSelf = this.updater((state, value: any) => ({
    ...state,
    listOrganizationSelf: value,
  }));

  readonly setListTopicSatuPeta = this.updater((state, value: any) => ({
    ...state,
    listTopicSatuPeta: value,
  }));

  readonly setListClassification = this.updater((state, value: any) => ({
    ...state,
    listClassification: value,
  }));

  readonly setListMapsetProgram = this.updater((state, value: any) => ({
    ...state,
    listMapsetProgram: value,
  }));

  readonly setListRegional = this.updater((state, value: any) => ({
    ...state,
    listRegional: value,
  }));

  readonly setListOrganizationSatuPeta = this.updater((state, value: any) => ({
    ...state,
    listOrganizationSatuPeta: value,
  }));

  readonly setListOrganizationSub = this.updater((state, value: any) => ({
    ...state,
    listOrganizationSub: value,
  }));

  readonly setListOrganizationUnit = this.updater((state, value: any) => ({
    ...state,
    listOrganizationUnit: value,
  }));

  readonly setListMapsetSource = this.updater((state, value: any) => ({
    ...state,
    listMapsetSource: value,
  }));

  readonly setListMapsetType = this.updater((state, value: any) => ({
    ...state,
    listMapsetType: value,
  }));

  readonly setListMapsetAreaCoverage = this.updater((state, value: any) => ({
    ...state,
    listMapsetAreaCoverage: value,
  }));

  readonly setListMapsetGeoserver = this.updater((state, value: any) => ({
    ...state,
    listMapsetGeoserver: value,
  }));

  readonly setListMapsetGeonetworkMetadata = this.updater(
    (state, value: any) => ({
      ...state,
      listMapsetGeonetworkMetadata: value,
    }),
  );

  // *********** Selectors *********** //
  readonly getMapsetFormGroup$ = this.select(
    ({ mapsetFormGroup }) => mapsetFormGroup,
  );
  readonly getMapsetAttributeFormGroup$ = this.select(
    ({ mapsetAttributeFormGroup }) => mapsetAttributeFormGroup,
  );
  readonly getMapsetAttribute$ = this.select(
    ({ mapsetAttribute }) => mapsetAttribute,
  );
  readonly getIsEditMode$ = this.select(({ isEditMode }) => isEditMode);
  readonly getIsNote$ = this.select(({ isNote }) => isNote);
  // List
  readonly getListOrganizationSelf$ = this.select(
    ({ listOrganizationSelf }) => listOrganizationSelf,
  );
  readonly getListTopicSatuPeta$ = this.select(
    ({ listTopicSatuPeta }) => listTopicSatuPeta,
  );
  readonly getListClassification$ = this.select(
    ({ listClassification }) => listClassification,
  );
  readonly getListMapsetProgram$ = this.select(
    ({ listMapsetProgram }) => listMapsetProgram,
  );
  readonly getListRegional$ = this.select(({ listRegional }) => listRegional);
  readonly getListOrganizationSatuPeta$ = this.select(
    ({ listOrganizationSatuPeta }) => listOrganizationSatuPeta,
  );
  readonly getListOrganizationSub$ = this.select(
    ({ listOrganizationSub }) => listOrganizationSub,
  );
  readonly getListOrganizationUnit$ = this.select(
    ({ listOrganizationUnit }) => listOrganizationUnit,
  );
  readonly getListMapsetSource$ = this.select(
    ({ listMapsetSource }) => listMapsetSource,
  );
  readonly getListMapsetType$ = this.select(
    ({ listMapsetType }) => listMapsetType,
  );
  readonly getListMapsetAreaCoverage$ = this.select(
    ({ listMapsetAreaCoverage }) => listMapsetAreaCoverage,
  );
  readonly getListMapsetGeoserver$ = this.select(
    ({ listMapsetGeoserver }) => listMapsetGeoserver,
  );
  readonly getListMapsetGeonetworkMetadata$ = this.select(
    ({ listMapsetGeonetworkMetadata }) => listMapsetGeonetworkMetadata,
  );

  // ViewModel of AddEdit component
  readonly vm$ = this.select(
    this.state$,
    this.getMapsetFormGroup$,
    this.getMapsetAttributeFormGroup$,
    this.getMapsetAttribute$,
    this.getIsEditMode$,
    this.getIsNote$,
    this.getListOrganizationSelf$,
    this.getListTopicSatuPeta$,
    this.getListClassification$,
    this.getListMapsetProgram$,
    this.getListRegional$,
    this.getListOrganizationSatuPeta$,
    this.getListOrganizationSub$,
    this.getListOrganizationUnit$,
    this.getListMapsetSource$,
    this.getListMapsetType$,
    this.getListMapsetAreaCoverage$,
    this.getListMapsetGeoserver$,
    this.getListMapsetGeonetworkMetadata$,
    (
      state,
      getMapsetFormGroup,
      getMapsetAttributeFormGroup,
      getMapsetAttribute,
      getIsEditMode,
      getIsNote,
      getListOrganizationSelf,
      getListTopicSatuPeta,
      getListClassification,
      getListMapsetProgram,
      getListRegional,
      getListOrganizationSatuPeta,
      getListOrganizationSub,
      getListOrganizationUnit,
      getListMapsetSource,
      getListMapsetType,
      getListMapsetAreaCoverage,
      getListMapsetGeoserver,
      getListMapsetGeonetworkMetadata,
    ) => ({
      mapsetFormGroup: state.mapsetFormGroup,
      mapsetAttributeFormGroup: state.mapsetAttributeFormGroup,
      mapsetAttribute: state.mapsetAttribute,
      isEditMode: state.isEditMode,
      isNote: state.isNote,
      listOrganizationSelf: state.listOrganizationSelf,
      listTopicSatuPeta: state.listTopicSatuPeta,
      listClassification: state.listClassification,
      listMapsetProgram: state.listMapsetProgram,
      listRegional: state.listRegional,
      listOrganizationSatuPeta: state.listOrganizationSatuPeta,
      listOrganizationSub: state.listOrganizationSub,
      listOrganizationUnit: state.listOrganizationUnit,
      listMapsetSource: state.listMapsetSource,
      listMapsetType: state.listMapsetType,
      listMapsetAreaCoverage: state.listMapsetAreaCoverage,
      listMapsetGeoserver: state.listMapsetGeoserver,
      listMapsetGeonetworkMetadata: state.listMapsetGeonetworkMetadata,
      getMapsetFormGroup,
      getMapsetAttributeFormGroup,
      getMapsetAttribute,
      getIsEditMode,
      getIsNote,
      getListOrganizationSelf,
      getListTopicSatuPeta,
      getListClassification,
      getListMapsetProgram,
      getListRegional,
      getListOrganizationSatuPeta,
      getListOrganizationSub,
      getListOrganizationUnit,
      getListMapsetSource,
      getListMapsetType,
      getListMapsetAreaCoverage,
      getListMapsetGeoserver,
      getListMapsetGeonetworkMetadata,
    }),
  );

  // *********** Actions *********** //
  readonly getOrganizationSelf = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      switchMap(() => {
        const params = {
          where: JSON.stringify({
            kode_skpd: this.satudataUser.kode_skpd,
            is_deleted: false,
          }),
        };

        const options = {
          pagination: false,
        };

        return this.organizationService
          .getQueryList(params, options)
          .result$.pipe(
            tap({
              next: (result) => {
                this.setListOrganizationSelf(result);
              },
            }),
          );
      }),
    );
  });

  readonly getAllTopicSatuPeta = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      switchMap(() => {
        const params = {
          sort: `name:asc`,
          where: JSON.stringify({
            is_deleted: false,
          }),
        };

        const options = {
          pagination: false,
        };
        const app = 'satupeta';

        return this.topicService
          .getQueryList(params, options, app)
          .result$.pipe(
            tap({
              next: (result) => {
                this.setListTopicSatuPeta(result);
              },
            }),
          );
      }),
    );
  });

  readonly getAllClassification = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      switchMap(() => {
        const params = {
          sort: `name:asc`,
        };

        const options = {
          pagination: false,
        };

        return this.classificationService
          .getQueryList(params, options)
          .result$.pipe(
            tap({
              next: (result) => {
                this.setListClassification(result);
              },
            }),
          );
      }),
    );
  });

  readonly getAllMapsetProgram = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      switchMap(() => {
        const params = {
          sort: `name:asc`,
          where: JSON.stringify({
            is_active: true,
            is_deleted: false,
          }),
        };

        const options = {
          pagination: false,
        };

        return this.mapsetProgramService
          .getQueryList(params, options)
          .result$.pipe(
            tap({
              next: (result) => {
                this.setListMapsetProgram(result);
              },
            }),
          );
      }),
    );
  });

  readonly getAllRegional = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      switchMap(() => {
        const params = {
          sort: `nama_kemendagri:asc`,
          where: JSON.stringify({
            is_active: true,
            is_deleted: false,
          }),
        };

        const options = {
          pagination: false,
        };

        return this.regionalService.getQueryList(params, options).result$.pipe(
          tap({
            next: (result) => {
              this.setListRegional(result);
            },
          }),
        );
      }),
    );
  });

  readonly getAllOrganizationSatuPeta = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      switchMap(() => {
        const params = {
          sort: `nama_skpd:asc`,
          where: JSON.stringify({
            is_deleted: false,
          }),
        };

        const options = {
          pagination: false,
        };
        const app = 'satupeta';

        return this.organizationService
          .getQueryList(params, options, app)
          .result$.pipe(
            tap({
              next: (result) => {
                this.setListOrganizationSatuPeta(result);
              },
            }),
          );
      }),
    );
  });

  readonly getAllOrganizationSub = this.effect((code$: Observable<string>) => {
    return code$.pipe(
      switchMap((code) => {
        const params = {
          sort: `nama_skpdsub:asc`,
          where: JSON.stringify({
            kode_skpd: code,
          }),
        };

        const options = {
          pagination: false,
        };

        return this.organizationSubService
          .getQueryList(params, options)
          .result$.pipe(
            tap({
              next: (result) => {
                this.setListOrganizationSub(result);
              },
            }),
          );
      }),
    );
  });

  readonly getAllOrganizationUnit = this.effect((code$: Observable<string>) => {
    return code$.pipe(
      switchMap((code) => {
        const params = {
          sort: `nama_skpdunit:asc`,
          where: JSON.stringify({
            kode_skpdsub: code,
          }),
        };

        const options = {
          pagination: false,
        };

        return this.organizationUnitService
          .getQueryList(params, options)
          .result$.pipe(
            tap({
              next: (result) => {
                this.setListOrganizationUnit(result);
              },
            }),
          );
      }),
    );
  });

  readonly getAllMapsetSource = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      switchMap(() => {
        const params = {
          sort: `name:asc`,
        };

        const options = {
          pagination: false,
        };

        return this.mapsetSourceService
          .getQueryList(params, options)
          .result$.pipe(
            tap({
              next: (result) => {
                this.setListMapsetSource(result);
              },
            }),
          );
      }),
    );
  });

  readonly getAllMapsetType = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      switchMap(() => {
        const params = {
          sort: `name:asc`,
        };

        const options = {
          pagination: false,
        };

        return this.mapsetTypeService
          .getQueryList(params, options)
          .result$.pipe(
            tap({
              next: (result) => {
                this.setListMapsetType(result);
              },
            }),
          );
      }),
    );
  });

  readonly getAllMapsetAreaCoverage = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      switchMap(() => {
        const params = {
          sort: `name:asc`,
        };

        const options = {
          pagination: false,
        };

        return this.mapsetAreaCoverageService
          .getQueryList(params, options)
          .result$.pipe(
            tap({
              next: (result) => {
                this.setListMapsetAreaCoverage(result);
              },
            }),
          );
      }),
    );
  });

  readonly getAllMapsetGeoserver = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      switchMap(() => {
        return this.mapsetGeoserverService.getQueryListLayer().result$.pipe(
          tap({
            next: (result) => {
              if (result.isError) {
                Swal.fire({
                  type: 'error',
                  text: 'Gagal memuat data dari Geoserver',
                  allowOutsideClick: false,
                  showCancelButton: true,
                  cancelButtonText: 'Batal',
                  confirmButtonColor: '#0753A6',
                  confirmButtonText: 'Muat Ulang',
                  reverseButtons: true,
                }).then((response) => {
                  if (response.value) {
                    this.getAllMapsetGeoserver();
                  }
                });

                return;
              }

              this.setListMapsetGeoserver(result);
            },
          }),
        );
      }),
    );
  });

  readonly getAllMapsetGeonetworkMetadata = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      switchMap(() => {
        const params = {
          bucket: 'metadata',
        };

        const body = {
          query: {
            term: {
              valid: '1',
            },
          },
          size: 10000,
        };

        return this.mapsetGeonetworkMetadataService
          .getQueryList(params, body)
          .result$.pipe(
            tap({
              next: (result) => {
                if (result.isError) {
                  Swal.fire({
                    type: 'error',
                    text: 'Gagal memuat data dari Geonetwork',
                    allowOutsideClick: false,
                    showCancelButton: true,
                    cancelButtonText: 'Batal',
                    confirmButtonColor: '#0753A6',
                    confirmButtonText: 'Muat Ulang',
                    reverseButtons: true,
                  }).then((response) => {
                    if (response.value) {
                      this.getAllMapsetGeonetworkMetadata();
                    }
                  });

                  return;
                }

                this.setListMapsetGeonetworkMetadata(result);
              },
            }),
          );
      }),
    );
  });

  readonly getMapset = this.effect((id$: Observable<number>) => {
    return id$.pipe(
      withLatestFrom(
        this.getMapsetFormGroup$,
        this.getMapsetAttributeFormGroup$,
      ),
      switchMap(([id, formInfo]) => {
        return this.mapsetService.getQuerySingle(id, {}).result$.pipe(
          filter((val) => val.data !== undefined),
          tap({
            next: (result) => {
              // Form Info
              if (
                (result.data.is_validate === 2 ||
                  result.data.is_validate === 3) &&
                result.data.is_active &&
                !result.data.is_deleted
              ) {
                this.setIsNote(true);
                formInfo
                  .get('change')
                  .setValidators([
                    Validators.required,
                    Validators.minLength(15),
                  ]);
                formInfo.get('change').updateValueAndValidity();
              } else {
                this.setIsNote(false);
                formInfo.get('change').setValidators(null);
                formInfo.get('change').updateValueAndValidity();
              }

              formInfo
                .get('meta_mapservice')
                .setValidators([Validators.pattern(null)]);
              formInfo.get('meta_mapservice').updateValueAndValidity();

              let regionalType = 'province';
              if (result.data.regional.id !== 1) {
                regionalType = 'county';
              }

              let organizationSub = result.data.skpdsub;
              if (result.data.skpdsub.kode_skpdsub === null) {
                organizationSub = null;
              }

              let organizationUnit = result.data.skpdunit;
              if (result.data.skpdunit.kode_skpdunit === null) {
                organizationUnit = null;
              }

              formInfo.patchValue({
                info_name: result.data.name,
                info_description: result.data.description,
                info_scale: result.data.scale,
                info_topic: result.data.sektoral,
                info_classification: result.data.dataset_class,
                info_classification_access: result.data.access_organization,
                info_status_data: result.data.is_permanent.toString(),
                info_program: result.data.mapset_program_id
                  ? result.data.mapset_program
                  : null,
                info_regional_type: regionalType,
                info_regional: result.data.regional,
                info_organization: result.data.skpd,
                info_organization_sub: organizationSub,
                info_organization_unit: organizationUnit,
                info_organization_county: result.data.organization_manual,
                info_organization_address: result.data.owner_address,
                info_organization_phone: result.data.owner_telephone,
                info_organization_email: result.data.owner_email,
                info_status: result.data.is_active,
                info_popular: result.data.is_popular,
                meta_mapset_source: result.data.mapset_source,
                meta_mapservice: mapserviceNamespaceLayer(result.data.app_link),
                meta_mapservice_url: result.data.app_link,
                meta_file_xml: result.data.metadata_xml
                  ? result.data.metadata_xml
                  : null,
                meta_file_zip: result.data.shp,
                meta_mapset_type: result.data.mapset_type,
                meta_area_coverage: result.data.mapset_area_coverage_id
                  ? result.data.mapset_area_coverage
                  : null,
                meta_period_update: result.data.update_period,
                meta_version: result.data.revision_date,
              });

              // Mapset Source = Geoserver
              if (
                result.data.mapset_source.id ===
                SOURCE_MAPSET.get('geoserver').id
              ) {
                this.getAllMapsetGeoserver();
              }

              // Form Attribute
              const metadata = {};
              forEach(result.data.metadata, (item) => {
                assign(metadata, {
                  [item.key]: new UntypedFormControl(item.value),
                });
              });
              const form = new UntypedFormGroup(metadata);
              this.setMapsetAttributeFormGroup(form);
              this.setMapsetAttribute(Object.keys(metadata).map((key) => key));
            },
          }),
        );
      }),
    );
  });

  readonly resetForm = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      withLatestFrom(
        this.getMapsetFormGroup$,
        this.getMapsetAttributeFormGroup$,
      ),
      tap(([, formInfo, formAttribute]) => {
        formInfo.get('change').setValidators(null);
        formInfo.get('change').updateValueAndValidity();
        formInfo.reset();
        formInfo.get('info_status').setValue(true);
        formInfo.get('info_popular').setValue(false);

        formAttribute.reset();
        formAttribute.get('kdprv').setValue('Kode Provinsi');
        formAttribute.get('shape').setValue('SHAPE');
        formAttribute.get('objectid').setValue('Object ID');
      }),
    );
  });
}
