import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  inject,
} from '@angular/core';
import { Title } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { RouterModule, Router, ActivatedRoute } from '@angular/router';
import { CdkStepperModule } from '@angular/cdk/stepper';
import {
  FormsModule,
  ReactiveFormsModule,
  UntypedFormGroup,
} from '@angular/forms';

// STYLE
import * as jds from '@styles';
// COMPONENT
import { BreadcrumbComponent } from '@components-v4';
// CONSTANT
import { STATUS_MAPSET } from '@constants-v4';
// INTERFACE
import { Styles, Breadcrumb } from '@interfaces-v4';
// SERVICE
import { GlobalService, MapsetService } from '@services-v4';

import { assign, isNil } from 'lodash';
import { useMutationResult } from '@ngneat/query';
import { SubscribeDirective } from '@ngneat/subscribe';
import { NgxLoadingModule } from 'ngx-loading';
import Swal from 'sweetalert2';

import {
  MyMapsetFormComponent,
  MyMapsetFormAttributeComponent,
  MyMapsetPreviewComponent,
  MyMapsetStepperComponent,
} from '../../components';

import { MyMapsetStore } from '../../store/my-mapset.store';

@Component({
  selector: 'app-my-mapset-add',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    CdkStepperModule,
    FormsModule,
    ReactiveFormsModule,
    BreadcrumbComponent,
    MyMapsetFormComponent,
    MyMapsetFormAttributeComponent,
    MyMapsetPreviewComponent,
    MyMapsetStepperComponent,
    SubscribeDirective,
    NgxLoadingModule,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './my-mapset-edit.component.html',
  providers: [MyMapsetStore],
})
export class MyMapsetEditComponent {
  jds: Styles = jds;

  // Setting
  title: string;
  label = 'Ubah Mapset';
  breadcrumb: Breadcrumb[] = [
    { label: 'Data Saya', link: '/my-data' },
    { label: 'Ubah Mapset', link: '/my-mapset/edit' },
  ];

  // Service
  private cdRef = inject(ChangeDetectorRef);
  private router = inject(Router);
  private route = inject(ActivatedRoute);
  private globalService = inject(GlobalService);
  private titleService = inject(Title);
  public myMapsetStore = inject(MyMapsetStore);

  private mapsetService = inject(MapsetService);
  editMapsetMutation = useMutationResult();

  // Variable
  id: number;
  formGroup: UntypedFormGroup;
  param: any;

  constructor() {
    // id
    this.id = this.route.snapshot.params.id;

    this.settingsAll();
    this.queryParams();
    this.initStore();
    this.getMapset();
  }

  settingsAll(): void {
    this.title = this.globalService.title;
    this.titleService.setTitle(`${this.label} - ${this.title}`);
    this.globalService.changeLabel(this.label);
    this.globalService.changeBreadcrumb(this.breadcrumb);
  }

  queryParams(): void {
    this.route.queryParams.subscribe((p) => {
      const pPage = !isNil(p['page']) ? p['page'] : null;
      const pStatus = !isNil(p['status']) ? p['status'] : null;

      this.param = {
        page: pPage,
        status: pStatus,
      };
    });
  }

  initStore(): void {
    this.myMapsetStore.resetForm();
    this.myMapsetStore.setIsEditMode(true);
  }

  getMapset() {
    this.myMapsetStore.getMapset(this.id);
  }

  getValueMetadata(data: any) {
    const bodyMetadata = Object.keys(data.metadata).map((key) => {
      const value = data.metadata[key];

      return {
        key,
        value: !isNil(value) ? value : null,
        mapset_metadata_type_id: 6,
      };
    });

    return bodyMetadata;
  }

  onSubmit(data: any): void {
    const submited = data.event.submitter.name;

    let categoryHistory;
    if (data.event.submitter.name === 'draft') {
      categoryHistory = null;
    } else {
      switch (this.param.status) {
        case 'draft-active':
          categoryHistory = 'new-active';
          break;
        case 'new-active':
          categoryHistory = null;
          break;
        case 'revision-active-new':
          categoryHistory = 'new-active';
          break;
        case 'revision-active-edit':
          categoryHistory = 'edit-active';
          break;
        case 'approve-active':
          categoryHistory = 'edit-active';
          break;
        case 'edit-active':
          categoryHistory = null;
          break;
        default:
          categoryHistory = null;
          break;
      }
    }

    const bodyMapset = {
      name: data.info_name,
      description: data.info_description,
      scale: data.info_scale,
      sektoral_id: data.info_topic.id,
      dataset_class_id: data.info_classification.id,
      is_permanent: data.info_status_data !== 'false',
      mapset_program_id: data.info_program ? data.info_program.id : null,
      regional_id: data.info_regional.id,
      owner_address: data.info_organization_address,
      owner_telephone: data.info_organization_phone,
      owner_email: data.info_organization_email,
      is_popular: data.info_popular,
      mapset_source_id: data.meta_mapset_source.id,
      app_link: data.meta_mapservice_url,
      metadata_xml: data.meta_file_xml ? data.meta_file_xml : null,
      shp: data.meta_file_zip,
      mapset_type_id: data.meta_mapset_type.id,
      mapset_area_coverage_id: data.meta_area_coverage
        ? data.meta_area_coverage.id
        : null,
      update_period: data.meta_period_update,
      revision_date: data.meta_version,
    };

    // History
    if (categoryHistory) {
      const bodyHistory = {
        category: categoryHistory,
        notes: data.change,
      };
      assign(bodyMapset, { history_draft: bodyHistory });
    }

    // Metadata
    const bodyMetadata = this.getValueMetadata(data);
    assign(bodyMapset, { metadata: bodyMetadata });

    // Classification Access
    if (data.info_classification === 4) {
      const accessPrivate = data.info_classification_access
        .map((item) => {
          return item.kode_skpd;
        })
        .join();

      assign(bodyMapset, {
        access_organization: accessPrivate,
      });
    }

    // Organization
    if (data.info_regional_type === 'province') {
      assign(bodyMapset, {
        skpd_id: data.info_organization.id,
        kode_skpd: data.info_organization.kode_skpd,
        kode_skpdsub: data.info_organization_sub
          ? data.info_organization_sub.kode_skpd
          : null,
        kode_skpdunit: data.info_organization_unit
          ? data.info_organization_unit.kode_skpd
          : null,
        owner: data.info_organization.nama_skpd,
      });
    } else {
      assign(bodyMapset, {
        owner: data.info_organization_county,
        organization_manual: data.info_organization_county,
      });
    }

    // Status
    if (categoryHistory) {
      assign(bodyMapset, STATUS_MAPSET.get(categoryHistory));
    }

    this.mapsetService
      .updateItem(this.id, bodyMapset)
      .pipe(this.editMapsetMutation.track())
      .subscribe(() => {
        let from;
        let navigation;
        let queryParams;
        if (submited === 'draft') {
          from = 'disimpan sebagai draf';
          navigation = '/my-data';
          queryParams = { catalog: 'mapset', status: 'draft' };
        } else {
          from = 'diubah';
          navigation = '/desk';

          switch (this.param.status) {
            case 'draft-active':
            case 'new-active':
            case 'revision-active-new':
              queryParams = {
                type: 'upload-change',
                status: 'verification',
              };
              break;
            case 'approve-active':
            case 'edit-active':
            case 'revision-active-edit':
              queryParams = {
                type: 'upload-change',
                status: 'verification',
                statusVerification: 'edit',
              };
              break;
            default:
              break;
          }
        }

        Swal.fire({
          type: 'success',
          text: `Mapset berhasil ${from}.`,
          allowOutsideClick: false,
        }).then(() => {
          this.router.navigate([navigation], { queryParams });
        });
      });
  }
}
