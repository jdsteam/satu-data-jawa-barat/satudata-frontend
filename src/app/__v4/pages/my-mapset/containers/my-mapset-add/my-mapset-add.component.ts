import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  inject,
} from '@angular/core';
import { Title } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { RouterModule, Router } from '@angular/router';
import { CdkStepperModule } from '@angular/cdk/stepper';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// STYLE
import * as jds from '@styles';
// COMPONENT
import { BreadcrumbComponent } from '@components-v4';
// CONSTANT
import { STATUS_MAPSET } from '@constants-v4';
// INTERFACE
import { Styles, Breadcrumb } from '@interfaces-v4';
// SERVICE
import { GlobalService, MapsetService } from '@services-v4';

import { assign, isNil } from 'lodash';
import { useMutationResult } from '@ngneat/query';
import { SubscribeDirective } from '@ngneat/subscribe';
import { NgxLoadingModule } from 'ngx-loading';
import Swal from 'sweetalert2';

import {
  MyMapsetFormComponent,
  MyMapsetFormAttributeComponent,
  MyMapsetPreviewComponent,
  MyMapsetStepperComponent,
} from '../../components';

import { MyMapsetStore } from '../../store/my-mapset.store';

@Component({
  selector: 'app-my-mapset-add',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    CdkStepperModule,
    FormsModule,
    ReactiveFormsModule,
    BreadcrumbComponent,
    MyMapsetFormComponent,
    MyMapsetFormAttributeComponent,
    MyMapsetPreviewComponent,
    MyMapsetStepperComponent,
    SubscribeDirective,
    NgxLoadingModule,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './my-mapset-add.component.html',
  providers: [MyMapsetStore],
})
export class MyMapsetAddComponent {
  jds: Styles = jds;

  // Setting
  title: string;
  label = 'Tambah Mapset';
  breadcrumb: Breadcrumb[] = [
    { label: 'Data Saya', link: '/my-data' },
    { label: 'Tambah Mapset', link: '/my-mapset/add' },
  ];

  // Service
  private cdRef = inject(ChangeDetectorRef);
  private router = inject(Router);
  private globalService = inject(GlobalService);
  private titleService = inject(Title);
  public myMapsetStore = inject(MyMapsetStore);

  private mapsetService = inject(MapsetService);
  addMapsetMutation = useMutationResult();

  constructor() {
    this.settingsAll();
    this.initStore();
  }

  settingsAll(): void {
    this.title = this.globalService.title;
    this.titleService.setTitle(`${this.label} - ${this.title}`);
    this.globalService.changeLabel(this.label);
    this.globalService.changeBreadcrumb(this.breadcrumb);
  }

  initStore(): void {
    this.myMapsetStore.resetForm();
    this.myMapsetStore.setIsEditMode(false);
  }

  getValueMetadata(data: any) {
    const bodyMetadata = Object.keys(data.metadata).map((key) => {
      const value = data.metadata[key];

      return {
        key,
        value: !isNil(value) ? value : null,
        mapset_metadata_type_id: 6,
      };
    });

    return bodyMetadata;
  }

  onSubmit(data: any): void {
    const submited = data.event.submitter.name;

    let categoryHistory;
    if (data.event.submitter.name === 'draft') {
      categoryHistory = 'draft-active';
    } else {
      categoryHistory = 'new-active';
    }

    const bodyMapset = {
      name: data.info_name,
      description: data.info_description,
      scale: data.info_scale,
      sektoral_id: data.info_topic.id,
      dataset_class_id: data.info_classification.id,
      is_permanent: data.info_status_data !== 'false',
      mapset_program_id: data.info_program ? data.info_program.id : null,
      regional_id: data.info_regional.id,
      owner_address: data.info_organization_address,
      owner_telephone: data.info_organization_phone,
      owner_email: data.info_organization_email,
      is_popular: data.info_popular,
      mapset_source_id: data.meta_mapset_source.id,
      app_link: data.meta_mapservice_url,
      metadata_xml: data.meta_file_xml ? data.meta_file_xml : null,
      shp: data.meta_file_zip,
      mapset_type_id: data.meta_mapset_type.id,
      mapset_area_coverage_id: data.meta_area_coverage
        ? data.meta_area_coverage.id
        : null,
      update_period: data.meta_period_update,
      revision_date: data.meta_version,
    };

    // History
    const bodyHistory = {
      category: categoryHistory,
      notes: null,
    };
    assign(bodyMapset, { history_draft: bodyHistory });

    // Metadata
    const bodyMetadata = this.getValueMetadata(data);
    assign(bodyMapset, { metadata: bodyMetadata });

    // Classification Access
    if (data.info_classification === 4) {
      const accessPrivate = data.info_classification_access
        .map((item) => {
          return item.kode_skpd;
        })
        .join();

      assign(bodyMapset, {
        access_organization: accessPrivate,
      });
    }

    // Organization
    if (data.info_regional_type === 'province') {
      assign(bodyMapset, {
        skpd_id: data.info_organization.id,
        kode_skpd: data.info_organization.kode_skpd,
        kode_skpdsub: data.info_organization_sub
          ? data.info_organization_sub.kode_skpd
          : null,
        kode_skpdunit: data.info_organization_unit
          ? data.info_organization_unit.kode_skpd
          : null,
        owner: data.info_organization.nama_skpd,
      });
    } else {
      assign(bodyMapset, {
        owner: data.info_organization_county,
        organization_manual: data.info_organization_county,
      });
    }

    // Status
    assign(bodyMapset, STATUS_MAPSET.get(categoryHistory));

    this.mapsetService
      .createItem(bodyMapset)
      .pipe(this.addMapsetMutation.track())
      .subscribe(() => {
        let from;
        let navigation;
        let queryParams;
        switch (submited) {
          case 'draft':
            from = 'disimpan sebagai draf';
            navigation = '/my-data';
            queryParams = { catalog: 'mapset', status: 'draft' };
            break;
          default:
            from = 'ditambahkan';
            navigation = '/desk';
            queryParams = {
              type: 'upload-change',
              status: 'verification',
            };
            break;
        }

        Swal.fire({
          type: 'success',
          text: `Mapset berhasil ${from}.`,
          allowOutsideClick: false,
        }).then(() => {
          this.router.navigate([navigation], { queryParams });
        });
      });
  }
}
