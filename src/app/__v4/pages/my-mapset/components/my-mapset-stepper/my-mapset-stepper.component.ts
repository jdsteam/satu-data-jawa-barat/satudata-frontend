import { Component, EventEmitter, inject, OnInit, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { UntypedFormGroup } from '@angular/forms';
import { CdkStepperModule, CdkStepper } from '@angular/cdk/stepper';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// COMPONENT
import { StepperFormComponent } from '@components-v4';
// JDS-BI
import { JdsBiButtonModule } from '@jds-bi/core';

import { isNil } from 'lodash';

import { MyMapsetStore } from '../../store/my-mapset.store';

@Component({
  selector: 'app-my-mapset-stepper',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    CdkStepperModule,
    JdsBiButtonModule,
    StepperFormComponent,
  ],
  // changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './my-mapset-stepper.component.html',
  providers: [{ provide: CdkStepper, useExisting: MyMapsetStepperComponent }],
})
export class MyMapsetStepperComponent extends CdkStepper implements OnInit {
  colorScheme = COLORSCHEME;
  jds: Styles = jds;

  // Store
  private route = inject(ActivatedRoute);
  private myMapsetStore = inject(MyMapsetStore);
  readonly vm$ = this.myMapsetStore.vm$;

  // Emitter
  @Output() handleAddSubmit = new EventEmitter();
  @Output() handleEditSubmit = new EventEmitter();

  // Variable
  mapsetFormGroup: UntypedFormGroup;
  mapsetAttributeFormGroup: UntypedFormGroup;
  isEditMode: boolean;
  param: any;

  stepper = ['Masukkan Mapset', 'Atribut Mapset', 'Ringkasan Mapset'];

  ngOnInit() {
    this.queryParams();

    this.myMapsetStore.getMapsetFormGroup$.subscribe(
      (value: UntypedFormGroup) => {
        this.mapsetFormGroup = value;
      },
    );

    this.myMapsetStore.getMapsetAttributeFormGroup$.subscribe(
      (value: UntypedFormGroup) => {
        this.mapsetAttributeFormGroup = value;
      },
    );

    this.myMapsetStore.getIsEditMode$.subscribe((value: boolean) => {
      this.isEditMode = value;
    });
  }

  queryParams(): void {
    this.route.queryParams.subscribe((p) => {
      const pPage = !isNil(p['page']) ? p['page'] : null;
      const pStatus = !isNil(p['status']) ? p['status'] : null;

      this.param = {
        page: pPage,
        status: pStatus,
      };
    });
  }

  selectStepByIndex(index: number): void {
    this.selectedIndex = index;
  }

  onSubmit(event: any): void {
    if (this.mapsetFormGroup.valid && this.mapsetAttributeFormGroup.valid) {
      const data = {
        event: { submitter: { name: event } },
        ...this.mapsetFormGroup.value,
        metadata: this.mapsetAttributeFormGroup.value,
      };

      if (!this.isEditMode) {
        this.handleAddSubmit.emit(data);
      } else {
        this.handleEditSubmit.emit(data);
      }
    }
  }
}
