import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  inject,
  OnInit,
} from '@angular/core';
import { CommonModule, ViewportScroller } from '@angular/common';
import {
  FormsModule,
  ReactiveFormsModule,
  UntypedFormControl,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { HttpEventType } from '@angular/common/http';
// eslint-disable-next-line object-curly-newline
import { catchError, concatMap, from, mergeMap, of, toArray } from 'rxjs';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME, SOURCE_MAPSET } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// MODEL
import { LoginData } from '@models-v3';
// PIPE
import { MapsetFileZIPPipe } from '@pipes-v4';
// SERVICE
import { AuthenticationService } from '@services-v3';
import {
  MapsetGeoserverService,
  MapsetGeoserverUploadService,
  MapsetArcgisUploadService,
  UploadService,
} from '@services-v4';
// UTIL
import { isValidHttpUrl } from '@utils-v4';
// JDS-BI
import { JdsBiAccordionModule, JdsBiInputModule } from '@jds-bi/core';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconCircleInfo,
} from '@jds-bi/icons';
// PLUGINS
import { assign, forEach, isNil } from 'lodash';
import { LetDirective } from '@ngrx/component';
import { SubscribeDirective } from '@ngneat/subscribe';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import * as CKEditor from '@scripts/ckeditor-5/ckeditor';
import { NgSelectModule } from '@ng-select/ng-select';

// STORE
import { DEFAULT_STATE, MyMapsetStore } from '../../store/my-mapset.store';

@Component({
  selector: 'app-my-mapset-form',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MapsetFileZIPPipe,
    LetDirective,
    SubscribeDirective,
    CKEditorModule,
    NgSelectModule,
    JdsBiAccordionModule,
    JdsBiInputModule,
    JdsBiIconsModule,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './my-mapset-form.component.html',
  styleUrls: ['./my-mapset-form.component.scss'],
})
export class MyMapsetFormComponent implements OnInit {
  env = environment;
  satudataUser: LoginData;
  colorScheme = COLORSCHEME;
  jds: Styles = jds;
  public Editor = CKEditor;

  // Service
  private cdRef = inject(ChangeDetectorRef);
  private viewportScroller = inject(ViewportScroller);
  private authenticationService = inject(AuthenticationService);
  private mapsetGeoserverService = inject(MapsetGeoserverService);
  private mapsetGeoserverUploadService = inject(MapsetGeoserverUploadService);
  private mapsetArcgisUploadService = inject(MapsetArcgisUploadService);
  private uploadService = inject(UploadService);

  // Store
  private myMapsetStore = inject(MyMapsetStore);
  readonly vm$ = this.myMapsetStore.vm$;

  // Editor
  EditorConfig = {
    toolbar: [
      'Bold',
      'Italic',
      'Underline',
      'Strikethrough',
      '|',
      'Link',
      '|',
      'Outdent',
      'Indent',
      '|',
      'BulletedList',
      'NumberedList',
      '|',
      'Undo',
      'Redo',
    ],
    htmlSupport: {
      allow: [
        {
          name: 'span',
          attributes: true,
        },
      ],
    },
  };

  // Variable
  formGroup: UntypedFormGroup;
  formGroupAttribute: UntypedFormGroup;
  isClassification = false;
  isClassificationAccess = false;
  mapsetSourceGeoserver = SOURCE_MAPSET.get('geoserver').id;
  mapsetSourceArcgis = SOURCE_MAPSET.get('arcgis').id;
  mapsetMapserviceInvalid = null;

  constructor(private iconService: JdsBiIconsService) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    this.myMapsetStore.getMapsetFormGroup$.subscribe(
      (value: UntypedFormGroup) => {
        this.formGroup = value;
      },
    );

    this.myMapsetStore.getMapsetAttributeFormGroup$.subscribe(
      (value: UntypedFormGroup) => {
        this.formGroupAttribute = value;
      },
    );

    this.iconService.registerIcons([iconCircleInfo]);
  }

  ngOnInit() {
    if (this.satudataUser.role_name === 'opd') {
      this.getSelfOrganization();
    }

    this.getAllTopicSatuPeta();
    this.getAllClassification();
    this.getAllMapsetProgram();
    this.getAllOrganizationSatuPeta();
    this.getAllRegional();
    this.getAllMapsetSource();
    this.getAllMapsetType();
    this.getAllMapsetAreaCoverage();
    this.getAllMapsetGeonetworkMetadata();
  }

  // GET =======================================================================
  get f() {
    return this.formGroup.controls;
  }

  getSelfOrganization(): void {
    this.myMapsetStore.getOrganizationSelf();

    this.myMapsetStore.getListOrganizationSelf$.subscribe((result: any) => {
      if (!isNil(result.data)) {
        const data = result.data.list[0];

        this.f.info_regional.setValue(data.regional);
        this.onSelectedRegional(data.regional);

        this.f.info_organization.setValue(data);
        this.onSelectedOrganization(data);
      }
    });
  }

  getAllTopicSatuPeta() {
    this.myMapsetStore.getAllTopicSatuPeta();
  }

  getAllClassification() {
    this.myMapsetStore.getAllClassification();
  }

  getAllMapsetProgram() {
    this.myMapsetStore.getAllMapsetProgram();
  }

  getAllOrganizationSatuPeta() {
    this.myMapsetStore.getAllOrganizationSatuPeta();
  }

  getAllOrganizationSub(kodeSkpd: string) {
    this.myMapsetStore.getAllOrganizationSub(kodeSkpd);
  }

  getAllOrganizationUnit(kodeSkpdSub: string) {
    this.myMapsetStore.getAllOrganizationUnit(kodeSkpdSub);
  }

  getAllRegional() {
    this.myMapsetStore.getAllRegional();
  }

  getAllMapsetSource() {
    this.myMapsetStore.getAllMapsetSource();
  }

  getAllMapsetType() {
    this.myMapsetStore.getAllMapsetType();
  }

  getAllMapsetAreaCoverage() {
    this.myMapsetStore.getAllMapsetAreaCoverage();
  }

  getAllMapsetGeoserver() {
    this.myMapsetStore.getAllMapsetGeoserver();
  }

  getAllMapsetGeonetworkMetadata() {
    this.myMapsetStore.getAllMapsetGeonetworkMetadata();
  }

  // ONCHANGE ==================================================================
  onSelectedClassification(value) {
    if (isNil(value)) {
      return;
    }

    this.isClassification = false;

    if (value && value?.id === 4) {
      this.isClassificationAccess = true;
      this.f.info_classification_access.setValidators([Validators.required]);
      this.f.info_classification_access.updateValueAndValidity();
    } else {
      this.isClassificationAccess = false;
      this.f.info_classification_access.removeValidators([Validators.required]);
      this.f.info_classification_access.updateValueAndValidity();
      this.f.info_classification_access.setValue(null);
    }
  }

  onClearClassification() {
    this.isClassification = false;
  }

  onSelectedRegional(value) {
    if (isNil(value)) {
      return;
    }

    this.onClearRegional();

    if (value.kode_kemendagri === '32') {
      this.f.info_regional_type.setValue('province');
      this.f.info_organization.setValidators(Validators.required);
      this.f.info_organization.updateValueAndValidity();
    } else {
      this.f.info_regional_type.setValue('county');
      this.f.info_organization.removeValidators(Validators.required);
      this.f.info_organization.updateValueAndValidity();
      this.f.info_organization.setValue(null);
    }
  }

  onClearRegional() {
    this.f.info_regional_type.reset();
    this.f.info_organization.reset();
    this.f.info_organization_sub.reset();
    this.f.info_organization_unit.reset();
    this.f.info_organization_address.reset();
    this.f.info_organization_phone.reset();
    this.f.info_organization_email.reset();
  }

  onSelectedOrganization(value) {
    if (isNil(value)) {
      return;
    }

    this.f.info_organization_sub.reset();
    this.f.info_organization_unit.reset();
    this.getAllOrganizationSub(value.kode_skpd);

    this.f.info_organization_address.setValue(value.address);
    this.f.info_organization_phone.setValue(value.phone);
    this.f.info_organization_email.setValue(value.email);
  }

  onClearOrganization() {
    this.f.info_organization_sub.reset();
    this.f.info_organization_unit.reset();
    this.f.info_organization_address.reset();
    this.f.info_organization_phone.reset();
    this.f.info_organization_email.reset();
  }

  onSelectedOrganizationSub(value) {
    if (isNil(value)) {
      return;
    }

    this.f.info_organization_unit.reset();
    this.getAllOrganizationUnit(value.kode_skpdsub);
  }

  onClearOrganizationSub() {
    this.f.info_organization_unit.reset();
  }

  onSelectedSource(value) {
    if (isNil(value)) {
      return;
    }

    let form;

    // Source = Geo Server
    if (value.id === this.mapsetSourceGeoserver) {
      this.getAllMapsetGeoserver();
      form = new UntypedFormGroup({});
    } else {
      form = DEFAULT_STATE.mapsetAttributeFormGroup;
    }

    this.myMapsetStore.setMapsetAttributeFormGroup(form);
    this.f.meta_mapservice.reset();
    this.f.meta_file_zip.reset();
  }

  onClearSource() {
    const form = DEFAULT_STATE.mapsetAttributeFormGroup;

    this.myMapsetStore.setMapsetAttributeFormGroup(form);
    this.f.meta_mapservice.reset();
    this.f.meta_file_zip.reset();
  }

  onSelectedGeoserver(value) {
    if (isNil(value)) return;

    const layer = { title: value.title };

    this.mapsetGeoserverService
      .getListWorkspace(layer)
      .pipe(
        concatMap((response1) => {
          return from(response1).pipe(
            mergeMap((data) => {
              const body = { title: layer.title, dataStore: data.name };

              return this.mapsetGeoserverService
                .getListWorkspaceFeature(body)
                .pipe(
                  catchError(() => {
                    return of(null);
                  }),
                );
            }),
            toArray(),
          );
        }),
        concatMap((response2) => {
          const urls = response2.filter((el) => el !== null);

          const body = {
            mapset_wms_url: urls[0],
          };

          return this.mapsetGeoserverUploadService.uploadFile(body);
        }),
      )
      .subscribe({
        next: (response) => {
          if (response.type === HttpEventType.Response) {
            if (response.body.error === 1) {
              this.mapsetMapserviceInvalid = true;
              this.f.meta_mapservice.setErrors({ mapservice: true });
              this.f.meta_mapservice.markAsDirty();
              this.cdRef.detectChanges();
            } else {
              // Form Info
              this.formGroup.patchValue({
                meta_mapservice_url: response.body.data.mapset_wms_url,
                meta_file_zip: response.body.data.mapset_shp_url,
              });

              // Form Attribute
              const metadata = {};
              forEach(response.body.data.metadata_kugi, (item) => {
                assign(metadata, {
                  [item.key]: new UntypedFormControl(item.value, [
                    Validators.maxLength(255),
                  ]),
                });
              });
              const form = new UntypedFormGroup(metadata);
              this.myMapsetStore.setMapsetAttributeFormGroup(form);
              this.myMapsetStore.setMapsetAttribute(
                response.body.data.metadata_kugi.map((item) => item.key),
              );

              this.mapsetMapserviceInvalid = false;
              this.f.meta_mapservice.clearValidators();
              this.f.meta_mapservice.markAsPristine();
              this.cdRef.detectChanges();
            }
          }
        },
        error: () => {
          this.mapsetMapserviceInvalid = true;
          this.f.meta_mapservice.setErrors({ mapservice: true });
          this.f.meta_mapservice.markAsDirty();
          this.cdRef.detectChanges();
        },
      });
  }

  onClearGeoserver() {
    const form = DEFAULT_STATE.mapsetAttributeFormGroup;

    this.myMapsetStore.setMapsetAttributeFormGroup(form);
    this.mapsetMapserviceInvalid = null;
    this.f['meta_file_zip'].reset();
  }

  // TODO: remove this function when mapset arcgis is not required
  onBlurMapservice(value: string) {
    const source = this.f.meta_mapset_source.value;
    const isValid = this.f.meta_mapservice.valid;

    if (!value) return;

    if (!source) {
      this.f.meta_mapservice.setErrors({ source: true });
      this.f.meta_mapservice.markAsDirty();
      this.cdRef.detectChanges();
      return;
    }

    if (!isValid) return;

    const body = {
      mapset_wms_url: value,
    };

    const upload = (service, data) => {
      service.uploadFile(data).subscribe({
        next: (response) => {
          if (response.type === HttpEventType.Response) {
            if (response.body.error === 1) {
              this.mapsetMapserviceInvalid = true;
              this.f.meta_mapservice.setErrors({ mapservice: true });
              this.f.meta_mapservice.markAsDirty();
              this.cdRef.detectChanges();
            } else {
              // Form Info
              this.formGroup.patchValue({
                meta_file_zip: response.body.data.mapset_shp_url,
              });

              // Form Attribute
              const metadata = {};
              forEach(response.body.data.metadata_kugi, (item) => {
                assign(metadata, {
                  [item.key]: new UntypedFormControl(item.value, [
                    Validators.maxLength(255),
                  ]),
                });
              });
              const form = new UntypedFormGroup(metadata);
              this.myMapsetStore.setMapsetAttributeFormGroup(form);
              this.myMapsetStore.setMapsetAttribute(
                response.body.data.metadata_kugi.map((item) => item.key),
              );

              this.mapsetMapserviceInvalid = false;
              this.f.meta_mapservice.clearValidators();
              this.f.meta_mapservice.markAsPristine();
              this.cdRef.detectChanges();
            }
          }
        },
        error: () => {
          this.mapsetMapserviceInvalid = true;
          this.f.meta_mapservice.setErrors({ mapservice: true });
          this.f.meta_mapservice.markAsDirty();
          this.cdRef.detectChanges();
        },
      });
    };

    switch (source.id) {
      case this.mapsetSourceGeoserver:
        upload(this.mapsetGeoserverUploadService, body);
        break;
      case this.mapsetSourceArcgis:
        upload(this.mapsetArcgisUploadService, body);
        break;
      default:
        break;
    }
  }

  onDownloadSHP() {
    const { meta_file_zip: metaFileZIP, meta_mapset_source: metaMapsetSource } =
      this.f;
    const shp = metaFileZIP.value;
    const mapsetSource = metaMapsetSource.value;

    let url = shp;
    let link = 'original';

    if (!isValidHttpUrl(shp)) {
      url = environment.backendURL + shp;
      link = 'modify';
    }

    if (link === 'original' && mapsetSource.id === this.mapsetSourceGeoserver) {
      const filename = `${new Date().valueOf()}-shp.zip`;
      this.onDownloadGeoserverSHP(url, filename);
    } else {
      window.open(url, '_blank');
    }
  }

  onDownloadGeoserverSHP(url: string, filename: string) {
    this.mapsetGeoserverService.getFileSHP(url).subscribe((blob) => {
      const downloadUrl = URL.createObjectURL(blob);

      const link = document.createElement('a');
      link.href = downloadUrl;
      link.download = filename;
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);

      URL.revokeObjectURL(downloadUrl);
    });
  }

  // POST ======================================================================
  onUpload(field: string, event: any) {
    const fileList: FileList = event.target.files;

    if (fileList.length > 0) {
      const file: File = fileList[0];
      const myFormUpload: FormData = new FormData();
      myFormUpload.append('file', file, file.name);

      this.uploadService.uploadFile(myFormUpload).subscribe((response) => {
        if (response.type === HttpEventType.Response) {
          this.formGroup.patchValue({
            [field]: response.body.data.url,
          });

          this.cdRef.detectChanges();
        }
      });
    }
  }

  // ADDITIONAL ================================================================
  panelCollapse(to: string) {
    this.viewportScroller.setOffset([0, 100]);

    setTimeout(() => {
      this.viewportScroller.scrollToAnchor(to);
    }, 500);
  }
}
