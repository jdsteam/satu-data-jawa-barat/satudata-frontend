import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// JDS-BI
import { JdsBiAccordionModule, JdsBiInputModule } from '@jds-bi/core';

import { LetDirective } from '@ngrx/component';

import { MyMapsetStore } from '../../store/my-mapset.store';

@Component({
  selector: 'app-my-mapset-form-attribute',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    LetDirective,
    JdsBiAccordionModule,
    JdsBiInputModule,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './my-mapset-form-attribute.component.html',
  styleUrls: ['./my-mapset-form-attribute.component.scss'],
})
export class MyMapsetFormAttributeComponent {
  colorScheme = COLORSCHEME;
  jds: Styles = jds;

  // Store
  private myMapsetStore = inject(MyMapsetStore);
  readonly vm$ = this.myMapsetStore.vm$;

  beforeChange($event: any) {
    if ($event.panelId === 'accordion-mapset-create-attribute') {
      $event.preventDefault();
    }
  }

  getFormAttribute(value) {
    return Object.keys(value).map((key) => ({ key, value: value[key] }));
  }
}
