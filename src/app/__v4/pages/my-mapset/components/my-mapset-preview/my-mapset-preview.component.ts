import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// SERVICE
import { MapsetService } from '@services-v4';

// JDS-BI
import { jdsBiColor } from '@jds-bi/cdk';
import { JdsBiAccordionModule, JdsBiBadgeModule } from '@jds-bi/core';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconArrowUpRightFromSquare,
  iconCloudArrowDown,
  iconCircleCheck,
  iconFileTimer,
} from '@jds-bi/icons';
import {
  MapsetMetadataXMLPipe,
  MapsetFileZIPPipe,
  SafeHtmlPipe,
} from '@pipes-v4';

import { LetDirective } from '@ngrx/component';

// STORE
import { MyMapsetStore } from '../../store/my-mapset.store';

@Component({
  selector: 'app-my-mapset-preview',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    LetDirective,
    JdsBiAccordionModule,
    JdsBiBadgeModule,
    JdsBiIconsModule,
    MapsetMetadataXMLPipe,
    MapsetFileZIPPipe,
    SafeHtmlPipe,
  ],
  changeDetection: ChangeDetectionStrategy.Default,
  templateUrl: './my-mapset-preview.component.html',
  styleUrls: ['./my-mapset-preview.component.scss'],
})
export class MyMapsetPreviewComponent {
  env = environment;
  colorScheme = COLORSCHEME;
  jds: Styles = jds;
  jdsBiColor = jdsBiColor;

  // Store
  private myMapsetStore = inject(MyMapsetStore);
  readonly vm$ = this.myMapsetStore.vm$;

  // Service
  private mapsetService = inject(MapsetService);

  constructor(private iconService: JdsBiIconsService) {
    this.iconService.registerIcons([
      iconArrowUpRightFromSquare,
      iconCloudArrowDown,
      iconCircleCheck,
      iconFileTimer,
    ]);
  }

  getStatusData(value: boolean) {
    return this.mapsetService.getStatusData(value);
  }

  getRegional(value: any) {
    let regional = 'province';

    if (value && value.kode_kemendagri !== '32') {
      regional = 'county';
    }

    return regional;
  }

  beforeChange($event: any) {
    switch ($event.panelId) {
      case 'accordion-mapset-preview-information':
      case 'accordion-mapset-preview-metadata':
      case 'accordion-mapset-preview-attribute':
        $event.preventDefault();
        break;
      default:
        break;
    }
  }

  getFormAttribute(value) {
    return Object.keys(value).map((key) => ({ key, value: value[key] }));
  }
}
