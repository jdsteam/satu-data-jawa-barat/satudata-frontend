import { inject } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivateFn, Routes } from '@angular/router';

// GUARD
import { MyMapsetAddService } from './containers/my-mapset-add/my-mapset-add.service';
import { MyMapsetEditService } from './containers/my-mapset-edit/my-mapset-edit.service';

const canActivateMyMapsetAdd: CanActivateFn = () => {
  return inject(MyMapsetAddService).canActivate();
};

const canActivateMyMapsetEdit: CanActivateFn = (
  route: ActivatedRouteSnapshot,
) => {
  return inject(MyMapsetEditService).canActivate(route.params.id);
};

export const MY_MAPSET_ROUTES: Routes = [
  {
    path: '',
    children: [
      {
        path: 'add',
        canActivate: [canActivateMyMapsetAdd],
        loadComponent: () =>
          import('./containers/my-mapset-add/my-mapset-add.component').then(
            (m) => m.MyMapsetAddComponent,
          ),
      },
      {
        path: 'edit/:id',
        canActivate: [canActivateMyMapsetEdit],
        loadComponent: () =>
          import('./containers/my-mapset-edit/my-mapset-edit.component').then(
            (m) => m.MyMapsetEditComponent,
          ),
      },
    ],
  },
];
