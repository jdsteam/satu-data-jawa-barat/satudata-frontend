import { Component, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';

// COMPONENT
import { AgreementComponent as ModalAgreementComponent } from '@components-v3/modal/agreement/agreement.component';
// SERVICES
import { AuthenticationService, StateService } from '@services-v3';

import { NgxLoadingModule } from 'ngx-loading';

@Component({
  selector: 'app-autologin-digiteam',
  standalone: true,
  imports: [CommonModule, NgxLoadingModule, ModalAgreementComponent],
  template: `
    <ngx-loading [show]="loading" />
    <app-modal-agreement></app-modal-agreement>
  `,
})
export class AutologinDigiteamPageComponent {
  @ViewChild(ModalAgreementComponent)
  modalAgreementComponent: ModalAgreementComponent;

  // Variable
  loading = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private stateService: StateService,
  ) {
    const { code } = this.route.snapshot.queryParams;

    this.loading = true;
    this.authenticationService
      .verifyTokenSSODigiteam(code)
      .pipe(first())
      .subscribe(
        (result) => {
          this.loading = false;

          if (result.error === 0) {
            if (!result.data.is_agree) {
              this.stateService.changeAuthenticationLogin(result.data);
              this.authenticationService.setToken(result.data.jwt);
              this.modalAgreementComponent.openModal();
            } else {
              this.authenticationService.setSession(result.data);
              this.router.navigate(['/home']);
            }
          } else {
            this.router.navigate(['/auth/login'], {
              state: {
                errorType: 'access',
                error: 'Username dan Kata Sandi tidak ditemukan.',
              },
            });
          }
        },
        (error) => {
          this.loading = false;
          this.router.navigate(['/auth/login'], {
            state: { errorType: 'api', error: error.message },
          });
        },
      );
  }
}
