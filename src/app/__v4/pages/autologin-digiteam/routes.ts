import { Routes } from '@angular/router';

export const AUTOLOGIN_DIGITEAM_ROUTES: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        loadComponent: () =>
          import(
            './containers/autologin-digiteam-page/autologin-digiteam-page.component'
          ).then((m) => m.AutologinDigiteamPageComponent),
      },
    ],
  },
];
