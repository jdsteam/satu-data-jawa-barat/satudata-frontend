import { Routes } from '@angular/router';

export const MY_DATA_ROUTES: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        loadComponent: () =>
          import('./containers/my-data-page/my-data-page.component').then(
            (m) => m.MyDataPageComponent,
          ),
      },
    ],
  },
];
