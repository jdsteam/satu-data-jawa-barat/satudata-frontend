import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
  inject,
  viewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Title } from '@angular/platform-browser';
import { RouterModule, Router, ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';
// COMPONENT
import {
  HeaderOrganizationComponent,
  SkeletonHeaderOrganizationComponent,
  FilterOrganizationComponent,
  FilterTopicComponent,
  FilterMapsetTypeComponent,
  FilterClassificationComponent,
  FilterClassificationIndicatorComponent,
  FilterQualityComponent,
  FilterDatasetTagsComponent,
  FilterLicenseComponent,
  FilterCategoryIndicatorComponent,
  SkeletonFilterComponent,
  SearchCatalogComponent,
  TabCatalogComponent,
  SortCatalogComponent,
  StatusCatalogComponent,
  QuickAccessCatalogComponent,
  ListDatasetComponent,
  ListMapsetComponent,
  ListIndicatorComponent,
  ListVisualizationComponent,
  ListEmptyComponent,
  SkeletonCatalogComponent,
  ModalDatasetStatusComponent,
  ModalMapsetStatusComponent,
  ModalIndicatorStatusComponent,
} from '@components-v4';
// CONSTANT
import {
  COLORSCHEME,
  STATUS_DATASET,
  STATUS_MAPSET,
  STATUS_INDICATOR,
  STATUS_VISUALIZATION,
} from '@constants-v4';
// INTERFACE
import { Styles, Breadcrumb } from '@interfaces-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService, StateService } from '@services-v3';
import {
  GlobalService,
  DatasetService,
  MapsetService,
  IndicatorService,
  VisualizationService,
} from '@services-v4';
// TYPE
import type {
  DropdownDatasetActionDelete,
  DropdownDatasetActionArchive,
  DropdownDatasetActionActive,
  DropdownDatasetActionRecover,
  DropdownDatasetActionDiscontinue,
} from '@components-v4';
import type { StatusDataset } from '@types-v4';
// UTIL
import { mappingStatusDataset } from '@utils-v4';
// JDS-BI
import { jdsBiColor } from '@jds-bi/cdk';
import {
  JdsBiAccordionModule,
  JdsBiButtonModule,
  JdsBiPaginationModule,
} from '@jds-bi/core';

import { assign, isEmpty, isNil, set, split } from 'lodash';
import moment from 'moment';
import { patchState } from '@ngrx/signals';
import { fromWorker } from 'observable-webworker';
import { SubscribeDirective } from '@ngneat/subscribe';
import Swal from 'sweetalert2';
import { useMutationResult } from '@ngneat/query';
import { NgxLoadingModule } from 'ngx-loading';

import { initialState, MyDataPageStore } from './my-data-page.store';

@Component({
  selector: 'app-my-data-page',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    SubscribeDirective,
    NgxLoadingModule,
    HeaderOrganizationComponent,
    SkeletonHeaderOrganizationComponent,
    JdsBiAccordionModule,
    JdsBiButtonModule,
    JdsBiPaginationModule,
    FilterOrganizationComponent,
    FilterTopicComponent,
    FilterMapsetTypeComponent,
    FilterClassificationComponent,
    FilterClassificationIndicatorComponent,
    FilterQualityComponent,
    FilterDatasetTagsComponent,
    FilterLicenseComponent,
    FilterCategoryIndicatorComponent,
    SkeletonFilterComponent,
    SearchCatalogComponent,
    TabCatalogComponent,
    SortCatalogComponent,
    StatusCatalogComponent,
    QuickAccessCatalogComponent,
    ListDatasetComponent,
    ListMapsetComponent,
    ListIndicatorComponent,
    ListVisualizationComponent,
    ListEmptyComponent,
    SkeletonCatalogComponent,
    ModalDatasetStatusComponent,
    ModalMapsetStatusComponent,
    ModalIndicatorStatusComponent,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './my-data-page.component.html',
  styleUrls: ['./my-data-page.component.scss'],
  providers: [MyDataPageStore],
})
export class MyDataPageComponent implements OnInit, OnDestroy {
  satudataUser: LoginData;
  satudataToken: string;
  colorScheme = COLORSCHEME;
  jds: Styles = jds;
  moment = moment;

  // Log
  log = null;
  pageInfo = null;
  deviceInfo = null;
  userInfo = null;

  // Settings
  title: string;
  label = 'Data Saya';
  breadcrumb: Breadcrumb[] = [{ label: 'Data Saya', link: '/my-data' }];

  // Modal
  modalDatasetStatusComponent = viewChild(ModalDatasetStatusComponent);
  modalMapsetStatusComponent = viewChild(ModalMapsetStatusComponent);
  modalIndicatorStatusComponent = viewChild(ModalIndicatorStatusComponent);

  // Service
  private router = inject(Router);
  private route = inject(ActivatedRoute);
  private globalService = inject(GlobalService);
  private authenticationService = inject(AuthenticationService);
  private stateService = inject(StateService);
  private titleService = inject(Title);
  private myDataPageStore = inject(MyDataPageStore);

  private datasetService = inject(DatasetService);
  private mapsetService = inject(MapsetService);
  private indicatorService = inject(IndicatorService);
  private visualizationService = inject(VisualizationService);
  editDatasetMutation = useMutationResult();
  editMapsetMutation = useMutationResult();
  editIndicatorMutation = useMutationResult();
  editVisualizationMutation = useMutationResult();

  // Component Store View Model
  readonly vm = this.myDataPageStore;

  // Variable
  perPageItems: any[] = [
    { value: 5, label: '5' },
    { value: 10, label: '10' },
  ];

  constructor() {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
    this.authenticationService.satudataToken.subscribe(
      (x) => (this.satudataToken = x),
    );
    this.stateService.currentLogUser.subscribe((x) => (this.userInfo = x));
  }

  ngOnInit(): void {
    this.logPage('init');
    this.settingsAll();
    this.queryParams();

    this.myDataPageStore.getOrganizationSelf();
    this.myDataPageStore.getAllOrganizationSatuData();
    this.myDataPageStore.getAllOrganizationSatuPeta();
    this.myDataPageStore.getAllTopicSatuData();
    this.myDataPageStore.getAllTopicSatuPeta();
    this.myDataPageStore.getAllMapsetType();
    this.myDataPageStore.getAllClassification();
    this.myDataPageStore.getAllClassificationIndicator();
    this.myDataPageStore.getAllDatasetQuality();
    this.myDataPageStore.getAllDatasetTags();
    this.myDataPageStore.getAllDatasetLicense();
    this.myDataPageStore.getAllIndicatorCategory();
    this.myDataPageStore.getAllData();
  }

  ngOnDestroy(): void {
    this.logPage('destroy');
  }

  settingsAll(): void {
    this.title = this.globalService.title;
    this.titleService.setTitle(`${this.label} - ${this.title}`);
    this.globalService.changeLabel(this.label);
    this.globalService.changeBreadcrumb(this.breadcrumb);
  }

  queryParams(): void {
    this.route.queryParams.subscribe((p) => {
      const checkArray = (param, value) => {
        return !isNil(param) && !isEmpty(param) ? split(param, ',') : value;
      };

      const {
        filterSearch: search,
        filterCatalog: catalog,
        filterSort: sort,
        filterStatus: status,
        filterOrganization: organization,
        filterTopic: topic,
        filterType: type,
        filterClassification: classification,
        filterQuality: quality,
        filterTags: tags,
        filterLicense: license,
        filterCategory: category,
        filterPerPage: perPage,
        filterCurrentPage: currentPage,
      } = initialState;

      const pSearch = !isNil(p['q']) ? p['q'] : search;
      const pCatalog = !isNil(p['catalog']) ? p['catalog'] : catalog;
      const pSort = !isNil(p['sort']) ? p['sort'] : sort;
      const pStatus = !isNil(p['status']) ? p['status'] : status;
      const pOrganization = checkArray(p['organization'], organization);
      const pTopic = checkArray(p['topic'], topic);
      const pType = checkArray(p['type'], type);
      const pClassification = checkArray(p['classification'], classification);
      const pQuality = checkArray(p['quality'], quality);
      const pTags = checkArray(p['tags'], tags);
      const pLicense = checkArray(p['license'], license);
      const pCategory = checkArray(p['category'], category);
      const pPerPage = !isNil(p['perPage']) ? p['perPage'] : perPage;
      const pCurrentPage = !isNil(p['page']) ? p['page'] : currentPage;

      patchState(this.vm, {
        filterSearch: pSearch,
        filterCatalog: pCatalog,
        filterStatus: pStatus,
        filterSort: pSort,
        filterOrganization: pOrganization,
        filterTopic: pTopic,
        filterType: pType,
        filterClassification: pClassification,
        filterQuality: pQuality,
        filterTags: pTags,
        filterLicense: pLicense,
        filterCategory: pCategory,
        filterPerPage: pPerPage,
        filterCurrentPage: pCurrentPage,
      });
    });
  }

  // Action ====================================================================
  onDatasetDelete(obj: DropdownDatasetActionDelete) {
    Swal.fire({
      type: 'warning',
      text: 'Apakah Anda yakin untuk menghapus Dataset ini?',
      allowOutsideClick: false,
      showCancelButton: true,
      cancelButtonText: 'Batal',
      confirmButtonColor: '#d33',
      confirmButtonText: 'Hapus',
    }).then((result) => {
      if (result.value) {
        const data = {
          type: 'dataset',
          type_id: Number(obj.id),
          category: obj.category,
          notes: null,
        };
        this.onDatasetUpdate(data);
      }
    });
  }

  onDatasetDiscontinue(obj: DropdownDatasetActionDiscontinue) {
    Swal.fire({
      type: 'warning',
      text: 'Apakah Anda yakin untuk mendiskontinu Dataset ini?',
      allowOutsideClick: false,
      showCancelButton: true,
      cancelButtonText: 'Batal',
      confirmButtonColor: jdsBiColor.blue['700'],
      confirmButtonText: 'Diskontinu',
    }).then((result) => {
      if (result.value) {
        const data = {
          type: 'dataset',
          type_id: Number(obj.id),
          category: obj.category,
          notes: null,
        };
        this.onDatasetUpdate(data);
      }
    });
  }

  onDatasetRecover(obj: DropdownDatasetActionRecover) {
    Swal.fire({
      type: 'warning',
      text: 'Apakah Anda yakin untuk memulihkan Dataset ini?',
      allowOutsideClick: false,
      showCancelButton: true,
      cancelButtonText: 'Batal',
      confirmButtonColor: jdsBiColor.blue['700'],
      confirmButtonText: 'Pulihkan',
    }).then((result) => {
      if (result.value) {
        const data = {
          type: 'dataset',
          type_id: Number(obj.id),
          category: 'recover' as const,
          lastCategory: obj.lastCategory,
          notes: null,
        };
        this.onDatasetUpdate(data);
      }
    });
  }

  onDatasetArchive(data: DropdownDatasetActionArchive) {
    this.modalDatasetStatusComponent().openModal(data.id);
  }

  onDatasetActive(data: DropdownDatasetActionActive) {
    this.modalDatasetStatusComponent().openModal(data.id);
  }

  onDatasetUpdate(formData: {
    type: string;
    type_id: number;
    category: StatusDataset | 'recover';
    lastCategory?: null | string;
    notes?: null | string;
  }) {
    const { category, lastCategory } = formData;

    let text;
    let status;
    let categoryHistory;
    switch (category) {
      case 'approve-active':
        text = 'diaktifkan';
        status = 'active';
        categoryHistory = 'approve';
        break;
      case 'approve-archive':
        text = 'diarsipkan';
        status = 'archive';
        categoryHistory = 'archive';
        break;
      case 'delete-active':
        text = 'dihapus';
        status = 'delete';
        categoryHistory = 'delete-active';
        break;
      case 'delete-delete':
        text = 'dihapus';
        status = 'delete';
        categoryHistory = 'delete-delete';
        break;
      case 'discontinue-active':
        text = 'didiskontinu';
        categoryHistory = 'discontinue-active';
        break;
      case 'discontinue-archive':
        text = 'didiskontinu';
        categoryHistory = 'discontinue-archive';
        break;
      case 'recover':
        text = 'dipulihkan';
        status = 'delete';
        categoryHistory = lastCategory;
        break;
      default:
        break;
    }

    const bodyDataset = {
      id: formData.type_id,
      history_draft: { ...formData, category: categoryHistory },
    };

    if (category !== 'recover') {
      assign(bodyDataset, STATUS_DATASET.get(category));
    } else {
      assign(
        bodyDataset,
        STATUS_DATASET.get(mappingStatusDataset(lastCategory)),
      );
    }

    if (
      category === 'discontinue-active' ||
      category === 'discontinue-archive'
    ) {
      // TODO: activate if backend discontinue date submit and approve ready
      // const bodyDataset = { is_discontinue: 1, discontinue_submit_date: new Date() };

      // TODO: change if backend discontinue date submit and approve ready
      assign(bodyDataset, { is_discontinue: 1 });
    }

    this.datasetService
      .updateItem(bodyDataset.id, bodyDataset)
      .pipe(this.editDatasetMutation.track())
      .subscribe(() => {
        Swal.fire({
          type: 'success',
          text: `Dataset berhasil ${text}`,
          allowOutsideClick: false,
        }).then(() => {
          if (
            category === 'discontinue-active' ||
            category === 'discontinue-archive'
          ) {
            this.router.navigate(['/desk'], {
              queryParams: { type: 'discontinue' },
            });
          } else {
            this.myDataPageStore.handleFilterStatus(status);
          }
        });
      });
  }

  onMapsetDelete(id: number) {
    Swal.fire({
      type: 'warning',
      text: 'Apakah Anda yakin untuk menghapus Mapset ini?',
      allowOutsideClick: false,
      showCancelButton: true,
      cancelButtonText: 'Batal',
      confirmButtonColor: '#d33',
      confirmButtonText: 'Hapus',
    }).then((result) => {
      if (result.value) {
        const data = {
          type: 'mapset',
          type_id: Number(id),
          category: 'draft-delete',
          notes: null,
        };
        this.onMapsetUpdate(data);
      }
    });
  }

  onMapsetArchive(id: number) {
    this.modalMapsetStatusComponent().openModal(id);
  }

  onMapsetActive(id: number) {
    this.modalMapsetStatusComponent().openModal(id);
  }

  onMapsetUpdate(formData: any) {
    const { category } = formData;
    const bodyMapset = { id: formData.type_id, history_draft: formData };

    assign(bodyMapset, STATUS_MAPSET.get(category));

    this.mapsetService
      .updateItem(bodyMapset.id, bodyMapset)
      .pipe(this.editMapsetMutation.track())
      .subscribe(() => {
        let text;
        let status;
        switch (category) {
          case 'draft-delete':
            text = 'dihapus';
            status = 'draft';
            break;
          case 'approve-active':
            text = 'diaktifkan';
            status = 'active';
            break;
          case 'approve-archive':
            text = 'diarsipkan';
            status = 'archive';
            break;
          default:
            break;
        }

        Swal.fire({
          type: 'success',
          text: `Mapset berhasil ${text}`,
          allowOutsideClick: false,
        }).then(() => {
          this.myDataPageStore.handleFilterStatus(status);
        });
      });
  }

  onIndicatorArchive(id: number) {
    this.modalIndicatorStatusComponent().openModal(id);
  }

  onIndicatorActive(id: number) {
    this.modalIndicatorStatusComponent().openModal(id);
  }

  onIndicatorUpdate(formData: any) {
    const { category } = formData;

    // TODO: activate if backend history_draft ready
    // const bodyIndicator = { id: formData.type_id, history_draft: formData };

    // TODO: change if backend history_draft ready
    const bodyIndicator = { id: formData.type_id };

    assign(bodyIndicator, STATUS_INDICATOR.get(category));

    this.indicatorService
      .updateItem(bodyIndicator.id, bodyIndicator)
      .pipe(this.editIndicatorMutation.track())
      .subscribe(() => {
        let text;
        let status;
        switch (category) {
          case 'draft-delete':
            text = 'dihapus';
            status = 'draft';
            break;
          case 'approve-active':
            text = 'diaktifkan';
            status = 'active';
            break;
          case 'approve-archive':
            text = 'diarsipkan';
            status = 'archive';
            break;
          default:
            break;
        }

        Swal.fire({
          type: 'success',
          text: `Indikator berhasil ${text}`,
          allowOutsideClick: false,
        }).then(() => {
          this.myDataPageStore.handleFilterStatus(status);
        });
      });
  }

  onVisualizationArchive(id: number) {
    Swal.fire({
      type: 'warning',
      text: 'Apakah Anda yakin untuk mengarsipkan Visualisasi ini?',
      allowOutsideClick: false,
      showCancelButton: true,
      cancelButtonText: 'Batal',
      confirmButtonColor: '#d33',
      confirmButtonText: 'Arsipkan',
    }).then((result) => {
      if (result.value) {
        const data = {
          type: 'visualization',
          type_id: Number(id),
          category: 'approve-archive',
          notes: null,
        };
        this.onVisualizationUpdate(data);
      }
    });
  }

  onVisualizationActive(id: number) {
    Swal.fire({
      type: 'warning',
      text: 'Apakah Anda yakin untuk mengaktifkan Visualisasi ini?',
      allowOutsideClick: false,
      showCancelButton: true,
      cancelButtonText: 'Batal',
      confirmButtonColor: '#d33',
      confirmButtonText: 'Aktifkan',
    }).then((result) => {
      if (result.value) {
        const data = {
          type: 'visualization',
          type_id: Number(id),
          category: 'approve-active',
          notes: null,
        };
        this.onVisualizationUpdate(data);
      }
    });
  }

  onVisualizationUpdate(formData: any) {
    const { category } = formData;
    const bodyVisualization = { id: formData.type_id };

    assign(bodyVisualization, STATUS_VISUALIZATION.get(category));

    this.visualizationService
      .updateItem(bodyVisualization.id, bodyVisualization)
      .pipe(this.editVisualizationMutation.track())
      .subscribe(() => {
        let text;
        let status;
        switch (category) {
          case 'draft-delete':
            text = 'dihapus';
            status = 'draft';
            break;
          case 'approve-active':
            text = 'diaktifkan';
            status = 'active';
            break;
          case 'approve-archive':
            text = 'diarsipkan';
            status = 'archive';
            break;
          default:
            break;
        }

        Swal.fire({
          type: 'success',
          text: `Visualisasi berhasil ${text}`,
          allowOutsideClick: false,
        }).then(() => {
          this.myDataPageStore.handleFilterStatus(status);
        });
      });
  }

  // Log =======================================================================
  logPage(type: string): void {
    if (type === 'init') {
      this.pageInfo = {
        page: this.router.url,
        date_start: moment().format('YYYY-MM-DD HH:mm:ss'),
      };

      this.log = {
        log: { ...this.pageInfo, ...this.userInfo },
        token: this.satudataToken,
      };
    } else if (type === 'destroy') {
      if (typeof Worker !== 'undefined') {
        set(this.log.log, 'date_end', moment().format('YYYY-MM-DD HH:mm:ss'));

        const log = of(this.log);

        fromWorker<object, string>(
          () =>
            new Worker(
              new URL('src/app/__v4/workers/log-page.worker', import.meta.url),
              { type: 'module' },
            ),
          log,
        ).subscribe((message) => {
          if (!environment.production) {
            console.info(`Log page: ${message}`);
          }
        });
      }
    }
  }
}
