// import { inject } from '@angular/core';
import { Routes } from '@angular/router';

// GUARDS
import { StoreVisualizationDetailGuard } from '@guards-v3';

export const VISUALIZATION_ROUTES: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        loadComponent: () =>
          import(
            './containers/visualization-page/visualization-page.component'
          ).then((m) => m.VisualizationPageComponent),
      },
      // {
      //   path: 'detail/:id',
      //   canActivate: [canActivateDatasetDetail],
      //   loadComponent: () =>
      //     import('./containers/visualization-detail/visualization-detail.component').then(
      //       (m) => m.DatasetDetailComponent,
      //     ),
      // },
      {
        path: ':id',
        canActivate: [StoreVisualizationDetailGuard],
        loadChildren: () =>
          import(
            '../../../visualization/visualization-detail/visualization-detail.module'
          ).then((m) => m.VisualizationDetailModule),
      },
    ],
  },
];
