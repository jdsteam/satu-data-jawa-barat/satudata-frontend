import { inject } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivateFn, Routes } from '@angular/router';

import { MapsetDetailService } from './containers/mapset-detail/mapset-detail.service';

const canActivateMapsetDetail: CanActivateFn = (
  route: ActivatedRouteSnapshot,
) => {
  return inject(MapsetDetailService).canActivate(route.params.id);
};

export const MAPSET_ROUTES: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        loadComponent: () =>
          import('./containers/mapset-page/mapset-page.component').then(
            (m) => m.MapsetPageComponent,
          ),
      },
      {
        path: 'detail/:id',
        canActivate: [canActivateMapsetDetail],
        loadComponent: () =>
          import('./containers/mapset-detail/mapset-detail.component').then(
            (m) => m.MapsetDetailComponent,
          ),
      },
    ],
  },
];
