import {
  ChangeDetectionStrategy,
  Component,
  inject,
  Input,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';
// COMPONENT
import { OrganizationInformationDetailComponent } from '@components-v4';
// CONSTANT
import { COLORSCHEME, SOURCE_MAPSET } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// MODEL
import { MapsetData } from '@models-v4';
// PIPE
import {
  MapsetMetadataXMLPipe,
  MapsetFileZIPPipe,
  SafeHtmlPipe,
} from '@pipes-v4';
// SERVICE
import { MapsetGeoserverService } from '@services-v4';
// UTIL
import { isValidHttpUrl } from '@utils-v4';
// JDS-BI
import { jdsBiDefaultProp } from '@jds-bi/cdk';
import { JdsBiNavModule } from '@jds-bi/core';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconArrowUpRightFromSquare,
  iconCloudArrowDown,
} from '@jds-bi/icons';

@Component({
  selector: 'app-mapset-information',
  standalone: true,
  imports: [
    CommonModule,
    MapsetMetadataXMLPipe,
    MapsetFileZIPPipe,
    SafeHtmlPipe,
    JdsBiNavModule,
    JdsBiIconsModule,
    OrganizationInformationDetailComponent,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './mapset-information.component.html',
})
export class MapsetInformationComponent {
  env = environment;
  colorScheme = COLORSCHEME;
  jds: Styles = jds;

  @Input()
  @jdsBiDefaultProp()
  data: MapsetData;

  // Service
  private mapsetGeoserverService = inject(MapsetGeoserverService);

  // Variable
  activeNav = 'about';
  mapsetSourceGeoserver = SOURCE_MAPSET.get('geoserver').id;

  constructor(private iconService: JdsBiIconsService) {
    this.iconService.registerIcons([
      iconArrowUpRightFromSquare,
      iconCloudArrowDown,
    ]);
  }

  onDownloadSHP() {
    const { title, shp, mapset_source: mapsetSource } = this.data;

    let url = shp;
    let link = 'original';

    if (!isValidHttpUrl(shp)) {
      url = environment.backendURL + shp;
      link = 'modify';
    }

    if (link === 'original' && mapsetSource.id === this.mapsetSourceGeoserver) {
      const filename = `${title}-shp.zip`;
      this.onDownloadGeoserverSHP(url, filename);
    } else {
      window.open(url, '_blank');
    }
  }

  onDownloadGeoserverSHP(url: string, filename: string) {
    this.mapsetGeoserverService.getFileSHP(url).subscribe((blob) => {
      const downloadUrl = URL.createObjectURL(blob);

      const link = document.createElement('a');
      link.href = downloadUrl;
      link.download = filename;
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);

      URL.revokeObjectURL(downloadUrl);
    });
  }
}
