import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  Input,
} from '@angular/core';
import { CommonModule } from '@angular/common';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME, SOURCE_MAPSET } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// MODEL
import { MapsetData } from '@models-v4';
// JDS-BI
import { jdsBiDefaultProp } from '@jds-bi/cdk';

import { v4 as uuidv4 } from 'uuid';
import * as L from 'leaflet';
import * as esri from 'esri-leaflet';
import 'esri-leaflet-renderers';

@Component({
  selector: 'app-mapset-preview',
  standalone: true,
  imports: [CommonModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './mapset-preview.component.html',
})
export class MapsetPreviewComponent implements AfterViewInit {
  colorScheme = COLORSCHEME;
  jds: Styles = jds;

  @Input()
  @jdsBiDefaultProp()
  data: MapsetData;

  // Variable
  id = uuidv4();

  ngAfterViewInit(): void {
    this.createMap();
  }

  createMap(): void {
    const map = L.map(`viewMapDetail-${this.id}`, {
      minZoom: 6,
      zoomControl: false,
      fullscreenControl: false,
      attributionControl: false,
      zoomSnap: 0.2,
      preferCanvas: true,
      worldCopyJump: false,
    });

    map.setView([-6.932694, 107.627449], 8);

    const basemapEnum = 'Topographic';

    esri.basemapLayer(basemapEnum).addTo(map);

    const url = this.data.app_link;

    switch (this.data.mapset_source.id) {
      case SOURCE_MAPSET.get('arcgis').id:
        this.createMapArcGIS(map, url);
        break;
      case SOURCE_MAPSET.get('geoserver').id:
        this.createMapGeoServer(map, url);
        break;
      default:
        break;
    }
  }

  createMapArcGIS(map: any, url: string) {
    const splitUrl = url.split('/');
    const mapserviceId = splitUrl[splitUrl.length - 1].replace(/\D/g, '');
    splitUrl.pop();
    const mapserverUrl = splitUrl.join('/');

    esri
      .dynamicMapLayer({
        url: mapserverUrl,
        layers: [mapserviceId],
      })
      .addTo(map);
  }

  createMapGeoServer(map: any, url: string) {
    const urlWMS = url.split('?')[0];
    const queryString = url.split('?')[1];
    const params = new URLSearchParams(queryString);
    const layers = params.get('layers');

    if (layers) {
      const wmsOptions = {
        layers,
        transparent: true,
        format: 'image/png',
      };

      L.tileLayer.wms(urlWMS, wmsOptions).addTo(map);
    }
  }
}
