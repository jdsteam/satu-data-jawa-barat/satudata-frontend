import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  ViewChild,
  inject,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, RouterModule } from '@angular/router';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';
// COMPONENT
import { ModalMapsetStatusComponent } from '@components-v4';
// CONSTANT
import { COLORSCHEME, STATUS_MAPSET, SOURCE_MAPSET } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// MODEL
import { LoginData } from '@models-v3';
import { MapsetData } from '@models-v4';
// SERVICE
import { AuthenticationService } from '@services-v3';
import {
  NotificationService,
  MapsetService,
  MapsetGeoserverService,
} from '@services-v4';
// UTIL
import { isValidHttpUrl } from '@utils-v4';
// JDS-BI
import { jdsBiColor, jdsBiDefaultProp } from '@jds-bi/cdk';
import {
  JdsBiButtonModule,
  JdsBiBadgeModule,
  JdsBiDropdownModule,
  JdsBiHostedDropdownModule,
} from '@jds-bi/core';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconCalendar,
  iconCircleCheck,
  iconFileTimer,
  iconBook,
  iconClockThree,
  iconBuilding,
  iconCircleDown,
  iconEye,
  iconChevronDown,
  iconEllipsis,
} from '@jds-bi/icons';

import { assign } from 'lodash';
import moment from 'moment';
import Swal from 'sweetalert2';
import { SubscribeDirective } from '@ngneat/subscribe';
import { useMutationResult } from '@ngneat/query';
import { NgxLoadingModule } from 'ngx-loading';

@Component({
  selector: 'app-mapset-header',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    JdsBiButtonModule,
    JdsBiBadgeModule,
    JdsBiDropdownModule,
    JdsBiHostedDropdownModule,
    JdsBiIconsModule,
    ModalMapsetStatusComponent,
    NgxLoadingModule,
    SubscribeDirective,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './mapset-header.component.html',
})
export class MapsetHeaderComponent {
  env = environment;
  satudataUser: LoginData;
  colorScheme = COLORSCHEME;
  jds: Styles = jds;
  jdsBiColor = jdsBiColor;
  moment = moment;

  @ViewChild(ModalMapsetStatusComponent)
  modalMapsetStatusComponent: ModalMapsetStatusComponent;

  @Input()
  @jdsBiDefaultProp()
  data: MapsetData;

  // Service
  private notificationService = inject(NotificationService);
  private mapsetService = inject(MapsetService);
  private mapsetGeoserverService = inject(MapsetGeoserverService);
  editMapsetMutation = useMutationResult();

  // Variable
  public isDropdownDownload = false;
  public isDropdownAction = false;
  status: string = null;
  mapsetSourceGeoserver = SOURCE_MAPSET.get('geoserver').id;

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private authenticationService: AuthenticationService,
    private iconService: JdsBiIconsService,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    this.iconService.registerIcons([
      iconCalendar,
      iconCircleCheck,
      iconFileTimer,
      iconBook,
      iconClockThree,
      iconBuilding,
      iconCircleDown,
      iconEye,
      iconChevronDown,
      iconEllipsis,
    ]);
  }

  getStatusData(value: boolean) {
    if (value) {
      return {
        icon: 'circle-check',
        name: 'Tetap',
        color: 'green',
      };
    }

    return {
      icon: 'file-timer',
      name: 'Sementara',
      color: 'yellow',
    };
  }

  onDownload(type: string) {
    const {
      title,
      shp,
      app_link: mapservice,
      mapset_source: mapsetSource,
    } = this.data;

    let url = shp;
    let link = 'original';

    switch (type) {
      case 'shp':
        if (!isValidHttpUrl(shp)) {
          url = environment.backendURL + shp;
          link = 'modify';
        }
        break;
      case 'mapservice':
        url = mapservice;
        break;
      default:
        break;
    }

    this.mapsetService
      .counter(this.data.id, { category: 'access_private' })
      .subscribe(() => {
        if (
          type === 'shp' &&
          link === 'original' &&
          mapsetSource.id === this.mapsetSourceGeoserver
        ) {
          const filename = `${title}-shp.zip`;
          this.onDownloadGeoserverSHP(url, filename);
        } else {
          window.open(url, '_blank');
        }
      });
  }

  onDownloadGeoserverSHP(url: string, filename: string) {
    this.mapsetGeoserverService.getFileSHP(url).subscribe((blob) => {
      const downloadUrl = URL.createObjectURL(blob);

      const link = document.createElement('a');
      link.href = downloadUrl;
      link.download = filename;
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);

      URL.revokeObjectURL(downloadUrl);
    });
  }

  onDelete(id: number) {
    Swal.fire({
      type: 'warning',
      text: 'Apakah Anda yakin untuk menghapus Mapset ini?',
      allowOutsideClick: false,
      showCancelButton: true,
      cancelButtonText: 'Batal',
      confirmButtonColor: '#d33',
      confirmButtonText: 'Hapus',
    }).then((result) => {
      if (result.value) {
        const data = {
          type: 'mapset',
          type_id: Number(id),
          category: 'draft-delete',
          notes: null,
        };
        this.onUpdate(data);
      }
    });
  }

  onArchive(id: number) {
    this.status = 'active';
    this.modalMapsetStatusComponent.openModal(id);
  }

  onActive(id: number) {
    this.status = 'archive';
    this.modalMapsetStatusComponent.openModal(id);
  }

  onUpdate(formData: any) {
    let category;
    switch (this.status) {
      case 'active':
        category = 'approve-archive';
        break;
      case 'archive':
        category = 'approve-active';
        break;
      case 'revision-new':
        category = 'revision-active-new';
        break;
      case 'revision-edit':
        category = 'revision-active-edit';
        break;
      default:
        category = formData.category;
        break;
    }

    const bodyMapset = {
      id: formData.type_id,
      history_draft: { ...formData, category },
    };

    assign(bodyMapset, STATUS_MAPSET.get(category));

    this.mapsetService
      .updateItem(bodyMapset.id, bodyMapset)
      .pipe(this.editMapsetMutation.track())
      .subscribe(() => {
        let text;
        let stat;
        switch (category) {
          case 'draft-delete':
            text = 'dihapus';
            stat = 'draft';
            break;
          case 'approve-active':
            text = 'diaktifkan';
            stat = 'active';
            break;
          case 'approve-archive':
            text = 'diarsipkan';
            stat = 'archive';
            break;
          default:
            break;
        }

        Swal.fire({
          type: 'success',
          text: `Mapset berhasil ${text}`,
          allowOutsideClick: false,
        }).then(() => {
          this.router.navigate(['/my-data'], {
            queryParams: {
              catalog: 'mapset',
              status: stat,
            },
          });
        });
      });
  }
}
