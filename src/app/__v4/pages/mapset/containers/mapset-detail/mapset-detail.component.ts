import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  ViewChild,
  inject,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, ActivatedRoute, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { zip } from 'rxjs';

// STYLE
import * as jds from '@styles';
// COMPONENT
import {
  BreadcrumbComponent,
  ModalMapsetStatusComponent,
} from '@components-v4';
// CONSTANT
import { COLORSCHEME, STATUS_MAPSET } from '@constants-v4';
// INTERFACE
import { Styles, Breadcrumb } from '@interfaces-v4';
// SERVICE
import { AuthenticationService } from '@services-v3';
import {
  GlobalService,
  NotificationService,
  MapsetService,
} from '@services-v4';
// JDS-BI
import { JdsBiButtonModule } from '@jds-bi/core';

import { assign } from 'lodash';
import { SubscribeDirective } from '@ngneat/subscribe';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { useMutationResult } from '@ngneat/query';
import { NgxLoadingModule } from 'ngx-loading';
import Swal from 'sweetalert2';

import { MapsetHeaderComponent } from '../../components/mapset-header/mapset-header.component';
import { MapsetInformationComponent } from '../../components/mapset-information/mapset-information.component';
import { MapsetPreviewComponent } from '../../components/mapset-preview/mapset-preview.component';

@Component({
  selector: 'app-mapset-detail',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    BreadcrumbComponent,
    JdsBiButtonModule,
    SubscribeDirective,
    NgxSkeletonLoaderModule,
    NgxLoadingModule,
    MapsetHeaderComponent,
    MapsetInformationComponent,
    MapsetPreviewComponent,
    ModalMapsetStatusComponent,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './mapset-detail.component.html',
})
export class MapsetDetailComponent implements OnInit {
  satudataUser: any;
  colorScheme = COLORSCHEME;
  jds: Styles = jds;

  @ViewChild(ModalMapsetStatusComponent)
  modalMapsetStatusComponent: ModalMapsetStatusComponent;

  // Settings
  title: string;
  label = 'Mapset';
  breadcrumb: Breadcrumb[] = [
    { label: 'Mapset', link: '/mapset' },
    { label: 'Detail Mapset', link: '/mapset/detail' },
  ];

  // Service
  private notificationService = inject(NotificationService);
  private mapsetService = inject(MapsetService);
  editMapsetMutation = useMutationResult();

  // Variable
  id: number;

  // Data
  mapsetData$: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private titleService: Title,
    private globalService: GlobalService,
    private authenticationService: AuthenticationService,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    // id
    this.id = this.route.snapshot.params.id;
  }

  ngOnInit(): void {
    this.settingsAll();
    this.getMapset();
  }

  settingsAll(): void {
    this.title = this.globalService.title;
    this.titleService.setTitle(`${this.label} - ${this.title}`);
    this.globalService.changeLabel(this.label);
    this.globalService.changeBreadcrumb(this.breadcrumb);
  }

  getMapset() {
    const params = {};

    this.mapsetData$ = this.mapsetService.getQuerySingle(
      this.id,
      params,
    ).result$;
  }

  checkAction(validate: number) {
    let accessDraft: boolean;
    if (this.satudataUser.role_name === 'walidata') {
      if (validate === 1 || validate === 4) {
        accessDraft = true;
      } else {
        accessDraft = false;
      }
    } else if (
      this.satudataUser.role_name === 'opd' ||
      this.satudataUser.role_name === 'opd-view'
    ) {
      accessDraft = false;
    }

    return accessDraft;
  }

  onDecline(id: number) {
    this.modalMapsetStatusComponent.openModal(id);
  }

  onUpdate(formData: any) {
    // Notification
    const bodyNotification = { is_enable: false, is_read: true };
    const paramsNotification = {
      type: 'mapset',
      type_id: formData.type_id,
      receiver: this.satudataUser.id,
    };

    const updateNotification = this.notificationService.updateItemBulk(
      bodyNotification,
      paramsNotification,
    );

    // Mapset
    const bodyMapset = { id: formData.type_id, history_draft: formData };

    assign(bodyMapset, STATUS_MAPSET.get('revision-active'));

    const updateMapset = this.mapsetService.updateItem(
      bodyMapset.id,
      bodyMapset,
    );

    zip(updateNotification, updateMapset)
      .pipe(this.editMapsetMutation.track())
      .subscribe(() => {
        Swal.fire({
          type: 'success',
          text: `Catatan revisi mapset berhasil terkirim`,
          allowOutsideClick: false,
        }).then(() => {
          this.router.navigate(['/desk'], {
            queryParams: {
              type: 'upload-change',
              status: 'revision',
            },
          });
        });
      });
  }

  onAccept(id: number) {
    Swal.fire({
      type: 'warning',
      text: 'Apakah Anda yakin mapset ini sudah layak dipublikasikan?',
      allowOutsideClick: false,
      showCancelButton: true,
      cancelButtonText: 'Batal',
      confirmButtonColor: '#0753A6',
      confirmButtonText: 'Setuju',
      reverseButtons: true,
    }).then((result) => {
      if (!result.value) {
        return;
      }

      // Notification
      const bodyNotification = { is_enable: false, is_read: true };
      const paramsNotification = {
        type: 'mapset',
        type_id: id,
        receiver: this.satudataUser.id,
      };

      const updateNotification = this.notificationService.updateItemBulk(
        bodyNotification,
        paramsNotification,
      );

      // Mapset
      const bodyHistory = {
        type: 'mapset',
        type_id: Number(this.id),
        category: 'approve-active',
        notes: null,
      };
      const bodyMapset = { id, history_draft: bodyHistory };

      assign(bodyMapset, STATUS_MAPSET.get('approve-active'));

      const updateMapset = this.mapsetService.updateItem(
        bodyMapset.id,
        bodyMapset,
      );

      zip(updateNotification, updateMapset)
        .pipe(this.editMapsetMutation.track())
        .subscribe(() => {
          Swal.fire({
            type: 'success',
            text: `Mapset berhasil dipublikasikan`,
            allowOutsideClick: false,
          }).then(() => {
            this.router.navigate(['/desk'], {
              queryParams: {
                type: 'upload-change',
                status: 'approve',
              },
            });
          });
        });
    });
  }
}
