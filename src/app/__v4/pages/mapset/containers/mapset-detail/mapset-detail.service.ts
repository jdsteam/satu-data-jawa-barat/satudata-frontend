import { Injectable, inject } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';

// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';
import { MapsetService } from '@services-v4';

import { isEmpty } from 'lodash';

@Injectable({
  providedIn: 'root',
})
export class MapsetDetailService {
  satudataUser: LoginData;

  // Service
  private router = inject(Router);
  private authenticationService = inject(AuthenticationService);
  private mapsetService = inject(MapsetService);

  // Variable
  id: number;

  constructor() {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
  }

  canActivate(id: number): Observable<boolean> {
    const params = {
      category: 'view_private',
    };

    return this.mapsetService.counter(id, params).pipe(
      filter((val) => val.data !== undefined),
      map((response) => {
        if (isEmpty(response.data)) {
          this.router.navigate(['/404']);
          return false;
        }

        // Check Access
        let access: boolean;
        switch (this.satudataUser.role_name) {
          case 'walidata':
            access = true;
            break;
          case 'opd':
            if (this.satudataUser.kode_skpd === response.data.skpd.kode_skpd) {
              access = true;
            } else if (
              response.data.is_validate === 3 &&
              this.satudataUser.kode_skpd !== response.data.skpd.kode_skpd
            ) {
              access = true;
            } else {
              access = false;
            }
            break;
          case 'opd-view':
            if (response.data.is_validate === 3) {
              access = true;
            } else {
              access = false;
            }
            break;
          default:
            break;
        }

        if (!access) {
          this.router.navigate(['/404']);
          return false;
        }

        return true;
      }),
    );
  }
}
