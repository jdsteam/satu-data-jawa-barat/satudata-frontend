import { computed, inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs';
import {
  patchState,
  signalStore,
  signalStoreFeature,
  withComputed,
  withHooks,
  withMethods,
  withState,
} from '@ngrx/signals';
import { rxMethod } from '@ngrx/signals/rxjs-interop';
import { tapResponse } from '@ngrx/operators';

// CONSTANT
import { SORT_MAPSET, STATUS_MAPSET } from '@constants-v4';
// FEATURE
import {
  withClassification,
  withMapsetType,
  withOrganizationSatuPeta,
  withTopicSatuPeta,
} from '@features-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';
import { MapsetService } from '@services-v4';

import { assign, isEmpty } from 'lodash';

type MapsetPageState = {
  satudataUser: LoginData;
  filterSearch: string | null;
  filterSort: string;
  filterOrganization: string[];
  filterTopic: string[];
  filterType: string[];
  filterClassification: number[] | string[];
  filterPerPage: number;
  filterCurrentPage: number;
  listMapset: any;
};

export const initialState: MapsetPageState = {
  satudataUser: null,
  filterSearch: null,
  filterSort: 'newest',
  filterOrganization: [],
  filterTopic: [],
  filterType: [],
  filterClassification: [],
  filterPerPage: 5,
  filterCurrentPage: 1,
  listMapset: null,
};

export function withMapsetPage() {
  return signalStoreFeature(
    withOrganizationSatuPeta(),
    withTopicSatuPeta(),
    withClassification(),
    withMapsetType(),
  );
}

export const MapsetPageStore = signalStore(
  withState(initialState),
  withHooks((store) => {
    const authenticationService = inject(AuthenticationService);

    return {
      onInit() {
        authenticationService.satudataUser.subscribe((x) => {
          patchState(store, { satudataUser: x });
        });
      },
    };
  }),
  withComputed((store) => ({
    hasFilterQuickAccess: computed(() => {
      if (
        store.filterOrganization().length > 0 ||
        store.filterTopic().length > 0 ||
        store.filterType().length > 0 ||
        store.filterClassification().length > 0
      ) {
        return true;
      }

      return false;
    }),
    hasFilterSearch: computed(() => {
      if (
        store.filterSearch() ||
        store.filterOrganization().length ||
        store.filterTopic().length ||
        store.filterType().length ||
        store.filterClassification().length
      ) {
        return true;
      }

      return false;
    }),
  })),
  withMapsetPage(),
  withMethods(
    (
      store,
      router = inject(Router),
      route = inject(ActivatedRoute),
      mapsetService = inject(MapsetService),
    ) => ({
      handleFilterSearch(search: string | null): void {
        const { filterSearch: previous } = store;
        const value = isEmpty(search) === false ? search : null;

        if (previous() !== value) {
          router.navigate([], {
            relativeTo: route,
            queryParams: {
              q: value,
              page: null,
            },
            queryParamsHandling: 'merge',
          });

          patchState(store, () => ({
            filterSearch: value,
            filterCurrentPage: initialState.filterCurrentPage,
          }));

          this.getAllMapset();
        }
      },
      handleFilterSort(sort: string): void {
        router.navigate([], {
          relativeTo: route,
          queryParams: {
            sort,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterSort: sort,
          filterCurrentPage: initialState.filterCurrentPage,
        }));

        this.getAllMapset();
      },
      handleFilterOrganization(organization: string[]): void {
        const value = !isEmpty(organization) ? organization.join(',') : null;

        router.navigate([], {
          relativeTo: route,
          queryParams: {
            organization: value,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterOrganization: organization,
          filterCurrentPage: initialState.filterCurrentPage,
        }));

        this.getAllMapset();
      },
      handleFilterTopic(topic: string[]): void {
        const value = !isEmpty(topic) ? topic.join(',') : null;

        router.navigate([], {
          relativeTo: route,
          queryParams: {
            topic: value,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterTopic: topic,
          filterCurrentPage: initialState.filterCurrentPage,
        }));

        this.getAllMapset();
      },
      handleFilterMapsetType(type: string[]): void {
        const value = !isEmpty(type) ? type.join(',') : null;

        router.navigate([], {
          relativeTo: route,
          queryParams: {
            type: value,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterType: type,
          filterCurrentPage: initialState.filterCurrentPage,
        }));

        this.getAllMapset();
      },
      handleFilterClassification(classification: number[] | string[]): void {
        const value = !isEmpty(classification)
          ? classification.join(',')
          : null;

        router.navigate([], {
          relativeTo: route,
          queryParams: {
            classification: value,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterClassification: classification,
          filterCurrentPage: initialState.filterCurrentPage,
        }));

        this.getAllMapset();
      },
      handleFilterPerPage(perPage: number): void {
        router.navigate([], {
          relativeTo: route,
          queryParams: {
            perPage,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterPerPage: perPage,
          filterCurrentPage: initialState.filterCurrentPage,
        }));

        this.getAllMapset();
      },
      handleFilterCurrentPage(currentPage: number): void {
        router.navigate([], {
          relativeTo: route,
          queryParams: {
            page: currentPage,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterCurrentPage: currentPage,
        }));

        this.getAllMapset();
      },
      handleFilterClear(): void {
        router.navigate([], {
          relativeTo: route,
          queryParams: {
            organization: [],
            topic: [],
            type: [],
            classification: [],
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterOrganization: initialState.filterOrganization,
          filterTopic: initialState.filterTopic,
          filterType: initialState.filterType,
          filterClassification: initialState.filterClassification,
          filterCurrentPage: initialState.filterCurrentPage,
        }));

        this.getAllMapset();
      },
      getAllMapset: rxMethod<void>(
        switchMap(() => {
          const {
            filterSearch: search,
            filterOrganization: organization,
            filterTopic: topic,
            filterType: type,
            filterClassification: classification,
            filterSort: sort,
            filterPerPage: perPage,
            filterCurrentPage: currentPage,
          } = store;

          const pWhere = {};

          assign(pWhere, STATUS_MAPSET.get('approve-active'));

          if (!isEmpty(organization())) {
            assign(pWhere, { kode_skpd: organization() });
          }

          if (!isEmpty(topic())) {
            assign(pWhere, { sektoral_id: topic() });
          }

          if (!isEmpty(type())) {
            assign(pWhere, { mapset_type_id: type() });
          }

          if (!isEmpty(classification())) {
            assign(pWhere, { dataset_class_id: classification() });
          }

          const params = {
            search: search() || '',
            sort: SORT_MAPSET.get(sort()).paramSort,
            skip: perPage() * currentPage() - perPage(),
            limit: perPage(),
            where: JSON.stringify(pWhere),
          };

          return mapsetService.getQueryList(params).result$.pipe(
            tapResponse({
              next: (result) => {
                patchState(store, { listMapset: result });
              },
              error: console.error,
            }),
          );
        }),
      ),
    }),
  ),
);
