import { computed, inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs';
import {
  patchState,
  signalStore,
  withComputed,
  withHooks,
  withMethods,
  withState,
} from '@ngrx/signals';
import { rxMethod } from '@ngrx/signals/rxjs-interop';
import { tapResponse } from '@ngrx/operators';

// CONSTANT
import { SORT_ORGANIZATION } from '@constants-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';
import { OrganizationService } from '@services-v4';

import { isEmpty } from 'lodash';

type OrganizationPageState = {
  satudataUser: LoginData;
  filterSearch: string | null;
  filterSort: string;
  filterPerPage: number;
  filterCurrentPage: number;
  listOrganization: any;
};

export const initialState: OrganizationPageState = {
  satudataUser: null,
  filterSearch: null,
  filterSort: 'alphabet',
  filterPerPage: 18,
  filterCurrentPage: 1,
  listOrganization: null,
};

export const OrganizationPageStore = signalStore(
  withState(initialState),
  withHooks((store) => {
    const authenticationService = inject(AuthenticationService);

    return {
      onInit() {
        authenticationService.satudataUser.subscribe((x) => {
          patchState(store, { satudataUser: x });
        });
      },
    };
  }),
  withComputed((store) => ({
    hasFilterSearch: computed(() => {
      if (store.filterSearch()) {
        return true;
      }

      return false;
    }),
  })),
  withMethods(
    (
      store,
      router = inject(Router),
      route = inject(ActivatedRoute),
      organizationService = inject(OrganizationService),
    ) => ({
      handleFilterSearch(search: string | null): void {
        const { filterSearch: previous } = store;
        const value = isEmpty(search) === false ? search : null;

        if (previous() !== value) {
          router.navigate([], {
            relativeTo: route,
            queryParams: {
              q: value,
              page: null,
            },
            queryParamsHandling: 'merge',
          });

          patchState(store, () => ({
            filterSearch: value,
            filterCurrentPage: initialState.filterCurrentPage,
          }));

          this.getAllOrganization();
        }
      },
      handleFilterSort(sort: string): void {
        router.navigate([], {
          relativeTo: route,
          queryParams: {
            sort,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterSort: sort,
          filterCurrentPage: initialState.filterCurrentPage,
        }));

        this.getAllOrganization();
      },
      handleFilterPerPage(perPage: number): void {
        router.navigate([], {
          relativeTo: route,
          queryParams: {
            perPage,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterPerPage: perPage,
          filterCurrentPage: initialState.filterCurrentPage,
        }));

        this.getAllOrganization();
      },
      handleFilterCurrentPage(currentPage: number): void {
        router.navigate([], {
          relativeTo: route,
          queryParams: {
            page: currentPage,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterCurrentPage: currentPage,
        }));

        this.getAllOrganization();
      },
      getAllOrganization: rxMethod<void>(
        switchMap(() => {
          const {
            filterSearch: search,
            filterSort: sort,
            filterPerPage: perPage,
            filterCurrentPage: currentPage,
          } = store;

          const pWhere = {
            is_deleted: false,
            is_external: false,
          };

          const params = {
            search: search() || '',
            sort: SORT_ORGANIZATION.get(sort()).paramSort,
            skip: perPage() * currentPage() - perPage(),
            limit: perPage(),
            where: JSON.stringify(pWhere),
          };

          return organizationService.getQueryList(params).result$.pipe(
            tapResponse({
              next: (result) => {
                patchState(store, { listOrganization: result });
              },
              error: console.error,
            }),
          );
        }),
      ),
    }),
  ),
);
