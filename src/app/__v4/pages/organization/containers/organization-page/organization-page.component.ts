import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
  inject,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Title } from '@angular/platform-browser';
import { RouterModule, Router, ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';
// COMPONENT
import {
  HeaderCatalogComponent,
  SearchCatalogComponent,
  SortCatalogComponent,
  ListOrganizationComponent,
  ListEmptyComponent,
  SkeletonCatalogOrganizationComponent,
} from '@components-v4';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Breadcrumb } from '@interfaces-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { StateService } from '@services-v3';
import { GlobalService } from '@services-v4';
// JDS-BI
import {
  JdsBiAccordionModule,
  JdsBiButtonModule,
  JdsBiPaginationModule,
} from '@jds-bi/core';

import { isNil, set } from 'lodash';
import moment from 'moment';
import { patchState } from '@ngrx/signals';
import { fromWorker } from 'observable-webworker';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { SubscribeDirective } from '@ngneat/subscribe';

import { initialState, OrganizationPageStore } from './organization-page.store';

@Component({
  selector: 'app-organization-page',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    HeaderCatalogComponent,
    JdsBiAccordionModule,
    JdsBiButtonModule,
    JdsBiPaginationModule,
    SearchCatalogComponent,
    SortCatalogComponent,
    ListOrganizationComponent,
    ListEmptyComponent,
    SkeletonCatalogOrganizationComponent,
    NgxSkeletonLoaderModule,
    SubscribeDirective,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './organization-page.component.html',
  providers: [OrganizationPageStore],
})
export class OrganizationPageComponent implements OnInit, OnDestroy {
  satudataUser: LoginData;
  satudataToken: string;
  colorScheme = COLORSCHEME;
  jds: any = jds;
  moment = moment;

  // Log
  log = null;
  pageInfo = null;
  deviceInfo = null;
  userInfo = null;

  // Setting
  title: string;
  label = 'Organisasi';
  description =
    'Satu Data Jabar didukung oleh seluruh Organisasi Perangkat Daerah di Provinsi Jawa Barat' +
    'dalam membangun ekosistem data yang terbuka dan terpercaya, serta memfasilitasi fungsi berbagi' +
    'pakai data antar Organisasi Perangkat Daerah.';
  breadcrumb: Breadcrumb[] = [{ label: 'Organisasi', link: '/organisasi' }];

  // Service
  private router = inject(Router);
  private route = inject(ActivatedRoute);
  private globalService = inject(GlobalService);
  private stateService = inject(StateService);
  private titleService = inject(Title);
  private organizationPageStore = inject(OrganizationPageStore);

  // Component Store View Model
  readonly vm = this.organizationPageStore;

  // Variable
  perPageItems: any[] = [
    { value: 18, label: '18' },
    { value: 36, label: '36' },
  ];

  constructor() {
    this.stateService.currentLogUser.subscribe((x) => (this.userInfo = x));
  }

  ngOnInit(): void {
    this.logPage('init');
    this.settingsAll();
    this.queryParams();

    this.organizationPageStore.getAllOrganization();
  }

  ngOnDestroy(): void {
    this.logPage('destroy');
  }

  settingsAll(): void {
    this.title = this.globalService.title;
    this.titleService.setTitle(`${this.label} - ${this.title}`);
    this.globalService.changeLabel(this.label);
    this.globalService.changeBreadcrumb(this.breadcrumb);
  }

  queryParams(): void {
    this.route.queryParams.subscribe((p) => {
      const {
        filterSearch: search,
        filterSort: sort,
        filterPerPage: perPage,
        filterCurrentPage: currentPage,
      } = initialState;

      const pSearch = !isNil(p['q']) ? p['q'] : search;
      const pSort = !isNil(p['sort']) ? p['sort'] : sort;
      const pPerPage = !isNil(p['perPage']) ? p['perPage'] : perPage;
      const pCurrentPage = !isNil(p['page']) ? p['page'] : currentPage;

      patchState(this.vm, {
        filterSearch: pSearch,
        filterSort: pSort,
        filterPerPage: pPerPage,
        filterCurrentPage: pCurrentPage,
      });
    });
  }

  // Log =======================================================================
  logPage(type: string): void {
    if (type === 'init') {
      this.pageInfo = {
        page: this.router.url,
        date_start: moment().format('YYYY-MM-DD HH:mm:ss'),
      };

      this.log = {
        log: { ...this.pageInfo, ...this.userInfo },
        token: this.satudataToken,
      };
    } else if (type === 'destroy') {
      if (typeof Worker !== 'undefined') {
        set(this.log.log, 'date_end', moment().format('YYYY-MM-DD HH:mm:ss'));

        const log = of(this.log);

        fromWorker<object, string>(
          () =>
            new Worker(
              new URL('src/app/__v4/workers/log-page.worker', import.meta.url),
              { type: 'module' },
            ),
          log,
        ).subscribe((message) => {
          if (!environment.production) {
            console.info(`Log page: ${message}`);
          }
        });
      }
    }
  }
}
