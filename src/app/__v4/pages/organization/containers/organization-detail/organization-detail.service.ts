import { Injectable, inject } from '@angular/core';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';

// SERVICE
import { OrganizationService } from '@services-v4';

import { isEmpty } from 'lodash';

@Injectable({
  providedIn: 'root',
})
export class OrganizationDetailService {
  // Service
  private router = inject(Router);
  private organizationService = inject(OrganizationService);

  // Variable
  id: number;

  canActivate(next: ActivatedRouteSnapshot, id: number): Observable<boolean> {
    const params = {
      where: JSON.stringify({
        is_deleted: false,
        is_external: false,
      }),
    };

    return this.organizationService.getQuerySingle(id, params).result$.pipe(
      filter((val) => val.data !== undefined),
      map((response) => {
        if (isEmpty(response.data)) {
          this.router.navigate(['/404']);
          return false;
        }

        next.data = { ...next.data, organization: response.data };
        return true;
      }),
    );
  }

  resolve(route: ActivatedRouteSnapshot) {
    return route.data.organization;
  }
}
