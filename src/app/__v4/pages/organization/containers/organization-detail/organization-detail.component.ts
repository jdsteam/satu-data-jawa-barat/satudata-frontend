import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnDestroy,
  OnInit,
  inject,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Title } from '@angular/platform-browser';
import { RouterModule, Router, ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';
// COMPONENT
import {
  HeaderOrganizationComponent,
  SkeletonHeaderOrganizationComponent,
  FilterTopicComponent,
  FilterMapsetTypeComponent,
  FilterClassificationComponent,
  FilterClassificationIndicatorComponent,
  FilterQualityComponent,
  FilterDatasetTagsComponent,
  FilterLicenseComponent,
  FilterCategoryIndicatorComponent,
  SkeletonFilterComponent,
  SearchCatalogComponent,
  TabCatalogComponent,
  SortCatalogComponent,
  StatusCatalogComponent,
  QuickAccessCatalogComponent,
  ListDatasetComponent,
  ListMapsetComponent,
  ListIndicatorComponent,
  ListVisualizationComponent,
  ListEmptyComponent,
  SkeletonCatalogComponent,
} from '@components-v4';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Styles, Breadcrumb } from '@interfaces-v4';
// MODEL
import { LoginData } from '@models-v3';
import { OrganizationData } from '@models-v4';
// SERVICE
import { AuthenticationService, StateService } from '@services-v3';
import { GlobalService } from '@services-v4';

// JDS-BI
import {
  JdsBiAccordionModule,
  JdsBiButtonModule,
  JdsBiPaginationModule,
} from '@jds-bi/core';

import { isEmpty, isNil, set, split } from 'lodash';
import moment from 'moment';
import { patchState } from '@ngrx/signals';
import { fromWorker } from 'observable-webworker';
import { SubscribeDirective } from '@ngneat/subscribe';
import { NgxLoadingModule } from 'ngx-loading';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

import {
  initialState,
  OrganizationDetailStore,
} from './organization-detail.store';

@Component({
  selector: 'app-organization-detail',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    SubscribeDirective,
    NgxLoadingModule,
    HeaderOrganizationComponent,
    SkeletonHeaderOrganizationComponent,
    JdsBiAccordionModule,
    JdsBiButtonModule,
    JdsBiPaginationModule,
    FilterTopicComponent,
    FilterMapsetTypeComponent,
    FilterClassificationComponent,
    FilterClassificationIndicatorComponent,
    FilterQualityComponent,
    FilterDatasetTagsComponent,
    FilterLicenseComponent,
    FilterCategoryIndicatorComponent,
    SkeletonFilterComponent,
    SearchCatalogComponent,
    TabCatalogComponent,
    SortCatalogComponent,
    StatusCatalogComponent,
    QuickAccessCatalogComponent,
    ListDatasetComponent,
    ListMapsetComponent,
    ListIndicatorComponent,
    ListVisualizationComponent,
    ListEmptyComponent,
    SkeletonCatalogComponent,
    NgxSkeletonLoaderModule,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './organization-detail.component.html',
  styleUrls: ['./organization-detail.component.scss'],
  providers: [OrganizationDetailStore],
})
export class OrganizationDetailComponent implements OnInit, OnDestroy {
  satudataUser: LoginData;
  satudataToken: string;
  colorScheme = COLORSCHEME;
  jds: Styles = jds;
  moment = moment;

  // Log
  pageLog = null;
  organizationLog = null;
  pageInfo = null;
  organizationInfo = null;
  deviceInfo = null;
  userInfo = null;

  // Settings
  title: string;
  label = 'Organisasi';
  breadcrumb: Breadcrumb[] = [{ label: 'Organisasi', link: '/organisasi' }];

  @Input() organization?: OrganizationData;

  // Service
  private router = inject(Router);
  private route = inject(ActivatedRoute);
  private globalService = inject(GlobalService);
  private authenticationService = inject(AuthenticationService);
  private stateService = inject(StateService);
  private titleService = inject(Title);
  private myDataPageStore = inject(OrganizationDetailStore);

  // Component Store View Model
  readonly vm = this.myDataPageStore;

  // Variable
  perPageItems: any[] = [
    { value: 5, label: '5' },
    { value: 10, label: '10' },
  ];

  constructor() {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
    this.authenticationService.satudataToken.subscribe(
      (x) => (this.satudataToken = x),
    );
    this.stateService.currentLogUser.subscribe((x) => (this.userInfo = x));
  }

  ngOnInit(): void {
    this.logPage('init');
    this.logOrganization('init');
    this.settingsAll();
    this.queryParams();

    this.myDataPageStore.handleSelfOrganization(this.organization);
    this.myDataPageStore.getAllTopicSatuData();
    this.myDataPageStore.getAllTopicSatuPeta();
    this.myDataPageStore.getAllMapsetType();
    this.myDataPageStore.getAllClassification();
    this.myDataPageStore.getAllClassificationIndicator();
    this.myDataPageStore.getAllDatasetQuality();
    this.myDataPageStore.getAllDatasetTags();
    this.myDataPageStore.getAllDatasetLicense();
    this.myDataPageStore.getAllIndicatorCategory();
    this.myDataPageStore.getAllData();
  }

  ngOnDestroy(): void {
    this.logPage('destroy');
    this.logOrganization('destroy');
  }

  settingsAll(): void {
    this.title = this.globalService.title;
    this.titleService.setTitle(`${this.label} - ${this.title}`);
    this.globalService.changeLabel(this.label);
    this.globalService.changeBreadcrumb(this.breadcrumb);
  }

  queryParams(): void {
    this.route.queryParams.subscribe((p) => {
      const checkArray = (param, value) => {
        return !isNil(param) && !isEmpty(param) ? split(param, ',') : value;
      };

      const {
        filterSearch: search,
        filterCatalog: catalog,
        filterSort: sort,
        filterTopic: topic,
        filterType: type,
        filterClassification: classification,
        filterQuality: quality,
        filterTags: tags,
        filterLicense: license,
        filterCategory: category,
        filterPerPage: perPage,
        filterCurrentPage: currentPage,
      } = initialState;

      const pSearch = !isNil(p['q']) ? p['q'] : search;
      const pCatalog = !isNil(p['catalog']) ? p['catalog'] : catalog;
      const pSort = !isNil(p['sort']) ? p['sort'] : sort;
      const pTopic = checkArray(p['topic'], topic);
      const pType = checkArray(p['type'], type);
      const pClassification = checkArray(p['classification'], classification);
      const pQuality = checkArray(p['quality'], quality);
      const pTags = checkArray(p['tags'], tags);
      const pLicense = checkArray(p['license'], license);
      const pCategory = checkArray(p['category'], category);
      const pPerPage = !isNil(p['perPage']) ? p['perPage'] : perPage;
      const pCurrentPage = !isNil(p['page']) ? p['page'] : currentPage;

      patchState(this.vm, {
        filterSearch: pSearch,
        filterCatalog: pCatalog,
        filterSort: pSort,
        filterTopic: pTopic,
        filterType: pType,
        filterClassification: pClassification,
        filterQuality: pQuality,
        filterTags: pTags,
        filterLicense: pLicense,
        filterCategory: pCategory,
        filterPerPage: pPerPage,
        filterCurrentPage: pCurrentPage,
      });
    });
  }

  // Log =======================================================================
  logPage(type: string): void {
    if (type === 'init') {
      this.pageInfo = {
        page: this.router.url,
        date_start: moment().format('YYYY-MM-DD HH:mm:ss'),
      };

      this.pageLog = {
        log: { ...this.pageInfo, ...this.userInfo },
        token: this.satudataToken,
      };
    } else if (type === 'destroy') {
      if (typeof Worker !== 'undefined') {
        set(
          this.pageLog.log,
          'date_end',
          moment().format('YYYY-MM-DD HH:mm:ss'),
        );

        const log = of(this.pageLog);

        fromWorker<object, string>(
          () =>
            new Worker(
              new URL('src/app/__v4/workers/log-page.worker', import.meta.url),
              { type: 'module' },
            ),
          log,
        ).subscribe((message) => {
          if (!environment.production) {
            console.info(`Log page: ${message}`);
          }
        });
      }
    }
  }

  logOrganization(type: string): void {
    if (type === 'init') {
      this.organizationInfo = {
        page: this.router.url,
        action: 'view',
        status: 'success',
        date_start: moment().format('YYYY-MM-DD HH:mm:ss'),
        organization_id: this.organization.id,
        organization_code: this.organization.kode_skpd,
        organization_title: this.organization.title,
        organization_name: this.organization.nama_skpd,
      };

      this.organizationLog = {
        log: { ...this.organizationInfo, ...this.userInfo },
        token: this.satudataToken,
      };
    } else if (type === 'destroy') {
      if (typeof Worker !== 'undefined') {
        set(
          this.organizationLog.log,
          'date_end',
          moment().format('YYYY-MM-DD HH:mm:ss'),
        );

        fromWorker<object, string>(
          () =>
            new Worker(
              new URL(
                'src/app/__v4/workers/log-organization.worker',
                import.meta.url,
              ),
              {
                type: 'module',
                name: 'organization',
              },
            ),
          of(this.organizationLog),
        ).subscribe((message) => {
          if (!environment.production) {
            console.info(`Log organization: ${message}`);
          }
        });
      }
    }
  }
}
