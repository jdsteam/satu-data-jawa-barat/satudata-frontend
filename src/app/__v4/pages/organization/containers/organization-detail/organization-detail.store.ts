import { computed, inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs';
import {
  patchState,
  signalStore,
  signalStoreFeature,
  withComputed,
  withHooks,
  withMethods,
  withState,
} from '@ngrx/signals';
import { rxMethod } from '@ngrx/signals/rxjs-interop';
import { tapResponse } from '@ngrx/operators';

// CONSTANT
import {
  SORT_DATASET,
  SORT_MAPSET,
  SORT_INDICATOR,
  SORT_VISUALIZATION,
  STATUS_DATASET,
  STATUS_MAPSET,
  STATUS_INDICATOR,
  STATUS_VISUALIZATION,
  QUALITY_DATA,
} from '@constants-v4';
// FEATURE
import {
  withClassification,
  withClassificationIndicator,
  withDatasetLicense,
  withDatasetQuality,
  withDatasetTags,
  withIndicatorCategory,
  withMapsetType,
  withTopicSatuData,
  withTopicSatuPeta,
} from '@features-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';
import {
  DatasetService,
  MapsetService,
  IndicatorService,
  VisualizationService,
} from '@services-v4';

import { assign, isEmpty } from 'lodash';

type OrganizationDetailState = {
  satudataUser: LoginData;
  filterSearch: string | null;
  filterCatalog: string;
  filterSort: string;
  filterTopic: string[];
  filterType: string[];
  filterClassification: number[] | string[];
  filterQuality: string[];
  filterTags: string[];
  filterLicense: string[];
  filterCategory: string[];
  filterPerPage: number;
  filterCurrentPage: number;
  listOrganizationSelf: any;
  listDataset: any;
  listMapset: any;
  listIndicator: any;
  listVisualization: any;
};

export const initialState: OrganizationDetailState = {
  satudataUser: null,
  filterSearch: null,
  filterCatalog: 'dataset',
  filterSort: 'newest',
  filterTopic: [],
  filterType: [],
  filterClassification: [],
  filterQuality: [],
  filterTags: [],
  filterLicense: [],
  filterCategory: [],
  filterPerPage: 5,
  filterCurrentPage: 1,
  listOrganizationSelf: null,
  listDataset: null,
  listMapset: null,
  listIndicator: null,
  listVisualization: null,
};

export function withOrganizationDetail1Page() {
  return signalStoreFeature(
    withTopicSatuData(),
    withTopicSatuPeta(),
    withMapsetType(),
    withClassification(),
    withClassificationIndicator(),
  );
}

export function withOrganizationDetail2Page() {
  return signalStoreFeature(
    withDatasetQuality(),
    withDatasetTags(),
    withDatasetLicense(),
    withIndicatorCategory(),
  );
}

export const OrganizationDetailStore = signalStore(
  withState(initialState),
  withHooks((store) => {
    const authenticationService = inject(AuthenticationService);

    return {
      onInit() {
        authenticationService.satudataUser.subscribe((x) => {
          patchState(store, { satudataUser: x });
        });
      },
    };
  }),
  withComputed((store) => ({
    hasFilterQuickAccess: computed(() => {
      if (
        store.filterTopic().length > 0 ||
        store.filterType().length > 0 ||
        store.filterClassification().length > 0 ||
        store.filterQuality().length > 0 ||
        store.filterTags().length > 0 ||
        store.filterLicense().length > 0 ||
        store.filterCategory().length > 0
      ) {
        return true;
      }

      return false;
    }),
    hasFilterSearch: computed(() => {
      if (
        store.filterSearch() ||
        store.filterTopic().length ||
        store.filterType().length ||
        store.filterClassification().length ||
        store.filterQuality().length ||
        store.filterTags().length ||
        store.filterLicense().length ||
        store.filterCategory().length
      ) {
        return true;
      }

      return false;
    }),
  })),
  withOrganizationDetail1Page(),
  withOrganizationDetail2Page(),
  withMethods(
    (
      store,
      router = inject(Router),
      route = inject(ActivatedRoute),
      datasetService = inject(DatasetService),
      mapsetService = inject(MapsetService),
      indicatorService = inject(IndicatorService),
      visualizationService = inject(VisualizationService),
    ) => ({
      handleSelfOrganization(organization: any): void {
        patchState(store, () => ({
          listOrganizationSelf: organization,
        }));
      },
      handleFilterSearch(search: string | null): void {
        const { filterSearch: previous } = store;
        const value = isEmpty(search) === false ? search : null;

        if (previous() !== value) {
          router.navigate([], {
            relativeTo: route,
            queryParams: {
              q: value,
              page: null,
            },
            queryParamsHandling: 'merge',
          });

          patchState(store, () => ({
            filterSearch: value,
            filterCurrentPage: initialState.filterCurrentPage,
          }));

          this.getAllData();
        }
      },
      handleFilterCatalog(catalog: string): void {
        router.navigate([], {
          relativeTo: route,
          queryParams: {
            q: null,
            catalog,
            sort: null,
            status: null,
            organization: [],
            topic: [],
            type: [],
            classification: [],
            quality: [],
            tags: [],
            license: [],
            category: [],
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterSearch: initialState.filterSearch,
          filterCatalog: catalog,
          filterSort: initialState.filterSort,
          filterTopic: initialState.filterTopic,
          filterType: initialState.filterType,
          filterClassification: initialState.filterClassification,
          filterQuality: initialState.filterQuality,
          filterTags: initialState.filterTags,
          filterLicense: initialState.filterLicense,
          filterCategory: initialState.filterCategory,
          filterCurrentPage: initialState.filterCurrentPage,
        }));

        this.getAllData();
      },
      handleFilterSort(sort: string): void {
        router.navigate([], {
          relativeTo: route,
          queryParams: {
            sort,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterSort: sort,
          filterCurrentPage: initialState.filterCurrentPage,
        }));

        this.getAllData();
      },
      handleFilterTopic(topic: string[]): void {
        const value = !isEmpty(topic) ? topic.join(',') : null;

        router.navigate([], {
          relativeTo: route,
          queryParams: {
            topic: value,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterTopic: topic,
          filterCurrentPage: initialState.filterCurrentPage,
        }));

        this.getAllData();
      },
      handleFilterMapsetType(type: string[]): void {
        const value = !isEmpty(type) ? type.join(',') : null;

        router.navigate([], {
          relativeTo: route,
          queryParams: {
            type: value,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterType: type,
          filterCurrentPage: initialState.filterCurrentPage,
        }));

        this.getAllMapset();
      },
      handleFilterClassification(classification: number[] | string[]): void {
        const value = !isEmpty(classification)
          ? classification.join(',')
          : null;

        router.navigate([], {
          relativeTo: route,
          queryParams: {
            classification: value,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterClassification: classification,
          filterCurrentPage: initialState.filterCurrentPage,
        }));

        this.getAllData();
      },
      handleFilterQuality(quality: string[]): void {
        const value = !isEmpty(quality) ? quality.join(',') : null;

        router.navigate([], {
          relativeTo: route,
          queryParams: {
            quality: value,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterQuality: quality,
          filterCurrentPage: initialState.filterCurrentPage,
        }));

        this.getAllData();
      },
      handleFilterTags(tags: string[]): void {
        const value = !isEmpty(tags) ? tags.join(',') : null;

        router.navigate([], {
          relativeTo: route,
          queryParams: {
            tags: value,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterTags: tags,
          filterCurrentPage: initialState.filterCurrentPage,
        }));

        this.getAllData();
      },
      handleFilterLicense(license: string[]): void {
        const value = !isEmpty(license) ? license.join(',') : null;

        router.navigate([], {
          relativeTo: route,
          queryParams: {
            license: value,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterLicense: license,
          filterCurrentPage: initialState.filterCurrentPage,
        }));

        this.getAllData();
      },
      handleFilterCategory(category: string[]): void {
        const value = !isEmpty(category) ? category.join(',') : null;

        router.navigate([], {
          relativeTo: route,
          queryParams: {
            category: value,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterCategory: category,
          filterCurrentPage: initialState.filterCurrentPage,
        }));

        this.getAllIndicator();
      },
      handleFilterPerPage(perPage: number): void {
        router.navigate([], {
          relativeTo: route,
          queryParams: {
            perPage,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterPerPage: perPage,
          filterCurrentPage: initialState.filterCurrentPage,
        }));

        this.getAllData();
      },
      handleFilterCurrentPage(currentPage: number): void {
        router.navigate([], {
          relativeTo: route,
          queryParams: {
            page: currentPage,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterCurrentPage: currentPage,
        }));

        this.getAllData();
      },
      handleFilterClear(): void {
        router.navigate([], {
          relativeTo: route,
          queryParams: {
            topic: [],
            type: [],
            classification: [],
            quality: [],
            tags: [],
            license: [],
            category: [],
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterTopic: initialState.filterTopic,
          filterType: initialState.filterType,
          filterClassification: initialState.filterClassification,
          filterQuality: initialState.filterQuality,
          filterTags: initialState.filterTags,
          filterLicense: initialState.filterLicense,
          filterCategory: initialState.filterCategory,
          filterCurrentPage: initialState.filterCurrentPage,
        }));

        this.getAllData();
      },
      getAllData(): void {
        const { filterCatalog: catalog } = store;

        switch (catalog()) {
          case 'dataset':
            return this.getAllDataset();
          case 'mapset':
            return this.getAllMapset();
          case 'indicator':
            return this.getAllIndicator();
          case 'visualization':
            return this.getAllVisualization();
          default:
            return null;
        }
      },
      getAllDataset: rxMethod<void>(
        switchMap(() => {
          const {
            filterSearch: search,
            filterTopic: topic,
            filterClassification: classification,
            filterQuality: quality,
            filterTags: tags,
            filterLicense: license,
            filterSort: sort,
            filterPerPage: perPage,
            filterCurrentPage: currentPage,
            listOrganizationSelf: organization,
          } = store;

          const pWhere = {};

          assign(pWhere, STATUS_DATASET.get('approve-active'));
          assign(pWhere, { kode_skpd: organization().kode_skpd });

          if (!isEmpty(topic())) {
            assign(pWhere, { sektoral_id: topic() });
          }

          if (!isEmpty(classification())) {
            assign(pWhere, { dataset_class_id: classification() });
          }

          if (!isEmpty(quality())) {
            const pQuality = quality().map((item) => {
              const data = QUALITY_DATA.get(item);
              return data.name;
            });

            assign(pWhere, {
              category: 'Data Agregat',
              quality_score_label: pQuality,
            });
          }

          if (!isEmpty(tags())) {
            assign(pWhere, { tags: tags() });
          }

          if (!isEmpty(license())) {
            assign(pWhere, { license_id: license() });
          }

          const params = {
            search: search() || '',
            sort: SORT_DATASET.get(sort()).paramSort,
            skip: perPage() * currentPage() - perPage(),
            limit: perPage(),
            where: JSON.stringify(pWhere),
          };

          return datasetService.getQueryList(params).result$.pipe(
            tapResponse({
              next: (result) => {
                patchState(store, { listDataset: result });
              },
              error: console.error,
            }),
          );
        }),
      ),
      getAllMapset: rxMethod<void>(
        switchMap(() => {
          const {
            filterSearch: search,
            filterTopic: topic,
            filterType: type,
            filterClassification: classification,
            filterSort: sort,
            filterPerPage: perPage,
            filterCurrentPage: currentPage,
            listOrganizationSelf: organization,
          } = store;

          const pWhere = {};

          assign(pWhere, STATUS_MAPSET.get('approve-active'));
          assign(pWhere, { kode_skpd: organization().kode_skpd });

          if (!isEmpty(topic())) {
            assign(pWhere, { sektoral_id: topic() });
          }

          if (!isEmpty(type())) {
            assign(pWhere, { mapset_type_id: type() });
          }

          if (!isEmpty(classification())) {
            assign(pWhere, { dataset_class_id: classification() });
          }

          const params = {
            search: search() || '',
            sort: SORT_MAPSET.get(sort()).paramSort,
            skip: perPage() * currentPage() - perPage(),
            limit: perPage(),
            where: JSON.stringify(pWhere),
          };

          return mapsetService.getQueryList(params).result$.pipe(
            tapResponse({
              next: (result) => {
                patchState(store, { listMapset: result });
              },
              error: console.error,
            }),
          );
        }),
      ),
      getAllIndicator: rxMethod<void>(
        switchMap(() => {
          const {
            filterSearch: search,
            filterClassification: classification,
            filterCategory: category,
            filterSort: sort,
            filterPerPage: perPage,
            filterCurrentPage: currentPage,
            listOrganizationSelf: organization,
          } = store;

          const pWhere = {};

          assign(pWhere, STATUS_INDICATOR.get('approve-active'));
          assign(pWhere, { kode_skpd: organization().kode_skpd });

          if (!isEmpty(classification())) {
            assign(pWhere, { classification: classification() });
          }

          if (!isEmpty(category())) {
            assign(pWhere, { indikator_category_id: category() });
          }

          const params = {
            search: search() || '',
            sort: SORT_INDICATOR.get(sort()).paramSort,
            skip: perPage() * currentPage() - perPage(),
            limit: perPage(),
            where: JSON.stringify(pWhere),
          };

          return indicatorService.getQueryList(params).result$.pipe(
            tapResponse({
              next: (result) => {
                patchState(store, { listIndicator: result });
              },
              error: console.error,
            }),
          );
        }),
      ),
      getAllVisualization: rxMethod<void>(
        switchMap(() => {
          const {
            filterSearch: search,
            filterTopic: topic,
            filterClassification: classification,
            filterSort: sort,
            filterPerPage: perPage,
            filterCurrentPage: currentPage,
            listOrganizationSelf: organization,
          } = store;

          const pWhere = {};

          assign(pWhere, STATUS_VISUALIZATION.get('approve-active'));
          assign(pWhere, { kode_skpd: organization().kode_skpd });

          if (!isEmpty(topic())) {
            assign(pWhere, { sektoral_id: topic() });
          }

          if (!isEmpty(classification())) {
            assign(pWhere, { dataset_class_id: classification() });
          }

          const params = {
            search: search() || '',
            sort: SORT_VISUALIZATION.get(sort()).paramSort,
            skip: perPage() * currentPage() - perPage(),
            limit: perPage(),
            where: JSON.stringify(pWhere),
          };

          return visualizationService.getQueryList(params).result$.pipe(
            tapResponse({
              next: (result) => {
                patchState(store, { listVisualization: result });
              },
              error: console.error,
            }),
          );
        }),
      ),
    }),
  ),
);
