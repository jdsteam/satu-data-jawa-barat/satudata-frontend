import { inject } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivateFn, Routes } from '@angular/router';

// SERVICE
import { OrganizationDetailService } from './containers/organization-detail/organization-detail.service';

const canActivateOrganizationDetail: CanActivateFn = (
  route: ActivatedRouteSnapshot,
) => {
  return inject(OrganizationDetailService).canActivate(route, route.params.id);
};

export const ORGANIZATION_ROUTES: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        loadComponent: () =>
          import(
            './containers/organization-page/organization-page.component'
          ).then((m) => m.OrganizationPageComponent),
      },
      {
        path: ':id',
        canActivate: [canActivateOrganizationDetail],
        loadComponent: () =>
          import(
            './containers/organization-detail/organization-detail.component'
          ).then((m) => m.OrganizationDetailComponent),
        resolve: {
          organization: (route: ActivatedRouteSnapshot) =>
            inject(OrganizationDetailService).resolve(route),
        },
      },
    ],
  },
];
