import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// STYLE
import * as jds from '@styles';
// INTERFACE
import { Styles, Breadcrumb } from '@interfaces-v4';
// COMPONENT
import { HeaderCatalogComponent } from '@components-v4';

import { EWalidataTabTypeComponent } from '../../components';

@Component({
  selector: 'app-e-walidata-normal',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    HeaderCatalogComponent,
    EWalidataTabTypeComponent,
  ],
  templateUrl: './e-walidata-normal.component.html',
})
export class EWalidataNormalComponent {
  jds: Styles = jds;

  // Setting
  title: string;
  label = 'E-Walidata';
  description =
    'Integrasi antara Satu Data Jabar dan SIPD Perencanaan bertujuan untuk meningkatkan efektivitas dan efisiensi dalam pengelolaan data dan perencanaan pembangunan di Provinsi Jawa Barat.';
  breadcrumb: Breadcrumb[] = [{ label: 'E-Walidata', link: '/e-walidata' }];
}
