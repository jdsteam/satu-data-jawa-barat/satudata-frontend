import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// STYLE
import * as jds from '@styles';
// INTERFACE
import { Styles } from '@interfaces-v4';
// COMPONENT
import { HeaderCatalogComponent } from '@components-v4';

import { EWalidataTabMonitorTypeComponent } from '../../components';

@Component({
  selector: 'app-e-walidata-monitor',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    HeaderCatalogComponent,
    EWalidataTabMonitorTypeComponent,
  ],
  templateUrl: './e-walidata-monitor.component.html',
})
export class EWalidataMonitorComponent {
  jds: Styles = jds;
}
