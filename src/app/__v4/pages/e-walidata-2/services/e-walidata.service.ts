import { Injectable, inject } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';

// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';

@Injectable({
  providedIn: 'root',
})
export class EWalidataService {
  satudataUser: LoginData;

  // Service
  private router = inject(Router);
  private authenticationService = inject(AuthenticationService);

  constructor() {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
  }

  canActivate(): Observable<boolean> {
    let access: boolean;
    switch (this.satudataUser.role_name) {
      case 'walidata':
      case 'opd':
        access = true;
        break;
      case 'opd-view':
        access = false;
        break;
      default:
        break;
    }

    if (!access) {
      this.router.navigate(['/404']);
      return of(false);
    }

    return of(true);
  }
}
