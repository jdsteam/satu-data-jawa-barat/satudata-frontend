import { inject } from '@angular/core';
import { CanActivateFn, Routes } from '@angular/router';

import { EWalidataService } from './services/e-walidata.service';
import { EWalidataAssignService } from './containers/e-walidata-assign/e-walidata-assign.service';
import { EWalidataMonitorBusinessFieldService } from './containers/e-walidata-monitor-business-field/e-walidata-monitor-business-field.service';
import { EWalidataMonitorIndicatorService } from './containers/e-walidata-monitor-indicator/e-walidata-monitor-indicator.service';
import { EWalidataFillService } from './containers/e-walidata-fill/e-walidata-fill.service';

const canActivateEWalidata: CanActivateFn = () => {
  return inject(EWalidataService).canActivate();
};
const canActivateEWalidataAssign: CanActivateFn = () => {
  return inject(EWalidataAssignService).canActivate();
};
const canActivateEWalidataMonitorBusinessField: CanActivateFn = () => {
  return inject(EWalidataMonitorBusinessFieldService).canActivate();
};
const canActivateEWalidataMonitorIndicator: CanActivateFn = () => {
  return inject(EWalidataMonitorIndicatorService).canActivate();
};
const canActivateEWalidataFill: CanActivateFn = () => {
  return inject(EWalidataFillService).canActivate();
};

export const E_WALIDATA_ROUTES: Routes = [
  {
    path: '',
    canActivate: [canActivateEWalidata],
    loadComponent: () =>
      import('./layouts/e-walidata-normal/e-walidata-normal.component').then(
        (m) => m.EWalidataNormalComponent,
      ),
    children: [
      {
        path: '',
        redirectTo: 'assign',
        pathMatch: 'full',
      },
      {
        path: 'assign',
        canActivate: [canActivateEWalidataAssign],
        loadComponent: () =>
          import(
            './containers/e-walidata-assign/e-walidata-assign.component'
          ).then((m) => m.EWalidataAssignComponent),
      },
      {
        path: 'monitor',
        loadComponent: () =>
          import(
            './layouts/e-walidata-monitor/e-walidata-monitor.component'
          ).then((m) => m.EWalidataMonitorComponent),
        children: [
          {
            path: '',
            redirectTo: 'business-field',
            pathMatch: 'full',
          },
          {
            path: 'business-field',
            canActivate: [canActivateEWalidataMonitorBusinessField],
            loadComponent: () =>
              import(
                './containers/e-walidata-monitor-business-field/e-walidata-monitor-business-field.component'
              ).then((m) => m.EWalidataMonitorBusinessFieldComponent),
          },
          {
            path: 'indicator',
            canActivate: [canActivateEWalidataMonitorIndicator],
            loadComponent: () =>
              import(
                './containers/e-walidata-monitor-indicator/e-walidata-monitor-indicator.component'
              ).then((m) => m.EWalidataMonitorIndicatorComponent),
          },
        ],
      },
      {
        path: 'fill',
        canActivate: [canActivateEWalidataFill],
        loadComponent: () =>
          import('./containers/e-walidata-fill/e-walidata-fill.component').then(
            (m) => m.EWalidataFillComponent,
          ),
      },
    ],
  },
];
