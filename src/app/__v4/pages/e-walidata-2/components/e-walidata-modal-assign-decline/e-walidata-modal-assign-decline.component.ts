import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Output,
  inject,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  FormsModule,
  ReactiveFormsModule,
  UntypedFormArray,
  UntypedFormControl,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { delay, map, tap } from 'rxjs';

// STYLE
import * as jds from '@styles';
// CONSTANT
import {
  COLORSCHEME,
  STATUS_E_WALIDATA_ASSIGN_ASSIGNMENT,
} from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';
// JDS-BI
import { jdsBiColor } from '@jds-bi/cdk';
import {
  JdsBiButtonModule,
  JdsBiModalModule,
  JdsBiModalService,
  JdsBiInputModule,
  JdsBiScrollbarModule,
  JdsBiBadgeModule,
} from '@jds-bi/core';
import { JdsBiIconsModule, JdsBiIconsService, iconXmark } from '@jds-bi/icons';

import moment from 'moment';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { LetDirective } from '@ngrx/component';
import { useMutationResult } from '@ngneat/query';
import { SubscribeDirective } from '@ngneat/subscribe';
import { NgxLoadingModule } from 'ngx-loading';
import Swal from 'sweetalert2';
import { NgSelectModule } from '@ng-select/ng-select';

import { EWalidataAssignService } from '../../containers/e-walidata-assign/e-walidata-assign.service';
import { EWalidataAssignStore } from '../../containers/e-walidata-assign/e-walidata-assign.store';

@Component({
  selector: 'app-e-walidata-modal-assign-decline',
  standalone: true,
  imports: [
    CommonModule,
    JdsBiButtonModule,
    JdsBiIconsModule,
    JdsBiModalModule,
    JdsBiInputModule,
    JdsBiScrollbarModule,
    JdsBiBadgeModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSkeletonLoaderModule,
    LetDirective,
    NgxLoadingModule,
    SubscribeDirective,
    NgSelectModule,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './e-walidata-modal-assign-decline.component.html',
})
export class EWalidataModalAssignDeclineComponent {
  satudataUser: LoginData;
  colorScheme = COLORSCHEME;
  jds: Styles = jds;
  jdsBiColor = jdsBiColor;

  @Output() removeAllSelected = new EventEmitter<any>();

  // Service
  private authenticationService = inject(AuthenticationService);
  private jdsBiModalService = inject(JdsBiModalService);
  private jdsBiIconsService = inject(JdsBiIconsService);
  private eWalidataAssignService = inject(EWalidataAssignService);
  private eWalidataAssignStore = inject(EWalidataAssignStore);
  declineIndicatorMutation = useMutationResult();

  // Variable
  formGroup = new UntypedFormGroup({
    business_field: new UntypedFormArray([]),
  });
  selectedIndicator: any;
  selectedIndicatorGroup: any;
  statusAssignment: any[] = [
    { value: 'Dikembalikan', label: 'Dikembalikan' },
    { value: 'Pengajuan Nonaktif', label: 'Nonaktifkan' },
  ];

  // Component Store
  readonly vm$ = this.eWalidataAssignStore.vm$;

  constructor() {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    this.eWalidataAssignStore.getSelectedIndicator$.subscribe(
      (value) => (this.selectedIndicator = value),
    );

    this.eWalidataAssignStore.getSelectedIndicatorGroup$.subscribe(
      (value) => (this.selectedIndicatorGroup = value),
    );

    this.jdsBiIconsService.registerIcons([iconXmark]);
  }

  get f() {
    return this.formGroup.controls;
  }

  get fdbf() {
    return this.f.business_field as UntypedFormArray;
  }

  openModal() {
    this.jdsBiModalService.open('e-walidata-modal-assign-decline');
    this.eWalidataAssignStore.getAllOrganizationSatuData();
    this.createForm();
  }

  closeModal() {
    this.jdsBiModalService.close('e-walidata-modal-assign-decline');
  }

  createForm() {
    this.formGroup = new UntypedFormGroup({
      business_field: new UntypedFormArray([]),
    });

    this.selectedIndicatorGroup.forEach((item) => {
      this.fdbf.push(this.createFormBusinessField(item));
    });
  }

  createFormBusinessField(value: any): UntypedFormGroup {
    const indicator = [];
    value.data.forEach((item) => {
      indicator.push(this.createFormIndicator(item));
    });

    return new UntypedFormGroup({
      code: new UntypedFormControl(value.businessFieldId),
      name: new UntypedFormControl(value.businessFieldName),
      indicator: new UntypedFormArray(indicator),
    });
  }

  createFormIndicator(value: any): UntypedFormGroup {
    return new UntypedFormGroup({
      id: new UntypedFormControl(value.assignmentId),
      code: new UntypedFormControl(value.indicatorId),
      name: new UntypedFormControl(value.indicatorName),
      status: new UntypedFormControl(value.indicatorStatus),
      action: new UntypedFormControl(null, [Validators.required]),
      note: new UntypedFormControl(null, [Validators.required]),
    });
  }

  getStatusAssignment(value: string) {
    return STATUS_E_WALIDATA_ASSIGN_ASSIGNMENT.get(value).color;
  }

  onSubmit() {
    this.closeModal();

    const { business_field: businessField } = this.formGroup.value;

    const body = [];
    businessField.forEach((main) => {
      main.indicator.forEach((item) => {
        body.push({
          id: item.id,
          kode_indikator: item.code,
          kode_skpd: this.satudataUser.kode_skpd,
          status_assignment: item.action,
          history: {
            datetime: moment().format('YYYY-MM-DD HH:mm:ss'),
            status_assignment: item.action,
            notes: item.note,
          },
        });
      });
    });

    this.eWalidataAssignService
      .updateIndicator(body)
      .pipe(this.declineIndicatorMutation.track())
      .pipe(
        tap(() => {
          this.selectedIndicatorGroup.forEach((item) => {
            this.eWalidataAssignStore.getAllEWalidataIndicator(
              item.businessFieldId,
            );
          });
        }),
        delay(1000),
        map(() => {
          this.removeAllSelected.emit();
        }),
      )
      .subscribe({
        next: () => {
          Swal.fire({
            type: 'success',
            text: 'Uraian berhasil ditolak.',
            allowOutsideClick: false,
          });
        },
        error: () => {
          Swal.fire({
            type: 'error',
            text: 'Terjadi kesalahan.',
            allowOutsideClick: false,
          });
        },
      });
  }
}
