import {
  ChangeDetectionStrategy,
  Component,
  ViewChild,
  inject,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { delay, map, tap } from 'rxjs';

// STYLE
import * as jds from '@styles';
// CONSTANT
import {
  COLORSCHEME,
  STATUS_E_WALIDATA_ASSIGN_ASSIGNMENT,
} from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';
// JDS-BI
import { jdsBiColor } from '@jds-bi/cdk';
import {
  JdsBiButtonModule,
  JdsBiBadgeModule,
  JdsBiScrollbarModule,
} from '@jds-bi/core';

import { NgxPopperjsModule } from 'ngx-popperjs';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import Swal from 'sweetalert2';
import { QueryClientService, useMutationResult } from '@ngneat/query';
import { SubscribeDirective } from '@ngneat/subscribe';
import { NgxLoadingModule } from 'ngx-loading';
import moment from 'moment';

import { EWalidataModalAssignComponent } from '../e-walidata-modal-assign/e-walidata-modal-assign.component';
import { EWalidataModalAssignDeclineComponent } from '../e-walidata-modal-assign-decline/e-walidata-modal-assign-decline.component';

import { EWalidataAssignService } from '../../containers/e-walidata-assign/e-walidata-assign.service';
import { EWalidataAssignStore } from '../../containers/e-walidata-assign/e-walidata-assign.store';

@Component({
  selector: 'app-e-walidata-offcanvas-assign',
  standalone: true,
  imports: [
    CommonModule,
    JdsBiButtonModule,
    JdsBiBadgeModule,
    JdsBiScrollbarModule,
    NgxPopperjsModule,
    NgxSkeletonLoaderModule,
    SubscribeDirective,
    NgxLoadingModule,
    EWalidataModalAssignComponent,
    EWalidataModalAssignDeclineComponent,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './e-walidata-offcanvas-assign.component.html',
  styleUrls: ['./e-walidata-offcanvas-assign.component.scss'],
})
export class EWalidataOffcanvasAssignComponent {
  satudataUser: LoginData;
  colorScheme = COLORSCHEME;
  jdsBiColor = jdsBiColor;
  jds: Styles = jds;
  moment = moment;

  @ViewChild(EWalidataModalAssignComponent)
  modalAssignment: EWalidataModalAssignComponent;

  @ViewChild(EWalidataModalAssignDeclineComponent)
  modalDecline: EWalidataModalAssignDeclineComponent;

  // Service
  private authenticationService = inject(AuthenticationService);
  private eWalidataAssignService = inject(EWalidataAssignService);
  private eWalidataAssignStore = inject(EWalidataAssignStore);
  private queryClient = inject(QueryClientService);
  nonActiveIndicatorMutation = useMutationResult();
  acceptIndicatorMutation = useMutationResult();

  // Variable
  isOpenDetail = false;
  selectedIndicator: any;
  selectedIndicatorGroup: any;

  // Component Store
  readonly vm$ = this.eWalidataAssignStore.vm$;

  constructor() {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    this.eWalidataAssignStore.getSelectedIndicator$.subscribe(
      (value) => (this.selectedIndicator = value),
    );

    this.eWalidataAssignStore.getSelectedIndicatorGroup$.subscribe(
      (value) => (this.selectedIndicatorGroup = value),
    );
  }

  getStatusAssignment(value: string) {
    return STATUS_E_WALIDATA_ASSIGN_ASSIGNMENT.get(value).color;
  }

  onRemoveAllSelected() {
    this.isOpenDetail = false;
    this.eWalidataAssignStore.removeAllBusinessField();
    this.eWalidataAssignStore.removeAllIndicator();
  }

  onNotActive() {
    const total = this.selectedIndicator.length;

    Swal.fire({
      type: 'warning',
      text: `Nonaktifkan ${total} uraian?`,
      allowOutsideClick: false,
      showCancelButton: true,
      cancelButtonText: 'Batal',
      confirmButtonColor: jdsBiColor.blue['700'],
      confirmButtonText: 'Ya',
      reverseButtons: true,
    }).then((result) => {
      if (result.value) {
        const body = this.selectedIndicator.map((item) => {
          return {
            id: item.assignmentId,
            kode_indikator: item.indicatorId,
            kode_skpd: item.assignmentOrganization,
            status_assignment: 'Nonaktif',
            history: {
              datetime: moment().format('YYYY-MM-DD HH:mm:ss'),
              status_assignment: 'Nonaktif',
            },
          };
        });

        this.eWalidataAssignService
          .updateIndicator(body)
          .pipe(this.nonActiveIndicatorMutation.track())
          .pipe(
            tap(() => {
              this.selectedIndicatorGroup.forEach((item) => {
                this.eWalidataAssignStore.getAllEWalidataIndicator(
                  item.businessFieldId,
                );
              });
            }),
            delay(1000),
            map(() => {
              this.onRemoveAllSelected();
            }),
          )
          .subscribe({
            next: () => {
              Swal.fire({
                type: 'success',
                text: 'Uraian berhasil dinonaktifkan.',
                allowOutsideClick: false,
              });
            },
            error: () => {
              Swal.fire({
                type: 'error',
                text: 'Terjadi kesalahan.',
                allowOutsideClick: false,
              });
            },
          });
      }
    });
  }

  onAssignment() {
    this.modalAssignment.openModal();
  }

  onAccept() {
    const total = this.selectedIndicator.length;

    Swal.fire({
      type: 'warning',
      text: `Terima ${total} uraian?`,
      allowOutsideClick: false,
      showCancelButton: true,
      cancelButtonText: 'Batal',
      confirmButtonColor: jdsBiColor.blue['700'],
      confirmButtonText: 'Ya',
      reverseButtons: true,
    }).then((result) => {
      if (result.value) {
        const body = this.selectedIndicator.map((item) => {
          return {
            id: item.assignmentId,
            kode_indikator: item.indicatorId,
            kode_skpd: item.assignmentOrganization,
            status_assignment: 'Diterima',
            history: {
              datetime: moment().format('YYYY-MM-DD HH:mm:ss'),
              status_assignment: 'Diterima',
            },
          };
        });

        this.eWalidataAssignService
          .updateIndicator(body)
          .pipe(this.acceptIndicatorMutation.track())
          .pipe(
            tap(() => {
              this.selectedIndicatorGroup.forEach((item) => {
                this.eWalidataAssignStore.getAllEWalidataIndicator(
                  item.businessFieldId,
                );
              });
            }),
            delay(1000),
            map(() => {
              this.onRemoveAllSelected();
            }),
          )
          .subscribe({
            next: () => {
              Swal.fire({
                type: 'success',
                text: 'Uraian diterima, silahkan cek tab menu Pengisian untuk mengisi.',
                allowOutsideClick: false,
              });
            },
            error: () => {
              Swal.fire({
                type: 'error',
                text: 'Terjadi kesalahan.',
                allowOutsideClick: false,
              });
            },
          });
      }
    });
  }

  onDecline() {
    this.modalDecline.openModal();
  }
}
