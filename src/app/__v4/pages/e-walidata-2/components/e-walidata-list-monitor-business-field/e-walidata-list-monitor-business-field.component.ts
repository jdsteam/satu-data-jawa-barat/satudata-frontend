import {
  Component,
  ChangeDetectionStrategy,
  inject,
  computed,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// DIRECTIVE
import { StickyColumnDirective, StickyTableDirective } from '@directives-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// JDS-BI
import { jdsBiColor } from '@jds-bi/cdk';
import {
  JdsBiTableHeadModule,
  JdsBiPaginationModule,
  JdsBiDropdownModule,
  JdsBiHostedDropdownModule,
} from '@jds-bi/core';

import { filter, map } from 'lodash';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

import { EWalidataMonitorBusinessFieldStore } from '../../containers/e-walidata-monitor-business-field/e-walidata-monitor-business-field.store';

@Component({
  selector: 'app-e-walidata-list-monitor-business-field',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    StickyColumnDirective,
    StickyTableDirective,
    JdsBiTableHeadModule,
    JdsBiPaginationModule,
    JdsBiDropdownModule,
    JdsBiHostedDropdownModule,
    NgSelectModule,
    NgxSkeletonLoaderModule,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './e-walidata-list-monitor-business-field.component.html',
  styleUrls: ['./e-walidata-list-monitor-business-field.component.scss'],
})
export class EWalidataListMonitorBusinessFieldComponent {
  colorScheme = COLORSCHEME;
  jds: Styles = jds;
  jdsBiColor = jdsBiColor;

  // Service
  private eWalidataMonitorStore = inject(EWalidataMonitorBusinessFieldStore);

  // Variable
  isOpenOrganization = false;
  perPageItems = [
    { value: 10, label: '10' },
    { value: 25, label: '25' },
  ];
  listOrganization = computed(() => {
    return this.eWalidataMonitorStore.getListOrganizationSatuData$();
  });

  // Component Store
  readonly vm$ = this.eWalidataMonitorStore.vm$;

  onPerPage(value: number) {
    this.eWalidataMonitorStore.filterPerPage(value);
  }

  onPage(value: number) {
    this.eWalidataMonitorStore.filterCurrentPage(value);
  }

  onSort(value: any, direction: string) {
    let dir = '';
    if (direction === 'desc') {
      dir = 'asc';
    } else if (direction === 'asc') {
      dir = 'desc';
    }

    this.eWalidataMonitorStore.filterSortTable(`${value}:${dir}`);
  }

  onOpenFilterOrganization() {
    this.eWalidataMonitorStore.getAllOrganizationSatuData();
  }

  onFilterOrganization(code: string, isChecked: boolean) {
    const items = this.listOrganization().data.list;

    items.map((item) => {
      if (item.kode_skpd === code) {
        item.checked = isChecked;
      }

      return item;
    });

    const value = map(filter(items, 'checked'), 'kode_skpd');
    this.eWalidataMonitorStore.filterOrganization(value);
  }
}
