import {
  Component,
  ChangeDetectionStrategy,
  Output,
  EventEmitter,
  Input,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// STYLE
import * as jds from '@styles';
// INTERFACE
import { Styles } from '@interfaces-v4';
// JDS-BI
import { jdsBiDefaultProp } from '@jds-bi/cdk';

import { NgSelectModule } from '@ng-select/ng-select';

@Component({
  selector: 'app-e-walidata-filter-year',
  standalone: true,
  imports: [CommonModule, FormsModule, NgSelectModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './e-walidata-filter-year.component.html',
  styleUrls: ['./e-walidata-filter-year.component.scss'],
})
export class EWalidataFilterYearComponent {
  jds: Styles = jds;

  @Input()
  @jdsBiDefaultProp()
  filterData: string | number;

  @Output() filter = new EventEmitter<any>();

  // Variable
  yearOption = Array.from({ length: 10 }, (_, i) => ({
    value: (new Date().getFullYear() - i).toString(),
    label: (new Date().getFullYear() - i).toString(),
  }));

  onFilter(value: string): void {
    this.filter.emit(value);
  }
}
