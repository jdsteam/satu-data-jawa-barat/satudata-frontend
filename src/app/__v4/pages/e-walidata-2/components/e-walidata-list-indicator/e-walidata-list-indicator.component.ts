import {
  Component,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter,
  inject,
  signal,
  ViewChildren,
  QueryList,
  ElementRef,
  Renderer2,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// DIRECTIVE
import { StickyColumnDirective, StickyTableDirective } from '@directives-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';
// JDS-BI
import { jdsBiColor, jdsBiDefaultProp } from '@jds-bi/cdk';
import {
  JdsBiTableHeadModule,
  JdsBiPaginationModule,
  JdsBiDropdownModule,
  JdsBiHostedDropdownModule,
  JdsBiInputModule,
} from '@jds-bi/core';

import { filter, map } from 'lodash';
import moment from 'moment';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgxLoadingModule } from 'ngx-loading';
import { NgxMaskModule } from 'ngx-mask';
import { useMutationResult } from '@ngneat/query';
import { SubscribeDirective } from '@ngneat/subscribe';
import Swal from 'sweetalert2';

import { EWalidataFillService } from '../../containers/e-walidata-fill/e-walidata-fill.service';

@Component({
  selector: 'app-e-walidata-list-indicator',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    StickyColumnDirective,
    StickyTableDirective,
    JdsBiTableHeadModule,
    JdsBiPaginationModule,
    JdsBiDropdownModule,
    JdsBiHostedDropdownModule,
    JdsBiInputModule,
    NgSelectModule,
    NgxSkeletonLoaderModule,
    NgxLoadingModule,
    NgxMaskModule,
    SubscribeDirective,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './e-walidata-list-indicator.component.html',
  styleUrls: ['./e-walidata-list-indicator.component.scss'],
})
export class EWalidataListIndicatorComponent {
  satudataUser: LoginData;
  colorScheme = COLORSCHEME;
  jds: Styles = jds;
  jdsBiColor = jdsBiColor;
  moment = moment;

  @ViewChildren('tableData') tableData: QueryList<ElementRef>;
  @ViewChildren('contentData') contentData: QueryList<ElementRef>;
  @ViewChildren('inputData') inputData: QueryList<ElementRef>;

  @Input()
  listOrganization: any;

  @Input()
  @jdsBiDefaultProp()
  listIndicator: any;

  @Input()
  @jdsBiDefaultProp()
  listIndicatorYear: any;

  @Input()
  @jdsBiDefaultProp()
  filterPerPageData: any;

  @Input()
  @jdsBiDefaultProp()
  filterCurrentPageData: any;

  @Output() openFilterOrganization = new EventEmitter<any>();
  @Output() filterOrganization = new EventEmitter<any>();
  @Output() filterPerPage = new EventEmitter<any>();
  @Output() filterCurrentPage = new EventEmitter<any>();

  // Service
  private renderer = inject(Renderer2);
  private authenticationService = inject(AuthenticationService);
  private eWalidataFillService = inject(EWalidataFillService);
  updateIndicatorDataMutation = useMutationResult();

  // Variable
  isOpenOrganization = false;
  perPageItems = [
    { value: 15, label: '15' },
    { value: 30, label: '30' },
  ];
  isEditable = signal(false);

  constructor() {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
  }

  getYearData(data: any, year: string) {
    const findData = data.find((item) => {
      return item.tahun === year;
    });

    if (!findData) {
      return {
        data: null,
        verification: false,
        year,
        pemda: '3200',
      };
    }

    return {
      data: Number(findData.data),
      verification: findData.status_verifikasi_walidata === 'Y',
      year,
      pemda: findData.kode_pemda,
    };
  }

  onPerPage(value: number) {
    this.filterPerPage.emit(value);
  }

  onPage(value: number) {
    this.filterCurrentPage.emit(value);
  }

  onOpenFilterOrganization() {
    this.openFilterOrganization.emit();
  }

  onFilterOrganization(code: string, isChecked: boolean) {
    const items = this.listOrganization.data.list;

    items.map((item) => {
      if (item.kode_skpd === code) {
        item.checked = isChecked;
      }

      return item;
    });

    const value = map(filter(items, 'checked'), 'kode_skpd');
    this.filterOrganization.emit(value);
  }

  onClickData(item: any, indexMain: number, indexSub: number, count: number) {
    if (this.satudataUser.role_name !== 'opd' || item.verification) {
      return;
    }

    const index = count * indexMain + indexSub;

    let height = 0;

    this.tableData.forEach((elem, i) => {
      if (index === i) {
        this.renderer.setStyle(elem.nativeElement, 'padding', '0');
        height = elem.nativeElement.offsetHeight;
      }
    });

    this.contentData.forEach((elem, i) => {
      if (index === i) {
        this.renderer.setStyle(elem.nativeElement, 'display', 'none');
      }
    });

    this.inputData.forEach((elem, i) => {
      if (index === i) {
        this.renderer.setStyle(elem.nativeElement, 'display', 'inline-block');
        elem.nativeElement.value = item.data;
        elem.nativeElement.focus();
        this.renderer.setStyle(elem.nativeElement, 'height', `${height - 2}px`);
      }
    });
  }

  onBlurData(item: any, indexMain: number, indexSub: number, count: number) {
    const index = count * indexMain + indexSub;

    this.tableData.forEach((elem, i) => {
      if (index === i) {
        this.renderer.setStyle(elem.nativeElement, 'padding', '10px 16px');
      }
    });

    this.contentData.forEach((elem, i) => {
      if (index === i) {
        this.renderer.setStyle(elem.nativeElement, 'display', 'block');
      }
    });

    this.inputData.forEach((elem, i) => {
      if (index === i) {
        this.renderer.setStyle(elem.nativeElement, 'display', 'none');
      }
    });

    const body = [
      {
        kode_pemda: item.pemda,
        kode_indikator: item.code,
        tahun: item.year,
        data: Number(item.value.replace('.', '')),
      },
    ];

    this.eWalidataFillService
      .updateIndicatorData(body)
      .pipe(this.updateIndicatorDataMutation.track())
      .subscribe({
        next: (response) => {
          if (response.error === 1) {
            Swal.fire({
              type: 'error',
              text: 'Pengisian rincian uraian gagal.',
              allowOutsideClick: false,
            });
          } else {
            Swal.fire({
              type: 'success',
              text: 'Pengisian rincian uraian berhasil.',
              allowOutsideClick: false,
            });
          }
        },
        error: () => {
          Swal.fire({
            type: 'error',
            text: 'Terjadi kesalahan.',
            allowOutsideClick: false,
          });
        },
      });
  }
}
