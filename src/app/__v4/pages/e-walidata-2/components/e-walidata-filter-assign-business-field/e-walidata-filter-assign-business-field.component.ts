import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  inject,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// STYLE
import * as jds from '@styles';
// COMPONENT
import { SkeletonFilterComponent } from '@components-v4';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// JDS-BI
import { jdsBiColor } from '@jds-bi/cdk';
import {
  JdsBiButtonModule,
  JdsBiDropdownModule,
  JdsBiHostedDropdownModule,
  JdsBiInputModule,
  JdsBiScrollbarModule,
  JdsBiBadgeModule,
} from '@jds-bi/core';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconMagnifyingGlass,
  iconCircleXmark,
} from '@jds-bi/icons';

// eslint-disable-next-line object-curly-newline
import { filter, keyBy, map, merge, orderBy, values } from 'lodash';

import { EWalidataAssignStore } from '../../containers/e-walidata-assign/e-walidata-assign.store';

@Component({
  selector: 'app-e-walidata-filter-assign-business-field',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    JdsBiButtonModule,
    JdsBiDropdownModule,
    JdsBiHostedDropdownModule,
    JdsBiInputModule,
    JdsBiIconsModule,
    JdsBiScrollbarModule,
    SkeletonFilterComponent,
    JdsBiBadgeModule,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './e-walidata-filter-assign-business-field.component.html',
})
export class EWalidataFilterAssignBusinessFieldComponent {
  colorScheme = COLORSCHEME;
  jds: Styles = jds;
  jdsBiColor = jdsBiColor;

  // Service
  private cdRef = inject(ChangeDetectorRef);
  private iconService = inject(JdsBiIconsService);
  private eWalidataAssignStore = inject(EWalidataAssignStore);

  // Variable
  isOpen = false;
  filterSearch = null;
  filterData: string[] = [];

  // Data
  data: any[] = [];
  dataTemp: any[] = [];

  readonly vm$ = this.eWalidataAssignStore.vm$;

  constructor() {
    this.eWalidataAssignStore.getListBusinessField$.subscribe((value) => {
      if (value && value.data) {
        this.data = value.data.list;
        this.dataTemp = value.data.list;
        this.createChecklist();
      }
    });

    this.eWalidataAssignStore.getFilterBusinessField$.subscribe((value) => {
      this.filterData = value;
    });

    this.iconService.registerIcons([iconMagnifyingGlass, iconCircleXmark]);
  }

  createChecklist(): any {
    const items = this.data;
    let result: any;

    items.forEach((item) => (item.checked = false));

    if (this.filterData.length) {
      const checked = [];
      this.filterData.forEach((code) => {
        checked.push({ kode_bidang_urusan: code, checked: true });
      });

      const merged = merge(
        keyBy(items, 'kode_bidang_urusan'),
        keyBy(checked, 'kode_bidang_urusan'),
      );
      const vals = values(merged);
      result = orderBy(vals, ['kode_bidang_urusan'], ['asc']);
    } else {
      result = orderBy(items, ['kode_bidang_urusan'], ['asc']);
    }

    this.data = result;
    this.dataTemp = result;
  }

  onSearch(value: string): void {
    this.filterSearch = value;

    const items = this.data;

    if (value !== '') {
      const result = filter(items, (o) => {
        return o.nama_bidang_urusan.toLowerCase().indexOf(value) > -1;
      });

      this.dataTemp = result;
    }
  }

  onClearSearch(): void {
    this.filterSearch = null;
    this.dataTemp = this.data;
  }

  onFilter(code: string, isChecked: boolean): void {
    const items = this.data;

    items.forEach((item) => {
      if (item.kode_bidang_urusan === code) {
        item.checked = isChecked;
      }

      return item;
    });

    const emit = map(filter(items, 'checked'), 'kode_bidang_urusan');
    this.eWalidataAssignStore.filterBusinessField(emit);
  }

  onClearFilter(): void {
    const items = this.data;

    items.forEach((item) => (item.checked = false));

    this.dataTemp = items;

    const emit = map(filter(items, 'checked'), 'kode_bidang_urusan');
    this.eWalidataAssignStore.filterBusinessField(emit);

    this.cdRef.detectChanges();
  }
}
