import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  QueryList,
  ViewChildren,
  inject,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// JDS-BI
import { jdsBiColor } from '@jds-bi/cdk';
import {
  JdsBiAccordionModule,
  JdsBiBadgeModule,
  JdsBiButtonModule,
  JdsBiInputModule,
  JdsBiTableHeadModule,
  JdsBiPaginationModule,
} from '@jds-bi/core';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconMagnifyingGlass,
  iconChevronDown,
} from '@jds-bi/icons';

import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

import { EWalidataListAssignIndicatorComponent } from '../e-walidata-list-assign-indicator/e-walidata-list-assign-indicator.component';

import { EWalidataAssignService } from '../../containers/e-walidata-assign/e-walidata-assign.service';
import { EWalidataAssignStore } from '../../containers/e-walidata-assign/e-walidata-assign.store';

@Component({
  selector: 'app-e-walidata-list-assign',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    JdsBiAccordionModule,
    JdsBiBadgeModule,
    JdsBiButtonModule,
    JdsBiIconsModule,
    JdsBiInputModule,
    JdsBiTableHeadModule,
    JdsBiPaginationModule,
    NgxSkeletonLoaderModule,
    EWalidataListAssignIndicatorComponent,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './e-walidata-list-assign.component.html',
  styleUrls: ['./e-walidata-list-assign.component.scss'],
})
export class EWalidataListAssignComponent {
  colorScheme = COLORSCHEME;
  jdsBiColor = jdsBiColor;
  jds: Styles = jds;

  @ViewChildren('inputGroupSearches') inputGroupSearches: QueryList<ElementRef>;
  @ViewChildren('buttonSearches') buttonSearches: QueryList<ElementRef>;

  // Service
  private eWalidataAssignService = inject(EWalidataAssignService);
  private eWalidataAssignStore = inject(EWalidataAssignStore);
  private iconService = inject(JdsBiIconsService);

  // Variable
  perPageItems: any[] = [
    { value: 10, label: '10' },
    { value: 25, label: '25' },
  ];

  // Component Store
  readonly vm$ = this.eWalidataAssignStore.vm$;

  constructor() {
    this.iconService.registerIcons([iconMagnifyingGlass, iconChevronDown]);
  }

  onToggleSearch(action, index) {
    this.buttonSearches.forEach((elem, i) => {
      if (index === i) {
        elem.nativeElement.style.setProperty(
          'display',
          action === 'open' ? 'none' : 'block',
          'important',
        );
      }
    });

    this.inputGroupSearches.forEach((elem, i) => {
      if (index === i) {
        elem.nativeElement.style.setProperty(
          'display',
          action === 'open' ? 'flex' : 'none',
          'important',
        );
      }
    });
  }

  checkSearch(id: string, value: string) {
    return this.eWalidataAssignService.checkSearch(id, value);
  }

  onSearch(id: string, value: string) {
    this.eWalidataAssignStore.filterSearchIndicator({
      id,
      value,
    });
  }

  checkClearSearch(id: string, value: string) {
    if (this.eWalidataAssignService.checkSearch(id, value)) {
      return true;
    }

    return false;
  }

  onClearSearch(id: string) {
    this.eWalidataAssignStore.filterSearchIndicator({
      id,
      value: null,
    });
  }

  onIndicator(action, code) {
    if (action) {
      this.eWalidataAssignStore.getAllEWalidataIndicator(code);
    }
  }

  onSelectedGroup(obj: any, isChecked: boolean) {
    this.eWalidataAssignStore.addRemoveIndicatorGroup({
      obj,
      isChecked,
    });
  }

  filterPerPage(value: number) {
    this.eWalidataAssignStore.filterPerPage(value);
  }

  filterPage(value: number) {
    this.eWalidataAssignStore.filterCurrentPage(value);
  }
}
