import {
  Component,
  ChangeDetectionStrategy,
  inject,
  ChangeDetectorRef,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavigationEnd, Router, RouterModule } from '@angular/router';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';
// COMPONENT
import { JdsBiButtonModule } from '@jds-bi/core';

@Component({
  selector: 'app-e-walidata-tab-monitor-type',
  standalone: true,
  imports: [CommonModule, RouterModule, JdsBiButtonModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './e-walidata-tab-monitor-type.component.html',
})
export class EWalidataTabMonitorTypeComponent {
  colorScheme = COLORSCHEME;
  satudataUser: LoginData;
  jds: Styles = jds;

  // Service
  private cdRef = inject(ChangeDetectorRef);
  private router = inject(Router);
  private authenticationService = inject(AuthenticationService);

  // Variable
  currentRoute: string;

  constructor() {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    this.getCurrentUrl('init', this.router.url);

    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.getCurrentUrl('change', event.url);
      }
    });
  }

  getCurrentUrl(action: string, url: string) {
    const arr = url.split('?')[0].split('/');

    let current: string;
    if (arr[2]) {
      current = 'business-field';

      if (arr[3]) {
        // eslint-disable-next-line prefer-destructuring
        current = arr[3];
      }
    }

    this.currentRoute = current;

    if (action === 'change') {
      this.cdRef.detectChanges();
    }
  }

  onType(value: string) {
    this.router.navigate(['/e-walidata-2/monitor/', value]);
  }
}
