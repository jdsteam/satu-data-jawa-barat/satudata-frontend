import { ChangeDetectionStrategy, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { SORT_E_WALIDATA_ASSIGN } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';

import { NgSelectModule } from '@ng-select/ng-select';

import { EWalidataAssignStore } from '../../containers/e-walidata-assign/e-walidata-assign.store';

@Component({
  selector: 'app-e-walidata-sort-assign-business-field',
  standalone: true,
  imports: [CommonModule, FormsModule, NgSelectModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './e-walidata-sort-assign-business-field.component.html',
  styleUrls: ['./e-walidata-sort-assign-business-field.component.scss'],
})
export class EWalidataSortAssignBusinessFieldComponent {
  jds: Styles = jds;

  // Variable
  sortOption = Array.from(SORT_E_WALIDATA_ASSIGN, ([, value]) => ({
    value: value.id,
    label: value.name,
  }));

  // Component Store
  readonly vm$ = this.eWalidataAssignStore.vm$;

  constructor(private eWalidataAssignStore: EWalidataAssignStore) {}

  onSort(value: string): void {
    this.eWalidataAssignStore.filterSort(value);
  }
}
