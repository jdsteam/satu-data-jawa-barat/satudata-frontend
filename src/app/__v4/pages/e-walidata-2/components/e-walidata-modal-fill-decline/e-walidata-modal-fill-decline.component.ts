import { Component, computed, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  FormArray,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  UntypedFormArray,
  UntypedFormControl,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';
// JDS-BI
import { jdsBiColor } from '@jds-bi/cdk';
import {
  JdsBiButtonModule,
  JdsBiModalModule,
  JdsBiModalService,
  JdsBiInputModule,
  JdsBiScrollbarModule,
  JdsBiAccordionModule,
} from '@jds-bi/core';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconXmark,
  iconChevronDown,
} from '@jds-bi/icons';

import { isEmpty } from 'lodash';
import moment from 'moment';
import { LetDirective } from '@ngrx/component';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgxLoadingModule } from 'ngx-loading';
import { useMutationResult } from '@ngneat/query';
import Swal from 'sweetalert2';
import { SubscribeDirective } from '@ngneat/subscribe';

import { EWalidataFillService } from '../../containers/e-walidata-fill/e-walidata-fill.service';
import { EWalidataFillStore } from '../../containers/e-walidata-fill/e-walidata-fill.store';

@Component({
  selector: 'app-e-walidata-modal-fill-decline',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    JdsBiButtonModule,
    JdsBiIconsModule,
    JdsBiModalModule,
    JdsBiInputModule,
    JdsBiScrollbarModule,
    JdsBiAccordionModule,
    NgxSkeletonLoaderModule,
    NgxLoadingModule,
    NgSelectModule,
    LetDirective,
    SubscribeDirective,
  ],
  // changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './e-walidata-modal-fill-decline.component.html',
  styleUrls: ['./e-walidata-modal-fill-decline.component.scss'],
})
export class EWalidataModalFillDeclineComponent {
  satudataUser: LoginData;
  colorScheme = COLORSCHEME;
  jds: Styles = jds;
  jdsBiColor = jdsBiColor;

  // Service
  private authenticationService = inject(AuthenticationService);
  private jdsBiModalService = inject(JdsBiModalService);
  private jdsBiIconsService = inject(JdsBiIconsService);
  private eWalidataFillService = inject(EWalidataFillService);
  private eWalidataFillStore = inject(EWalidataFillStore);
  declineIndicatorMutation = useMutationResult();

  // Variable
  formGroup = new UntypedFormGroup({
    business_field: new UntypedFormArray([]),
  });
  statusAssignment: any[] = [
    { value: 'Dikembalikan', label: 'Dikembalikan' },
    { value: 'Pengajuan Nonaktif', label: 'Nonaktifkan' },
  ];

  // Data
  listBusinessField = computed(() => {
    return this.eWalidataFillStore.getListEWalidataFillBusinessFieldMonitor$();
  });
  listIndicatorGroup = computed(() => {
    return this.eWalidataFillStore.getListEWalidataFillIndicatorMonitorGroup$();
  });

  // Component Store
  readonly vm$ = this.eWalidataFillStore.vm$;

  constructor() {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    this.jdsBiIconsService.registerIcons([iconXmark, iconChevronDown]);
  }

  get f() {
    return this.formGroup.controls;
  }

  get fdbf() {
    return this.f.business_field as UntypedFormArray;
  }

  openModal() {
    this.jdsBiModalService.open('e-walidata-modal-fill-decline');
    this.createForm();
  }

  closeModal() {
    this.jdsBiModalService.close('e-walidata-modal-fill-decline');
  }

  createForm() {
    this.formGroup = new UntypedFormGroup({
      business_field: new UntypedFormArray([]),
    });

    this.listBusinessField().data.list.forEach((item) => {
      this.fdbf.push(this.createFormBusinessField(item));
    });
  }

  createFormBusinessField(value: any): UntypedFormGroup {
    const indicator = [];

    if (this.listIndicatorGroup()[value.kode_bidang_urusan]) {
      this.listIndicatorGroup()[value.kode_bidang_urusan].forEach((item) => {
        indicator.push(this.createFormIndicator(item));
      });
    }

    return new UntypedFormGroup({
      code: new UntypedFormControl(value.kode_bidang_urusan),
      name: new UntypedFormControl(value.nama_bidang_urusan),
      indicator: new UntypedFormArray(indicator),
    });
  }

  createFormIndicator(value: any): UntypedFormGroup {
    return new UntypedFormGroup({
      id: new UntypedFormControl(value.id_assignment),
      code: new UntypedFormControl(value.kode_indikator),
      name: new UntypedFormControl(value.nama_indikator),
      checked: new UntypedFormControl(false),
      action: new UntypedFormControl({ value: null, disabled: true }),
      note: new UntypedFormControl({ value: null, disabled: true }),
    });
  }

  onSelected(isChecked: boolean, i, j) {
    const arrayBF = this.formGroup.controls.business_field as FormArray;
    const groupBF = arrayBF.controls[i] as FormGroup;
    const arrayI = groupBF.controls.indicator as FormArray;
    const groupI = arrayI.controls[j] as FormGroup;

    if (isChecked) {
      groupI.controls['action'].enable();
      groupI.controls['note'].enable();

      groupI.controls['action'].addValidators(Validators.required);
      groupI.controls['note'].addValidators(Validators.required);
    } else {
      groupI.controls['action'].disable();
      groupI.controls['note'].disable();

      groupI.controls['action'].clearValidators();
      groupI.controls['note'].clearValidators();
    }

    groupI.controls['action'].updateValueAndValidity();
    groupI.controls['note'].updateValueAndValidity();
  }

  onSubmit() {
    this.closeModal();

    const { business_field: businessField } = this.formGroup.value;

    const body = [];
    businessField.forEach((main) => {
      main.indicator.forEach((item) => {
        if (item.checked) {
          body.push({
            id: item.id,
            kode_indikator: item.code,
            kode_skpd: this.satudataUser.kode_skpd,
            status_assignment: item.action,
            history: {
              datetime: moment().format('YYYY-MM-DD HH:mm:ss'),
              status_assignment: item.action,
              notes: item.note,
            },
          });
        }
      });
    });

    if (isEmpty(body)) {
      Swal.fire({
        type: 'error',
        text: 'Tidak ada uraian yg ditolak.',
        allowOutsideClick: false,
      });
      return;
    }

    this.eWalidataFillService
      .updateIndicator(body)
      .pipe(this.declineIndicatorMutation.track())
      .subscribe({
        next: () => {
          Swal.fire({
            type: 'success',
            text: 'Uraian berhasil ditolak.',
            allowOutsideClick: false,
          });
        },
        error: () => {
          Swal.fire({
            type: 'error',
            text: 'Terjadi kesalahan.',
            allowOutsideClick: false,
          });
        },
      });
  }
}
