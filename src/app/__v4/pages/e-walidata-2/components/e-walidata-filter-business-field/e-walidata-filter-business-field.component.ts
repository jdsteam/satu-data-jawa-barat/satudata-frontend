import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// STYLE
import * as jds from '@styles';
// INTERFACE
import { Styles } from '@interfaces-v4';
// JDS-BI
import { jdsBiDefaultProp } from '@jds-bi/cdk';

import { NgSelectModule } from '@ng-select/ng-select';

@Component({
  selector: 'app-e-walidata-filter-business-field',
  standalone: true,
  imports: [CommonModule, FormsModule, NgSelectModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './e-walidata-filter-business-field.component.html',
  styleUrls: ['./e-walidata-filter-business-field.component.scss'],
})
export class EWalidataFilterBusinessFieldComponent {
  jds: Styles = jds;

  @Input()
  @jdsBiDefaultProp()
  filterData: string | number;

  @Input()
  @jdsBiDefaultProp()
  listBusinessField: any;

  @Output() filter = new EventEmitter<any>();

  onFilter(value: string): void {
    this.filter.emit(value);
  }
}
