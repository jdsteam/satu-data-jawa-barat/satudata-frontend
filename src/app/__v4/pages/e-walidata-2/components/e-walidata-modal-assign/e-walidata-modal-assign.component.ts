import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Output,
  inject,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  FormsModule,
  ReactiveFormsModule,
  UntypedFormControl,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { delay, map, of, tap, zip } from 'rxjs';

// STYLE
import * as jds from '@styles';
// CONSTANT
import {
  COLORSCHEME,
  STATUS_E_WALIDATA_ASSIGN_ASSIGNMENT,
} from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// JDS-BI
import { jdsBiColor } from '@jds-bi/cdk';
import {
  JdsBiButtonModule,
  JdsBiModalModule,
  JdsBiModalService,
  JdsBiInputModule,
  JdsBiScrollbarModule,
  JdsBiBadgeModule,
} from '@jds-bi/core';
import { JdsBiIconsModule, JdsBiIconsService, iconXmark } from '@jds-bi/icons';

import { isEmpty } from 'lodash';
import moment from 'moment';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { LetDirective } from '@ngrx/component';
import { useMutationResult } from '@ngneat/query';
import { SubscribeDirective } from '@ngneat/subscribe';
import { NgxLoadingModule } from 'ngx-loading';
import Swal from 'sweetalert2';
import { NgSelectModule } from '@ng-select/ng-select';

import { EWalidataAssignService } from '../../containers/e-walidata-assign/e-walidata-assign.service';
import { EWalidataAssignStore } from '../../containers/e-walidata-assign/e-walidata-assign.store';

@Component({
  selector: 'app-e-walidata-modal-assign',
  standalone: true,
  imports: [
    CommonModule,
    JdsBiButtonModule,
    JdsBiIconsModule,
    JdsBiModalModule,
    JdsBiInputModule,
    JdsBiScrollbarModule,
    JdsBiBadgeModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSkeletonLoaderModule,
    LetDirective,
    NgxLoadingModule,
    SubscribeDirective,
    NgSelectModule,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './e-walidata-modal-assign.component.html',
})
export class EWalidataModalAssignComponent {
  colorScheme = COLORSCHEME;
  jds: Styles = jds;
  jdsBiColor = jdsBiColor;

  @Output() removeAllSelected = new EventEmitter<any>();

  // Service
  private jdsBiModalService = inject(JdsBiModalService);
  private jdsBiIconsService = inject(JdsBiIconsService);
  private eWalidataAssignService = inject(EWalidataAssignService);
  private eWalidataAssignStore = inject(EWalidataAssignStore);
  assignmentIndicatorMutation = useMutationResult();

  // Variable
  formGroup = new UntypedFormGroup({
    organization: new UntypedFormControl(null, [Validators.required]),
  });
  selectedIndicator: any;
  selectedIndicatorGroup: any;

  // Component Store
  readonly vm$ = this.eWalidataAssignStore.vm$;

  constructor() {
    this.eWalidataAssignStore.getSelectedIndicator$.subscribe(
      (value) => (this.selectedIndicator = value),
    );

    this.eWalidataAssignStore.getSelectedIndicatorGroup$.subscribe(
      (value) => (this.selectedIndicatorGroup = value),
    );

    this.jdsBiIconsService.registerIcons([iconXmark]);
  }

  get f() {
    return this.formGroup.controls;
  }

  openModal() {
    this.jdsBiModalService.open('e-walidata-modal-assign');
    this.eWalidataAssignStore.getAllOrganizationSatuData();
  }

  closeModal() {
    this.jdsBiModalService.close('e-walidata-modal-assign');
  }

  getStatusAssignment(value: string) {
    return STATUS_E_WALIDATA_ASSIGN_ASSIGNMENT.get(value).color;
  }

  onSubmit() {
    this.closeModal();

    const { organization } = this.formGroup.value;

    const bodyCreate = [];
    const bodyUpdate = [];
    this.selectedIndicator.forEach((item) => {
      if (isEmpty(item.assignmentOrganization)) {
        bodyCreate.push({
          kode_indikator: item.indicatorId,
          kode_skpd: organization.kode_skpd,
          status_assignment: 'Ditugaskan',
          history: {
            datetime: moment().format('YYYY-MM-DD HH:mm:ss'),
            status_assignment: 'Ditugaskan',
          },
        });
      } else {
        bodyUpdate.push({
          id: item.assignmentId,
          kode_indikator: item.indicatorId,
          kode_skpd: organization.kode_skpd,
          status_assignment: 'Ditugaskan',
          history: {
            datetime: moment().format('YYYY-MM-DD HH:mm:ss'),
            status_assignment: 'Ditugaskan',
          },
        });
      }
    });

    const create = !isEmpty(bodyCreate)
      ? this.eWalidataAssignService.createIndicator(bodyCreate)
      : of('Create');
    const update = !isEmpty(bodyUpdate)
      ? this.eWalidataAssignService.updateIndicator(bodyUpdate)
      : of('Update');

    zip(create, update)
      .pipe(this.assignmentIndicatorMutation.track())
      .pipe(
        tap(() => {
          this.selectedIndicatorGroup.forEach((item) => {
            this.eWalidataAssignStore.getAllEWalidataIndicator(
              item.businessFieldId,
            );
          });
        }),
        delay(1000),
        map(() => {
          this.removeAllSelected.emit();
        }),
      )
      .subscribe({
        next: () => {
          Swal.fire({
            type: 'success',
            text: 'Uraian berhasil ditugaskan.',
            allowOutsideClick: false,
          });

          this.formGroup.reset();
        },
        error: () => {
          Swal.fire({
            type: 'error',
            text: 'Terjadi kesalahan.',
            allowOutsideClick: false,
          });
        },
      });
  }
}
