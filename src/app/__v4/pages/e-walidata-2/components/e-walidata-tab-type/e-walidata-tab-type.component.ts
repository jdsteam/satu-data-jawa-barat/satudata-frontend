import { Component, ChangeDetectionStrategy, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, RouterModule } from '@angular/router';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';
// COMPONENT
import { JdsBiNavModule } from '@jds-bi/core';

@Component({
  selector: 'app-e-walidata-tab-type',
  standalone: true,
  imports: [CommonModule, RouterModule, JdsBiNavModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './e-walidata-tab-type.component.html',
})
export class EWalidataTabTypeComponent {
  colorScheme = COLORSCHEME;
  satudataUser: LoginData;
  jds: Styles = jds;

  // Service
  private router = inject(Router);
  private authenticationService = inject(AuthenticationService);

  // Variable
  currentRoute: string;

  constructor() {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    // eslint-disable-next-line prefer-destructuring
    this.currentRoute = this.router.url.split('?')[0].split('/')[2];
  }
}
