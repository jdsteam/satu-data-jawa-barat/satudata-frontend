import { Component, ChangeDetectionStrategy, inject } from '@angular/core';
import { CommonModule } from '@angular/common';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// JDS-BI
import { jdsBiColor } from '@jds-bi/cdk';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconClockThree,
} from '@jds-bi/icons';

import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

import { EWalidataFillStore } from '../../containers/e-walidata-fill/e-walidata-fill.store';

@Component({
  selector: 'app-e-walidata-list-fill-business-field',
  standalone: true,
  imports: [CommonModule, JdsBiIconsModule, NgxSkeletonLoaderModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './e-walidata-list-fill-business-field.component.html',
})
export class EWalidataListFillBusinessFieldComponent {
  colorScheme = COLORSCHEME;
  jds: Styles = jds;
  jdsBiColor = jdsBiColor;

  // Service
  private iconService = inject(JdsBiIconsService);
  private eWalidataFillStore = inject(EWalidataFillStore);

  // Component Store
  readonly vm$ = this.eWalidataFillStore.vm$;

  constructor() {
    this.iconService.registerIcons([iconClockThree]);
  }
}
