import {
  ChangeDetectionStrategy,
  Component,
  Input,
  inject,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// STYLE
import * as jds from '@styles';
// CONSTANT
import {
  COLORSCHEME,
  STATUS_E_WALIDATA_ASSIGN_AVAILABLE,
  STATUS_E_WALIDATA_ASSIGN_ASSIGNMENT,
} from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';
// JDS-BI
import { jdsBiColor, jdsBiDefaultProp } from '@jds-bi/cdk';
import {
  JdsBiTableHeadModule,
  JdsBiBadgeModule,
  JdsBiDropdownModule,
  JdsBiHostedDropdownModule,
} from '@jds-bi/core';

import { filter, isEmpty, last, map, orderBy } from 'lodash';
import moment from 'moment';
import { NgxPopperjsModule } from 'ngx-popperjs';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

import { EWalidataAssignService } from '../../containers/e-walidata-assign/e-walidata-assign.service';
import { EWalidataAssignStore } from '../../containers/e-walidata-assign/e-walidata-assign.store';

@Component({
  selector: 'app-e-walidata-list-assign-indicator',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    JdsBiTableHeadModule,
    JdsBiBadgeModule,
    JdsBiDropdownModule,
    JdsBiHostedDropdownModule,
    NgxPopperjsModule,
    NgxSkeletonLoaderModule,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './e-walidata-list-assign-indicator.component.html',
  styleUrls: ['./e-walidata-list-assign-indicator.component.scss'],
})
export class EWalidataListAssignIndicatorComponent {
  satudataUser: LoginData;
  colorScheme = COLORSCHEME;
  jdsBiColor = jdsBiColor;
  moment = moment;
  jds: Styles = jds;

  @Input()
  @jdsBiDefaultProp()
  id: string;

  @Input()
  @jdsBiDefaultProp()
  name: string;

  // Service
  private authenticationService = inject(AuthenticationService);
  private eWalidataAssignService = inject(EWalidataAssignService);
  private eWalidataAssignStore = inject(EWalidataAssignStore);

  // Variable
  isOpenStatusAvailable = false;
  isOpenStatusAssignment = false;
  statusAvailable = Array.from(
    STATUS_E_WALIDATA_ASSIGN_AVAILABLE,
    ([, value]) => ({
      id: value.slug,
      name: value.name,
      color: value.color,
      checked: false,
    }),
  );
  statusAssignment = Array.from(
    STATUS_E_WALIDATA_ASSIGN_ASSIGNMENT,
    ([, value]) => ({
      id: value.slug,
      name: value.name,
      color: value.color,
      checked: false,
    }),
  );

  // Component Store
  readonly vm$ = this.eWalidataAssignStore.vm$;

  constructor() {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
  }

  getStatusAvailable(value: string) {
    return STATUS_E_WALIDATA_ASSIGN_AVAILABLE.get(value).color;
  }

  getStatusAssignment(value: string) {
    return STATUS_E_WALIDATA_ASSIGN_ASSIGNMENT.get(value).color;
  }

  getHistory(history: any[]) {
    if (isEmpty(history)) {
      return {
        note: '-',
        date: '-',
      };
    }

    const sortedArray = orderBy(
      history,
      [(obj) => new Date(obj.datetime)],
      ['asc'],
    );
    const lastArray = last(sortedArray);

    return {
      status_assignment: lastArray.status_assignment,
      name: lastArray.name,
      nama_skpd: lastArray.nama_skpd,
      note: lastArray.notes || '-',
      date: moment(lastArray.datetime).locale('id').format('DD MMM YYYY HH:mm'),
    };
  }

  getNote({ status, name, organization, note, date }) {
    let from: string;
    if (this.satudataUser.role_name === 'walidata') {
      from = `${status} oleh ${name} dari ${organization}`;
    } else {
      from = `${status} oleh ${organization}`;
    }

    return `
      <div>
        <div>${from}</div>
        <div>Catatan: ${note}</div>
        <div>${date}</div>
      </div>
    `;
  }

  checkSort(id: string, value: string) {
    return this.eWalidataAssignService.checkSort(id, value);
  }

  checkDirection(id: string, value: string) {
    return this.eWalidataAssignService.checkDirection(id, value);
  }

  checkCheckboxDisable(statusAvailable: string, statusAssignment: string) {
    if (this.satudataUser.role_name === 'walidata') {
      if (statusAvailable !== 'Aktif') {
        return true;
      }
      return false;
    }

    if (statusAssignment !== 'Ditugaskan') {
      return true;
    }
    return false;
  }

  onSort(id: string, value: any, direction: string) {
    let dir = '';
    if (direction === 'desc') {
      dir = 'asc';
    } else if (direction === 'asc') {
      dir = 'desc';
    }

    this.eWalidataAssignStore.filterSortTableIndicator({
      id,
      value: `${value}:${dir}`,
    });
  }

  onFilterStatusAvailable(id: string, code: string, isChecked: boolean) {
    const items = this.statusAvailable;

    items.forEach((item) => {
      if (item.name === code) {
        item.checked = isChecked;
      }

      return item;
    });

    const value = map(filter(items, 'checked'), 'name');
    this.eWalidataAssignStore.filterStatusAvailableIndicator({ id, value });
  }

  onFilterStatusAssignment(id: string, code: string, isChecked: boolean) {
    const items = this.statusAssignment;

    items.forEach((item) => {
      if (item.name === code) {
        item.checked = isChecked;
      }

      return item;
    });

    const value = map(filter(items, 'checked'), 'name');
    this.eWalidataAssignStore.filterStatusAssignmentIndicator({ id, value });
  }

  onSelected(obj: any, isChecked: boolean) {
    this.eWalidataAssignStore.addRemoveIndicator({ obj, isChecked });
  }
}
