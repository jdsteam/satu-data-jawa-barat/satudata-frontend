import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
  inject,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// JDS-BI
import { jdsBiColor, jdsBiDefaultProp } from '@jds-bi/cdk';
import { JdsBiInputModule, JdsBiButtonModule } from '@jds-bi/core';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconMagnifyingGlass,
  iconCircleXmark,
} from '@jds-bi/icons';

@Component({
  selector: 'app-e-walidata-search-indicator',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    JdsBiInputModule,
    JdsBiButtonModule,
    JdsBiIconsModule,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './e-walidata-search-indicator.component.html',
})
export class EWalidataSearchIndicatorComponent {
  colorScheme = COLORSCHEME;
  jds: Styles = jds;
  jdsBiColor = jdsBiColor;

  @Input()
  @jdsBiDefaultProp()
  searchData: string;

  @Output() search = new EventEmitter<any>();

  // Service
  private iconService = inject(JdsBiIconsService);

  constructor() {
    this.iconService.registerIcons([iconMagnifyingGlass, iconCircleXmark]);
  }

  onSearch(value: string): void {
    this.search.emit(value);
  }
}
