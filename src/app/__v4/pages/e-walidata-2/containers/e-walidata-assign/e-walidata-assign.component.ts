import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
  inject,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Title } from '@angular/platform-browser';
import { RouterModule, Router, ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';
// INTERFACE
import { Styles, Breadcrumb } from '@interfaces-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService, StateService } from '@services-v3';
import { GlobalService } from '@services-v4';

import { isEmpty, isNil, set, split } from 'lodash';
import moment from 'moment';
import { fromWorker } from 'observable-webworker';

import {
  EWalidataFilterAssignBusinessFieldComponent,
  EWalidataSortAssignBusinessFieldComponent,
  EWalidataListAssignComponent,
  EWalidataOffcanvasAssignComponent,
} from '../../components';

import { DEFAULT_STATE, EWalidataAssignStore } from './e-walidata-assign.store';

@Component({
  selector: 'app-e-walidata-assign',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    EWalidataFilterAssignBusinessFieldComponent,
    EWalidataSortAssignBusinessFieldComponent,
    EWalidataListAssignComponent,
    EWalidataOffcanvasAssignComponent,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './e-walidata-assign.component.html',
  providers: [EWalidataAssignStore],
})
export class EWalidataAssignComponent implements OnInit, OnDestroy {
  satudataUser: LoginData;
  satudataToken: string;
  jds: Styles = jds;
  moment = moment;

  // Log
  log = null;
  pageInfo = null;
  deviceInfo = null;
  userInfo = null;

  // Settings
  title: string;
  label = 'E-Walidata';
  breadcrumb: Breadcrumb[] = [{ label: 'E-Walidata', link: '/e-walidata' }];

  // Service
  private router = inject(Router);
  private route = inject(ActivatedRoute);
  private globalService = inject(GlobalService);
  private authenticationService = inject(AuthenticationService);
  private stateService = inject(StateService);
  private titleService = inject(Title);
  private eWalidataAssignStore = inject(EWalidataAssignStore);

  // Component Store
  readonly vm$ = this.eWalidataAssignStore.vm$;

  constructor() {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
    this.authenticationService.satudataToken.subscribe(
      (x) => (this.satudataToken = x),
    );
    this.stateService.currentLogUser.subscribe((x) => (this.userInfo = x));
  }

  ngOnInit(): void {
    this.eWalidataAssignStore.removeAllBusinessField();
    this.eWalidataAssignStore.removeAllIndicator();

    this.logPage('init');
    this.settingsAll();
    this.queryParams();
    this.getAllBusinessField();
    this.getAllEWalidataAssignBusinessField();
  }

  ngOnDestroy(): void {
    this.logPage('destroy');
  }

  settingsAll(): void {
    this.title = this.globalService.title;
    this.titleService.setTitle(`${this.label} - ${this.title}`);
    this.globalService.changeLabel(this.label);
    this.globalService.changeBreadcrumb(this.breadcrumb);
  }

  queryParams(): void {
    this.route.queryParams.subscribe((p) => {
      const checkArray = (param, value) => {
        return !isNil(param) && !isEmpty(param) ? split(param, ',') : value;
      };

      const {
        filterBusinessField: businessField,
        filterSort: sort,
        filterPerPage: perPage,
        filterCurrentPage: currentPage,
      } = DEFAULT_STATE;

      const pBusinessField = checkArray(p['businessField'], businessField);
      const pSort = !isNil(p['sort']) ? p['sort'] : sort;
      const pPerPage = !isNil(p['perPage']) ? p['perPage'] : perPage;
      const pCurrentPage = !isNil(p['page']) ? p['page'] : currentPage;

      this.eWalidataAssignStore.setFilterBusinessField(pBusinessField);
      this.eWalidataAssignStore.setFilterSort(pSort);
      this.eWalidataAssignStore.setFilterPerPage(pPerPage);
      this.eWalidataAssignStore.setFilterCurrentPage(pCurrentPage);
    });
  }

  // Get =======================================================================
  getAllBusinessField() {
    this.eWalidataAssignStore.getAllBusinessField();
  }

  getAllEWalidataAssignBusinessField() {
    this.eWalidataAssignStore.getAllEWalidataBusinessField();
  }

  // Log =======================================================================
  logPage(type: string): void {
    if (type === 'init') {
      this.pageInfo = {
        page: this.router.url,
        date_start: moment().format('YYYY-MM-DD HH:mm:ss'),
      };

      this.log = {
        log: { ...this.pageInfo, ...this.userInfo },
        token: this.satudataToken,
      };
    } else if (type === 'destroy') {
      if (typeof Worker !== 'undefined') {
        set(this.log.log, 'date_end', moment().format('YYYY-MM-DD HH:mm:ss'));

        const log = of(this.log);

        fromWorker<object, string>(
          () =>
            new Worker(
              new URL('src/app/__v4/workers/log-page.worker', import.meta.url),
              { type: 'module' },
            ),
          log,
        ).subscribe((message) => {
          if (!environment.production) {
            console.info(`Log page: ${message}`);
          }
        });
      }
    }
  }
}
