import { Injectable, inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ComponentStore } from '@ngrx/component-store';
import { Observable } from 'rxjs';
import { tap, withLatestFrom, switchMap, map, mergeMap } from 'rxjs/operators';

// CONSTANT
import { SORT_E_WALIDATA_ASSIGN } from '@constants-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';
import { BusinessFieldService, OrganizationService } from '@services-v4';

import { assign, chain, find, isEmpty, orderBy } from 'lodash';

import { EWalidataAssignService } from './e-walidata-assign.service';

export interface EWalidataAssignState {
  filterBusinessField: string[];
  filterSort: string;
  filterPerPage: number;
  filterCurrentPage: number;
  listBusinessField: any;
  listEWalidataAssignBusinessField: any;
  filterSearchIndicator: any;
  filterSortIndicator: any;
  filterDirectionIndicator: any;
  filterStatusAvailableIndicator: any;
  filterStatusAssignmentIndicator: any;
  listEWalidataAssignIndicator: any;
  selectedIndicator: any;
  selectedIndicatorGroup: any;
  selectedIndicatorNonactive: boolean;
  listOrganizationSatuData: any;
}

export const DEFAULT_STATE: EWalidataAssignState = {
  filterBusinessField: [],
  filterSort: 'highest',
  filterPerPage: 10,
  filterCurrentPage: 1,
  listBusinessField: null,
  listEWalidataAssignBusinessField: null,
  filterSearchIndicator: null,
  filterSortIndicator: null,
  filterDirectionIndicator: null,
  filterStatusAvailableIndicator: [],
  filterStatusAssignmentIndicator: [],
  listEWalidataAssignIndicator: null,
  selectedIndicator: [],
  selectedIndicatorGroup: [],
  selectedIndicatorNonactive: false,
  listOrganizationSatuData: null,
};

@Injectable()
export class EWalidataAssignStore extends ComponentStore<EWalidataAssignState> {
  satudataUser: LoginData;

  // Service
  private router = inject(Router);
  private route = inject(ActivatedRoute);
  private authenticationService = inject(AuthenticationService);
  private businessFieldService = inject(BusinessFieldService);
  private organizationService = inject(OrganizationService);
  private eWalidataAssignService = inject(EWalidataAssignService);

  constructor() {
    super(DEFAULT_STATE);

    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
  }

  readonly setFilterBusinessField = this.updater((state, value: string[]) => ({
    ...state,
    filterBusinessField: value,
  }));

  readonly setFilterSort = this.updater((state, value: string) => ({
    ...state,
    filterSort: value,
  }));

  readonly setFilterPerPage = this.updater((state, value: number) => ({
    ...state,
    filterPerPage: Number(value),
  }));

  readonly setFilterCurrentPage = this.updater((state, value: number) => ({
    ...state,
    filterCurrentPage: Number(value),
  }));

  readonly setListBusinessField = this.updater((state, value: any) => ({
    ...state,
    listBusinessField: value,
  }));

  readonly setListEWalidataAssignBusinessField = this.updater(
    (state, value: any) => ({
      ...state,
      listEWalidataAssignBusinessField: value,
    }),
  );

  readonly setFilterSearchIndicator = this.updater((state, value: any) => ({
    ...state,
    filterSearchIndicator: {
      ...state.filterSearchIndicator,
      [value.id]: value.value,
    },
  }));

  readonly setFilterSortIndicator = this.updater((state, value: any) => ({
    ...state,
    filterSortIndicator: {
      ...state.filterSortIndicator,
      [value.id]: value.value,
    },
  }));

  readonly setFilterDirectionIndicator = this.updater((state, value: any) => ({
    ...state,
    filterDirectionIndicator: {
      ...state.filterDirectionIndicator,
      [value.id]: value.value,
    },
  }));

  readonly setFilterStatusAvailableIndicator = this.updater(
    (state, value: any) => ({
      ...state,
      filterStatusAvailableIndicator: {
        ...state.filterStatusAvailableIndicator,
        [value.id]: value.value,
      },
    }),
  );

  readonly setFilterStatusAssignmentIndicator = this.updater(
    (state, value: any) => ({
      ...state,
      filterStatusAssignmentIndicator: {
        ...state.filterStatusAssignmentIndicator,
        [value.id]: value.value,
      },
    }),
  );

  readonly setListEWalidataAssignIndicator = this.updater(
    (state, value: any) => ({
      ...state,
      listEWalidataAssignIndicator: {
        ...state.listEWalidataAssignIndicator,
        [value.id]: value.value,
      },
    }),
  );

  readonly setSelectedIndicator = this.updater((state, value: any) => ({
    ...state,
    selectedIndicator: value,
  }));

  readonly setSelectedIndicatorGroup = this.updater((state, value: any) => ({
    ...state,
    selectedIndicatorGroup: value,
  }));

  readonly setSelectedIndicatorNonactive = this.updater(
    (state, value: any) => ({
      ...state,
      selectedIndicatorNonactive: value,
    }),
  );

  readonly setListOrganizationSatuData = this.updater((state, value: any) => ({
    ...state,
    listOrganizationSatuData: value,
  }));

  // *********** Selectors *********** //
  readonly getFilterBusinessField$ = this.select(
    ({ filterBusinessField }) => filterBusinessField,
  );
  readonly getFilterSort$ = this.select(({ filterSort }) => filterSort);
  readonly getFilterPerPage$ = this.select(
    ({ filterPerPage }) => filterPerPage,
  );
  readonly getFilterCurrentPage$ = this.select(
    ({ filterCurrentPage }) => filterCurrentPage,
  );
  readonly getListBusinessField$ = this.select(
    ({ listBusinessField }) => listBusinessField,
  );
  readonly getListEWalidataAssignBusinessField$ = this.select(
    ({ listEWalidataAssignBusinessField }) => listEWalidataAssignBusinessField,
  );
  readonly getFilterSearchIndicator$ = this.select(
    ({ filterSearchIndicator }) => filterSearchIndicator,
  );
  readonly getFilterSortIndicator$ = this.select(
    ({ filterSortIndicator }) => filterSortIndicator,
  );
  readonly getFilterDirectionIndicator$ = this.select(
    ({ filterDirectionIndicator }) => filterDirectionIndicator,
  );
  readonly getFilterStatusAvailableIndicator$ = this.select(
    ({ filterStatusAvailableIndicator }) => filterStatusAvailableIndicator,
  );
  readonly getFilterStatusAssignmentIndicator$ = this.select(
    ({ filterStatusAssignmentIndicator }) => filterStatusAssignmentIndicator,
  );
  readonly getListEWalidataAssignIndicator$ = this.select(
    ({ listEWalidataAssignIndicator }) => listEWalidataAssignIndicator,
  );
  readonly getSelectedIndicator$ = this.select(
    ({ selectedIndicator }) => selectedIndicator,
  );
  readonly getSelectedIndicatorGroup$ = this.select(({ selectedIndicator }) => {
    const sort = orderBy(selectedIndicator, ['businessFieldId']);

    const group = chain(sort)
      .groupBy('businessFieldId')
      .map((value, key) => ({
        businessFieldId: key,
        businessFieldName: value[0].businessFieldName,
        data: orderBy(value, ['indicatorId']),
      }))
      .value();

    return group;
  });
  readonly getSelectedIndicatorNonactive$ = this.select(
    ({ selectedIndicator }) => {
      const finds = find(selectedIndicator, (item) => {
        return item.indicatorStatus !== 'Pengajuan Nonaktif';
      });

      if (finds) {
        return true;
      }

      return false;
    },
  );
  readonly getListOrganizationSatuData$ = this.select(
    ({ listOrganizationSatuData }) => listOrganizationSatuData,
  );

  readonly vm$ = this.select(
    this.state$,
    this.getFilterBusinessField$,
    this.getFilterSort$,
    this.getFilterPerPage$,
    this.getFilterCurrentPage$,
    this.getListBusinessField$,
    this.getListEWalidataAssignBusinessField$,
    this.getFilterSearchIndicator$,
    this.getFilterSortIndicator$,
    this.getFilterDirectionIndicator$,
    this.getFilterStatusAvailableIndicator$,
    this.getFilterStatusAssignmentIndicator$,
    this.getListEWalidataAssignIndicator$,
    this.getSelectedIndicator$,
    this.getSelectedIndicatorGroup$,
    this.getSelectedIndicatorNonactive$,
    this.getListOrganizationSatuData$,
    (
      state,
      getFilterBusinessField,
      getFilterSort,
      getFilterPerPage,
      getFilterCurrentPage,
      getListBusinessField,
      getListEWalidataAssignBusinessField,
      getFilterSearchIndicator,
      getFilterSortIndicator,
      getFilterDirectionIndicator,
      getFilterStatusAvailableIndicator,
      getFilterStatusAssignmentIndicator,
      getListEWalidataAssignIndicator,
      getSelectedIndicator,
      getSelectedIndicatorGroup,
      getSelectedIndicatorNonactive,
      getListOrganizationSatuData,
    ) => ({
      filterBusinessField: state.filterBusinessField,
      filterSort: state.filterSort,
      filterPerPage: state.filterPerPage,
      filterCurrentPage: state.filterCurrentPage,
      listBusinessField: state.listBusinessField,
      listEWalidataAssignBusinessField: state.listEWalidataAssignBusinessField,
      filterSearchIndicator: state.filterSearchIndicator,
      filterSortIndicator: state.filterSortIndicator,
      filterDirectionIndicator: state.filterDirectionIndicator,
      filterStatusAvailableIndicator: state.filterStatusAvailableIndicator,
      filterStatusAssignmentIndicator: state.filterStatusAssignmentIndicator,
      listEWalidataAssignIndicator: state.listEWalidataAssignIndicator,
      selectedIndicator: state.selectedIndicator,
      selectedIndicatorGroup: state.selectedIndicatorGroup,
      selectedIndicatorNonactive: state.selectedIndicatorNonactive,
      listOrganizationSatuData: state.listOrganizationSatuData,
      getFilterBusinessField,
      getFilterSort,
      getFilterPerPage,
      getFilterCurrentPage,
      getListBusinessField,
      getListEWalidataAssignBusinessField,
      getFilterSearchIndicator,
      getFilterSortIndicator,
      getFilterDirectionIndicator,
      getFilterStatusAvailableIndicator,
      getFilterStatusAssignmentIndicator,
      getListEWalidataAssignIndicator,
      getSelectedIndicator,
      getSelectedIndicatorGroup,
      getSelectedIndicatorNonactive,
      getListOrganizationSatuData,
    }),
  );

  // *********** Actions *********** //
  // Business Field
  readonly filterSort = this.effect((sort$: Observable<string>) => {
    return sort$.pipe(
      withLatestFrom(this.getFilterSort$),
      tap<[string, string]>(([sort]) => {
        this.router.navigate([], {
          relativeTo: this.route,
          queryParams: {
            sort,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        this.setFilterSort(sort);
        this.setFilterCurrentPage(DEFAULT_STATE.filterCurrentPage);
      }),
      map(() => {
        this.getAllEWalidataBusinessField();
      }),
    );
  });

  readonly filterBusinessField = this.effect(
    (businessField$: Observable<string[]>) => {
      return businessField$.pipe(
        withLatestFrom(this.getFilterBusinessField$),
        tap<[string[], string[]]>(([businessField]) => {
          const value = !isEmpty(businessField)
            ? businessField.join(',')
            : null;

          this.router.navigate([], {
            relativeTo: this.route,
            queryParams: {
              businessField: value,
              page: null,
            },
            queryParamsHandling: 'merge',
          });

          this.setFilterBusinessField(businessField);
          this.setFilterCurrentPage(DEFAULT_STATE.filterCurrentPage);
        }),
        map(() => {
          this.getAllEWalidataBusinessField();
        }),
      );
    },
  );

  readonly filterPerPage = this.effect((perPage$: Observable<number>) => {
    return perPage$.pipe(
      withLatestFrom(this.getFilterPerPage$),
      tap<[number, number]>(([perPage]) => {
        this.router.navigate([], {
          relativeTo: this.route,
          queryParams: {
            perPage,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        this.setFilterPerPage(perPage);
        this.setFilterCurrentPage(DEFAULT_STATE.filterCurrentPage);
      }),
      map(() => {
        this.getAllEWalidataBusinessField();
      }),
    );
  });

  readonly filterCurrentPage = this.effect(
    (currentPage$: Observable<number>) => {
      return currentPage$.pipe(
        withLatestFrom(this.getFilterCurrentPage$),
        tap<[number, number]>(([currentPage]) => {
          this.router.navigate([], {
            relativeTo: this.route,
            queryParams: {
              page: currentPage,
            },
            queryParamsHandling: 'merge',
          });

          this.setFilterCurrentPage(currentPage);
        }),
        map(() => {
          this.getAllEWalidataBusinessField();
        }),
      );
    },
  );

  readonly getAllBusinessField = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      switchMap(() => {
        const params = {
          sort: `kode_bidang_urusan:asc`,
        };

        const options = {
          pagination: false,
        };

        return this.businessFieldService
          .getQueryList(params, options)
          .result$.pipe(
            tap({
              next: (result) => {
                this.setListBusinessField(result);
              },
            }),
          );
      }),
    );
  });

  readonly getAllOrganizationSatuData = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      switchMap(() => {
        const params = {
          sort: `nama_skpd:asc`,
          where: JSON.stringify({
            is_deleted: false,
          }),
        };

        const options = {
          pagination: false,
        };
        const app = 'satudata';

        return this.organizationService
          .getQueryList(params, options, app)
          .result$.pipe(
            tap({
              next: (result) => {
                this.setListOrganizationSatuData(result);
              },
            }),
          );
      }),
    );
  });

  readonly getAllEWalidataBusinessField = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      withLatestFrom(this.vm$),
      switchMap(([, vm]) => {
        const {
          filterBusinessField: businessField,
          filterSort: sort,
          filterPerPage: perPage,
          filterCurrentPage: currentPage,
          selectedIndicatorGroup,
        } = vm;

        const pWhere = {};

        if (
          this.satudataUser.role_name === 'opd' ||
          this.satudataUser.role_name === 'opd-view'
        ) {
          assign(pWhere, { status_assignment: 'Ditugaskan' });
        }

        if (!isEmpty(businessField)) {
          assign(pWhere, { kode_bidang_urusan: businessField });
        }

        const params = {
          sort: SORT_E_WALIDATA_ASSIGN.get(sort).paramSort,
          skip: perPage * currentPage - perPage,
          limit: perPage,
          where: JSON.stringify(pWhere),
        };

        if (
          this.satudataUser.role_name === 'opd' ||
          this.satudataUser.role_name === 'opd-view'
        ) {
          assign(params, { kode_skpd: this.satudataUser.kode_skpd });
        }

        return this.eWalidataAssignService
          .getQueryListBusinessField(params)
          .result$.pipe(
            tap({
              next: (result) => {
                if (result.data) {
                  if (isEmpty(selectedIndicatorGroup)) {
                    result.data.list.map((item) => (item.checked = false));
                  } else {
                    result.data.list.map((item) => {
                      return (item.checked =
                        this.eWalidataAssignService.checkSelectedGroup(
                          item.kode_bidang_urusan,
                          selectedIndicatorGroup,
                        ));
                    });
                  }
                }

                this.setListEWalidataAssignBusinessField(result);
              },
            }),
          );
      }),
    );
  });

  // Business Field Indicator
  readonly filterSearchIndicator = this.effect((obj$: Observable<any>) => {
    return obj$.pipe(
      withLatestFrom(this.getFilterSearchIndicator$),
      tap<[any, any]>(([search, previous]) => {
        const value = isEmpty(search.value) === false ? search.value : null;

        if (isEmpty(previous)) {
          this.setFilterSearchIndicator({
            id: search.id,
            value,
          });
          return;
        }

        if (previous.value !== value) {
          this.setFilterSearchIndicator({
            id: search.id,
            value,
          });
        }
      }),
      map(([search]) => {
        this.getAllEWalidataIndicator(search.id);
      }),
    );
  });

  readonly filterSortTableIndicator = this.effect((obj$: Observable<any>) => {
    return obj$.pipe(
      withLatestFrom(this.getFilterSortIndicator$),
      tap<[any, any]>(([sort]) => {
        const obj = sort.value.split(':');

        this.setFilterSortIndicator({
          id: sort.id,
          value: obj[0],
        });
        this.setFilterDirectionIndicator({
          id: sort.id,
          value: obj[1],
        });
      }),
      map(([sort]) => {
        this.getAllEWalidataIndicator(sort.id);
      }),
    );
  });

  readonly filterStatusAvailableIndicator = this.effect(
    (obj$: Observable<any>) => {
      return obj$.pipe(
        withLatestFrom(this.getFilterStatusAvailableIndicator$),
        tap<[any, any]>(([status]) => {
          this.setFilterStatusAvailableIndicator({
            id: status.id,
            value: status.value,
          });
        }),
        map(([sort]) => {
          this.getAllEWalidataIndicator(sort.id);
        }),
      );
    },
  );

  readonly filterStatusAssignmentIndicator = this.effect(
    (obj$: Observable<any>) => {
      return obj$.pipe(
        withLatestFrom(this.getFilterStatusAssignmentIndicator$),
        tap<[any, any]>(([status]) => {
          this.setFilterStatusAssignmentIndicator({
            id: status.id,
            value: status.value,
          });
        }),
        map(([sort]) => {
          this.getAllEWalidataIndicator(sort.id);
        }),
      );
    },
  );

  readonly getAllEWalidataIndicator = this.effect((id$: Observable<string>) => {
    return id$.pipe(
      withLatestFrom(this.vm$),
      mergeMap(([id, vm]) => {
        const {
          filterSearchIndicator: search,
          filterSortIndicator: sort,
          filterDirectionIndicator: direction,
          filterStatusAvailableIndicator: statusAvailable,
          filterStatusAssignmentIndicator: statusAssignment,
          selectedIndicator,
        } = vm;

        const pSearch = this.eWalidataAssignService.checkSearch(id, search);
        const pSort = this.eWalidataAssignService.checkSort(id, sort);
        const pDirection = this.eWalidataAssignService.checkDirection(
          id,
          direction,
        );
        const pStatusAvailable =
          this.eWalidataAssignService.checkStatusAvailable(id, statusAvailable);
        const pStatusAssignment =
          this.eWalidataAssignService.checkStatusAssignment(
            id,
            statusAssignment,
          );

        const pWhere = {
          kode_bidang_urusan: id,
        };

        if (!isEmpty(pStatusAvailable)) {
          assign(pWhere, { status_available: pStatusAvailable });
        }

        if (!isEmpty(pStatusAssignment)) {
          assign(pWhere, { status_assignment: pStatusAssignment });
        }

        if (
          this.satudataUser.role_name === 'opd' ||
          this.satudataUser.role_name === 'opd-view'
        ) {
          assign(pWhere, { status_assignment: 'Ditugaskan' });
        }

        const params = {
          sort: `${pSort}:${pDirection}`,
          where: JSON.stringify(pWhere),
          limit: 1000,
        };

        if (pSearch) {
          assign(params, { search: pSearch });
        }

        if (
          this.satudataUser.role_name === 'opd' ||
          this.satudataUser.role_name === 'opd-view'
        ) {
          assign(params, { kode_skpd: this.satudataUser.kode_skpd });
        }

        const options = {
          pagination: false,
        };

        return this.eWalidataAssignService
          .getQueryListIndicator(id, params, options)
          .result$.pipe(
            tap({
              next: (result) => {
                if (result.data) {
                  if (isEmpty(selectedIndicator)) {
                    result.data.list.map((item) => (item.checked = false));
                  } else {
                    result.data.list.map((item) => {
                      return (item.checked =
                        this.eWalidataAssignService.checkSelected(
                          item.kode_indikator,
                          selectedIndicator,
                        ));
                    });
                  }
                }

                this.setListEWalidataAssignIndicator({
                  id,
                  value: result,
                });
              },
            }),
          );
      }),
    );
  });

  // Selected Indicator
  readonly addRemoveIndicator = this.effect((obj$: Observable<any>) => {
    return obj$.pipe(
      withLatestFrom(this.getSelectedIndicator$),
      tap<[any, any]>(([selected, previous]) => {
        let result = previous;
        if (selected.isChecked) {
          const { indicatorId } = selected.obj;

          const findSel = result.find((item: any) => {
            return item.indicatorId === indicatorId;
          });

          if (!findSel) {
            result.push(selected.obj);
          }
        } else {
          const { indicatorId } = selected.obj;
          const removedSel = result.filter((item: any) => {
            return item.indicatorId !== indicatorId;
          });
          result = removedSel;
        }

        this.setSelectedIndicator(result);
      }),
    );
  });

  readonly addRemoveIndicatorGroup = this.effect((obj$: Observable<any>) => {
    return obj$.pipe(
      withLatestFrom(this.getSelectedIndicatorGroup$),
      mergeMap(([selected, previous]) => {
        let group = previous;
        if (selected.isChecked) {
          const { businessFieldId } = selected.obj;

          const findSel = group.find((item: any) => {
            return item.businessFieldId === businessFieldId;
          });

          if (!findSel) {
            group.push(selected.obj);
          }
        } else {
          const { businessFieldId } = selected.obj;
          const removed = group.filter((item: any) => {
            return item.businessFieldId !== businessFieldId;
          });
          group = removed;
        }

        this.setSelectedIndicatorGroup(group);

        const { businessFieldId, businessFieldName } = selected.obj;

        const pWhere = {
          kode_bidang_urusan: businessFieldId,
        };

        if (
          this.satudataUser.role_name === 'opd' ||
          this.satudataUser.role_name === 'opd-view'
        ) {
          assign(pWhere, { status_assignment: 'Ditugaskan' });
        }

        const params = {
          where: JSON.stringify(pWhere),
          limit: 1000,
        };

        if (
          this.satudataUser.role_name === 'opd' ||
          this.satudataUser.role_name === 'opd-view'
        ) {
          assign(params, { kode_skpd: this.satudataUser.kode_skpd });
        }

        const options = {
          pagination: false,
        };

        return this.eWalidataAssignService
          .getQueryListIndicator(businessFieldId, params, options)
          .result$.pipe(
            tap({
              next: (result) => {
                if (result.data) {
                  result.data.list.map((item) => {
                    if (
                      (this.satudataUser.role_name === 'walidata' &&
                        item.status_available === 'Aktif') ||
                      this.satudataUser.role_name === 'opd'
                    ) {
                      this.addRemoveIndicator({
                        obj: {
                          businessFieldId,
                          businessFieldName,
                          indicatorId: item.kode_indikator,
                          indicatorName: item.nama_indikator,
                          indicatorStatus: item.status_assignment,
                          assignmentId: item.id_assignment,
                          assignmentOrganization: item.kode_skpd,
                        },
                        isChecked: selected.isChecked,
                      });

                      if (selected.isChecked) {
                        return (item.checked = true);
                      }

                      return (item.checked = false);
                    }

                    return (item.checked = false);
                  });
                }

                this.setListEWalidataAssignIndicator({
                  businessFieldId,
                  value: result,
                });
              },
            }),
          );
      }),
    );
  });

  readonly removeAllBusinessField = this.effect((trigger$) => {
    return trigger$.pipe(
      withLatestFrom(this.getListEWalidataAssignBusinessField$),
      tap(([, list]) => {
        if (list) {
          list.data.list.map((item: any) => {
            item.checked = false;
            return item;
          });
        }

        this.setSelectedIndicatorGroup([]);
      }),
    );
  });

  readonly removeAllIndicator = this.effect((trigger$) => {
    return trigger$.pipe(
      withLatestFrom(this.getListEWalidataAssignIndicator$),
      tap(([, list]) => {
        if (list) {
          Object.keys(list).forEach((k: any) => {
            list[k].data.list = list[k].data.list.map((item: any) => {
              item.checked = false;
              return item;
            });
          });
        }

        this.setSelectedIndicator([]);
      }),
    );
  });
}
