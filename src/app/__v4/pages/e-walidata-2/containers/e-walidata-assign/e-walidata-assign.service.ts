import { inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable, of, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';

// MODEL
import { LoginData } from '@models-v3';
import { BusinessField, BusinessFieldIndicator } from '@models-v4';
// SERVICE
import { AuthenticationService, PaginationService } from '@services-v3';
// UTIL
import { hasOwnProperty } from '@utils-v4';

import { QueryClientService, UseQuery } from '@ngneat/query';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root',
})
export class EWalidataAssignService {
  satudataUser: LoginData;
  title = 'E-Walidata Assign';

  // API URL
  apiUrlBusinessField = `${environment.backendURL}bidang_urusan`;
  apiUrlIndicator = `${environment.backendURL}bidang_urusan_indikator`;
  apiUrlIndicatorOrganization = `${environment.backendURL}bidang_urusan_indikator_skpd`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  private router = inject(Router);
  private http = inject(HttpClient);
  private queryClient = inject(QueryClientService);
  private useQuery = inject(UseQuery);
  private authenticationService = inject(AuthenticationService);
  private paginationService = inject(PaginationService);
  private toastr = inject(ToastrService);

  constructor() {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
  }

  // Can Activate
  canActivate(): Observable<boolean> {
    let access: boolean;
    switch (this.satudataUser.role_name) {
      case 'walidata':
      case 'opd':
        access = true;
        break;
      case 'opd-view':
        access = false;
        break;
      default:
        break;
    }

    if (!access) {
      this.router.navigate(['/404']);
      return of(false);
    }

    return of(true);
  }

  // List Business Field
  getListBusinessField(params: any, options: any) {
    const merge = new URLSearchParams(params);

    const queryName = `List ${this.title} Business Field`;

    return this.http
      .get<any>(`${this.apiUrlBusinessField}?${merge}`, this.httpOptions)
      .pipe(
        map((response) => {
          if (response.error === 1 && response.message !== 'Data not found') {
            throw new Error(response.message);
          }

          return new BusinessField().deserialize(response);
        }),
        map((response) => {
          const hasPagination = hasOwnProperty(options, 'pagination');
          const hasInfinite = hasOwnProperty(options, 'infinite');

          const isPagination = hasPagination ? options.pagination : true;
          const isInfinite = hasInfinite ? options.infinite : false;

          let pagination = { empty: true, infinite: isInfinite };
          if (isPagination) {
            const resultPagination = this.paginationService.getPagination(
              response.meta.total_record,
              params.skip,
              params.limit,
            );

            pagination = {
              empty: false,
              infinite: isInfinite,
              ...resultPagination,
            };
          }

          return {
            pagination,
            list: response.data,
          };
        }),
        catchError((e) => {
          this.toastr.error(e.message, queryName);
          return of(null);
        }),
      );
  }

  getQueryListBusinessField(params: any, options: any = {}) {
    return this.useQuery(
      ['eWalidataBusinessField'],
      () => {
        return this.getListBusinessField(params, options);
      },
      options,
    );
  }

  // List Indicator
  getListIndicator(params: any, options: any) {
    const merge = new URLSearchParams(params);

    const queryName = `List ${this.title} Indicator`;

    return this.http
      .get<any>(`${this.apiUrlIndicator}?${merge}`, this.httpOptions)
      .pipe(
        map((response) => {
          if (response.error === 1 && response.message !== 'Data not found') {
            throw new Error(response.message);
          }

          return new BusinessFieldIndicator().deserialize(response);
        }),
        map((response) => {
          const hasPagination = hasOwnProperty(options, 'pagination');
          const hasInfinite = hasOwnProperty(options, 'infinite');

          const isPagination = hasPagination ? options.pagination : true;
          const isInfinite = hasInfinite ? options.infinite : false;

          let pagination = { empty: true, infinite: isInfinite };
          if (isPagination) {
            const resultPagination = this.paginationService.getPagination(
              response.meta.total_record,
              params.skip,
              params.limit,
            );

            pagination = {
              empty: false,
              infinite: isInfinite,
              ...resultPagination,
            };
          }

          return {
            pagination,
            list: response.data,
          };
        }),
        catchError((e) => {
          this.toastr.error(e.message, queryName);
          return of(null);
        }),
      );
  }

  getQueryListIndicator(uniq: any, params: any, options: any = {}) {
    return this.useQuery(
      ['eWalidataIndicator', uniq],
      () => {
        return this.getListIndicator(params, options);
      },
      options,
    );
  }

  // Create
  createIndicator(item: any) {
    return this.http
      .post<any>(`${this.apiUrlIndicatorOrganization}/multiple`, item)
      .pipe(
        tap(() => {
          this.queryClient.invalidateQueries(['eWalidataBusinessField']);
          this.queryClient.invalidateQueries(['eWalidataIndicator']);
        }),
        catchError((e) => {
          this.toastr.error(e.message, `Create ${this.title} Indicator`);
          return throwError(() => e);
        }),
      );
  }

  // Update
  updateIndicator(item: any) {
    return this.http
      .put<any>(
        `${this.apiUrlIndicatorOrganization}/multiple`,
        item,
        this.httpOptions,
      )
      .pipe(
        tap(() => {
          this.queryClient.invalidateQueries(['eWalidataBusinessField']);
          this.queryClient.invalidateQueries(['eWalidataIndicator']);
        }),
        catchError((e) => {
          this.toastr.error(e.message, `Update ${this.title} Indicator`);
          return throwError(() => e);
        }),
      );
  }

  // Check Search
  checkSearch(id, value) {
    if (value && hasOwnProperty(value, id)) {
      return value[id];
    }

    return null;
  }

  // Check Sort
  checkSort(id, value) {
    if (value && hasOwnProperty(value, id)) {
      return value[id];
    }

    return 'kode_indikator';
  }

  // Check Direction
  checkDirection(id, value) {
    if (value && hasOwnProperty(value, id)) {
      return value[id];
    }

    return 'asc';
  }

  // Check Status Available
  checkStatusAvailable(id, value) {
    if (value && hasOwnProperty(value, id)) {
      return value[id];
    }

    return [];
  }

  // Check Status Assignment
  checkStatusAssignment(id, value) {
    if (value && hasOwnProperty(value, id)) {
      return value[id];
    }

    return [];
  }

  // Check Selected
  checkSelectedGroup(id, value) {
    const check = value.find((item) => {
      return item.businessFieldId === id;
    });

    if (check) {
      return true;
    }

    return false;
  }

  checkSelected(id, value) {
    const check = value.find((item) => {
      return item.indicatorId === id;
    });

    if (check) {
      return true;
    }

    return false;
  }
}
