import { inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable, of, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';

// MODEL
import { LoginData } from '@models-v3';
import {
  BusinessFieldMonitor,
  BusinessFieldIndicatorMonitor,
} from '@models-v4';
// SERVICE
import { AuthenticationService, PaginationService } from '@services-v3';
// UTIL
import { hasOwnProperty } from '@utils-v4';

import { QueryClientService, UseQuery } from '@ngneat/query';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root',
})
export class EWalidataFillService {
  satudataUser: LoginData;
  title = 'E-Walidata Fill';

  // API URL
  apiUrlBusinessField = `${environment.backendURL}bidang_urusan/monitor`;
  apiUrlIndicator = `${environment.backendURL}bidang_urusan_indikator/monitor`;
  apiUrlIndicatorOrganization = `${environment.backendURL}bidang_urusan_indikator_skpd`;
  apiUrlEWalidata = `${environment.backendURL}ewalidata_data`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  private router = inject(Router);
  private http = inject(HttpClient);
  private queryClient = inject(QueryClientService);
  private useQuery = inject(UseQuery);
  private authenticationService = inject(AuthenticationService);
  private paginationService = inject(PaginationService);
  private toastr = inject(ToastrService);

  constructor() {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
  }

  // Can Activate
  canActivate(): Observable<boolean> {
    let access: boolean;
    switch (this.satudataUser.role_name) {
      case 'opd':
        access = true;
        break;
      case 'walidata':
      case 'opd-view':
        access = false;
        break;
      default:
        break;
    }

    if (!access) {
      this.router.navigate(['/404']);
      return of(false);
    }

    return of(true);
  }

  // List Business Field
  getListBusinessFieldMonitor(params: any, options: any) {
    const merge = new URLSearchParams(params);

    const queryName = `List ${this.title} Business Field Monitor`;

    return this.http
      .get<any>(`${this.apiUrlBusinessField}?${merge}`, this.httpOptions)
      .pipe(
        map((response) => {
          if (response.error === 1 && response.message !== 'Data not found') {
            throw new Error(response.message);
          }

          return new BusinessFieldMonitor().deserialize(response);
        }),
        map((response) => {
          const hasPagination = hasOwnProperty(options, 'pagination');
          const hasInfinite = hasOwnProperty(options, 'infinite');

          const isPagination = hasPagination ? options.pagination : true;
          const isInfinite = hasInfinite ? options.infinite : false;

          let pagination = { empty: true, infinite: isInfinite };
          if (isPagination) {
            const resultPagination = this.paginationService.getPagination(
              response.meta.total_record,
              params.skip,
              params.limit,
            );

            pagination = {
              empty: false,
              infinite: isInfinite,
              ...resultPagination,
            };
          }

          return {
            pagination,
            list: response.data,
          };
        }),
        catchError((e) => {
          this.toastr.error(e.message, queryName);
          return of(null);
        }),
      );
  }

  getQueryListBusinessFieldMonitor(uniq: any, params: any, options: any = {}) {
    return this.useQuery(
      ['eWalidataBusinessFieldMonitor', uniq],
      () => {
        return this.getListBusinessFieldMonitor(params, options);
      },
      options,
    );
  }

  // List Indicator
  getListIndicatorMonitor(params: any, options: any) {
    const merge = new URLSearchParams(params);

    const queryName = `List ${this.title} Indicator Monitor`;

    return this.http
      .get<any>(`${this.apiUrlIndicator}?${merge}`, this.httpOptions)
      .pipe(
        map((response) => {
          if (response.error === 1 && response.message !== 'Data not found') {
            throw new Error(response.message);
          }

          return new BusinessFieldIndicatorMonitor().deserialize(response);
        }),
        map((response) => {
          const hasPagination = hasOwnProperty(options, 'pagination');
          const hasInfinite = hasOwnProperty(options, 'infinite');

          const isPagination = hasPagination ? options.pagination : true;
          const isInfinite = hasInfinite ? options.infinite : false;

          let pagination = { empty: true, infinite: isInfinite };
          if (isPagination) {
            const resultPagination = this.paginationService.getPagination(
              response.meta.total_record,
              params.skip,
              params.limit,
            );

            pagination = {
              empty: false,
              infinite: isInfinite,
              ...resultPagination,
            };
          }

          return {
            pagination,
            list: response.data,
          };
        }),
        catchError((e) => {
          this.toastr.error(e.message, queryName);
          return of(null);
        }),
      );
  }

  getQueryListIndicatorMonitor(uniq: any, params: any, options: any = {}) {
    return this.useQuery(
      ['eWalidataIndicatorMonitor', uniq],
      () => {
        return this.getListIndicatorMonitor(params, options);
      },
      options,
    );
  }

  // Update Indicator
  updateIndicator(item: any) {
    return this.http
      .put<any>(
        `${this.apiUrlIndicatorOrganization}/multiple`,
        item,
        this.httpOptions,
      )
      .pipe(
        tap(() => {
          this.queryClient.invalidateQueries(['eWalidataBusinessFieldMonitor']);
          this.queryClient.invalidateQueries(['eWalidataIndicatorMonitor']);
        }),
        catchError((e) => {
          this.toastr.error(e.message, `Update ${this.title} Indicator`);
          return throwError(() => e);
        }),
      );
  }

  // Update Indicator Data
  updateIndicatorData(item: any) {
    return this.http
      .post<any>(`${this.apiUrlEWalidata}/submit`, item, this.httpOptions)
      .pipe(
        tap(() => {
          this.queryClient.invalidateQueries(['eWalidataIndicatorMonitor']);
        }),
        catchError((e) => {
          this.toastr.error(e.message, `Update ${this.title} Indicator Data`);
          return throwError(() => e);
        }),
      );
  }
}
