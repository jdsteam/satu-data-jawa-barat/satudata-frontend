import { Injectable, inject, Signal } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { ActivatedRoute, Router } from '@angular/router';
import { ComponentStore } from '@ngrx/component-store';
import { tap, map, switchMap } from 'rxjs/operators';

// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';
import { BusinessFieldService } from '@services-v4';

import { assign, groupBy, isEmpty, orderBy } from 'lodash';

import { EWalidataFillService } from './e-walidata-fill.service';

export interface EWalidataFillState {
  filterYear: string;
  filterSearch: string | null;
  filterSort: string;
  filterDirection: string;
  filterPerPage: number;
  filterCurrentPage: number;
  filterBusinessField: string;
  listBusinessField: any;
  listEWalidataFillBusinessFieldMonitor: any;
  listEWalidataFillIndicatorMonitor: any;
}

export const DEFAULT_STATE: EWalidataFillState = {
  filterYear: new Date().getFullYear().toString(),
  filterSearch: null,
  filterSort: 'kode_bidang_urusan',
  filterDirection: 'asc',
  filterPerPage: 15,
  filterCurrentPage: 1,
  filterBusinessField: null,
  listBusinessField: null,
  listEWalidataFillBusinessFieldMonitor: null,
  listEWalidataFillIndicatorMonitor: null,
};

@Injectable()
export class EWalidataFillStore extends ComponentStore<EWalidataFillState> {
  satudataUser: LoginData;

  // Service
  private router = inject(Router);
  private route = inject(ActivatedRoute);
  private authenticationService = inject(AuthenticationService);
  private businessFieldService = inject(BusinessFieldService);
  private eWalidataFillService = inject(EWalidataFillService);

  constructor() {
    super(DEFAULT_STATE);

    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
  }

  // *********** Updaters *********** //
  readonly setFilterYear = this.updater((state, value: string) => ({
    ...state,
    filterYear: value,
  }));

  readonly setFilterSearch = this.updater((state, value: string | null) => ({
    ...state,
    filterSearch: value,
  }));

  readonly setFilterSort = this.updater((state, value: string) => ({
    ...state,
    filterSort: value,
  }));

  readonly setFilterDirection = this.updater((state, value: string) => ({
    ...state,
    filterDirection: value,
  }));

  readonly setFilterPerPage = this.updater((state, value: number) => ({
    ...state,
    filterPerPage: Number(value),
  }));

  readonly setFilterCurrentPage = this.updater((state, value: number) => ({
    ...state,
    filterCurrentPage: Number(value),
  }));

  readonly setFilterBusinessField = this.updater((state, value: string) => ({
    ...state,
    filterBusinessField: value,
  }));

  readonly setListBusinessField = this.updater((state, value: any) => ({
    ...state,
    listBusinessField: value,
  }));

  readonly setEWalidataFillBusinessFieldMonitor = this.updater(
    (state, value: any) => ({
      ...state,
      listEWalidataFillBusinessFieldMonitor: value,
    }),
  );

  readonly setEWalidataFillIndicatorMonitor = this.updater(
    (state, value: any) => ({
      ...state,
      listEWalidataFillIndicatorMonitor: value,
    }),
  );

  // *********** Selectors *********** //
  readonly getFilterYear$: Signal<string> = this.selectSignal(
    ({ filterYear }) => filterYear,
  );
  readonly getFilterSearch$: Signal<string | null> = this.selectSignal(
    ({ filterSearch }) => filterSearch,
  );
  readonly getFilterSort$: Signal<string> = this.selectSignal(
    ({ filterSort }) => filterSort,
  );
  readonly getFilterDirection$: Signal<string> = this.selectSignal(
    ({ filterDirection }) => filterDirection,
  );
  readonly getFilterPerPage$: Signal<number> = this.selectSignal(
    ({ filterPerPage }) => filterPerPage,
  );
  readonly getFilterCurrentPage$: Signal<number> = this.selectSignal(
    ({ filterCurrentPage }) => filterCurrentPage,
  );
  readonly getFilterBusinessField$: Signal<string> = this.selectSignal(
    ({ filterBusinessField }) => filterBusinessField,
  );
  readonly getListBusinessField$: Signal<any> = this.selectSignal(
    ({ listBusinessField }) => listBusinessField,
  );
  readonly getListEWalidataFillBusinessFieldMonitor$: Signal<any> =
    this.selectSignal(
      ({ listEWalidataFillBusinessFieldMonitor }) =>
        listEWalidataFillBusinessFieldMonitor,
    );
  readonly getListEWalidataFillIndicatorMonitor$: Signal<any> =
    this.selectSignal(
      ({ listEWalidataFillIndicatorMonitor }) =>
        listEWalidataFillIndicatorMonitor,
    );
  readonly getListEWalidataFillIndicatorMonitorYear$: Signal<any> =
    this.selectSignal(({ listEWalidataFillIndicatorMonitor }) => {
      const data = listEWalidataFillIndicatorMonitor;
      const currentYear = new Date().getFullYear().toString();

      const empty = [currentYear];

      if (isEmpty(data) || isEmpty(data.data)) {
        return empty;
      }

      const index = data.data.list.reduce((maxIndex, item, i) => {
        return item.ewalidata_data.length >
          data.data.list[maxIndex].ewalidata_data.length
          ? i
          : maxIndex;
      }, 0);

      const currentData = data.data.list[index];
      if (!currentData) {
        return empty;
      }

      const findCurrentYear = currentData.ewalidata_data.find(
        (item) => item.tahun === currentYear,
      );

      const order = orderBy(currentData.ewalidata_data, 'tahun', 'desc');
      let result = order.map(({ tahun }) => tahun);

      if (!findCurrentYear) {
        result = [currentYear, ...result];
      }

      return result;
    });
  readonly getListEWalidataFillIndicatorMonitorGroup$: Signal<any> =
    this.selectSignal(({ listEWalidataFillIndicatorMonitor }) => {
      const data = listEWalidataFillIndicatorMonitor;

      if (isEmpty(data) || isEmpty(data.data)) {
        return [];
      }

      const result = groupBy(data.data.list, 'kode_bidang_urusan');

      return result;
    });

  readonly vm$ = this.selectSignal(
    toSignal(this.state$),
    this.getFilterYear$,
    this.getFilterSearch$,
    this.getFilterSort$,
    this.getFilterDirection$,
    this.getFilterPerPage$,
    this.getFilterCurrentPage$,
    this.getFilterBusinessField$,
    this.getListBusinessField$,
    this.getListEWalidataFillBusinessFieldMonitor$,
    this.getListEWalidataFillIndicatorMonitor$,
    this.getListEWalidataFillIndicatorMonitorYear$,
    this.getListEWalidataFillIndicatorMonitorGroup$,
    (
      state,
      getFilterYear,
      getFilterSearch,
      getFilterSort,
      getFilterDirection,
      getFilterPerPage,
      getFilterCurrentPage,
      getFilterBusinessField,
      getListBusinessField,
      getListEWalidataFillBusinessFieldMonitor,
      getListEWalidataFillIndicatorMonitor,
      getListEWalidataFillIndicatorMonitorYear,
      getListEWalidataFillIndicatorMonitorGroup,
    ) => ({
      filterYear: state.filterYear,
      filterSearch: state.filterSearch,
      filterSort: state.filterSort,
      filterDirection: state.filterDirection,
      filterPerPage: state.filterPerPage,
      filterCurrentPage: state.filterCurrentPage,
      filterBusinessField: state.filterBusinessField,
      listBusinessField: state.listBusinessField,
      listEWalidataFillBusinessFieldMonitor:
        state.listEWalidataFillBusinessFieldMonitor,
      listEWalidataFillIndicatorMonitor:
        state.listEWalidataFillIndicatorMonitor,
      getFilterYear,
      getFilterSearch,
      getFilterSort,
      getFilterDirection,
      getFilterPerPage,
      getFilterCurrentPage,
      getFilterBusinessField,
      getListBusinessField,
      getListEWalidataFillBusinessFieldMonitor,
      getListEWalidataFillIndicatorMonitor,
      getListEWalidataFillIndicatorMonitorYear,
      getListEWalidataFillIndicatorMonitorGroup,
    }),
  );

  // *********** Actions *********** //
  readonly filterYear = this.effect<string>((year$) => {
    return year$.pipe(
      tap((year) => {
        this.router.navigate([], {
          relativeTo: this.route,
          queryParams: {
            year,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        this.setFilterYear(year);
        this.setFilterCurrentPage(DEFAULT_STATE.filterCurrentPage);
      }),
      map(() => {
        this.getAllEWalidataBusinessFieldMonitor();
      }),
    );
  });

  readonly filterSearch = this.effect<string | null>((search$) => {
    return search$.pipe(
      tap((search) => {
        const { filterSearch: previous } = this.vm$();
        const value = isEmpty(search) === false ? search : null;

        if (previous !== value) {
          this.router.navigate([], {
            relativeTo: this.route,
            queryParams: {
              q: value,
              page: null,
            },
            queryParamsHandling: 'merge',
          });

          this.setFilterSearch(value);
          this.setFilterCurrentPage(DEFAULT_STATE.filterCurrentPage);
        }
      }),
      map(() => {
        this.getAllEWalidataIndicatorMonitor();
      }),
    );
  });

  readonly filterSortTable = this.effect<string>((sort$) => {
    return sort$.pipe(
      tap((sort) => {
        const obj = sort.split(':');

        this.router.navigate([], {
          relativeTo: this.route,
          queryParams: {
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        this.setFilterSort(obj[0]);
        this.setFilterDirection(obj[1]);
        this.setFilterCurrentPage(DEFAULT_STATE.filterCurrentPage);
      }),
      map(() => {
        this.getAllEWalidataIndicatorMonitor();
      }),
    );
  });

  readonly filterPerPage = this.effect<number>((perPage$) => {
    return perPage$.pipe(
      tap((perPage) => {
        this.router.navigate([], {
          relativeTo: this.route,
          queryParams: {
            perPage,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        this.setFilterPerPage(perPage);
        this.setFilterCurrentPage(DEFAULT_STATE.filterCurrentPage);
      }),
      map(() => {
        this.getAllEWalidataIndicatorMonitor();
      }),
    );
  });

  readonly filterCurrentPage = this.effect<number>((currentPage$) => {
    return currentPage$.pipe(
      tap((currentPage) => {
        this.router.navigate([], {
          relativeTo: this.route,
          queryParams: {
            page: currentPage,
          },
          queryParamsHandling: 'merge',
        });

        this.setFilterCurrentPage(currentPage);
      }),
      map(() => {
        this.getAllEWalidataIndicatorMonitor();
      }),
    );
  });

  readonly filterBusinessField = this.effect<string>((businessField$) => {
    return businessField$.pipe(
      tap((businessField) => {
        this.router.navigate([], {
          relativeTo: this.route,
          queryParams: {
            businessField,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        this.setFilterBusinessField(businessField);
        this.setFilterCurrentPage(DEFAULT_STATE.filterCurrentPage);
      }),
      map(() => {
        this.getAllEWalidataIndicatorMonitor();
      }),
    );
  });

  readonly getAllBusinessField = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      switchMap(() => {
        const params = {
          sort: `kode_bidang_urusan:asc`,
        };

        const options = {
          pagination: false,
        };

        return this.businessFieldService
          .getQueryList(params, options)
          .result$.pipe(
            tap({
              next: (result) => {
                this.setListBusinessField(result);
              },
            }),
          );
      }),
    );
  });

  readonly getAllEWalidataBusinessFieldMonitor = this.effect<void>(
    (trigger$) => {
      return trigger$.pipe(
        switchMap(() => {
          const codeOrganization = this.satudataUser.kode_skpd;

          const { filterYear: year } = this.vm$();

          const pWhere = {
            status_assignment: 'Diterima',
          };

          const params = {
            where: JSON.stringify(pWhere),
            tahun: year.toString(),
            kode_skpd: codeOrganization,
          };

          const options = {
            pagination: false,
          };

          return this.eWalidataFillService
            .getQueryListBusinessFieldMonitor(codeOrganization, params, options)
            .result$.pipe(
              tap({
                next: (result) => {
                  this.setEWalidataFillBusinessFieldMonitor(result);
                },
              }),
            );
        }),
      );
    },
  );

  readonly getAllEWalidataIndicatorMonitor = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      switchMap(() => {
        const codeOrganization = this.satudataUser.kode_skpd;

        const {
          filterSearch: search,
          filterSort: sort,
          filterDirection: direction,
          filterPerPage: perPage,
          filterCurrentPage: currentPage,
          filterBusinessField: businessField,
        } = this.vm$();

        const pWhere = {
          status_assignment: 'Diterima',
        };

        if (!isEmpty(businessField)) {
          assign(pWhere, { kode_bidang_urusan: businessField });
        }

        const params = {
          search: search || '',
          sort: `${sort}:${direction}`,
          skip: perPage * currentPage - perPage,
          limit: perPage,
          where: JSON.stringify(pWhere),
          kode_skpd: codeOrganization,
        };

        return this.eWalidataFillService
          .getQueryListIndicatorMonitor(codeOrganization, params)
          .result$.pipe(
            tap({
              next: (result) => {
                this.setEWalidataFillIndicatorMonitor(result);
              },
            }),
          );
      }),
    );
  });
}
