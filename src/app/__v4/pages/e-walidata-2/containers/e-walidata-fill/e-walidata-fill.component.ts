import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
  inject,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Title } from '@angular/platform-browser';
import { RouterModule, Router, ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';
// INTERFACE
import { Styles, Breadcrumb } from '@interfaces-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService, StateService } from '@services-v3';
import { GlobalService } from '@services-v4';
// JDS-BI
import { JdsBiButtonModule } from '@jds-bi/core';

import { isNil, set } from 'lodash';
import moment from 'moment';
import { fromWorker } from 'observable-webworker';

import {
  EWalidataFilterYearComponent,
  EWalidataListFillBusinessFieldComponent,
  EWalidataModalFillDeclineComponent,
  EWalidataFilterBusinessFieldComponent,
  EWalidataSearchIndicatorComponent,
  EWalidataListIndicatorComponent,
} from '../../components';

import { DEFAULT_STATE, EWalidataFillStore } from './e-walidata-fill.store';

@Component({
  selector: 'app-e-walidata-fill',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    JdsBiButtonModule,
    EWalidataFilterYearComponent,
    EWalidataListFillBusinessFieldComponent,
    EWalidataModalFillDeclineComponent,
    EWalidataFilterBusinessFieldComponent,
    EWalidataSearchIndicatorComponent,
    EWalidataListIndicatorComponent,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './e-walidata-fill.component.html',
  providers: [EWalidataFillStore],
})
export class EWalidataFillComponent implements OnInit, OnDestroy {
  satudataUser: LoginData;
  satudataToken: string;
  jds: Styles = jds;
  moment = moment;

  @ViewChild(EWalidataModalFillDeclineComponent)
  modalDecline: EWalidataModalFillDeclineComponent;

  // Log
  log = null;
  pageInfo = null;
  deviceInfo = null;
  userInfo = null;

  // Settings
  title: string;
  label = 'E-Walidata';
  breadcrumb: Breadcrumb[] = [{ label: 'E-Walidata', link: '/e-walidata' }];

  // Service
  private router = inject(Router);
  private route = inject(ActivatedRoute);
  private globalService = inject(GlobalService);
  private authenticationService = inject(AuthenticationService);
  private stateService = inject(StateService);
  private titleService = inject(Title);
  private eWalidataFillStore = inject(EWalidataFillStore);

  // Component Store
  readonly vm$ = this.eWalidataFillStore.vm$;

  constructor() {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
    this.authenticationService.satudataToken.subscribe(
      (x) => (this.satudataToken = x),
    );
    this.stateService.currentLogUser.subscribe((x) => (this.userInfo = x));
  }

  ngOnInit(): void {
    this.logPage('init');
    this.settingsAll();
    this.queryParams();
    this.getAllBusinessField();
    this.getAllEWalidataFillBusinessFieldMonitor();
    this.getAllEWalidataFillIndicatorMonitor();
  }

  ngOnDestroy(): void {
    this.logPage('destroy');
  }

  settingsAll(): void {
    this.title = this.globalService.title;
    this.titleService.setTitle(`${this.label} - ${this.title}`);
    this.globalService.changeLabel(this.label);
    this.globalService.changeBreadcrumb(this.breadcrumb);
  }

  queryParams(): void {
    this.route.queryParams.subscribe((p) => {
      const {
        filterSearch: search,
        filterSort: sort,
        filterPerPage: perPage,
        filterCurrentPage: currentPage,
        filterBusinessField: businessField,
      } = DEFAULT_STATE;

      const pSearch = !isNil(p['q']) ? p['q'] : search;
      const pSort = !isNil(p['sort']) ? p['sort'] : sort;
      const pPerPage = !isNil(p['perPage']) ? p['perPage'] : perPage;
      const pCurrentPage = !isNil(p['page']) ? p['page'] : currentPage;
      const pBusinessField = !isNil(p['businessField'])
        ? p['businessField']
        : businessField;

      this.eWalidataFillStore.setFilterSearch(pSearch);
      this.eWalidataFillStore.setFilterSort(pSort);
      this.eWalidataFillStore.setFilterPerPage(pPerPage);
      this.eWalidataFillStore.setFilterCurrentPage(pCurrentPage);
      this.eWalidataFillStore.setFilterBusinessField(pBusinessField);
    });
  }

  // Get =======================================================================
  getAllBusinessField() {
    this.eWalidataFillStore.getAllBusinessField();
  }

  getAllEWalidataFillBusinessFieldMonitor() {
    this.eWalidataFillStore.getAllEWalidataBusinessFieldMonitor();
  }

  getAllEWalidataFillIndicatorMonitor() {
    this.eWalidataFillStore.getAllEWalidataIndicatorMonitor();
  }

  // Filter ====================================================================
  onSearch(value: string) {
    this.eWalidataFillStore.filterSearch(value);
  }

  onPerPage(value: number) {
    this.eWalidataFillStore.filterPerPage(value);
  }

  onPage(value: number) {
    this.eWalidataFillStore.filterCurrentPage(value);
  }

  onFilterYear(value: string) {
    this.eWalidataFillStore.filterYear(value);
  }

  onFilterBusinessField(value: string) {
    this.eWalidataFillStore.filterBusinessField(value);
  }

  // Modal
  onDecline() {
    this.modalDecline.openModal();
  }

  // Log =======================================================================
  logPage(type: string): void {
    if (type === 'init') {
      this.pageInfo = {
        page: this.router.url,
        date_start: moment().format('YYYY-MM-DD HH:mm:ss'),
      };

      this.log = {
        log: { ...this.pageInfo, ...this.userInfo },
        token: this.satudataToken,
      };
    } else if (type === 'destroy') {
      if (typeof Worker !== 'undefined') {
        set(this.log.log, 'date_end', moment().format('YYYY-MM-DD HH:mm:ss'));

        const log = of(this.log);

        fromWorker<object, string>(
          () =>
            new Worker(
              new URL('src/app/__v4/workers/log-page.worker', import.meta.url),
              { type: 'module' },
            ),
          log,
        ).subscribe((message) => {
          if (!environment.production) {
            console.info(`Log page: ${message}`);
          }
        });
      }
    }
  }
}
