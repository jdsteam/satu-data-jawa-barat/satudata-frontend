import { Injectable, inject, Signal } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { ActivatedRoute, Router } from '@angular/router';
import { ComponentStore } from '@ngrx/component-store';
import { tap, map, switchMap } from 'rxjs/operators';

// SERVICE
import { OrganizationService } from '@services-v4';

import { assign, isEmpty } from 'lodash';

import { EWalidataMonitorBusinessFieldService } from './e-walidata-monitor-business-field.service';

export interface EWalidataMonitorBusinessFieldState {
  filterYear: string;
  filterSort: string;
  filterDirection: string;
  filterPerPage: number;
  filterCurrentPage: number;
  filterOrganization: string[];
  listOrganizationSatuData: any;
  listEWalidataMonitorBusinessFieldMonitor: any;
}

export const DEFAULT_STATE: EWalidataMonitorBusinessFieldState = {
  filterYear: new Date().getFullYear().toString(),
  filterSort: 'kode_bidang_urusan',
  filterDirection: 'asc',
  filterPerPage: 10,
  filterCurrentPage: 1,
  filterOrganization: [],
  listOrganizationSatuData: null,
  listEWalidataMonitorBusinessFieldMonitor: null,
};

@Injectable()
export class EWalidataMonitorBusinessFieldStore extends ComponentStore<EWalidataMonitorBusinessFieldState> {
  // Service
  private router = inject(Router);
  private route = inject(ActivatedRoute);
  private organizationService = inject(OrganizationService);
  private eWalidataMonitorService = inject(
    EWalidataMonitorBusinessFieldService,
  );

  constructor() {
    super(DEFAULT_STATE);
  }

  readonly setFilterYear = this.updater((state, value: string) => ({
    ...state,
    filterYear: value,
  }));

  readonly setFilterSort = this.updater((state, value: string) => ({
    ...state,
    filterSort: value,
  }));

  readonly setFilterDirection = this.updater((state, value: string) => ({
    ...state,
    filterDirection: value,
  }));

  readonly setFilterPerPage = this.updater((state, value: number) => ({
    ...state,
    filterPerPage: Number(value),
  }));

  readonly setFilterCurrentPage = this.updater((state, value: number) => ({
    ...state,
    filterCurrentPage: Number(value),
  }));

  readonly setFilterOrganization = this.updater((state, value: string[]) => ({
    ...state,
    filterOrganization: value,
  }));

  readonly setListOrganizationSatuData = this.updater((state, value: any) => ({
    ...state,
    listOrganizationSatuData: value,
  }));

  readonly setEWalidataMonitorBusinessFieldMonitor = this.updater(
    (state, value: any) => ({
      ...state,
      listEWalidataMonitorBusinessFieldMonitor: value,
    }),
  );

  // *********** Selectors *********** //
  readonly getFilterYear$: Signal<string> = this.selectSignal(
    ({ filterYear }) => filterYear,
  );
  readonly getFilterSort$: Signal<string> = this.selectSignal(
    ({ filterSort }) => filterSort,
  );
  readonly getFilterDirection$: Signal<string> = this.selectSignal(
    ({ filterDirection }) => filterDirection,
  );
  readonly getFilterPerPage$: Signal<number> = this.selectSignal(
    ({ filterPerPage }) => filterPerPage,
  );
  readonly getFilterCurrentPage$: Signal<number> = this.selectSignal(
    ({ filterCurrentPage }) => filterCurrentPage,
  );
  readonly getFilterOrganization$: Signal<string[]> = this.selectSignal(
    ({ filterOrganization }) => filterOrganization,
  );
  readonly getListOrganizationSatuData$: Signal<any> = this.selectSignal(
    ({ listOrganizationSatuData }) => listOrganizationSatuData,
  );
  readonly getListEWalidataMonitorBusinessFieldMonitor$: Signal<any> =
    this.selectSignal(
      ({ listEWalidataMonitorBusinessFieldMonitor }) =>
        listEWalidataMonitorBusinessFieldMonitor,
    );

  readonly vm$ = this.selectSignal(
    toSignal(this.state$),
    this.getFilterYear$,
    this.getFilterSort$,
    this.getFilterDirection$,
    this.getFilterPerPage$,
    this.getFilterCurrentPage$,
    this.getFilterOrganization$,
    this.getListOrganizationSatuData$,
    this.getListEWalidataMonitorBusinessFieldMonitor$,
    (
      state,
      getFilterYear,
      getFilterSort,
      getFilterDirection,
      getFilterPerPage,
      getFilterCurrentPage,
      getFilterOrganization,
      getListOrganizationSatuData,
      getListEWalidataMonitorBusinessFieldMonitor,
    ) => ({
      filterYear: state.filterYear,
      filterSort: state.filterSort,
      filterDirection: state.filterDirection,
      filterPerPage: state.filterPerPage,
      filterCurrentPage: state.filterCurrentPage,
      filterOrganization: state.filterOrganization,
      listOrganizationSatuData: state.listOrganizationSatuData,
      listEWalidataMonitorBusinessFieldMonitor:
        state.listEWalidataMonitorBusinessFieldMonitor,
      getFilterYear,
      getFilterSort,
      getFilterDirection,
      getFilterPerPage,
      getFilterCurrentPage,
      getFilterOrganization,
      getListOrganizationSatuData,
      getListEWalidataMonitorBusinessFieldMonitor,
    }),
  );

  // *********** Actions *********** //
  readonly filterYear = this.effect<string>((year$) => {
    return year$.pipe(
      tap((year) => {
        this.router.navigate([], {
          relativeTo: this.route,
          queryParams: {
            year,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        this.setFilterYear(year);
        this.setFilterSort(DEFAULT_STATE.filterSort);
        this.setFilterDirection(DEFAULT_STATE.filterDirection);
        this.setFilterCurrentPage(DEFAULT_STATE.filterCurrentPage);
      }),
      map(() => {
        this.getAllEWalidataBusinessFieldMonitor();
      }),
    );
  });

  readonly filterSortTable = this.effect<string>((sort$) => {
    return sort$.pipe(
      tap((sort) => {
        const obj = sort.split(':');

        this.router.navigate([], {
          relativeTo: this.route,
          queryParams: {
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        this.setFilterSort(obj[0]);
        this.setFilterDirection(obj[1]);
        this.setFilterCurrentPage(DEFAULT_STATE.filterCurrentPage);
      }),
      map(() => {
        this.getAllEWalidataBusinessFieldMonitor();
      }),
    );
  });

  readonly filterPerPage = this.effect<number>((perPage$) => {
    return perPage$.pipe(
      tap((perPage) => {
        this.router.navigate([], {
          relativeTo: this.route,
          queryParams: {
            perPage,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        this.setFilterPerPage(perPage);
        this.setFilterCurrentPage(DEFAULT_STATE.filterCurrentPage);
      }),
      map(() => {
        this.getAllEWalidataBusinessFieldMonitor();
      }),
    );
  });

  readonly filterCurrentPage = this.effect<number>((currentPage$) => {
    return currentPage$.pipe(
      tap((currentPage) => {
        this.router.navigate([], {
          relativeTo: this.route,
          queryParams: {
            page: currentPage,
          },
          queryParamsHandling: 'merge',
        });

        this.setFilterCurrentPage(currentPage);
      }),
      map(() => {
        this.getAllEWalidataBusinessFieldMonitor();
      }),
    );
  });

  readonly filterOrganization = this.effect<string[]>((organization$) => {
    return organization$.pipe(
      tap((organization) => {
        this.router.navigate([], {
          relativeTo: this.route,
          queryParams: {
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        this.setFilterOrganization(organization);
        this.setFilterCurrentPage(DEFAULT_STATE.filterCurrentPage);
      }),
      map(() => {
        this.getAllEWalidataBusinessFieldMonitor();
      }),
    );
  });

  readonly getAllOrganizationSatuData = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      switchMap(() => {
        const { filterOrganization: organization } = this.vm$();

        const params = {
          sort: `nama_skpd:asc`,
          where: JSON.stringify({
            is_deleted: false,
          }),
        };

        const options = {
          pagination: false,
        };
        const app = 'satudata';

        return this.organizationService
          .getQueryList(params, options, app)
          .result$.pipe(
            tap({
              next: (result) => {
                if (result.data) {
                  if (isEmpty(organization)) {
                    result.data.list.map((item) => (item.checked = false));
                  } else {
                    result.data.list.forEach((item) => {
                      if (organization.includes(item.kode_skpd)) {
                        item.checked = true;
                      } else {
                        item.checked = false;
                      }
                    });
                  }
                }

                this.setListOrganizationSatuData(result);
              },
            }),
          );
      }),
    );
  });

  readonly getAllEWalidataBusinessFieldMonitor = this.effect<void>(
    (trigger$) => {
      return trigger$.pipe(
        switchMap(() => {
          const {
            filterYear: year,
            filterSort: sort,
            filterDirection: direction,
            filterPerPage: perPage,
            filterCurrentPage: currentPage,
            filterOrganization: organization,
          } = this.vm$();

          const pWhere = {};

          const params = {
            sort: `${sort}:${direction}`,
            skip: perPage * currentPage - perPage,
            limit: perPage,
            where: JSON.stringify(pWhere),
            tahun: year.toString(),
          };

          if (!isEmpty(organization)) {
            assign(params, { kode_skpd: organization });
          }

          return this.eWalidataMonitorService
            .getQueryListBusinessFieldMonitor(params)
            .result$.pipe(
              tap({
                next: (result) => {
                  this.setEWalidataMonitorBusinessFieldMonitor(result);
                },
              }),
            );
        }),
      );
    },
  );
}
