import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
  inject,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Title } from '@angular/platform-browser';
import { RouterModule, Router, ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';
// INTERFACE
import { Styles, Breadcrumb } from '@interfaces-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService, StateService } from '@services-v3';
import { GlobalService } from '@services-v4';

import { isNil, set } from 'lodash';
import moment from 'moment';
import { fromWorker } from 'observable-webworker';

import {
  EWalidataFilterYearComponent,
  EWalidataListMonitorBusinessFieldComponent,
} from '../../components';

import {
  DEFAULT_STATE,
  EWalidataMonitorBusinessFieldStore,
} from './e-walidata-monitor-business-field.store';

@Component({
  selector: 'app-e-walidata-monitor-business-field',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    EWalidataFilterYearComponent,
    EWalidataListMonitorBusinessFieldComponent,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './e-walidata-monitor-business-field.component.html',
  providers: [EWalidataMonitorBusinessFieldStore],
})
export class EWalidataMonitorBusinessFieldComponent
  implements OnInit, OnDestroy
{
  satudataUser: LoginData;
  satudataToken: string;
  jds: Styles = jds;
  moment = moment;

  // Log
  log = null;
  pageInfo = null;
  deviceInfo = null;
  userInfo = null;

  // Settings
  title: string;
  label = 'E-Walidata';
  breadcrumb: Breadcrumb[] = [{ label: 'E-Walidata', link: '/e-walidata' }];

  // Service
  private router = inject(Router);
  private route = inject(ActivatedRoute);
  private globalService = inject(GlobalService);
  private authenticationService = inject(AuthenticationService);
  private stateService = inject(StateService);
  private titleService = inject(Title);
  private eWalidataMonitorStore = inject(EWalidataMonitorBusinessFieldStore);

  // Component Store
  readonly vm$ = this.eWalidataMonitorStore.vm$;

  constructor() {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
    this.authenticationService.satudataToken.subscribe(
      (x) => (this.satudataToken = x),
    );
    this.stateService.currentLogUser.subscribe((x) => (this.userInfo = x));
  }

  ngOnInit(): void {
    this.logPage('init');
    this.settingsAll();
    this.queryParams();
    this.getAllEWalidataMonitorBusinessFieldMonitor();
  }

  ngOnDestroy(): void {
    this.logPage('destroy');
  }

  settingsAll(): void {
    this.title = this.globalService.title;
    this.titleService.setTitle(`${this.label} - ${this.title}`);
    this.globalService.changeLabel(this.label);
    this.globalService.changeBreadcrumb(this.breadcrumb);
  }

  queryParams(): void {
    this.route.queryParams.subscribe((p) => {
      const {
        filterYear: year,
        filterSort: sort,
        filterPerPage: perPage,
        filterCurrentPage: currentPage,
      } = DEFAULT_STATE;

      const pYear = !isNil(p['year']) ? p['year'] : year;
      const pSort = !isNil(p['sort']) ? p['sort'] : sort;
      const pPerPage = !isNil(p['perPage']) ? p['perPage'] : perPage;
      const pCurrentPage = !isNil(p['page']) ? p['page'] : currentPage;

      this.eWalidataMonitorStore.setFilterYear(pYear);
      this.eWalidataMonitorStore.setFilterSort(pSort);
      this.eWalidataMonitorStore.setFilterPerPage(pPerPage);
      this.eWalidataMonitorStore.setFilterCurrentPage(pCurrentPage);
    });
  }

  // Get =======================================================================
  getAllEWalidataMonitorBusinessFieldMonitor() {
    this.eWalidataMonitorStore.getAllEWalidataBusinessFieldMonitor();
  }

  // Filter ====================================================================
  onFilterYear(value: string) {
    this.eWalidataMonitorStore.filterYear(value);
  }

  // Log =======================================================================
  logPage(type: string): void {
    if (type === 'init') {
      this.pageInfo = {
        page: this.router.url,
        date_start: moment().format('YYYY-MM-DD HH:mm:ss'),
      };

      this.log = {
        log: { ...this.pageInfo, ...this.userInfo },
        token: this.satudataToken,
      };
    } else if (type === 'destroy') {
      if (typeof Worker !== 'undefined') {
        set(this.log.log, 'date_end', moment().format('YYYY-MM-DD HH:mm:ss'));

        const log = of(this.log);

        fromWorker<object, string>(
          () =>
            new Worker(
              new URL('src/app/__v4/workers/log-page.worker', import.meta.url),
              { type: 'module' },
            ),
          log,
        ).subscribe((message) => {
          if (!environment.production) {
            console.info(`Log page: ${message}`);
          }
        });
      }
    }
  }
}
