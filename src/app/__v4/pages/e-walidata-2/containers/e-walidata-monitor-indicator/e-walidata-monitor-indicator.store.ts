import { Injectable, inject, Signal } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { ActivatedRoute, Router } from '@angular/router';
import { ComponentStore } from '@ngrx/component-store';
import { tap, map, switchMap } from 'rxjs/operators';

// SERVICE
import { BusinessFieldService, OrganizationService } from '@services-v4';

import { assign, isEmpty, orderBy } from 'lodash';

import { EWalidataMonitorIndicatorService } from './e-walidata-monitor-indicator.service';

export interface EWalidataMonitorIndicatorState {
  filterSearch: string | null;
  filterSort: string;
  filterDirection: string;
  filterPerPage: number;
  filterCurrentPage: number;
  filterBusinessField: string;
  filterOrganization: string[];
  listBusinessField: any;
  listOrganizationSatuData: any;
  listEWalidataMonitorIndicatorMonitor: any;
}

export const DEFAULT_STATE: EWalidataMonitorIndicatorState = {
  filterSearch: null,
  filterSort: 'kode_indikator',
  filterDirection: 'asc',
  filterPerPage: 15,
  filterCurrentPage: 1,
  filterBusinessField: null,
  filterOrganization: [],
  listBusinessField: null,
  listOrganizationSatuData: null,
  listEWalidataMonitorIndicatorMonitor: null,
};

@Injectable()
export class EWalidataMonitorIndicatorStore extends ComponentStore<EWalidataMonitorIndicatorState> {
  // Service
  private router = inject(Router);
  private route = inject(ActivatedRoute);
  private businessFieldService = inject(BusinessFieldService);
  private organizationService = inject(OrganizationService);
  private eWalidataMonitorService = inject(EWalidataMonitorIndicatorService);

  constructor() {
    super(DEFAULT_STATE);
  }

  // *********** Updaters *********** //
  readonly setFilterSearch = this.updater((state, value: string | null) => ({
    ...state,
    filterSearch: value,
  }));

  readonly setFilterSort = this.updater((state, value: string) => ({
    ...state,
    filterSort: value,
  }));

  readonly setFilterDirection = this.updater((state, value: string) => ({
    ...state,
    filterDirection: value,
  }));

  readonly setFilterPerPage = this.updater((state, value: number) => ({
    ...state,
    filterPerPage: Number(value),
  }));

  readonly setFilterCurrentPage = this.updater((state, value: number) => ({
    ...state,
    filterCurrentPage: Number(value),
  }));

  readonly setFilterBusinessField = this.updater((state, value: string) => ({
    ...state,
    filterBusinessField: value,
  }));

  readonly setFilterOrganization = this.updater((state, value: string[]) => ({
    ...state,
    filterOrganization: value,
  }));

  readonly setListBusinessField = this.updater((state, value: any) => ({
    ...state,
    listBusinessField: value,
  }));

  readonly setListOrganizationSatuData = this.updater((state, value: any) => ({
    ...state,
    listOrganizationSatuData: value,
  }));

  readonly setEWalidataMonitorIndicatorMonitor = this.updater(
    (state, value: any) => ({
      ...state,
      listEWalidataMonitorIndicatorMonitor: value,
    }),
  );

  // *********** Selectors *********** //
  readonly getFilterSearch$: Signal<string | null> = this.selectSignal(
    ({ filterSearch }) => filterSearch,
  );
  readonly getFilterSort$: Signal<string> = this.selectSignal(
    ({ filterSort }) => filterSort,
  );
  readonly getFilterDirection$: Signal<string> = this.selectSignal(
    ({ filterDirection }) => filterDirection,
  );
  readonly getFilterPerPage$: Signal<number> = this.selectSignal(
    ({ filterPerPage }) => filterPerPage,
  );
  readonly getFilterCurrentPage$: Signal<number> = this.selectSignal(
    ({ filterCurrentPage }) => filterCurrentPage,
  );
  readonly getFilterBusinessField$: Signal<string> = this.selectSignal(
    ({ filterBusinessField }) => filterBusinessField,
  );
  readonly getFilterOrganization$: Signal<string[]> = this.selectSignal(
    ({ filterOrganization }) => filterOrganization,
  );
  readonly getListBusinessField$: Signal<any> = this.selectSignal(
    ({ listBusinessField }) => listBusinessField,
  );
  readonly getListOrganizationSatuData$: Signal<any> = this.selectSignal(
    ({ listOrganizationSatuData }) => listOrganizationSatuData,
  );
  readonly getListEWalidataMonitorIndicatorMonitor$: Signal<any> =
    this.selectSignal(
      ({ listEWalidataMonitorIndicatorMonitor }) =>
        listEWalidataMonitorIndicatorMonitor,
    );
  readonly getListEWalidataMonitorIndicatorMonitorYear$: Signal<any> =
    this.selectSignal(({ listEWalidataMonitorIndicatorMonitor }) => {
      const data = listEWalidataMonitorIndicatorMonitor;
      const currentYear = new Date().getFullYear().toString();

      const empty = [currentYear];

      if (isEmpty(data) || isEmpty(data.data)) {
        return empty;
      }

      const index = data.data.list.reduce((maxIndex, item, i) => {
        return item.ewalidata_data.length >
          data.data.list[maxIndex].ewalidata_data.length
          ? i
          : maxIndex;
      }, 0);

      const currentData = data.data.list[index];
      if (!currentData) {
        return empty;
      }

      const findCurrentYear = currentData.ewalidata_data.find(
        (item) => item.tahun === currentYear,
      );

      const order = orderBy(currentData.ewalidata_data, 'tahun', 'desc');
      let result = order.map(({ tahun }) => tahun);

      if (!findCurrentYear) {
        result = [currentYear, ...result];
      }

      return result;
    });

  readonly vm$ = this.selectSignal(
    toSignal(this.state$),
    this.getFilterSearch$,
    this.getFilterSort$,
    this.getFilterDirection$,
    this.getFilterPerPage$,
    this.getFilterCurrentPage$,
    this.getFilterBusinessField$,
    this.getFilterOrganization$,
    this.getListBusinessField$,
    this.getListOrganizationSatuData$,
    this.getListEWalidataMonitorIndicatorMonitor$,
    this.getListEWalidataMonitorIndicatorMonitorYear$,
    (
      state,
      getFilterSearch,
      getFilterSort,
      getFilterDirection,
      getFilterPerPage,
      getFilterCurrentPage,
      getFilterBusinessField,
      getFilterOrganization,
      getListBusinessField,
      getListOrganizationSatuData,
      getListEWalidataMonitorIndicatorMonitor,
      getListEWalidataMonitorIndicatorMonitorYear,
    ) => ({
      filterSearch: state.filterSearch,
      filterSort: state.filterSort,
      filterDirection: state.filterDirection,
      filterPerPage: state.filterPerPage,
      filterCurrentPage: state.filterCurrentPage,
      filterBusinessField: state.filterBusinessField,
      filterOrganization: state.filterOrganization,
      listBusinessField: state.listBusinessField,
      listOrganizationSatuData: state.listOrganizationSatuData,
      listEWalidataMonitorIndicatorMonitor:
        state.listEWalidataMonitorIndicatorMonitor,
      getFilterSearch,
      getFilterSort,
      getFilterDirection,
      getFilterPerPage,
      getFilterCurrentPage,
      getFilterBusinessField,
      getFilterOrganization,
      getListBusinessField,
      getListOrganizationSatuData,
      getListEWalidataMonitorIndicatorMonitor,
      getListEWalidataMonitorIndicatorMonitorYear,
    }),
  );

  // *********** Actions *********** //
  readonly filterSearch = this.effect<string | null>((search$) => {
    return search$.pipe(
      tap((search) => {
        const { filterSearch: previous } = this.vm$();
        const value = isEmpty(search) === false ? search : null;

        if (previous !== value) {
          this.router.navigate([], {
            relativeTo: this.route,
            queryParams: {
              q: value,
              page: null,
            },
            queryParamsHandling: 'merge',
          });

          this.setFilterSearch(value);
          this.setFilterCurrentPage(DEFAULT_STATE.filterCurrentPage);
        }
      }),
      map(() => {
        this.getAllEWalidataIndicatorMonitor();
      }),
    );
  });

  readonly filterSortTable = this.effect<string>((sort$) => {
    return sort$.pipe(
      tap((sort) => {
        const obj = sort.split(':');

        this.router.navigate([], {
          relativeTo: this.route,
          queryParams: {
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        this.setFilterSort(obj[0]);
        this.setFilterDirection(obj[1]);
        this.setFilterCurrentPage(DEFAULT_STATE.filterCurrentPage);
      }),
      map(() => {
        this.getAllEWalidataIndicatorMonitor();
      }),
    );
  });

  readonly filterPerPage = this.effect<number>((perPage$) => {
    return perPage$.pipe(
      tap((perPage) => {
        this.router.navigate([], {
          relativeTo: this.route,
          queryParams: {
            perPage,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        this.setFilterPerPage(perPage);
        this.setFilterCurrentPage(DEFAULT_STATE.filterCurrentPage);
      }),
      map(() => {
        this.getAllEWalidataIndicatorMonitor();
      }),
    );
  });

  readonly filterCurrentPage = this.effect<number>((currentPage$) => {
    return currentPage$.pipe(
      tap((currentPage) => {
        this.router.navigate([], {
          relativeTo: this.route,
          queryParams: {
            page: currentPage,
          },
          queryParamsHandling: 'merge',
        });

        this.setFilterCurrentPage(currentPage);
      }),
      map(() => {
        this.getAllEWalidataIndicatorMonitor();
      }),
    );
  });

  readonly filterBusinessField = this.effect<string>((businessField$) => {
    return businessField$.pipe(
      tap((businessField) => {
        this.router.navigate([], {
          relativeTo: this.route,
          queryParams: {
            businessField,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        this.setFilterBusinessField(businessField);
        this.setFilterCurrentPage(DEFAULT_STATE.filterCurrentPage);
      }),
      map(() => {
        this.getAllEWalidataIndicatorMonitor();
      }),
    );
  });

  readonly filterOrganization = this.effect<string[]>((organization$) => {
    return organization$.pipe(
      tap((organization) => {
        this.router.navigate([], {
          relativeTo: this.route,
          queryParams: {
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        this.setFilterOrganization(organization);
        this.setFilterCurrentPage(DEFAULT_STATE.filterCurrentPage);
      }),
      map(() => {
        this.getAllEWalidataIndicatorMonitor();
      }),
    );
  });

  readonly getAllBusinessField = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      switchMap(() => {
        const params = {
          sort: `kode_bidang_urusan:asc`,
        };

        const options = {
          pagination: false,
        };

        return this.businessFieldService
          .getQueryList(params, options)
          .result$.pipe(
            tap({
              next: (result) => {
                this.setListBusinessField(result);
              },
            }),
          );
      }),
    );
  });

  readonly getAllOrganizationSatuData = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      switchMap(() => {
        const { filterOrganization: organization } = this.vm$();

        const params = {
          sort: `nama_skpd:asc`,
          where: JSON.stringify({
            is_deleted: false,
          }),
        };

        const options = {
          pagination: false,
        };
        const app = 'satudata';

        return this.organizationService
          .getQueryList(params, options, app)
          .result$.pipe(
            tap({
              next: (result) => {
                if (result.data) {
                  if (isEmpty(organization)) {
                    result.data.list.map((item) => (item.checked = false));
                  } else {
                    result.data.list.forEach((item) => {
                      if (organization.includes(item.kode_skpd)) {
                        item.checked = true;
                      } else {
                        item.checked = false;
                      }
                    });
                  }
                }

                this.setListOrganizationSatuData(result);
              },
            }),
          );
      }),
    );
  });

  readonly getAllEWalidataIndicatorMonitor = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      switchMap(() => {
        const {
          filterSearch: search,
          filterSort: sort,
          filterDirection: direction,
          filterPerPage: perPage,
          filterCurrentPage: currentPage,
          filterBusinessField: businessField,
          filterOrganization: organization,
        } = this.vm$();

        const pWhere = {};

        if (!isEmpty(businessField)) {
          assign(pWhere, { kode_bidang_urusan: businessField });
        }

        const params = {
          search: search || '',
          sort: `${sort}:${direction}`,
          skip: perPage * currentPage - perPage,
          limit: perPage,
          where: JSON.stringify(pWhere),
        };

        if (!isEmpty(organization)) {
          assign(params, { kode_skpd: organization });
        }

        return this.eWalidataMonitorService
          .getQueryListIndicatorMonitor(params)
          .result$.pipe(
            tap({
              next: (result) => {
                this.setEWalidataMonitorIndicatorMonitor(result);
              },
            }),
          );
      }),
    );
  });
}
