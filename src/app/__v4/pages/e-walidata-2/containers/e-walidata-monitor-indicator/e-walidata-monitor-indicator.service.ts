import { inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';

// MODEL
import { LoginData } from '@models-v3';
import { BusinessFieldIndicatorMonitor } from '@models-v4';
// SERVICE
import { AuthenticationService, PaginationService } from '@services-v3';
// UTIL
import { hasOwnProperty } from '@utils-v4';

import { QueryClientService, UseQuery } from '@ngneat/query';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root',
})
export class EWalidataMonitorIndicatorService {
  satudataUser: LoginData;
  title = 'E-Walidata Monitor';

  // API URL
  apiUrlBusinessFieldIndicator = `${environment.backendURL}bidang_urusan_indikator/monitor`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  private router = inject(Router);
  private http = inject(HttpClient);
  private queryClient = inject(QueryClientService);
  private useQuery = inject(UseQuery);
  private authenticationService = inject(AuthenticationService);
  private paginationService = inject(PaginationService);
  private toastr = inject(ToastrService);

  constructor() {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
  }

  // Can Activate
  canActivate(): Observable<boolean> {
    let access: boolean;
    switch (this.satudataUser.role_name) {
      case 'walidata':
        access = true;
        break;
      case 'opd':
      case 'opd-view':
        access = false;
        break;
      default:
        break;
    }

    if (!access) {
      this.router.navigate(['/404']);
      return of(false);
    }

    return of(true);
  }

  // List Business Field
  getListIndicatorMonitor(params: any, options: any) {
    const merge = new URLSearchParams(params);

    const queryName = `List ${this.title} Indikator Monitor`;

    return this.http
      .get<any>(
        `${this.apiUrlBusinessFieldIndicator}?${merge}`,
        this.httpOptions,
      )
      .pipe(
        map((response) => {
          if (response.error === 1 && response.message !== 'Data not found') {
            throw new Error(response.message);
          }

          return new BusinessFieldIndicatorMonitor().deserialize(response);
        }),
        map((response) => {
          const hasPagination = hasOwnProperty(options, 'pagination');
          const hasInfinite = hasOwnProperty(options, 'infinite');

          const isPagination = hasPagination ? options.pagination : true;
          const isInfinite = hasInfinite ? options.infinite : false;

          let pagination = { empty: true, infinite: isInfinite };
          if (isPagination) {
            const resultPagination = this.paginationService.getPagination(
              response.meta.total_record,
              params.skip,
              params.limit,
            );

            pagination = {
              empty: false,
              infinite: isInfinite,
              ...resultPagination,
            };
          }

          return {
            pagination,
            list: response.data,
          };
        }),
        catchError((e) => {
          this.toastr.error(e.message, queryName);
          return of(null);
        }),
      );
  }

  getQueryListIndicatorMonitor(params: any, options: any = {}) {
    return this.useQuery(
      ['eWalidataIndicatorMonitor'],
      () => {
        return this.getListIndicatorMonitor(params, options);
      },
      options,
    );
  }
}
