// import { inject } from '@angular/core';
import { Routes } from '@angular/router';

// GUARDS
import { StoreIndicatorDetailGuard } from '@guards-v3';

// import { DatasetDetailService } from './containers/dataset-detail/dataset-detail.service';

// const canActivateDatasetDetail: CanActivateFn = (
//   route: ActivatedRouteSnapshot,
// ) => {
//   return inject(DatasetDetailService).canActivate(route.params.id);
// };

export const INDICATOR_ROUTES: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        loadComponent: () =>
          import('./containers/indicator-page/indicator-page.component').then(
            (m) => m.IndicatorPageComponent,
          ),
      },
      // {
      //   path: 'detail/:id',
      //   canActivate: [canActivateDatasetDetail],
      //   loadComponent: () =>
      //     import('./containers/dataset-detail/dataset-detail.component').then(
      //       (m) => m.DatasetDetailComponent,
      //     ),
      // },
      {
        path: 'detail/:id',
        canActivate: [StoreIndicatorDetailGuard],
        loadChildren: () =>
          import(
            '../../../indicator/indicator-detail-v2/indicator-detail-v2.module'
          ).then((m) => m.IndicatorDetailV2Module),
      },
      {
        path: 'download',
        loadChildren: () =>
          import(
            '../../../indicator/indicator-download/indicator-download.module'
          ).then((m) => m.IndicatorDownloadModule),
      },
    ],
  },
];
