import { computed, inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs';
import {
  patchState,
  signalStore,
  signalStoreFeature,
  withComputed,
  withHooks,
  withMethods,
  withState,
} from '@ngrx/signals';
import { rxMethod } from '@ngrx/signals/rxjs-interop';
import { tapResponse } from '@ngrx/operators';

// CONSTANT
import { SORT_INDICATOR, STATUS_INDICATOR } from '@constants-v4';
// FEATURE
import {
  withClassificationIndicator,
  withIndicatorCategory,
  withOrganizationSatuData,
} from '@features-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';
import { IndicatorService } from '@services-v4';

import { assign, isEmpty } from 'lodash';

type IndicatorPageState = {
  satudataUser: LoginData;
  filterSearch: string | null;
  filterSort: string;
  filterOrganization: string[];
  filterClassification: number[] | string[];
  filterCategory: number[];
  filterPerPage: number;
  filterCurrentPage: number;
  listIndicator: any;
};

export const initialState: IndicatorPageState = {
  satudataUser: null,
  filterSearch: null,
  filterSort: 'newest',
  filterOrganization: [],
  filterClassification: [],
  filterCategory: [],
  filterPerPage: 5,
  filterCurrentPage: 1,
  listIndicator: null,
};

export function withIndicatorPage() {
  return signalStoreFeature(
    withOrganizationSatuData(),
    withClassificationIndicator(),
    withIndicatorCategory(),
  );
}

export const IndicatorPageStore = signalStore(
  withState(initialState),
  withHooks((store) => {
    const authenticationService = inject(AuthenticationService);

    return {
      onInit() {
        authenticationService.satudataUser.subscribe((x) => {
          patchState(store, { satudataUser: x });
        });
      },
    };
  }),
  withComputed((store) => ({
    hasFilterQuickAccess: computed(() => {
      if (
        store.filterOrganization().length > 0 ||
        store.filterClassification().length > 0 ||
        store.filterCategory().length > 0
      ) {
        return true;
      }

      return false;
    }),
    hasFilterSearch: computed(() => {
      if (
        store.filterSearch() ||
        store.filterOrganization().length ||
        store.filterClassification().length ||
        store.filterCategory().length
      ) {
        return true;
      }

      return false;
    }),
  })),
  withIndicatorPage(),
  withMethods(
    (
      store,
      router = inject(Router),
      route = inject(ActivatedRoute),
      indicatorService = inject(IndicatorService),
    ) => ({
      handleFilterSearch(search: string | null): void {
        const { filterSearch: previous } = store;
        const value = isEmpty(search) === false ? search : null;

        if (previous() !== value) {
          router.navigate([], {
            relativeTo: route,
            queryParams: {
              q: value,
              page: null,
            },
            queryParamsHandling: 'merge',
          });

          patchState(store, () => ({
            filterSearch: value,
            filterCurrentPage: initialState.filterCurrentPage,
          }));

          this.getAllIndicator();
        }
      },
      handleFilterSort(sort: string): void {
        router.navigate([], {
          relativeTo: route,
          queryParams: {
            sort,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterSort: sort,
          filterCurrentPage: initialState.filterCurrentPage,
        }));

        this.getAllIndicator();
      },
      handleFilterOrganization(organization: string[]): void {
        const value = !isEmpty(organization) ? organization.join(',') : null;

        router.navigate([], {
          relativeTo: route,
          queryParams: {
            organization: value,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterOrganization: organization,
          filterCurrentPage: initialState.filterCurrentPage,
        }));

        this.getAllIndicator();
      },
      handleFilterClassification(classification: number[] | string[]): void {
        const value = !isEmpty(classification)
          ? classification.join(',')
          : null;

        router.navigate([], {
          relativeTo: route,
          queryParams: {
            classification: value,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterClassification: classification,
          filterCurrentPage: initialState.filterCurrentPage,
        }));

        this.getAllIndicator();
      },
      handleFilterCategory(category: number[]): void {
        const value = !isEmpty(category) ? category.join(',') : null;

        router.navigate([], {
          relativeTo: route,
          queryParams: {
            category: value,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterCategory: category,
          filterCurrentPage: initialState.filterCurrentPage,
        }));

        this.getAllIndicator();
      },
      handleFilterPerPage(perPage: number): void {
        router.navigate([], {
          relativeTo: route,
          queryParams: {
            perPage,
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterPerPage: perPage,
          filterCurrentPage: initialState.filterCurrentPage,
        }));

        this.getAllIndicator();
      },
      handleFilterCurrentPage(currentPage: number): void {
        router.navigate([], {
          relativeTo: route,
          queryParams: {
            page: currentPage,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterCurrentPage: currentPage,
        }));

        this.getAllIndicator();
      },
      handleFilterClear(): void {
        router.navigate([], {
          relativeTo: route,
          queryParams: {
            organization: [],
            classification: [],
            category: [],
            page: null,
          },
          queryParamsHandling: 'merge',
        });

        patchState(store, () => ({
          filterOrganization: initialState.filterOrganization,
          filterClassification: initialState.filterClassification,
          filterCategory: initialState.filterCategory,
          filterCurrentPage: initialState.filterCurrentPage,
        }));

        this.getAllIndicator();
      },
      getAllIndicator: rxMethod<void>(
        switchMap(() => {
          const {
            filterSearch: search,
            filterOrganization: organization,
            filterClassification: classification,
            filterCategory: category,
            filterSort: sort,
            filterPerPage: perPage,
            filterCurrentPage: currentPage,
          } = store;

          const pWhere = {};

          assign(pWhere, STATUS_INDICATOR.get('approve-active'));

          if (!isEmpty(organization())) {
            assign(pWhere, { kode_skpd: organization() });
          }

          if (!isEmpty(classification())) {
            assign(pWhere, { classification: classification() });
          }

          if (!isEmpty(category())) {
            assign(pWhere, { indikator_category_id: category() });
          }

          const params = {
            search: search() || '',
            sort: SORT_INDICATOR.get(sort()).paramSort,
            skip: perPage() * currentPage() - perPage(),
            limit: perPage(),
            where: JSON.stringify(pWhere),
          };

          return indicatorService.getQueryList(params).result$.pipe(
            tapResponse({
              next: (result) => {
                patchState(store, { listIndicator: result });
              },
              error: console.error,
            }),
          );
        }),
      ),
    }),
  ),
);
