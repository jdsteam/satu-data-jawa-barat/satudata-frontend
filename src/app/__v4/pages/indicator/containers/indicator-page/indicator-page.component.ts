import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
  inject,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Title } from '@angular/platform-browser';
import { RouterModule, Router, ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';
// COMPONENT
import {
  HeaderCatalogComponent,
  FilterOrganizationComponent,
  FilterClassificationIndicatorComponent,
  FilterCategoryIndicatorComponent,
  SkeletonFilterComponent,
  SearchCatalogComponent,
  SortCatalogComponent,
  QuickAccessCatalogComponent,
  ListIndicatorComponent,
  ListEmptyComponent,
  SkeletonCatalogComponent,
} from '@components-v4';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Breadcrumb } from '@interfaces-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { StateService } from '@services-v3';
import { GlobalService } from '@services-v4';
// JDS-BI
import {
  JdsBiAccordionModule,
  JdsBiButtonModule,
  JdsBiPaginationModule,
} from '@jds-bi/core';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconChevronRight,
} from '@jds-bi/icons';

import { isEmpty, isNil, set, split } from 'lodash';
import moment from 'moment';
import { patchState } from '@ngrx/signals';
import { fromWorker } from 'observable-webworker';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { SubscribeDirective } from '@ngneat/subscribe';

import { initialState, IndicatorPageStore } from './indicator-page.store';

@Component({
  selector: 'app-indicator-page',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    HeaderCatalogComponent,
    JdsBiAccordionModule,
    JdsBiButtonModule,
    JdsBiPaginationModule,
    JdsBiIconsModule,
    FilterOrganizationComponent,
    FilterClassificationIndicatorComponent,
    FilterCategoryIndicatorComponent,
    SkeletonFilterComponent,
    SearchCatalogComponent,
    SortCatalogComponent,
    QuickAccessCatalogComponent,
    ListIndicatorComponent,
    ListEmptyComponent,
    SkeletonCatalogComponent,
    NgxSkeletonLoaderModule,
    SubscribeDirective,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './indicator-page.component.html',
  styleUrls: ['./indicator-page.component.scss'],
  providers: [IndicatorPageStore],
})
export class IndicatorPageComponent implements OnInit, OnDestroy {
  satudataUser: LoginData;
  satudataToken: string;
  colorScheme = COLORSCHEME;
  jds: any = jds;
  moment = moment;

  // Log
  log = null;
  pageInfo = null;
  deviceInfo = null;
  userInfo = null;

  // Setting
  title: string;
  label = 'Katalog Indikator';
  description =
    'Layanan portal Satu Data Jabar menyediakan akses ke indikator ' +
    'pembangunan dari seluruh Organisasi Perangkat Daerah di Provinsi Jawa Barat yang ' +
    'terdapat dalam dokumen perencanaan, meliputi LAKIP, LKPD, LKPJ, RKPD, RPJMD, dan SDGs.';
  breadcrumb: Breadcrumb[] = [{ label: 'Indikator', link: '/indicator' }];

  // Service
  private router = inject(Router);
  private route = inject(ActivatedRoute);
  private globalService = inject(GlobalService);
  private stateService = inject(StateService);
  private titleService = inject(Title);
  private iconService = inject(JdsBiIconsService);
  private indicatorPageStore = inject(IndicatorPageStore);

  // Component Store View Model
  readonly vm = this.indicatorPageStore;

  // Variable
  perPageItems: any[] = [
    { value: 5, label: '5' },
    { value: 10, label: '10' },
  ];

  constructor() {
    this.stateService.currentLogUser.subscribe((x) => (this.userInfo = x));

    this.iconService.registerIcons([iconChevronRight]);
  }

  ngOnInit(): void {
    this.logPage('init');
    this.settingsAll();
    this.queryParams();

    this.indicatorPageStore.getAllOrganizationSatuData();
    this.indicatorPageStore.getAllClassificationIndicator();
    this.indicatorPageStore.getAllIndicatorCategory();
    this.indicatorPageStore.getAllIndicator();
  }

  ngOnDestroy(): void {
    this.logPage('destroy');
  }

  settingsAll(): void {
    this.title = this.globalService.title;
    this.titleService.setTitle(`${this.label} - ${this.title}`);
    this.globalService.changeLabel(this.label);
    this.globalService.changeBreadcrumb(this.breadcrumb);
  }

  queryParams(): void {
    this.route.queryParams.subscribe((p) => {
      const checkArray = (param, value) => {
        return !isNil(param) && !isEmpty(param) ? split(param, ',') : value;
      };

      const {
        filterSearch: search,
        filterSort: sort,
        filterOrganization: organization,
        filterClassification: classification,
        filterCategory: category,
        filterPerPage: perPage,
        filterCurrentPage: currentPage,
      } = initialState;

      const pSearch = !isNil(p['q']) ? p['q'] : search;
      const pSort = !isNil(p['sort']) ? p['sort'] : sort;
      const pOrganization = checkArray(p['organization'], organization);
      const pClassification = checkArray(p['classification'], classification);
      const pCategory = checkArray(p['category'], category);
      const pPerPage = !isNil(p['perPage']) ? p['perPage'] : perPage;
      const pCurrentPage = !isNil(p['page']) ? p['page'] : currentPage;

      patchState(this.vm, {
        filterSearch: pSearch,
        filterSort: pSort,
        filterOrganization: pOrganization,
        filterClassification: pClassification,
        filterCategory: pCategory,
        filterPerPage: pPerPage,
        filterCurrentPage: pCurrentPage,
      });
    });
  }

  // Log =======================================================================
  logPage(type: string): void {
    if (type === 'init') {
      this.pageInfo = {
        page: this.router.url,
        date_start: moment().format('YYYY-MM-DD HH:mm:ss'),
      };

      this.log = {
        log: { ...this.pageInfo, ...this.userInfo },
        token: this.satudataToken,
      };
    } else if (type === 'destroy') {
      if (typeof Worker !== 'undefined') {
        set(this.log.log, 'date_end', moment().format('YYYY-MM-DD HH:mm:ss'));

        const log = of(this.log);

        fromWorker<object, string>(
          () =>
            new Worker(
              new URL('src/app/__v4/workers/log-page.worker', import.meta.url),
              { type: 'module' },
            ),
          log,
        ).subscribe((message) => {
          if (!environment.production) {
            console.info(`Log page: ${message}`);
          }
        });
      }
    }
  }
}
