import { Component, ChangeDetectionStrategy, inject } from '@angular/core';
import { CommonModule, DecimalPipe } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// JDS-BI
import {
  JdsBiTableHeadModule,
  JdsBiDropdownModule,
  JdsBiHostedDropdownModule,
  JdsBiPaginationModule,
  JdsBiBadgeModule,
  JdsBiButtonModule,
  JdsBiInputModule,
} from '@jds-bi/core';

import moment from 'moment';
import 'moment/locale/id';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgxMaskModule } from 'ngx-mask';
import { SubscribeDirective } from '@ngneat/subscribe';

import { NumberFormatPipe } from '@pipes-v4';
import { StickyColumnDirective, StickyTableDirective } from '@directives-v4';
import { AuthenticationService } from '@services-v3';
import { LoginData } from '@models-v3';
import { NgSelectModule } from '@ng-select/ng-select';
import { Styles } from '@interfaces-v4';
import { EWalidataStore } from '../../e-walidata.store';

@Component({
  selector: 'app-e-walidata-list-rincian-uraian',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    JdsBiTableHeadModule,
    JdsBiDropdownModule,
    JdsBiHostedDropdownModule,
    JdsBiPaginationModule,
    JdsBiBadgeModule,
    JdsBiButtonModule,
    NgxSkeletonLoaderModule,
    JdsBiInputModule,
    NumberFormatPipe,
    StickyColumnDirective,
    StickyTableDirective,
    NgxMaskModule,
    NgSelectModule,
    SubscribeDirective,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './e-walidata-list-rincian-uraian.component.html',
  providers: [DecimalPipe],
})
export class EWalidataListRincianUraianComponent {
  satudataUser: LoginData;
  colorScheme = COLORSCHEME;
  jds: Styles = jds;
  moment: any = moment;

  // Variable
  typeOptions = [
    { value: 'belum-terisi', label: 'Belum Terisi', checked: false },
    { value: 'terisi', label: 'Terisi', checked: false },
  ];
  perPageItems = [
    { value: 10, label: '10' },
    { value: 25, label: '25' },
  ];

  private authenticationService = inject(AuthenticationService);

  get isWalidata(): boolean {
    return this.satudataUser?.role_name === 'walidata';
  }

  get isOPD(): boolean {
    return this.satudataUser?.role_name === 'opd';
  }

  // STORE
  private eWalidataStore = inject(EWalidataStore);
  readonly vm$ = this.eWalidataStore.vm$;

  constructor() {
    this.authenticationService.satudataUser.subscribe((x) => {
      this.satudataUser = x;
    });
    this.moment.locale('id');
  }

  getRincian(row: any, tahun: any) {
    return row.rincian.find((item: any) => item.tahun === tahun.label);
  }

  getRincianIndex(row: any, tahun: any) {
    return row.ewalidata_data.findIndex(
      (item: any) => item.tahun === tahun.label,
    );
  }

  getListIndikatorBidangUrusan() {
    if (this.isWalidata)
      this.eWalidataStore.fetchListIndikatorBidangUrusanWalidata();
    if (this.isOPD) this.eWalidataStore.fetchListIndikatorBidangUrusanOPD();
  }

  onPerPage(value: any) {
    this.eWalidataStore.setPaginationParamsIndikatorUrusan({
      perPage: value,
      currentPage: 1,
    });
    this.getListIndikatorBidangUrusan();
  }

  onPageChange(value: any) {
    this.eWalidataStore.setPaginationParamsIndikatorUrusan({
      currentPage: value,
    });
    this.getListIndikatorBidangUrusan();
  }
}
