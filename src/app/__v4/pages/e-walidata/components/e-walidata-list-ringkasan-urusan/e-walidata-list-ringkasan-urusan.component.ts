import {
  Component,
  ChangeDetectionStrategy,
  inject,
  OnInit,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// JDS-BI
import {
  JdsBiTableHeadModule,
  JdsBiDropdownModule,
  JdsBiHostedDropdownModule,
  JdsBiPaginationModule,
  JdsBiBadgeModule,
  JdsBiButtonModule,
} from '@jds-bi/core';

import moment from 'moment';
import 'moment/locale/id';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgSelectModule } from '@ng-select/ng-select';
import { Styles } from '@interfaces-v4';
import { EWalidataStore } from '../../e-walidata.store';

@Component({
  selector: 'app-e-walidata-list-ringkasan-urusan',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    JdsBiTableHeadModule,
    JdsBiDropdownModule,
    JdsBiHostedDropdownModule,
    JdsBiPaginationModule,
    JdsBiBadgeModule,
    JdsBiButtonModule,
    NgxSkeletonLoaderModule,
    NgSelectModule,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './e-walidata-list-ringkasan-urusan.component.html',
})
export class EWalidataListRingkasanUrusanComponent implements OnInit {
  colorScheme = COLORSCHEME;
  jds: Styles = jds;
  moment: any = moment;

  // Variable
  typeOptions = [
    { value: 'dataset', label: 'Dataset', checked: false },
    { value: 'mapset', label: 'Mapset', checked: false },
  ];
  perPageItems = [
    { value: 10, label: '10' },
    { value: 25, label: '25' },
  ];
  filterDirection = 'asc';
  filterSort = 'kode_bidang_urusan';

  // STORE
  private eWalidataStore = inject(EWalidataStore);
  readonly vm$ = this.eWalidataStore.vm$;

  constructor() {
    this.moment.locale('id');
  }

  ngOnInit(): void {
    this.getSKPDMasterData();
    this.getListBidangUrusanWalidata();
  }

  onPerPage(value: any) {
    this.eWalidataStore.setPaginationParamsBidangUrusanWalidata({
      perPage: value,
      currentPage: 1,
    });
    this.getListBidangUrusanWalidata();
  }

  onPageChange(value: any) {
    this.eWalidataStore.setPaginationParamsBidangUrusanWalidata({
      currentPage: value,
    });
    this.getListBidangUrusanWalidata();
  }

  onSort(value: any) {
    let dir = '';
    if (this.filterDirection === 'desc') {
      dir = 'asc';
    } else if (this.filterDirection === 'asc') {
      dir = 'desc';
    }

    this.filterDirection = dir;
    this.filterSort = value;
    this.eWalidataStore.setPaginationParamsBidangUrusanWalidata({
      sort: `${value}:${dir}`,
    });
    this.getListBidangUrusanWalidata();
  }

  getListBidangUrusanWalidata() {
    this.eWalidataStore.fetchListBidangUrusanWalidata();
  }

  getSKPDMasterData() {
    this.eWalidataStore.fetchSKPDMasterData();
  }

  filterSKPD(value: any) {
    this.eWalidataStore.setFilterSKPD(value?.kode_skpd);
    this.eWalidataStore.setPaginationParamsBidangUrusanWalidata({
      currentPage: 1,
    });
    this.getListBidangUrusanWalidata();
  }

  disableLocalSearch() {
    return false;
  }
}
