import { CommonModule } from '@angular/common';
import { Component, OnInit, inject } from '@angular/core';
import {
  BreadcrumbComponent,
  HeaderCatalogComponent,
  SkeletonCatalogComponent,
} from '@components-v4';
import { Breadcrumb, Styles } from '@interfaces-v4';
import * as jds from '@styles';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { jdsBiColor } from '@jds-bi/cdk';
import moment from 'moment';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconMagnifyingGlass,
  iconCircleXmark,
  iconClockThree,
  iconCircleInfo,
} from '@jds-bi/icons';
import { SubscribeDirective } from '@ngneat/subscribe';
import { NgxLoadingModule } from 'ngx-loading';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
import {
  JdsBiNavModule,
  JdsBiInputModule,
  JdsBiButtonModule,
} from '@jds-bi/core';
import { LoginData } from '@models-v3';
import { AuthenticationService } from '@services-v3';
import { GlobalService } from '@services-v4';
import { Title } from '@angular/platform-browser';
import {
  EWalidataListRingkasanUrusanComponent,
  EWalidataListRincianUraianComponent,
} from '../../components';
import { EWalidataStore } from '../../e-walidata.store';

@Component({
  selector: 'app-e-walidata-page',
  templateUrl: './e-walidata-page.component.html',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    BreadcrumbComponent,
    HeaderCatalogComponent,
    NgSelectModule,
    JdsBiIconsModule,
    JdsBiNavModule,
    EWalidataListRingkasanUrusanComponent,
    EWalidataListRincianUraianComponent,
    JdsBiInputModule,
    JdsBiButtonModule,
    SkeletonCatalogComponent,
    NgxLoadingModule,
    SubscribeDirective,
  ],
  providers: [EWalidataStore],
})
export class EWalidataPageComponent implements OnInit {
  satudataUser: LoginData;
  jds: Styles = jds;
  jdsBiColor = jdsBiColor;
  colorScheme = COLORSCHEME;

  moment: any = moment;
  tabActiveId: 'ringkasan-urusan' | 'rincian-uraian' = 'ringkasan-urusan';

  // Setting
  title: string;
  label = 'E-WALIDATA';
  description =
    'Integrasi antara Satu Data Jabar dan SIPD Perencanaan bertujuan untuk meningkatkan efektivitas dan efisiensi dalam pengelolaan data dan perencanaan pembangunan di Provinsi Jawa Barat.';
  breadcrumb: Breadcrumb[] = [{ label: 'E-Walidata', link: '/e-walidata' }];

  urusanFilter = '1.01';
  private iconService = inject(JdsBiIconsService);
  private authenticationService = inject(AuthenticationService);
  private globalService = inject(GlobalService);
  private titleService = inject(Title);

  get isWalidata(): boolean {
    return this.satudataUser?.role_name === 'walidata';
  }

  get isOPD(): boolean {
    return this.satudataUser?.role_name === 'opd';
  }

  // STORE
  private eWalidataStore = inject(EWalidataStore);
  readonly vm$ = this.eWalidataStore.vm$;

  constructor() {
    this.authenticationService.satudataUser.subscribe((x) => {
      this.satudataUser = x;
      if (this.isOPD) this.tabActiveId = 'rincian-uraian';
    });
    this.iconService.registerIcons([
      iconMagnifyingGlass,
      iconCircleXmark,
      iconClockThree,
      iconCircleInfo,
    ]);
  }

  ngOnInit(): void {
    this.settingsAll();
    this.getListTahun();
    if (this.isOPD) this.getListUrusanOPD();
    this.getBidangUrusanMasterData();
  }

  settingsAll(): void {
    this.title = this.globalService.title;
    this.titleService.setTitle(`${this.label} - ${this.title}`);
    this.globalService.changeLabel(this.label);
    this.globalService.changeBreadcrumb(this.breadcrumb);
  }

  getListTahun() {
    this.eWalidataStore.enumerateTahun();
  }

  getListUrusanOPD() {
    this.eWalidataStore.fetchListUrusanOPD();
  }

  getListBidangUrusanWalidata() {
    this.eWalidataStore.fetchListBidangUrusanWalidata();
  }

  getBidangUrusanMasterData() {
    if (this.isWalidata)
      this.eWalidataStore.fetchBidangUrusanMasterDataWalidata();
    if (this.isOPD) this.eWalidataStore.fetchBidangUrusanMasterDataOPD();
  }

  filterTahun(value: any) {
    if (this.isWalidata) {
      this.eWalidataStore.setFilterTahunWalidata(value.value);
      this.eWalidataStore.setPaginationParamsBidangUrusanWalidata({
        currentPage: 1,
      });
      this.getListBidangUrusanWalidata();
    }
    if (this.isOPD) {
      this.eWalidataStore.setFilterTahunOPD(value.value);
      this.eWalidataStore.setPaginationParamsBidangUrusanOPD({
        currentPage: 1,
      });
      this.getListUrusanOPD();
    }
  }

  filterBidangUrusan(value: any) {
    this.eWalidataStore.setFilterBidangUrusan(value.kode_bidang_urusan);
    this.eWalidataStore.setPaginationParamsIndikatorUrusan({
      currentPage: 1,
    });
    this.getListIndikatorBidangUrusan();
  }

  filterSearchIndikatorBidangUraian(value: any) {
    this.eWalidataStore.setPaginationParamsIndikatorUrusan({
      currentPage: 1,
      search: value,
    });
    this.getListIndikatorBidangUrusan();
  }

  getListIndikatorBidangUrusan() {
    if (this.isWalidata)
      this.eWalidataStore.fetchListIndikatorBidangUrusanWalidata();
    if (this.isOPD) this.eWalidataStore.fetchListIndikatorBidangUrusanOPD();
  }

  disableLocalSearch() {
    return false;
  }

  filterSearchBidangUrusanMasterData(value: any) {
    this.eWalidataStore.setPaginationParamsMasterDataBidangUrusan({
      currentPage: 1,
      search: value.term,
    });
    this.getBidangUrusanMasterData();
  }

  onTabChanged(value: any) {
    if (value === 'rincian-uraian' && this.isWalidata) {
      this.getListIndikatorBidangUrusan();
    }
  }
}
