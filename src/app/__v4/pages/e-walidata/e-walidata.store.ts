import { Injectable, inject } from '@angular/core';
import { ComponentStore } from '@ngrx/component-store';
import { LoginData } from '@models-v3';
import { AuthenticationService } from '@services-v3';
import { OrganizationService } from '@services-v4';
import { assign, range, uniqWith } from 'lodash';
import moment from 'moment';
import {
  tap,
  switchMap,
  withLatestFrom,
  Subject,
  distinctUntilChanged,
  debounceTime,
  map,
} from 'rxjs';
import { mapResultData, useMutationResult } from '@ngneat/query';
import { EWalidataService } from './e-walidata.service';

export interface PaginationParams {
  currentPage: number;
  sort?: string;
  perPage: number;
  search?: string;
  searchInput$?: Subject<string>;
}

export interface EWalidataState {
  listTahun?: [{ label: string; value: number }];
  listUrusanOPD?: any;
  listUrusanWalidata?: any;
  listIndikatorBidangUrusan?: any;
  skpdMasterData?: any;
  bidangUrusanMasterData?: any;
  filterTahunOPD?: any;
  filterTahunWalidata?: any;
  filterSearchUrusan?: any;
  filterSearchUraianUrusan?: any;
  filterSKPD?: any;
  filterBidangUrusan?: any;
  paginationParamsBidangUrusanWalidata?: PaginationParams;
  paginationParamsBidangUrusanOPD?: PaginationParams;
  paginationParamsIndikatorUrusan?: PaginationParams;
  paginationParamsMasterDataBidangUrusan?: PaginationParams;
  paginationParamsMasterDataSKPD?: PaginationParams;
}

export const DEFAULT_STATE: EWalidataState = {
  listTahun: null,
  listUrusanOPD: null,
  listUrusanWalidata: null,
  listIndikatorBidangUrusan: null,
  filterTahunOPD: moment().year(),
  filterTahunWalidata: moment().year(),
  filterSearchUrusan: null,
  filterSearchUraianUrusan: null,
  filterSKPD: null,
  filterBidangUrusan: null,
  skpdMasterData: null,
  bidangUrusanMasterData: null,
  paginationParamsBidangUrusanWalidata: {
    currentPage: 1,
    sort: null,
    perPage: 10,
    search: null,
  },
  paginationParamsBidangUrusanOPD: {
    currentPage: 1,
    sort: null,
    perPage: 100,
    search: null,
  },
  paginationParamsIndikatorUrusan: {
    currentPage: 1,
    sort: null,
    perPage: 10,
    search: null,
  },
  paginationParamsMasterDataBidangUrusan: {
    currentPage: 1,
    sort: null,
    perPage: 100,
    search: null,
    searchInput$: new Subject<string>(),
  },
  paginationParamsMasterDataSKPD: {
    currentPage: 1,
    sort: null,
    perPage: 100,
    search: null,
    searchInput$: new Subject<string>(),
  },
};

@Injectable()
export class EWalidataStore extends ComponentStore<EWalidataState> {
  satudataUser: LoginData;
  today = moment();
  pushDataMutation = useMutationResult();

  private authenticationService = inject(AuthenticationService);
  private eWalidataService = inject(EWalidataService);
  private organizationService = inject(OrganizationService);

  constructor() {
    super(DEFAULT_STATE);

    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
  }

  // UPDATERS
  readonly setListTahun = this.updater((state, value: any) => ({
    ...state,
    listTahun: value || [],
  }));

  readonly setListUrusanOPD = this.updater((state, value: any) => ({
    ...state,
    listUrusanOPD: value || [],
  }));

  readonly setListUrusanWalidata = this.updater((state, value: any) => ({
    ...state,
    listUrusanWalidata: value || [],
  }));

  readonly setListIndikatorBidangUrusan = this.updater((state, value: any) => ({
    ...state,
    listIndikatorBidangUrusan: value || [],
  }));

  readonly setFilterTahunOPD = this.updater((state, value: any) => ({
    ...state,
    filterTahunOPD: value,
  }));

  readonly setFilterTahunWalidata = this.updater((state, value: any) => ({
    ...state,
    filterTahunWalidata: value,
  }));

  readonly setFilterSearchUrusan = this.updater((state, value: any) => ({
    ...state,
    filterSearchUrusan: value,
  }));

  readonly setFilterSearchUraianUrusan = this.updater((state, value: any) => ({
    ...state,
    filterSearchUraianUrusan: value,
  }));

  readonly setFilterSKPD = this.updater((state, value: any) => ({
    ...state,
    filterSKPD: value,
  }));

  readonly setFilterBidangUrusan = this.updater((state, value: any) => ({
    ...state,
    filterBidangUrusan: value,
  }));

  readonly setSKPDMasterData = this.updater((state, value: any) => ({
    ...state,
    skpdMasterData: value,
  }));

  readonly setBidangUrusanMasterData = this.updater((state, value: any) => ({
    ...state,
    bidangUrusanMasterData: value,
  }));

  readonly setPaginationParamsBidangUrusanWalidata = this.updater(
    (state, value: any) => ({
      ...state,
      paginationParamsBidangUrusanWalidata: {
        ...state.paginationParamsBidangUrusanWalidata,
        ...value,
      },
    }),
  );

  readonly setPaginationParamsBidangUrusanOPD = this.updater(
    (state, value: any) => ({
      ...state,
      paginationParamsBidangUrusanOPD: {
        ...state.paginationParamsBidangUrusanOPD,
        ...value,
      },
    }),
  );

  readonly setPaginationParamsIndikatorUrusan = this.updater(
    (state, value: any) => ({
      ...state,
      paginationParamsIndikatorUrusan: {
        ...state.paginationParamsIndikatorUrusan,
        ...value,
      },
    }),
  );

  readonly setPaginationParamsMasterDataBidangUrusan = this.updater(
    (state, value: any) => ({
      ...state,
      paginationParamsMasterDataBidangUrusan: {
        ...state.paginationParamsMasterDataBidangUrusan,
        ...value,
      },
    }),
  );

  readonly setPaginationParamsMasterDataSKPD = this.updater(
    (state, value: any) => ({
      ...state,
      paginationParamsMasterDataSKPD: {
        ...state.paginationParamsMasterDataSKPD,
        ...value,
      },
    }),
  );

  // SELECTORS
  readonly getListTahun$ = this.select(({ listTahun }) => listTahun);
  readonly getListUrusanOPD$ = this.select(
    ({ listUrusanOPD }) => listUrusanOPD,
  );
  readonly getListUrusanWalidata$ = this.select(
    ({ listUrusanWalidata }) => listUrusanWalidata,
  );
  readonly getListIndikatorBidangUrusan$ = this.select(
    ({ listIndikatorBidangUrusan }) => listIndikatorBidangUrusan,
  );
  readonly getFilterTahunOPD$ = this.select(
    ({ filterTahunOPD }) => filterTahunOPD,
  );
  readonly getFilterTahunWalidata$ = this.select(
    ({ filterTahunWalidata }) => filterTahunWalidata,
  );
  readonly getFilterSearchUrusan$ = this.select(
    ({ filterSearchUrusan }) => filterSearchUrusan,
  );
  readonly getFilterSearchUraianUrusan$ = this.select(
    ({ filterSearchUraianUrusan }) => filterSearchUraianUrusan,
  );
  readonly getFilterSKPD$ = this.select(({ filterSKPD }) => filterSKPD);
  readonly getFilterBidangUrusan$ = this.select(
    ({ filterBidangUrusan }) => filterBidangUrusan,
  );
  readonly getSKPDMasterData$ = this.select(
    ({ skpdMasterData }) => skpdMasterData,
  );
  readonly getBidangUrusanMasterData$ = this.select(
    ({ bidangUrusanMasterData }) => bidangUrusanMasterData,
  );
  readonly getPaginationParamsBidangUrusanWalidata$ = this.select(
    ({ paginationParamsBidangUrusanWalidata }) =>
      paginationParamsBidangUrusanWalidata,
  );
  readonly getPaginationParamsBidangUrusanOPD$ = this.select(
    ({ paginationParamsBidangUrusanOPD }) => paginationParamsBidangUrusanOPD,
  );
  readonly getPaginationParamsIndikatorUrusan$ = this.select(
    ({ paginationParamsIndikatorUrusan }) => paginationParamsIndikatorUrusan,
  );
  readonly getPaginationParamsMasterDataBidangUrusan$ = this.select(
    ({ paginationParamsMasterDataBidangUrusan }) =>
      paginationParamsMasterDataBidangUrusan,
  );
  readonly getPaginationParamsMasterDataSKPD$ = this.select(
    ({ paginationParamsMasterDataSKPD }) => paginationParamsMasterDataSKPD,
  );

  // VIEWMODEL
  readonly vm$ = this.select(
    this.state$,
    this.getListTahun$,
    this.getFilterTahunOPD$,
    this.getFilterTahunWalidata$,
    this.getListUrusanOPD$,
    this.getListUrusanWalidata$,
    this.getListIndikatorBidangUrusan$,
    this.getFilterSearchUrusan$,
    this.getFilterSearchUraianUrusan$,
    this.getFilterSKPD$,
    this.getFilterBidangUrusan$,
    this.getPaginationParamsBidangUrusanWalidata$,
    this.getPaginationParamsBidangUrusanOPD$,
    this.getPaginationParamsIndikatorUrusan$,
    this.getPaginationParamsMasterDataBidangUrusan$,
    this.getPaginationParamsMasterDataSKPD$,
    this.getBidangUrusanMasterData$,
    this.getSKPDMasterData$,
    (
      state,
      getListTahun,
      getFilterTahunOPD,
      getFilterTahunWalidata,
      getListUrusanOPD,
      getListUrusanWalidata,
      getListIndikatorBidangUrusan,
      getFilterSearchUrusan,
      getFilterSearchUraianUrusan,
      getFilterSKPD,
      getFilterBidangUrusan,
      getPaginationParamsBidangUrusanWalidata,
      getPaginationParamsBidangUrusanOPD,
      getPaginationParamsIndikatorUrusan,
      getPaginationParamsMasterDataBidangUrusan,
      getPaginationParamsMasterDataSKPD,
      getBidangUrusanMasterData,
      getSKPDMasterData,
    ) => ({
      getListTahun,
      getFilterTahunOPD,
      getFilterTahunWalidata,
      getListUrusanOPD,
      getListUrusanWalidata,
      getListIndikatorBidangUrusan,
      getFilterSearchUrusan,
      getFilterSearchUraianUrusan,
      getFilterSKPD,
      getFilterBidangUrusan,
      getPaginationParamsBidangUrusanWalidata,
      getPaginationParamsBidangUrusanOPD,
      getPaginationParamsIndikatorUrusan,
      getPaginationParamsMasterDataBidangUrusan,
      getPaginationParamsMasterDataSKPD,
      getBidangUrusanMasterData,
      getSKPDMasterData,
      filterTahunOPD: state.filterTahunOPD,
      filterTahunWalidata: state.filterTahunWalidata,
      listTahun: state.listTahun,
      filterSearchUrusan: state.filterSearchUrusan,
      filterSearchUraianUrusan: state.filterSearchUraianUrusan,
      filterSKPD: state.filterSKPD,
      filterBidangUrusan: state.filterBidangUrusan,
      listUrusanOPD: state.listUrusanOPD,
      listUrusanWalidata: state.listUrusanWalidata,
      listIndikatorBidangUrusan: state.listIndikatorBidangUrusan,
      listTahunReversed: JSON.parse(
        JSON.stringify(state.listTahun ?? []),
      ).reverse(),
      paginationParamsBidangUrusanWalidata:
        state.paginationParamsBidangUrusanWalidata,
      paginationParamsBidangUrusanOPD: state.paginationParamsBidangUrusanOPD,
      paginationParamsIndikatorUrusan: state.paginationParamsIndikatorUrusan,
      paginationParamsMasterDataBidangUrusan:
        state.paginationParamsMasterDataBidangUrusan,
      paginationParamsMasterDataSKPD: state.paginationParamsMasterDataSKPD,
      skpdMasterData: state.skpdMasterData,
      bidangUrusanMasterData: state.bidangUrusanMasterData,
      pushDataMutation: this.pushDataMutation,
    }),
  );

  // ACTIONS
  readonly enumerateTahun = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      tap(() => {
        this.setListTahun(
          range(2017, this.today.year() + 1).map((item: number) => {
            return {
              value: +item,
              label: String(item),
            };
          }),
        );
      }),
    );
  });

  readonly fetchListUrusanOPD = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      withLatestFrom(this.vm$),
      switchMap(([, vm]) => {
        const {
          filterTahunOPD: tahun,
          paginationParamsBidangUrusanOPD: paginationParams,
        } = vm;

        const pWhere = {};

        const params = {
          ...this.getPaginationParams(paginationParams),
          where: JSON.stringify(pWhere),
          tahun,
        };

        return this.eWalidataService
          .fetchQueryListUrusanOPD(params)
          .result$.pipe(
            tap({
              next: (result) => {
                this.setListUrusanOPD(result);
              },
            }),
          );
      }),
    );
  });

  readonly fetchListBidangUrusanWalidata = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      withLatestFrom(this.vm$),
      switchMap(([, vm]) => {
        const {
          filterTahunWalidata: tahun,
          paginationParamsBidangUrusanWalidata: paginationParams,
          filterSKPD: skpd,
        } = vm;

        const pWhere = {};

        if (skpd) {
          assign(pWhere, { 'bidang_urusan_skpd.kode_skpd': skpd });
        }

        const params = {
          ...this.getPaginationParams(paginationParams),
          where: JSON.stringify(pWhere),
          tahun,
        };

        return this.eWalidataService
          .fetchQueryListBidangUrusanWalidata(params)
          .result$.pipe(
            tap({
              next: (result) => {
                this.setListUrusanWalidata(result);
              },
            }),
          );
      }),
    );
  });

  readonly fetchListIndikatorBidangUrusanWalidata = this.effect<void>(
    (trigger$) => {
      return trigger$.pipe(
        withLatestFrom(this.vm$),
        switchMap(([, vm]) => {
          const {
            paginationParamsIndikatorUrusan: paginationParams,
            filterBidangUrusan: bidangUrusan,
            listTahun,
          } = vm;

          const pWhere = {};

          if (bidangUrusan) {
            assign(pWhere, { kode_bidang_urusan: bidangUrusan });
          }

          const params = {
            ...this.getPaginationParams(paginationParams),
            where: JSON.stringify(pWhere),
          };

          return this.eWalidataService
            .fetchQueryListIndikatorBidangUrusanWalidata(params)
            .result$.pipe(
              mapResultData((result: any) => {
                result.list = result.list.map((item: any) => {
                  const existingYear = item.ewalidata_data.map(
                    (ewalidata_data: any) => ewalidata_data.tahun,
                  );
                  const notExistingYear = listTahun
                    .filter((tahun: any) => !existingYear.includes(tahun.label))
                    .map((tahun: any) => ({
                      tahun: tahun.label,
                      kode_indikator: item.kode_indikator,
                      kode_pemda: '3200',
                      data: null,
                      status_verifikasi_walidata: null,
                    }));
                  return {
                    ...item,
                    ewalidata_data: [
                      ...item.ewalidata_data,
                      ...notExistingYear,
                    ].map((ewalidata_data: any) => ({
                      ...ewalidata_data,
                      input$: new Subject<string>(),
                      hovered: false,
                      data: ewalidata_data.data ? +ewalidata_data.data : null,
                    })),
                  };
                });
                result.list.forEach((item: any) => {
                  item.ewalidata_data.forEach((data: any) => {
                    data.input$
                      .pipe(
                        distinctUntilChanged(),
                        debounceTime(500),
                        map((term: any) => {
                          return this.eWalidataService
                            .postEWalidataData({
                              data: String(term),
                              tahun: data.tahun,
                              kode_indikator: data.kode_indikator,
                              kode_pemda: data.kode_pemda,
                            })
                            .pipe(this.pushDataMutation.track())
                            .subscribe();
                        }),
                      )
                      .subscribe();
                  });
                });
                return result;
              }),
              tap({
                next: (result) => {
                  this.setListIndikatorBidangUrusan(result);
                },
              }),
            );
        }),
      );
    },
  );

  readonly fetchListIndikatorBidangUrusanOPD = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      withLatestFrom(this.vm$),
      switchMap(([, vm]) => {
        const {
          paginationParamsIndikatorUrusan: paginationParams,
          filterBidangUrusan: bidangUrusan,
          listTahun,
        } = vm;

        const pWhere = {};

        if (bidangUrusan) {
          assign(pWhere, { kode_bidang_urusan: bidangUrusan });
        }

        const params = {
          ...this.getPaginationParams(paginationParams),
          where: JSON.stringify(pWhere),
        };

        return this.eWalidataService
          .fetchQueryListIndikatorBidangUrusanOPD(params)
          .result$.pipe(
            mapResultData((result: any) => {
              result.list = result.list.map((item: any) => {
                const existingYear = item.ewalidata_data.map(
                  (ewalidata_data: any) => ewalidata_data.tahun,
                );
                const notExistingYear = listTahun
                  .filter((tahun: any) => !existingYear.includes(tahun.label))
                  .map((tahun: any) => ({
                    tahun: tahun.label,
                    kode_indikator: item.kode_indikator,
                    kode_pemda: '3200',
                    data: null,
                    status_verifikasi_walidata: null,
                  }));
                return {
                  ...item,
                  ewalidata_data: [
                    ...item.ewalidata_data,
                    ...notExistingYear,
                  ].map((ewalidata_data: any) => ({
                    ...ewalidata_data,
                    input$: new Subject<string>(),
                    hovered: false,
                    data: ewalidata_data.data ? +ewalidata_data.data : null,
                  })),
                };
              });
              result.list.forEach((item: any) => {
                item.ewalidata_data.forEach((data: any) => {
                  data.input$
                    .pipe(
                      distinctUntilChanged(),
                      debounceTime(500),
                      map((term: any) => {
                        return this.eWalidataService
                          .postEWalidataData({
                            data: String(term),
                            tahun: data.tahun,
                            kode_indikator: data.kode_indikator,
                            kode_pemda: data.kode_pemda,
                          })
                          .pipe(this.pushDataMutation.track())
                          .subscribe();
                      }),
                    )
                    .subscribe();
                });
              });
              return result;
            }),
            tap({
              next: (result) => {
                this.setListIndikatorBidangUrusan(result);
              },
            }),
          );
      }),
    );
  });

  readonly fetchSKPDMasterData = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      withLatestFrom(this.vm$),
      switchMap(([, vm]) => {
        const {
          paginationParamsMasterDataSKPD: paginationParams,
          skpdMasterData,
        } = vm;

        const pWhere = {};

        const params = {
          ...this.getPaginationParams(paginationParams),
          where: JSON.stringify(pWhere),
        };

        return this.organizationService.getQueryList(params).result$.pipe(
          tap({
            next: (result) => {
              if (!skpdMasterData) {
                paginationParams.searchInput$
                  .pipe(
                    distinctUntilChanged(),
                    debounceTime(500),
                    map((term: any) => {
                      this.setPaginationParamsMasterDataSKPD({
                        search: term,
                      });
                      return this.fetchSKPDMasterData();
                    }),
                  )
                  .subscribe();
              }
              this.setSKPDMasterData(result);
            },
          }),
        );
      }),
    );
  });

  readonly fetchBidangUrusanMasterDataWalidata = this.effect<void>(
    (trigger$) => {
      return trigger$.pipe(
        withLatestFrom(this.vm$),
        switchMap(([, vm]) => {
          const {
            paginationParamsMasterDataBidangUrusan: paginationParams,
            filterBidangUrusan,
            bidangUrusanMasterData,
          } = vm;

          const pWhere = {};

          const params = {
            ...this.getPaginationParams(paginationParams),
            where: JSON.stringify(pWhere),
          };

          return this.eWalidataService
            .fetchQueryMasterDataBidangUrusanWalidata(params)
            .result$.pipe(
              mapResultData((result: any) => {
                result.list = uniqWith(
                  result.list,
                  (arrVal: any, othVal: any) =>
                    arrVal.kode_bidang_urusan === othVal.kode_bidang_urusan,
                ).map((item: any) => ({
                  ...item,
                  label: `${item.kode_bidang_urusan} - ${item.nama_bidang_urusan}`,
                }));
                return result;
              }),
              tap({
                next: (result) => {
                  if (
                    result.isSuccess === true &&
                    result.isFetchedAfterMount === true &&
                    !filterBidangUrusan
                  ) {
                    this.setFilterBidangUrusan(
                      (result?.data?.list ?? [{}])[0]?.kode_bidang_urusan,
                    );
                    this.fetchListIndikatorBidangUrusanWalidata();
                  }
                  if (!bidangUrusanMasterData) {
                    paginationParams.searchInput$
                      .pipe(
                        distinctUntilChanged(),
                        debounceTime(500),
                        map((term: any) => {
                          this.setPaginationParamsMasterDataBidangUrusan({
                            search: term,
                          });
                          return this.fetchBidangUrusanMasterDataWalidata();
                        }),
                      )
                      .subscribe();
                  }
                  this.setBidangUrusanMasterData(result);
                },
              }),
            );
        }),
      );
    },
  );

  readonly fetchBidangUrusanMasterDataOPD = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      withLatestFrom(this.vm$),
      switchMap(([, vm]) => {
        const {
          paginationParamsMasterDataBidangUrusan: paginationParams,
          filterBidangUrusan,
          bidangUrusanMasterData,
        } = vm;

        const pWhere = {};

        const params = {
          ...this.getPaginationParams(paginationParams),
          where: JSON.stringify(pWhere),
        };

        return this.eWalidataService
          .fetchQueryBidangUrusanMasterDataOPD(params)
          .result$.pipe(
            mapResultData((result: any) => {
              result.list = result.list.map((item: any) => ({
                ...item,
                label: `${item.kode_bidang_urusan} - ${item.nama_bidang_urusan}`,
              }));
              return result;
            }),
            tap({
              next: (result) => {
                if (
                  result.isSuccess === true &&
                  result.isFetchedAfterMount === true &&
                  !filterBidangUrusan
                ) {
                  this.setFilterBidangUrusan(
                    (result?.data?.list ?? [{}])[0]?.kode_bidang_urusan,
                  );
                  this.fetchListIndikatorBidangUrusanOPD();
                }
                if (!bidangUrusanMasterData) {
                  paginationParams.searchInput$
                    .pipe(
                      distinctUntilChanged(),
                      debounceTime(500),
                      map((term: any) => {
                        this.setPaginationParamsMasterDataBidangUrusan({
                          search: term,
                        });
                        return this.fetchBidangUrusanMasterDataOPD();
                      }),
                    )
                    .subscribe();
                }
                this.setBidangUrusanMasterData(result);
              },
            }),
          );
      }),
    );
  });

  getPaginationParams(params: PaginationParams): any {
    return {
      limit: params.perPage,
      skip:
        params.currentPage === 1
          ? 0
          : params.perPage * (params.currentPage - 1),
      search: params.search || '',
      ...(params.sort ? { sort: params.sort } : {}),
    };
  }
}
