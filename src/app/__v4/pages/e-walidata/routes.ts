import { Routes } from '@angular/router';

export const E_WALIDATA_ROUTES: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        loadComponent: () =>
          import('./containers/e-walidata-page/e-walidata-page.component').then(
            (m) => m.EWalidataPageComponent,
          ),
      },
    ],
  },
];
