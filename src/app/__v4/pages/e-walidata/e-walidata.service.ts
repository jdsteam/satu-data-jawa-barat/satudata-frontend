/* eslint-disable @typescript-eslint/no-unused-vars */
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { UseQuery } from '@ngneat/query';
import { hasOwnProperty } from '@utils-v4';
import { PaginationService } from '@services-v3';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { catchError, map, of, tap } from 'rxjs';
import { BidangUrusan, IndikatorBidangUrusan } from '@models-v4';

@Injectable({
  providedIn: 'root',
})
export class EWalidataService {
  title = 'EWalidata';

  apiUrl = `${environment.backendURL}`;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  private http = inject(HttpClient);
  private useQuery = inject(UseQuery);
  private paginationService = inject(PaginationService);
  private toastr = inject(ToastrService);

  fetchListUrusanOPD(params: any, options: any) {
    const merge = new URLSearchParams(params);

    const queryName = `List Bidang Urusan OPD ${this.title}`;

    return this.http
      .get<any>(`${this.apiUrl}bidang_urusan_skpd?${merge}`, this.httpOptions)
      .pipe(
        map((response) => {
          if (response.error === 1 && response.message !== 'Data not found') {
            throw new Error(response.message);
          }

          return new BidangUrusan().deserialize(response);
        }),
        map((response) => {
          const hasPagination = hasOwnProperty(options, 'pagination');
          const hasInfinite = hasOwnProperty(options, 'infinite');

          const isPagination = hasPagination ? options.pagination : true;
          const isInfinite = hasInfinite ? options.infinite : false;

          let pagination = { empty: true, infinite: isInfinite };
          if (isPagination && response.meta) {
            const resultPagination = this.paginationService.getPagination(
              response?.meta?.total_record,
              params.skip,
              params.limit,
            );

            pagination = {
              empty: false,
              infinite: isInfinite,
              ...resultPagination,
            };
          }

          return {
            pagination,
            list: response.data,
          };
        }),
        catchError((e) => {
          this.toastr.error(e.message, queryName);
          return of(null);
        }),
      );
  }

  fetchQueryListUrusanOPD(params: any, options: any = {}) {
    return this.useQuery(
      ['listBidangUrusanOPD'],
      () => {
        return this.fetchListUrusanOPD(params, options);
      },
      options,
    );
  }

  fetchListBidangUrusanWalidata(params: any, options: any) {
    const merge = new URLSearchParams(params);

    const queryName = `List Bidang Urusan Walidata ${this.title}`;

    return this.http
      .get<any>(`${this.apiUrl}bidang_urusan?${merge}`, this.httpOptions)
      .pipe(
        map((response) => {
          if (response.error === 1 && response.message !== 'Data not found') {
            throw new Error(response.message);
          }

          return new BidangUrusan().deserialize(response);
        }),
        map((response) => {
          const hasPagination = hasOwnProperty(options, 'pagination');
          const hasInfinite = hasOwnProperty(options, 'infinite');

          const isPagination = hasPagination ? options.pagination : true;
          const isInfinite = hasInfinite ? options.infinite : false;

          let pagination = { empty: true, infinite: isInfinite };
          if (isPagination && response.meta) {
            const resultPagination = this.paginationService.getPagination(
              response?.meta?.total_record,
              params.skip,
              params.limit,
            );

            pagination = {
              empty: false,
              infinite: isInfinite,
              ...resultPagination,
            };
          }

          return {
            pagination,
            list: response.data,
          };
        }),
        catchError((e) => {
          this.toastr.error(e.message, queryName);
          return of(null);
        }),
      );
  }

  fetchQueryListBidangUrusanWalidata(params: any, options: any = {}) {
    return this.useQuery(
      ['listBidangUrusanWalidata'],
      () => {
        return this.fetchListBidangUrusanWalidata(params, options);
      },
      options,
    );
  }

  fetchQueryMasterDataBidangUrusanWalidata(params: any, options: any = {}) {
    return this.useQuery(
      ['masterDataBidangUrusanWalidata'],
      () => {
        return this.fetchListBidangUrusanWalidata(params, options);
      },
      options,
    );
  }

  fetchListIndikatorBidangUrusanWalidata(params: any, options: any) {
    const merge = new URLSearchParams(params);

    const queryName = `List Indikator Bidang Urusan Walidata ${this.title}`;

    return this.http
      .get<any>(
        `${this.apiUrl}bidang_urusan_indikator?${merge}`,
        this.httpOptions,
      )
      .pipe(
        map((response) => {
          if (response.error === 1 && response.message !== 'Data not found') {
            throw new Error(response.message);
          }

          return new IndikatorBidangUrusan().deserialize(response);
        }),
        map((response) => {
          const hasPagination = hasOwnProperty(options, 'pagination');
          const hasInfinite = hasOwnProperty(options, 'infinite');

          const isPagination = hasPagination ? options.pagination : true;
          const isInfinite = hasInfinite ? options.infinite : false;

          let pagination = { empty: true, infinite: isInfinite };
          if (isPagination && response.meta) {
            const resultPagination = this.paginationService.getPagination(
              response?.meta?.total_record,
              params.skip,
              params.limit,
            );

            pagination = {
              empty: false,
              infinite: isInfinite,
              ...resultPagination,
            };
          }

          return {
            pagination,
            list: response.data,
          };
        }),
        catchError((e) => {
          this.toastr.error(e.message, queryName);
          return of(null);
        }),
      );
  }

  fetchQueryListIndikatorBidangUrusanWalidata(params: any, options: any = {}) {
    return this.useQuery(
      ['listIndikatorBidangUrusanWalidata'],
      () => {
        return this.fetchListIndikatorBidangUrusanWalidata(params, options);
      },
      options,
    );
  }

  fetchListIndikatorBidangUrusanOPD(params: any, options: any) {
    const merge = new URLSearchParams(params);

    const queryName = `List Indikator Bidang Urusan OPD ${this.title}`;

    return this.http
      .get<any>(
        `${this.apiUrl}bidang_urusan_indikator?${merge}`,
        this.httpOptions,
      )
      .pipe(
        map((response) => {
          if (response.error === 1 && response.message !== 'Data not found') {
            throw new Error(response.message);
          }

          return new IndikatorBidangUrusan().deserialize(response);
        }),
        map((response) => {
          const hasPagination = hasOwnProperty(options, 'pagination');
          const hasInfinite = hasOwnProperty(options, 'infinite');

          const isPagination = hasPagination ? options.pagination : true;
          const isInfinite = hasInfinite ? options.infinite : false;

          let pagination = { empty: true, infinite: isInfinite };
          if (isPagination && response.meta) {
            const resultPagination = this.paginationService.getPagination(
              response?.meta?.total_record,
              params.skip,
              params.limit,
            );

            pagination = {
              empty: false,
              infinite: isInfinite,
              ...resultPagination,
            };
          }

          return {
            pagination,
            list: response.data,
          };
        }),
        catchError((e) => {
          this.toastr.error(e.message, queryName);
          return of(null);
        }),
      );
  }

  fetchQueryListIndikatorBidangUrusanOPD(params: any, options: any = {}) {
    return this.useQuery(
      ['listIndikatorBidangUrusanOPD'],
      () => {
        return this.fetchListIndikatorBidangUrusanOPD(params, options);
      },
      options,
    );
  }

  fetchQueryBidangUrusanMasterDataOPD(params: any, options: any = {}) {
    return this.useQuery(
      ['bidangUrusanMasterDataOPD'],
      () => {
        return this.fetchListUrusanOPD(params, options);
      },
      options,
    );
  }

  postEWalidataData(body: any) {
    const queryName = `Push Data Indikator Bidang Urusan ${this.title}`;

    return this.http
      .post<any>(
        `${this.apiUrl}bidang_urusan_indikator/data/submit`,
        body,
        this.httpOptions,
      )
      .pipe(
        map((response) => {
          if (response.error === 1 && response.message !== 'Data not found') {
            throw new Error(response.message);
          }

          return new BidangUrusan().deserialize(response);
        }),
        tap(() => {
          this.toastr.success(
            `Berhasil update data dengan Kode SSD ${body.kode_indikator} pada tahun ${body.tahun}`,
            'Indikator Bidang Urusan',
          );
        }),
        catchError((e) => {
          this.toastr.error(e.message, queryName);
          return of(null);
        }),
      );
  }
}
