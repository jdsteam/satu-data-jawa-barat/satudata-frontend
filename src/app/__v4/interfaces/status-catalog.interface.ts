export interface StatusCatalog {
  is_validate: 0 | 1 | 2 | 3 | 4 | 5 | 6;
  is_active: boolean;
  is_deleted: boolean;
}
