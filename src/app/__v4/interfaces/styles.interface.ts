// TYPE
import {
  AlignItems,
  BorderColor,
  Display,
  JustifyContent,
  List,
  ObjectFit,
  Position,
  TextAlign,
  VerticalAlign,
} from '@types-v4';

export interface Styles {
  /*
    Example:
    doSomething(data: object): string;
    doSomething(): string;
  */

  // Accordion
  accordion: string;
  accordionButton: string;
  accordionButtonComponent: string;
  accordionHeader: string;
  accordionItem: string;
  accordionItemComponent: string;
  accordionBody: string;
  accordionBodyComponent: string;

  // Alert
  alert: string;
  alertHeading: string;
  alertIcon: string;
  alertLink: string;
  alertDismissible: string;
  alertPrimary: string;
  alertSecondary: string;
  alertSuccess: string;
  alertInfo: string;
  alertWarning: string;
  alertDanger: string;
  alertLight: string;
  alertDark: string;
  alertWhite: string;
  alertBlack: string;
  alertCustom(value: string): string;

  // Badge
  badge: string;

  // Breadcrumb
  breadcrumb: string;
  breadcrumbItem: string;

  // Button
  button: string;
  buttonPrimary: string;
  buttonSecondary: string;
  buttonSuccess: string;
  buttonInfo: string;
  buttonWarning: string;
  buttonDanger: string;
  buttonLight: string;
  buttonDark: string;
  buttonWhite: string;
  buttonBlack: string;
  buttonLink: string;
  buttonDefault: string;
  buttonTag: string;
  buttonStatus: string;
  buttonRequest(value: string): string;
  buttonCustom(value: string): string;
  buttonOutlinePrimary: string;
  buttonOutlineSecondary: string;
  buttonOutlineSuccess: string;
  buttonOutlineInfo: string;
  buttonOutlineWarning: string;
  buttonOutlineDanger: string;
  buttonOutlineLight: string;
  buttonOutlineDark: string;
  buttonOutlineWhite: string;
  buttonOutlineBlack: string;
  buttonOutlineCustom(value: string): string;
  buttonLg: string;
  buttonSm: string;
  buttonClose: string;
  buttonCloseWhite: string;

  // Card
  card: string;
  cardHeader: string;
  cardTitle: string;
  cardBody: string;
  cardFooter: string;

  // CKEditor
  ckeditor: string;

  // Container
  container: string;
  containerFluid: string;
  containerXxl: string;
  containerXl: string;
  containerLg: string;
  containerMd: string;
  containerSm: string;

  // Form
  // Form Check
  formCheck: string;
  formSwitch: string;
  formCheckInput: string;
  formCheckLabel: string;
  formCheckInline: string;

  // Form Label
  formLabel: string;

  // Form Text
  formText: string;

  // Form Input
  inputSearch: string;
  inputSearchSm: string;
  inputSearchMd: string;

  // Grid
  row: string;
  rowCols(i: number | string): string;
  rowColsSm(i: number | string): string;
  rowColsMd(i: number | string): string;
  rowColsLg(i: number | string): string;
  rowColsXl(i: number | string): string;
  rowColsXxl(i: number | string): string;
  col(i: number | string): string;
  colSm(i: number | string): string;
  colMd(i: number | string): string;
  colLg(i: number | string): string;
  colXl(i: number | string): string;
  colXxl(i: number | string): string;
  offset(i: number | string): string;
  offsetSm(i: number | string): string;
  offsetMd(i: number | string): string;
  offsetLg(i: number | string): string;
  offsetXl(i: number | string): string;
  offsetXxl(i: number | string): string;

  // Nav
  nav: string;
  navLink: string;
  navPills: string;
  navFlush: string;
  tabContent: string;

  // Navbar
  navbarLink: string;
  dropdownMenu: string;
  dropdownNavbarItem: string;
  dropdownNavbarLink: string;

  // Progress
  progress: string;
  progressBar: string;
  progressBarStriped: string;
  progressBarAnimated: string;

  // Circular Progress
  circularProgress(value: number, size: number, color: string): string;

  // Progress
  table: string;
  tableSm: string;
  tableBordered: string;
  tableBorderless: string;
  tableStriped: string;
  tableResponsive: string;
  tableSortable: string;
  tablePaging: string;

  // Timeline
  timeline: string;

  // Transitions
  fade: string;
  collapse: string;
  collapsing: string;

  // Utilities
  // Align Items
  alignItems(value: AlignItems): string;
  alignItemsSm(value: AlignItems): string;
  alignItemsMd(value: AlignItems): string;
  alignItemsLg(value: AlignItems): string;
  alignItemsXl(value: AlignItems): string;
  alignItemsXxl(value: AlignItems): string;

  // Background Color
  bg(value: string): string;

  // Border
  border(value: number): string;
  borderTop(value: number): string;
  borderEnd(value: number): string;
  borderBottom(value: number): string;
  borderStart(value: number): string;

  // Border Color
  borderColor(value: BorderColor | string): string;

  // Color
  text(value: string): string;

  // Display
  d(value: Display): string;
  dSm(value: Display): string;
  dMd(value: Display): string;
  dLg(value: Display): string;
  dXl(value: Display): string;
  dXxl(value: Display): string;

  // Font Size
  fs(value: number | string): string;

  // Font Weight
  fw(value: string): string;

  // Gap
  gap(value: number): string;
  gapSm(value: number): string;
  gapMd(value: number): string;
  gapLg(value: number): string;
  gapXl(value: number): string;
  gapXxl(value: number): string;
  gapCol(value: number): string;
  gapColSm(value: number): string;
  gapColMd(value: number): string;
  gapColLg(value: number): string;
  gapColXl(value: number): string;
  gapColXxl(value: number): string;
  gapRow(value: number): string;
  gapRowSm(value: number): string;
  gapRowMd(value: number): string;
  gapRowLg(value: number): string;
  gapRowXl(value: number): string;
  gapRowXxl(value: number): string;

  // Height
  h(value: number | string): string;
  minH(value: number | string): string;
  maxH(value: number | string): string;

  // Justify Content
  justifyContent(value: JustifyContent): string;
  justifyContentSm(value: JustifyContent): string;
  justifyContentMd(value: JustifyContent): string;
  justifyContentLg(value: JustifyContent): string;
  justifyContentXl(value: JustifyContent): string;
  justifyContentXxl(value: JustifyContent): string;

  // Line Height
  leading(value: number): string;

  // List
  list(value: List): string;

  // Margin
  m(value: number | string): string;
  mSm(value: number | string): string;
  mMd(value: number | string): string;
  mLg(value: number | string): string;
  mXl(value: number | string): string;
  mXxl(value: number | string): string;
  mx(value: number | string): string;
  mxSm(value: number | string): string;
  mxMd(value: number | string): string;
  mxLg(value: number | string): string;
  mxXl(value: number | string): string;
  mxXxl(value: number | string): string;
  my(value: number | string): string;
  mySm(value: number | string): string;
  myMd(value: number | string): string;
  myLg(value: number | string): string;
  myXl(value: number | string): string;
  myXxl(value: number | string): string;
  mt(value: number | string): string;
  mtSm(value: number | string): string;
  mtMd(value: number | string): string;
  mtLg(value: number | string): string;
  mtXl(value: number | string): string;
  mtXxl(value: number | string): string;
  me(value: number | string): string;
  meSm(value: number | string): string;
  meMd(value: number | string): string;
  meLg(value: number | string): string;
  meXl(value: number | string): string;
  meXxl(value: number | string): string;
  mb(value: number | string): string;
  mbSm(value: number | string): string;
  mbMd(value: number | string): string;
  mbLg(value: number | string): string;
  mbXl(value: number | string): string;
  mbXxl(value: number | string): string;
  ms(value: number | string): string;
  msSm(value: number | string): string;
  msMd(value: number | string): string;
  msLg(value: number | string): string;
  msXl(value: number | string): string;
  msXxl(value: number | string): string;

  // Object Fit
  object(value: ObjectFit): string;

  // Offcanvas
  offcanvasBackdrop: string;
  offcanvas: string;
  offcanvasBottom: string;

  // Padding
  p(value: number | string): string;
  pSm(value: number | string): string;
  pMd(value: number | string): string;
  pLg(value: number | string): string;
  pXl(value: number | string): string;
  pXxl(value: number | string): string;
  px(value: number | string): string;
  pxSm(value: number | string): string;
  pxMd(value: number | string): string;
  pxLg(value: number | string): string;
  pxXl(value: number | string): string;
  pxXxl(value: number | string): string;
  py(value: number | string): string;
  pySm(value: number | string): string;
  pyMd(value: number | string): string;
  pyLg(value: number | string): string;
  pyXl(value: number | string): string;
  pyXxl(value: number | string): string;
  pt(value: number | string): string;
  ptSm(value: number | string): string;
  ptMd(value: number | string): string;
  ptLg(value: number | string): string;
  ptXl(value: number | string): string;
  ptXxl(value: number | string): string;
  pe(value: number | string): string;
  peSm(value: number | string): string;
  peMd(value: number | string): string;
  peLg(value: number | string): string;
  peXl(value: number | string): string;
  peXxl(value: number | string): string;
  pb(value: number | string): string;
  pbSm(value: number | string): string;
  pbMd(value: number | string): string;
  pbLg(value: number | string): string;
  pbXl(value: number | string): string;
  pbXxl(value: number | string): string;
  ps(value: number | string): string;
  psSm(value: number | string): string;
  psMd(value: number | string): string;
  psLg(value: number | string): string;
  psXl(value: number | string): string;
  psXxl(value: number | string): string;

  // Position
  pos(value: Position): string;

  // Rounded
  rounded(value: number | string): string;
  roundedTop(value: number): string;
  roundedEnd(value: number): string;
  roundedBottom(value: number): string;
  roundedStart(value: number): string;

  // Text Align
  textAlign(value: TextAlign): string;
  textAlignSm(value: TextAlign): string;
  textAlignMd(value: TextAlign): string;
  textAlignLg(value: TextAlign): string;
  textAlignXl(value: TextAlign): string;
  textAlignXxl(value: TextAlign): string;

  // Text Decoration
  textDecoration(value: string): string;

  // Top / Right / Bottom / Left
  inset(value: number): string;
  insetX(value: number): string;
  insetY(value: number): string;
  start(value: number): string;
  end(value: number): string;
  top(value: number): string;
  right(value: number): string;
  bottom(value: number): string;
  left(value: number): string;

  // Width
  align(value: VerticalAlign): string;

  // Width
  w(value: number | string): string;
  minW(value: number | string): string;
  maxW(value: number | string): string;
}
