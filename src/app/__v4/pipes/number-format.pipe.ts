import { DecimalPipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'numberFormat',
  standalone: true,
})
export class NumberFormatPipe implements PipeTransform {
  constructor(private decimalPipe: DecimalPipe) {}
  transform(value: any) {
    const converted = this.decimalPipe.transform(value, '1.0-3', 'id');
    return converted ? converted.toString() : '0';
  }
}
