import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'removeHtmlTag', standalone: true })
export class RemoveHtmlTagPipe implements PipeTransform {
  transform(html: string): string {
    if (html === null || html === '') {
      return null;
    }

    let result = html;
    result = result.toString();
    result = result.replace(/<p>/g, '').replace(/<\/p>/g, ' ');
    result = result.substr(0, result.length - 1);
    result = result.replace(/&nbsp;/g, ' ');
    return result.replace(/(<([^>]+)>)/gi, '');
  }
}
