import { Pipe, PipeTransform } from '@angular/core';
import { Observable, of } from 'rxjs';
import { reduce, switchMap, take } from 'rxjs/operators';

import { LogDatasetData } from '@models-v3';
import moment from 'moment';

@Pipe({ name: 'groupByDate', pure: false, standalone: true })
export class GroupByDatePipe implements PipeTransform {
  transform(
    collection: Observable<Array<any>>,
    property = 'date_start_modify',
  ) {
    if (!collection) {
      return null;
    }

    return collection.pipe(
      take(1),
      switchMap((data) => {
        const modify = [];
        data.forEach((res) => {
          modify.push({
            ...res,
            date_start_modify: moment(res.date_start).format('YYYY-MM-DD'),
          });
        });
        return modify as LogDatasetData[];
      }),
      reduce((previous, current) => {
        if (!previous[current[property]]) {
          // eslint-disable-next-line no-param-reassign
          previous[current[property]] = [];
        }

        previous[current[property]].push(current);
        return previous;
      }, new Array<any>()),
      switchMap((data) => {
        return of(
          Object.keys(data).map((date) => ({ date, data: data[date] })),
        );
      }),
    );
  }
}
