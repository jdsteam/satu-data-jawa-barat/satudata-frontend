import { Pipe, PipeTransform } from '@angular/core';
import { isValidHttpUrl } from '@utils-v4';
import { environment } from 'src/environments/environment';

@Pipe({ name: 'mapsetMetadataXML', standalone: true })
export class MapsetMetadataXMLPipe implements PipeTransform {
  transform(value: string): string {
    if (!value) return null;

    if (isValidHttpUrl(value)) {
      return value;
    }

    return environment.backendURL + value;
  }
}
