import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Output,
  computed,
  input,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// JDS-BI
import { JdsBiScrollbarModule } from '@jds-bi/core';

import { keyBy, merge, orderBy, values } from 'lodash';

@Component({
  selector: 'app-filter-topic',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CommonModule, FormsModule, JdsBiScrollbarModule],
  templateUrl: './filter-topic.component.html',
})
export class FilterTopicComponent {
  colorScheme = COLORSCHEME;
  jds: Styles = jds;

  data = input<any[]>([]);
  filter = input<string[]>([]);

  @Output() filterChange = new EventEmitter<any>();

  checklist = computed(() => {
    const items = this.data();
    let result: any[];

    items.forEach((item) => (item.checked = false));

    if (this.filter().length) {
      const checked = this.filter().map((code) => {
        return { id: code, checked: true };
      });

      const merged = merge(keyBy(items, 'id'), keyBy(checked, 'id'));
      const vals = values(merged);
      result = orderBy(vals, ['name'], ['asc']);
    } else {
      result = orderBy(items, ['name'], ['asc']);
    }

    return result;
  });

  onFilter(): void {
    const items = this.checklist();

    const emit = items.filter((item) => item.checked).map((item) => item.id);
    this.filterChange.emit(emit);
  }
}
