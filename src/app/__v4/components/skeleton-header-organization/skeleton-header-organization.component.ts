import { ChangeDetectionStrategy, Component } from '@angular/core';
import { CommonModule } from '@angular/common';

// STYLE
import * as jds from '@styles';
// INTERFACE
import { Styles } from '@interfaces-v4';

import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

@Component({
  selector: 'app-skeleton-header-organization',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CommonModule, NgxSkeletonLoaderModule],
  template: `
    <div class="skeleton-header-organization">
      <div [ngClass]="jds.container">
        <div [ngClass]="[jds.py(40)]">
          @for (number of [80, 40]; track $index) {
            <div class="wrapper-skeleton">
              <ngx-skeleton-loader
                [theme]="{
                  width: number + '%',
                  'border-radius': '0',
                  height: '22px',
                  'margin-bottom': '0'
                }"
              />
            </div>
          }
        </div>
      </div>
    </div>
  `,
  styles: [
    `
      .skeleton-header-organization {
        background: url(/assets/images/backgrounds/bg_header.png);
        background-position: center;
        background-size: cover;
      }
    `,
  ],
})
export class SkeletonHeaderOrganizationComponent {
  jds: Styles = jds;
}
