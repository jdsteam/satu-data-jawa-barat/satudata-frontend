import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Output,
  input,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// STYLE
import * as jds from '@styles';
// INTERFACE
import { Styles } from '@interfaces-v4';
// TYPE
import type { StatusDataset } from '@types-v4';

export type DropdownDatasetActionDelete = {
  id: number;
  category: StatusDataset;
};

export type DropdownDatasetActionArchive = {
  id: number;
};

export type DropdownDatasetActionActive = {
  id: number;
};

export type DropdownDatasetActionRecover = {
  id: number;
  lastCategory: string;
};

export type DropdownDatasetActionDiscontinue = {
  id: number;
  category: StatusDataset;
};

@Component({
  selector: 'app-dropdown-dataset-action',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CommonModule, RouterModule],
  templateUrl: './dropdown-dataset-action.component.html',
})
export class DropdownDatasetActionComponent {
  jds: Styles = jds;

  datasetID = input.required<number>();
  datasetLastCategory = input<string>();
  width = input<number>(150);
  status = input<string>();

  @Output() delete = new EventEmitter<DropdownDatasetActionDelete>();
  @Output() archive = new EventEmitter<DropdownDatasetActionArchive>();
  @Output() active = new EventEmitter<DropdownDatasetActionActive>();
  @Output() recover = new EventEmitter<DropdownDatasetActionRecover>();
  @Output() discontinue = new EventEmitter<DropdownDatasetActionDiscontinue>();

  onDelete(data: DropdownDatasetActionDelete) {
    this.delete.emit(data);
  }

  onArchive(data: DropdownDatasetActionArchive) {
    this.archive.emit(data);
  }

  onActive(data: DropdownDatasetActionActive) {
    this.active.emit(data);
  }

  onRecover(data: DropdownDatasetActionRecover) {
    this.recover.emit(data);
  }

  onDiscontinue(data: DropdownDatasetActionDiscontinue) {
    this.discontinue.emit(data);
  }
}
