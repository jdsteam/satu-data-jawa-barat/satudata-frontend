import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { CommonModule, NgOptimizedImage } from '@angular/common';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { VERSION, WHATSAPP, WHATSAPP_INT } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// JDS-BI
import { jdsBiColor } from '@jds-bi/cdk';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconLocationDot,
  iconWhatsapp,
  iconEnvelope,
} from '@jds-bi/icons';

import moment from 'moment';

@Component({
  selector: 'app-footer',
  standalone: true,
  imports: [CommonModule, NgOptimizedImage, JdsBiIconsModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './footer.component.html',
})
export class FooterComponent {
  jds: Styles = jds;
  jdsBiColor = jdsBiColor;

  // Service
  private iconService = inject(JdsBiIconsService);

  // Variable
  version = VERSION;
  whatsapp = WHATSAPP;

  // Variable
  message = `https://wa.me/${WHATSAPP_INT}?text=Hi!%20saya%20ingin%20bertanya%20mengenai%20Satu%20Data%20Jabar`;
  year: string = moment().locale('id').format('YYYY');

  constructor() {
    this.iconService.registerIcons([
      iconLocationDot,
      iconWhatsapp,
      iconEnvelope,
    ]);
  }
}
