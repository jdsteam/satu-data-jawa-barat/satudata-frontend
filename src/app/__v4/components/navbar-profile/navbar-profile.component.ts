import {
  Component,
  Inject,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  inject,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { WINDOW } from '@ng-web-apis/common';

// STYLE
import * as jds from '@styles';
// INTERFACE
import { Styles } from '@interfaces-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';
// UTIL
import { isValidHttpUrl } from '@utils-v4';
// JDS-BI
import { jdsBiColor } from '@jds-bi/cdk';
import {
  JDS_BI_MEDIA,
  JdsBiMedia,
  JdsBiButtonModule,
  JdsBiDropdownModule,
  JdsBiHostedDropdownModule,
} from '@jds-bi/core';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconChevronDown,
} from '@jds-bi/icons';

@Component({
  selector: 'app-navbar-profile',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    JdsBiButtonModule,
    JdsBiDropdownModule,
    JdsBiHostedDropdownModule,
    JdsBiIconsModule,
  ],
  templateUrl: './navbar-profile.component.html',
  styleUrls: ['./navbar-profile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavbarProfileComponent {
  satudataUser: LoginData;
  jds: Styles = jds;
  jdsBiColor = jdsBiColor;

  // Service
  private router = inject(Router);
  private authenticationService = inject(AuthenticationService);
  private iconService = inject(JdsBiIconsService);

  @Output() closeMobile = new EventEmitter<boolean>();

  // Variable
  open = false;
  navbarName: string;

  constructor(
    @Inject(WINDOW) private readonly windowRef: Window,
    @Inject(JDS_BI_MEDIA) private readonly media: JdsBiMedia,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    this.iconService.registerIcons([iconChevronDown]);

    this.navbarName = this.truncateName(this.satudataUser.name, 35);
  }

  get dropdownLimitWidth(): string {
    return this.windowRef.innerWidth <= this.media.mobile ? 'fixed' : 'custom';
  }

  get profileImage(): string {
    if (isValidHttpUrl(this.satudataUser.profile_pic)) {
      return this.satudataUser.profile_pic;
    }

    return environment.backendURL + this.satudataUser.profile_pic;
  }

  truncateName(text: string, maxLength: number): string {
    return text.length > maxLength
      ? `${text.slice(0, maxLength - 1)}...`
      : text;
  }

  closeDropdown(): void {
    this.open = false;
    this.closeMobile.emit(this.open);
  }

  navigateTo(url: string): void {
    this.closeDropdown();
    this.router.navigate([url]);
  }

  logout(): void {
    this.closeDropdown();
    this.authenticationService.logout();
    this.router.navigate(['/auth/login']);
  }
}
