import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Output,
  input,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { SOURCE_MAPSET } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// MODEL
import { LoginData } from '@models-v3';
import { MapsetData } from '@models-v4';
// PIPE
import { RemoveHtmlTagPipe } from '@pipes-v4';
// SERVICE
import { AuthenticationService } from '@services-v3';
// JDS-BI
import { jdsBiColor } from '@jds-bi/cdk';
import {
  JdsBiBadgeModule,
  JdsBiDropdownModule,
  JdsBiHostedDropdownModule,
  JdsBiButtonModule,
} from '@jds-bi/core';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconCalendar,
  iconCircleCheck,
  iconFileTimer,
  iconBook,
  iconMapLocation,
  iconClockThree,
  iconBuilding,
  iconCircleDown,
  iconEye,
  iconEllipsis,
} from '@jds-bi/icons';

import moment from 'moment';
import { NgxPopperjsModule } from 'ngx-popperjs';
import { v4 as uuidv4 } from 'uuid';
import * as L from 'leaflet';
import * as esri from 'esri-leaflet';
import 'esri-leaflet-renderers';

@Component({
  selector: 'app-list-mapset',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    CommonModule,
    RouterModule,
    JdsBiBadgeModule,
    JdsBiDropdownModule,
    JdsBiHostedDropdownModule,
    JdsBiButtonModule,
    JdsBiIconsModule,
    NgxPopperjsModule,
    RemoveHtmlTagPipe,
  ],
  templateUrl: './list-mapset.component.html',
  styleUrls: ['./list-mapset.component.scss'],
})
export class ListMapsetComponent implements AfterViewInit {
  satudataUser: LoginData;
  jds: Styles = jds;
  jdsBiColor = jdsBiColor;
  moment: any = moment;

  isAction = input<boolean>();
  status = input<string>('active');
  data = input<MapsetData>();

  @Output() delete = new EventEmitter<number>();
  @Output() archive = new EventEmitter<number>();
  @Output() active = new EventEmitter<number>();

  // Variable
  activePath: string;
  id = uuidv4();
  public isDropdown = false;

  constructor(
    private authenticationService: AuthenticationService,
    private iconService: JdsBiIconsService,
  ) {
    // eslint-disable-next-line prefer-destructuring
    this.activePath = window.location.pathname.split('/')[1];

    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    this.iconService.registerIcons([
      iconCalendar,
      iconCircleCheck,
      iconFileTimer,
      iconBook,
      iconMapLocation,
      iconClockThree,
      iconBuilding,
      iconCircleDown,
      iconEye,
      iconEllipsis,
    ]);
  }

  ngAfterViewInit(): void {
    this.createMap();
  }

  createMap(): void {
    const map = L.map(`viewMapList-${this.id}`, {
      minZoom: 6,
      scrollWheelZoom: false,
      zoomControl: false,
      fullscreenControl: false,
      dragging: false,
    });

    map.setView([-6.932694, 107.627449], 6);

    const basemapEnum = 'Topographic';

    esri.basemapLayer(basemapEnum).addTo(map);

    const url = this.data().app_link;

    switch (this.data().mapset_source.id) {
      case SOURCE_MAPSET.get('arcgis').id:
        this.createMapArcGIS(map, url);
        break;
      case SOURCE_MAPSET.get('geoserver').id:
        this.createMapGeoServer(map, url);
        break;
      default:
        break;
    }
  }

  createMapArcGIS(map: any, url: string) {
    const splitUrl = url.split('/');
    const mapserviceId = splitUrl[splitUrl.length - 1].replace(/\D/g, '');
    splitUrl.pop();
    const mapserverUrl = splitUrl.join('/');

    esri
      .dynamicMapLayer({
        url: mapserverUrl,
        layers: [mapserviceId],
      })
      .addTo(map);
  }

  createMapGeoServer(map: any, url: string) {
    const urlWMS = url.split('?')[0];
    const queryString = url.split('?')[1];
    const params = new URLSearchParams(queryString);
    const layers = params.get('layers');

    if (layers) {
      const wmsOptions = {
        layers,
        transparent: true,
        format: 'image/png',
      };

      L.tileLayer.wms(urlWMS, wmsOptions).addTo(map);
    }
  }

  getStatusData(value: boolean) {
    if (value) {
      return {
        icon: 'circle-check',
        name: 'Tetap',
        color: 'green',
      };
    }

    return {
      icon: 'file-timer',
      name: 'Sementara',
      color: 'yellow',
    };
  }

  onDelete(id: number) {
    this.delete.emit(id);
  }

  onArchive(id: number) {
    this.archive.emit(id);
  }

  onActive(id: number) {
    this.active.emit(id);
  }
}
