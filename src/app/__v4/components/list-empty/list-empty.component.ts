import {
  ChangeDetectionStrategy,
  Component,
  computed,
  inject,
  input,
} from '@angular/core';
import { CommonModule } from '@angular/common';

// STYLE
import * as jds from '@styles';
// INTERFACE
import { Styles } from '@interfaces-v4';
// JDS-BI
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconDatabase,
  iconBuilding,
} from '@jds-bi/icons';

@Component({
  selector: 'app-list-empty',
  standalone: true,
  imports: [CommonModule, JdsBiIconsModule],
  templateUrl: './list-empty.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListEmptyComponent {
  jds: Styles = jds;

  type = input<string>();
  isCard = input<boolean>(true);

  // Service
  private iconService = inject(JdsBiIconsService);

  icon = computed(() => {
    switch (this.type()) {
      case 'dataset':
      case 'indicator':
      case 'visualization':
        return 'database';
      default:
        return 'building';
    }
  });

  constructor() {
    this.iconService.registerIcons([iconDatabase, iconBuilding]);
  }
}
