import { Injectable } from '@angular/core';

// JDS-BI
import { jdsBiColor } from '@jds-bi/cdk';
// PACKAGE
import { css } from '@emotion/css';

@Injectable()
export class FloatingToTopStyle {
  getStyle(): any {
    const button = css`
      position: fixed;
      display: flex;
      align-items: center;
      justify-content: center;
      width: 60px;
      height: 60px;
      bottom: 40px;
      right: 20px;
      color: #fff;
      border-radius: 50px;
      text-align: center;
      box-shadow: 2px 2px 3px rgb(0 0 0 / 30%);
      background-color: ${jdsBiColor.blue['600']};
      z-index: 15;
      cursor: pointer;
      border: none;
    `;

    return {
      button,
    };
  }
}
