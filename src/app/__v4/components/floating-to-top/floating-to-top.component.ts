import {
  ChangeDetectionStrategy,
  Component,
  HostListener,
  computed,
  inject,
  signal,
} from '@angular/core';
import { CommonModule } from '@angular/common';

// JDS-BI
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconArrowUp,
} from '@jds-bi/icons';

import { FloatingToTopStyle } from './floating-to-top.style';

@Component({
  selector: 'app-floating-to-top',
  standalone: true,
  imports: [CommonModule, JdsBiIconsModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './floating-to-top.component.html',
  providers: [FloatingToTopStyle],
})
export class FloatingToTopComponent {
  // Service
  private floatingToTopStyle = inject(FloatingToTopStyle);
  private iconService = inject(JdsBiIconsService);

  // Variable
  className = computed(() => this.floatingToTopStyle.getStyle());
  isShow = signal<boolean>(false);
  topPosToStartShowing = signal<number>(100);

  constructor() {
    this.iconService.registerIcons([iconArrowUp]);
  }

  @HostListener('window:scroll')
  checkScroll() {
    const position =
      window.scrollY ||
      document.documentElement.scrollTop ||
      document.body.scrollTop ||
      0;

    if (position >= this.topPosToStartShowing()) {
      this.isShow.set(true);
    } else {
      this.isShow.set(false);
    }
  }

  scrollToTop() {
    window.scroll({ top: 0, left: 0, behavior: 'smooth' });
  }
}
