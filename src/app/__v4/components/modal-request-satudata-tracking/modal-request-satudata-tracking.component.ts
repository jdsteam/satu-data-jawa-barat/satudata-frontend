import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
  inject,
} from '@angular/core';
import { CommonModule } from '@angular/common';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME, STATUS_REQUEST_SATUDATA } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';
// JDS-BI
import { jdsBiDefaultProp } from '@jds-bi/cdk';
import {
  JdsBiButtonModule,
  JdsBiModalModule,
  JdsBiModalService,
} from '@jds-bi/core';
import { JdsBiIconsModule, JdsBiIconsService, iconXmark } from '@jds-bi/icons';

import { find, includes } from 'lodash';
import moment from 'moment';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

@Component({
  selector: 'app-modal-request-satudata-tracking',
  standalone: true,
  imports: [
    CommonModule,
    JdsBiButtonModule,
    JdsBiIconsModule,
    JdsBiModalModule,
    NgxSkeletonLoaderModule,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './modal-request-satudata-tracking.component.html',
})
export class ModalRequestSatuDataTrackingComponent implements OnInit {
  satudataUser: LoginData;
  colorScheme = COLORSCHEME;
  jds: Styles = jds;
  moment = moment;

  @Input()
  @jdsBiDefaultProp()
  feature = 'desk';

  @Input()
  @jdsBiDefaultProp()
  notificationData = null;

  @Input()
  @jdsBiDefaultProp()
  historyData: any;

  // Service
  private jdsBiModalService = inject(JdsBiModalService);
  private authenticationService = inject(AuthenticationService);

  // Variable
  modalId: string;
  statusRequest = Array.from(STATUS_REQUEST_SATUDATA, ([, value]) => ({
    id: value.id,
    name: value.name,
    color: value.color,
  }));

  constructor(private JdsBiIconService: JdsBiIconsService) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    this.JdsBiIconService.registerIcons([iconXmark]);
  }

  ngOnInit(): void {
    this.modalId = `${this.feature}-modal-request-satudata-tracking`;
  }

  openModal() {
    this.jdsBiModalService.open(this.modalId);
  }

  closeModal() {
    this.jdsBiModalService.close(this.modalId);
  }

  getStatusName(value: number) {
    return find(this.statusRequest, { id: value }).name;
  }

  getNotificationContent(title: string, content: string) {
    let request: string;
    if (includes(content, 'Nomor ticket')) {
      const splitted = content.split('Nomor ticket');
      request = `${title} ${splitted[0]}<br>Nomor Tiket${splitted[1]}`;
    } else if (includes(content, 'Nomor tiket')) {
      const splitted = content.split('Nomor tiket');
      request = `${title} ${splitted[0]}<br>Nomor Tiket${splitted[1]}`;
    } else {
      request = `${title} ${content}`;
    }

    return request;
  }
}
