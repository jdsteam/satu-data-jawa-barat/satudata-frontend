import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Output,
  QueryList,
  ViewChild,
  ViewChildren,
  computed,
  inject,
  input,
  signal,
} from '@angular/core';
import { CommonModule } from '@angular/common';

// STYLE
import * as jds from '@styles';
// INTERFACE
import { Styles } from '@interfaces-v4';
// JDS-BI
import { JdsBiDropdownModule, JdsBiHostedDropdownModule } from '@jds-bi/core';

import { includes } from 'lodash';

@Component({
  selector: 'app-filter-dataset-tags-inline',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CommonModule, JdsBiDropdownModule, JdsBiHostedDropdownModule],
  templateUrl: './filter-dataset-tags-inline.component.html',
})
export class FilterDatasetTagsInlineComponent implements AfterViewInit {
  jds: Styles = jds;

  @ViewChild('tags', { static: true }) tagsElement: ElementRef;
  @ViewChildren('text') textElements: QueryList<ElementRef>;

  // Service
  private cdRef = inject(ChangeDetectorRef);

  data = input<any[]>();
  filter = input<string[]>([]);

  @Output() filterChange = new EventEmitter<any>();

  // Variable
  public isDropdown = false;
  helper = signal(true);
  helperData = computed(() => this.data());
  helperShow = signal(null);
  helperHide = signal(null);

  ngAfterViewInit(): void {
    const divWidth = this.tagsElement.nativeElement.clientWidth;

    let shouldSkip = false;
    let spanIndex = null;
    let spanAllWidth = 0;
    this.textElements.forEach((span: ElementRef, index: number) => {
      const spanWidth = span.nativeElement.clientWidth;
      spanAllWidth += spanWidth;

      if (shouldSkip) {
        return;
      }
      if (spanAllWidth > divWidth) {
        shouldSkip = true;
        return;
      }

      spanIndex = index;
    });

    if (this.textElements.length === spanIndex + 1) {
      this.helperShow.set(this.data());
    } else {
      this.helperShow.set(this.data().slice(0, spanIndex));
      this.helperHide.set(this.data().slice(spanIndex));
    }

    this.helper.set(false);

    this.cdRef.detectChanges();
  }

  onFilter(code: string) {
    if (includes(this.filter(), code)) {
      return;
    }

    const emit = [...this.filter(), code];

    this.filterChange.emit(emit);
  }
}
