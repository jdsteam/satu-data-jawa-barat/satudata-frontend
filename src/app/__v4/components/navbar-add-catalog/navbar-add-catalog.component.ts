import {
  Component,
  Inject,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  inject,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { WINDOW } from '@ng-web-apis/common';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';
// JDS-BI
import {
  JDS_BI_MEDIA,
  JdsBiMedia,
  JdsBiButtonModule,
  JdsBiDropdownModule,
  JdsBiHostedDropdownModule,
} from '@jds-bi/core';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconPlus,
  iconChevronDown,
} from '@jds-bi/icons';

@Component({
  selector: 'app-navbar-add-catalog',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    JdsBiButtonModule,
    JdsBiDropdownModule,
    JdsBiHostedDropdownModule,
    JdsBiIconsModule,
  ],
  templateUrl: './navbar-add-catalog.component.html',
  styleUrls: ['./navbar-add-catalog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavbarAddCatalogComponent {
  colorScheme = COLORSCHEME;
  satudataUser: LoginData;
  jds: Styles = jds;

  // Service
  private authenticationService = inject(AuthenticationService);
  private iconService = inject(JdsBiIconsService);

  @Output() closeMobile = new EventEmitter<boolean>();

  // Variable
  open = false;

  constructor(
    @Inject(WINDOW) private readonly windowRef: Window,
    @Inject(JDS_BI_MEDIA) private readonly media: JdsBiMedia,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    this.iconService.registerIcons([iconChevronDown, iconPlus]);
  }

  get dropdownLimitWidth(): string {
    return this.windowRef.innerWidth <= this.media.mobile ? 'fixed' : 'min';
  }

  onClick(): void {
    this.open = false;
    this.closeMobile.emit(this.open);
  }
}
