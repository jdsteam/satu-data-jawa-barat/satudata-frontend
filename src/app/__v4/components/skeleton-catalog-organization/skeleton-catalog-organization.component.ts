import { ChangeDetectionStrategy, Component } from '@angular/core';
import { CommonModule } from '@angular/common';

// STYLE
import * as jds from '@styles';
// INTERFACE
import { Styles } from '@interfaces-v4';

import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

@Component({
  selector: 'app-skeleton-catalog-organization',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CommonModule, NgxSkeletonLoaderModule],
  template: `
    <div
      [ngClass]="[jds.row]"
      style="--jds-gutter-x: 20px"
    >
      @for (row of [1, 2, 3, 4, 5, 6]; track $index) {
        <div [ngClass]="[jds.col(2)]">
          <div [ngClass]="[jds.card, jds.mb(20)]">
            <div [ngClass]="['frame-panel', jds.cardBody]">
              <div class="frame">
                <ngx-skeleton-loader
                  [theme]="{
                    width: '90%',
                    height: '120px',
                    'border-radius': '0'
                  }"
                  appearance="circle"
                />
              </div>
              <div class="frame-footer">
                @for (number of [70, 50]; track $index) {
                  <div class="wrapper-skeleton">
                    <ngx-skeleton-loader
                      [theme]="{
                        width: number + '%',
                        'border-radius': '0',
                        height: '20px',
                        'margin-bottom': '5px'
                      }"
                    />
                  </div>
                }
              </div>
            </div>
          </div>
        </div>
      }
    </div>
  `,
  styles: [
    `
      .frame-panel .frame {
        width: 100%;
        height: 130px;
        white-space: nowrap;
        text-align: center;
        position: relative;
        vertical-align: top;
        display: inline-block;
        margin-bottom: 20px;
      }

      .frame-panel .frame-footer {
        height: 76px;
      }
    `,
  ],
})
export class SkeletonCatalogOrganizationComponent {
  jds: Styles = jds;
}
