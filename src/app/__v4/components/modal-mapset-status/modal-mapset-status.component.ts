import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
  inject,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  FormsModule,
  ReactiveFormsModule,
  UntypedFormGroup,
  UntypedFormControl,
  Validators,
} from '@angular/forms';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// JDS-BI
import { jdsBiDefaultProp } from '@jds-bi/cdk';
import {
  JdsBiButtonModule,
  JdsBiInputModule,
  JdsBiModalModule,
  JdsBiModalService,
} from '@jds-bi/core';
import { JdsBiIconsModule, JdsBiIconsService, iconXmark } from '@jds-bi/icons';

import { LetDirective } from '@ngrx/component';

@Component({
  selector: 'app-modal-mapset-status',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    JdsBiButtonModule,
    JdsBiInputModule,
    JdsBiIconsModule,
    JdsBiModalModule,
    JdsBiIconsModule,
    LetDirective,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './modal-mapset-status.component.html',
})
export class ModalMapsetStatusComponent {
  colorScheme = COLORSCHEME;
  jds: Styles = jds;

  @Input()
  @jdsBiDefaultProp()
  from = '';

  @Input()
  @jdsBiDefaultProp()
  status: string;

  @Output() update: EventEmitter<any> = new EventEmitter();

  // Service
  private jdsBiModalService = inject(JdsBiModalService);

  // Variable
  mapsetHistoryFormGroup = new UntypedFormGroup({
    type: new UntypedFormControl('mapset'),
    type_id: new UntypedFormControl(),
    category: new UntypedFormControl(),
    notes: new UntypedFormControl(null, [Validators.required]),
  });

  constructor(private JdsBiIconService: JdsBiIconsService) {
    this.JdsBiIconService.registerIcons([iconXmark]);
  }

  get id() {
    const from = this.from === '' ? '' : `-${this.from}`;
    return `modal-mapset-status${from}`;
  }

  get f() {
    return this.mapsetHistoryFormGroup.controls;
  }

  openModal(id: number) {
    let category;
    switch (this.status) {
      case 'active':
        category = 'approve-archive';
        break;
      case 'archive':
        category = 'approve-active';
        break;
      case 'revision-new':
        category = 'revision-active-new';
        break;
      case 'revision-edit':
        category = 'revision-active-edit';
        break;
      default:
        break;
    }

    this.mapsetHistoryFormGroup.patchValue({
      type: 'mapset',
      type_id: id,
      category,
    });

    this.jdsBiModalService.open(this.id);
  }

  getTitle(value: string) {
    let category;
    switch (value) {
      case 'active':
        category = 'Arsipkan';
        break;
      case 'archive':
        category = 'Aktifkan';
        break;
      case 'revision':
        category = 'Tolak';
        break;
      default:
        break;
    }

    return category;
  }

  closeModal() {
    this.mapsetHistoryFormGroup.reset();

    this.jdsBiModalService.close(this.id);
  }

  onSubmit() {
    this.update.emit(this.mapsetHistoryFormGroup.value);

    this.closeModal();
  }
}
