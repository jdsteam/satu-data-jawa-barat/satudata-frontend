import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Output,
  inject,
  input,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// STYLE
import * as jds from '@styles';
// INTERFACE
import { Styles } from '@interfaces-v4';
// MODEL
import { LoginData } from '@models-v3';
import { DatasetMeilisearchData } from '@models-v4';
// PIPE
import { RemoveHtmlTagPipe } from '@pipes-v4';
// SERVICE
import { AuthenticationService } from '@services-v3';
// JDS-BI
import { jdsBiColor } from '@jds-bi/cdk';
import {
  JdsBiBadgeModule,
  JdsBiDropdownModule,
  JdsBiHostedDropdownModule,
  JdsBiButtonModule,
} from '@jds-bi/core';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconBook,
  iconBuilding,
  iconCalendar,
  iconCircleCheck,
  iconCircleDown,
  iconClockRotateLeft,
  iconClockThree,
  iconEye,
  iconFileTimer,
  iconEllipsis,
} from '@jds-bi/icons';

import moment from 'moment';
import { NgxPopperjsModule } from 'ngx-popperjs';

import type {
  DropdownDatasetActionDelete,
  DropdownDatasetActionArchive,
  DropdownDatasetActionActive,
  DropdownDatasetActionRecover,
  DropdownDatasetActionDiscontinue,
} from '../dropdown-dataset-action/dropdown-dataset-action.component';
import { DropdownDatasetActionComponent } from '../dropdown-dataset-action/dropdown-dataset-action.component';
import { FilterDatasetTagsInlineComponent } from '../filter-dataset-tags-inline/filter-dataset-tags-inline.component';

@Component({
  selector: 'app-list-dataset',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    CommonModule,
    RouterModule,
    JdsBiBadgeModule,
    JdsBiDropdownModule,
    JdsBiHostedDropdownModule,
    JdsBiButtonModule,
    JdsBiIconsModule,
    NgxPopperjsModule,
    DropdownDatasetActionComponent,
    FilterDatasetTagsInlineComponent,
    RemoveHtmlTagPipe,
  ],
  templateUrl: './list-dataset.component.html',
})
export class ListDatasetComponent {
  satudataUser: LoginData;
  jds: Styles = jds;
  jdsBiColor = jdsBiColor;
  moment: any = moment;

  isAction = input<boolean>();
  status = input<string>('active');
  data = input<DatasetMeilisearchData>();
  filterTag = input<string[]>([]);

  @Output() delete = new EventEmitter<DropdownDatasetActionDelete>();
  @Output() archive = new EventEmitter<DropdownDatasetActionArchive>();
  @Output() active = new EventEmitter<DropdownDatasetActionActive>();
  @Output() recover = new EventEmitter<DropdownDatasetActionRecover>();
  @Output() discontinue = new EventEmitter<DropdownDatasetActionDiscontinue>();
  @Output() filterTagChange = new EventEmitter<any>();

  // Service
  private authenticationService = inject(AuthenticationService);
  private iconService = inject(JdsBiIconsService);

  // Variable
  public isDropdown = false;

  constructor() {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    this.iconService.registerIcons([
      iconBook,
      iconBuilding,
      iconCalendar,
      iconCircleCheck,
      iconCircleDown,
      iconClockRotateLeft,
      iconClockThree,
      iconEye,
      iconFileTimer,
      iconEllipsis,
    ]);
  }

  onDelete(data: DropdownDatasetActionDelete) {
    this.delete.emit(data);
  }

  onArchive(data: DropdownDatasetActionArchive) {
    this.archive.emit(data);
  }

  onActive(data: DropdownDatasetActionActive) {
    this.active.emit(data);
  }

  onRecover(data: DropdownDatasetActionRecover) {
    this.recover.emit(data);
  }

  onDiscontinue(data: DropdownDatasetActionDiscontinue) {
    this.discontinue.emit(data);
  }

  onFilterTags(value: string[]) {
    this.filterTagChange.emit(value);
  }
}
