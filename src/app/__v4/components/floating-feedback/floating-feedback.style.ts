import { Injectable } from '@angular/core';

// PACKAGE
import { css } from '@emotion/css';

@Injectable()
export class FloatingFeedbackStyle {
  getStyle(): any {
    const box = css`
      float: left;
      position: fixed;
      top: calc(50% - 47px);
      right: 0;
      z-index: 99;

      button {
        display: block;
        background-color: #3596f3;
        border: 0;
        border-radius: 5px 0 0 5px;
        padding: 0;
        box-shadow: 0 0 3px rgb(0 0 0 / 30%);
        transition: all 0.2s ease-in-out;
        cursor: pointer;

        &:hover {
          padding-right: 20px;
        }
      }
    `;

    return {
      box,
    };
  }
}
