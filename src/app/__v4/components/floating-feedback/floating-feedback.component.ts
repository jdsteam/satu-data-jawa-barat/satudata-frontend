import {
  ChangeDetectionStrategy,
  Component,
  computed,
  inject,
  viewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';

// COMPONENT
import { ModalFeedbackComponent } from '../modal-feedback';

import { FloatingFeedbackStyle } from './floating-feedback.style';

@Component({
  selector: 'app-floating-feedback',
  standalone: true,
  imports: [CommonModule, ModalFeedbackComponent],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './floating-feedback.component.html',
  providers: [FloatingFeedbackStyle],
})
export class FloatingFeedbackComponent {
  modalFeedback = viewChild(ModalFeedbackComponent);

  // Service
  private floatingFeedbackStyle = inject(FloatingFeedbackStyle);

  // Variable
  className = computed(() => this.floatingFeedbackStyle.getStyle());

  openFeedback() {
    this.modalFeedback().openModal();
  }
}
