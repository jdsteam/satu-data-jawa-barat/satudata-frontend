import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Output,
  computed,
  inject,
  input,
  signal,
  viewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { fromEvent } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  tap,
} from 'rxjs/operators';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// JDS-BI
import { JdsBiInputModule, JdsBiScrollbarModule } from '@jds-bi/core';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconMagnifyingGlass,
} from '@jds-bi/icons';

import { keyBy, merge, orderBy, values } from 'lodash';

@Component({
  selector: 'app-filter-organization',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    CommonModule,
    FormsModule,
    JdsBiInputModule,
    JdsBiIconsModule,
    JdsBiScrollbarModule,
  ],
  templateUrl: './filter-organization.component.html',
})
export class FilterOrganizationComponent implements AfterViewInit {
  colorScheme = COLORSCHEME;
  jds: Styles = jds;

  // Service
  iconService = inject(JdsBiIconsService);

  search = viewChild<ElementRef>('search');
  data = input<any[]>([]);
  filter = input<string[]>([]);
  searchValue = signal<string>('');

  @Output() filterChange = new EventEmitter<any>();

  constructor() {
    this.iconService.registerIcons([iconMagnifyingGlass]);
  }

  ngAfterViewInit(): void {
    fromEvent(this.search().nativeElement, 'keyup')
      .pipe(
        filter(Boolean),
        debounceTime(300),
        distinctUntilChanged(),
        tap(() => {
          this.onSearch(this.search().nativeElement.value);
        }),
      )
      .subscribe();
  }

  checklist = computed(() => {
    const items = this.data();
    let result: any[];

    items.forEach((item) => (item.checked = false));

    if (this.filter().length) {
      const checked = this.filter().map((code) => {
        return { kode_skpd: code, checked: true };
      });

      const merged = merge(
        keyBy(items, 'kode_skpd'),
        keyBy(checked, 'kode_skpd'),
      );
      const vals = values(merged);
      result = orderBy(vals, ['nama_skpd'], ['asc']);
    } else {
      result = orderBy(items, ['nama_skpd'], ['asc']);
    }

    if (this.searchValue() !== '') {
      return items.filter((item) => {
        return item.nama_skpd.toLowerCase().indexOf(this.searchValue()) > -1;
      });
    }

    return result;
  });

  onSearch(value: string): void {
    this.searchValue.set(value);
  }

  onFilter(): void {
    const items = this.checklist();

    const emit = items
      .filter((item) => item.checked)
      .map((item) => item.kode_skpd);
    this.filterChange.emit(emit);
  }
}
