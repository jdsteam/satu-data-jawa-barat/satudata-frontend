import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
  inject,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  FormsModule,
  ReactiveFormsModule,
  UntypedFormControl,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// JDS-BI
import { jdsBiDefaultProp } from '@jds-bi/cdk';
import {
  JdsBiButtonModule,
  JdsBiInputModule,
  JdsBiModalModule,
  JdsBiModalService,
} from '@jds-bi/core';
import { JdsBiIconsModule, JdsBiIconsService, iconXmark } from '@jds-bi/icons';

import { LetDirective } from '@ngrx/component';

@Component({
  selector: 'app-modal-dataset-status',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    JdsBiButtonModule,
    JdsBiInputModule,
    JdsBiIconsModule,
    JdsBiModalModule,
    JdsBiIconsModule,
    LetDirective,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './modal-dataset-status.component.html',
})
export class ModalDatasetStatusComponent {
  colorScheme = COLORSCHEME;
  jds: Styles = jds;

  @Input()
  @jdsBiDefaultProp()
  status: string;

  @Output() update: EventEmitter<any> = new EventEmitter();

  // Service
  private jdsBiIconService = inject(JdsBiIconsService);
  private jdsBiModalService = inject(JdsBiModalService);

  // Form
  datasetHistoryFormGroup = new UntypedFormGroup({
    type: new UntypedFormControl('dataset'),
    type_id: new UntypedFormControl(),
    category: new UntypedFormControl(),
    notes: new UntypedFormControl(null, [Validators.required]),
  });

  constructor() {
    this.jdsBiIconService.registerIcons([iconXmark]);
  }

  get f() {
    return this.datasetHistoryFormGroup.controls;
  }

  openModal(id: number) {
    let category;
    switch (this.status) {
      case 'active':
        category = 'approve-archive';
        break;
      case 'archive':
        category = 'approve-active';
        break;
      default:
        break;
    }

    this.datasetHistoryFormGroup.patchValue({
      type: 'dataset',
      type_id: id,
      category,
    });

    this.jdsBiModalService.open('modal-dataset-status');
  }

  closeModal() {
    this.datasetHistoryFormGroup.reset();

    this.jdsBiModalService.close('modal-dataset-status');
  }

  getTitle(value: string) {
    let category;
    switch (value) {
      case 'active':
        category = 'Arsipkan';
        break;
      case 'archive':
        category = 'Aktifkan';
        break;
      default:
        break;
    }

    return category;
  }

  onSubmit() {
    this.update.emit(this.datasetHistoryFormGroup.value);

    this.closeModal();
  }
}
