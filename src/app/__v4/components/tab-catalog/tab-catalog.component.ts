import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Output,
  input,
} from '@angular/core';
import { CommonModule } from '@angular/common';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// TYPE
import { Catalog } from '@types-v4';
// JDS-BI
import { JdsBiNavModule } from '@jds-bi/core';

@Component({
  selector: 'app-tab-catalog',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CommonModule, JdsBiNavModule],
  templateUrl: './tab-catalog.component.html',
  styleUrls: ['./tab-catalog.component.scss'],
})
export class TabCatalogComponent {
  colorScheme = COLORSCHEME;
  jds: Styles = jds;

  catalog = input<Catalog>();

  @Output() catalogChange = new EventEmitter<any>();

  onCatalog(value: Catalog): void {
    this.catalogChange.emit(value);
  }
}
