import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Output,
  computed,
  input,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// JDS-BI
import { JdsBiButtonModule, JdsBiScrollbarModule } from '@jds-bi/core';

import { keyBy, merge, orderBy, values } from 'lodash';

@Component({
  selector: 'app-filter-dataset-tags',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CommonModule, FormsModule, JdsBiScrollbarModule, JdsBiButtonModule],
  templateUrl: './filter-dataset-tags.component.html',
})
export class FilterDatasetTagsComponent {
  colorScheme = COLORSCHEME;
  jds: Styles = jds;

  data = input<any[]>([]);
  filter = input<string[]>([]);

  @Output() filterChange = new EventEmitter<any>();

  checklist = computed(() => {
    const items = this.data();
    let result: any[];

    items.forEach((item) => (item.checked = false));

    if (this.filter().length) {
      const checked = this.filter().map((code) => {
        return { tag: code, checked: true };
      });

      const merged = merge(keyBy(items, 'tag'), keyBy(checked, 'tag'));
      const vals = values(merged);
      result = orderBy(vals, ['tag'], ['asc']);
    } else {
      result = orderBy(items, ['tag'], ['asc']);
    }

    return result;
  });

  onFilter(code: string, isChecked: boolean): void {
    const items = this.checklist();

    items.forEach((item) => {
      if (item.tag === code) {
        item.checked = isChecked;
      }
      return item;
    });

    const emit = items.filter((item) => item.checked).map((item) => item.tag);
    this.filterChange.emit(emit);
  }
}
