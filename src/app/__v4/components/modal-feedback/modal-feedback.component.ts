import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  inject,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  FormsModule,
  ReactiveFormsModule,
  UntypedFormGroup,
  UntypedFormControl,
  Validators,
} from '@angular/forms';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// SERVICE
import { FeedbackService } from '@services-v4';
// JDS-BI
import {
  JdsBiButtonModule,
  JdsBiModalModule,
  JdsBiModalService,
  JdsBiInputModule,
} from '@jds-bi/core';
import { JdsBiIconsModule, JdsBiIconsService, iconXmark } from '@jds-bi/icons';

import { NgSelectModule } from '@ng-select/ng-select';
import { SubscribeDirective } from '@ngneat/subscribe';
import { NgxLoadingModule } from 'ngx-loading';
import { useMutationResult } from '@ngneat/query';
import Swal from 'sweetalert2';

import { ModalFeedbackStyle } from './modal-feedback.style';

@Component({
  selector: 'app-modal-feedback',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    JdsBiButtonModule,
    JdsBiIconsModule,
    JdsBiModalModule,
    JdsBiInputModule,
    NgSelectModule,
    SubscribeDirective,
    NgxLoadingModule,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './modal-feedback.component.html',
  styleUrls: ['./modal-feedback.component.scss'],
  providers: [ModalFeedbackStyle],
})
export class ModalFeedbackComponent implements OnInit {
  colorScheme = COLORSCHEME;
  jds: Styles = jds;

  // Service
  private jdsBiModalService = inject(JdsBiModalService);
  private modalFeedbackStyle = inject(ModalFeedbackStyle);
  private feedbackService = inject(FeedbackService);
  createFeedback = useMutationResult();

  // Variable
  className!: any;
  modalId: string;
  isScore = false;
  isPurposeOther = false;
  isPurposeEasyFind = true;
  isProblemOther = false;
  activeScore: number;

  myForm = new UntypedFormGroup({
    score: new UntypedFormControl({ value: null, disabled: false }, [
      Validators.required,
    ]),
    tujuan: new UntypedFormControl({ value: null, disabled: false }, [
      Validators.required,
    ]),
    tujuan_other: new UntypedFormControl({ value: null, disabled: false }),
    tujuan_tercapai: new UntypedFormControl({ value: null, disabled: false }, [
      Validators.required,
    ]),
    tujuan_mudah_ditemukan: new UntypedFormControl(
      { value: null, disabled: false },
      [Validators.required],
    ),
    masalah: new UntypedFormControl({ value: null, disabled: false }),
    masalah_other: new UntypedFormControl({ value: null, disabled: false }),
    saran: new UntypedFormControl({ value: null, disabled: false }, [
      Validators.maxLength(255),
    ]),
  });

  // Data
  scoreList = [
    { value: 0, image: 'Sangat-Tidak-Puas.svg', name: 'Sangat tidak puas' },
    { value: 1, image: 'Tidak-Puas.svg', name: 'Tidak puas' },
    { value: 2, image: 'Biasa-Saja.svg', name: 'Biasa saja' },
    { value: 3, image: 'Puas.svg', name: 'Puas' },
    { value: 4, image: 'Sangat-Puas.svg', name: 'Sangat puas' },
  ];

  feedbackInfoData$: any;

  constructor(private JdsBiIconService: JdsBiIconsService) {
    this.JdsBiIconService.registerIcons([iconXmark]);

    this.modalId = `modal-feedback`;
  }

  ngOnInit(): void {
    this.getClassnames();
    this.getFeedbackInfo();
  }

  get f() {
    return this.myForm.controls;
  }

  getClassnames(): void {
    this.className = this.modalFeedbackStyle.getStyle();
  }

  getFeedbackInfo() {
    const params = {};
    this.feedbackInfoData$ = this.feedbackService.getQueryInfo(params).result$;
  }

  openModal() {
    this.jdsBiModalService.open(this.modalId);
  }

  closeModal() {
    this.jdsBiModalService.close(this.modalId);
  }

  // ONCHANGE ==================================================================
  onSelectedScore(value: number) {
    this.f.score.setValue(value);
    this.activeScore = value;
    this.isScore = true;
  }

  onSelectedPurpose(value: string) {
    this.f.tujuan_other.reset();

    if (value === 'Lainnya (tulis secara spesifik).') {
      this.isPurposeOther = true;
      this.f.tujuan_other.setValidators([
        Validators.required,
        Validators.maxLength(255),
      ]);
    } else {
      this.isPurposeOther = false;
      this.f.tujuan_other.clearValidators();
    }

    this.f.tujuan_other.updateValueAndValidity();
  }

  onSelectedPurposeAchieved(value: string) {
    this.f.masalah.reset();
    this.f.masalah_other.reset();

    const isTrueSet = value === 'true';
    if (isTrueSet) {
      this.isPurposeEasyFind = true;
      this.f.tujuan_mudah_ditemukan.setValidators([Validators.required]);
      this.f.masalah.clearValidators();
      this.f.masalah_other.clearValidators();
    } else {
      this.isPurposeEasyFind = false;
      this.f.tujuan_mudah_ditemukan.clearValidators();
      this.f.tujuan_mudah_ditemukan.reset();
      this.f.masalah.setValidators([Validators.required]);
    }

    this.f.tujuan_mudah_ditemukan.updateValueAndValidity();
    this.f.masalah.updateValueAndValidity();
    this.f.masalah_other.updateValueAndValidity();
  }

  onSelectedProblem(value: string) {
    this.f.masalah_other.reset();

    if (value === 'Lainnya (tulis secara spesifik)') {
      this.isProblemOther = true;
      this.f.masalah_other.setValidators([
        Validators.required,
        Validators.maxLength(255),
      ]);
    } else {
      this.isProblemOther = false;
      this.f.masalah_other.clearValidators();
    }

    this.f.masalah_other.updateValueAndValidity();
  }

  onSubmit() {
    this.closeModal();

    const input = this.myForm.value;

    let easyToFind: boolean;
    switch (input.tujuan_tercapai) {
      case 'true':
        easyToFind = true;
        break;
      case 'false':
        easyToFind = false;
        break;
      default:
        easyToFind = null;
        break;
    }

    const bodyFeedback = {
      score: input.score,
      tujuan:
        input.tujuan !== 'Lainnya (tulis secara spesifik).'
          ? input.tujuan
          : input.tujuan_other,
      tujuan_tercapai: input.tujuan_tercapai !== 'false',
      tujuan_mudah_ditemukan: easyToFind,
      masalah:
        input.masalah !== 'Lainnya (tulis secara spesifik)'
          ? input.masalah
          : input.masalah_other,
      saran: input.saran,
    };

    this.feedbackService
      .createItem(bodyFeedback)
      .pipe(this.createFeedback.track())
      .subscribe(() => {
        Swal.fire({
          type: 'success',
          text: 'Terima kasih atas Penilaian dan Feedback yang telah Anda berikan.',
          allowOutsideClick: false,
        }).then(() => {
          this.myForm.reset();
          this.activeScore = null;
          this.isScore = false;
          this.isPurposeOther = false;
          this.isPurposeEasyFind = true;
          this.isProblemOther = false;
        });
      });
  }
}
