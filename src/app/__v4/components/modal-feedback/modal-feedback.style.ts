import { Injectable } from '@angular/core';

// PACKAGE
import { css } from '@emotion/css';

@Injectable()
export class ModalFeedbackStyle {
  getStyle(): any {
    const score = css`
      display: flex;
      margin-top: 10px;

      a {
        cursor: pointer;
        letter-spacing: 0;

        span {
          visibility: hidden;
        }

        &:hover {
          text-decoration: none;
          color: unset;

          span {
            visibility: visible;
          }
        }
      }

      &.active {
        a.active {
          span {
            visibility: visible;
          }
        }

        a:not(.active) {
          img {
            opacity: 0.5;
          }

          span {
            visibility: hidden;
          }
        }
      }

      &:hover {
        a,
        a.active {
          img {
            opacity: 0.5;
          }

          span {
            visibility: hidden;
          }
        }

        a:hover {
          img {
            opacity: 1;
          }

          span {
            visibility: visible;
          }
        }
      }
    `;

    return {
      score,
    };
  }
}
