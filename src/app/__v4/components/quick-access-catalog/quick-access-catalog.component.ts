import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Output,
  computed,
  input,
} from '@angular/core';
import { CommonModule } from '@angular/common';

// STYLE
import * as jds from '@styles';
// INTERFACE
import { Styles } from '@interfaces-v4';
// JDS-BI
import { JdsBiButtonModule } from '@jds-bi/core';
import { JdsBiIconsModule, JdsBiIconsService, iconXmark } from '@jds-bi/icons';

import { filter, includes, remove } from 'lodash';

@Component({
  selector: 'app-quick-access-catalog',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CommonModule, JdsBiButtonModule, JdsBiIconsModule],
  templateUrl: './quick-access-catalog.component.html',
})
export class QuickAccessCatalogComponent {
  jds: Styles = jds;

  type = input<string>();
  data = input<any[]>();
  filter = input<string[]>([]);

  @Output() filterChange = new EventEmitter<any>();

  constructor(private iconService: JdsBiIconsService) {
    this.iconService.registerIcons([iconXmark]);
  }

  button = computed(() => {
    const items = this.data();

    let result: any;
    if (this.filter().length) {
      result = filter(items, (item) => {
        switch (this.type()) {
          case 'topic':
          case 'mapset-type':
          case 'classification':
          case 'license':
          case 'category-indicator':
            return includes(this.filter(), String(item[this.typeObj().code]));
          default:
            return includes(this.filter(), item[this.typeObj().code]);
        }
      });
    } else {
      result = [];
    }

    return result;
  });

  typeObj = computed(() => {
    const item = this.type();

    let result;
    switch (item) {
      case 'organization':
        result = {
          code: 'kode_skpd',
          name: 'nama_skpd',
        };
        break;
      case 'topic':
      case 'mapset-type':
      case 'classification':
      case 'license':
      case 'category-indicator':
        result = {
          code: 'id',
          name: 'name',
        };
        break;
      case 'classification-indicator':
        result = {
          code: 'name',
          name: 'name',
        };
        break;
      case 'quality':
        result = {
          code: 'slug',
          name: 'name',
        };
        break;
      case 'dataset-tags':
        result = {
          code: 'tag',
          name: 'tag',
        };
        break;
      default:
        result = null;
        break;
    }

    return result;
  });

  onClear(code: string) {
    const emit = remove(this.filter(), (item) => {
      switch (this.type()) {
        case 'topic':
        case 'mapset-type':
        case 'classification':
        case 'license':
        case 'category-indicator':
          return item !== String(code);
        default:
          return item !== code;
      }
    });
    this.filterChange.emit(emit);
  }
}
