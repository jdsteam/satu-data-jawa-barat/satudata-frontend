import {
  ChangeDetectionStrategy,
  Component,
  inject,
  input,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';
// INTERFACE
import { Styles } from '@interfaces-v4';
// MODEL
import { OrganizationData } from '@models-v4';
// JDS-BI
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconDatabase,
  iconBoxArchive,
  iconChartSimple,
} from '@jds-bi/icons';

import { NgxPopperjsModule } from 'ngx-popperjs';

@Component({
  selector: 'app-list-organization',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CommonModule, RouterModule, JdsBiIconsModule, NgxPopperjsModule],
  templateUrl: './list-organization.component.html',
  styleUrls: ['./list-organization.component.scss'],
})
export class ListOrganizationComponent {
  env = environment;
  jds: Styles = jds;

  data = input<OrganizationData>();

  // Service
  private iconService = inject(JdsBiIconsService);

  constructor() {
    this.iconService.registerIcons([
      iconDatabase,
      iconBoxArchive,
      iconChartSimple,
    ]);
  }
}
