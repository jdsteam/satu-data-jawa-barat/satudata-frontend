import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { CommonModule, NgOptimizedImage } from '@angular/common';
import { RouterModule } from '@angular/router';

// STYLE
import * as jds from '@styles';
// INTERFACE
import { Styles } from '@interfaces-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';

import { NavbarNotificationComponent } from '../navbar-notification';
import { NavbarAddCatalogComponent } from '../navbar-add-catalog';
import { NavbarProfileComponent } from '../navbar-profile';

@Component({
  selector: 'app-navbar-default',
  standalone: true,
  imports: [
    CommonModule,
    NgOptimizedImage,
    RouterModule,
    NavbarNotificationComponent,
    NavbarAddCatalogComponent,
    NavbarProfileComponent,
  ],
  templateUrl: './navbar-default.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavbarDefaultComponent {
  satudataUser: LoginData;
  jds: Styles = jds;

  // Service
  private authenticationService = inject(AuthenticationService);

  // Variable
  openMobile = false;

  links = [
    { label: 'Beranda', url: '/home' },
    { label: 'Dataset', url: '/dataset' },
    { label: 'Mapset', url: '/mapset' },
    { label: 'Indikator', url: '/indicator' },
    { label: 'Visualisasi', url: '/visualization' },
    { label: 'Organisasi', url: '/organization' },
    // { label: 'E-Walidata', url: '/e-walidata-2' },
  ];

  constructor() {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
  }
}
