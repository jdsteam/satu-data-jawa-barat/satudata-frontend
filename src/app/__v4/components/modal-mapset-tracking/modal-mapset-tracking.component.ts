import {
  ChangeDetectionStrategy,
  Component,
  Input,
  inject,
} from '@angular/core';
import { CommonModule } from '@angular/common';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';
// JDS-BI
import { jdsBiDefaultProp } from '@jds-bi/cdk';
import {
  JdsBiButtonModule,
  JdsBiModalModule,
  JdsBiModalService,
} from '@jds-bi/core';
import { JdsBiIconsModule, JdsBiIconsService, iconXmark } from '@jds-bi/icons';

import moment from 'moment';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

@Component({
  selector: 'app-modal-mapset-tracking',
  standalone: true,
  imports: [
    CommonModule,
    JdsBiButtonModule,
    JdsBiIconsModule,
    JdsBiModalModule,
    NgxSkeletonLoaderModule,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './modal-mapset-tracking.component.html',
})
export class ModalMapsetTrackingComponent {
  satudataUser: LoginData;
  colorScheme = COLORSCHEME;
  jds: Styles = jds;
  moment = moment;

  @Input()
  @jdsBiDefaultProp()
  historyData: any;

  // Service
  private jdsBiModalService = inject(JdsBiModalService);
  private authenticationService = inject(AuthenticationService);

  // Variable
  modalId: string;

  constructor(private JdsBiIconService: JdsBiIconsService) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    this.JdsBiIconService.registerIcons([iconXmark]);

    this.modalId = `modal-mapset-tracking`;
  }

  openModal() {
    this.jdsBiModalService.open(this.modalId);
  }

  closeModal() {
    this.jdsBiModalService.close(this.modalId);
  }

  getStatusName(value: string) {
    let verification: string;
    let revision: string;
    if (this.satudataUser.role_name === 'walidata') {
      verification = 'Membutuhkan';
      revision = 'Menunggu';
    } else {
      verification = 'Menunggu';
      revision = 'Membutuhkan';
    }

    let category: string;
    switch (value) {
      case 'draft-active':
        category = 'Draf';
        break;
      case 'new-active':
        category = `${verification} Verifikasi`;
        break;
      case 'revision-active':
      case 'revision-active-new':
      case 'revision-active-edit':
        category = `${revision} Revisi`;
        break;
      case 'approve-active':
        category = 'Disetujui';
        break;
      case 'edit-active':
        category = `${verification} Verifikasi (Perubahan)`;
        break;
      case 'approve-archive':
        category = 'Diarsipkan';
        break;
      default:
        break;
    }

    return category;
  }
}
