import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Output,
  computed,
  input,
  model,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// STYLE
import * as jds from '@styles';
// INTERFACE
import { Styles } from '@interfaces-v4';
// TYPE
import { Catalog } from '@types-v4';

import { NgSelectModule } from '@ng-select/ng-select';

@Component({
  selector: 'app-sort-catalog',
  standalone: true,
  imports: [CommonModule, FormsModule, NgSelectModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './sort-catalog.component.html',
  styleUrls: ['./sort-catalog.component.scss'],
})
export class SortCatalogComponent {
  jds: Styles = jds;

  catalog = input<Catalog>();
  sort = model();

  @Output() sortChange = new EventEmitter<any>();

  // Variable
  sortOption: any[] = [
    { value: 'newest', label: 'Terbaru' },
    { value: 'alphabet', label: 'Abjad' },
    { value: 'see', label: 'Dilihat Terbanyak' },
    { value: 'access', label: 'Diunduh Terbanyak' },
  ];
  sortVisualizationOption: any[] = [
    { value: 'newest', label: 'Terbaru' },
    { value: 'alphabet', label: 'Abjad' },
    { value: 'see', label: 'Dilihat Terbanyak' },
  ];
  sortOrganizationOption: any[] = [
    { value: 'alphabet', label: 'Abjad' },
    { value: 'dataset', label: 'Dataset Terbanyak' },
    { value: 'indicator', label: 'Indikator Terbanyak' },
    { value: 'visualization', label: 'Visualisasi Terbanyak' },
  ];

  options = computed(() => {
    switch (this.catalog()) {
      case 'visualization':
        return this.sortVisualizationOption;
      case 'organization':
        return this.sortOrganizationOption;
      default:
        return this.sortOption;
    }
  });

  onSort(): void {
    this.sortChange.emit(this.sort());
  }
}
