import { ChangeDetectionStrategy, Component } from '@angular/core';
import { CommonModule } from '@angular/common';

// STYLE
import * as jds from '@styles';
// INTERFACE
import { Styles } from '@interfaces-v4';

import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

@Component({
  selector: 'app-skeleton-catalog',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CommonModule, NgxSkeletonLoaderModule],
  template: `
    <div [ngClass]="[jds.card, jds.mb(20)]">
      <div [ngClass]="[jds.cardHeader, jds.py(15)]">
        @for (number of [80, 40]; track $index) {
          <div class="wrapper-skeleton">
            <ngx-skeleton-loader
              [theme]="{
                width: number + '%',
                'border-radius': '0',
                height: '22px',
                'margin-bottom': '0'
              }"
            />
          </div>
        }
      </div>
      <div [ngClass]="[jds.cardBody, jds.py(15), jds.px(24)]">
        @for (number of [50, 70, 30]; track $index) {
          <div class="wrapper-skeleton">
            <ngx-skeleton-loader
              [theme]="{
                width: number + '%',
                'border-radius': '0',
                height: '20px',
                'margin-bottom': '5px'
              }"
            />
          </div>
        }
      </div>
    </div>
  `,
})
export class SkeletonCatalogComponent {
  jds: Styles = jds;
}
