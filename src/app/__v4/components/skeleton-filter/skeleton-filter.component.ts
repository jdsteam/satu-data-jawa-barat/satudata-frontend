import { ChangeDetectionStrategy, Component } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

@Component({
  selector: 'app-skeleton-filter',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CommonModule, NgxSkeletonLoaderModule],
  template: `
    @for (number of [90, 20, 50, 90, 50]; track $index) {
      <div class="wrapper-skeleton">
        <ngx-skeleton-loader
          [theme]="{
            width: number + '%',
            'border-radius': '0',
            height: '20px',
            'margin-bottom': '5px'
          }"
        />
      </div>
    }
  `,
})
export class SkeletonFilterComponent {}
