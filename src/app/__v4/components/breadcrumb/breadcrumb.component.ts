import {
  ChangeDetectionStrategy,
  Component,
  inject,
  input,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// STYLE
import * as jds from '@styles';
// INTERFACE
import { Styles, Breadcrumb } from '@interfaces-v4';
// JDS-BI
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconChevronRight,
} from '@jds-bi/icons';

@Component({
  selector: 'app-breadcrumb',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CommonModule, RouterModule, JdsBiIconsModule],
  templateUrl: './breadcrumb.component.html',
})
export class BreadcrumbComponent {
  jds: Styles = jds;

  breadcrumb = input<Breadcrumb[]>();

  // Service
  private iconService = inject(JdsBiIconsService);

  constructor() {
    this.iconService.registerIcons([iconChevronRight]);
  }
}
