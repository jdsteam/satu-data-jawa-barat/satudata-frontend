import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Output,
  inject,
  input,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { CLASSIFICATION_INDICATOR } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// MODEL
import { LoginData } from '@models-v3';
import { IndicatorData } from '@models-v4';
// SERVICE
import { AuthenticationService } from '@services-v3';
// JDS-BI
import { jdsBiColor } from '@jds-bi/cdk';
import {
  JdsBiBadgeModule,
  JdsBiDropdownModule,
  JdsBiHostedDropdownModule,
  JdsBiButtonModule,
} from '@jds-bi/core';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconBuilding,
  iconBriefcase,
  iconCalendar,
  iconCabinetFiling,
  iconCircleDown,
  iconEye,
  iconEllipsis,
} from '@jds-bi/icons';

import moment from 'moment';
import { NgxPopperjsModule } from 'ngx-popperjs';

@Component({
  selector: 'app-list-indicator',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    CommonModule,
    RouterModule,
    JdsBiBadgeModule,
    JdsBiDropdownModule,
    JdsBiHostedDropdownModule,
    JdsBiButtonModule,
    JdsBiIconsModule,
    NgxPopperjsModule,
  ],
  templateUrl: './list-indicator.component.html',
})
export class ListIndicatorComponent {
  satudataUser: LoginData;
  jds: Styles = jds;
  jdsBiColor = jdsBiColor;
  moment: any = moment;

  isAction = input<boolean>();
  status = input<string>('active');
  data = input<IndicatorData>();

  @Output() delete = new EventEmitter<number | object>();
  @Output() archive = new EventEmitter<number>();
  @Output() active = new EventEmitter<number>();

  // Service
  private authenticationService = inject(AuthenticationService);
  private iconService = inject(JdsBiIconsService);

  // Variable
  activePath: string;
  public isDropdown = false;

  constructor() {
    // eslint-disable-next-line prefer-destructuring
    this.activePath = window.location.pathname.split('/')[1];

    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    this.iconService.registerIcons([
      iconBuilding,
      iconBriefcase,
      iconCalendar,
      iconCabinetFiling,
      iconCircleDown,
      iconEye,
      iconEllipsis,
    ]);
  }

  getClassification(id: number) {
    return CLASSIFICATION_INDICATOR.get(id);
  }

  onDelete(id: number, category: string) {
    this.delete.emit({ id, category });
  }

  onArchive(id: number) {
    this.archive.emit(id);
  }

  onActive(id: number) {
    this.active.emit(id);
  }
}
