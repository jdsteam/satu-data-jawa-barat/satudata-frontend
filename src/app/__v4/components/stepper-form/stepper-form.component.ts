import { ChangeDetectionStrategy, Component, input } from '@angular/core';
import { CommonModule } from '@angular/common';

// STYLE
import * as jds from '@styles';
// INTERFACE
import { Styles } from '@interfaces-v4';
// JDS-BI
import { jdsBiColor } from '@jds-bi/cdk';

@Component({
  selector: 'app-stepper-form',
  standalone: true,
  imports: [CommonModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './stepper-form.component.html',
})
export class StepperFormComponent {
  jds: Styles = jds;
  jdsBiColor = jdsBiColor;

  stepper = input<string[]>([]);
  current = input<number>(0);
}
