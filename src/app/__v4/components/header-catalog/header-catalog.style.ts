import { Injectable } from '@angular/core';

// PACKAGE
import { css } from '@emotion/css';

@Injectable()
export class HeaderCatalogStyle {
  getStyle(): any {
    const header = css`
      padding-top: 40px;
      padding-right: 0;
      padding-bottom: 35px;
      padding-left: 0;
      background: url(/assets/images/backgrounds/bg_header.png);
      background-position: center;
      background-size: cover;
    `;

    return {
      header,
    };
  }
}
