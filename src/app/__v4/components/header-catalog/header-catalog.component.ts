import {
  ChangeDetectionStrategy,
  Component,
  Signal,
  computed,
  inject,
  input,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Router } from '@angular/router';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// COMPONENT
import { BreadcrumbComponent } from '@components-v4';
// INTERFACE
import { Styles, Breadcrumb } from '@interfaces-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';
// TYPE
import type { Catalog } from '@types-v4';
// JDS-BI
import { JdsBiButtonModule } from '@jds-bi/core';

import { HeaderCatalogStyle } from './header-catalog.style';

export interface HeaderCatalogStyles {
  header: string;
}

@Component({
  selector: 'app-header-catalog',
  standalone: true,
  imports: [CommonModule, RouterModule, BreadcrumbComponent, JdsBiButtonModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './header-catalog.component.html',
  providers: [HeaderCatalogStyle],
})
export class HeaderCatalogComponent {
  colorScheme = COLORSCHEME;
  satudataUser: LoginData;
  jds: Styles = jds;

  catalog = input<Catalog>(null);
  label = input(null);
  description = input(null);
  breadcrumb = input<Breadcrumb[]>(null);

  // Service
  private router = inject(Router);
  private authenticationService = inject(AuthenticationService);
  private headerCatalogStyle = inject(HeaderCatalogStyle);

  // Variable
  className: Signal<HeaderCatalogStyles> = computed(() =>
    this.headerCatalogStyle.getStyle(),
  );

  constructor() {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
  }

  onRequestDataset() {
    const url = this.router.serializeUrl(
      this.router.createUrlTree([`/request/dataset`]),
    );

    window.open(url, '_blank');
  }
}
