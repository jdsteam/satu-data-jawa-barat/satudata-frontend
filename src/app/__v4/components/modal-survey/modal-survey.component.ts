import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
import {
  JdsBiButtonModule,
  JdsBiModalModule,
  JdsBiModalService,
} from '@jds-bi/core';
import { JdsBiIconsModule, JdsBiIconsService, iconXmark } from '@jds-bi/icons';

@Component({
  selector: 'app-modal-survey',
  standalone: true,
  imports: [
    CommonModule,
    JdsBiButtonModule,
    JdsBiIconsModule,
    JdsBiModalModule,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './modal-survey.component.html',
})
export class ModalSurveyComponent {
  colorScheme = COLORSCHEME;
  jds: Styles = jds;

  // Service
  private jdsBiModalService = inject(JdsBiModalService);

  // Variable
  url = 'https://data.jabarprov.go.id/form/s/SKM2024';

  constructor(private JdsBiIconService: JdsBiIconsService) {
    this.JdsBiIconService.registerIcons([iconXmark]);
  }

  openModal() {
    this.jdsBiModalService.open('modal-survey');
  }

  closeModal() {
    this.jdsBiModalService.close('modal-survey');
  }

  openSurvey() {
    this.closeModal();

    window.open(this.url, 'WindowName', 'noopener');
  }
}
