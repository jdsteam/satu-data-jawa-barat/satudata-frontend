import {
  ChangeDetectionStrategy,
  Component,
  inject,
  input,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// MODEL
import {
  OrganizationData,
  OrganizationSubData,
  OrganizationUnitData,
} from '@models-v4';
// JDS-BI
import { jdsBiColor } from '@jds-bi/cdk';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconEnvelope,
  iconPhone,
  iconBuildingColumns,
} from '@jds-bi/icons';

@Component({
  selector: 'app-organization-information-detail',
  standalone: true,
  imports: [CommonModule, JdsBiIconsModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './organization-information-detail.component.html',
  styleUrls: ['./organization-information-detail.component.scss'],
})
export class OrganizationInformationDetailComponent {
  env = environment;
  colorScheme = COLORSCHEME;
  jds: Styles = jds;
  jdsBiColor = jdsBiColor;

  title = input<string>();
  data = input<OrganizationData>();
  dataSub = input<OrganizationSubData>();
  dataUnit = input<OrganizationUnitData>();

  // Service
  private iconService = inject(JdsBiIconsService);

  constructor() {
    this.iconService.registerIcons([
      iconEnvelope,
      iconPhone,
      iconBuildingColumns,
    ]);
  }
}
