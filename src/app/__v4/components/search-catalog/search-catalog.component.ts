import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Output,
  computed,
  input,
  model,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { CATALOG, COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// TYPE
import { Catalog } from '@types-v4';
// JDS-BI
import { jdsBiColor } from '@jds-bi/cdk';
import { JdsBiButtonModule, JdsBiInputModule } from '@jds-bi/core';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconCircleXmark,
} from '@jds-bi/icons';

@Component({
  selector: 'app-search-catalog',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    JdsBiButtonModule,
    JdsBiInputModule,
    JdsBiIconsModule,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './search-catalog.component.html',
})
export class SearchCatalogComponent {
  colorScheme = COLORSCHEME;
  jds: Styles = jds;
  jdsBiColor = jdsBiColor;

  catalog = input<Catalog>();
  search = model();

  @Output() searchChange = new EventEmitter<any>();

  placeholder = computed(() => {
    return CATALOG.get(this.catalog()).name.toLowerCase();
  });

  constructor(private iconService: JdsBiIconsService) {
    this.iconService.registerIcons([iconCircleXmark]);
  }

  onSearch(): void {
    this.searchChange.emit(this.search());
  }

  onClear(): void {
    this.search.set('');
    this.searchChange.emit(this.search());
  }
}
