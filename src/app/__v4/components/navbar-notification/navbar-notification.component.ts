import {
  Component,
  ChangeDetectionStrategy,
  OnInit,
  inject,
  Inject,
  viewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Router } from '@angular/router';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// COMPONENT
import { ModalRequestSatuDataTrackingComponent } from '@components-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';
import { NotificationService } from '@services-v4';
// JDS-BI
import { jdsBiColor } from '@jds-bi/cdk';
import {
  JDS_BI_MEDIA,
  JdsBiMedia,
  JdsBiDropdownModule,
  JdsBiHostedDropdownModule,
  JdsBiScrollbarModule,
  JdsBiNavModule,
  JdsBiButtonModule,
  JdsBiBadgeModule,
} from '@jds-bi/core';

import { includes } from 'lodash';
import moment from 'moment';
import { WINDOW } from '@ng-web-apis/common';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

import { TType, NavbarNotificationStore } from './navbar-notification.store';

@Component({
  selector: 'app-navbar-notification',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    NgxSkeletonLoaderModule,
    JdsBiDropdownModule,
    JdsBiHostedDropdownModule,
    JdsBiScrollbarModule,
    JdsBiNavModule,
    JdsBiButtonModule,
    JdsBiBadgeModule,
    ModalRequestSatuDataTrackingComponent,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './navbar-notification.component.html',
  providers: [NavbarNotificationStore],
})
export class NavbarNotificationComponent implements OnInit {
  colorScheme = COLORSCHEME;
  satudataUser: LoginData;
  jds: Styles = jds;
  jdsBiColor = jdsBiColor;
  moment = moment;

  modalTracking = viewChild(ModalRequestSatuDataTrackingComponent);

  // Service
  private router = inject(Router);
  private authenticationService = inject(AuthenticationService);
  private navbarNotificationStore = inject(NavbarNotificationStore);
  private notificationService = inject(NotificationService);

  // Variable
  isOpen = false;

  // Component Store
  readonly vm$ = this.navbarNotificationStore.vm$;

  constructor(
    @Inject(WINDOW) private readonly windowRef: Window,
    @Inject(JDS_BI_MEDIA) private readonly media: JdsBiMedia,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
  }

  ngOnInit() {
    this.getNotificationTotal();
  }

  get dropdownLimitWidth(): string {
    return this.windowRef.innerWidth <= this.media.mobile ? 'min' : 'custom';
  }

  getNotificationTotal() {
    this.navbarNotificationStore.getCountNotification();
  }

  getAllNotification() {
    this.navbarNotificationStore.getAllNotification();
  }

  updateNotification(type: string, typeId: number) {
    const body = { is_enable: false, is_read: true };
    const params = {
      type,
      type_id: typeId,
      receiver: this.satudataUser.id,
    };

    this.notificationService.updateItemBulk(body, params).subscribe();
  }

  onOpen() {
    this.onClear();
    this.getAllNotification();
  }

  onClear() {
    this.navbarNotificationStore.clearNotification();
  }

  onType(value: TType): void {
    this.navbarNotificationStore.filterType(value);
  }

  onRead(
    feature: number,
    type: string,
    typeId: number,
    title: string = null,
    notes: string = null,
  ) {
    this.updateNotification(type, typeId);

    if (feature === 1) {
      this.router.navigate(['/dataset/review', typeId]);
    } else if (feature === 2) {
      if (this.satudataUser.role_name === 'walidata') {
        this.router.navigate(['/desk'], {
          queryParams: {
            type: 'upload-change',
            status: 'draft',
            statusVerification: notes,
          },
        });
      } else if (this.satudataUser.role_name === 'opd') {
        if (title === 'Walidata melakukan publikasi terhadap dataset ') {
          this.router.navigate(['/dataset/detail', typeId]);
        } else {
          this.router.navigate(['/desk'], {
            queryParams: {
              type: 'upload-change',
              status: 'revision',
            },
          });
        }
      }
    } else if (feature === 4) {
      if (this.satudataUser.role_name === 'walidata') {
        this.router.navigate(['/desk'], {
          queryParams: {
            type: 'request-dataset',
            application: 'opendata',
          },
        });
      }
    } else if (feature === 5) {
      this.router.navigate(['/e-walidata-2/assign']);
    }
  }

  onRequestDatasetPrivate(type: string, typeId: number): void {
    this.updateNotification(type, typeId);

    if (this.satudataUser.role_name === 'walidata') {
      this.router.navigate(['/desk'], {
        queryParams: {
          type: 'request-dataset',
          application: 'satudata',
        },
      });
    } else if (
      this.satudataUser.role_name === 'opd' ||
      this.satudataUser.role_name === 'opd-view'
    ) {
      this.modalTracking().openModal();
      this.navbarNotificationStore.getLastNotification(typeId);
      this.navbarNotificationStore.getHistoryRequestDatasetSatuData(typeId);
    }
  }

  getCatalog(catalog: string) {
    let title = '';
    if (catalog === 'dataset') {
      title = 'Dataset';
    } else if (catalog === 'indicator') {
      title = 'Indikator';
    } else if (catalog === 'visualization') {
      title = 'Visualisasi';
    }
    return title;
  }

  getRequestDatasetPrivateContent(title: string, content: string) {
    let request: string;
    if (includes(content, 'Nomor ticket')) {
      const splitted = content.split('Nomor ticket');
      request = `${title} ${splitted[0]}<br>Nomor Tiket${splitted[1]}`;
    } else if (includes(content, 'Nomor tiket')) {
      const splitted = content.split('Nomor tiket');
      request = `${title} ${splitted[0]}<br>Nomor Tiket${splitted[1]}`;
    } else {
      request = `${title} ${content}`;
    }

    return request;
  }
}
