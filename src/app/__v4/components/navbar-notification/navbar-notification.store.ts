import { Injectable } from '@angular/core';
import { ComponentStore } from '@ngrx/component-store';
import { Observable } from 'rxjs';
import { tap, withLatestFrom, switchMap, map } from 'rxjs/operators';

// CONSTANT
import { NOTIFICATION } from '@constants-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';
import {
  UserService,
  HistoryRequestDatasetSatuDataService,
} from '@services-v4';

import { NavbarNotificationService } from './navbar-notification.service';

const Notification = Array.from(NOTIFICATION, ([, value]) => value.id);
export type TType = (typeof Notification)[number];

export interface NavbarNotificationState {
  filterType: TType;
  countNotification: any;
  listNotification: any;
  detailNotification: any;
  historyRequestDatasetSatuData: any;
}

export const DEFAULT_STATE: NavbarNotificationState = {
  filterType: 'upload-change',
  countNotification: null,
  listNotification: null,
  detailNotification: null,
  historyRequestDatasetSatuData: null,
};

@Injectable()
export class NavbarNotificationStore extends ComponentStore<NavbarNotificationState> {
  satudataUser: LoginData;

  constructor(
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private navbarNotificationService: NavbarNotificationService,
    private historyRequestDatasetSatuDataService: HistoryRequestDatasetSatuDataService,
  ) {
    super(DEFAULT_STATE);

    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
  }

  // *********** Updaters *********** //
  readonly setFilterType = this.updater((state, value: TType) => ({
    ...state,
    filterType: value,
  }));

  readonly setCountNotification = this.updater((state, value: any) => ({
    ...state,
    countNotification: value,
  }));

  readonly setListNotification = this.updater((state, value: any) => ({
    ...state,
    listNotification: value,
  }));

  readonly setDetailNotification = this.updater((state, value: any) => ({
    ...state,
    detailNotification: value,
  }));

  readonly setHistoryRequestDatasetSatuData = this.updater(
    (state, value: any) => ({
      ...state,
      historyRequestDatasetSatuData: value,
    }),
  );

  // *********** Selectors *********** //
  readonly getFilterType$ = this.select(({ filterType }) => filterType);
  readonly getCountNotification$ = this.select(
    ({ countNotification }) => countNotification,
  );
  readonly getListNotification$ = this.select(
    ({ listNotification }) => listNotification,
  );
  readonly getDetailNotification$ = this.select(
    ({ detailNotification }) => detailNotification,
  );
  readonly getHistoryRequestDatasetSatuData$ = this.select(
    ({ historyRequestDatasetSatuData }) => historyRequestDatasetSatuData,
  );

  // ViewModel of Paginator component
  readonly vm$ = this.select(
    this.state$,
    this.getFilterType$,
    this.getCountNotification$,
    this.getListNotification$,
    this.getDetailNotification$,
    this.getHistoryRequestDatasetSatuData$,
    (
      state,
      getFilterType,
      getCountNotification,
      getListNotification,
      getDetailNotification,
      getHistoryRequestDatasetSatuData,
    ) => ({
      filterType: state.filterType,
      countNotification: state.countNotification,
      listNotification: state.listNotification,
      detailNotification: state.detailNotification,
      historyRequestDatasetSatuData: state.historyRequestDatasetSatuData,
      getFilterType,
      getCountNotification,
      getListNotification,
      getDetailNotification,
      getHistoryRequestDatasetSatuData,
    }),
  );

  // *********** Actions *********** //
  readonly filterType = this.effect((type$: Observable<TType>) => {
    return type$.pipe(
      withLatestFrom(this.getFilterType$),
      tap<[TType, TType]>(([type]) => {
        this.setFilterType(type);
      }),
      map(() => {
        this.getAllNotification();
      }),
    );
  });

  readonly getCountNotification = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      withLatestFrom(this.vm$),
      switchMap(() => {
        const params = {};

        return this.userService
          .getQuerySingle(this.satudataUser.id, params)
          .result$.pipe(
            tap({
              next: (result) => {
                this.setCountNotification(result);
              },
            }),
          );
      }),
    );
  });

  readonly getAllNotification = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      withLatestFrom(this.vm$),
      switchMap(([, vm]) => {
        const { filterType: type } = vm;

        const pWhere = {
          receiver: this.satudataUser.id,
        };

        const params = {
          sort: 'cdate:desc',
          limit: 5,
          where: JSON.stringify(pWhere),
        };

        const options = {
          pagination: false,
        };

        return this.navbarNotificationService
          .getQueryList(params, options, type)
          .result$.pipe(
            tap({
              next: (result) => {
                this.setListNotification(result);
              },
            }),
          );
      }),
    );
  });

  readonly getLastNotification = this.effect((id$: Observable<number>) => {
    return id$.pipe(
      switchMap((id) => {
        const pWhere = {
          receiver: this.satudataUser.id,
          type: 'request_private',
          type_id: id,
        };

        const params = {
          sort: 'cdate:desc',
          skip: 0,
          limit: 1,
          where: JSON.stringify(pWhere),
        };

        const options = {
          pagination: false,
        };

        return this.navbarNotificationService
          .getQueryLast(id, params, options)
          .result$.pipe(
            tap({
              next: (result) => {
                this.setDetailNotification(result);
              },
            }),
          );
      }),
    );
  });

  readonly clearNotification = this.effect<void>((trigger$) => {
    return trigger$.pipe(
      withLatestFrom(this.vm$),
      switchMap(() => {
        return this.navbarNotificationService.clear().pipe(
          tap({
            next: () => {
              this.getCountNotification();
            },
          }),
        );
      }),
    );
  });

  readonly getHistoryRequestDatasetSatuData = this.effect(
    (id$: Observable<number>) => {
      return id$.pipe(
        switchMap((id) => {
          const pWhere = {
            request_private_id: id,
          };

          const params = {
            sort: 'datetime:desc',
            where: JSON.stringify(pWhere),
          };

          const options = {
            pagination: false,
          };

          return this.historyRequestDatasetSatuDataService
            .getQueryList(id, params, options)
            .result$.pipe(
              tap({
                next: (result) => {
                  this.setHistoryRequestDatasetSatuData(result);
                },
              }),
            );
        }),
      );
    },
  );
}
