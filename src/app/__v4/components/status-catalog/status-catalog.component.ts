import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Output,
  inject,
  input,
} from '@angular/core';
import { CommonModule } from '@angular/common';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Styles } from '@interfaces-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService } from '@services-v3';
// TYPE
import { Catalog } from '@types-v4';
// JDS-BI
import { JdsBiButtonModule } from '@jds-bi/core';

@Component({
  selector: 'app-status-catalog',
  standalone: true,
  imports: [CommonModule, JdsBiButtonModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './status-catalog.component.html',
})
export class StatusCatalogComponent {
  satudataUser: LoginData;
  colorScheme = COLORSCHEME;
  jds: Styles = jds;

  catalog = input<Catalog>();
  status = input<string>();

  @Output() statusChange = new EventEmitter<any>();

  // Service
  private authenticationService = inject(AuthenticationService);

  constructor() {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
  }

  onStatus(value: any): void {
    this.statusChange.emit(value);
  }
}
