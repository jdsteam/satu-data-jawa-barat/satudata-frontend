import {
  ChangeDetectionStrategy,
  Component,
  Signal,
  computed,
  inject,
  input,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';
// INTERFACE
import { Styles } from '@interfaces-v4';
// JDS-BI
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconGlobe,
  iconPhone,
  iconEnvelope,
} from '@jds-bi/icons';

import { isArray } from 'lodash';

import { HeaderOrganizationStyle } from './header-organization.style';

export interface HeaderOrganizationStyles {
  header: string;
}

@Component({
  selector: 'app-header-organization',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CommonModule, JdsBiIconsModule],
  templateUrl: './header-organization.component.html',
  providers: [HeaderOrganizationStyle],
})
export class HeaderOrganizationComponent {
  env = environment;
  jds: Styles = jds;

  data = input();

  // Service
  private iconService = inject(JdsBiIconsService);
  private headerCatalogStyle = inject(HeaderOrganizationStyle);

  // Variable
  className: Signal<HeaderOrganizationStyles> = computed(() =>
    this.headerCatalogStyle.getStyle(),
  );
  organization = computed(() => {
    return isArray(this.data()) ? this.data()[0] : this.data();
  });

  constructor() {
    this.iconService.registerIcons([iconGlobe, iconPhone, iconEnvelope]);
  }
}
