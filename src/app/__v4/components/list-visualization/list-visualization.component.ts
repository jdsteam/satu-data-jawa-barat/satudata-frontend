import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Output,
  inject,
  input,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';
// INTERFACE
import { Styles } from '@interfaces-v4';
// MODEL
import { LoginData } from '@models-v3';
import { VisualizationData } from '@models-v4';
// PIPE
import { RemoveHtmlTagPipe } from '@pipes-v4';
// SERVICE
import { AuthenticationService } from '@services-v3';
// JDS-BI
import { jdsBiColor } from '@jds-bi/cdk';
import {
  JdsBiBadgeModule,
  JdsBiDropdownModule,
  JdsBiHostedDropdownModule,
  JdsBiButtonModule,
} from '@jds-bi/core';
import {
  JdsBiIconsModule,
  JdsBiIconsService,
  iconBook,
  iconBuilding,
  iconCalendar,
  iconEye,
  iconEllipsis,
} from '@jds-bi/icons';

import moment from 'moment';
import { NgxPopperjsModule } from 'ngx-popperjs';

@Component({
  selector: 'app-list-visualization',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    CommonModule,
    RouterModule,
    JdsBiBadgeModule,
    JdsBiDropdownModule,
    JdsBiHostedDropdownModule,
    JdsBiButtonModule,
    JdsBiIconsModule,
    NgxPopperjsModule,
    RemoveHtmlTagPipe,
  ],
  templateUrl: './list-visualization.component.html',
})
export class ListVisualizationComponent {
  env = environment;
  satudataUser: LoginData;
  jds: Styles = jds;
  jdsBiColor = jdsBiColor;
  moment: any = moment;

  isAction = input<boolean>();
  status = input<string>('active');
  data = input<VisualizationData>();

  @Output() delete = new EventEmitter<number | object>();
  @Output() archive = new EventEmitter<number>();
  @Output() active = new EventEmitter<number>();

  // Service
  private authenticationService = inject(AuthenticationService);
  private iconService = inject(JdsBiIconsService);

  // Variable
  public isDropdown = false;

  constructor() {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );

    this.iconService.registerIcons([
      iconBook,
      iconBuilding,
      iconCalendar,
      iconEye,
      iconEllipsis,
    ]);
  }

  onDelete(id: number, category: string) {
    this.delete.emit({ id, category });
  }

  onArchive(id: number) {
    this.archive.emit(id);
  }

  onActive(id: number) {
    this.active.emit(id);
  }
}
