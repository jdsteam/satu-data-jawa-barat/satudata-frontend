import { HttpClient, HttpHeaders } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class UploadService {
  // API URL
  apiUrl = `${environment.backendURL}upload`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  private http = inject(HttpClient);

  uploadFile(item: any): Observable<any> {
    return this.http.post(this.apiUrl, item, {
      reportProgress: true,
      observe: 'events',
      headers: {
        'Cache-Control': 'no-cache',
      },
    });
  }
}
