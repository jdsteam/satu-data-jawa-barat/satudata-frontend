import { HttpClient, HttpHeaders } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { catchError, map, tap } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { environment } from 'src/environments/environment';

// MODELS
import { STATUS_DATASET } from '@constants-v4';
// MODELS
import { Dataset, DatasetMeilisearch } from '@models-v4';
// SERVICE
import { PaginationService } from '@services-v3';
// UTIL
import { hasOwnProperty } from '@utils-v4';

import { QueryClientService, UseQuery } from '@ngneat/query';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root',
})
export class DatasetService {
  title = 'Dataset';

  // API URL
  apiUrl = `${environment.backendURL}dataset`;
  apiMelisearchUrl = `${environment.backendURL}dataset/list`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  private http = inject(HttpClient);
  private queryClient = inject(QueryClientService);
  private useQuery = inject(UseQuery);
  private paginationService = inject(PaginationService);
  private toastr = inject(ToastrService);

  // Total
  getTotal(params: any) {
    const merge = new URLSearchParams(params);

    const queryName = `List ${this.title}`;

    return this.http
      .get<any>(`${this.apiUrl}?${merge}&count=true`, this.httpOptions)
      .pipe(
        map((response) => {
          if (response.error === 1 && response.message !== 'Data not found') {
            throw new Error(response.message);
          }

          return new Dataset().deserialize(response);
        }),
        map((response) => {
          return response.data.count;
        }),
        catchError((e) => {
          this.toastr.error(e.message, queryName);
          return throwError(() => e);
        }),
      );
  }

  getQueryTotal(params: any, options: any = {}) {
    return this.useQuery(
      ['countDataset'],
      () => {
        return this.getTotal(params);
      },
      options,
    );
  }

  // List
  getList(params: any, options: any) {
    const merge = new URLSearchParams(params);

    const queryName = `List ${this.title}`;

    return this.http.get<any>(`${this.apiUrl}?${merge}`, this.httpOptions).pipe(
      map((response) => {
        if (response.error === 1 && response.message !== 'Data not found') {
          throw new Error(response.message);
        }

        return new Dataset().deserialize(response);
      }),
      map((response) => {
        const hasPagination = hasOwnProperty(options, 'pagination');
        const hasInfinite = hasOwnProperty(options, 'infinite');

        const isPagination = hasPagination ? options.pagination : true;
        const isInfinite = hasInfinite ? options.infinite : false;

        let pagination = { empty: true, infinite: isInfinite };
        if (isPagination) {
          const resultPagination = this.paginationService.getPagination(
            response.meta.total_record,
            params.skip,
            params.limit,
          );

          pagination = {
            empty: false,
            infinite: isInfinite,
            ...resultPagination,
          };
        }

        return {
          pagination,
          list: response.data,
        };
      }),
      catchError((e) => {
        this.toastr.error(e.message, queryName);
        return throwError(() => e);
      }),
    );
  }

  getQueryList(params: any, options: any = {}) {
    return this.useQuery(
      ['dataset'],
      () => {
        return this.getList(params, options);
      },
      options,
    );
  }

  // List Meilisearch
  getListMeilisearch(params: any, options: any) {
    const merge = new URLSearchParams(params);

    const queryName = `List ${this.title} Meilisearch`;

    return this.http
      .get<any>(`${this.apiMelisearchUrl}?${merge}`, this.httpOptions)
      .pipe(
        map((response) => {
          if (response.error === 1 && response.message !== 'Data not found') {
            throw new Error(response.message);
          }

          return new DatasetMeilisearch().deserialize(response);
        }),
        map((response) => {
          const hasPagination = hasOwnProperty(options, 'pagination');
          const hasInfinite = hasOwnProperty(options, 'infinite');

          const isPagination = hasPagination ? options.pagination : true;
          const isInfinite = hasInfinite ? options.infinite : false;

          let pagination = { empty: true, infinite: isInfinite };
          if (isPagination) {
            const resultPagination = this.paginationService.getPagination(
              response.meta.total_record,
              params.skip,
              params.limit,
            );

            pagination = {
              empty: false,
              infinite: isInfinite,
              ...resultPagination,
            };
          }

          return {
            pagination,
            list: response.data,
          };
        }),
        catchError((e) => {
          this.toastr.error(e.message, queryName);
          return throwError(() => e);
        }),
      );
  }

  getQueryListMeilisearch(params: any, options: any = {}) {
    return this.useQuery(
      ['datasetMeilisearch'],
      () => {
        return this.getListMeilisearch(params, options);
      },
      options,
    );
  }

  // Single
  getSingle(uniq: number, params: any) {
    const merge = new URLSearchParams(params);

    const queryName = `Single ${this.title}`;

    return this.http
      .get<any>(`${this.apiUrl}/${uniq}?${merge}`, this.httpOptions)
      .pipe(
        map((response) => {
          if (response.error === 1) {
            if (response.message !== 'Data not found') {
              throw new Error(response.message);
            }
          }

          return new Dataset().deserialize(response);
        }),
        map((response) => {
          return response.data;
        }),
        catchError((e) => {
          this.toastr.error(e.message, queryName);
          return throwError(() => e);
        }),
      );
  }

  getQuerySingle(uniq: number, params: any, options = {}) {
    return this.useQuery(
      ['dataset', uniq],
      () => {
        return this.getSingle(uniq, params);
      },
      options,
    );
  }

  // Create
  createItem(item: any) {
    return this.http.post<any>(this.apiUrl, item).pipe(
      tap(() => {
        this.queryClient.invalidateQueries(['dataset']);
      }),
      catchError((e) => {
        this.toastr.error(e.message, 'Dataset');
        return throwError(() => e);
      }),
    );
  }

  // Update
  updateItem(uniq: any, item: any, options: any = {}) {
    return this.http
      .put<any>(`${this.apiUrl}/${uniq}`, item, this.httpOptions)
      .pipe(
        tap(() => {
          const hasInvalidate = hasOwnProperty(options, 'invalidate');
          const isInvalidate = hasInvalidate ? options.invalidate : false;

          if (isInvalidate) {
            this.queryClient.invalidateQueries(['dataset']);
          }
        }),
        catchError((e) => {
          this.toastr.error(e.message, 'Dataset');
          return throwError(() => e);
        }),
      );
  }

  searchItem(search: string) {
    const queryName = `Search ${this.title}`;

    const params = {
      sort: 'name:asc',
      search,
      where: JSON.stringify(STATUS_DATASET.get('approve-active')),
    };
    const merge = new URLSearchParams(params);

    return this.http.get<any>(`${this.apiUrl}?${merge}`, this.httpOptions).pipe(
      map((response) => {
        if (response.error === 1 && response.message !== 'Data not found') {
          throw new Error(response.message);
        }

        return new Dataset().deserialize(response);
      }),
      map((response) => {
        return response.data;
      }),
      catchError((e) => {
        this.toastr.error(e.message, queryName);
        return throwError(() => e);
      }),
    );
  }
}
