import { HttpClient, HttpHeaders } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { catchError, map, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { environment } from 'src/environments/environment';

// CONSTANT
import { NOTIFICATION } from '@constants-v4';
// MODEL
import { Notification } from '@models-v4';
// SERVICE
import { PaginationService } from '@services-v3';
// UTIL
import { hasOwnProperty } from '@utils-v4';

import { assign } from 'lodash';
import { QueryClientService, UseQuery } from '@ngneat/query';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root',
})
export class NotificationService {
  title = 'Notification';

  // API URL
  apiUrl = `${environment.backendURL}notification`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  private http = inject(HttpClient);
  private queryClient = inject(QueryClientService);
  private useQuery = inject(UseQuery);
  private paginationService = inject(PaginationService);
  private toastr = inject(ToastrService);

  modifyWhereParams(type: any, params: any) {
    if (type) {
      const where = JSON.parse(params.where);
      assign(where, { feature: type.paramWhere });
      params.where = JSON.stringify(where);
    }

    return params;
  }

  // Total
  getTotal(params: any, options: any, type = null) {
    const typeData = NOTIFICATION.get(type);
    const newParams = this.modifyWhereParams(typeData, params);
    const merge = new URLSearchParams(newParams);

    const queryName = typeData
      ? `Count ${this.title} ${typeData.name}`
      : `Count ${this.title}`;

    return this.http
      .get<any>(`${this.apiUrl}?${merge}&count=true`, this.httpOptions)
      .pipe(
        map((response) => {
          if (response.error === 1 && response.message !== 'Data not found') {
            throw new Error(response.message);
          }

          return new Notification().deserialize(response);
        }),
        map((response) => {
          return response.data.count;
        }),
        catchError((e) => {
          this.toastr.error(e.message, queryName);
          return of(null);
        }),
      );
  }

  getQueryTotal(params: any, options: any = {}, type = null) {
    const typeData = NOTIFICATION.get(type);

    const queryKey = typeData
      ? `countNotification${typeData.alias}`
      : `countNotification`;

    return this.useQuery(
      [queryKey],
      () => {
        return this.getTotal(params, options, type);
      },
      options,
    );
  }

  // List
  getList(params: any, options: any, type = null) {
    const typeData = NOTIFICATION.get(type);
    const newParams = this.modifyWhereParams(typeData, params);
    const merge = new URLSearchParams(newParams);

    const queryName = typeData
      ? `List ${this.title} ${typeData.name}`
      : `List ${this.title}`;

    return this.http.get<any>(`${this.apiUrl}?${merge}`, this.httpOptions).pipe(
      map((response) => {
        if (response.error === 1 && response.message !== 'Data not found') {
          throw new Error(response.message);
        }

        return new Notification().deserialize(response);
      }),
      map((response) => {
        const hasPagination = hasOwnProperty(options, 'pagination');
        const hasInfinite = hasOwnProperty(options, 'infinite');

        const isPagination = hasPagination ? options.pagination : true;
        const isInfinite = hasInfinite ? options.infinite : false;

        let pagination = { empty: true, infinite: isInfinite };
        if (isPagination) {
          const resultPagination = this.paginationService.getPagination(
            response.meta.total_record,
            params.skip,
            params.limit,
          );

          pagination = {
            empty: false,
            infinite: isInfinite,
            ...resultPagination,
          };
        }

        return {
          pagination,
          list: response.data,
        };
      }),
      catchError((e) => {
        this.toastr.error(e.message, queryName);
        return of(null);
      }),
    );
  }

  getQueryList(params: any, options: any = {}, type = null) {
    const typeData = NOTIFICATION.get(type);

    const queryKey = typeData
      ? `notification${typeData.alias}`
      : `notification`;

    return this.useQuery(
      [queryKey],
      () => {
        return this.getList(params, options, type);
      },
      options,
    );
  }

  // Last
  getQueryLast(uniq: number, params: any, options: any = {}) {
    return this.useQuery(
      ['notificationLast', uniq],
      () => {
        return this.getList(params, options);
      },
      options,
    );
  }

  // Update Bulk
  updateItemBulk(item: any, params: any) {
    const merge = new URLSearchParams(params);

    const queryName = `Update Bulk ${this.title}`;

    return this.http
      .put<any>(`${this.apiUrl}?${merge}`, item, this.httpOptions)
      .pipe(
        tap(() => {
          this.queryClient.invalidateQueries(['notification']);
        }),
        catchError((e) => {
          this.toastr.error(e.message, queryName);
          return of(null);
        }),
      );
  }

  // Clear
  clear() {
    const queryName = `Clear ${this.title}`;

    return this.http.get<any>(`${this.apiUrl}/clear`, this.httpOptions).pipe(
      catchError((e) => {
        this.toastr.error(e.message, queryName);
        return of(null);
      }),
    );
  }
}
