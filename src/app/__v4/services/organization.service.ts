import { HttpClient, HttpHeaders } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { environment } from 'src/environments/environment';

// CONSTANT
import { APPLICATION } from '@constants-v4';
// MODEL
import { Organization } from '@models-v4';
// SERVICE
import { PaginationService } from '@services-v3';
// UTIL
import { hasOwnProperty } from '@utils-v4';

import { assign } from 'lodash';
import { UseQuery } from '@ngneat/query';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root',
})
export class OrganizationService {
  title = 'Organization';

  // API URL
  apiUrl = `${environment.backendURL}skpd`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  private http = inject(HttpClient);
  private useQuery = inject(UseQuery);
  private paginationService = inject(PaginationService);
  private toastr = inject(ToastrService);

  modifyWhereParams(app: any, params: any) {
    if (app) {
      const where = JSON.parse(params.where);
      assign(where, { [app.paramWhere]: true });
      params.where = JSON.stringify(where);
    }

    return params;
  }

  // Total
  getTotal(params: any, app = null) {
    const appData = APPLICATION.get(app);
    const newParams = this.modifyWhereParams(appData, params);
    const merge = new URLSearchParams(newParams);

    const queryName = appData
      ? `Count ${this.title} ${appData.name}`
      : `Count ${this.title}`;

    return this.http
      .get<any>(`${this.apiUrl}?${merge}&count=true`, this.httpOptions)
      .pipe(
        map((response) => {
          if (response.error === 1 && response.message !== 'Data not found') {
            throw new Error(response.message);
          }

          return new Organization().deserialize(response);
        }),
        map((response) => {
          return response.data.count;
        }),
        catchError((e) => {
          this.toastr.error(e.message, queryName);
          return throwError(() => e);
        }),
      );
  }

  getQueryTotal(params: any, options: any = {}, app = null) {
    const appData = APPLICATION.get(app);

    const queryKey = appData
      ? `countOrganization${appData.alias}`
      : `countOrganization`;

    return this.useQuery(
      [queryKey],
      () => {
        return this.getTotal(params, app);
      },
      options,
    );
  }

  // List
  getList(params: any, options: any, app = null) {
    const appData = APPLICATION.get(app);
    const newParams = this.modifyWhereParams(appData, params);
    const merge = new URLSearchParams(newParams);

    const queryName = appData
      ? `List ${this.title} ${appData.name}`
      : `List ${this.title}`;

    return this.http.get<any>(`${this.apiUrl}?${merge}`, this.httpOptions).pipe(
      map((response) => {
        if (response.error === 1 && response.message !== 'Data not found') {
          throw new Error(response.message);
        }

        return new Organization().deserialize(response);
      }),
      map((response) => {
        const hasPagination = hasOwnProperty(options, 'pagination');
        const hasInfinite = hasOwnProperty(options, 'infinite');

        const isPagination = hasPagination ? options.pagination : true;
        const isInfinite = hasInfinite ? options.infinite : false;

        let pagination = { empty: true, infinite: isInfinite };
        if (isPagination) {
          const resultPagination = this.paginationService.getPagination(
            response.meta.total_record,
            params.skip,
            params.limit,
          );

          pagination = {
            empty: false,
            infinite: isInfinite,
            ...resultPagination,
          };
        }

        return {
          pagination,
          list: response.data,
        };
      }),
      catchError((e) => {
        this.toastr.error(e.message, queryName);
        return throwError(() => e);
      }),
    );
  }

  getQueryList(params: any, options: any = {}, app = null) {
    const appData = APPLICATION.get(app);

    const queryKey = appData ? `organization${appData.alias}` : `organization`;

    return this.useQuery(
      [queryKey],
      () => {
        return this.getList(params, options, app);
      },
      options,
    );
  }

  // Single
  getSingle(uniq: number, params: any, app = null) {
    const appData = APPLICATION.get(app);
    const newParams = this.modifyWhereParams(appData, params);
    const merge = new URLSearchParams(newParams);

    const queryName = appData
      ? `Single ${this.title} ${appData.name}`
      : `Single ${this.title}`;

    return this.http
      .get<any>(`${this.apiUrl}/${uniq}?${merge}`, this.httpOptions)
      .pipe(
        map((response) => {
          if (response.error === 1) {
            if (response.message !== 'Data not found') {
              throw new Error(response.message);
            }
          }

          return new Organization().deserialize(response);
        }),
        map((response) => {
          return response.data;
        }),
        catchError((e) => {
          this.toastr.error(e.message, queryName);
          return throwError(() => e);
        }),
      );
  }

  getQuerySingle(uniq: number, params: any, options: any = {}, app = null) {
    const appData = APPLICATION.get(app);

    const queryKey = appData ? `organization${appData.alias}` : `organization`;

    return this.useQuery(
      [queryKey, uniq],
      () => {
        return this.getSingle(uniq, params, app);
      },
      options,
    );
  }
}
