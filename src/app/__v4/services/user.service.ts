import { HttpClient, HttpHeaders } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { of } from 'rxjs';
import { environment } from 'src/environments/environment';

// MODELS
import { User } from '@models-v4';
// SERVICE
import { PaginationService } from '@services-v3';

import { QueryClientService, UseQuery } from '@ngneat/query';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  title = 'User';

  // API URL
  apiUrl = `${environment.backendURL}user`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  private http = inject(HttpClient);
  private queryClient = inject(QueryClientService);
  private useQuery = inject(UseQuery);
  private paginationService = inject(PaginationService);
  private toastr = inject(ToastrService);

  // Single
  getSingle(uniq: number, params: any) {
    const merge = new URLSearchParams(params);

    const queryName = `Single ${this.title}`;

    return this.http
      .get<any>(`${this.apiUrl}/${uniq}?${merge}`, this.httpOptions)
      .pipe(
        map((response) => {
          if (response.error === 1) {
            if (response.message !== 'Data not found') {
              throw new Error(response.message);
            }
          }

          return new User().deserialize(response);
        }),
        map((response) => {
          return response.data;
        }),
        catchError((e) => {
          this.toastr.error(e.message, queryName);
          return of(null);
        }),
      );
  }

  getQuerySingle(uniq: number, params: any, options: any = {}) {
    return this.useQuery(
      ['user', uniq],
      () => {
        return this.getSingle(uniq, params);
      },
      options,
    );
  }
}
