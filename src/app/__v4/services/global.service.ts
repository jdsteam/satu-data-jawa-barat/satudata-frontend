import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

// INTERFACE
import { Breadcrumb } from '@interfaces-v4';

@Injectable()
export class GlobalService {
  readonly title: string = 'Satu Data Jabar';

  private labelSource = new BehaviorSubject('Satu Data Jabar');
  currentLabel = this.labelSource.asObservable();

  private labeldescriptionSource = new BehaviorSubject('Satu Data Jabar');
  currentLabelDescription = this.labeldescriptionSource.asObservable();

  private breadcrumbSource = new BehaviorSubject([]);
  currentBreadcrumb = this.breadcrumbSource.asObservable();

  private notificationSource = new BehaviorSubject({});
  currentNotification = this.notificationSource.asObservable();

  changeLabel(label: string) {
    this.labelSource.next(label);
  }

  changeLabelDescription(labeldescription: string) {
    this.labeldescriptionSource.next(labeldescription);
  }

  changeBreadcrumb(breadcrumb: Breadcrumb[]) {
    this.breadcrumbSource.next(breadcrumb);
  }

  changeNotification(total: number) {
    this.notificationSource.next(total);
  }
}
