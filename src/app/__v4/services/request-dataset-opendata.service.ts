import { HttpClient, HttpHeaders } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { catchError, map, tap } from 'rxjs/operators';
import { of, zip } from 'rxjs';
import { environment } from 'src/environments/environment';

// CONSTANT
import { STATUS_REQUEST_OPENDATA as STATUS_REQUEST } from '@constants-v4';
// MODEL
import { RequestDatasetOpenData } from '@models-v4';
// SERVICE
import { PaginationService } from '@services-v3';
// UTIL
import { hasOwnProperty } from '@utils-v4';

import { assign } from 'lodash';
import { QueryClientService, UseQuery } from '@ngneat/query';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root',
})
export class RequestDatasetOpenDataService {
  title = 'Request Dataset Open Data';

  // API URL
  apiUrl = `${environment.backendURL}request_public`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  private http = inject(HttpClient);
  private queryClient = inject(QueryClientService);
  private useQuery = inject(UseQuery);
  private paginationService = inject(PaginationService);
  private toastr = inject(ToastrService);

  modifyWhereParams(status: any, params: any) {
    if (status) {
      const where = JSON.parse(params.where);
      assign(where, { status: status.id });
      params.where = JSON.stringify(where);
    }

    return params;
  }

  // Total
  getTotal(params: any, status = null) {
    const appData = STATUS_REQUEST.get(status);
    const newParams = this.modifyWhereParams(appData, params);
    const merge = new URLSearchParams(newParams);

    const queryName = appData
      ? `Count ${this.title} ${appData.name}`
      : `Count ${this.title}`;

    return this.http
      .get<any>(`${this.apiUrl}?${merge}&count=true`, this.httpOptions)
      .pipe(
        map((response) => {
          if (response.error === 1 && response.message !== 'Data not found') {
            throw new Error(response.message);
          }

          return new RequestDatasetOpenData().deserialize(response);
        }),
        map((response) => {
          return response.data.count;
        }),
        catchError((e) => {
          this.toastr.error(e.message, queryName);
          return of(null);
        }),
      );
  }

  getQueryTotal(params: any, options: any = {}, status = null) {
    const statusData = STATUS_REQUEST.get(status);

    const queryKey = statusData
      ? `countRequestDatasetOpenData${statusData.alias}`
      : `countRequestDatasetOpenData`;

    return this.useQuery(
      [queryKey],
      () => {
        return this.getTotal(params, status);
      },
      options,
    );
  }

  // List
  getList(params: any, options: any, status = null) {
    const appData = STATUS_REQUEST.get(status);
    const newParams = this.modifyWhereParams(appData, params);
    const merge = new URLSearchParams(newParams);

    const queryName = appData
      ? `List ${this.title} ${appData.name}`
      : `List ${this.title}`;

    // TODO: remove if meta exist
    const getTotal = this.getTotal(params, status);
    const getList = this.http.get<any>(
      `${this.apiUrl}?${merge}`,
      this.httpOptions,
    );

    return zip(getTotal, getList).pipe(
      map(([total, list]) => {
        if (list.error === 1) {
          throw new Error(list.message);
        }

        return [total, new RequestDatasetOpenData().deserialize(list)];
      }),
      map(([total, list]) => {
        const hasPagination = hasOwnProperty(options, 'pagination');
        const hasInfinite = hasOwnProperty(options, 'infinite');

        const isPagination = hasPagination ? options.pagination : true;
        const isInfinite = hasInfinite ? options.infinite : false;

        let pagination = { empty: true, infinite: isInfinite };
        if (isPagination) {
          const resultPagination = this.paginationService.getPagination(
            total,
            params.skip,
            params.limit,
          );

          pagination = {
            empty: false,
            infinite: isInfinite,
            ...resultPagination,
          };
        }

        return {
          pagination,
          list: list.data,
        };
      }),
      catchError((e) => {
        this.toastr.error(e.message, queryName);
        return of(null);
      }),
    );

    // return this.http.get<any>(`${this.apiUrl}?${merge}`, this.httpOptions).pipe(
    //   map((response) => {
    //     if (response.error === 1) {
    //       throw new Error(response.message);
    //     }

    //     return new RequestDatasetOpenData().deserialize(response);
    //   }),
    //   map((response) => {
    //     const hasPagination = hasOwnProperty(options, 'pagination');
    //     const hasInfinite = hasOwnProperty(options, 'infinite');

    //     const isPagination = hasPagination ? options.pagination : true;
    //     const isInfinite = hasInfinite ? options.infinite : false;

    //     let pagination = { empty: true, infinite: isInfinite };
    //     if (isPagination) {
    //       const resultPagination = this.paginationService.getPagination(
    //         response.meta.total_record,
    //         params.skip,
    //         params.limit,
    //       );

    //       pagination = {
    //         empty: false,
    //         infinite: isInfinite,
    //         ...resultPagination,
    //       };
    //     }

    //     return {
    //       pagination,
    //       list: response.data,
    //     };
    //   }),
    //   catchError((e) => {
    //     this.toastr.error(e.message, queryName);
    //     return of(null);
    //   }),
    // );
  }

  getQueryList(params: any, options: any = {}, status = null) {
    const statusData = STATUS_REQUEST.get(status);

    const queryKey = statusData
      ? `requestDatasetOpenData${statusData.alias}`
      : `requestDatasetOpenData`;

    return this.useQuery(
      [queryKey],
      () => {
        return this.getList(params, options, status);
      },
      options,
    );
  }

  // Single
  getSingle(uniq: number, params: any) {
    const merge = new URLSearchParams(params);

    const queryName = `Single ${this.title}`;

    return this.http
      .get<any>(`${this.apiUrl}/${uniq}?${merge}`, this.httpOptions)
      .pipe(
        map((response) => {
          if (response.error === 1) {
            if (response.message !== 'Data not found') {
              throw new Error(response.message);
            }
          }

          return new RequestDatasetOpenData().deserialize(response);
        }),
        map((response) => {
          return response.data;
        }),
        catchError((e) => {
          this.toastr.error(e.message, queryName);
          return of(null);
        }),
      );
  }

  getQuerySingle(uniq: number, params: any, options = {}) {
    return this.useQuery(
      ['requestDatasetOpenData', uniq],
      () => {
        return this.getSingle(uniq, params);
      },
      options,
    );
  }

  // Update
  updateItem(uniq: any, item: any) {
    const queryName = `Update ${this.title}`;

    return this.http
      .put<any>(`${this.apiUrl}/${uniq}`, item, this.httpOptions)
      .pipe(
        tap(() => {
          this.queryClient.invalidateQueries(['requestDatasetOpenData']);
        }),
        catchError((e) => {
          this.toastr.error(e.message, queryName);
          return of(null);
        }),
      );
  }
}
