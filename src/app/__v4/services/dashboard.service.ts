import { HttpClient, HttpHeaders } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { environment } from 'src/environments/environment';

import { QueryClientService } from '@ngneat/query';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root',
})
export class DashboardService {
  title = 'Dashboard';

  // API URL
  apiStatTotalUrl = `${environment.backendURL}dashboard/stat_total`;
  apiTopOrganizationUrl = `${environment.backendURL}dashboard/top_opd`;
  apiTopDatasetUrl = `${environment.backendURL}dashboard/top_dataset`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  private http = inject(HttpClient);
  private queryClient = inject(QueryClientService);
  private toastr = inject(ToastrService);

  // List
  getListStatTotal(params: any) {
    const merge = new URLSearchParams(params);

    const queryName = `List Stat Total ${this.title}`;

    return this.http
      .get<any>(`${this.apiStatTotalUrl}?${merge}`, this.httpOptions)
      .pipe(
        catchError((e) => {
          this.toastr.error(e.message, queryName);
          return throwError(() => e);
        }),
      );
  }

  getListTopOrganization(params: any) {
    const merge = new URLSearchParams(params);

    const queryName = `List Top Organization ${this.title}`;

    return this.http
      .get<any>(`${this.apiTopOrganizationUrl}?${merge}`, this.httpOptions)
      .pipe(
        catchError((e) => {
          this.toastr.error(e.message, queryName);
          return throwError(() => e);
        }),
      );
  }

  getListTopDataset(params: any) {
    const merge = new URLSearchParams(params);

    const queryName = `List Top Dataset ${this.title}`;

    return this.http
      .get<any>(`${this.apiTopDatasetUrl}?${merge}`, this.httpOptions)
      .pipe(
        catchError((e) => {
          this.toastr.error(e.message, queryName);
          return throwError(() => e);
        }),
      );
  }
}
