import { HttpClient, HttpHeaders } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { catchError, map } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';

// MODELS
import {
  MapsetGeoserverLayer,
  MapsetGeoserverWorkspace,
  MapsetGeoserverWorkspaceData,
} from '@models-v4';

import { QueryClientService, UseQuery } from '@ngneat/query';
import WMSCapabilities from 'ol/format/WMSCapabilities';

@Injectable({
  providedIn: 'root',
})
export class MapsetGeoserverService {
  title = 'Mapset Geoserver';

  // API URL
  apiUrl = environment.geoserverURL;

  // Http Options
  httpAuthorization = {
    Authorization: `Basic ${btoa(
      `${environment.geoserverUsername}:${environment.geoserverPassword}`,
    )}`,
  };
  httpOptionsWMS = {
    responseType: 'text' as 'json',
  };
  httpOptionsRest = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      ...this.httpAuthorization,
    }),
  };
  httpOptionsFileSHP = {
    headers: new HttpHeaders({
      ...this.httpAuthorization,
    }),
    responseType: 'arraybuffer' as 'json',
  };

  private parserWMS = new WMSCapabilities();

  private http = inject(HttpClient);
  private queryClient = inject(QueryClientService);
  private useQuery = inject(UseQuery);
  private sanitizer = inject(DomSanitizer);

  // List
  getList(params: any) {
    const merge = new URLSearchParams(params);

    return this.http
      .get<any>(`${this.apiUrl}wms?${merge}`, this.httpOptionsWMS)
      .pipe(
        map((response) => this.parserWMS.read(response)),
        map((response) => {
          const records = response.Capability.Layer.Layer;

          const data = records
            .map((record: any) => {
              const namespace = record.Name.split(':')[0];
              const layers = record.Name;
              const bbox = record.EX_GeographicBoundingBox;
              const width = Math.round((bbox[3] - bbox[1]) * 1000);
              const height = Math.round((bbox[2] - bbox[0]) * 1000);

              const openlayerParams = {
                service: 'WMS',
                version: '1.1.0',
                request: 'GetMap',
                layers,
                bbox,
                width: width.toString(),
                height: height.toString(),
                srs: 'EPSG:4326',
                styles: '',
                format: 'application/openlayers',
              };

              const openlayerBaseUrl = `${this.apiUrl}${namespace}/wms`;
              const openlayerUrl = `${openlayerBaseUrl}?${new URLSearchParams(
                openlayerParams,
              )}`;

              return {
                url: openlayerUrl,
                title: record.Name,
              };
            })
            .sort((a, b) => a.title.localeCompare(b.title));

          return {
            list: data,
          };
        }),
        catchError((e) => {
          return throwError(() => e);
        }),
      );
  }

  getQueryList(params: any) {
    return this.useQuery(['mapsetGeoserver'], () => {
      return this.getList(params);
    });
  }

  getListLayer(): Observable<{ list: { title: string }[] }> {
    return this.http
      .get<MapsetGeoserverLayer>(
        `${this.apiUrl}rest/layers.json`,
        this.httpOptionsRest,
      )
      .pipe(
        map((response) => {
          const layers = response.layers.layer;

          const data = layers
            .map((layer: MapsetGeoserverWorkspaceData) => {
              return {
                title: layer.name,
              };
            })
            .sort((a, b) => a.title.localeCompare(b.title));

          return {
            list: data,
          };
        }),
        catchError((e) => {
          return throwError(() => e);
        }),
      );
  }

  getQueryListLayer() {
    return this.useQuery(['mapsetGeoserver'], () => {
      return this.getListLayer();
    });
  }

  getListWorkspace(data: {
    title: string;
  }): Observable<MapsetGeoserverWorkspaceData[]> {
    const namespace = data.title.split(':')[0];

    return this.http
      .get<MapsetGeoserverWorkspace>(
        `${this.apiUrl}rest/workspaces/${namespace}/datastores.json`,
        this.httpOptionsRest,
      )
      .pipe(
        map((response) => {
          const dataStores = response.dataStores.dataStore;

          return dataStores;
        }),
        catchError((e) => {
          return throwError(() => e);
        }),
      );
  }

  getListWorkspaceFeature(data: {
    title: string;
    dataStore: string;
  }): Observable<string> {
    const namespace = data.title.split(':')[0];
    const layer = data.title.split(':')[1];

    return this.http
      .get<any>(
        `${this.apiUrl}rest/workspaces/${namespace}/datastores/${data.dataStore}/featuretypes/${layer}.json`,
        this.httpOptionsRest,
      )
      .pipe(
        map((response) => {
          const { featureType } = response;
          const featureNamespace = featureType.namespace.name;
          const featureLayer = featureType.name;
          const bbox = featureType.nativeBoundingBox;
          const { srs } = featureType;

          const openlayerParams = {
            service: 'WMS',
            version: '1.1.0',
            request: 'GetMap',
            layers: `${featureNamespace}:${featureLayer}`,
            bbox: `${bbox.minx},${bbox.miny},${bbox.maxx},${bbox.maxy}`,
            width: '800',
            height: '650',
            srs,
            styles: '',
            format: 'application/openlayers',
          };

          const openlayerBaseUrl = `${this.apiUrl}${featureNamespace}/wms`;
          const openlayerSearchParams = new URLSearchParams(openlayerParams);
          const openlayerUrl = `${openlayerBaseUrl}?${openlayerSearchParams}`;

          return openlayerUrl;
        }),
        catchError((e) => {
          return throwError(() => e);
        }),
      );
  }

  getFileSHP(url: string) {
    return this.http.get<any>(url, this.httpOptionsFileSHP).pipe(
      map((response: ArrayBuffer) => {
        return new Blob([response], { type: 'application/zip' });
      }),
      catchError((e) => {
        return throwError(() => e);
      }),
    );
  }
}
