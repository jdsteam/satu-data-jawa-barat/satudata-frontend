import { HttpClient, HttpHeaders } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { environment } from 'src/environments/environment';

// MODELS
import { IndicatorConnect } from '@models-v4';
// SERVICE
import { PaginationService } from '@services-v3';
// UTIL
import { hasOwnProperty } from '@utils-v4';

import { QueryClientService, UseQuery } from '@ngneat/query';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root',
})
export class IndicatorConnectService {
  title = 'Indicator Connect';

  // API URL
  apiUrl = `${environment.backendURL}indikator_terhubung`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  private http = inject(HttpClient);
  private useQuery = inject(UseQuery);
  private queryClient = inject(QueryClientService);
  private paginationService = inject(PaginationService);
  private toastr = inject(ToastrService);

  // Total
  getTotal(params: any) {
    const merge = new URLSearchParams(params);

    const queryName = `List ${this.title}`;

    return this.http
      .get<any>(`${this.apiUrl}?${merge}&count=true`, this.httpOptions)
      .pipe(
        map((response) => {
          if (response.error === 1 && response.message !== 'Data not found') {
            throw new Error(response.message);
          }

          return new IndicatorConnect().deserialize(response);
        }),
        map((response) => {
          return response.data.count;
        }),
        catchError((e) => {
          this.toastr.error(e.message, queryName);
          return throwError(() => e);
        }),
      );
  }

  getQueryTotal(params: any, options: any = {}) {
    return this.useQuery(
      ['countIndicatorConnect'],
      () => {
        return this.getTotal(params);
      },
      options,
    );
  }

  // List
  getList(params: any, options: any) {
    const merge = new URLSearchParams(params);

    const queryName = `List ${this.title}`;

    return this.http.get<any>(`${this.apiUrl}?${merge}`, this.httpOptions).pipe(
      map((response) => {
        if (response.error === 1 && response.message !== 'Data not found') {
          throw new Error(response.message);
        }

        return new IndicatorConnect().deserialize(response);
      }),
      map((response) => {
        const hasPagination = hasOwnProperty(options, 'pagination');
        const hasInfinite = hasOwnProperty(options, 'infinite');

        const isPagination = hasPagination ? options.pagination : true;
        const isInfinite = hasInfinite ? options.infinite : false;

        let pagination = { empty: true, infinite: isInfinite };
        if (isPagination) {
          const resultPagination = this.paginationService.getPagination(
            response.meta.total_record,
            params.skip,
            params.limit,
          );

          pagination = {
            empty: false,
            infinite: isInfinite,
            ...resultPagination,
          };
        }

        return {
          pagination,
          list: response.data,
        };
      }),
      catchError((e) => {
        this.toastr.error(e.message, queryName);
        return throwError(() => e);
      }),
    );
  }

  getQueryList(params: any, options: any = {}) {
    return this.useQuery(
      ['indicatorConnect'],
      () => {
        return this.getList(params, options);
      },
      options,
    );
  }
}
