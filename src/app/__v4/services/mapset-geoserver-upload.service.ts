import { HttpClient, HttpHeaders } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

// MODELS
import { MapsetUpload } from '@models-v4';

@Injectable({
  providedIn: 'root',
})
export class MapsetGeoserverUploadService {
  // API URL
  apiUrl = `${environment.backendURL}mapset/geoserver_upload`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  private http = inject(HttpClient);

  uploadFile(item: any): Observable<any> {
    return this.http
      .post(this.apiUrl, item, {
        reportProgress: true,
        observe: 'events',
        headers: {
          'Cache-Control': 'no-cache',
        },
      })
      .pipe(
        map((response) => {
          return new MapsetUpload().deserialize(response);
        }),
      );
  }
}

@Injectable({
  providedIn: 'root',
})
export class MapsetArcgisUploadService {
  // API URL
  apiUrl = `${environment.backendURL}mapset/arcgis_upload`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  private http = inject(HttpClient);

  uploadFile(item: any): Observable<any> {
    return this.http
      .post(this.apiUrl, item, {
        reportProgress: true,
        observe: 'events',
        headers: {
          'Cache-Control': 'no-cache',
        },
      })
      .pipe(
        map((response) => {
          return new MapsetUpload().deserialize(response);
        }),
      );
  }
}
