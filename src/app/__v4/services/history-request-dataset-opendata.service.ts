import { HttpClient, HttpHeaders } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { catchError, map, tap } from 'rxjs/operators';
import { of, zip } from 'rxjs';
import { environment } from 'src/environments/environment';

// MODEL
import { HistoryRequestDatasetOpenData } from '@models-v4';
// SERVICE
import { PaginationService } from '@services-v3';
// UTIL
import { hasOwnProperty } from '@utils-v4';

import { assign } from 'lodash';
import { QueryClientService, UseQuery } from '@ngneat/query';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root',
})
export class HistoryRequestDatasetOpenDataService {
  title = 'History Request Dataset Open Data';

  // API URL
  apiUrl = `${environment.backendURL}history_request_public`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  private http = inject(HttpClient);
  private queryClient = inject(QueryClientService);
  private useQuery = inject(UseQuery);
  private paginationService = inject(PaginationService);
  private toastr = inject(ToastrService);

  modifyWhereParams(status: any, params: any) {
    if (status) {
      const where = JSON.parse(params.where);
      assign(where, { status: status.id });
      params.where = JSON.stringify(where);
    }

    return params;
  }

  // Total
  getTotal(params: any) {
    const merge = new URLSearchParams(params);

    const queryName = `Count ${this.title}`;

    return this.http
      .get<any>(`${this.apiUrl}?${merge}&count=true`, this.httpOptions)
      .pipe(
        map((response) => {
          if (response.error === 1 && response.message !== 'Data not found') {
            throw new Error(response.message);
          }

          return new HistoryRequestDatasetOpenData().deserialize(response);
        }),
        map((response) => {
          return response.data.count;
        }),
        catchError((e) => {
          this.toastr.error(e.message, queryName);
          return of(null);
        }),
      );
  }

  getQueryTotal(params: any, options: any = {}) {
    return this.useQuery(
      ['countHistoryRequestDatasetOpenData'],
      () => {
        return this.getTotal(params);
      },
      options,
    );
  }

  // List
  getList(params: any, options: any) {
    const merge = new URLSearchParams(params);

    const queryName = `List ${this.title}`;

    // TODO: remove if meta exist
    const getTotal = this.getTotal(params);
    const getList = this.http.get<any>(
      `${this.apiUrl}?${merge}`,
      this.httpOptions,
    );

    return zip(getTotal, getList).pipe(
      map(([total, list]) => {
        if (list.error === 1 && list.message !== 'Data not found') {
          throw new Error(list.message);
        }

        return [total, new HistoryRequestDatasetOpenData().deserialize(list)];
      }),
      map(([total, list]) => {
        const hasPagination = hasOwnProperty(options, 'pagination');
        const hasInfinite = hasOwnProperty(options, 'infinite');

        const isPagination = hasPagination ? options.pagination : true;
        const isInfinite = hasInfinite ? options.infinite : false;

        let pagination = { empty: true, infinite: isInfinite };
        if (isPagination) {
          const resultPagination = this.paginationService.getPagination(
            total,
            params.skip,
            params.limit,
          );

          pagination = {
            empty: false,
            infinite: isInfinite,
            ...resultPagination,
          };
        }

        return {
          pagination,
          list: list.data,
        };
      }),
      catchError((e) => {
        this.toastr.error(e.message, queryName);
        return of(null);
      }),
    );

    // return this.http.get<any>(`${this.apiUrl}?${merge}`, this.httpOptions).pipe(
    //   map((response) => {
    //     if (response.error === 1) {
    //       throw new Error(response.message);
    //     }

    //     return new HistoryRequestDatasetOpenData().deserialize(response);
    //   }),
    //   map((response) => {
    //     const hasPagination = hasOwnProperty(options, 'pagination');
    //     const hasInfinite = hasOwnProperty(options, 'infinite');

    //     const isPagination = hasPagination ? options.pagination : true;
    //     const isInfinite = hasInfinite ? options.infinite : false;

    //     let pagination = { empty: true, infinite: isInfinite };
    //     if (isPagination) {
    //       const resultPagination = this.paginationService.getPagination(
    //         response.meta.total_record,
    //         params.skip,
    //         params.limit,
    //       );

    //       pagination = {
    //         empty: false,
    //         infinite: isInfinite,
    //         ...resultPagination,
    //       };
    //     }

    //     return {
    //       pagination,
    //       list: response.data,
    //     };
    //   }),
    //   catchError((e) => {
    //     this.toastr.error(e.message, queryName);
    //     return of(null);
    //   }),
    // );
  }

  getQueryList(uniq: any, params: any, options: any = {}) {
    return this.useQuery(
      ['historyRequestDatasetOpenData', uniq],
      () => {
        return this.getList(params, options);
      },
      options,
    );
  }

  // Create
  createItem(item: any) {
    const queryName = `Create ${this.title}`;

    return this.http.post<any>(this.apiUrl, item).pipe(
      tap(() => {
        this.queryClient.invalidateQueries(['historyRequestDatasetOpenData']);
      }),
      catchError((e) => {
        this.toastr.error(e.message, queryName);
        return of(null);
      }),
    );
  }
}
