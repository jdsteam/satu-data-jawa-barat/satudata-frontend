import { HttpClient, HttpHeaders } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { environment } from 'src/environments/environment';

import { QueryClientService, UseQuery } from '@ngneat/query';

@Injectable({
  providedIn: 'root',
})
export class MapsetGeonetworkMetadataService {
  title = 'Mapset Geonetwork Metadata';

  // API URL
  apiUrl = `${environment.geonetworkURL}srv/api/search/records/_search`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  private http = inject(HttpClient);
  private queryClient = inject(QueryClientService);
  private useQuery = inject(UseQuery);

  // List
  getList(params: any, item: any) {
    const merge = new URLSearchParams(params);

    return this.http
      .post<any>(`${this.apiUrl}?${merge}`, item, this.httpOptions)
      .pipe(
        map((response) => {
          const records = response.hits.hits;

          const data = records
            .map((record: any) => {
              const source = record._source;
              return {
                url: `${environment.geonetworkURL}srv/api/records/${source.metadataIdentifier}/formatters/xml`,
                title: source.resourceTitleObject.default,
              };
            })
            .sort((a, b) => a.title.localeCompare(b.title));

          return {
            list: data,
          };
        }),
        catchError((e) => {
          return throwError(() => e);
        }),
      );
  }

  getQueryList(params: any, item: any) {
    return this.useQuery(['mapsetGeonetworkMetadata'], () => {
      return this.getList(params, item);
    });
  }
}
