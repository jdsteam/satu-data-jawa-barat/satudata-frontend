import { HttpClient, HttpHeaders } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { catchError, tap } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { environment } from 'src/environments/environment';

import { QueryClientService } from '@ngneat/query';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root',
})
export class VisualizationMetadataService {
  title = 'Visualization Metadata';

  // API URL
  apiUrl = `${environment.backendURL}metadata_visualization`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  private http = inject(HttpClient);
  private queryClient = inject(QueryClientService);
  private toastr = inject(ToastrService);

  // List
  getList(params: any) {
    const merge = new URLSearchParams(params);

    const queryName = `List ${this.title}`;

    return this.http.get<any>(`${this.apiUrl}?${merge}`, this.httpOptions).pipe(
      catchError((e) => {
        this.toastr.error(e.message, queryName);
        return throwError(() => e);
      }),
    );
  }

  // Create
  createItem(item: any) {
    return this.http.post<any>(this.apiUrl, item).pipe(
      tap(() => {
        this.queryClient.invalidateQueries(['visualizationMetadata']);
      }),
      catchError((e) => {
        this.toastr.error(e.message, 'Visualization Metadata');
        return throwError(() => e);
      }),
    );
  }

  // Update
  updateItem(uniq: any, item: any) {
    return this.http
      .put<any>(`${this.apiUrl}/${uniq}`, item, this.httpOptions)
      .pipe(
        tap(() => {
          this.queryClient.invalidateQueries(['visualizationMetadata']);
        }),
        catchError((e) => {
          this.toastr.error(e.message, 'Visualization Metadata');
          return throwError(() => e);
        }),
      );
  }

  updateItemBulk(item: any, params: any) {
    const merge = new URLSearchParams(params);

    const queryName = `Update Bulk ${this.title}`;

    return this.http
      .put<any>(`${this.apiUrl}?${merge}`, item, this.httpOptions)
      .pipe(
        tap(() => {
          this.queryClient.invalidateQueries(['visualizationMetadata']);
        }),
        catchError((e) => {
          this.toastr.error(e.message, queryName);
          return throwError(() => e);
        }),
      );
  }
}
