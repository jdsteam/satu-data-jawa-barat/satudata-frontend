import { HttpClient, HttpHeaders } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { catchError, map, tap } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { environment } from 'src/environments/environment';

// MODELS
import { Mapset } from '@models-v4';
// SERVICE
import { PaginationService } from '@services-v3';
// UTIL
import { hasOwnProperty } from '@utils-v4';

import { QueryClientService, UseQuery } from '@ngneat/query';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root',
})
export class MapsetService {
  title = 'Mapset';

  // API URL
  apiUrl = `${environment.backendURL}mapset`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  private http = inject(HttpClient);
  private queryClient = inject(QueryClientService);
  private useQuery = inject(UseQuery);
  private paginationService = inject(PaginationService);
  private toastr = inject(ToastrService);

  // List
  getList(params: any, options: any) {
    const merge = new URLSearchParams(params);

    const queryName = `List ${this.title}`;

    return this.http.get<any>(`${this.apiUrl}?${merge}`, this.httpOptions).pipe(
      map((response) => {
        if (response.error === 1 && response.message !== 'Data not found') {
          throw new Error(response.message);
        }

        return new Mapset().deserialize(response);
      }),
      map((response) => {
        const hasPagination = hasOwnProperty(options, 'pagination');
        const hasInfinite = hasOwnProperty(options, 'infinite');

        const isPagination = hasPagination ? options.pagination : true;
        const isInfinite = hasInfinite ? options.infinite : false;

        let pagination = { empty: true, infinite: isInfinite };
        if (isPagination) {
          const resultPagination = this.paginationService.getPagination(
            response.meta.total_record,
            params.skip,
            params.limit,
          );

          pagination = {
            empty: false,
            infinite: isInfinite,
            ...resultPagination,
          };
        }

        return {
          pagination,
          list: response.data,
        };
      }),
      catchError((e) => {
        this.toastr.error(e.message, queryName);
        return throwError(() => e);
      }),
    );
  }

  getQueryList(params: any, options: any = {}) {
    return this.useQuery(
      ['mapset'],
      () => {
        return this.getList(params, options);
      },
      options,
    );
  }

  // Single
  getSingle(uniq: number, params: any) {
    const merge = new URLSearchParams(params);

    const queryName = `Single ${this.title}`;

    return this.http
      .get<any>(`${this.apiUrl}/${uniq}?${merge}`, this.httpOptions)
      .pipe(
        map((response) => {
          if (response.error === 1) {
            if (response.message !== 'Data not found') {
              throw new Error(response.message);
            }
          }

          return new Mapset().deserialize(response);
        }),
        map((response) => {
          return response.data;
        }),
        catchError((e) => {
          this.toastr.error(e.message, queryName);
          return throwError(() => e);
        }),
      );
  }

  getQuerySingle(uniq: number, params: any, options: any = {}) {
    return this.useQuery(
      ['mapset', uniq],
      () => {
        return this.getSingle(uniq, params);
      },
      options,
    );
  }

  // Create
  createItem(item: any) {
    return this.http.post<any>(this.apiUrl, item).pipe(
      tap(() => {
        this.queryClient.invalidateQueries(['mapset']);
      }),
      catchError((e) => {
        this.toastr.error(e.message, 'Mapset');
        return throwError(() => e);
      }),
    );
  }

  // Update
  updateItem(uniq: any, item: any) {
    return this.http
      .put<any>(`${this.apiUrl}/${uniq}`, item, this.httpOptions)
      .pipe(
        tap(() => {
          this.queryClient.invalidateQueries(['mapset']);
        }),
        catchError((e) => {
          this.toastr.error(e.message, 'Mapset');
          return throwError(() => e);
        }),
      );
  }

  // Counter
  counter(uniq: any, params: any) {
    const merge = new URLSearchParams(params);
    return this.http
      .get<any>(`${this.apiUrl}/counter/${uniq}?${merge}`, this.httpOptions)
      .pipe(
        catchError((e) => {
          this.toastr.error(e.message, 'Mapset');
          return throwError(() => e);
        }),
      );
  }

  getStatusData(value: boolean | string) {
    let status: string;
    let name: string;
    let className: string;
    let color: string;
    let icon: string;

    if (value === 'true') {
      status = 'permanent';
      name = 'Tetap';
      className = 'permanent-status';
      color = 'green';
      icon = 'circle-check';
    } else {
      status = 'temporary';
      name = 'Sementara';
      className = 'temporary-status';
      color = 'yellow';
      icon = 'file-timer';
    }

    return {
      status,
      name,
      className,
      color,
      icon,
    };
  }
}
