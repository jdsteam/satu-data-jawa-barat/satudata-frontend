import { HttpClient, HttpHeaders } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { environment } from 'src/environments/environment';

// CONSTANT
import { APPLICATION } from '@constants-v4';
// MODELS
import { Topic } from '@models-v4';
// SERVICE
import { PaginationService } from '@services-v3';
// UTIL
import { hasOwnProperty } from '@utils-v4';

import { assign } from 'lodash';
import { UseQuery } from '@ngneat/query';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root',
})
export class TopicService {
  title = 'Topic';

  // API URL
  apiUrl = `${environment.backendURL}sektoral`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  private http = inject(HttpClient);
  private useQuery = inject(UseQuery);
  private paginationService = inject(PaginationService);
  private toastr = inject(ToastrService);

  modifyWhereParams(app: any, params: any) {
    if (app) {
      const where = JSON.parse(params.where);
      assign(where, { [app.paramWhere]: true });
      params.where = JSON.stringify(where);
    }

    return params;
  }

  // List
  getList(params: any, options: any, app = null) {
    const appData = APPLICATION.get(app);
    const newParams = this.modifyWhereParams(appData, params);
    const merge = new URLSearchParams(newParams);

    const queryName = appData
      ? `List ${this.title} ${appData.name}`
      : `List ${this.title}`;

    return this.http.get<any>(`${this.apiUrl}?${merge}`, this.httpOptions).pipe(
      map((response) => {
        if (response.error === 1 && response.message !== 'Data not found') {
          throw new Error(response.message);
        }

        return new Topic().deserialize(response);
      }),
      map((response) => {
        const hasPagination = hasOwnProperty(options, 'pagination');
        const hasInfinite = hasOwnProperty(options, 'infinite');

        const isPagination = hasPagination ? options.pagination : true;
        const isInfinite = hasInfinite ? options.infinite : false;

        let pagination = { empty: true, infinite: isInfinite };
        if (isPagination) {
          const resultPagination = this.paginationService.getPagination(
            response.meta.total_record,
            params.skip,
            params.limit,
          );

          pagination = {
            empty: false,
            infinite: isInfinite,
            ...resultPagination,
          };
        }

        return {
          pagination,
          list: response.data,
        };
      }),
      catchError((e) => {
        this.toastr.error(e.message, queryName);
        return throwError(() => e);
      }),
    );
  }

  getQueryList(params: any, options: any = {}, app = null) {
    const appData = APPLICATION.get(app);

    const queryKey = appData ? `topic${appData.alias}` : `topic`;

    return this.useQuery(
      [queryKey],
      () => {
        return this.getList(params, options, app);
      },
      options,
    );
  }
}
