import { HttpClient, HttpHeaders } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { catchError, map, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { environment } from 'src/environments/environment';

// MODELS
import { FeedbackInfo } from '@models-v4';
// SERVICE
import { PaginationService } from '@services-v3';

import { QueryClientService, UseQuery } from '@ngneat/query';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root',
})
export class FeedbackService {
  title = 'Feedback';

  // API URL
  apiUrl = `${environment.backendURL}feedback_private`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  private http = inject(HttpClient);
  private queryClient = inject(QueryClientService);
  private useQuery = inject(UseQuery);
  private paginationService = inject(PaginationService);
  private toastr = inject(ToastrService);

  // Create
  createItem(item: any) {
    const queryName = `Create ${this.title}`;

    return this.http.post<any>(this.apiUrl, item).pipe(
      tap(() => {
        this.queryClient.invalidateQueries(['feedback']);
      }),
      catchError((e) => {
        this.toastr.error(e.message, queryName);
        return of(null);
      }),
    );
  }

  // Info
  getQueryInfo(params: any, options: any = {}) {
    return this.useQuery(
      ['feedbackInfo'],
      () => {
        const merge = new URLSearchParams(params);

        const queryName = `Info ${this.title}`;

        return this.http
          .get<any>(`${this.apiUrl}/info?${merge}`, this.httpOptions)
          .pipe(
            map((response) => {
              if (
                response.error === 1 &&
                response.message !== 'Data not found'
              ) {
                throw new Error(response.message);
              }

              return new FeedbackInfo().deserialize(response);
            }),
            map((response) => {
              return {
                pagination: { empty: true, infinite: false },
                list: response.data,
              };
            }),
            catchError((e) => {
              this.toastr.error(e.message, queryName);
              return of(null);
            }),
          );
      },
      options,
    );
  }
}
