import { HttpClient, HttpHeaders } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { catchError, tap } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { environment } from 'src/environments/environment';

import { QueryClientService } from '@ngneat/query';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root',
})
export class IndicatorHistoryService {
  title = 'Indicator History';

  // API URL
  apiUrl = `${environment.backendURL}indikator_history`;

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  private http = inject(HttpClient);
  private queryClient = inject(QueryClientService);
  private toastr = inject(ToastrService);

  // Create
  createItem(item: any) {
    return this.http.post<any>(this.apiUrl, item).pipe(
      tap(() => {
        this.queryClient.invalidateQueries(['indicatorHistory']);
      }),
      catchError((e) => {
        this.toastr.error(e.message, 'Indicator History');
        return throwError(() => e);
      }),
    );
  }

  // Update
  updateItem(uniq: any, item: any) {
    return this.http
      .put<any>(`${this.apiUrl}/${uniq}`, item, this.httpOptions)
      .pipe(
        tap(() => {
          this.queryClient.invalidateQueries(['indicatorHistory']);
        }),
        catchError((e) => {
          this.toastr.error(e.message, 'Indicator History');
          return throwError(() => e);
        }),
      );
  }
}
