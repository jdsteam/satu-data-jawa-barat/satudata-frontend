import {
  AfterViewInit,
  Directive,
  ElementRef,
  Input,
  Optional,
} from '@angular/core';
import { StickyTableDirective } from './sticky-table.directive';

@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: '[stickyColumn]',
  standalone: true,
})
export class StickyColumnDirective implements AfterViewInit {
  @Input()
  leftMargin = 0;

  constructor(
    private el: ElementRef,
    @Optional() private table: StickyTableDirective,
  ) {}

  ngAfterViewInit() {
    const el = this.el.nativeElement as HTMLElement;
    const { x, width } = el.getBoundingClientRect();
    el.style.position = 'sticky';
    el.style.left = this.table
      ? `${x - this.table.x + this.leftMargin}px`
      : '0px';
    el.style.minWidth = `${width}px`;
    el.style.maxWidth = `${width}px`;
  }
}
