import { DOCUMENT } from '@angular/common';
import { Directive, ElementRef, Inject, Output } from '@angular/core';
import {
  distinctUntilChanged,
  map,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';
import { fromEvent } from 'rxjs';

@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: '[resizable]',
  standalone: true,
})
export class TableResizableDirective {
  @Output()
  readonly resizable = fromEvent<MouseEvent>(
    this.elementRef.nativeElement,
    'mousedown',
  ).pipe(
    tap((e) => e.preventDefault()),
    switchMap(() => {
      const { width, right } = this.elementRef.nativeElement
        .closest('th')
        .getBoundingClientRect();

      const table = this.elementRef.nativeElement.closest('table');
      const tableWidth = table.getBoundingClientRect().width;

      const parent = table.parentElement;
      const parentWidth = parent.getBoundingClientRect().width;

      return fromEvent<MouseEvent>(this.documentRef, 'mousemove').pipe(
        map(({ clientX }) => {
          if (tableWidth > parentWidth) {
            table.style.width = `${width + clientX - right + tableWidth}px`;
          }
          return width + clientX - right;
        }),
        distinctUntilChanged(),
        takeUntil(fromEvent(this.documentRef, 'mouseup')),
      );
    }),
  );

  constructor(
    @Inject(DOCUMENT) private readonly documentRef: Document,
    @Inject(ElementRef)
    private readonly elementRef: ElementRef<HTMLElement>,
  ) {}
}
