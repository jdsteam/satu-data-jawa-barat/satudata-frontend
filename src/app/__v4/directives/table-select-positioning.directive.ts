import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: '[positioning]',
  standalone: true,
})
export class TableSelectPositioningDirective {
  @Input() positioning: ElementRef<HTMLElement>;
  @Input() isLast = false;
  @Input() readonly = false;

  @HostListener('click', ['$event']) onClick($event) {
    if (!this.isLast || this.readonly) {
      $event.stopPropagation();
      $event.preventDefault();
      $event.stopImmediatePropagation();
    } else {
      const panel =
        this.positioning.nativeElement.querySelector<HTMLElement>(
          'ng-dropdown-panel',
        );
      panel.style.left = 'auto';
      panel.style.right = '16px';
    }
  }
}
