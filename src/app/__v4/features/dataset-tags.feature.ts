import { inject } from '@angular/core';
import {
  patchState,
  signalStoreFeature,
  withMethods,
  withState,
} from '@ngrx/signals';
import { switchMap } from 'rxjs';
import { rxMethod } from '@ngrx/signals/rxjs-interop';
import { tapResponse } from '@ngrx/operators';

// SERVICE
import { DatasetTagsService } from '@services-v4';

type DatasetTagsState = {
  listDatasetTags: any;
};

const initialState: DatasetTagsState = {
  listDatasetTags: null,
};

export function withDatasetTags() {
  return signalStoreFeature(
    withState<DatasetTagsState>(initialState),
    withMethods((store, datasetTagsService = inject(DatasetTagsService)) => ({
      getAllDatasetTags: rxMethod<void>(
        switchMap(() => {
          const params = {
            sort: `tag:asc`,
            distinct: true,
          };

          const options = {
            pagination: false,
          };

          return datasetTagsService.getQueryList(params, options).result$.pipe(
            tapResponse({
              next: (result) => {
                patchState(store, { listDatasetTags: result });
              },
              error: console.error,
            }),
          );
        }),
      ),
    })),
  );
}
