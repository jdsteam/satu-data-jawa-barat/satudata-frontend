import { inject } from '@angular/core';
import {
  patchState,
  signalStoreFeature,
  withMethods,
  withState,
} from '@ngrx/signals';
import { switchMap } from 'rxjs';
import { rxMethod } from '@ngrx/signals/rxjs-interop';
import { tapResponse } from '@ngrx/operators';

// SERVICE
import { TopicService } from '@services-v4';

type TopicSatuPetaState = {
  listTopicSatuPeta: any;
};

const initialState: TopicSatuPetaState = {
  listTopicSatuPeta: null,
};

export function withTopicSatuPeta() {
  return signalStoreFeature(
    withState<TopicSatuPetaState>(initialState),
    withMethods((store, topicService = inject(TopicService)) => ({
      getAllTopicSatuPeta: rxMethod<void>(
        switchMap(() => {
          const params = {
            sort: `name:asc`,
            where: JSON.stringify({
              is_deleted: false,
            }),
          };

          const options = {
            pagination: false,
          };
          const app = 'satupeta';

          return topicService.getQueryList(params, options, app).result$.pipe(
            tapResponse({
              next: (result) => {
                patchState(store, { listTopicSatuPeta: result });
              },
              error: console.error,
            }),
          );
        }),
      ),
    })),
  );
}
