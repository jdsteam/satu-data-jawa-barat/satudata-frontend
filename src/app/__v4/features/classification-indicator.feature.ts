import { inject } from '@angular/core';
import {
  patchState,
  signalStoreFeature,
  withMethods,
  withState,
} from '@ngrx/signals';
import { switchMap } from 'rxjs';
import { rxMethod } from '@ngrx/signals/rxjs-interop';
import { tapResponse } from '@ngrx/operators';

// SERVICE
import { ClassificationIndicatorService } from '@services-v4';

type ClassificationIndicatorState = {
  listClassificationIndicator: any;
};

const initialState: ClassificationIndicatorState = {
  listClassificationIndicator: null,
};

export function withClassificationIndicator() {
  return signalStoreFeature(
    withState<ClassificationIndicatorState>(initialState),
    withMethods(
      (
        store,
        classificationIndicatorService = inject(ClassificationIndicatorService),
      ) => ({
        getAllClassificationIndicator: rxMethod<void>(
          switchMap(() => {
            const params = {
              sort: `name:asc`,
            };

            const options = {
              pagination: false,
            };

            return classificationIndicatorService
              .getQueryList(params, options)
              .result$.pipe(
                tapResponse({
                  next: (result) => {
                    patchState(store, { listClassificationIndicator: result });
                  },
                  error: console.error,
                }),
              );
          }),
        ),
      }),
    ),
  );
}
