import { inject } from '@angular/core';
import {
  patchState,
  signalStoreFeature,
  withMethods,
  withState,
} from '@ngrx/signals';
import { switchMap } from 'rxjs';
import { rxMethod } from '@ngrx/signals/rxjs-interop';
import { tapResponse } from '@ngrx/operators';

// SERVICE
import { OrganizationService } from '@services-v4';

type OrganizationSatuDataState = {
  listOrganizationSatuData: any;
};

const initialState: OrganizationSatuDataState = {
  listOrganizationSatuData: null,
};

export function withOrganizationSatuData() {
  return signalStoreFeature(
    withState<OrganizationSatuDataState>(initialState),
    withMethods((store, organizationService = inject(OrganizationService)) => ({
      getAllOrganizationSatuData: rxMethod<void>(
        switchMap(() => {
          const params = {
            sort: `nama_skpd:asc`,
            where: JSON.stringify({
              is_deleted: false,
            }),
          };

          const options = {
            pagination: false,
          };
          const app = 'satudata';

          return organizationService
            .getQueryList(params, options, app)
            .result$.pipe(
              tapResponse({
                next: (result) => {
                  patchState(store, { listOrganizationSatuData: result });
                },
                error: console.error,
              }),
            );
        }),
      ),
    })),
  );
}
