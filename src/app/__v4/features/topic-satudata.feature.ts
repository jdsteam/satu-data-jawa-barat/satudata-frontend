import { inject } from '@angular/core';
import {
  patchState,
  signalStoreFeature,
  withMethods,
  withState,
} from '@ngrx/signals';
import { switchMap } from 'rxjs';
import { rxMethod } from '@ngrx/signals/rxjs-interop';
import { tapResponse } from '@ngrx/operators';

// SERVICE
import { TopicService } from '@services-v4';

type TopicSatuDataState = {
  listTopicSatuData: any;
};

const initialState: TopicSatuDataState = {
  listTopicSatuData: null,
};

export function withTopicSatuData() {
  return signalStoreFeature(
    withState<TopicSatuDataState>(initialState),
    withMethods((store, topicService = inject(TopicService)) => ({
      getAllTopicSatuData: rxMethod<void>(
        switchMap(() => {
          const params = {
            sort: `name:asc`,
            where: JSON.stringify({
              is_deleted: false,
            }),
          };

          const options = {
            pagination: false,
          };
          const app = 'satudata';

          return topicService.getQueryList(params, options, app).result$.pipe(
            tapResponse({
              next: (result) => {
                patchState(store, { listTopicSatuData: result });
              },
              error: console.error,
            }),
          );
        }),
      ),
    })),
  );
}
