import {
  patchState,
  signalStoreFeature,
  withMethods,
  withState,
} from '@ngrx/signals';
import { tap } from 'rxjs';
import { rxMethod } from '@ngrx/signals/rxjs-interop';

// CONSTANT
import { QUALITY_DATA } from '@constants-v4';

type DatasetQualityState = {
  listDatasetQuality: any;
};

const initialState: DatasetQualityState = {
  listDatasetQuality: null,
};

export function withDatasetQuality() {
  return signalStoreFeature(
    withState<DatasetQualityState>(initialState),
    withMethods((store) => ({
      getAllDatasetQuality: rxMethod<void>(
        tap(() => {
          const list = Array.from(QUALITY_DATA, ([, value]) => ({
            id: value.id,
            name: value.name,
            slug: value.slug,
          }));

          patchState(store, { listDatasetQuality: list });
        }),
      ),
    })),
  );
}
