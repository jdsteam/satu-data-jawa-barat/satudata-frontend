import { inject } from '@angular/core';
import {
  patchState,
  signalStoreFeature,
  withMethods,
  withState,
} from '@ngrx/signals';
import { switchMap } from 'rxjs';
import { rxMethod } from '@ngrx/signals/rxjs-interop';
import { tapResponse } from '@ngrx/operators';

// SERVICE
import { OrganizationService } from '@services-v4';

type OrganizationSatuPetaState = {
  listOrganizationSatuPeta: any;
};

const initialState: OrganizationSatuPetaState = {
  listOrganizationSatuPeta: null,
};

export function withOrganizationSatuPeta() {
  return signalStoreFeature(
    withState<OrganizationSatuPetaState>(initialState),
    withMethods((store, organizationService = inject(OrganizationService)) => ({
      getAllOrganizationSatuPeta: rxMethod<void>(
        switchMap(() => {
          const params = {
            sort: `nama_skpd:asc`,
            where: JSON.stringify({
              is_deleted: false,
            }),
          };

          const options = {
            pagination: false,
          };
          const app = 'satupeta';

          return organizationService
            .getQueryList(params, options, app)
            .result$.pipe(
              tapResponse({
                next: (result) => {
                  patchState(store, { listOrganizationSatuPeta: result });
                },
                error: console.error,
              }),
            );
        }),
      ),
    })),
  );
}
