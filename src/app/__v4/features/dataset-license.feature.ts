import { inject } from '@angular/core';
import {
  patchState,
  signalStoreFeature,
  withMethods,
  withState,
} from '@ngrx/signals';
import { switchMap } from 'rxjs';
import { rxMethod } from '@ngrx/signals/rxjs-interop';
import { tapResponse } from '@ngrx/operators';

// SERVICE
import { LicenseService } from '@services-v4';

type DatasetLicenseState = {
  listDatasetLicense: any;
};

const initialState: DatasetLicenseState = {
  listDatasetLicense: null,
};

export function withDatasetLicense() {
  return signalStoreFeature(
    withState<DatasetLicenseState>(initialState),
    withMethods((store, licenseService = inject(LicenseService)) => ({
      getAllDatasetLicense: rxMethod<void>(
        switchMap(() => {
          const params = {
            sort: `name:asc`,
          };

          const options = {
            pagination: false,
          };

          return licenseService.getQueryList(params, options).result$.pipe(
            tapResponse({
              next: (result) => {
                patchState(store, { listDatasetLicense: result });
              },
              error: console.error,
            }),
          );
        }),
      ),
    })),
  );
}
