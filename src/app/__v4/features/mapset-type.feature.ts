import { inject } from '@angular/core';
import {
  patchState,
  signalStoreFeature,
  withMethods,
  withState,
} from '@ngrx/signals';
import { switchMap } from 'rxjs';
import { rxMethod } from '@ngrx/signals/rxjs-interop';
import { tapResponse } from '@ngrx/operators';

// SERVICE
import { MapsetTypeService } from '@services-v4';

type MapsetTypeState = {
  listMapsetType: any;
};

const initialState: MapsetTypeState = {
  listMapsetType: null,
};

export function withMapsetType() {
  return signalStoreFeature(
    withState<MapsetTypeState>(initialState),
    withMethods((store, mapsetTypeService = inject(MapsetTypeService)) => ({
      getAllMapsetType: rxMethod<void>(
        switchMap(() => {
          const params = {
            sort: `name:asc`,
          };

          const options = {
            pagination: false,
          };

          return mapsetTypeService.getQueryList(params, options).result$.pipe(
            tapResponse({
              next: (result) => {
                patchState(store, { listMapsetType: result });
              },
              error: console.error,
            }),
          );
        }),
      ),
    })),
  );
}
