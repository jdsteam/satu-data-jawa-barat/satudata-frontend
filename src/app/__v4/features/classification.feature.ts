import { inject } from '@angular/core';
import {
  patchState,
  signalStoreFeature,
  withMethods,
  withState,
} from '@ngrx/signals';
import { switchMap } from 'rxjs';
import { rxMethod } from '@ngrx/signals/rxjs-interop';
import { tapResponse } from '@ngrx/operators';

// SERVICE
import { ClassificationService } from '@services-v4';

type ClassificationState = {
  listClassification: any;
};

const initialState: ClassificationState = {
  listClassification: null,
};

export function withClassification() {
  return signalStoreFeature(
    withState<ClassificationState>(initialState),
    withMethods(
      (store, classificationService = inject(ClassificationService)) => ({
        getAllClassification: rxMethod<void>(
          switchMap(() => {
            const params = {
              sort: `name:asc`,
            };

            const options = {
              pagination: false,
            };

            return classificationService
              .getQueryList(params, options)
              .result$.pipe(
                tapResponse({
                  next: (result) => {
                    patchState(store, { listClassification: result });
                  },
                  error: console.error,
                }),
              );
          }),
        ),
      }),
    ),
  );
}
