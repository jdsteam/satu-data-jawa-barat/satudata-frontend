import { inject } from '@angular/core';
import {
  patchState,
  signalStoreFeature,
  withMethods,
  withState,
} from '@ngrx/signals';
import { switchMap } from 'rxjs';
import { rxMethod } from '@ngrx/signals/rxjs-interop';
import { tapResponse } from '@ngrx/operators';

// SERVICE
import { CategoryIndicatorService } from '@services-v4';

type IndicatorCategoryState = {
  listIndicatorCategory: any;
};

const initialState: IndicatorCategoryState = {
  listIndicatorCategory: null,
};

export function withIndicatorCategory() {
  return signalStoreFeature(
    withState<IndicatorCategoryState>(initialState),
    withMethods(
      (store, indicatorCategoryService = inject(CategoryIndicatorService)) => ({
        getAllIndicatorCategory: rxMethod<void>(
          switchMap(() => {
            const params = {
              sort: `name:asc`,
            };

            const options = {
              pagination: false,
            };

            return indicatorCategoryService
              .getQueryList(params, options)
              .result$.pipe(
                tapResponse({
                  next: (result) => {
                    patchState(store, { listIndicatorCategory: result });
                  },
                  error: console.error,
                }),
              );
          }),
        ),
      }),
    ),
  );
}
