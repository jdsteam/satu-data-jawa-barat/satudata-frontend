import { isNil } from 'lodash';

export const businessDays = (startDate, days) => {
  if (isNil(days)) {
    return;
  }
  if (!(startDate instanceof Date)) {
    return;
  }

  const dow = startDate.getDay();
  let daysToAdd = parseInt(days);

  // eslint-disable-next-line no-plusplus
  if (dow === 0) daysToAdd++;
  if (dow + daysToAdd >= 6) {
    const remainingWorkDays = daysToAdd - (5 - dow);
    daysToAdd += 2;
    if (remainingWorkDays > 5) {
      daysToAdd += 2 * Math.floor(remainingWorkDays / 5);
      if (remainingWorkDays % 5 === 0) daysToAdd -= 2;
    }
  }
  startDate.setDate(startDate.getDate() + daysToAdd);

  // eslint-disable-next-line consistent-return
  return startDate;
};
