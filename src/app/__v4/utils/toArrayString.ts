export const toArrayString = (obj: object) =>
  Object.entries(obj).map(([key, value]) => {
    if (Array.isArray(value) && value.length > 1) {
      return value.map((val) => `${key}=${val}`);
    }
    return `${key}=${value}`;
  });
