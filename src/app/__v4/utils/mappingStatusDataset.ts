import type { StatusDataset } from '@types-v4';

export function mappingStatusDataset(value: string): StatusDataset {
  let status: StatusDataset;
  switch (value) {
    case 'draft':
    case 'preview':
      status = 'draft-active';
      break;
    case 'waiting':
      status = 'new-active';
      break;
    case 'revision':
    case 'revision-new':
    case 'revision-edit':
      status = 'revision-active';
      break;
    case 'approve':
      status = 'approve-active';
      break;
    case 'archive':
      status = 'approve-archive';
      break;
    case 'edit':
      status = 'edit-active';
      break;
    default:
      status = value as StatusDataset;
      break;
  }

  return status;
}
