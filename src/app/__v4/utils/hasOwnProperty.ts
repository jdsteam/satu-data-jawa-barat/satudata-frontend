import { isArray } from 'lodash';

export function hasOwnProperty(data: any, value: string | string[]): boolean {
  if (isArray(value)) {
    return value.every((prop) =>
      Object.prototype.hasOwnProperty.call(data, prop),
    );
  }

  return Object.prototype.hasOwnProperty.call(data, value);
}
