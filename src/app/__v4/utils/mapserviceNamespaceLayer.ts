export function mapserviceNamespaceLayer(url: string): string {
  const parsedUrl = new URL(url);
  const searchParams = new URLSearchParams(parsedUrl.search);

  const namespace = parsedUrl.pathname.split('/')[2];
  const layer = searchParams.get('layers') || '';

  return `${namespace}:${layer}`;
}
