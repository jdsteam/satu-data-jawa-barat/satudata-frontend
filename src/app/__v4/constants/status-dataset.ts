import type { StatusCatalog } from '@interfaces-v4';
import type { StatusDataset } from '@types-v4';

export const STATUS_DATASET = new Map<StatusDataset, StatusCatalog>()
  .set('draft-active', {
    is_validate: 0,
    is_active: true,
    is_deleted: false,
  })
  .set('draft-archive', {
    is_validate: 0,
    is_active: false,
    is_deleted: false,
  })
  .set('draft-delete', {
    is_validate: 0,
    is_active: false,
    is_deleted: true,
  })
  .set('new-active', {
    is_validate: 1,
    is_active: true,
    is_deleted: false,
  })
  .set('new-archive', {
    is_validate: 1,
    is_active: false,
    is_deleted: false,
  })
  .set('new-delete', {
    is_validate: 1,
    is_active: false,
    is_deleted: true,
  })
  .set('revision-active', {
    is_validate: 2,
    is_active: true,
    is_deleted: false,
  })
  .set('revision-archive', {
    is_validate: 2,
    is_active: false,
    is_deleted: false,
  })
  .set('revision-delete', {
    is_validate: 2,
    is_active: false,
    is_deleted: true,
  })
  .set('approve-active', {
    is_validate: 3,
    is_active: true,
    is_deleted: false,
  })
  .set('approve-archive', {
    is_validate: 3,
    is_active: false,
    is_deleted: false,
  })
  .set('approve-delete', {
    is_validate: 3,
    is_active: false,
    is_deleted: true,
  })
  .set('edit-active', {
    is_validate: 4,
    is_active: true,
    is_deleted: false,
  })
  .set('edit-archive', {
    is_validate: 4,
    is_active: false,
    is_deleted: false,
  })
  .set('edit-delete', {
    is_validate: 4,
    is_active: false,
    is_deleted: true,
  })
  .set('delete-active', {
    is_validate: 5,
    is_active: true,
    is_deleted: false,
  })
  .set('delete-archive', {
    is_validate: 5,
    is_active: false,
    is_deleted: false,
  })
  .set('delete-delete', {
    is_validate: 5,
    is_active: false,
    is_deleted: true,
  })
  .set('discontinue-active', {
    is_validate: 6,
    is_active: true,
    is_deleted: false,
  })
  .set('discontinue-archive', {
    is_validate: 6,
    is_active: false,
    is_deleted: false,
  })
  .set('discontinue-delete', {
    is_validate: 6,
    is_active: false,
    is_deleted: true,
  });
