export const STATUS_E_WALIDATA_ASSIGN_ASSIGNMENT = new Map()
  .set('Ditugaskan', {
    id: 0,
    slug: 'assigned',
    alias: 'Assigned',
    name: 'Ditugaskan',
    color: 'blue',
    alert: null,
  })
  .set('Dikembalikan', {
    id: 1,
    slug: 'returned',
    alias: 'returned',
    name: 'Dikembalikan',
    color: 'purple',
    alert: null,
  })
  .set('Diterima', {
    id: 2,
    slug: 'approved',
    alias: 'Approved',
    name: 'Diterima',
    color: 'green',
    alert: null,
  })
  .set('Pengajuan Nonaktif', {
    id: 3,
    slug: 'inactive-submission',
    alias: 'Inactive Submission',
    name: 'Pengajuan Nonaktif',
    color: 'blue-gray',
    alert: null,
  })
  .set('Nonaktif', {
    id: 4,
    slug: 'not-active',
    alias: 'Not Active',
    name: 'Nonaktif',
    color: 'red',
    alert: null,
  });
