export const CLASSIFICATION_INDICATOR = new Map()
  .set(1, {
    id: 1,
    slug: 'LAKIP',
    name: 'LAKIP',
    bgColor: 'blue',
  })
  .set(2, {
    id: 2,
    slug: 'LKPD',
    name: 'LKPD',
    bgColor: 'green',
  })
  .set(3, {
    id: 3,
    slug: 'LKPJ',
    name: 'LKPJ',
    bgColor: 'purple',
  })
  .set(4, {
    id: 4,
    slug: 'RKPD',
    name: 'RKPD',
    bgColor: 'yellow',
  })
  .set(5, {
    id: 5,
    slug: 'RPJMD',
    name: 'RPJMD',
    bgColor: 'gray',
  })
  .set(6, {
    id: 6,
    slug: 'SDGs',
    name: 'SDGs',
    bgColor: 'pink',
  })
  .set(7, {
    id: 7,
    slug: 'RPD',
    name: 'RPD',
    bgColor: 'blue-gray',
  });
