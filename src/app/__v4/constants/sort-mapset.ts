export const SORT_MAPSET = new Map()
  .set('newest', {
    id: 'newest',
    slug: 'newest',
    name: 'Terbaru',
    paramSort: 'mdate:desc',
  })
  .set('alphabet', {
    id: 'alphabet',
    slug: 'alphabet',
    name: 'Abjad',
    paramSort: 'name:asc',
  })
  .set('see', {
    id: 'see',
    slug: 'see',
    name: 'Dilihat Terbanyak',
    paramSort: 'count_view:desc',
  })
  .set('access', {
    id: 'access',
    slug: 'access',
    name: 'Diunduh Terbanyak',
    paramSort: 'count_access:desc',
  });
