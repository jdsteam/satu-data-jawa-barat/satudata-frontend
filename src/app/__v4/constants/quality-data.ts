export const QUALITY_DATA = new Map()
  .set('very-good', {
    id: 'very-good',
    name: 'Sangat Baik',
    slug: 'very-good',
  })
  .set('good', {
    id: 'good',
    name: 'Baik',
    slug: 'good',
  })
  .set('enough', {
    id: 'enough',
    name: 'Cukup',
    slug: 'enough',
  })
  .set('need-repair', {
    id: 'need-repair',
    name: 'Perlu Diperbaiki',
    slug: 'need-repair',
  });
