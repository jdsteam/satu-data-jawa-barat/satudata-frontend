export const SORT_E_WALIDATA_ASSIGN = new Map()
  .set('code', {
    id: 'code',
    slug: 'code',
    name: 'Kode urusan',
    paramSort: 'kode_bidang_urusan:asc',
  })
  .set('highest', {
    id: 'highest',
    slug: 'highest',
    name: 'Uraian aktif tertinggi',
    paramSort: 'count_indikator:desc',
  })
  .set('lowest', {
    id: 'lowest',
    slug: 'lowest',
    name: 'Uraian aktif terendah',
    paramSort: 'count_indikator:asc',
  });
