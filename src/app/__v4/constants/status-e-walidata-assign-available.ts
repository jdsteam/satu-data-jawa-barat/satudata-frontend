export const STATUS_E_WALIDATA_ASSIGN_AVAILABLE = new Map()
  .set('Aktif', {
    id: 0,
    slug: 'active',
    alias: 'Active',
    name: 'Aktif',
    color: 'green',
    alert: null,
  })
  .set('Tidak Tersedia', {
    id: 1,
    slug: 'not-available',
    alias: 'Not Available',
    name: 'Tidak Tersedia',
    color: 'red',
    alert: null,
  })
  .set('', {
    id: 1,
    slug: 'not-available',
    alias: 'Not Available',
    name: 'Tidak Tersedia',
    color: 'red',
    alert: null,
  })
  .set('Tidak Aktif', {
    id: 2,
    slug: 'not-active',
    alias: 'Not Active',
    name: 'Tidak Aktif',
    color: 'gray',
    alert: null,
  });
