export const NOTIFICATION = new Map()
  .set('review-dataset', {
    id: 'review-dataset',
    slug: 'review-dataset',
    name: 'Review Dataset',
    nameNavbar: 'Ulasan <br /> Dataset',
    alias: 'ReviewDataset',
    paramWhere: 1,
  })
  .set('upload-change', {
    id: 'upload-change',
    slug: 'upload-change',
    name: 'Upload Change',
    nameNavbar: 'Permintaan <br /> Unggah & Ubah',
    alias: 'UploadChange',
    paramWhere: 2,
  })
  .set('request-dataset', {
    id: 'request-dataset',
    slug: 'request-dataset',
    name: 'Request Dataset',
    nameNavbar: 'Permohonan <br /> Dataset',
    alias: 'RequestDataset',
    paramWhere: [3, 4],
  })
  .set('e-walidata', {
    id: 'e-walidata',
    slug: 'e-walidata',
    name: 'E-Walidata',
    nameNavbar: 'E-Walidata',
    alias: 'EWalidata',
    paramWhere: 5,
  });
