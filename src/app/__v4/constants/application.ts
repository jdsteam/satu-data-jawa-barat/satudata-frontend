export const APPLICATION = new Map()
  .set('satudata', {
    id: 'SD',
    slug: 'satudata',
    name: 'Satu Data',
    alias: 'SatuData',
    paramWhere: 'is_satudata',
  })
  .set('opendata', {
    id: 'SD',
    slug: 'opendata',
    name: 'Open Data',
    alias: 'OpenData',
    paramWhere: 'is_opendata',
  })
  .set('satupeta', {
    id: 'SD',
    slug: 'satupeta',
    name: 'Satu Peta',
    alias: 'SatuPeta',
    paramWhere: 'is_satupeta',
  })
  .set('external', {
    id: 'EXT',
    slug: 'external',
    name: 'External',
    alias: 'External',
    paramWhere: 'is_external',
  });
