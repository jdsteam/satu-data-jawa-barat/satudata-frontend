export const CLASSIFICATION = new Map()
  .set(3, {
    id: 3,
    slug: 'public',
    name: 'Publik',
    bgColor: 'green',
  })
  .set(5, {
    id: 5,
    slug: 'internal',
    name: 'Internal',
    bgColor: 'blue-gray',
  })
  .set(4, {
    id: 4,
    slug: 'private',
    name: 'Dikecualikan',
    bgColor: 'red',
  });
