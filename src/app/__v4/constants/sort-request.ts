export const SORT_REQUEST = new Map()
  .set('newest', {
    id: 'newest',
    slug: 'newest',
    name: 'Terbaru',
    paramSort: 'mdate:desc',
  })
  .set('oldest', {
    id: 'oldest',
    slug: 'oldest',
    name: 'Terlama',
    paramSort: 'mdate:asc',
  })
  .set('ticket', {
    id: 'ticket',
    slug: 'ticket',
    name: 'No. Tiket',
    paramSort: 'id:desc',
  });
