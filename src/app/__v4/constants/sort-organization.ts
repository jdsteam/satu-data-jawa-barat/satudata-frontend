export const SORT_ORGANIZATION = new Map()
  .set('alphabet', {
    id: 'alphabet',
    slug: 'alphabet',
    name: 'Abjad',
    paramSort: 'nama_skpd:asc',
  })
  .set('dataset', {
    id: 'dataset',
    slug: 'dataset',
    name: 'Dataset Terbanyak',
    paramSort: 'count_dataset:desc',
  })
  .set('indicator', {
    id: 'indicator',
    slug: 'indicator',
    name: 'Indikator Terbanyak',
    paramSort: 'count_indikator:desc',
  })
  .set('visualization', {
    id: 'visualization',
    slug: 'visualization',
    name: 'Visualisasi Terbanyak',
    paramSort: 'count_visualization:desc',
  });
