export const STATUS_REQUEST_OPENDATA = new Map()
  .set('all', {
    id: 0,
    slug: 'all',
    alias: 'All',
    name: 'Semua',
    color: 'white',
    alert: null,
  })
  .set('submitted', {
    id: 1,
    slug: 'submitted',
    alias: 'Submitted',
    name: 'Diajukan',
    color: 'blue',
    alery: null,
  })
  .set('processed', {
    id: 2,
    slug: 'processed',
    alias: 'Processed',
    name: 'Diproses',
    color: 'purple',
    alert: 'Permohonan dataset diproses.',
  })
  .set('accommodated', {
    id: 4,
    slug: 'accommodated',
    alias: 'Accommodated',
    name: 'Diakomodir',
    color: 'green',
    alert: 'Permohonan dataset selesai diproses.',
  })
  .set('rejected', {
    id: 3,
    slug: 'rejected',
    alias: 'Rejected',
    name: 'Ditolak',
    color: 'red',
    alert: 'Permohonan dataset ditolak.',
  });
