export const SOURCE_MAPSET = new Map()
  .set('geoserver', {
    id: 11,
    slug: 'geoserver',
    name: 'Geo Server',
  })
  .set('arcgis', {
    id: 2,
    slug: 'arcgis',
    name: 'Arc GIS',
  });
