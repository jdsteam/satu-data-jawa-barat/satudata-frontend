export const SORT_DATASET = new Map()
  .set('newest', {
    id: 'newest',
    slug: 'newest',
    name: 'Terbaru',
    paramSort: 'mdate:desc',
  })
  .set('alphabet', {
    id: 'alphabet',
    slug: 'alphabet',
    name: 'Abjad',
    paramSort: 'name:asc',
  })
  .set('see', {
    id: 'see',
    slug: 'see',
    name: 'Dilihat Terbanyak',
    paramSort: 'count_view_private:desc',
  })
  .set('access', {
    id: 'access',
    slug: 'access',
    name: 'Diunduh Terbanyak',
    paramSort: 'count_access_private:desc',
  });
