export const STATUS_REQUEST_OPENDATA_REASON_DECLINE = new Map()
  .set('Data tidak tersedia', {
    id: 'Data tidak tersedia',
    name: 'Data tidak tersedia',
  })
  .set('Permohonan kurang jelas', {
    id: 'Permohonan kurang jelas',
    name: 'Permohonan kurang jelas',
  })
  .set('Diluar kewenangan pemerintah Provinsi Jawa Barat', {
    id: 'Diluar kewenangan pemerintah Provinsi Jawa Barat',
    name: 'Diluar kewenangan pemerintah Provinsi Jawa Barat',
  })
  .set('Data yang dimohonkan bersifat dikecualikan/terbatas', {
    id: 'Data yang dimohonkan bersifat dikecualikan/terbatas',
    name: 'Data yang dimohonkan bersifat dikecualikan/terbatas',
  });
