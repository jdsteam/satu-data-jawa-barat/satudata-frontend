export const CATALOG = new Map()
  .set('dataset', {
    id: 'dataset',
    slug: 'dataset',
    name: 'Dataset',
  })
  .set('mapset', {
    id: 'mapset',
    slug: 'mapset',
    name: 'Mapset',
  })
  .set('indicator', {
    id: 'indicator',
    slug: 'indicator',
    name: 'Indikator',
  })
  .set('visualization', {
    id: 'visualization',
    slug: 'visualization',
    name: 'Visualisasi',
  })
  .set('organization', {
    id: 'organization',
    slug: 'organization',
    name: 'Organisasi',
  });
