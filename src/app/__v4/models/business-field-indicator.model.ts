/* eslint-disable @typescript-eslint/no-use-before-define */
import { Deserializable } from '@models-v4';

import { isArray } from 'lodash';

export class BusinessFieldIndicator implements Deserializable {
  [x: string]: any;
  message: string;
  data: BusinessFieldIndicatorData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new BusinessFieldIndicatorData().deserialize(res),
      );
    } else {
      this.data = new BusinessFieldIndicatorData().deserialize(input.data);
    }

    return this;
  }
}

export class BusinessFieldIndicatorData implements Deserializable {
  [x: string]: any;
  status_assignment: string;
  id: number;
  id_assignment: number;
  kode_indikator: string;
  satuan: string;
  uraian_indikator: string;
  status: string;
  status_available: string;
  kode_bidang_urusan: string;
  nama_indikator: string;
  definisi_operasional: string;
  cuid: number;
  cdate: Date;
  muid: number;
  mdate: Date;
  history: any[];

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
