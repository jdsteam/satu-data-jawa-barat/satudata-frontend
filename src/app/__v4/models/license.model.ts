/* eslint-disable @typescript-eslint/no-use-before-define */
import { Deserializable } from '@models-v4';

import { isArray } from 'lodash';

export class License implements Deserializable {
  [x: string]: any;
  message: string;
  data: LicenseData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new LicenseData().deserialize(res),
      );
    } else {
      this.data = new LicenseData().deserialize(input.data);
    }

    return this;
  }
}

export class LicenseData implements Deserializable {
  [x: string]: any;
  id: number;
  name: string;
  notes: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
