/* eslint-disable @typescript-eslint/no-use-before-define */
import { Deserializable } from '@models-v4';
import { CLASSIFICATION } from '@constants-v4';

import { isArray } from 'lodash';

export class BidangUrusan implements Deserializable {
  [x: string]: any;
  message: string;
  data: BidangUrusanData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new BidangUrusanData().deserialize(res),
      );
    } else {
      this.data = new BidangUrusanData().deserialize(input.data);
    }

    return this;
  }
}

export class BidangUrusanData implements Deserializable {
  [x: string]: any;
  nama_bidang_urusan: string;
  kode_bidang_urusan: string;
  cdate: string;
  mdate: string;
  notes: string;
  id: number;
  cuid: number;
  muid: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }

  getClassification() {
    return CLASSIFICATION.get(this.dataset_class.id);
  }
}
