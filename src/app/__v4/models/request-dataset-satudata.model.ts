/* eslint-disable @typescript-eslint/no-use-before-define */
import { Deserializable } from '@models-v3';

import { isArray } from 'lodash';

export class RequestDatasetSatuData implements Deserializable {
  [x: string]: any;
  message: string;
  data: RequestDatasetSatuDataData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new RequestDatasetSatuDataData().deserialize(res),
      );
    } else {
      this.data = new RequestDatasetSatuDataData().deserialize(input.data);
    }

    return this;
  }
}

export class RequestDatasetSatuDataData implements Deserializable {
  [x: string]: any;
  id: number;
  user_id: number;
  nama: string;
  telp: string;
  email: string;
  judul_data: string;
  tau_skpd: boolean;
  nama_skpd: string;
  kebutuhan_data: string;
  tujuan_data: string;
  datetime: Date;
  status: number;
  notes: string;
  cdate: Date;
  mdate: Date;
  is_active: boolean;
  is_deleted: boolean;
  history: any;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
