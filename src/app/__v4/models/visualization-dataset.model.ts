/* eslint-disable @typescript-eslint/no-use-before-define */
import { DatasetData, DatasetTagsData, Deserializable } from '@models-v4';

import { isArray } from 'lodash';

export class VisualizationDataset implements Deserializable {
  [x: string]: any;
  message: string;
  data: VisualizationDatasetData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new VisualizationDatasetData().deserialize(res),
      );
    } else {
      this.data = new VisualizationDatasetData().deserialize(input.data);
    }

    return this;
  }
}

export class VisualizationDatasetData implements Deserializable {
  [x: string]: any;
  id: number;
  dataset_id: number;
  dataset: DatasetData;
  dataset_tag: DatasetTagsData;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
