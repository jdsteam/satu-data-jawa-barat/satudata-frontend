/* eslint-disable @typescript-eslint/no-use-before-define */
import {
  Deserializable,
  RegionalData,
  OrganizationData,
  OrganizationSubData,
} from '@models-v4';

// PLUGIN
import { isArray } from 'lodash';

export class OrganizationUnit implements Deserializable {
  [x: string]: any;
  message: string;
  data: OrganizationUnitData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new OrganizationUnitData().deserialize(res),
      );
    } else {
      this.data = new OrganizationUnitData().deserialize(input.data);
    }

    return this;
  }
}

export class OrganizationUnitData implements Deserializable {
  [x: string]: any;
  id: number;
  regional_id: number;
  skpd_id: number;
  skpdsub_id: number;
  kode_skpd: string;
  kode_skpdsub: string;
  kode_skpdunit: string;
  nama_skpd: string;
  nama_skpdsub: string;
  nama_skpdunit: string;
  notes: string;
  cdate: string;
  cuid: number;
  regional: RegionalData;
  skpd: OrganizationData;
  skpdsub: OrganizationSubData;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
