/* eslint-disable @typescript-eslint/no-use-before-define */
import { Deserializable } from '@models-v4';

import { isArray } from 'lodash';

export class MapsetUpload implements Deserializable {
  [x: string]: any;
  message: string;
  data: MapsetUploadData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new MapsetUploadData().deserialize(res),
      );
    } else {
      this.data = new MapsetUploadData().deserialize(input.data);
    }

    return this;
  }
}

export class MapsetUploadData implements Deserializable {
  [x: string]: any;
  mapset_wms_url: string;
  mapset_json_url: string;
  mapset_shp_url: string;
  metadata_kugi: any;
  shp_upload_url: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
