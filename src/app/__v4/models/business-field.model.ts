/* eslint-disable @typescript-eslint/no-use-before-define */
import { Deserializable } from '@models-v4';

import { isArray } from 'lodash';

export class BusinessField implements Deserializable {
  [x: string]: any;
  message: string;
  data: BusinessFieldData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new BusinessFieldData().deserialize(res),
      );
    } else {
      this.data = new BusinessFieldData().deserialize(input.data);
    }

    return this;
  }
}

export class BusinessFieldData implements Deserializable {
  [x: string]: any;
  nama_bidang_urusan: string;
  kode_bidang_urusan: string;
  cdate: string;
  mdate: string;
  notes: string;
  id: number;
  cuid: number;
  muid: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
