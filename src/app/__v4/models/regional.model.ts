/* eslint-disable @typescript-eslint/no-use-before-define */
import { Deserializable, RegionalLevel } from '@models-v4';

// PLUGIN
import { isArray } from 'lodash';

export class Regional implements Deserializable {
  [x: string]: any;
  message: string;
  data: RegionalData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new RegionalData().deserialize(res),
      );
    } else {
      this.data = new RegionalData().deserialize(input.data);
    }

    return this;
  }
}

export class RegionalData implements Deserializable {
  [x: string]: any;
  id: number;
  regional_level: RegionalLevel;
  kode_bps: string;
  nama_bps: string;
  kode_kemendagri: string;
  nama_kemendagri: string;
  image: string;
  notes: string;
  is_active: boolean;
  is_deleted: boolean;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
