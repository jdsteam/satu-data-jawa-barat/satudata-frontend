/* eslint-disable @typescript-eslint/no-use-before-define */
import { Deserializable } from '@models-v4';

import { isArray } from 'lodash';

export class User implements Deserializable {
  [x: string]: any;
  message: string;
  data: UserData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) => new UserData().deserialize(res));
    } else {
      this.data = new UserData().deserialize(input.data);
    }

    return this;
  }
}

export class UserData implements Deserializable {
  [x: string]: any;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
