/* eslint-disable @typescript-eslint/no-use-before-define */
import { Deserializable } from '@models-v4';

import { isArray } from 'lodash';

export class HistoryCatalogStatus implements Deserializable {
  [x: string]: any;
  message: string;
  data: HistoryCatalogStatusData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new HistoryCatalogStatusData().deserialize(res),
      );
    } else {
      this.data = new HistoryCatalogStatusData().deserialize(input.data);
    }

    return this;
  }
}

export class HistoryCatalogStatusData implements Deserializable {
  [x: string]: any;
  id: number;
  type: string;
  type_id: number;
  category: string;
  notes: string;
  user_id: number;
  datetime: string;
  history_id: number;
  dataset_id: number;
  name: string;
  nama_skpd_alias: string;
  cdate: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
