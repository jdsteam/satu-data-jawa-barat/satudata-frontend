/* eslint-disable @typescript-eslint/no-use-before-define */
import { Deserializable, RegionalData } from '@models-v4';

import { isArray } from 'lodash';

export class Organization implements Deserializable {
  [x: string]: any;
  message: string;
  data: OrganizationData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new OrganizationData().deserialize(res),
      );
    } else {
      this.data = new OrganizationData().deserialize(input.data);
    }

    return this;
  }
}

export class OrganizationData implements Deserializable {
  [x: string]: any;
  id: number;
  regional_id: number;
  regional: RegionalData;
  kode_skpd: string;
  nama_skpd: string;
  nama_skpd_alias: string;
  description: string;
  phone: string;
  notes: string;
  address: string;
  email: string;
  logo: string;
  media_website: string;
  media_twitter: string;
  media_youtube: string;
  media_facebook: string;
  media_instagram: string;
  count_dataset: number;
  datecount: any;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
