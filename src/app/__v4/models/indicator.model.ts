/* eslint-disable @typescript-eslint/no-use-before-define */
import { Deserializable } from '@models-v4';

import { isArray } from 'lodash';

export class Indicator implements Deserializable {
  [x: string]: any;
  message: string;
  data: IndicatorData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new IndicatorData().deserialize(res),
      );
    } else {
      this.data = new IndicatorData().deserialize(input.data);
    }

    return this;
  }
}

export class IndicatorData implements Deserializable {
  [x: string]: any;
  nama_skpd: string;
  id_skpd: number;
  email: string;
  history_class: any;
  count_access: number;
  count_view: number;
  cdate: Date;
  nama_skpdsub: string;
  data_dasar: string;
  nama_skpd_alias: string;
  datetime: Date;
  history_class_string: string;
  dataset_type_id: number;
  satuan_name: string;
  indikator_category_id: number;
  id: number;
  logo: string;
  name: string;
  mdate: Date;
  satuan_id: number;
  rumus: string;
  is_active: boolean;
  kode_skpd: string;
  notes: null;
  is_deleted: boolean;
  category_name: string;
  description: string;
  title: string;
  phone: null;
  kode_skpd_sub: string;
  source: string;
  interpretation: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
