/* eslint-disable max-classes-per-file */
/* eslint-disable @typescript-eslint/no-use-before-define */
import { Deserializable } from '@models-v4';

export class MapsetGeoserverLayer implements Deserializable {
  layers: {
    layer: MapsetGeoserverLayerData[];
  };

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class MapsetGeoserverLayerData implements Deserializable {
  name: string;
  href: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class MapsetGeoserverWorkspace implements Deserializable {
  dataStores: {
    dataStore: MapsetGeoserverWorkspaceData[];
  };

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class MapsetGeoserverWorkspaceData implements Deserializable {
  name: string;
  href: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
