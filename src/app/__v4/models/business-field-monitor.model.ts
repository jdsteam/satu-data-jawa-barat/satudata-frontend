/* eslint-disable @typescript-eslint/no-use-before-define */
import { Deserializable } from '@models-v4';

import { isArray, isEmpty } from 'lodash';
import moment from 'moment';

export class BusinessFieldMonitor implements Deserializable {
  [x: string]: any;
  message: string;
  data: BusinessFieldMonitorData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new BusinessFieldMonitorData().deserialize(res),
      );
    } else {
      this.data = new BusinessFieldMonitorData().deserialize(input.data);
    }

    return this;
  }
}

export class BusinessFieldMonitorData implements Deserializable {
  [x: string]: any;
  id: number;
  kode_bidang_urusan: string;
  nama_bidang_urusan: string;
  tahun: string;
  progress: number;
  count_total: number;
  count_ditugaskan: number;
  count_dikembalikan: number;
  count_diterima: number;
  count_pengajuan_nonaktif: number;
  count_nonaktif: number;
  count_belum_ditugaskan: number;
  count_diisi: number;
  count_diisi_persentase: number;
  kode_skpd: string[];
  nama_skpd: string[];
  last_update: Date;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }

  getOrganization(): string {
    if (!isEmpty(this.nama_skpd)) {
      return this.nama_skpd.join(', ');
    }

    return '-';
  }

  getLastUpdate(): string {
    if (this.last_update) {
      return moment(this.last_update).locale('id').format('DD MMMM YYYY');
    }

    return '-';
  }
}
