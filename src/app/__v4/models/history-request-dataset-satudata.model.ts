/* eslint-disable @typescript-eslint/no-use-before-define */
import { Deserializable } from '@models-v4';

import { isArray } from 'lodash';

export class HistoryRequestDatasetSatuData implements Deserializable {
  [x: string]: any;
  message: string;
  data: HistoryRequestDatasetSatuDataData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new HistoryRequestDatasetSatuDataData().deserialize(res),
      );
    } else {
      this.data = new HistoryRequestDatasetSatuDataData().deserialize(
        input.data,
      );
    }

    return this;
  }
}

export class HistoryRequestDatasetSatuDataData implements Deserializable {
  [x: string]: any;
  request_private_id: number;
  status: number;
  id: number;
  notes: string;
  datetime: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
