/* eslint-disable @typescript-eslint/no-use-before-define */
import { Deserializable } from '@models-v4';

import { isArray } from 'lodash';

export class Unit implements Deserializable {
  [x: string]: any;
  message: string;
  data: UnitData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) => new UnitData().deserialize(res));
    } else {
      this.data = new UnitData().deserialize(input.data);
    }

    return this;
  }
}

export class UnitData implements Deserializable {
  [x: string]: any;
  id: number;
  name: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
