/* eslint-disable @typescript-eslint/no-use-before-define */
import { Deserializable } from '@models-v4';
import { CLASSIFICATION } from '@constants-v4';

import { isArray } from 'lodash';

export class EWalidataData implements Deserializable {
  [x: string]: any;
  message: string;
  data: EWalidataDataData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new EWalidataDataData().deserialize(res),
      );
    } else {
      this.data = new EWalidataDataData().deserialize(input.data);
    }

    return this;
  }
}

export class EWalidataDataData implements Deserializable {
  [x: string]: any;
  kode_indikator: string;
  kode_pemda: string;
  tahun: string;
  data: string;
  status_verifikasi_walidata: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }

  getClassification() {
    return CLASSIFICATION.get(this.dataset_class.id);
  }
}
