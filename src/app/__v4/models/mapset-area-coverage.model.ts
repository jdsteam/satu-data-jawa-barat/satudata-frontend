/* eslint-disable @typescript-eslint/no-use-before-define */
import { Deserializable } from '@models-v4';

// PLUGIN
import { isArray } from 'lodash';

export class MapsetAreaCoverage implements Deserializable {
  [x: string]: any;
  message: string;
  data: MapsetAreaCoverageData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new MapsetAreaCoverageData().deserialize(res),
      );
    } else {
      this.data = new MapsetAreaCoverageData().deserialize(input.data);
    }

    return this;
  }
}

export class MapsetAreaCoverageData implements Deserializable {
  [x: string]: any;
  id: number;
  name: string;
  notes: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
