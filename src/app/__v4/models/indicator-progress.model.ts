import { Deserializable } from '@models-v3';

export class IndicatorProgress implements Deserializable {
  [x: string]: any;
  message: string;
  data: IndicatorProgressData;
  error: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class IndicatorProgressData implements Deserializable {
  [x: string]: any;
  is_final: boolean;
  cuid: number;
  mdate: Date;
  target: string;
  capaian: string;
  id: number;
  indikator_id: number;
  year: number;
  notes: string;
  cdate: Date;
  muid: number;
  index: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
