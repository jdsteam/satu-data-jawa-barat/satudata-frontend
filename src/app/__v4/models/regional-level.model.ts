/* eslint-disable @typescript-eslint/no-use-before-define */
import { Deserializable } from '@models-v4';

// PLUGIN
import { isArray } from 'lodash';

export class RegionalLevel implements Deserializable {
  [x: string]: any;
  message: string;
  data: RegionalLevelData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new RegionalLevelData().deserialize(res),
      );
    } else {
      this.data = new RegionalLevelData().deserialize(input.data);
    }

    return this;
  }
}

export class RegionalLevelData implements Deserializable {
  [x: string]: any;
  id: number;
  name: string;
  notes: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
