/* eslint-disable @typescript-eslint/no-use-before-define */
import { Deserializable } from '@models-v4';

import { isArray } from 'lodash';

export class ClassificationIndicator implements Deserializable {
  [x: string]: any;
  message: string;
  data: ClassificationIndicatorData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new ClassificationIndicatorData().deserialize(res),
      );
    } else {
      this.data = new ClassificationIndicatorData().deserialize(input.data);
    }

    return this;
  }
}

export class ClassificationIndicatorData implements Deserializable {
  [x: string]: any;
  id: number;
  name: string;
  notes: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
