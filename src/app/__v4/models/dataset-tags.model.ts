/* eslint-disable @typescript-eslint/no-use-before-define */
import { Deserializable } from '@models-v4';

import { isArray } from 'lodash';

export class DatasetTags implements Deserializable {
  [x: string]: any;
  message: string;
  data: DatasetTagsData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new DatasetTagsData().deserialize(res),
      );
    } else {
      this.data = new DatasetTagsData().deserialize(input.data);
    }

    return this;
  }
}

export class DatasetTagsData implements Deserializable {
  [x: string]: any;
  id: number;
  dataset_id: number;
  tag: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
