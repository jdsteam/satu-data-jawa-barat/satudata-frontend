/* eslint-disable @typescript-eslint/no-use-before-define */
import { Deserializable } from '@models-v4';
import { CLASSIFICATION } from '@constants-v4';

import { isArray } from 'lodash';
import { EWalidataData } from './ewalidata-data.model';

export class IndikatorBidangUrusan implements Deserializable {
  [x: string]: any;
  message: string;
  data: IndikatorBidangUrusanData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new IndikatorBidangUrusanData().deserialize(res),
      );
    } else {
      this.data = new IndikatorBidangUrusanData().deserialize(input.data);
    }

    return this;
  }
}

export class IndikatorBidangUrusanData implements Deserializable {
  [x: string]: any;
  id: 2194;
  nama_indikator: string;
  definisi_operasional: string;
  status: string;
  cuid: string;
  satuan: string;
  kode_indikator: string;
  kode_bidang_urusan: string;
  uraian_indikator: string;
  cdate: string;
  ewalidata_data: EWalidataData[];

  deserialize(input: any): this {
    return Object.assign(this, input);
  }

  getClassification() {
    return CLASSIFICATION.get(this.dataset_class.id);
  }
}
