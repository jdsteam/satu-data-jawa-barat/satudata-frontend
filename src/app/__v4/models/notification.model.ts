/* eslint-disable @typescript-eslint/no-use-before-define */
import { Deserializable } from '@models-v4';

import { isArray } from 'lodash';

export class Notification implements Deserializable {
  [x: string]: any;
  message: string;
  data: NotificationData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new NotificationData().deserialize(res),
      );
    } else {
      this.data = new NotificationData().deserialize(input.data);
    }

    return this;
  }
}

export class NotificationData implements Deserializable {
  [x: string]: any;
  id: number;
  type: string;
  type_id: number;
  sender: number;
  reciver: number;
  title: string;
  content: string;
  is_read: boolean;
  cdate: Date;
  mdate: Date;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
