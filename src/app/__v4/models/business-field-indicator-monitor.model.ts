/* eslint-disable @typescript-eslint/no-use-before-define */
import { Deserializable } from '@models-v4';

import { isArray } from 'lodash';

export class BusinessFieldIndicatorMonitor implements Deserializable {
  [x: string]: any;
  message: string;
  data: BusinessFieldIndicatorMonitorData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new BusinessFieldIndicatorMonitorData().deserialize(res),
      );
    } else {
      this.data = new BusinessFieldIndicatorMonitorData().deserialize(
        input.data,
      );
    }

    return this;
  }
}

export class BusinessFieldIndicatorMonitorData implements Deserializable {
  [x: string]: any;
  id: number;
  kode_bidang_urusan: string;
  nama_bidang_urusan: string;
  kode_indikator: string;
  nama_indikator: string;
  uraian_indikator: string;
  definisi_operasional: string;
  status: string;
  satuan: string;
  kode_skpd: string;
  nama_skpd: string;
  cuid: number;
  cdate: Date;
  ewalidata_data: any[];

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
