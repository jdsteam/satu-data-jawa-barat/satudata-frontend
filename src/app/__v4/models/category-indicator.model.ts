/* eslint-disable @typescript-eslint/no-use-before-define */
import { Deserializable } from '@models-v4';

import { isArray } from 'lodash';

export class CategoryIndicator implements Deserializable {
  [x: string]: any;
  message: string;
  data: CategoryIndicatorData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new CategoryIndicatorData().deserialize(res),
      );
    } else {
      this.data = new CategoryIndicatorData().deserialize(input.data);
    }

    return this;
  }
}

export class CategoryIndicatorData implements Deserializable {
  [x: string]: any;
  id: number;
  name: string;
  notes: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
