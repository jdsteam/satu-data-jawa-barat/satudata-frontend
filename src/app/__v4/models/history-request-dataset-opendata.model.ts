/* eslint-disable @typescript-eslint/no-use-before-define */
import { Deserializable } from '@models-v4';

import { isArray } from 'lodash';

export class HistoryRequestDatasetOpenData implements Deserializable {
  [x: string]: any;
  message: string;
  data: HistoryRequestDatasetOpenDataData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new HistoryRequestDatasetOpenDataData().deserialize(res),
      );
    } else {
      this.data = new HistoryRequestDatasetOpenDataData().deserialize(
        input.data,
      );
    }

    return this;
  }
}

export class HistoryRequestDatasetOpenDataData implements Deserializable {
  [x: string]: any;
  request_private_id: number;
  status: number;
  id: number;
  notes: string;
  datetime: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
