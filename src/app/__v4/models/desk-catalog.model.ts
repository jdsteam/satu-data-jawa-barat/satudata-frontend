/* eslint-disable @typescript-eslint/no-use-before-define */
import { Deserializable } from '@models-v4';

import { isArray } from 'lodash';

export class DeskCatalog implements Deserializable {
  [x: string]: any;
  message: string;
  data: DeskCatalogData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new DeskCatalogData().deserialize(res),
      );
    } else {
      this.data = new DeskCatalogData().deserialize(input.data);
    }

    return this;
  }
}

export class DeskCatalogData implements Deserializable {
  [x: string]: any;
  id: string;
  type: string;
  type_id: number;
  name: string;
  kode_skpd: string;
  nama_skpd: string;
  nama_skpd_alias: string;
  cuid: number;
  username: string;
  cdate: string;
  mdate: string;
  datetime: string;
  is_active: boolean;
  is_deleted: boolean;
  is_validate: boolean;
  history_draft: any;
  history_drafts: any;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
