/* eslint-disable @typescript-eslint/no-use-before-define */
import { Deserializable, RegionalData, OrganizationData } from '@models-v4';

// PLUGIN
import { isArray } from 'lodash';

export class OrganizationSub implements Deserializable {
  [x: string]: any;
  message: string;
  data: OrganizationSubData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new OrganizationSubData().deserialize(res),
      );
    } else {
      this.data = new OrganizationSubData().deserialize(input.data);
    }

    return this;
  }
}

export class OrganizationSubData implements Deserializable {
  [x: string]: any;
  id: number;
  regional_id: number;
  skpd_id: number;
  kode_skpd: string;
  kode_skpdsub: string;
  nama_skpd: string;
  nama_skpdsub: string;
  notes: string;
  cdate: string;
  cuid: number;
  regional: RegionalData;
  skpd: OrganizationData;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
