/* eslint-disable @typescript-eslint/no-use-before-define */
import { Deserializable } from '@models-v4';

import { isArray } from 'lodash';

export class BusinessFieldProgress implements Deserializable {
  [x: string]: any;
  message: string;
  data: BusinessFieldProgressData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new BusinessFieldProgressData().deserialize(res),
      );
    } else {
      this.data = new BusinessFieldProgressData().deserialize(input.data);
    }

    return this;
  }
}

export class BusinessFieldProgressData implements Deserializable {
  [x: string]: any;
  id: number;
  kode_bidang_urusan: string;
  nama_bidang_urusan: string;
  nama_skpd: string;
  notes: string;
  progress: number;
  cdate: Date;
  mdate: Date;
  last_update: Date;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
