/* eslint-disable @typescript-eslint/no-use-before-define */
import { Deserializable } from '@models-v4';

import { isArray } from 'lodash';

export class IndicatorDataset implements Deserializable {
  [x: string]: any;
  message: string;
  data: IndicatorDatasetData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new IndicatorDatasetData().deserialize(res),
      );
    } else {
      this.data = new IndicatorDatasetData().deserialize(input.data);
    }

    return this;
  }
}

export class IndicatorDatasetData implements Deserializable {
  [x: string]: any;
  id: number;
  name: string;
  is_active: boolean;
  is_deleted: boolean;
  is_validate: number;
  validate: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
