/* eslint-disable @typescript-eslint/no-use-before-define */
import {
  Deserializable,
  ClassificationData,
  TopicData,
  MapsetTypeData,
  OrganizationData,
} from '@models-v4';
import { CLASSIFICATION } from '@constants-v4';

import { isArray } from 'lodash';

export class Mapset implements Deserializable {
  [x: string]: any;
  message: string;
  data: MapsetData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new MapsetData().deserialize(res),
      );
    } else {
      this.data = new MapsetData().deserialize(input.data);
    }

    return this;
  }
}

export class MapsetData implements Deserializable {
  [x: string]: any;
  dataset_class: ClassificationData;
  sektoral: TopicData;
  mapset_type: MapsetTypeData;
  skdp: OrganizationData;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }

  getClassification() {
    return CLASSIFICATION.get(this.dataset_class.id);
  }
}
