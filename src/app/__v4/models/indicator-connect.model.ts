/* eslint-disable @typescript-eslint/no-use-before-define */
import { Deserializable } from '@models-v4';

import { isArray } from 'lodash';

export class IndicatorConnect implements Deserializable {
  [x: string]: any;
  message: string;
  data: IndicatorConnectData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new IndicatorConnectData().deserialize(res),
      );
    } else {
      this.data = new IndicatorConnectData().deserialize(input.data);
    }

    return this;
  }
}

export class IndicatorConnectData implements Deserializable {
  [x: string]: any;
  id: number;
  name: string;
  is_active: boolean;
  is_deleted: boolean;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
