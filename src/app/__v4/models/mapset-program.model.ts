/* eslint-disable @typescript-eslint/no-use-before-define */
import { Deserializable } from '@models-v4';

// PLUGIN
import { isArray } from 'lodash';

export class MapsetProgram implements Deserializable {
  [x: string]: any;
  message: string;
  data: MapsetProgramData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new MapsetProgramData().deserialize(res),
      );
    } else {
      this.data = new MapsetProgramData().deserialize(input.data);
    }

    return this;
  }
}

export class MapsetProgramData implements Deserializable {
  [x: string]: any;
  id: number;
  name: string;
  picture: string;
  notes: string;
  is_active: boolean;
  is_deleted: boolean;
  cuid: number;
  cdate: Date;
  muid: number;
  mdate: Date;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
