/* eslint-disable @typescript-eslint/no-use-before-define */
import {
  Deserializable,
  HistoryCatalogStatusData,
  DatasetTagsData,
  TopicData,
  OrganizationData,
  OrganizationSubData,
  OrganizationUnitData,
  LicenseData,
  RegionalData,
  ClassificationData,
} from '@models-v4';
import { CLASSIFICATION } from '@constants-v4';
import { jdsBiColor } from '@jds-bi/cdk';

import { isArray } from 'lodash';

export class DatasetMeilisearch implements Deserializable {
  [x: string]: any;
  message: string;
  data: DatasetMeilisearchData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new DatasetMeilisearchData().deserialize(res),
      );
    } else {
      this.data = new DatasetMeilisearchData().deserialize(input.data);
    }

    return this;
  }
}

export class DatasetMeilisearchData implements Deserializable {
  [x: string]: any;
  id: number;
  dataset_type_id: number;
  dataset_class_id: number;
  regional_id: number;
  license_id: number;
  sektoral_id: number;
  kode_skpd: string;
  kode_skpdsub: string;
  kode_skpdunit: string;
  app_id: number;
  app_service_id: number;
  name: string;
  title: string;
  year: string;
  image: string;
  description: string;
  owner: string;
  owner_email: string;
  maintainer: string;
  maintainer_email: string;
  notes: string;
  is_active: boolean;
  is_deleted: boolean;
  is_permanent: boolean;
  is_validate: number;
  cdate: string;
  cuid: number;
  mdate: string;
  muid: number;
  count_column: number;
  count_row: number;
  count_view: number;
  count_access: number;
  count_rating: string;
  history_draft: HistoryCatalogStatusData;
  dataset_tags: DatasetTagsData;
  sektoral: TopicData;
  license: LicenseData;
  skpd: OrganizationData;
  skpdsub: OrganizationSubData;
  skpdunit: OrganizationUnitData;
  regional: RegionalData;
  dataset_class: ClassificationData;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }

  getClassification() {
    return CLASSIFICATION.get(this.dataset_class_id);
  }

  getDimension(): string {
    const start = this.metadata.find(
      (item) => item.key === 'Dimensi Dataset Awal',
    );
    const end = this.metadata.find(
      (item) => item.key === 'Dimensi Dataset Akhir',
    );
    const available = this.metadata.find(
      (item) => item.key === 'Tahun Tersedia Dataset',
    );

    let result: string;
    if (start && end) {
      result = `${start.value} - ${end.value}`;
    } else if (available) {
      result = available.value;
    } else {
      result = '-';
    }

    return result;
  }

  getStatus() {
    let status: string;
    let name: string;
    let className: string;
    let color: string;
    let icon: string;

    if (this.is_realtime) {
      status = 'realtime';
      name = 'Realtime';
      className = 'realtime-status';
      color = 'blue';
      icon = 'clock-rotate-left';
    } else {
      // eslint-disable-next-line no-lonely-if
      if (this.is_permanent) {
        status = 'permanent';
        name = 'Tetap';
        className = 'permanent-status';
        color = 'green';
        icon = 'circle-check';
      } else {
        status = 'temporary';
        name = 'Sementara';
        className = 'temporary-status';
        color = 'yellow';
        icon = 'file-timer';
      }
    }

    return {
      status,
      name,
      className,
      color,
      icon,
    };
  }

  getRating() {
    return Number(this.count_rating);
  }

  getQualityScoreColor() {
    let color: string;

    if (this.quality_score_point >= 90) {
      color = jdsBiColor.green['600'];
    } else if (this.quality_score_point > 75) {
      color = jdsBiColor.blue['600'];
    } else if (this.quality_score_point >= 60) {
      color = jdsBiColor.yellow['900'];
    } else if (this.quality_score_point < 60) {
      color = jdsBiColor.red['600'];
    } else {
      color = jdsBiColor.gray['800'];
    }

    return color;
  }
}
