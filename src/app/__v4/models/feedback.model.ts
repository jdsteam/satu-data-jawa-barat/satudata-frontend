/* eslint-disable max-classes-per-file */
/* eslint-disable @typescript-eslint/no-use-before-define */
import { Deserializable } from '@models-v4';

import { isArray } from 'lodash';

export class Feedback implements Deserializable {
  [x: string]: any;
  message: string;
  data: FeedbackData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new FeedbackData().deserialize(res),
      );
    } else {
      this.data = new FeedbackData().deserialize(input.data);
    }

    return this;
  }
}

export class FeedbackData implements Deserializable {
  [x: string]: any;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class FeedbackInfo implements Deserializable {
  [x: string]: any;
  message: string;
  data: FeedbackInfoData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new FeedbackInfoData().deserialize(res),
      );
    } else {
      this.data = new FeedbackInfoData().deserialize(input.data);
    }

    return this;
  }
}

export class FeedbackInfoData implements Deserializable {
  [x: string]: any;
  masalah: any;
  tujuan: any;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
