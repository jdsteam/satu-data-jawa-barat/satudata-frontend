/* eslint-disable @typescript-eslint/no-use-before-define */
import { Deserializable } from '@models-v4';

import { isArray } from 'lodash';

export class Topic implements Deserializable {
  [x: string]: any;
  message: string;
  data: TopicData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new TopicData().deserialize(res),
      );
    } else {
      this.data = new TopicData().deserialize(input.data);
    }

    return this;
  }
}

export class TopicData implements Deserializable {
  [x: string]: any;
  id: number;
  name: string;
  picture: string;
  notes: string;
  is_deleted: boolean;
  is_satudata: boolean;
  is_opendata: boolean;
  is_satupeta: boolean;
  picture_banner_1: string;
  picture_banner_2: string;
  picture_banner_3: string;
  picture_banner_4: string;
  picture_banner_5: string;
  picture_thumbnail: string;
  picture_thumbnail_1: string;
  picture_thumbnail_2: string;
  picture_thumbnail_3: string;
  picture_thumbnail_4: string;
  picture_thumbnail_5: string;
  picture_satupeta: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
