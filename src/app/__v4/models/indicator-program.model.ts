/* eslint-disable @typescript-eslint/no-use-before-define */
import { Deserializable } from '@models-v4';

import { isArray } from 'lodash';

export class IndicatorProgram implements Deserializable {
  [x: string]: any;
  message: string;
  data: IndicatorProgramData;
  error: number;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (isArray(input.data)) {
      this.data = input.data.map((res: any) =>
        new IndicatorProgramData().deserialize(res),
      );
    } else {
      this.data = new IndicatorProgramData().deserialize(input.data);
    }

    return this;
  }
}

export class IndicatorProgramData implements Deserializable {
  [x: string]: any;
  kode_program: string;
  nama_program: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
