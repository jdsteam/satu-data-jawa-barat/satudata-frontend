import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full',
      },
      {
        path: 'login',
        loadChildren: () =>
          import('./login-v2/login-v2.module').then((m) => m.LoginV2Module),
      },
      {
        path: 'autologin',
        loadChildren: () =>
          import('./autologin/autologin.module').then((m) => m.AutologinModule),
      },
      {
        path: 'autologin_sso',
        loadChildren: () =>
          import('./autologin-sso/autologin-sso.module').then(
            (m) => m.AutologinSSOModule,
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthenticationRoutingModule {}
