import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';

// SERVICES
import { AuthenticationService, StateService } from '@services-v3';

// COMPONENT
import { AgreementComponent as ModalAgreementComponent } from '@components-v3/modal/agreement/agreement.component';

@Component({
  selector: 'app-login',
  template: `
    <ngx-loading [show]="loading" />
    <app-modal-agreement></app-modal-agreement>
  `,
})
export class AutologinComponent {
  @ViewChild(ModalAgreementComponent)
  modalAgreementComponent: ModalAgreementComponent;

  // Variable
  loading = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private stateService: StateService,
  ) {
    const { username, authenticatorKey, anotationKey } =
      this.route.snapshot.queryParams;

    this.loading = true;
    this.authenticationService
      .verifyToken(username, authenticatorKey, anotationKey)
      .pipe(first())
      .subscribe(
        (result) => {
          this.loading = false;

          if (result.error === 0) {
            if (!result.data.is_agree) {
              this.stateService.changeAuthenticationLogin(result.data);
              this.authenticationService.setToken(result.data.jwt);
              this.modalAgreementComponent.openModal();
            } else {
              this.authenticationService.setSession(result.data);
              this.router.navigate(['/home']);
            }
          } else {
            this.router.navigate(['/auth/login'], {
              state: {
                errorType: 'access',
                error: 'Username dan Kata Sandi tidak ditemukan.',
              },
            });
          }
        },
        (error) => {
          this.loading = false;
          this.router.navigate(['/auth/login'], {
            state: { errorType: 'api', error: error.message },
          });
        },
      );
  }
}
