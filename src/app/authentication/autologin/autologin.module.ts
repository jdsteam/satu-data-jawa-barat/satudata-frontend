import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';

// MODULE
import { NgxLoadingModule } from 'ngx-loading';

import { AgreementComponent as ModalAgreementComponent } from '@components-v3/modal/agreement/agreement.component';

import { AutologinComponent } from './autologin.component';

export const routes: Routes = [
  {
    path: '',
    component: AutologinComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    NgxLoadingModule,
    ModalAgreementComponent,
  ],
  declarations: [AutologinComponent],
  bootstrap: [AutologinComponent],
})
export class AutologinModule {}
