import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';

// MODULE
import { NgxLoadingModule } from 'ngx-loading';

import { AgreementComponent as ModalAgreementComponent } from '@components-v3/modal/agreement/agreement.component';

import { AutologinSSOComponent } from './autologin-sso.component';

export const routes: Routes = [
  {
    path: '',
    component: AutologinSSOComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    NgxLoadingModule,
    ModalAgreementComponent,
  ],
  declarations: [AutologinSSOComponent],
  bootstrap: [AutologinSSOComponent],
})
export class AutologinSSOModule {}
