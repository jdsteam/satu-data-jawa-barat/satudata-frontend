import { Injectable } from '@angular/core';

// PACKAGE
import { jdsBiColor } from '@jds-bi/cdk';
import { css } from '@emotion/css';

import { IStyleLoginV2 } from './login-v2.interface';

@Injectable()
export class LoginV2Style {
  getDynamicStyle(): IStyleLoginV2 {
    const main = css`
      padding: 80px;
      height: 100vh;
      display: flex;
      flex-direction: row;
      align-items: center;
      justify-content: center;
      margin-left: auto;
      margin-right: auto;

      @media (min-width: 1200px) {
        width: 1240px;
      }

      @media (max-width: 1199.98px) {
        padding: 40px;
      }

      @media (max-width: 991.98px) {
        padding: 30px;
      }

      @media (max-width: 767.98px) {
        flex-direction: column;
      }

      @media (max-width: 575.98px) {
        overflow-y: auto;
        height: auto;
        padding: 50px 10px;
      }
    `;

    const information = css`
      width: 60%;
      padding-left: 20px;
      padding-right: 20px;

      @media (max-width: 991.98px) {
        width: 45%;
      }

      @media (max-width: 575.98px) {
        width: 100%;
      }
    `;

    const logo = css`
      width: 20%;

      @media (max-width: 991.98px) {
        width: 45%;
      }

      @media (max-width: 575.98px) {
        width: 35%;
        display: block;
        margin-left: auto;
        margin-right: auto;
      }
    `;

    const description = css`
      color: black;
      font-size: 18px;
      margin-top: 20px;
      margin-bottom: 30px;
      line-height: 28px;

      @media (max-width: 991.98px) {
        font-size: 17px;
        margin-bottom: 0;
      }

      @media (max-width: 575.98px) {
        text-align: center;
        font-size: 16px;
        margin-bottom: 30px;
      }
    `;

    const illustration = css`
      width: 60%;
      display: block;
      margin-left: auto;
      margin-right: auto;

      @media (max-width: 991.98px) {
        display: none;
      }
    `;

    const content = css`
      width: 40%;
      padding-left: 20px;
      padding-right: 20px;
      margin-left: auto;
      margin-right: auto;
      display: flex;
      flex-direction: column;
      align-items: center;

      @media (max-width: 991.98px) {
        width: 55%;
      }

      @media (max-width: 575.98px) {
        width: 100%;
      }
    `;

    const card = css`
      width: 100%;
      border: none;
      background-color: #fff;
    `;

    const cardHeader = css`
      padding: 48px 32px 12px 32px;
      border-bottom: none;

      @media (max-width: 991.98px) {
        padding: 32px 32px 12px 32px;
      }

      @media (max-width: 767.98px) {
        padding: 24px 24px 12px 24px;
      }
    `;

    const cardBody = css`
      padding: 10px 32px 48px;

      @media (max-width: 767.98px) {
        padding: 10px 24px;
      }
    `;

    const cardFooter = css`
      padding: 20px 32px 10px 32px;
      border-top: none;
      background-color: #f3fbfe;
      text-align: center;
      font-size: 10px;
      color: #aaa;

      @media (max-width: 991.98px) {
        padding: 10px 24px;
      }
    `;

    const labelError = css`
      color: #dc3545;
    `;

    const rememberForgot = css`
      display: flex;
      align-items: center;

      @media (max-width: 575.98px) {
        flex-direction: column;
      }
    `;

    const formCheck = css`
      margin-bottom: 0;

      @media (max-width: 575.98px) {
        margin-bottom: 8px;
      }
    `;

    const forgotPassword = css`
      @media (min-width: 576px) {
        margin-left: auto;
      }
    `;

    const forgotLink = css`
      font-size: 14px;

      &:hover {
        color: #0753a6;
        text-decoration: none;
      }
    `;

    const separator = css`
      display: flex;
      align-items: center;
      font-size: 14px;

      &::after,
      ::before {
        content: '';
        height: 1px;
        background-color: ${jdsBiColor.gray['300']};
        flex-grow: 1;
      }

      &::before {
        margin-right: 12px;
      }

      &::after {
        margin-left: 12px;
      }
    `;

    const accordion = css`
      &.t-accordion {
        .t-accordion-item {
          background-color: ${jdsBiColor.gray['50']} !important;
          border-color: ${jdsBiColor.gray['500']} !important;

          .t-accordion-button {
            padding: 10px 16px !important;
            font-size: 14px !important;
            font-weight: 400 !important;
            line-height: 18px !important;
            text-decoration: underline;
            display: flex;
            justify-content: center;
            gap: 10px;

            &::after {
              content: none !important;
            }

            &:focus-within {
              box-shadow: none !important;
            }

            &:not(.t-collapsed) {
              .t-accordion-button-icon {
                transform: rotate(180deg);
              }
            }
          }

          .t-accordion-body {
            border-top-width: 0 !important;
            padding: 24px 16px !important;
          }
        }
      }
    `;

    return {
      main,
      information,
      logo,
      description,
      illustration,
      content,
      card,
      cardHeader,
      cardBody,
      cardFooter,
      labelError,
      rememberForgot,
      formCheck,
      forgotPassword,
      forgotLink,
      separator,
      accordion,
    };
  }
}
