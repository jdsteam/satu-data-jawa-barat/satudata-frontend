import {
  Component,
  OnInit,
  OnDestroy,
  Renderer2,
  ViewChild,
  inject,
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import {
  UntypedFormGroup,
  UntypedFormControl,
  Validators,
} from '@angular/forms';
import { first } from 'rxjs/operators';
import { of } from 'rxjs';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Breadcrumb } from '@interfaces-v4';
// SERVICE
import {
  AuthenticationService,
  StateService,
  TokenService,
} from '@services-v3';
import { GlobalService } from '@services-v4';

// COMPONENT
import { ForgotPasswordComponent as ModalForgotPasswordComponent } from '@components-v3/modal/forgot-password/forgot-password.component';
import { AgreementComponent as ModalAgreementComponent } from '@components-v3/modal/agreement/agreement.component';
import {
  JdsBiIconsService,
  iconChevronDown,
  iconEye,
  iconEyeSlash,
} from '@jds-bi/icons';

// PLUGINS
import moment from 'moment';
import { fromWorker } from 'observable-webworker';
import { ToastrService } from 'ngx-toastr';

import { IStyleLoginV2 } from './login-v2.interface';
import { LoginV2Style } from './login-v2.style';

@Component({
  selector: 'app-login',
  templateUrl: './login-v2.component.html',
  providers: [LoginV2Style],
})
export class LoginV2Component implements OnInit, OnDestroy {
  env = environment;
  colorScheme = COLORSCHEME;
  satudataToken: string;
  moment = moment;
  jds = jds;

  // Service
  private router = inject(Router);
  private route = inject(ActivatedRoute);
  private titleService = inject(Title);
  private renderer = inject(Renderer2);
  private globalService = inject(GlobalService);
  private authenticationService = inject(AuthenticationService);
  private stateService = inject(StateService);
  private tokenService = inject(TokenService);
  private toastr = inject(ToastrService);
  private iconService = inject(JdsBiIconsService);
  private loginStyle = inject(LoginV2Style);

  // Log
  log = null;

  @ViewChild(ModalForgotPasswordComponent)
  modalForgotPasswordComponent: ModalForgotPasswordComponent;

  @ViewChild(ModalAgreementComponent)
  modalAgreementComponent: ModalAgreementComponent;

  className!: IStyleLoginV2;

  // Setting
  title: string;
  label = 'Login';
  breadcrumb: Breadcrumb[] = [{ label: 'Login', link: '/auth' }];

  // Variable
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';

  fieldTextType: boolean;
  isRememberMe = false;
  getDataRememberMe: string;

  ssoURL: string;
  ssoDigiteamURL: string;

  // Form
  loginForm: UntypedFormGroup;

  constructor() {
    const { authURL } = this.authenticationService.configSSO();
    this.ssoURL = authURL;

    const { authURL: authDigiteamURL } =
      this.authenticationService.configSSODigiteam();
    this.ssoDigiteamURL = authDigiteamURL;

    const current = this.router.getCurrentNavigation();

    if (current) {
      const { state } = current.extras;
      if (state) {
        if (state.errorType === 'api') {
          this.toastr.error(state.error, 'Auto Login');
        } else if (state.errorType === 'access') {
          this.error = state.error;
        }
      }
    }

    if (this.authenticationService.satudataUserValue) {
      this.router.navigate(['/home']);
    }

    this.renderer.addClass(document.body, 'satudata-login');

    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/home';

    this.getDataRememberMe = this.authenticationService.dataRememberMe;

    this.iconService.registerIcons([iconChevronDown, iconEye, iconEyeSlash]);
  }

  ngOnInit() {
    this.settingsAll();

    this.className = this.loginStyle.getDynamicStyle();

    this.loginForm = new UntypedFormGroup({
      username: new UntypedFormControl('', [Validators.required]),
      password: new UntypedFormControl('', [Validators.required]),
      recaptcha: new UntypedFormControl('', [Validators.required]),
    });

    if (this.getDataRememberMe !== null) {
      this.f.username.setValue(this.getDataRememberMe);
    }
  }

  ngOnDestroy() {
    this.stateService.changeAuthenticationLogin(null);
    this.renderer.removeClass(document.body, 'satudata-login');
  }

  settingsAll(): void {
    this.title = this.globalService.title;
    this.titleService.setTitle(`${this.label} - ${this.title}`);
    this.globalService.changeLabel(this.label);
    this.globalService.changeBreadcrumb(this.breadcrumb);
  }

  get f() {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    this.error = '';

    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;
    this.authenticationService
      .login(
        this.f.username.value,
        this.f.password.value,
        this.f.recaptcha.value,
        this.isRememberMe,
      )
      .pipe(first())
      .subscribe(
        (result) => {
          this.loading = false;

          const { body } = result;

          if (!body.data.is_agree) {
            this.stateService.changeAuthenticationLogin(body.data);
            this.authenticationService.setToken(body.data.jwt);
            this.modalAgreementComponent.openModal();
          } else {
            this.authenticationService.setSession(body.data);
            this.router.navigate([this.returnUrl]);
          }
        },
        (error) => {
          this.loading = false;

          switch (error.status) {
            case 401:
              this.error = 'Username dan Kata Sandi tidak ditemukan.';
              break;
            case 403:
              this.error =
                'Beberapa percobaan login belum berhasil. Silahkan coba lagi dalam 5 menit';
              break;
            default:
              this.toastr.error(error.message, 'Login');
              break;
          }

          this.loginForm.patchValue({ recaptcha: null });
        },
      );
  }

  toggleFieldTextType() {
    this.fieldTextType = !this.fieldTextType;
  }

  forgotPassword() {
    this.modalForgotPasswordComponent.openModal();
  }

  rememberMe(value) {
    this.isRememberMe = value;
  }

  logLogin(status: string, user = null): void {
    const global = {
      status,
      date_start: moment().format('YYYY-MM-DD HH:mm:ss'),
      date_end: moment().format('YYYY-MM-DD HH:mm:ss'),
    };

    let additional: object;
    if (status === 'success') {
      this.satudataToken = user.jwt;

      additional = {
        user_id: user.id,
        user_username: user.username,
        user_name: user.name,
        user_role: user.role_name,
        user_organization_id: user.satuan_kerja_id,
        user_organization_name: user.satuan_kerja_nama,
        user_position_id: user.jabatan_id,
        user_position_name: user.jabatan_nama,
      };
    } else if (status === 'failed') {
      this.satudataToken = this.tokenService.createToken(this.f.username.value);

      additional = {
        user_username: this.f.username.value,
      };
    }

    this.log = {
      log: { ...global, ...additional },
      token: this.satudataToken,
    };

    fromWorker<object, string>(
      () =>
        new Worker(
          new URL('src/app/__v4/workers/log-login.worker', import.meta.url),
          {
            type: 'module',
            name: 'login',
          },
        ),
      of(this.log),
    ).subscribe((message) => {
      if (!environment.production) {
        console.info(`Log login: ${message}`);
      }
    });
  }
}
