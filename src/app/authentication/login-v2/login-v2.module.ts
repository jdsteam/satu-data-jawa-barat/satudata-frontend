import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule, NgOptimizedImage } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// MODULE
import { LetDirective } from '@ngrx/component';
import { NgxLoadingModule } from 'ngx-loading';
import { RecaptchaModule, RecaptchaFormsModule } from 'ng-recaptcha';
import {
  JdsBiAccordionModule,
  JdsBiInputModule,
  JdsBiButtonModule,
} from '@jds-bi/core';
import { JdsBiIconsModule } from '@jds-bi/icons';

import { ForgotPasswordComponent as ModalForgotPasswordComponent } from '@components-v3/modal/forgot-password/forgot-password.component';
import { AgreementComponent as ModalAgreementComponent } from '@components-v3/modal/agreement/agreement.component';

import { LoginV2Component } from './login-v2.component';

export const routes: Routes = [
  {
    path: '',
    component: LoginV2Component,
  },
];

@NgModule({
  imports: [
    CommonModule,
    NgOptimizedImage,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    LetDirective,
    NgxLoadingModule,
    JdsBiAccordionModule,
    JdsBiInputModule,
    JdsBiButtonModule,
    JdsBiIconsModule,
    ModalForgotPasswordComponent,
    ModalAgreementComponent,
    RecaptchaModule,
    RecaptchaFormsModule,
  ],
  declarations: [LoginV2Component],
  bootstrap: [LoginV2Component],
})
export class LoginV2Module {}
