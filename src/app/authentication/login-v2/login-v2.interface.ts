export interface IStyleLoginV2 {
  main: string;
  information: string;
  logo: string;
  description: string;
  illustration: string;
  content: string;
  card: string;
  cardHeader: string;
  cardBody: string;
  cardFooter: string;
  labelError: string;
  rememberForgot: string;
  formCheck: string;
  forgotPassword: string;
  forgotLink: string;
  separator: string;
  accordion: string;
}
