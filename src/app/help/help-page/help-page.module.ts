import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

// PIPE
import { SafeHtmlPipe } from '@pipes-v4';

import { JdsBiAccordionModule } from '@jds-bi/core';
import { JdsBiIconsModule } from '@jds-bi/icons';

import { HelpComponent as HeaderHelpComponent } from '@components-v3/header/help/help.component';

import { HelpPageComponent } from './help-page.component';

export const routes: Routes = [
  {
    path: '',
    component: HelpPageComponent,
  },
];

@NgModule({
  declarations: [HelpPageComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    JdsBiAccordionModule,
    JdsBiIconsModule,
    HeaderHelpComponent,
    SafeHtmlPipe,
  ],
  bootstrap: [HelpPageComponent],
})
export class HelpPageModule {}
