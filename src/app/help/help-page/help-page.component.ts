import {
  Component,
  OnInit,
  OnDestroy,
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  ViewChildren,
  QueryList,
  ElementRef,
} from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { WHATSAPP, WHATSAPP_INT } from '@constants-v4';
// INTERFACE
import { Breadcrumb } from '@interfaces-v4';
// MODEL
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService, StateService } from '@services-v3';
import { GlobalService } from '@services-v4';
// JDS-BI
import { JdsBiIconsService, iconEnvelope, iconWhatsapp } from '@jds-bi/icons';

import * as _ from 'lodash';
import moment from 'moment';
import { fromWorker } from 'observable-webworker';

import helpJson from '@data/help-v2.js';

@Component({
  selector: 'app-help-page',
  templateUrl: './help-page.component.html',
  styleUrls: ['./help-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HelpPageComponent implements OnInit, OnDestroy, AfterViewInit {
  satudataUser: LoginData;
  satudataToken: string;
  whatsapp = WHATSAPP;
  moment = moment;
  jds = jds;

  @ViewChildren('subcontent') subcontent: QueryList<ElementRef>;
  @ViewChildren('panel') panel: QueryList<ElementRef>;
  @ViewChildren('accordion') accordion: QueryList<ElementRef>;

  // Log
  log = null;
  pageInfo = null;
  deviceInfo = null;
  userInfo = null;

  // Setting
  title: string;
  label = 'Bantuan';
  description = 'Bantuan dan Dokumentasi teknik dalam Satu Data Jabar';
  breadcrumb: Breadcrumb[] = [{ label: 'Bantuan', link: '/help' }];

  // Variable
  dataHelp = helpJson;
  message = `https://wa.me/${WHATSAPP_INT}?text=Hi!%20saya%20ingin%20bertanya%20mengenai%20Satu%20Data%20Jabar`;

  filter = {
    topic: 'satudata',
  };

  fragment: string;

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private globalService: GlobalService,
    private authenticationService: AuthenticationService,
    private stateService: StateService,
    private iconService: JdsBiIconsService,
    private titleService: Title,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
    this.authenticationService.satudataToken.subscribe(
      (x) => (this.satudataToken = x),
    );
    this.stateService.currentLogUser.subscribe((x) => (this.userInfo = x));

    this.iconService.registerIcons([iconEnvelope, iconWhatsapp]);
  }

  ngOnInit(): void {
    this.settingsAll();
    this.queryParams();
    this.logPage('init');

    _.map(this.dataHelp, (result) => {
      if (result.id === this.filter.topic) {
        _.assign(result, { checked: true });
      } else {
        _.assign(result, { checked: false });
      }
    });
  }

  ngOnDestroy(): void {
    this.logPage('destroy');
  }

  ngAfterViewInit(): void {
    this.scrollFragment();
    this.cdRef.detectChanges();
  }

  settingsAll(): void {
    this.title = this.globalService.title;
    this.titleService.setTitle(`${this.label} - ${this.title}`);
    this.globalService.changeLabel(this.label);
    this.globalService.changeBreadcrumb(this.breadcrumb);
  }

  queryParams(): void {
    this.route.queryParams.subscribe((p) => {
      const pTopic = p.topic;

      this.filter.topic = !_.isNil(pTopic) ? pTopic : 'satudata';
    });

    this.route.fragment.subscribe((fragment: string) => {
      this.fragment = fragment;
    });
  }

  get userManualLink(): string {
    let res: string;
    switch (this.satudataUser.role_name) {
      case 'walidata':
        res =
          'https://docs.google.com/document/d/169DkmFJTa0uMw0h5gzQ5c98NIeErcH2Epm5w_xEF17Q/edit?usp=sharing';
        break;
      case 'opd':
        res =
          'https://docs.google.com/document/d/1GzeqWfUyCqMy3lpAeeTjBw48tlW8DuXWX6Ojhtu6gIg/edit?usp=sharing';
        break;
      case 'opd-view':
        res =
          'https://docs.google.com/document/d/1Ai13fFTHzIJdghckvgjC-ab9-csGgARuRYUXA-XrJv8/edit?usp=sharing';
        break;
      default:
        break;
    }

    return res;
  }

  filterTopic(topic: string, subtopic: string): void {
    _.map(this.dataHelp, (result) => {
      if (result.id === topic) {
        // eslint-disable-next-line no-param-reassign
        result.checked = true;
      } else {
        // eslint-disable-next-line no-param-reassign
        result.checked = false;
      }
    });

    if (this.filter.topic !== topic) {
      this.router
        .navigate([], {
          relativeTo: this.route,
          queryParams: {
            topic,
          },
          queryParamsHandling: 'merge',
          fragment: subtopic,
          state: { ignoreLoadingBar: true },
        })
        .then(() => {
          this.fragment = subtopic;

          setTimeout(() => {
            this.scrollFragment();
          }, 500);
        });
    } else {
      this.router
        .navigate([], {
          relativeTo: this.route,
          queryParams: {
            topic,
          },
          queryParamsHandling: 'merge',
          fragment: subtopic,
          state: { ignoreLoadingBar: true },
        })
        .then(() => {
          this.fragment = subtopic;
          this.scrollFragment();
        });
    }

    this.filter.topic = topic;
  }

  scrollFragment(): void {
    this.subcontent.forEach((div: ElementRef) => {
      if (div.nativeElement.id === this.fragment) {
        div.nativeElement.scrollIntoView();

        const scrolledY = window.scrollY;

        if (scrolledY) {
          window.scroll(0, scrolledY - 40);
        }
      }
    });
  }

  isOpen(id: string) {
    _.map(this.dataHelp, (result) => {
      if (result.id === id) {
        // eslint-disable-next-line no-param-reassign
        result.checked = !result.checked;
      }
    });
  }

  logPage(type: string): void {
    if (type === 'init') {
      this.pageInfo = {
        page: this.router.url,
        date_start: moment().format('YYYY-MM-DD HH:mm:ss'),
      };

      this.log = {
        log: { ...this.pageInfo, ...this.userInfo },
        token: this.satudataToken,
      };
    } else if (type === 'destroy') {
      if (typeof Worker !== 'undefined') {
        _.set(this.log.log, 'date_end', moment().format('YYYY-MM-DD HH:mm:ss'));

        const log = of(this.log);

        fromWorker<object, string>(
          () =>
            new Worker(
              new URL('src/app/__v4/workers/log-page.worker', import.meta.url),
              { type: 'module' },
            ),
          log,
        ).subscribe((message) => {
          if (!environment.production) {
            console.info(`Log page: ${message}`);
          }
        });
      }
    }
  }
}
