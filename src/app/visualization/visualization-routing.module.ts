import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// GUARDS
import { StoreTopicListGuard, StoreVisualizationDetailGuard } from '@guards-v3';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        canActivate: [StoreTopicListGuard],
        loadChildren: () =>
          import('./visualization-page/visualization-page.module').then(
            (m) => m.VisualizationPageModule,
          ),
      },
      {
        path: ':id',
        canActivate: [StoreVisualizationDetailGuard],
        loadChildren: () =>
          import('./visualization-detail/visualization-detail.module').then(
            (m) => m.VisualizationDetailModule,
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VisualizationRoutingModule {}
