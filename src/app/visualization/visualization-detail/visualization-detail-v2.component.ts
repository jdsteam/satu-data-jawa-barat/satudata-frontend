import {
  Component,
  OnDestroy,
  OnInit,
  AfterViewInit,
  Renderer2,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { Subject, of } from 'rxjs';
import { filter, take } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';

// INTERFACE
import { Breadcrumb } from '@interfaces-v4';

// MODEL
import { LoginData, VisualizationData } from '@models-v3';

// SERVICE
import { AuthenticationService, StateService } from '@services-v3';
import { GlobalService } from '@services-v4';

// STORE
import { selectVisualization } from '@store-v3/visualization/visualization.selectors';

// PLUGIN
import { Store, select } from '@ngrx/store';

import * as _ from 'lodash';
import moment from 'moment';
import { fromWorker } from 'observable-webworker';

@Component({
  selector: 'app-visualization-detail-v2',
  templateUrl: './visualization-detail-v2.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VisualizationDetailV2Component
  implements OnInit, OnDestroy, AfterViewInit
{
  satudataUser: LoginData;
  satudataToken: string;
  moment = moment;
  jds = jds;

  // Log
  pageLog = null;
  visualizationLog = null;

  pageInfo = null;
  visualizationInfo = null;
  userInfo = null;

  // Pengaturan
  title: string;
  label = 'Visualisasi';
  breadcrumb: Breadcrumb[] = [
    { label: 'Visualisasi', link: '/visualization' },
    { label: 'Detail', link: '/visualization/detail' },
  ];

  // Variable
  id: number;
  tableauViz = {};
  accessTableau: boolean;
  accessIsLoadingTableau: boolean;
  accessInfoTableau: string;

  // Data
  data: VisualizationData;

  private unsubscribeVisualization$ = new Subject<void>();

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private globalService: GlobalService,
    private stateService: StateService,
    private titleService: Title,
    private renderer: Renderer2,
    private store: Store<any>,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
    this.authenticationService.satudataToken.subscribe(
      (x) => (this.satudataToken = x),
    );
    this.stateService.currentLogUser.subscribe((x) => (this.userInfo = x));

    // id
    this.id = this.route.snapshot.params.id;
  }

  settingsAll() {
    this.renderer.removeClass(document.body, 'satudata');

    this.title = this.globalService.title;
    this.titleService.setTitle(`${this.label} - ${this.title}`);
    this.globalService.changeLabel(this.label);
    this.globalService.changeBreadcrumb(this.breadcrumb);
  }

  ngOnInit() {
    this.settingsAll();
    this.getVisualization();
    this.logPage('init');
    this.logVisualization('init');
  }

  ngOnDestroy() {
    this.logPage('destroy');
    this.logVisualization('destroy');
    this.unsubscribeVisualization$.next();
    this.unsubscribeVisualization$.complete();
  }

  ngAfterViewInit(): void {
    this.cdRef.detectChanges();
  }

  getVisualization() {
    this.store
      .pipe(
        select(selectVisualization(this.id)),
        filter((val) => val !== undefined),
        take(1),
      )
      .subscribe((result) => {
        this.data = result;
      });
  }

  logPage(type: string): void {
    if (type === 'init') {
      this.pageInfo = {
        page: this.router.url,
        date_start: moment().format('YYYY-MM-DD HH:mm:ss'),
      };

      this.pageLog = {
        log: { ...this.pageInfo, ...this.userInfo },
        token: this.satudataToken,
      };
    } else if (type === 'destroy') {
      if (typeof Worker !== 'undefined') {
        _.set(
          this.pageLog.log,
          'date_end',
          moment().format('YYYY-MM-DD HH:mm:ss'),
        );

        fromWorker<object, string>(
          () =>
            new Worker(
              new URL('src/app/__v4/workers/log-page.worker', import.meta.url),
              { type: 'module' },
            ),
          of(this.pageLog),
        ).subscribe((message) => {
          if (!environment.production) {
            console.info(`Log page: ${message}`);
          }
        });
      }
    }
  }

  logVisualization(type: string): void {
    if (type === 'init') {
      this.visualizationInfo = {
        page: this.router.url,
        action: 'view',
        status: 'success',
        date_start: moment().format('YYYY-MM-DD HH:mm:ss'),
        visualization_id: this.data.id,
        visualization_title: this.data.title,
        visualization_name: this.data.name,
        visualization_organization_code: this.data.skpd.kode_skpd,
        visualization_organization_name: this.data.skpd.nama_skpd,
      };

      this.visualizationLog = {
        log: { ...this.visualizationInfo, ...this.userInfo },
        token: this.satudataToken,
      };
    } else if (type === 'destroy') {
      if (typeof Worker !== 'undefined') {
        _.set(
          this.visualizationLog.log,
          'date_end',
          moment().format('YYYY-MM-DD HH:mm:ss'),
        );

        fromWorker<object, string>(
          () =>
            new Worker(
              new URL(
                'src/app/__v4/workers/log-visualization.worker',
                import.meta.url,
              ),
              {
                type: 'module',
                name: 'visualization',
              },
            ),
          of(this.visualizationLog),
        ).subscribe((message) => {
          if (!environment.production) {
            console.info(`Log visualization: ${message}`);
          }
        });
      }
    }
  }
}
