import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { BreadcrumbComponent } from '@components-v4';
import { HeaderV2Component } from '@components-v3/detail/visualization/header-v2/header-v2.component';
import { DescriptionV2Component } from '@components-v3/detail/visualization/description-v2/description-v2.component';

import { VisualizationDetailV2Component } from './visualization-detail-v2.component';

export const routes: Routes = [
  {
    path: '',
    component: VisualizationDetailV2Component,
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    BreadcrumbComponent,
    HeaderV2Component,
    DescriptionV2Component,
  ],
  declarations: [VisualizationDetailV2Component],
  bootstrap: [VisualizationDetailV2Component],
})
export class VisualizationDetailModule {}
