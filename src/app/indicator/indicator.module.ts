import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndicatorRoutingModule } from './indicator-routing.module';

@NgModule({
  imports: [CommonModule, IndicatorRoutingModule],
})
export class IndicatorModule {}
