import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgxLoadingModule } from 'ngx-loading';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgSelectModule } from '@ng-select/ng-select';
import {
  JdsBiBadgeModule,
  JdsBiDropdownModule,
  JdsBiHostedDropdownModule,
} from '@jds-bi/core';
import { JdsBiIconsModule } from '@jds-bi/icons';
import { ModalModule } from '@components-v3/modal';
import { EmptyComponent } from '@components-v3/list/empty/empty.component';

import { IndicatorDownloadComponent } from './indicator-download.component';

export const routes: Routes = [
  {
    path: '',
    component: IndicatorDownloadComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    NgxLoadingModule,
    NgxSkeletonLoaderModule,
    NgSelectModule,
    JdsBiIconsModule,
    JdsBiBadgeModule,
    ModalModule,
    JdsBiDropdownModule,
    JdsBiHostedDropdownModule,
    EmptyComponent,
  ],
  declarations: [IndicatorDownloadComponent],
  bootstrap: [IndicatorDownloadComponent],
})
export class IndicatorDownloadModule {}
