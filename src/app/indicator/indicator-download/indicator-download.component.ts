import {
  Component,
  OnInit,
  OnDestroy,
  Renderer2,
  ChangeDetectorRef,
} from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { Store, select } from '@ngrx/store';
import {
  UntypedFormGroup,
  UntypedFormControl,
  Validators,
} from '@angular/forms';
import { Observable, of } from 'rxjs';
import { filter } from 'rxjs/operators';
import { HttpEventType } from '@angular/common/http';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';

// INTERFACE
import { Breadcrumb } from '@interfaces-v4';

// MODEL
import {
  LoginData,
  IndicatorData,
  ClassificationIndicatorData,
  OrganizationData,
} from '@models-v3';

// SERVICE
import {
  AuthenticationService,
  StateService,
  HttpService,
  IndicatorService,
} from '@services-v3';
import { GlobalService } from '@services-v4';

// COMPONENT
import { ModalService } from '@components-v3/modal';
import { JdsBiIconsService, iconChevronDown } from '@jds-bi/icons';

import {
  selectAllClassificationIndicator,
  selectIsLoadingList as selectIsLoadingListClassificationIndicator,
} from '@store-v3/classification-indicator/classification-indicator.selectors';
import { fromClassificationIndicatorActions } from '@store-v3/classification-indicator/classification-indicator.actions';

import {
  selectAllOrganization,
  selectIsLoadingList as selectIsLoadingListOrganization,
} from '@store-v3/organization/organization.selectors';
import { fromOrganizationActions } from '@store-v3/organization/organization.actions';

import { ToastrService } from 'ngx-toastr';

import * as _ from 'lodash';
import moment from 'moment';
import Swal from 'sweetalert2';
import { saveAs } from 'file-saver';
import { fromWorker } from 'observable-webworker';

@Component({
  selector: 'app-indicator-download',
  templateUrl: './indicator-download.component.html',
  styleUrls: ['./indicator-download.component.css'],
})
export class IndicatorDownloadComponent implements OnInit, OnDestroy {
  satudataUser: LoginData;
  satudataToken: string;
  moment = moment;
  jds = jds;

  // Log
  log = null;
  pageInfo = null;
  deviceInfo = null;
  userInfo = null;

  // settings
  title: string;
  label = 'Unduh Indikator';
  breadcrumb: Breadcrumb[] = [
    { label: 'Indikator', link: '/indicator' },
    { label: 'Unduh Indikator', link: '/indicator/download' },
  ];

  // variable
  public isDropdown = false;
  loading = false;
  submitted = false;
  myForm: UntypedFormGroup;

  indicatorData: IndicatorData[];
  indicatorActive: any[];
  classificationActive: any[];
  organizationActive: any[];

  dataCSV = false;
  dataCSVName: string;
  dataCSVProgress = 0;
  dataExcel = false;
  dataExcelName: string;
  dataExcelProgress = 0;

  downloadIsLoading = false;
  downloadProgress = 0;

  // data
  indicatorData$: Observable<IndicatorData[]>;
  indicatorDataTotal$: Observable<number>;
  indicatorIsLoadingList$: Observable<boolean>;
  indicatorError$: Observable<string>;
  indicatorIsError: boolean;

  indicatorClassificationData$: Observable<ClassificationIndicatorData[]>;
  indicatorClassificationDataTotal$: Observable<number>;
  indicatorClassificationIsLoadingList$: Observable<boolean>;
  indicatorClassificationError$: Observable<string>;

  organizationData$: Observable<OrganizationData[]>;
  organizationDataTotal$: Observable<number>;
  organizationIsLoadingList$: Observable<boolean>;
  organizationError$: Observable<string>;

  constructor(
    private cdRef: ChangeDetectorRef,
    private authenticationService: AuthenticationService,
    private globalService: GlobalService,
    private stateService: StateService,
    private titleService: Title,
    private httpService: HttpService,
    private indicatorService: IndicatorService,
    private renderer: Renderer2,
    private router: Router,
    private route: ActivatedRoute,
    private modalService: ModalService,
    private iconService: JdsBiIconsService,
    private store: Store<any>,
    private toastr: ToastrService,
  ) {
    // settings
    this.renderer.removeClass(document.body, 'satudata');
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
    this.authenticationService.satudataToken.subscribe(
      (x) => (this.satudataToken = x),
    );
    this.stateService.currentLogUser.subscribe((x) => (this.userInfo = x));

    this.title = this.globalService.title;
    this.titleService.setTitle(`${this.label} - ${this.title}`);
    this.globalService.changeLabel(this.label);
    this.globalService.changeBreadcrumb(this.breadcrumb);

    this.iconService.registerIcons([iconChevronDown]);
  }

  ngOnInit() {
    this.logPage('init');
    this.getAllClassification();
    this.getAllOrganization();

    this.myForm = new UntypedFormGroup({
      classification: new UntypedFormControl([], [Validators.required]),
      kode_skpd: new UntypedFormControl([], [Validators.required]),
    });
  }

  ngOnDestroy(): void {
    this.logPage('destroy');
  }

  get f() {
    return this.myForm.controls;
  }

  // GET =====================================================================================================
  getAllClassification() {
    const pSort = 'name:asc';

    const params = {
      sort: pSort,
    };

    this.store.dispatch(
      fromClassificationIndicatorActions.loadAllClassificationIndicator({
        params,
        pagination: false,
        infinite: false,
      }),
    );

    this.indicatorClassificationIsLoadingList$ = this.store.pipe(
      select(selectIsLoadingListClassificationIndicator),
    );

    this.indicatorClassificationData$ = this.store.pipe(
      select(selectAllClassificationIndicator),
      filter((val) => val.length !== 0),
    );
  }

  getAllOrganization(): void {
    const pSort = `nama_skpd:asc`;

    const pWhere = JSON.stringify({
      is_satudata: true,
      is_deleted: false,
    });

    const params = {
      sort: pSort,
      where: pWhere,
    };

    this.store.dispatch(
      fromOrganizationActions.loadAllOrganization({
        params,
        pagination: false,
        infinite: false,
      }),
    );

    this.organizationIsLoadingList$ = this.store.pipe(
      select(selectIsLoadingListOrganization),
    );

    this.organizationData$ = this.store.pipe(
      select(selectAllOrganization),
      filter((val) => val.length !== 0),
    );
  }

  getAllIndicator() {
    const pWhere = {
      is_validate: 3,
      is_active: true,
      is_deleted: false,
    };

    if (this.classificationActive) {
      if (
        Array.isArray(this.classificationActive) &&
        !_.isEmpty(this.classificationActive)
      ) {
        const names = this.classificationActive.map((item) => `${item}`);
        _.assign(pWhere, { classification: names });
      }
    }

    if (this.organizationActive) {
      if (
        Array.isArray(this.organizationActive) &&
        !_.isEmpty(this.organizationActive)
      ) {
        const kodes = this.organizationActive.map((item) => `${item}`);
        _.assign(pWhere, { kode_skpd: kodes });
      }
    }

    const params = {
      where: JSON.stringify(pWhere),
    };

    this.indicatorIsLoadingList$ = of(true);
    this.indicatorService.getList(params).subscribe(
      (result) => {
        this.indicatorIsLoadingList$ = of(false);

        const ids = _.map(result.data, 'id').join(',');
        this.indicatorData = _.map(result.data, (resultIndicator) => {
          return _.assign(resultIndicator, { checked: true });
        });

        this.router.navigate([], {
          relativeTo: this.route,
          queryParams: {
            indicator: ids,
            currentpage: null,
          },
          queryParamsHandling: 'merge',
        });

        const active = _.filter(this.indicatorData, 'checked');
        this.indicatorActive = active;
      },
      (error) => {
        this.indicatorIsError = true;
        this.toastr.error(error.statusText, 'Indicator');
      },
    );
  }

  getClassificationColor(id: number) {
    return this.indicatorService.getClassificationColor(id);
  }

  // FILTER ==================================================================================================
  filterIndicator(isChecked: boolean, data: any) {
    if (isChecked) {
      _.map(this.indicatorData, (result) => {
        if (result.id === data.id) {
          _.assign(result, { checked: true });
        }
      });
    } else {
      _.map(this.indicatorData, (result) => {
        if (result.id === data.id) {
          // eslint-disable-next-line no-param-reassign
          delete result.checked;
        }
      });
    }

    const hasil = _.map(_.filter(this.indicatorData, 'checked'), 'id').join(
      ',',
    );
    const value = _.isEmpty(hasil) === false ? hasil : null;

    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        indicator: value,
        currentpage: null,
      },
      queryParamsHandling: 'merge',
    });

    const active = _.filter(this.indicatorData, 'checked');
    this.indicatorActive = active;
  }

  // ONCLICK =================================================================================================
  onFilter() {
    const { classification, kode_skpd: organization } = this.myForm.value;

    this.classificationActive = classification;
    this.organizationActive = organization;
    this.getAllIndicator();
  }

  checkChecked(id: number) {
    return _.find(this.indicatorData, { id });
  }

  checkCheckedAll(isChecked: boolean) {
    if (isChecked) {
      _.map(this.indicatorData, (result) => {
        _.assign(result, { checked: true });
      });
    } else {
      _.map(this.indicatorData, (result) => {
        // eslint-disable-next-line no-param-reassign
        delete result.checked;
      });
    }

    const hasil = _.map(_.filter(this.indicatorData, 'checked'), 'id').join(
      ',',
    );
    const value = _.isEmpty(hasil) === false ? hasil : null;

    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        indicator: value,
        currentpage: null,
      },
      queryParamsHandling: 'merge',
    });

    const active = _.filter(this.indicatorData, 'checked');
    this.indicatorActive = active;
  }

  getType(value: string) {
    let type: string;
    let blob: string;
    let extension: string;

    if (value === 'pdf') {
      type = 'pdf';
      blob = 'application/pdf';
      extension = 'pdf';
    } else if (value === 'csv') {
      type = 'csv';
      blob = 'text/csv';
      extension = 'csv';
    } else if (value === 'excel') {
      type = 'xls';
      blob = 'application/vnd.ms-excel';
      extension = 'xlsx';
    }

    return {
      type,
      blob,
      extension,
    };
  }

  onDownload(type: string) {
    const fileType = this.getType(type);

    this.downloadProgress = 0;
    this.openModal('modal-download-indicator-bulk');

    const urlFile = `${environment.backendURL}indikator`;

    const pWhere = {};

    if (_.isEmpty(this.indicatorActive) === false) {
      const ids = this.indicatorActive.map((item) => `${item.id}`);
      _.assign(pWhere, { id: ids });
    }

    const params = {
      where: JSON.stringify(pWhere),
      download: fileType.type,
      timestamp: new Date().valueOf(),
    };

    this.httpService.getFile(urlFile, params).subscribe((response) => {
      if (response.type === HttpEventType.DownloadProgress) {
        this.downloadIsLoading = true;
        this.downloadProgress = Math.round(
          (100 * response.loaded) / response.total,
        );
        this.cdRef.markForCheck();
      } else if (response.type === HttpEventType.Response) {
        const newBlob = new Blob([response.body], { type: fileType.blob });

        setTimeout(() => {
          this.closeModal('modal-download-indicator-bulk');
          this.downloadIsLoading = false;

          saveAs(newBlob, `indicator.${fileType.extension}`);

          Swal.fire({
            type: 'success',
            text: `Indikator berhasil diunduh.`,
            allowOutsideClick: false,
          });
        }, 1000);
      }
    });
  }

  openModal(id: string) {
    this.modalService.open(id);
  }

  closeModal(id: string) {
    this.modalService.close(id);
  }

  logPage(type: string): void {
    if (type === 'init') {
      this.pageInfo = {
        page: this.router.url,
        date_start: moment().format('YYYY-MM-DD HH:mm:ss'),
      };

      this.log = {
        log: { ...this.pageInfo, ...this.userInfo },
        token: this.satudataToken,
      };
    } else if (type === 'destroy') {
      if (typeof Worker !== 'undefined') {
        _.set(this.log.log, 'date_end', moment().format('YYYY-MM-DD HH:mm:ss'));

        const log = of(this.log);

        fromWorker<object, string>(
          () =>
            new Worker(
              new URL('src/app/__v4/workers/log-page.worker', import.meta.url),
              { type: 'module' },
            ),
          log,
        ).subscribe((message) => {
          if (!environment.production) {
            console.info(`Log page: ${message}`);
          }
        });
      }
    }
  }
}
