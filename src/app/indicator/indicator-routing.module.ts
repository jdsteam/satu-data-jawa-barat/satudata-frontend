import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// GUARD
import { StoreIndicatorDetailGuard } from '@guards-v3';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./indicator-page/indicator-page.module').then(
            (m) => m.IndicatorPageModule,
          ),
      },
      {
        path: 'detail/:id',
        canActivate: [StoreIndicatorDetailGuard],
        loadChildren: () =>
          import('./indicator-detail-v2/indicator-detail-v2.module').then(
            (m) => m.IndicatorDetailV2Module,
          ),
      },
      {
        path: 'download',
        loadChildren: () =>
          import('./indicator-download/indicator-download.module').then(
            (m) => m.IndicatorDownloadModule,
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IndicatorRoutingModule {}
