import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';

import { JdsBiButtonModule } from '@jds-bi/core';
import { NgxLoadingModule } from 'ngx-loading';
import { SubscribeDirective } from '@ngneat/subscribe';

import {
  BreadcrumbComponent,
  ModalIndicatorStatusComponent,
} from '@components-v4';
import { HeaderComponent } from '@components-v3/detail/indicator/header/header.component';
import { DescriptionComponent } from '@components-v3/detail/indicator/description/description.component';
import { PreviewComponent } from '@components-v3/detail/indicator/preview/preview.component';

import { IndicatorDetailV2Component } from './indicator-detail-v2.component';

export const routes: Routes = [
  {
    path: '',
    component: IndicatorDetailV2Component,
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    JdsBiButtonModule,
    SubscribeDirective,
    NgxLoadingModule,
    BreadcrumbComponent,
    ModalIndicatorStatusComponent,
    HeaderComponent,
    DescriptionComponent,
    PreviewComponent,
  ],
  declarations: [IndicatorDetailV2Component],
  bootstrap: [IndicatorDetailV2Component],
})
export class IndicatorDetailV2Module {}
