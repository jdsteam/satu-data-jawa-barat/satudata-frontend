import { Injectable } from '@angular/core';

// PACKAGE
import { css } from '@emotion/css';

import { Style } from './indicator-detail-v2.interface';

@Injectable()
export class IndicatorDetailV2Style {
  getDynamicStyle(): Style {
    const main = css``;

    return {
      main,
    };
  }
}
