import {
  Component,
  OnInit,
  OnDestroy,
  Renderer2,
  inject,
  ViewChild,
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { of, zip } from 'rxjs';
import { filter, take } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';
// COMPONENT
import { ModalIndicatorStatusComponent } from '@components-v4';
// CONSTANT
import { STATUS_INDICATOR } from '@constants-v4';
// INTERFACE
import { Breadcrumb } from '@interfaces-v4';
// MODEL
import { LoginData, IndicatorData } from '@models-v3';
// SERVICE
import { AuthenticationService, StateService } from '@services-v3';
import {
  GlobalService,
  NotificationService,
  IndicatorService,
} from '@services-v4';
// STORE
import { selectIndicator } from '@store-v3/indicator/indicator.selectors';

import { assign, set } from 'lodash';
import moment from 'moment';
import { Store, select } from '@ngrx/store';
import { fromWorker } from 'observable-webworker';
import { useMutationResult } from '@ngneat/query';
import Swal from 'sweetalert2';

import { IndicatorDetailV2Style } from './indicator-detail-v2.style';
import { Style } from './indicator-detail-v2.interface';

@Component({
  selector: 'app-indicator-detail-v2',
  templateUrl: './indicator-detail-v2.component.html',
  providers: [IndicatorDetailV2Style],
})
export class IndicatorDetailV2Component implements OnInit, OnDestroy {
  satudataUser: LoginData;
  satudataToken: string;
  moment = moment;
  jds = jds;

  @ViewChild(ModalIndicatorStatusComponent)
  modalIndicatorStatusComponent: ModalIndicatorStatusComponent;

  // Service
  private notificationService = inject(NotificationService);
  private indicatorService = inject(IndicatorService);
  editIndicatorMutation = useMutationResult();

  className!: Style;

  // Log
  pageLog = null;
  indicatorLog = null;

  pageInfo = null;
  indicatorInfo = null;
  userInfo = null;

  // Pengaturan
  title: string;
  label = 'Indikator';
  breadcrumb: Breadcrumb[] = [
    { label: 'Indikator', link: '/indicator' },
    { label: 'Detail', link: '/indicator/detail' },
  ];

  // Variable
  id: number;

  // Data
  data: IndicatorData;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private globalService: GlobalService,
    private stateService: StateService,
    private titleService: Title,
    private renderer: Renderer2,
    private indicatorDetailStyle: IndicatorDetailV2Style,
    private store: Store<any>,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
    this.authenticationService.satudataToken.subscribe(
      (x) => (this.satudataToken = x),
    );
    this.stateService.currentLogUser.subscribe((x) => (this.userInfo = x));

    // id
    this.id = this.route.snapshot.params.id;
  }

  settingsAll() {
    this.renderer.removeClass(document.body, 'satudata');

    this.title = this.globalService.title;
    this.titleService.setTitle(`${this.label} - ${this.title}`);
    this.globalService.changeLabel(this.label);
    this.globalService.changeBreadcrumb(this.breadcrumb);
  }

  ngOnInit() {
    this.className = this.indicatorDetailStyle.getDynamicStyle();

    this.settingsAll();
    this.getIndicator();
    this.logPage('init');
    this.logIndicator('init');
  }

  ngOnDestroy(): void {
    this.logPage('destroy');
    this.logIndicator('destroy');
  }

  getIndicator() {
    this.store
      .pipe(
        select(selectIndicator(this.id)),
        filter((val) => val !== undefined),
        take(1),
      )
      .subscribe((result) => {
        this.data = result;
      });
  }

  checkAction(validate: number) {
    let accessDraft: boolean;
    if (this.satudataUser.role_name === 'walidata') {
      if (validate === 1 || validate === 4) {
        accessDraft = true;
      } else {
        accessDraft = false;
      }
    } else if (
      this.satudataUser.role_name === 'opd' ||
      this.satudataUser.role_name === 'opd-view'
    ) {
      accessDraft = false;
    }

    return accessDraft;
  }

  onDecline(id: number) {
    this.modalIndicatorStatusComponent.openModal(id);
  }

  onUpdate(formData: any) {
    // Notification
    const bodyNotification = { is_enable: false, is_read: true };
    const paramsNotification = {
      type: 'indicator',
      type_id: formData.type_id,
      receiver: this.satudataUser.id,
    };

    const updateNotification = this.notificationService.updateItemBulk(
      bodyNotification,
      paramsNotification,
    );

    // Indicator
    // TODO: activate if backend history_draft ready
    // const bodyIndicator = { id: formData.type_id, history_draft: formData };

    // TODO: change if backend history_draft ready
    const bodyIndicator = { id: formData.type_id };

    assign(bodyIndicator, STATUS_INDICATOR.get('revision-active'));

    const updateIndicator = this.indicatorService.updateItem(
      bodyIndicator.id,
      bodyIndicator,
    );

    zip(updateNotification, updateIndicator)
      .pipe(this.editIndicatorMutation.track())
      .subscribe(() => {
        Swal.fire({
          type: 'success',
          text: `Catatan revisi mapset berhasil terkirim`,
          allowOutsideClick: false,
        }).then(() => {
          this.router.navigate(['/desk'], {
            queryParams: {
              type: 'upload-change',
              status: 'revision',
            },
          });
        });
      });
  }

  onAccept(id: number) {
    Swal.fire({
      type: 'warning',
      text: 'Apakah Anda yakin indikator ini sudah layak dipublikasikan?',
      allowOutsideClick: false,
      showCancelButton: true,
      cancelButtonText: 'Batal',
      confirmButtonColor: '#0753A6',
      confirmButtonText: 'Setuju',
      reverseButtons: true,
    }).then((result) => {
      if (!result.value) {
        return;
      }

      // Notification
      const bodyNotification = { is_enable: false, is_read: true };
      const paramsNotification = {
        type: 'indicator',
        type_id: id,
        receiver: this.satudataUser.id,
      };

      const updateNotification = this.notificationService.updateItemBulk(
        bodyNotification,
        paramsNotification,
      );

      // Indicator
      // TODO: activate if backend history_draft ready
      // const bodyHistory = {
      //   type: 'indicator',
      //   type_id: Number(this.id),
      //   category: 'approve-active',
      //   notes: null,
      // };
      // const bodyIndicator = { id, history_draft: bodyHistory };

      // TODO: change if backend history_draft ready
      const bodyIndicator = { id };

      assign(bodyIndicator, STATUS_INDICATOR.get('approve-active'));

      const updateIndicator = this.indicatorService.updateItem(
        bodyIndicator.id,
        bodyIndicator,
      );

      zip(updateNotification, updateIndicator)
        .pipe(this.editIndicatorMutation.track())
        .subscribe(() => {
          Swal.fire({
            type: 'success',
            text: `Indikator berhasil dipublikasikan`,
            allowOutsideClick: false,
          }).then(() => {
            this.router.navigate(['/desk'], {
              queryParams: {
                type: 'upload-change',
                status: 'approve',
              },
            });
          });
        });
    });
  }

  logPage(type: string): void {
    if (type === 'init') {
      this.pageInfo = {
        page: this.router.url,
        date_start: moment().format('YYYY-MM-DD HH:mm:ss'),
      };

      this.pageLog = {
        log: { ...this.pageInfo, ...this.userInfo },
        token: this.satudataToken,
      };
    } else if (type === 'destroy') {
      if (typeof Worker !== 'undefined') {
        set(
          this.pageLog.log,
          'date_end',
          moment().format('YYYY-MM-DD HH:mm:ss'),
        );

        fromWorker<object, string>(
          () =>
            new Worker(
              new URL('src/app/__v4/workers/log-page.worker', import.meta.url),
              { type: 'module' },
            ),
          of(this.pageLog),
        ).subscribe((message) => {
          if (!environment.production) {
            console.info(`Log page: ${message}`);
          }
        });
      }
    }
  }

  logIndicator(type: string): void {
    if (type === 'init') {
      this.indicatorInfo = {
        page: this.router.url,
        action: 'view',
        status: 'success',
        date_start: moment().format('YYYY-MM-DD HH:mm:ss'),
        indicator_id: this.data.id,
        indicator_title: this.data.title,
        indicator_name: this.data.name,
        indicator_organization_code: this.data.kode_skpd,
        indicator_organization_name: this.data.nama_skpd,
      };

      this.indicatorLog = {
        log: { ...this.indicatorInfo, ...this.userInfo },
        token: this.satudataToken,
      };
    } else if (type === 'destroy') {
      if (typeof Worker !== 'undefined') {
        set(
          this.indicatorLog.log,
          'date_end',
          moment().format('YYYY-MM-DD HH:mm:ss'),
        );

        fromWorker<object, string>(
          () =>
            new Worker(
              new URL(
                'src/app/__v4/workers/log-indicator.worker',
                import.meta.url,
              ),
              {
                type: 'module',
                name: 'indicator',
              },
            ),
          of(this.indicatorLog),
        ).subscribe((message) => {
          if (!environment.production) {
            console.info(`Log indicator: ${message}`);
          }
        });
      }
    }
  }
}
