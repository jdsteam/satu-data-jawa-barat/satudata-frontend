import { Routes } from '@angular/router';

// GUARD
import { AuthGuard } from '@guards-v3';

export const AppRoutingModule: Routes = [
  // PUBLIC
  {
    path: '',
    loadComponent: () =>
      import('@components-v3/container/login/login.component').then(
        (m) => m.LoginComponent,
      ),
    children: [
      {
        path: '',
        redirectTo: 'auth',
        pathMatch: 'full',
      },
      {
        path: 'auth',
        loadChildren: () =>
          import('./authentication/authentication.module').then(
            (m) => m.AuthenticationModule,
          ),
      },
      {
        path: 'autologin_digiteam',
        loadChildren: () =>
          import('./__v4/pages/autologin-digiteam/routes').then(
            (m) => m.AUTOLOGIN_DIGITEAM_ROUTES,
          ),
      },
    ],
  },
  // PRIVATE NO MENU
  {
    path: '',
    loadComponent: () =>
      import('@components-v3/container/no-menu/no-menu.component').then(
        (m) => m.NoMenuComponent,
      ),
    canActivate: [AuthGuard],
    children: [
      {
        path: 'request',
        loadChildren: () =>
          import('./request/request.module').then((m) => m.RequestModule),
      },
    ],
  },
  // PRIVATE ADMIN WITHOUT SIDEBAR
  {
    path: '',
    loadComponent: () =>
      import('@components-v3/container/normal/normal.component').then(
        (m) => m.NormalComponent,
      ),
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full',
      },
      {
        path: 'home',
        loadChildren: () =>
          import('./__v4/pages/home/routes').then((m) => m.HOME_ROUTES),
      },
      {
        path: 'my-data',
        loadChildren: () =>
          import('./__v4/pages/my-data/routes').then((m) => m.MY_DATA_ROUTES),
      },
      {
        path: 'my-dataset',
        loadChildren: () =>
          import('./my-dataset/my-dataset.module').then(
            (m) => m.MyDatasetModule,
          ),
      },
      {
        path: 'my-mapset',
        loadChildren: () =>
          import('./__v4/pages/my-mapset/routes').then(
            (m) => m.MY_MAPSET_ROUTES,
          ),
      },
      {
        path: 'my-indicator',
        loadChildren: () =>
          import('./my-indicator/my-indicator.module').then(
            (m) => m.MyIndicatorModule,
          ),
      },
      {
        path: 'my-visualization',
        loadChildren: () =>
          import('./my-visualization/my-visualization.module').then(
            (m) => m.MyVisualizationModule,
          ),
      },
      {
        path: 'dataset',
        loadChildren: () =>
          import('./__v4/pages/dataset/routes').then((m) => m.DATASET_ROUTES),
      },
      {
        path: 'mapset',
        loadChildren: () =>
          import('./__v4/pages/mapset/routes').then((m) => m.MAPSET_ROUTES),
      },
      {
        path: 'indicator',
        loadChildren: () =>
          import('./__v4/pages/indicator/routes').then(
            (m) => m.INDICATOR_ROUTES,
          ),
      },
      {
        path: 'visualization',
        loadChildren: () =>
          import('./__v4/pages/visualization/routes').then(
            (m) => m.VISUALIZATION_ROUTES,
          ),
      },
      {
        path: 'organization',
        loadChildren: () =>
          import('./__v4/pages/organization/routes').then(
            (m) => m.ORGANIZATION_ROUTES,
          ),
      },
      {
        path: 'history',
        loadChildren: () =>
          import('./history/history.module').then((m) => m.HistoryModule),
      },
      {
        path: 'statistic',
        loadChildren: () =>
          import('./statistic/statistic.module').then((m) => m.StatisticModule),
      },
      {
        path: 'help',
        loadChildren: () =>
          import('./help/help.module').then((m) => m.HelpModule),
      },
      {
        path: 'my-profile',
        loadChildren: () =>
          import('./my-profile/my-profile.module').then(
            (m) => m.MyProfileModule,
          ),
      },
      {
        path: 'desk',
        loadChildren: () =>
          import('./__v4/pages/desk/routes').then((m) => m.DESK_ROUTES),
      },
      {
        path: 'log-activity-user',
        loadChildren: () =>
          import('./log-activity/log-activity.module').then(
            (m) => m.LogActivityModule,
          ),
      },
      {
        path: 'notification',
        loadChildren: () =>
          import('./__v4/pages/notification/routes').then(
            (m) => m.NOTIFICATION_ROUTES,
          ),
      },
      // E-walidata
      // {
      //   path: 'e-walidata',
      //   loadChildren: () =>
      //     import('./__v4/pages/e-walidata/routes').then(
      //       (m) => m.E_WALIDATA_ROUTES,
      //     ),
      // },
      // {
      //   path: 'e-walidata-2',
      //   loadChildren: () =>
      //     import('./__v4/pages/e-walidata-2/routes').then(
      //       (m) => m.E_WALIDATA_ROUTES,
      //     ),
      // },
      // Test
      // {
      //   path: 'application',
      //   loadChildren: () =>
      //     import('./application/application.module').then(
      //       (m) => m.ApplicationModule,
      //     ),
      // },
    ],
  },
  // NOT FOUND
  {
    path: '**',
    redirectTo: '404',
  },
  {
    path: '404',
    loadComponent: () =>
      import('@components-v3/page-not-found/page-not-found.component').then(
        (m) => m.PageNotFoundComponent,
      ),
  },
];
