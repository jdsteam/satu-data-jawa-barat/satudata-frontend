import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Helper
import { StatisticPageGuard } from '@guards-v3';

const routes: Routes = [
  {
    path: '',
    canActivate: [StatisticPageGuard],
    loadChildren: () =>
      import('./statistic-dashboard/statistic-dashboard.module').then(
        (m) => m.StatisticDashboardModule,
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StatisticRoutingModule {}
