import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// Module
import { JdsBiInputModule, JdsBiTableHeadModule } from '@jds-bi/core';
import { JdsBiIconsModule } from '@jds-bi/icons';
import { NgxLoadingModule } from 'ngx-loading';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { NgxDaterangepickerBootstrapModule } from 'ngx-daterangepicker-bootstrap';
import { NgChartsModule } from 'ng2-charts';

import { StatisticDashboardComponent } from './statistic-dashboard.component';

export const routes: Routes = [
  {
    path: '',
    component: StatisticDashboardComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    JdsBiInputModule,
    JdsBiTableHeadModule,
    JdsBiIconsModule,
    NgxLoadingModule,
    NgxSkeletonLoaderModule,
    NgxDaterangepickerBootstrapModule,
    NgChartsModule,
  ],
  declarations: [StatisticDashboardComponent],
  bootstrap: [StatisticDashboardComponent],
})
export class StatisticDashboardModule {}
