import {
  Component,
  OnInit,
  OnDestroy,
  ViewEncapsulation,
  Renderer2,
} from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { ChartOptions, ChartType } from 'chart.js';
import { of } from 'rxjs';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Breadcrumb } from '@interfaces-v4';
// MODEL
import { LoginData } from '@models-v3';

// SERVICE
import { AuthenticationService, StateService } from '@services-v3';
import { GlobalService, DashboardService } from '@services-v4';

// COMPONENT
import {
  JdsBiIconsService,
  iconEye,
  iconStar,
  iconDatabase,
  iconCircleDown,
} from '@jds-bi/icons';
import { jdsBiColor } from '@jds-bi/cdk';

import * as _ from 'lodash';
import moment from 'moment';
import dayjs from 'dayjs';
import { fromWorker } from 'observable-webworker';

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'app-statistic-dashboard',
  templateUrl: './statistic-dashboard.component.html',
  styleUrls: ['./statistic-dashboard.component.css'],
})
export class StatisticDashboardComponent implements OnInit, OnDestroy {
  colorScheme = COLORSCHEME;
  satudataUser: LoginData;
  satudataToken: string;
  moment = moment;
  jds = jds;
  jdsBiColor = jdsBiColor;

  // Log
  log = null;
  pageInfo = null;
  deviceInfo = null;
  userInfo = null;

  // Pengaturan
  title: string;
  label = 'Statistik';
  labeldescription = '';
  breadcrumb: Breadcrumb[] = [{ label: 'Statistik', link: '/statistic' }];

  loading = false;
  loadingStatTotal = false;
  loadingTop = false;
  loadingTopDataset = false;

  dataStatTotal: any[] = [];
  dataTop: any[] = [];
  dataTopDataset: any[] = [];

  tanggalActive = '';
  tanggalawalActive: string;
  tanggalakhirActive: string;
  urutanActive: string;
  direktoriActive: string;

  daterangepicker: any;
  maxDate?: dayjs.Dayjs;
  locale = {
    firstDay: 1,
    startDate: dayjs().startOf('day'),
    endDate: dayjs().endOf('day'),
    format: 'DD MMMM YYYY',
    applyLabel: 'Apply',
    cancelLabel: 'Cancel',
    fromLabel: 'From',
    toLabel: 'To',
  };

  // Chart
  public barChartType: ChartType = 'bar';

  // Top 10 Produsen
  public barChartOptionsProdusen: ChartOptions = {
    responsive: true,
    scales: {
      x: {
        grid: {
          display: false,
        },
      },
      y: {
        beginAtZero: true,
        grid: {
          display: true,
        },
      },
    },
    plugins: {
      title: {
        display: false,
      },
      legend: {
        display: false,
      },
      tooltip: {
        displayColors: false,
        callbacks: {
          title: (tooltipItem) => {
            return `${tooltipItem[0].formattedValue} Dataset`;
          },
          label: (tooltipItem) => {
            const { dataIndex: index } = tooltipItem;
            return _.isNil(this.barChartLabelsProdusen[index]) === false
              ? this.barChartLabelsProdusen[index]
              : null;
          },
        },
      },
    },
  };
  public firstProdusen: number;
  public barChartLabelsAliasProdusen: string[];
  public barChartLabelsProdusen: string[];
  public barChartDataProdusen: any[] = [
    {
      data: [],
      label: 'OPD',
      backgroundColor: '#0753A6',
      hoverBackgroundColor: '#0753A6',
    },
  ];

  // Top 10 Konsumen
  public barChartOptionsKonsumen: ChartOptions = {
    responsive: true,
    scales: {
      x: {
        grid: {
          display: false,
        },
      },
      y: {
        beginAtZero: true,
        grid: {
          display: true,
        },
      },
    },
    plugins: {
      title: {
        display: false,
      },
      legend: {
        display: false,
      },
      tooltip: {
        displayColors: false,
        callbacks: {
          title: (tooltipItem) => {
            return `${tooltipItem[0].formattedValue} Dataset`;
          },
          label: (tooltipItem) => {
            const { dataIndex: index } = tooltipItem;
            return _.isNil(this.barChartLabelsKonsumen[index]) === false
              ? this.barChartLabelsKonsumen[index]
              : null;
          },
        },
      },
    },
  };
  public firstKonsumen: number;
  public barChartLabelsAliasKonsumen: string[];
  public barChartLabelsKonsumen: string[];
  public barChartDataKonsumen: any[] = [
    {
      data: [],
      label: 'OPD',
      backgroundColor: '#0753A6',
      hoverBackgroundColor: '#0753A6',
    },
  ];

  constructor(
    private authenticationService: AuthenticationService,
    private stateService: StateService,
    private globalService: GlobalService,
    private titleService: Title,
    private renderer: Renderer2,
    private dashboardService: DashboardService,
    private iconService: JdsBiIconsService,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    this.renderer.removeClass(document.body, 'satudata');
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
    this.authenticationService.satudataToken.subscribe(
      (x) => (this.satudataToken = x),
    );
    this.stateService.currentLogUser.subscribe((x) => (this.userInfo = x));

    this.title = this.globalService.title;
    this.titleService.setTitle(`${this.label} - ${this.title}`);
    this.globalService.changeLabel(this.label);
    this.globalService.changeLabelDescription(this.labeldescription);
    this.globalService.changeBreadcrumb(this.breadcrumb);

    this.iconService.registerIcons([
      iconEye,
      iconStar,
      iconDatabase,
      iconCircleDown,
    ]);
  }

  ngOnInit() {
    this.logPage('init');
    this.loadingStatTotal = true;
    this.loadingTop = true;
    this.loadingTopDataset = true;

    this.route.queryParams.subscribe((params) => {
      let start = '';
      let end = '';

      if (_.isNil(params.start) === false) {
        this.tanggalawalActive = params.start;
        start = dayjs(params.start).format('DD MMMM YYYY');
      } else {
        this.tanggalawalActive = null;
      }

      if (_.isNil(params.end) === false) {
        this.tanggalakhirActive = params.end;
        end = dayjs(params.end).format('DD MMMM YYYY');
      } else {
        this.tanggalakhirActive = null;
      }

      this.tanggalActive =
        _.isEmpty(start) === false && _.isEmpty(end) === false
          ? `${start} - ${end}`
          : null;

      if (!_.isEmpty(start) && !_.isEmpty(end)) {
        this.daterangepicker = {
          startDate: dayjs(params.start),
          endDate: dayjs(params.end),
        };
      }

      this.urutanActive = _.isNil(params.sort) === false ? params.sort : 'view';
      this.direktoriActive =
        _.isNil(params.dir) === false ? params.dir : 'desc';
    });

    setTimeout(() => {
      this.getDataStatTotal();
      this.getDataTopOrganisasi();
      this.getDataTopDataset();
    }, 250);
  }

  ngOnDestroy(): void {
    this.logPage('destroy');
  }

  // GET =====================================================================================================
  getDataStatTotal() {
    this.loadingStatTotal = true;
    let params = '?';

    // where start_date
    const paramStartDate =
      this.tanggalawalActive !== null
        ? `start_date=${this.tanggalawalActive}`
        : '';

    // where end_date
    const paramEndDate =
      this.tanggalakhirActive !== null
        ? `end_date=${this.tanggalakhirActive}`
        : '';

    params = `${params + paramStartDate}&${paramEndDate}`;

    this.dashboardService.getListStatTotal(params).subscribe((response) => {
      const res = [];
      res.push(response.data);

      const result = [];
      const all = _.assign(response.data, {
        count_all:
          res[0].count_public + res[0].count_private + res[0].count_internal,
      });
      result.push(all);

      this.dataStatTotal = result;
      this.loadingStatTotal = false;
    });
  }

  getDataTopOrganisasi() {
    this.loadingTop = true;
    let params = '?';

    // where start_date
    const paramStartDate =
      this.tanggalawalActive !== null
        ? `start_date=${this.tanggalawalActive}`
        : '';

    // where end_date
    const paramEndDate =
      this.tanggalakhirActive !== null
        ? `end_date=${this.tanggalakhirActive}`
        : '';

    params = `${params + paramStartDate}&${paramEndDate}`;

    this.dashboardService
      .getListTopOrganization(params)
      .subscribe((response) => {
        const res = [];
        res.push(response.data);
        this.dataTop = res;

        const produsenLabel = [];
        const produsenLabelAlias = [];
        const produsenData = [];
        _.map(response.data.opd_produsen, (prod, index: number) => {
          if (index === 0) {
            this.firstProdusen = prod.count_produsen;
          }
          produsenLabel.push(prod.nama_skpd);
          produsenLabelAlias.push(prod.nama_skpd_alias);
          produsenData.push(prod.count_produsen);
        });
        this.barChartLabelsProdusen = produsenLabel;
        this.barChartLabelsAliasProdusen = produsenLabelAlias;
        this.barChartDataProdusen[0].data = produsenData;

        const konsumenLabel = [];
        const konsumenLabelAlias = [];
        const konsumenData = [];
        _.map(response.data.opd_konsumen, (kons, index: number) => {
          if (index === 0) {
            this.firstKonsumen = kons.count_konsumen;
          }
          konsumenLabel.push(kons.nama_skpd);
          konsumenLabelAlias.push(kons.nama_skpd_alias);
          konsumenData.push(kons.count_konsumen);
        });
        this.barChartLabelsKonsumen = konsumenLabel;
        this.barChartLabelsAliasKonsumen = konsumenLabelAlias;
        this.barChartDataKonsumen[0].data = konsumenData;

        this.loadingTop = false;
      });
  }

  getDataTopDataset() {
    this.loadingTopDataset = true;
    let params = '?';

    // where start_date
    const paramStartDate =
      this.tanggalawalActive !== null
        ? `start_date=${this.tanggalawalActive}`
        : '';

    // where end_date
    const paramEndDate =
      this.tanggalakhirActive !== null
        ? `end_date=${this.tanggalakhirActive}`
        : '';

    // sort
    let paramSort = '';
    if (this.urutanActive === 'rating') {
      paramSort = `sort=count_rating:${this.direktoriActive}`;
    } else if (this.urutanActive === 'view') {
      paramSort = `sort=count_view_private:${this.direktoriActive}`;
    } else if (this.urutanActive === 'access') {
      paramSort = `sort=count_access_private:${this.direktoriActive}`;
    }

    params = `${params + paramStartDate}&${paramEndDate}&${paramSort}`;

    this.dashboardService.getListTopDataset(params).subscribe((response) => {
      this.dataTopDataset = response.data;
      this.loadingTopDataset = false;
    });
  }

  // FILTER ==================================================================================================
  filterTanggal($event: any) {
    if (_.isEmpty($event.startDate) || _.isEmpty($event.endDate)) {
      return;
    }

    const start = dayjs($event.startDate).format('YYYY-MM-DD');
    const end = dayjs($event.endDate).format('YYYY-MM-DD');

    const valStart = _.isEmpty(start) === false ? start : null;
    const valEnd = _.isEmpty(end) === false ? end : null;

    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        start: valStart,
        end: valEnd,
      },
      queryParamsHandling: 'merge',
    });

    const s = dayjs($event.startDate).format('DD MMMM YYYY');
    const e = dayjs($event.endDate).format('DD MMMM YYYY');
    this.tanggalActive = `${s} - ${e}`;

    this.tanggalawalActive = valStart;
    this.tanggalakhirActive = valEnd;

    this.getDataStatTotal();
    this.getDataTopOrganisasi();
    this.getDataTopDataset();
  }

  filterUrutkan(value: string) {
    const val = _.isEmpty(value) === false ? value : null;

    let dir = '';
    if (this.direktoriActive === 'desc') {
      dir = 'asc';
    } else if (this.direktoriActive === 'asc') {
      dir = 'desc';
    }

    this.urutanActive = val;
    this.direktoriActive = dir;
    this.getDataTopDataset();
  }

  logPage(type: string): void {
    if (type === 'init') {
      this.pageInfo = {
        page: this.router.url,
        date_start: moment().format('YYYY-MM-DD HH:mm:ss'),
      };

      this.log = {
        log: { ...this.pageInfo, ...this.userInfo },
        token: this.satudataToken,
      };
    } else if (type === 'destroy') {
      if (typeof Worker !== 'undefined') {
        _.set(this.log.log, 'date_end', moment().format('YYYY-MM-DD HH:mm:ss'));

        const log = of(this.log);

        fromWorker<object, string>(
          () =>
            new Worker(
              new URL('src/app/__v4/workers/log-page.worker', import.meta.url),
              { type: 'module' },
            ),
          log,
        ).subscribe((message) => {
          if (!environment.production) {
            console.info(`Log page: ${message}`);
          }
        });
      }
    }
  }
}
