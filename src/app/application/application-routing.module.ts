import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      // {
      //   path: ':application',
      //   loadChildren: () => import('./application-detail/application-detail.module').then(m => m.ApplicationDetailModule)
      // },
      {
        path: '',
        loadChildren: () =>
          import('./application-list/application-list.module').then(
            (m) => m.ApplicationListModule,
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ApplicationRoutingModule {}
