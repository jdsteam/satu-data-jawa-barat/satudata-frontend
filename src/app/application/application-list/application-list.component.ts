import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import {
  UntypedFormGroup,
  UntypedFormControl,
  Validators,
} from '@angular/forms';

// STYLE
import * as jds from '@styles';

// SERVICE
import { DatasetService } from '@services-v3';
import { GlobalService } from '@services-v4';

// PLUGIN
import moment from 'moment';
import accounting from 'accounting-js';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
declare const $: any;
// eslint-disable-next-line @typescript-eslint/naming-convention
declare const x_spreadsheet: any;

@Component({
  selector: 'app-application-list',
  templateUrl: './application-list.component.html',
  styleUrls: ['./application-list.component.scss'],
})
export class ApplicationListComponent implements OnInit {
  moment: any = moment;
  jds: any = jds;

  // Variable
  loading = false;
  myForm: UntypedFormGroup;

  public start = 'Year';
  public depth = 'Year';
  public format = 'MMMM y';

  acc1: any;
  acc2: any;
  acc3: any;
  acc4: any;
  acc5: any;
  acc6: any;
  acc7: any;

  spreadsheet: any;
  spreadsheetConfig = {
    num_row: 10000,
    num_col: 52,
  };

  // Setting
  title: string;
  label = 'Aplikasi';
  labeldescription = `Aplikasi lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do \
  eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`;
  breadcrumb: any[] = [{ label: 'Aplikasi', link: '/application' }];

  constructor(
    private globalService: GlobalService,
    private datasetService: DatasetService,
    private titleService: Title,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    this.title = this.globalService.title;
    this.titleService.setTitle(`${this.label} - ${this.title}`);
    this.globalService.changeLabel(this.label);
    this.globalService.changeLabelDescription(this.labeldescription);
    this.globalService.changeBreadcrumb(this.breadcrumb);
  }

  ngOnInit() {
    this.myForm = new UntypedFormGroup({
      name: new UntypedFormControl({ value: null, disabled: false }, [
        Validators.required,
        Validators.maxLength(255),
      ]),
      rumus: new UntypedFormControl({ value: null, disabled: false }, [
        Validators.required,
      ]),
      datetime: new UntypedFormControl({ value: null, disabled: false }, [
        Validators.required,
      ]),
    });

    this.myForm.patchValue({
      datetime: new Date('2021-01-01'),
      rumus: '',
    });

    this.acc1 = accounting.unformat('£ 12,345,678.90 GBP');
    this.acc7 = accounting.unformat('Rp12.345.678,90', ',');
    this.acc2 = accounting.unformat('100000', '.');
    this.acc3 = accounting.unformat('100000.2', '.');
    this.acc4 = accounting.unformat('100000,2', ',');
    this.acc5 = accounting.unformat('100,000.2', '.');
    this.acc6 = accounting.unformat('100.000,2', ',');

    this.initSpreadsheet();
  }

  get f() {
    return this.myForm.controls;
  }

  initSpreadsheet(): void {
    this.spreadsheet = x_spreadsheet('#input-spreadsheet', {
      mode: 'edit',
      showToolbar: false,
      showGrid: true,
      showContextmenu: true,
      view: {
        height: () => document.documentElement.clientHeight - 22,
        width: () => document.documentElement.clientWidth - 80,
      },
      row: {
        len: this.spreadsheetConfig.num_row,
        height: 25,
      },
      col: {
        len: this.spreadsheetConfig.num_col,
        width: 100,
        indexWidth: 60,
        minWidth: 60,
      },
      style: {
        bgcolor: '#ffffff',
        align: 'left',
        valign: 'middle',
        textwrap: false,
        strike: false,
        underline: false,
        color: '#0a0a0a',
        font: {
          name: 'Helvetica',
          size: 10,
          bold: false,
          italic: false,
        },
      },
    });
  }

  onSubmit() {
    const dataJson = this.datasetService.getDataJson(
      this.spreadsheet,
      this.spreadsheetConfig,
    );

    console.info(dataJson);
  }
}
