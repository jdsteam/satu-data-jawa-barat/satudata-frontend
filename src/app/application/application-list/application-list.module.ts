import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '@core-v3/shared.module';

import { NgxLoadingModule } from 'ngx-loading';
import { ToastrModule } from 'ngx-toastr';

import { ApplicationListComponent } from './application-list.component';

export const routes: Routes = [
  {
    path: '',
    component: ApplicationListComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    NgxLoadingModule.forRoot({}),
    ToastrModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  declarations: [ApplicationListComponent],
  bootstrap: [ApplicationListComponent],
})
export class ApplicationListModule {}
