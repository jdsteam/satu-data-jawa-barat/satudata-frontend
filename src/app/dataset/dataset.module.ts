import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DatasetRoutingModule } from './dataset-routing.module';

@NgModule({
  imports: [CommonModule, DatasetRoutingModule],
  declarations: [],
})
export class DatasetModule {}
