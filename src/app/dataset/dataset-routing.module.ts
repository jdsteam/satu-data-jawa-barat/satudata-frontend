import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// GUARDS
import {
  StoreTopicListGuard,
  StoreDatasetDetailGuard,
  DatasetReviewGuard,
} from '@guards-v3';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        canActivate: [StoreTopicListGuard],
        loadChildren: () =>
          import('./containers/dataset-page/dataset-page.module').then(
            (m) => m.DatasetPageModule,
          ),
      },
      {
        path: 'detail/:id',
        canActivate: [StoreDatasetDetailGuard],
        loadChildren: () =>
          import('./containers/dataset-detail/dataset-detail.module').then(
            (m) => m.DatasetDetailModule,
          ),
      },
      {
        path: 'review/:id',
        canActivate: [DatasetReviewGuard],
        loadChildren: () =>
          import('./containers/dataset-review/dataset-review.module').then(
            (m) => m.DatasetReviewModule,
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DatasetRoutingModule {}
