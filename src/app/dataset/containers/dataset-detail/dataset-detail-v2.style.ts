import { Injectable } from '@angular/core';

// PACKAGE
import { css } from '@emotion/css';

@Injectable()
export class DatasetDetailV2Style {
  getDynamicStyle(): any {
    const navLink = css`
      padding-left: 30px;
      padding-right: 30px;
    `;

    const navPills = css`
      display: inline-flex;
      background-color: #f4f5f7;
      padding: 2px 4px;
      border-radius: 8px;
      margin-bottom: 16px;
    `;

    return {
      navLink,
      navPills,
    };
  }
}
