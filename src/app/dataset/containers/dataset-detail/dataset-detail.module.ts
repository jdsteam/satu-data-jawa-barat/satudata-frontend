import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';

// MODULE
import { JdsBiIconsModule } from '@jds-bi/icons';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

import { BreadcrumbComponent } from '@components-v4';
import { HeaderV2Component } from '@components-v3/detail/dataset/header-v2/header-v2.component';
import { DescriptionV2Component } from '@components-v3/detail/dataset/description-v2/description-v2.component';
import { PreviewTableV2Component } from '@components-v3/detail/dataset/preview-table-v2/preview-table-v2.component';
import { PreviewGraphComponent } from '@components-v3/detail/dataset/preview-graph/preview-graph.component';
import { PreviewMapComponent } from '@components-v3/detail/dataset/preview-map/preview-map.component';
import { DraftActionComponent } from '@components-v3/detail/dataset/draft-action/draft-action.component';
import { DiscontinueActionComponent } from '@components-v3/detail/dataset/discontinue-action/discontinue-action.component';

import { DatasetDetailV2Component } from './dataset-detail-v2.component';

export const routes: Routes = [
  {
    path: '',
    component: DatasetDetailV2Component,
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    JdsBiIconsModule,
    NgxSkeletonLoaderModule,
    BreadcrumbComponent,
    HeaderV2Component,
    DescriptionV2Component,
    PreviewTableV2Component,
    PreviewGraphComponent,
    PreviewMapComponent,
    DraftActionComponent,
    DiscontinueActionComponent,
  ],
  declarations: [DatasetDetailV2Component],
  bootstrap: [DatasetDetailV2Component],
})
export class DatasetDetailModule {}
