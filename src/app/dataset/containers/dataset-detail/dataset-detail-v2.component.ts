/* eslint-disable no-param-reassign */
import { Component, OnDestroy, OnInit, Renderer2 } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DomSanitizer, Title } from '@angular/platform-browser';
import { Subject, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { filter, takeUntil, take } from 'rxjs/operators';
import { select, Store } from '@ngrx/store';
import { Actions } from '@ngrx/effects';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { WHATSAPP } from '@constants-v4';
// INTERFACE
import { Breadcrumb } from '@interfaces-v4';
// MODEL
import { LoginData, DatasetData } from '@models-v3';
// SERVICE
import {
  AuthenticationService,
  StateService,
  CoreDataService,
} from '@services-v3';
import { GlobalService } from '@services-v4';
// JDS-BI
import { JdsBiIconsService, iconBan, iconUserLock } from '@jds-bi/icons';

// STORE
import { selectDataset } from '@store-v3/dataset/dataset.selectors';

import * as _ from 'lodash';
import moment from 'moment';
import { fromWorker } from 'observable-webworker';

import { DatasetDetailV2Style } from './dataset-detail-v2.style';

@Component({
  selector: 'app-dataset-detail-v2',
  templateUrl: './dataset-detail-v2.component.html',
  providers: [DatasetDetailV2Style],
})
export class DatasetDetailV2Component implements OnInit, OnDestroy {
  satudataUser: LoginData;
  satudataToken: string;
  whatsapp = WHATSAPP;
  moment = moment;
  jds = jds;

  className!: any;

  // Log
  pageLog = null;
  datasetLog = null;

  pageInfo = null;
  datasetInfo = null;
  userInfo = null;

  // Pengaturan
  title: string;
  label = 'Dataset';
  breadcrumb: Breadcrumb[] = [
    { label: 'Dataset', link: '/dataset' },
    { label: 'Detail', link: '/dataset/detail' },
  ];

  // Variable
  id: number;
  accessBigData = false;
  accessinfoBigData: string;
  urlCoreData: string;
  schema: string;
  table: string;
  isRealtime: boolean;

  filter = {
    preview: 'table',
  };

  loadingAccess = false;

  // Data
  data: DatasetData;

  private unsubscribeDataset$ = new Subject<void>();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private globalService: GlobalService,
    private stateService: StateService,
    private coreDataService: CoreDataService,
    private iconService: JdsBiIconsService,
    private titleService: Title,
    private renderer: Renderer2,
    private store: Store<any>,
    private actions$: Actions,
    private sanitizer: DomSanitizer,
    private datasetDetailStyle: DatasetDetailV2Style,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
    this.authenticationService.satudataToken.subscribe(
      (x) => (this.satudataToken = x),
    );
    this.stateService.currentLogUser.subscribe((x) => (this.userInfo = x));

    // id
    this.id = this.route.snapshot.params.id;

    this.iconService.registerIcons([iconBan, iconUserLock]);
  }

  settingsAll() {
    this.renderer.removeClass(document.body, 'satudata');

    this.title = this.globalService.title;
    this.titleService.setTitle(`${this.label} - ${this.title}`);
    this.globalService.changeLabel(this.label);
    this.globalService.changeBreadcrumb(this.breadcrumb);
  }

  ngOnInit() {
    this.className = this.datasetDetailStyle.getDynamicStyle();

    this.settingsAll();
    this.getDataset();
    this.logPage('init');
    this.logDataset('init');
  }

  ngOnDestroy() {
    this.logPage('destroy');
    this.logDataset('destroy');
    this.unsubscribeDataset$.next();
    this.unsubscribeDataset$.complete();
  }

  getDataset() {
    this.store
      .pipe(
        takeUntil(this.unsubscribeDataset$),
        select(selectDataset(this.id)),
        filter((val) => val !== undefined),
        take(1),
      )
      .subscribe((result) => {
        this.data = result;

        // Dataset Access
        this.schema = result.schema;
        this.table = result.table;
        this.urlCoreData = `/${this.schema}/${this.table}`;

        const canAccess = result.can_access_new;

        if (canAccess) {
          this.accessBigData = true;
          this.getCoreData();
        } else {
          this.accessBigData = false;

          if (
            result.can_access_new_info.message === 'Schema and Table not found.'
          ) {
            this.accessinfoBigData = 'not_found';
          } else {
            this.accessinfoBigData = 'can_access';
          }
        }

        // Dataset Is Realtime
        this.isRealtime = result.is_realtime;
      });
  }

  getCoreData() {
    this.loadingAccess = true;
    this.coreDataService
      .getListV2(this.urlCoreData, {})
      .toPromise()
      .then((result) => {
        this.loadingAccess = false;
        this.accessBigData = true;

        this.stateService.changeDatasetDetail({
          id: this.id,
          url: this.urlCoreData,
          schema: this.schema,
          table: this.table,
          meta: result.meta,
          ...this.getMetadata(result.data, result.metadata_filter),
        });
      })
      .catch(() => {
        this.loadingAccess = false;
        this.accessBigData = false;
        this.accessinfoBigData = 'not_found';
      });
  }

  getMetadata(data: any, metadata: any) {
    const inputSearch = [];
    const inputSelect = [];

    // eslint-disable-next-line array-callback-return
    metadata.map((meta) => {
      const column50 = data.some((obj) => {
        const value = obj[meta.key] ? obj[meta.key].toString() : '';
        return value.length > 50;
      });
      const column40 = data.some((obj) => {
        const value = obj[meta.key] ? obj[meta.key].toString() : '';
        return value.length > 40;
      });
      const column30 = data.some((obj) => {
        const value = obj[meta.key] ? obj[meta.key].toString() : '';
        return value.length > 30;
      });

      if (column50) {
        meta.column = 'long50';
      } else if (column40) {
        meta.column = 'long40';
      } else if (column30) {
        meta.column = 'long30';
      } else {
        meta.column = 'short';
      }

      if (meta.type === 'free text') {
        meta.type = 'freetext';
        meta.disabled = false;

        inputSearch.push({ column: meta.key, value: null });
      } else if (meta.type === 'select') {
        if (meta.value.length > 1) {
          meta.disabled = false;
        } else {
          meta.disabled = true;
        }

        const options = meta.value.map((x) => {
          let value: any;
          let label: string;
          if (x === null) {
            value = null;
            label = 'null';
          } else {
            value = x.toString();
            label = x.toString();
          }

          return { value, label };
        });

        meta.value = options;

        inputSelect.push({ column: meta.key, value: [], all: false });
      }
    });

    return {
      metadata,
      search: inputSearch,
      select: inputSelect,
    };
  }

  filterPreview(event: string) {
    const value = _.isEmpty(event) === false ? event : null;
    this.filter.preview = value;
  }

  logPage(type: string): void {
    if (type === 'init') {
      this.pageInfo = {
        page: this.router.url,
        date_start: moment().format('YYYY-MM-DD HH:mm:ss'),
      };

      this.pageLog = {
        log: { ...this.pageInfo, ...this.userInfo },
        token: this.satudataToken,
      };
    } else if (type === 'destroy') {
      if (typeof Worker !== 'undefined') {
        _.set(
          this.pageLog.log,
          'date_end',
          moment().format('YYYY-MM-DD HH:mm:ss'),
        );

        fromWorker<object, string>(
          () =>
            new Worker(
              new URL('src/app/__v4/workers/log-page.worker', import.meta.url),
              { type: 'module' },
            ),
          of(this.pageLog),
        ).subscribe((message) => {
          if (!environment.production) {
            console.info(`Log page: ${message}`);
          }
        });
      }
    }
  }

  logDataset(type: string): void {
    if (type === 'init') {
      this.datasetInfo = {
        page: this.router.url,
        action: 'view',
        status: 'success',
        date_start: moment().format('YYYY-MM-DD HH:mm:ss'),
        dataset_id: this.data.id,
        dataset_title: this.data.title,
        dataset_name: this.data.name,
        dataset_organization_code: this.data.skpd.kode_skpd,
        dataset_organization_name: this.data.skpd.nama_skpd,
      };

      this.datasetLog = {
        log: { ...this.datasetInfo, ...this.userInfo },
        token: this.satudataToken,
      };
    } else if (type === 'destroy') {
      if (typeof Worker !== 'undefined') {
        _.set(
          this.datasetLog.log,
          'date_end',
          moment().format('YYYY-MM-DD HH:mm:ss'),
        );

        fromWorker<object, string>(
          () =>
            new Worker(
              new URL(
                'src/app/__v4/workers/log-dataset.worker',
                import.meta.url,
              ),
              {
                type: 'module',
                name: 'dataset',
              },
            ),
          of(this.datasetLog),
        ).subscribe((message) => {
          if (!environment.production) {
            console.info(`Log dataset: ${message}`);
          }
        });
      }
    }
  }
}
