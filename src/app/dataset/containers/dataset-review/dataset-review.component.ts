import { Component, OnInit, OnDestroy, Renderer2 } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import {
  UntypedFormGroup,
  UntypedFormControl,
  Validators,
} from '@angular/forms';
import { of } from 'rxjs';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Breadcrumb } from '@interfaces-v4';

// MODEL
import { LoginData, DatasetData, ReviewDatasetData } from '@models-v3';

// SERVICE
import {
  AuthenticationService,
  StateService,
  DatasetService,
  ReviewDatasetService,
} from '@services-v3';
import { GlobalService, NotificationService } from '@services-v4';

import * as _ from 'lodash';
import moment from 'moment';
import Swal from 'sweetalert2';
import { fromWorker } from 'observable-webworker';

moment.locale('id');

@Component({
  selector: 'app-dataset-review',
  templateUrl: './dataset-review.component.html',
})
export class DatasetReviewComponent implements OnInit, OnDestroy {
  colorScheme = COLORSCHEME;
  satudataUser: LoginData;
  satudataToken: string;
  moment = moment;
  jds = jds;

  // Log
  log = null;
  pageInfo = null;
  deviceInfo = null;
  userInfo = null;

  // Pengaturan
  title: string;
  label = 'Dataset';
  breadcrumb: Breadcrumb[] = [
    { label: 'Dataset', link: '/dataset' },
    { label: 'Ulasan', link: '/dataset/review' },
  ];

  loading = false;
  loadingDataset = false;
  submitted = false;
  myForm: UntypedFormGroup;

  id: number;
  dataRating = 0;
  dataDataset: DatasetData;
  dataReview: ReviewDatasetData;

  constructor(
    private authenticationService: AuthenticationService,
    private globalService: GlobalService,
    private datasetService: DatasetService,
    private reviewDatasetService: ReviewDatasetService,
    private notificationService: NotificationService,
    private stateService: StateService,
    private titleService: Title,
    private renderer: Renderer2,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    this.renderer.removeClass(document.body, 'satudata');
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
    this.authenticationService.satudataToken.subscribe(
      (x) => (this.satudataToken = x),
    );
    this.stateService.currentLogUser.subscribe((x) => (this.userInfo = x));

    this.title = this.globalService.title;
    this.titleService.setTitle(`${this.label} - ${this.title}`);
    this.globalService.changeLabel(this.label);
    this.globalService.changeBreadcrumb(this.breadcrumb);
  }

  ngOnInit() {
    this.logPage('init');

    this.loadingDataset = true;

    this.route.params.subscribe(() => {
      this.id = this.route.snapshot.params.id;
      this.getDataset();
    });

    this.myForm = new UntypedFormGroup({
      type: new UntypedFormControl(),
      type_id: new UntypedFormControl(),
      user_id: new UntypedFormControl(),
      rating: new UntypedFormControl({ value: null, disabled: false }, [
        Validators.required,
      ]), // rating
      review: new UntypedFormControl({ value: null, disabled: false }, [
        Validators.required,
        Validators.maxLength(255),
      ]),
    });
  }

  ngOnDestroy(): void {
    this.logPage('destroy');
  }

  // Validation ==========================================================================================================
  get rating() {
    return this.myForm.get('rating');
  }

  get review() {
    return this.myForm.get('review');
  }

  onRate(event: any) {
    this.dataRating = event;
    this.myForm.patchValue({
      rating: event,
    });
  }

  getDataset() {
    this.loadingDataset = true;
    this.datasetService.getSingle(this.id, {}).subscribe((response) => {
      this.dataDataset = response.data;
      this.loadingDataset = false;
    });
  }

  // Save Review ========================================================================================================
  onSubmit() {
    this.loading = true;
    this.submitted = true;

    const bodyReview = this.myForm.value;

    _.assign(bodyReview, {
      type: 'dataset',
      type_id: this.id,
      user_id: this.satudataUser.id,
    });

    this.reviewDatasetService.createItem(bodyReview).subscribe((response) => {
      this.dataReview = response.data;

      const { type: datasetType, type_id: datasetId } = response.data;

      // Update notification
      const bodyNotification = { is_enable: false };
      const params = {
        type: datasetType,
        type_id: Number(datasetId),
        receiver: this.satudataUser.id,
      };

      this.notificationService
        .updateItemBulk(bodyNotification, params)
        .toPromise();

      this.loading = false;
      this.showSuccess(datasetId);
    });
  }

  showSuccess(datasetId) {
    Swal.fire({
      text: 'Ulasan berhasil dibuat',
      type: 'success',
      allowOutsideClick: false,
    }).then(() => {
      this.router.navigate(['/dataset/detail/', datasetId]);
    });
  }

  logPage(type: string): void {
    if (type === 'init') {
      this.pageInfo = {
        page: this.router.url,
        date_start: moment().format('YYYY-MM-DD HH:mm:ss'),
      };

      this.log = {
        log: { ...this.pageInfo, ...this.userInfo },
        token: this.satudataToken,
      };
    } else if (type === 'destroy') {
      if (typeof Worker !== 'undefined') {
        _.set(this.log.log, 'date_end', moment().format('YYYY-MM-DD HH:mm:ss'));

        const log = of(this.log);

        fromWorker<object, string>(
          () =>
            new Worker(
              new URL('src/app/__v4/workers/log-page.worker', import.meta.url),
              { type: 'module' },
            ),
          log,
        ).subscribe((message) => {
          if (!environment.production) {
            console.info(`Log page: ${message}`);
          }
        });
      }
    }
  }
}
