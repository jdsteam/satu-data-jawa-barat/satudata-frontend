import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Module
import { JdsBiInputModule } from '@jds-bi/core';
import { NgxLoadingModule } from 'ngx-loading';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

import { BreadcrumbComponent } from '@components-v4';
import { StarRatingComponent } from '@components-v3/star-rating/star-rating.component';

import { DatasetReviewComponent } from './dataset-review.component';

export const routes: Routes = [
  {
    path: '',
    component: DatasetReviewComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    JdsBiInputModule,
    NgxLoadingModule,
    NgxSkeletonLoaderModule,
    BreadcrumbComponent,
    StarRatingComponent,
  ],
  declarations: [DatasetReviewComponent],
  bootstrap: [DatasetReviewComponent],
})
export class DatasetReviewModule {}
