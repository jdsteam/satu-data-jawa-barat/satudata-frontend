import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LetDirective } from '@ngrx/component';
import { JdsBiInputModule } from '@jds-bi/core';
import { NgxLoadingModule } from 'ngx-loading';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';

import { MyProfileViewComponent } from './my-profile-view.component';

export const routes: Routes = [
  {
    path: '',
    component: MyProfileViewComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    JdsBiInputModule,
    FormsModule,
    ReactiveFormsModule,
    LetDirective,
    NgxLoadingModule,
    NgxSkeletonLoaderModule,
  ],
  declarations: [MyProfileViewComponent],
  bootstrap: [MyProfileViewComponent],
})
export class MyProfileViewModule {}
