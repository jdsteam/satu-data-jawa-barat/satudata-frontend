import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import {
  UntypedFormGroup,
  Validators,
  UntypedFormBuilder,
} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
import { Store } from '@ngrx/store';
import { Actions } from '@ngrx/effects';
// MODEL
import { UserData } from '@models-v4';
import { LoginData } from '@models-v3';
// SERVICE
import { AuthenticationService, StateService, UserService } from '@services-v3';
// UTIL
import { isValidHttpUrl } from '@utils-v4';
// VALIDATOR
import { MustMatch } from '@validators-v4';

import * as _ from 'lodash';
import moment from 'moment';
import Swal from 'sweetalert2';
import { fromWorker } from 'observable-webworker';

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'app-my-profile-view',
  templateUrl: './my-profile-view.component.html',
})
export class MyProfileViewComponent implements OnInit, OnDestroy {
  colorScheme = COLORSCHEME;
  satudataUser: LoginData;
  satudataToken: string;
  jds = jds;

  // Log
  log = null;
  pageInfo = null;
  deviceInfo = null;
  userInfo = null;

  id: number;
  myForm: UntypedFormGroup;
  isChangePassword = false;

  // Data
  data: UserData;
  isLoading: boolean;
  isLoadingUpdate: boolean;

  changePassword$: Observable<UserData[]>;
  loadingChangePassword$: Observable<boolean>;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private stateService: StateService,
    private userService: UserService,
    private formBuilder: UntypedFormBuilder,
    private store: Store<any>,

    public action$: Actions,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
    this.authenticationService.satudataToken.subscribe(
      (x) => (this.satudataToken = x),
    );
    this.stateService.currentLogUser.subscribe((x) => (this.userInfo = x));

    // id
    this.id = this.satudataUser.id;
  }

  ngOnInit() {
    this.myForm = this.formBuilder.group(
      {
        old_password: ['', Validators.required],
        new_password: ['', [Validators.required, Validators.minLength(6)]],
        confirm_password: ['', Validators.required],
      },
      {
        validator: MustMatch('new_password', 'confirm_password'),
      },
    );

    this.getUser();
    this.logPage('init');
  }

  ngOnDestroy(): void {
    this.logPage('destroy');
  }

  get profileImage(): string {
    if (isValidHttpUrl(this.satudataUser.profile_pic)) {
      return this.satudataUser.profile_pic;
    }

    return environment.backendURL + this.satudataUser.profile_pic;
  }

  // get data
  get f() {
    return this.myForm.controls;
  }

  getUser() {
    const params = ``;

    this.isLoading = true;
    this.userService
      .getSingle(this.id, params)
      .toPromise()
      .then((result) => {
        if (result.error === 0) {
          this.isLoading = false;
          this.data = result.data;
        }
      });
  }

  onSubmit() {
    const body = {
      old: this.f.old_password.value,
      password: this.f.new_password.value,
    };

    this.isLoadingUpdate = true;

    this.userService.updatePassword(this.id, body, {}).subscribe((result) => {
      this.isLoadingUpdate = false;
      if (result.error === 1) {
        this.showMessage('warning', 'Ubah Password gagal.');
      } else if (result.error === 0) {
        this.isChangePassword = false;
        this.showMessage('success', 'Ubah Password berhasil.');
      }
    });
  }

  showMessage(type: string, msg: string) {
    Swal.fire({
      type: type === 'warning' ? 'warning' : 'success',
      text: msg,
      allowOutsideClick: false,
    }).then((result) => {
      if (result.value) {
        if (type === 'success') {
          this.myForm.reset();
        }
      }
    });
  }

  toggleChangePassword(isChangePassword: boolean) {
    this.isChangePassword = !isChangePassword;
  }

  logPage(type: string): void {
    if (type === 'init') {
      this.pageInfo = {
        page: this.router.url,
        date_start: moment().format('YYYY-MM-DD HH:mm:ss'),
      };

      this.log = {
        log: { ...this.pageInfo, ...this.userInfo },
        token: this.satudataToken,
      };
    } else if (type === 'destroy') {
      if (typeof Worker !== 'undefined') {
        _.set(this.log.log, 'date_end', moment().format('YYYY-MM-DD HH:mm:ss'));

        const log = of(this.log);

        fromWorker<object, string>(
          () =>
            new Worker(
              new URL('src/app/__v4/workers/log-page.worker', import.meta.url),
              { type: 'module' },
            ),
          log,
        ).subscribe((message) => {
          if (!environment.production) {
            console.info(`Log page: ${message}`);
          }
        });
      }
    }
  }
}
