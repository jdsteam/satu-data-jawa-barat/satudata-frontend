import {
  LOCALE_ID,
  NgModule,
  ENVIRONMENT_INITIALIZER,
  inject,
} from '@angular/core';
import { registerLocaleData } from '@angular/common';
import { BrowserModule, Title } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  provideRouter,
  RouterModule,
  withComponentInputBinding,
} from '@angular/router';
import {
  provideHttpClient,
  withInterceptorsFromDi,
} from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ServiceWorkerModule } from '@angular/service-worker';
import localeId from '@angular/common/locales/id';
import { environment } from 'src/environments/environment';

// Store
import { NgrxModule } from '@store-v3/ngrx.module';

// Service
import { StateService } from '@services-v3';
import { GlobalService } from '@services-v4';

// Module
import { JdsBiRootModule } from '@jds-bi/core';

// Plugin
import { JwtModule } from '@auth0/angular-jwt';
import { NgSelectModule } from '@ng-select/ng-select';
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';
import { ToastrModule } from 'ngx-toastr';
import { NgxLoadingModule, ngxLoadingAnimationTypes } from 'ngx-loading';
import { QueryClientService, provideQueryClientOptions } from '@ngneat/query';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import {
  NgxDaterangepickerBootstrapModule,
  NgxDaterangepickerLocaleService,
} from 'ngx-daterangepicker-bootstrap';
import { NgxPopperjsModule } from 'ngx-popperjs';
// import {
//   OpenTelemetryInterceptorModule,
//   OtelColExporterModule,
//   CompositePropagatorModule,
// } from '@jufab/opentelemetry-angular-interceptor';

// App
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

registerLocaleData(localeId);

export function tokenGetter() {
  return localStorage.getItem('satudata-token');
}

// TODO: signoz
// const openTelemetry =
//   environment.environment === 'development'
//     ? []
//     : [
//         OpenTelemetryInterceptorModule.forRoot({
//           commonConfig: {
//             console: !environment.production,
//             production: environment.production,
//             serviceName: `Satu Data Jabar Frontend ${
//               environment.production ? 'Production' : 'Staging'
//             }`,
//             probabilitySampler: '1',
//           },
//           otelcolConfig: {
//             url: environment.signozURL,
//           },
//         }),
//         OtelColExporterModule,
//         CompositePropagatorModule,
//       ];

export const options: Partial<null | IConfig> | (() => Partial<IConfig>) = null;

@NgModule({
  declarations: [AppComponent],
  bootstrap: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(AppRoutingModule, {
      bindToComponentInputs: true,
      scrollPositionRestoration: 'top',
    }),
    FormsModule,
    ReactiveFormsModule,
    JwtModule.forRoot({
      config: {
        tokenGetter,
        allowedDomains: [
          'localhost:4300',
          '0.0.0.0:5002',
          '0.0.0.0:5003',
          'localhost:5004',
          'satudata.jabarprov.go.id',
          'satudata.digitalservice.id',
          'satudata-dev.digitalservice.id',
          'satudata.ekosistemdatajabar.id',
          'data.jabarprov.go.id',
          'data.digitalservice.id',
          'data-dev.digitalservice.id',
          'data.ekosistemdatajabar.id',
          'satudata-socket.digitalservice.id',
          'satudata-socket-dev.digitalservice.id',
          'coredata.digitalservice.id',
          'coredata-dev.digitalservice.id',
        ],
        disallowedRoutes: [
          'localhost:4300/api-auth/auth/login',
          'satudata.jabarprov.go.id/api-auth/auth/login',
          'satudata.digitalservice.id/api-auth/auth/login',
          'satudata-dev.digitalservice.id/api-auth/auth/login',
          'satudata.ekosistemdatajabar.id/api-auth/auth/login',
          'data.jabarprov.go.id/api-auth/auth/login',
          'data.digitalservice.id/api-auth/auth/login',
          'data-dev.digitalservice.id/api-auth/auth/login',
          'data.ekosistemdatajabar.id/api-auth/auth/login',
          'localhost:4300/api-backend/auth/login2',
          'satudata.jabarprov.go.id/api-backend/auth/login2',
          'satudata.digitalservice.id/api-backend/auth/login2',
          'satudata-dev.digitalservice.id/api-backend/auth/login2',
          'satudata.ekosistemdatajabar.id/api-backend/auth/login2',
          'data.jabarprov.go.id/api-backend/auth/login2',
          'data.digitalservice.id/api-backend/auth/login2',
          'data-dev.digitalservice.id/api-backend/auth/login2',
          'data.ekosistemdatajabar.id/api-backend/auth/login2',
        ],
      },
    }),
    JdsBiRootModule,
    NgrxModule,
    NgSelectModule,
    LoadingBarRouterModule,
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-left',
      preventDuplicates: true,
      progressBar: true,
    }),
    NgxLoadingModule.forRoot({
      animationType: ngxLoadingAnimationTypes.wanderingCubes,
      backdropBackgroundColour: 'rgba(0,0,0,0.5)',
      backdropBorderRadius: '4px',
      fullScreenBackdrop: true,
      primaryColour: '#FDCC29',
      secondaryColour: '#289D56',
      tertiaryColour: '#28B8F0',
    }),
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      registrationStrategy: 'registerWhenStable:30000',
    }),
    NgxMaskModule.forRoot(),
    NgxDaterangepickerBootstrapModule.forRoot(),
    NgxPopperjsModule.forRoot(),
  ],
  providers: [
    provideRouter(AppRoutingModule, withComponentInputBinding()),
    provideHttpClient(withInterceptorsFromDi()),
    {
      provide: LOCALE_ID,
      useValue: 'id',
    },
    provideQueryClientOptions({
      defaultOptions: {
        queries: {
          refetchOnWindowFocus: false,
        },
      },
    }),
    environment.production
      ? []
      : {
          provide: ENVIRONMENT_INITIALIZER,
          multi: true,
          useValue() {
            const queryClient = inject(QueryClientService);
            import('@ngneat/query-devtools').then((m) => {
              m.ngQueryDevtools({ queryClient });
            });
          },
        },
    Title,
    GlobalService,
    StateService,
    NgxDaterangepickerLocaleService,
  ],
})
export class AppModule {}
