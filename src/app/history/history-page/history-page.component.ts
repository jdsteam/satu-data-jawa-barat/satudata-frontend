import {
  Component,
  OnInit,
  OnDestroy,
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';

// INTERFACE
import { Breadcrumb } from '@interfaces-v4';

// MODEL
import { LoginData } from '@models-v3';

// SERVICE
import { AuthenticationService, StateService } from '@services-v3';
import { GlobalService } from '@services-v4';

import * as _ from 'lodash';
import moment from 'moment';
import { fromWorker } from 'observable-webworker';

export interface Filter {
  sort: string;
  perPage: number;
  currentPage: number;
  search: string;
  status: string;
  data: string;
  isFirst: boolean;
}

@Component({
  selector: 'app-history-page',
  templateUrl: './history-page.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HistoryPageComponent implements OnInit, OnDestroy, AfterViewInit {
  satudataUser: LoginData;
  satudataToken: string;
  moment = moment;
  jds = jds;

  // Log
  log = null;
  pageInfo = null;
  deviceInfo = null;
  userInfo = null;

  // Setting
  title: string;
  label = 'Riwayat';
  labelHelper: string;
  breadcrumb: Breadcrumb[] = [{ label: 'Riwayat', link: '/history' }];

  filter = {
    sort: 'newest',
    perPage: 10,
    currentPage: 1,
    search: '',
    status: 'view',
    data: 'dataset',
    isFirst: true,
  };

  constructor(
    private cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private globalService: GlobalService,
    private authenticationService: AuthenticationService,
    private stateService: StateService,
    private titleService: Title,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
    this.authenticationService.satudataToken.subscribe(
      (x) => (this.satudataToken = x),
    );
    this.stateService.currentLogUser.subscribe((x) => (this.userInfo = x));
  }

  ngOnInit(): void {
    this.settingsAll();
    this.queryParams();
    this.logPage('init');
  }

  ngOnDestroy(): void {
    this.logPage('destroy');
  }

  ngAfterViewInit(): void {
    this.cdRef.detectChanges();
  }

  settingsAll(): void {
    this.title = this.globalService.title;
    this.titleService.setTitle(`${this.label} - ${this.title}`);
    this.globalService.changeLabel(this.label);
    this.globalService.changeBreadcrumb(this.breadcrumb);
  }

  queryParams(): void {
    this.route.queryParams.subscribe((p) => {
      const pPerPage = p.perpage;
      const pCurrentPage = p.currentpage;
      const pSort = p.sort;
      const pSearch = p.q;
      const pStatus = p.status;
      const pData = p.data;

      this.filter.perPage = !_.isNil(pPerPage) ? Number(pPerPage) : 10;
      this.filter.currentPage = !_.isNil(pCurrentPage)
        ? Number(pCurrentPage)
        : 1;
      this.filter.sort = !_.isNil(pSort) ? pSort : 'newest';
      this.filter.search = !_.isNil(pSearch) ? pSearch : '';
      this.filter.status = !_.isNil(pStatus) ? pStatus : 'view';
      this.filter.data = !_.isNil(pData) ? pData : 'dataset';
    });
  }

  filterData(event: Filter): void {
    this.filter.data = event.data;
    this.filter.currentPage = event.currentPage;
    this.filter.isFirst = true;
    this.filter = { ...this.filter };
  }

  filterSearch(event: Filter): void {
    this.filter.search = event.search;
    this.filter.currentPage = event.currentPage;
    this.filter.isFirst = true;
    this.filter = { ...this.filter };
  }

  filterStatus(event: Filter): void {
    this.filter.status = event.status;
    this.filter.currentPage = event.currentPage;
    this.filter.isFirst = true;
    this.filter = { ...this.filter };
  }

  filterSort(event: Filter): void {
    this.filter.sort = event.sort;
    this.filter.currentPage = event.currentPage;
    this.filter.isFirst = true;
    this.filter = { ...this.filter };
  }

  logPage(type: string): void {
    if (type === 'init') {
      this.pageInfo = {
        page: this.router.url,
        date_start: moment().format('YYYY-MM-DD HH:mm:ss'),
      };

      this.log = {
        log: { ...this.pageInfo, ...this.userInfo },
        token: this.satudataToken,
      };
    } else if (type === 'destroy') {
      if (typeof Worker !== 'undefined') {
        _.set(this.log.log, 'date_end', moment().format('YYYY-MM-DD HH:mm:ss'));

        const log = of(this.log);

        fromWorker<object, string>(
          () =>
            new Worker(
              new URL('src/app/__v4/workers/log-page.worker', import.meta.url),
              { type: 'module' },
            ),
          log,
        ).subscribe((message) => {
          if (!environment.production) {
            console.info(`Log page: ${message}`);
          }
        });
      }
    }
  }
}
