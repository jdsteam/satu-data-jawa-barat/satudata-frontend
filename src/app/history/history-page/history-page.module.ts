import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';

import { TitleSearchComponent as HeaderTitleSearchComponent } from '@components-v3/header/title-search/title-search.component';
import { HistoryComponent as TabHistoryComponent } from '@components-v3/tab/history/history.component';
import { HistoryComponent } from '@components-v3/list/history/history.component';
import { HistoryPageComponent } from './history-page.component';

export const routes: Routes = [
  {
    path: '',
    component: HistoryPageComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    HeaderTitleSearchComponent,
    TabHistoryComponent,
    HistoryComponent,
  ],
  declarations: [HistoryPageComponent],
  bootstrap: [HistoryPageComponent],
})
export class HistoryPageModule {}
