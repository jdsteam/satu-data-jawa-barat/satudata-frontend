import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// GUARD
import {
  MyVisualizationAddGuard,
  MyVisualizationEditGuard,
  MyVisualizationPreviewGuard,
} from '@guards-v3';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'add',
        canActivate: [MyVisualizationAddGuard],
        loadChildren: () =>
          import('./my-visualization-add/my-visualization-add.module').then(
            (m) => m.MyVisualizationAddModule,
          ),
      },
      {
        path: 'preview/:id',
        canActivate: [MyVisualizationPreviewGuard],
        loadChildren: () =>
          import(
            './my-visualization-preview/my-visualization-preview.module'
          ).then((m) => m.MyVisualizationPreviewModule),
      },
      {
        path: 'edit/:id',
        canActivate: [MyVisualizationEditGuard],
        loadChildren: () =>
          import('./my-visualization-edit/my-visualization-edit.module').then(
            (m) => m.MyVisualizationEditModule,
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MyVisualizationRoutingModule {}
