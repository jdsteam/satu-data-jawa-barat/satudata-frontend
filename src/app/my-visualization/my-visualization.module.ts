import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyVisualizationRoutingModule } from './my-visualization-routing.module';

@NgModule({
  imports: [CommonModule, MyVisualizationRoutingModule],
  declarations: [],
})
export class MyVisualizationModule {}
