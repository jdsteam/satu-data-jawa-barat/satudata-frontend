import { Component, OnInit, OnDestroy, Renderer2 } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Store } from '@ngrx/store';

// STYLE
import * as jds from '@styles';

// INTERFACE
import { Breadcrumb } from '@interfaces-v4';

// MODEL
import { LoginData } from '@models-v3';

// SERVICE
import { AuthenticationService, StateService } from '@services-v3';
import { GlobalService } from '@services-v4';

// STORE
import { fromVisualizationActions } from '@store-v3/visualization/visualization.actions';

// PLUGIN
import * as _ from 'lodash';
import moment from 'moment';
import { fromWorker } from 'observable-webworker';

@Component({
  selector: 'app-my-visualization-preview',
  templateUrl: './my-visualization-preview.component.html',
})
export class MyVisualizationPreviewComponent implements OnInit, OnDestroy {
  satudataUser: LoginData;
  satudataToken: string;
  moment = moment;
  jds = jds;

  // Log
  log = null;
  pageInfo = null;
  deviceInfo = null;
  userInfo = null;

  // Pengaturan
  title: string;
  label = 'Ringkasan Visualisasi';
  breadcrumb: Breadcrumb[] = [
    { label: 'Data Saya', link: '/my-data' },
    { label: 'Ringkasan Visualisasi', link: '/my-visualization/preview' },
  ];

  // Variable
  id: number;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private globalService: GlobalService,
    private stateService: StateService,
    private titleService: Title,
    private renderer: Renderer2,
    private store: Store<any>,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
    this.authenticationService.satudataToken.subscribe(
      (x) => (this.satudataToken = x),
    );
    this.stateService.currentLogUser.subscribe((x) => (this.userInfo = x));

    // id
    this.id = this.route.snapshot.params.id;
  }

  settingsAll() {
    this.renderer.removeClass(document.body, 'satudata');

    this.title = this.globalService.title;
    this.titleService.setTitle(`${this.label} - ${this.title}`);
    this.globalService.changeLabel(this.label);
    this.globalService.changeBreadcrumb(this.breadcrumb);
  }

  ngOnInit() {
    this.settingsAll();
    this.clearVisualization();
    this.logPage('init');
  }

  ngOnDestroy(): void {
    this.logPage('destroy');
  }

  clearVisualization() {
    this.store.dispatch(fromVisualizationActions.clearVisualization());
  }

  logPage(type: string): void {
    if (type === 'init') {
      this.pageInfo = {
        page: this.router.url,
        date_start: moment().format('YYYY-MM-DD HH:mm:ss'),
      };

      this.log = {
        log: { ...this.pageInfo, ...this.userInfo },
        token: this.satudataToken,
      };
    } else if (type === 'destroy') {
      if (typeof Worker !== 'undefined') {
        _.set(this.log.log, 'date_end', moment().format('YYYY-MM-DD HH:mm:ss'));

        const log = of(this.log);

        fromWorker<object, string>(
          () =>
            new Worker(
              new URL('src/app/__v4/workers/log-page.worker', import.meta.url),
              { type: 'module' },
            ),
          log,
        ).subscribe((message) => {
          if (!environment.production) {
            console.info(`Log page: ${message}`);
          }
        });
      }
    }
  }
}
