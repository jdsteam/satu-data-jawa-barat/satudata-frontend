import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';

import { BreadcrumbComponent } from '@components-v4';
import { FormComponent as HeaderFormComponent } from '@components-v3/header/form/form.component';
import { PreviewComponent as FormVisualizationPreviewComponent } from '@components-v3/form/visualization/preview/preview.component';

import { MyVisualizationPreviewComponent } from './my-visualization-preview.component';

export const routes: Routes = [
  {
    path: '',
    component: MyVisualizationPreviewComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    BreadcrumbComponent,
    HeaderFormComponent,
    FormVisualizationPreviewComponent,
  ],
  declarations: [MyVisualizationPreviewComponent],
  bootstrap: [MyVisualizationPreviewComponent],
})
export class MyVisualizationPreviewModule {}
