import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// MODULE
import { JdsBiAccordionModule, JdsBiInputModule } from '@jds-bi/core';
import { JdsBiIconsModule } from '@jds-bi/icons';
import { NgxLoadingModule } from 'ngx-loading';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxPopperjsModule } from 'ngx-popperjs';

import { BreadcrumbComponent } from '@components-v4';
import { FormComponent as HeaderFormComponent } from '@components-v3/header/form/form.component';
import { AccessControlComponent as FormAccessControlComponent } from '@components-v3/form/access-control/access-control.component';

import { MyVisualizationAddComponent } from './my-visualization-add.component';

export const routes: Routes = [
  {
    path: '',
    component: MyVisualizationAddComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    JdsBiAccordionModule,
    JdsBiInputModule,
    JdsBiIconsModule,
    NgxLoadingModule,
    NgSelectModule,
    NgxPopperjsModule,
    BreadcrumbComponent,
    HeaderFormComponent,
    FormAccessControlComponent,
  ],
  declarations: [MyVisualizationAddComponent],
  bootstrap: [MyVisualizationAddComponent],
})
export class MyVisualizationAddModule {}
