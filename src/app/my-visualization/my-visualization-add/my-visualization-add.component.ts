/* eslint-disable @typescript-eslint/dot-notation */
import { Component, OnInit, OnDestroy, Renderer2 } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import {
  UntypedFormGroup,
  UntypedFormControl,
  UntypedFormArray,
  Validators,
} from '@angular/forms';
import { Observable, Subject, concat, of } from 'rxjs';
import {
  filter,
  debounceTime,
  distinctUntilChanged,
  tap,
  switchMap,
  catchError,
  share,
} from 'rxjs/operators';
import { HttpEventType } from '@angular/common/http';
import { Store, select } from '@ngrx/store';
import { Actions } from '@ngrx/effects';
import { environment } from 'src/environments/environment';

// STYLE
import * as jds from '@styles';
// CONSTANT
import { COLORSCHEME } from '@constants-v4';
// INTERFACE
import { Breadcrumb } from '@interfaces-v4';
// MODEL
import { LoginData } from '@models-v3';
import { TopicData, OrganizationData, ClassificationData } from '@models-v4';
// SERVICE
import {
  AuthenticationService,
  StateService,
  OrganizationStructureService,
} from '@services-v3';
import {
  GlobalService,
  DatasetService,
  UploadService,
  VisualizationService,
  VisualizationDatasetService,
  VisualizationMetadataService,
} from '@services-v4';

// STORE
import {
  selectAllTopic,
  selectIsLoadingList as selectIsLoadingListTopic,
  selectError as selectErrorTopic,
} from '@store-v3/topic/topic.selectors';
import { fromTopicActions } from '@store-v3/topic/topic.actions';

import {
  selectAllOrganization,
  selectIsLoadingList as selectIsLoadingListOrganization,
  selectError as selectErrorOrganization,
} from '@store-v3/organization/organization.selectors';
import { fromOrganizationActions } from '@store-v3/organization/organization.actions';

import {
  selectAllClassificationDataset,
  selectIsLoadingList as selectIsLoadingListClassificationDataset,
  selectError as selectErrorClassificationDataset,
} from '@store-v3/classification-dataset/classification-dataset.selectors';
import { fromClassificationDatasetActions } from '@store-v3/classification-dataset/classification-dataset.actions';

// COMPONENT
import {
  JdsBiIconsService,
  iconCircleInfo,
  iconCirclePlus,
  iconTrash,
} from '@jds-bi/icons';

import * as _ from 'lodash';
import moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { fromWorker } from 'observable-webworker';

@Component({
  selector: 'app-my-visualization-add',
  templateUrl: './my-visualization-add.component.html',
  styleUrls: ['./my-visualization-add.component.scss'],
})
export class MyVisualizationAddComponent implements OnInit, OnDestroy {
  colorScheme = COLORSCHEME;
  satudataUser: LoginData;
  satudataToken: string;
  moment = moment;
  jds = jds;

  // Log
  log = null;
  pageInfo = null;
  deviceInfo = null;
  userInfo = null;

  // Pengaturan
  title: string;
  label = 'Tambah Visualisasi Saya';
  breadcrumb: Breadcrumb[] = [
    { label: 'Data Saya', link: '/my-data' },
    { label: 'Tambah Visualisasi', link: '/visualization/add' },
  ];

  loading = false;
  loadingDataset = false;
  loadingSkpd = false;
  loadingUpload = false;

  myForm: UntypedFormGroup;
  myFormDataset: UntypedFormArray;
  myFormMetadata: UntypedFormArray;

  isShowAccess = false;

  dataTopik: TopicData[] = [];
  dataTopikOption: any[];
  dataOrganisasi: OrganizationData[] = [];
  dataOrganisasiOption: any[];
  dataKlasifikasi: ClassificationData[] = [];
  dataKlasifikasiOption: any[];
  dataDataset$: any;
  dataSkpd$: any;
  dataUpload: any[] = [];
  dataVisualization: any[] = [];

  inputDataset$ = new Subject<string>();
  inputSkpd$ = new Subject<string>();

  organizationInputIsLoading = false;
  organizationInput$ = new Subject<string>();
  organizationInputData$: any;
  organizationInputValid = true;

  topicData$: Observable<any[]>;
  topicIsLoadingList$: Observable<boolean>;
  topicIsError: boolean;

  organizationData$: Observable<OrganizationData[]>;
  organizationIsLoadingList$: Observable<boolean>;
  organizationIsError: boolean;

  classificationDatasetData$: Observable<ClassificationData[]>;
  classificationDatasetIsLoadingList$: Observable<boolean>;
  classificationDatasetIsError: boolean;

  organizationStructureData = [];
  organizationStructureIsLoadingList = false;
  selectedPositions: any;

  thumbPath: string = null;
  thumbNull = 'assets/images/thumbnail.svg';

  progressUpload: number;

  constructor(
    private authenticationService: AuthenticationService,
    private globalService: GlobalService,
    private organizationStructureService: OrganizationStructureService,
    private datasetService: DatasetService,
    private uploadService: UploadService,
    private visualizationService: VisualizationService,
    private visualizationDatasetService: VisualizationDatasetService,
    private visualizationMetadataService: VisualizationMetadataService,
    private stateService: StateService,
    private titleService: Title,
    private renderer: Renderer2,
    private router: Router,
    private route: ActivatedRoute,
    private actions$: Actions,
    private store: Store<any>,
    private iconService: JdsBiIconsService,
    private toastr: ToastrService,
  ) {
    this.authenticationService.satudataUser.subscribe(
      (x) => (this.satudataUser = x),
    );
    this.authenticationService.satudataToken.subscribe(
      (x) => (this.satudataToken = x),
    );
    this.stateService.currentLogUser.subscribe((x) => (this.userInfo = x));

    this.iconService.registerIcons([iconTrash, iconCirclePlus, iconCircleInfo]);
  }

  ngOnInit() {
    this.settingsAll();
    this.logPage('init');

    this.myForm = new UntypedFormGroup({
      year: new UntypedFormControl(),
      name: new UntypedFormControl({ value: null, disabled: false }, [
        Validators.required,
        Validators.maxLength(255),
      ]),
      title: new UntypedFormControl(null, [Validators.maxLength(255)]),
      description: new UntypedFormControl({ value: null, disabled: false }, [
        Validators.required,
      ]),
      image: new UntypedFormControl({ value: null, disabled: false }, [
        Validators.required,
      ]),
      url: new UntypedFormControl({ value: null, disabled: false }, [
        Validators.required,
        Validators.pattern(
          /\b((http|https):\/\/?)[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|\/?))/g,
        ),
      ]),
      owner: new UntypedFormControl(null, [Validators.maxLength(255)]),
      owner_email: new UntypedFormControl(
        { value: null, disabled: false },
        Validators.maxLength(255),
      ),
      maintainer: new UntypedFormControl(null, [Validators.maxLength(255)]),
      maintainer_email: new UntypedFormControl(null, [
        Validators.maxLength(255),
      ]),
      kode_skpd: new UntypedFormControl({ value: null, disabled: false }, [
        Validators.required,
      ]),
      kode_skpdsub: new UntypedFormControl({ value: null, disabled: false }),
      kode_skpdunit: new UntypedFormControl({ value: null, disabled: false }),
      dataset_class_id: new UntypedFormControl(
        { value: null, disabled: false },
        [Validators.required],
      ),
      sektoral_id: new UntypedFormControl({ value: null, disabled: false }, [
        Validators.required,
      ]),
      regional_id: new UntypedFormControl(),
      license_id: new UntypedFormControl({ value: null, disabled: false }),
      is_active: new UntypedFormControl(),
      is_deleted: new UntypedFormControl(),
      is_validate: new UntypedFormControl(),
      count_view: new UntypedFormControl(),
      count_rating: new UntypedFormControl(),
      access_control: new UntypedFormControl([]),
      jabatan_access: new UntypedFormArray([]),
      divisualisasikan_oleh: new UntypedFormControl(
        { value: null, disabled: false },
        [Validators.required, Validators.maxLength(255)],
      ),
      tahun_pembuatan: new UntypedFormControl(
        { value: null, disabled: false },
        [Validators.required, Validators.maxLength(255)],
      ),
      visualization_dataset: new UntypedFormArray([this.createDataset()]),
      visualization_metadata: new UntypedFormArray([]),
    });

    this.getInputDataset();
    this.getInputOrganization();

    this.getAllTopic();
    this.getAllOrganization();
    this.getAllClassificationDataset();
  }

  ngOnDestroy(): void {
    this.logPage('destroy');
  }

  settingsAll() {
    this.renderer.removeClass(document.body, 'satudata');

    this.title = this.globalService.title;
    this.titleService.setTitle(`${this.label} - ${this.title}`);
    this.globalService.changeLabel(this.label);
    this.globalService.changeBreadcrumb(this.breadcrumb);
  }

  // GET =====================================================================================================
  get f() {
    return this.myForm.controls;
  }

  get fvd() {
    return this.f.visualization_dataset as UntypedFormArray;
  }

  get fvm() {
    return this.f.visualization_metadata as UntypedFormArray;
  }

  get fdp() {
    return this.f.jabatan_access as UntypedFormArray;
  }

  getInputDataset() {
    this.dataDataset$ = concat(
      of([]),
      this.inputDataset$.pipe(
        debounceTime(1000),
        distinctUntilChanged(),
        filter((value) => value !== null),
        tap(() => (this.loadingDataset = true)),
        switchMap((value) =>
          this.datasetService.searchItem(value).pipe(
            catchError(() => of([])),
            tap(() => (this.loadingDataset = false)),
          ),
        ),
      ),
    ).pipe(share());
  }

  getInputOrganization() {
    this.organizationInputData$ = concat(
      of([]),
      this.organizationInput$.pipe(
        debounceTime(1000),
        distinctUntilChanged(),
        filter((term) => term !== null),
        tap(() => (this.organizationInputIsLoading = true)),
        switchMap((term) =>
          this.organizationStructureService
            .getSearchItem({
              search: term || '',
              sort: `satuan_kerja_nama:asc`,
              where: JSON.stringify({
                level: '0',
              }),
            })
            .pipe(
              catchError(() => of([])),
              tap(() => (this.organizationInputIsLoading = false)),
            ),
        ),
      ),
    );
  }

  getAllTopic() {
    const pSort = `name:asc`;

    const pWhere = {
      is_deleted: false,
    };

    const params = {
      sort: pSort,
      where: JSON.stringify(pWhere),
    };

    this.store.dispatch(
      fromTopicActions.loadAllTopic({
        params,
        pagination: false,
      }),
    );

    this.topicIsLoadingList$ = this.store.pipe(
      select(selectIsLoadingListTopic),
    );

    this.store
      .pipe(
        select(selectErrorTopic),
        filter((val) => val !== null && val.error),
      )
      .subscribe((result) => {
        if (!result.error) {
          this.topicIsError = false;
        } else {
          this.topicIsError = true;
          this.toastr.error(result.message, 'Topic');
        }
      });

    this.topicData$ = this.store.pipe(
      select(selectAllTopic),
      filter((val) => val.length !== 0),
    );
  }

  getAllOrganization() {
    const pSort = `nama_skpd:asc`;

    const pWhere = {
      is_satudata: true,
      is_deleted: false,
    };

    const params = {
      sort: pSort,
      where: JSON.stringify(pWhere),
    };

    this.store.dispatch(
      fromOrganizationActions.loadAllOrganization({
        params,
        pagination: false,
        infinite: false,
      }),
    );

    this.organizationIsLoadingList$ = this.store.pipe(
      select(selectIsLoadingListOrganization),
    );

    this.store
      .pipe(
        select(selectErrorOrganization),
        filter((val) => val !== null && val.error),
      )
      .subscribe((result) => {
        if (!result.error) {
          this.organizationIsError = false;
        } else {
          this.organizationIsError = true;
          this.toastr.error(result.message, 'Organization');
        }
      });

    this.organizationData$ = this.store.pipe(
      select(selectAllOrganization),
      filter((val) => val.length !== 0),
    );
  }

  getAllClassificationDataset() {
    const pSort = `name:asc`;

    const params = {
      sort: pSort,
    };

    this.store.dispatch(
      fromClassificationDatasetActions.loadAllClassificationDataset({
        params,
        pagination: false,
        infinite: false,
      }),
    );

    this.classificationDatasetIsLoadingList$ = this.store.pipe(
      select(selectIsLoadingListClassificationDataset),
    );

    this.store
      .pipe(
        select(selectErrorClassificationDataset),
        filter((val) => val !== null && val.error),
      )
      .subscribe((result) => {
        if (!result.error) {
          this.classificationDatasetIsError = false;
        } else {
          this.classificationDatasetIsError = true;
          this.toastr.error(result.message, 'Classification Dataset');
        }
      });

    this.classificationDatasetData$ = this.store.pipe(
      select(selectAllClassificationDataset),
      filter((val) => val.length !== 0),
    );
  }

  async mappingPosition(data, organization) {
    let org0 = _.filter(data, { level: '0' });

    if (org0.length === 0) {
      const { data: dataPosition } = await this.getPosition(0, {
        satuan_kerja_id: organization.satuan_kerja_id,
      });
      org0 = dataPosition as any;
    }

    const getLevel = (allOrganisasi) => {
      const level = _.map(allOrganisasi, (res) => Number(res.level));
      return Math.max(...level);
    };
    const allLevel = getLevel(data);

    const setMapping = async (allOrganization, level = 1) => {
      const filterLevel = { level: level.toString() };

      return Promise.all(
        allOrganization.map(async (result) => {
          let done = false;
          switch (level) {
            case 1:
              _.assign(filterLevel, {
                satuan_kerja_id: result.satuan_kerja_id,
              });
              break;
            case 2:
              _.assign(filterLevel, {
                satuan_kerja_id: result.satuan_kerja_id,
              });
              _.assign(filterLevel, {
                lv1_unit_kerja_id: result.lv1_unit_kerja_id,
              });
              break;
            case 3:
              _.assign(filterLevel, {
                satuan_kerja_id: result.satuan_kerja_id,
              });
              _.assign(filterLevel, {
                lv1_unit_kerja_id: result.lv1_unit_kerja_id,
              });
              _.assign(filterLevel, {
                lv2_unit_kerja_id: result.lv2_unit_kerja_id,
              });
              break;
            case 4:
              _.assign(filterLevel, {
                satuan_kerja_id: result.satuan_kerja_id,
              });
              _.assign(filterLevel, {
                lv1_unit_kerja_id: result.lv1_unit_kerja_id,
              });
              _.assign(filterLevel, {
                lv2_unit_kerja_id: result.lv2_unit_kerja_id,
              });
              _.assign(filterLevel, {
                lv3_unit_kerja_id: result.lv3_unit_kerja_id,
              });
              break;
            default:
              done = true;
              break;
          }

          if (level > allLevel || done) {
            return { ...result };
          }

          let organizationFiltered = _.filter(data, filterLevel);

          if (organizationFiltered.length === 0) {
            const { data: dataPosition } = await this.getPosition(
              level,
              filterLevel,
            );
            organizationFiltered = dataPosition as any;
          }

          let populate: any;
          if (organizationFiltered.length > 0) {
            populate = await setMapping(organizationFiltered, level + 1);
            return { ...result, populate };
          }

          return { ...result };
        }),
      );
    };

    return setMapping(org0);
  }

  getPosition(level: number, filterLevel = null) {
    const pSort = `jabatan_nama:asc`;

    const pWhere = {
      level: level.toString(),
    };

    _.assign(pWhere, filterLevel);

    const params = {
      sort: pSort,
      where: JSON.stringify(pWhere),
    };

    return this.organizationStructureService.getList(params).toPromise();
  }

  getAllPosition(organization) {
    const pSort = `jabatan_nama:asc`;

    const pWhere = {
      satuan_kerja_id: organization.satuan_kerja_id,
    };

    const params = {
      sort: pSort,
      where: JSON.stringify(pWhere),
    };

    this.organizationStructureIsLoadingList = true;
    this.organizationStructureService.getList(params).subscribe((result) => {
      this.organizationStructureIsLoadingList = false;

      this.mappingPosition(result.data, organization).then((mapping) => {
        this.organizationStructureData.push({
          satuan_kerja_id: organization.satuan_kerja_id,
          satuan_kerja_nama: organization.satuan_kerja_nama,
          data: mapping,
          positions: result.data,
        });

        this.addPosition(mapping);
      });
    });
  }

  // DYNAMIC FORM ============================================================================================
  addMetadata() {
    this.fvm.push(this.createMetadata());
    return false;
  }

  createMetadata(): UntypedFormGroup {
    return new UntypedFormGroup({
      key: new UntypedFormControl({ value: null, disabled: false }, [
        Validators.maxLength(255),
      ]),
      value: new UntypedFormControl({ value: null, disabled: false }, [
        Validators.maxLength(255),
      ]),
    });
  }

  removeMetadata(i) {
    if (this.fvm) {
      this.fvm.removeAt(i);
    }
    return false;
  }

  addDataset() {
    this.fvd.push(this.createDataset());
    return false;
  }

  createDataset(): UntypedFormGroup {
    return new UntypedFormGroup({
      dataset: new UntypedFormControl({ value: null, disabled: false }),
    });
  }

  removeDataset(i) {
    if (this.fvd) {
      this.fvd.removeAt(i);
    }
    return false;
  }

  addPosition(data: any) {
    this.fdp.push(this.createPosition(data));
    return false;
  }

  createPosition(mapping: any) {
    const setForm = (allOrganization) => {
      return allOrganization.map((result) => {
        const temp = {
          position: new UntypedFormControl(false),
          checked: new UntypedFormControl(false),
        };

        if (result.populate !== undefined && result.populate.length > 0) {
          const populate = setForm(result.populate);
          _.assign(temp, { level: new UntypedFormArray(populate) });
        }

        return new UntypedFormGroup(temp);
      });
    };

    return new UntypedFormArray(setForm(mapping));
  }

  removePosition(organization) {
    const findIndex = _.findIndex(this.organizationStructureData, {
      satuan_kerja_id: organization.value.satuan_kerja_id,
    });

    if (this.fdp) {
      this.fdp.removeAt(findIndex);
      this.organizationStructureData.splice(findIndex, 1);
    }
    return false;
  }

  removePositionAll() {
    this.myForm.controls.jabatan_access = new UntypedFormArray([]);
    this.organizationStructureData = [];
  }

  // ONCHANGE ================================================================================================
  onSelectedClassificationDataset(value) {
    if (value === 4) {
      this.isShowAccess = true;
    } else {
      this.isShowAccess = false;
      this.myForm.controls.access_control.setValue([]);
      this.removePositionAll();
    }
  }

  onAddAccessControl(value) {
    this.getAllPosition(value);
  }

  onRemoveAccessControl(value) {
    this.removePosition(value);
  }

  // SAVE ====================================================================================================
  getValuePosition() {
    const accessControl = this.f.jabatan_access['controls'];

    const getValue = (input, data) => {
      return _.map(input['controls'], (res, i) => {
        let organization: any;
        if (data.data) {
          organization = data.data[i];
        } else {
          organization = data.populate[i];
        }

        const position = res.get('position').value && organization.jabatan_id;

        if (res.get('level')) {
          const newArray = [
            position,
            ...getValue(res.get('level'), organization),
          ];
          return newArray;
        }

        return position;
      });
    };

    const selected = [];
    _.each(accessControl, (res, i) => {
      const value = getValue(res, this.organizationStructureData[i]);
      selected.push(value);
    });

    const allSelected = _.flattenDeep(selected);

    return _.filter(allSelected, (result) => result !== false);
  }

  onUpload(event: any) {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file: File = fileList[0];
      const myFormUpload: FormData = new FormData();
      myFormUpload.append('file', file, file.name);

      this.uploadService.uploadFile(myFormUpload).subscribe((response) => {
        if (response.type === HttpEventType.UploadProgress) {
          this.loadingUpload = true;
          this.progressUpload = Math.round(
            (100 * response.loaded) / response.total,
          );
        } else if (response.type === HttpEventType.Response) {
          const result = [];
          result.push(response.body.data);
          this.dataUpload = result;

          this.myForm.patchValue({
            image: result[0].url,
          });
          this.thumbPath = environment.backendURL + result[0].url;

          this.loadingUpload = false;
        }
      });
    }
  }

  onSubmit() {
    this.loading = true;

    const input = this.myForm.value;

    // Position Access
    const bodyPosition = this.getValuePosition();

    // Dataset
    const bodyDataset = [];
    _.each(input.visualization_dataset, (result) => {
      if (_.isNil(result.dataset) === false) {
        bodyDataset.push({
          dataset_id: result.dataset.id,
        });
      }
    });

    // Metadata
    const bodyMetadata = [];
    bodyMetadata.push({
      key: 'Divisualisasikan Oleh',
      value: input.divisualisasikan_oleh,
      metadata_type_id: 1,
    });
    bodyMetadata.push({
      key: 'Tahun Pembuatan Visualisasi',
      value: input.tahun_pembuatan,
      metadata_type_id: 1,
    });

    _.each(input.visualization_metadata, (result) => {
      bodyMetadata.push({
        key: result.key,
        value: result.value,
        metadata_type_id: 4,
      });
    });

    this.myForm.patchValue({
      title: null,
      year: null,
      regional_id: 1,
      owner: null,
      maintainer: null,
      maintainer_email: null,
      is_active: false,
      is_deleted: true,
      is_validate: true,
      count_view: 0,
      count_rating: 0,
    });

    const bodyVisualization = input;
    delete bodyVisualization['divisualisasikan_oleh'];
    delete bodyVisualization['tahun_pembuatan'];
    delete bodyVisualization['visualization_dataset'];
    delete bodyVisualization['visualization_metadata'];
    delete bodyVisualization['access_control'];
    delete bodyVisualization['jabatan_access'];

    if (input.dataset_class_id === 4) {
      _.assign(bodyVisualization, {
        jabatan_access: bodyPosition,
      });
    }

    this.visualizationService
      .createItem(bodyVisualization)
      .subscribe((response) => {
        this.dataVisualization = response.data;

        const visualizationId = Number(response.data['id']);

        // Insert visualization dataset
        if (bodyDataset.length > 0) {
          const bodyVDataset = _.map(bodyDataset, (o) =>
            _.extend({ visualization_id: visualizationId }, o),
          );
          this.visualizationDatasetService.createItem(bodyVDataset).toPromise();
        }

        // Insert visualization metadata
        const bodyVMetadata = _.map(bodyMetadata, (o) =>
          _.extend({ visualization_id: visualizationId }, o),
        );
        this.visualizationMetadataService.createItem(bodyVMetadata).toPromise();

        this.loading = false;

        this.router.navigate(['/my-visualization/preview/', visualizationId]);
      });
  }

  logPage(type: string): void {
    if (type === 'init') {
      this.pageInfo = {
        page: this.router.url,
        date_start: moment().format('YYYY-MM-DD HH:mm:ss'),
      };

      this.log = {
        log: { ...this.pageInfo, ...this.userInfo },
        token: this.satudataToken,
      };
    } else if (type === 'destroy') {
      if (typeof Worker !== 'undefined') {
        _.set(this.log.log, 'date_end', moment().format('YYYY-MM-DD HH:mm:ss'));

        const log = of(this.log);

        fromWorker<object, string>(
          () =>
            new Worker(
              new URL('src/app/__v4/workers/log-page.worker', import.meta.url),
              { type: 'module' },
            ),
          log,
        ).subscribe((message) => {
          if (!environment.production) {
            console.info(`Log page: ${message}`);
          }
        });
      }
    }
  }
}
