import { Component, inject } from '@angular/core';
import { SwUpdate, VersionReadyEvent } from '@angular/service-worker';
import { filter } from 'rxjs';
import { environment } from 'src/environments/environment';

import Hotjar from '@hotjar/browser';

import { AppStyle } from './app.style';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function promptUser(event: VersionReadyEvent): boolean {
  return true;
}

@Component({
  selector: 'app-root',
  template: `
    <jds-bi-root>
      <ngx-loading-bar
        [includeSpinner]="false"
        color="#1D54A6"
      />
      <main [ngClass]="[className.main]">
        <router-outlet />
      </main>
    </jds-bi-root>
  `,
  providers: [AppStyle],
})
export class AppComponent {
  private appStyle = inject(AppStyle);
  className = this.appStyle.getDynamicStyle();

  constructor(private readonly swUpdate: SwUpdate) {
    swUpdate.versionUpdates
      .pipe(
        filter((evt): evt is VersionReadyEvent => evt.type === 'VERSION_READY'),
      )
      .subscribe((evt) => {
        if (promptUser(evt)) {
          document.location.reload();
        }
      });

    Hotjar.init(Number(environment.hotjarTrackingCode), 6);
  }
}
