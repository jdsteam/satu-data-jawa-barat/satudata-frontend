const { resolve } = require('path');
const { writeFileSync } = require('fs-extra');
const { version } = require('../package.json');

const file = resolve(
  __dirname,
  '..',
  'src',
  'app',
  '__v4',
  'constants',
  'version.ts',
);

console.log(file);

writeFileSync(
  file,
  `export const VERSION = '${version}';
`,
  { encoding: 'utf-8' },
);
