const { execSync } = require('child_process');

require('dotenv').config();

execSync(
  `sed -i -e "s/%GOOGLE_ANALYTICS%/${process.env.GOOGLE_ANALYTICS}/g" src/assets/scripts/google/analytics.js`,
);
