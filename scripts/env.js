const { writeFile } = require('fs');
const { argv } = require('yargs');

require('dotenv').config();

const { environment } = argv;
const isProduction = environment === 'production';

let targetPath = '';
if (environment === 'localhost') {
  targetPath = `./src/environments/environment.loc.ts`;
} else if (environment === 'development') {
  targetPath = `./src/environments/environment.dev.ts`;
} else if (environment === 'staging') {
  targetPath = `./src/environments/environment.stag.ts`;
} else if (environment === 'production') {
  targetPath = `./src/environments/environment.prod.ts`;
} else {
  targetPath = `./src/environments/environment.ts`;
}

const fileContent = `
export const environment = {
  environment: '${environment || 'development'}',
  production: ${isProduction},
  satudataURL: '${process.env.SATUDATA_URL}',
  ssoURL: '${process.env.SSO_URL}',
  ssoDigiteamURL: '${process.env.SSO_DIGITEAM_URL}',
  backendURL: '${process.env.BACKEND_URL}',
  socketURL: '${process.env.SATUDATA_SOCKET_URL}',
  socketWSS: '${process.env.SATUDATA_SOCKET_WSS}',
  logURL: '${process.env.LOG_URL}',
  fluentdURL: '${process.env.FLUENTD_URL}',
  sentryURL: '${process.env.SENTRY_URL}',
  signozURL: '${process.env.SIGNOZ_URL}',
  geoserverURL: '${process.env.GEOSERVER_URL}',
  geoserverUsername: '${process.env.GEOSERVER_USERNAME}',
  geoserverPassword: '${process.env.GEOSERVER_PASSWORD}',
  geonetworkURL: '${process.env.GEONETWORK_URL}',
  hotjarTrackingCode: '${process.env.HOTJAR_TRACKING_CODE}',
  googleRecaptcha: '${process.env.GOOGLE_RECAPTCHA}',
  jwtAlgorithm: '${process.env.JWT_ALGORITHM}',
  jwtSecretKey: '${process.env.JWT_SECRET_KEY}',
};
`;

writeFile(targetPath, fileContent, (err) => {
  if (err) {
    console.error(err);
  }
  console.info(`Wrote variables to ${targetPath}`);
});
