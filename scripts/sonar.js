const { writeFile } = require('fs');
const { version } = require('../package.json');

require('dotenv').config();

const targetPath = `./sonar-project.properties`;

const fileContent = `
sonar.host.url=${process.env.SONAR_URL}
sonar.token=${process.env.SONAR_TOKEN}
sonar.projectKey=${process.env.SONAR_PROJECT_KEY}
sonar.projectVersion=${version}
sonar.sourceEncoding=UTF-8
sonar.sources=src
sonar.exclusions=**/node_modules/**, src/assets/**
sonar.tests=src
sonar.test.inclusions=**/*.spec.ts
sonar.typescript.lcov.reportPaths=coverage/lcov.info
sonar.qualitygate.wait=true
`;

writeFile(targetPath, fileContent, (err) => {
  if (err) {
    console.error(err);
  }
  console.info(`Wrote variables to ${targetPath}`);
});
