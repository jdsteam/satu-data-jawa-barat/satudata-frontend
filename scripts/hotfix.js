const { argv } = require('process');
const { createInterface } = require('readline');
const { execSync } = require('child_process');
const { version } = require('../package.json');

const parsedVersion = version.split('.');

function bump(versionArray) {
  versionArray[2] = Number(versionArray[2]) + 1;
  return versionArray.join('.');
}

function checkChangelog() {
  let response;

  const readline = createInterface({
    input: process.stdin,
    output: process.stdout,
  });

  readline.setPrompt(
    `Check\n./CHANGELOG.md\nand save all fixes. Then press enter`,
  );
  readline.prompt();
  console.info('\n');

  return new Promise((resolve) => {
    readline.on('line', (userInput) => {
      response = userInput;
      readline.close();
    });

    readline.on('close', () => {
      resolve(response);
    });
  });
}

const newVersion = bump(parsedVersion);

execSync('git checkout hotfix');
execSync('git pull');
execSync(`git checkout -b release/${newVersion}`);
execSync(`npm run release -- --release-as patch`);

checkChangelog().then(() => {
  execSync('git add .');
  execSync(
    `git commit -m "chore(changelog): fix incorrect generated logs" --no-verify`,
  );
  execSync(`git push --set-upstream origin release/${newVersion}`);
  execSync(`git push --tags`);
});
