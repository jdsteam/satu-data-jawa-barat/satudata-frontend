const cheerio = require('cheerio');
const minfier = require('html-minifier');

module.exports = (targetOptions, indexHtml) => {
  const configuration = targetOptions.configuration;

  const $ = cheerio.load(indexHtml);
  const preconnectSelf = `<link rel="preconnect" href="${process.env.SATUDATA_URL}">`;
  const preconnectAssets = `<link rel="preconnect" href="${process.env.SATUDATA_ASSETS_URL}">`;
  const googleAnalyticsTag = `<script src="https://www.googletagmanager.com/gtag/js?id=${process.env.GOOGLE_ANALYTICS}"></script>`;
  const googleAnalytics = `<script src="assets/scripts/google/analytics.js"></script>`;

  const minified = ($) => {
    return minfier.minify($.html(), {
      removeComments: true,
      removeAttributeQuotes: true,
      collapseWhitespace: true,
    });
  };

  switch (configuration) {
    case 'production':
      $('head').append(preconnectSelf);
      $('head').append(preconnectAssets);
      $('head').append(googleAnalyticsTag);
      $('head').append(googleAnalytics);
      return minified($);
    case 'staging':
      $('head').append(preconnectSelf);
      $('head').append(preconnectAssets);
      return minified($);
    default:
      return indexHtml;
  }
};
