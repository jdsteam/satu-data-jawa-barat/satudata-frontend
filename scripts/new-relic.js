const { execSync } = require('child_process');

require('dotenv').config();

execSync(
  `sed -i -e "s/%NEW_RELIC_ACCOUNT_ID%/${process.env.NEW_RELIC_ACCOUNT_ID}/g" src/assets/scripts/new-relic/browser.js`,
);
execSync(
  `sed -i -e "s/%NEW_RELIC_APPLICATION_ID%/${process.env.NEW_RELIC_APPLICATION_ID}/g" src/assets/scripts/new-relic/browser.js`,
);
execSync(
  `sed -i -e "s/%NEW_RELIC_LICENSE_KEY%/${process.env.NEW_RELIC_LICENSE_KEY}/g" src/assets/scripts/new-relic/browser.js`,
);
