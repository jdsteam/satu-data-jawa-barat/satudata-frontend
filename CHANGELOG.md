# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [5.2.5](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v5.2.4...v5.2.5) (2024-10-21)


### Bug Fixes

* **home:** remove popup modal survey ([a376ec4](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/a376ec4e6931979fc631f7277051efdc37e4e2e5))

### [5.2.4](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v5.2.3...v5.2.4) (2024-10-12)


### Bug Fixes

* **indicator:** restore process verification ([c86f7fb](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/c86f7fb12642ec5d8ffbab20d170aee2afc9f4f5))

### [5.2.3](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v5.2.2...v5.2.3) (2024-09-30)


### Features

* **home:** add popup modal survey ([9254f6e](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/9254f6edd198cf734adf60f33964227495ba8397))


### Bug Fixes

* **dataset:** change status ([fffae05](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/fffae0538c296f14fcf1646b91e7ffa79aac4a97))
* **ewalidata:** hide from navbar ([1a57b3e](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/1a57b3ef25d944bd370feb129184b1a79120ec3d))

### [5.2.2](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v5.2.1...v5.2.2) (2024-08-13)


### Bug Fixes

* **mapset:** add authorization on download shp ([9dd6ab7](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/9dd6ab75f53bfd5fa07f745cea2285e8ca016ac8))

### [5.2.1](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v5.2.0...v5.2.1) (2024-08-12)


### Bug Fixes

* **dataset:** remove dataset last category on dropdown ([aab0ccc](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/aab0cccef866589d35c6320089aaae08d37d9761))
* **mapset:** change mapservice parameters ([e5ae5d1](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/e5ae5d1de5064b44b5744b65f9469cf0a61960e7))

## [5.2.0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v5.1.1...v5.2.0) (2024-08-09)


### Features

* (WIP) desk request dataset discontinue ([e9acd9d](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/e9acd9db649441aeefc4dad620d94053eaf9c50e))
* (WIP) desk request dataset discontinue ([dc70772](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/dc70772c3bfe617150d9bec5783ae319a80de361))
* **indicator:** add verification prosess ([cf6a45c](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/cf6a45c52d78ad6829071ea9017fe2527d109fdd))
* **mapset:** add list geoserver ([d0c45cc](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/d0c45ccef39e7971704ee8480c9dc2f9524fe263))
* my data dataset delete ([285fd10](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/285fd10e1eff50eedbf380ed298fe94f01581aef))


### Bug Fixes

* **dataset:** change dataset detail dropdown action ([4791ec1](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/4791ec1735263bcd2f613f6583ba4f6e05d8df4f))
* **mapset:** change geonetwork csw to elasticsearch ([589ae96](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/589ae9661fd51772503985943b871dd513928cea))
* **mapset:** change input arcgis ([a89583f](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/a89583f0feb647d4738d9e628401c81907bff7e5))
* **mapset:** change mapservice arcgis form control input ([16b3053](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/16b3053ca81084055b2a2f2f35512989a5ce68cb))
* **mapset:** change mapservice wms to rest api ([dda19f7](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/dda19f7b00938b1661cd42ce21fa225014bf3c7a))
* **mapset:** change metadata xml upload to dropdown geonetwork metadata ([99b122c](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/99b122cf3c8692656acdcd5e5b375d0124eb8a7f))
* **mapset:** change url file zip ([1bc6e33](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/1bc6e33d0d820f99fd7f84870519f641077fefdb))
* **mapset:** geonetwork elasticsearch query ([df60556](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/df605564a64d67aa789dff62cdc84e0ebf3fd5b5))
* my data dataset recovery ([ca42269](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/ca422690d50ee9c68d96510af8c24dc08b48106a))
* my dataset preview redirect 404 ([83382c7](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/83382c78335547f03a3ccf85fc2f938b61529009))
* my dataset preview redirect 404 ([a3b5739](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/a3b573920b90744708db4a4ba9472c787b950a09))

### [5.1.1](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v5.1.0...v5.1.1) (2024-03-06)


### Bug Fixes

* my indicator change id program ([c0fa1bd](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/c0fa1bdcd0f32de09c5d6d04d6cdc8d6dd8abbc0))

## [5.1.0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v5.0.0...v5.1.0) (2024-02-29)


### Features

* login google recaptcha ([b5fa6ca](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/b5fa6ca77c36bfbbbb91d7907e3f5f5cb534dc84))
* login sso digiteam ([2a23e38](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/2a23e38391c2a048d8491368646b3a985ffffb98))


### Bug Fixes

* header organization is undefined ([4565d67](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/4565d6715d1b1defd69cc6a180deb8263ee4c3ee))
* indicator basic data ([a050065](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/a050065f52bacbf5c442382e59fdad8d590e0292))
* indicator detail description font size ([b0ac72f](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/b0ac72faf4eaee42455e15bb450fc2fe3dfc1977))
* indicator dropdown program ([0facc39](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/0facc39569927eb4adcd2c8401f2a61aad2323fb))
* login remove sso siap jabar ([c427b70](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/c427b707370a87a153ef82c418945783321c3d8d))
* login status code ([02106a9](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/02106a9fb1ce8017f93af15c7748c534be4bffb6))
* login to many request ([5143a0a](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/5143a0a61dd27e78c014453cb110200b0ef79811))
* mapset url file SHP ([ceebd83](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/ceebd839cab3afd5d36db79b46b5b2b31eaae54c))
* my indicator progress final dont reset ([a1497ab](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/a1497ab0747715839cc6de9170fc3b6455832372))
* my indicator progress final dont reset ([032df33](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/032df33fd97d8ba8aa86d7535023ee9d8f29c369))
* my indicator remove mandatory program input ([06d9bed](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/06d9bedd54007dd4b4a6919f3105eca3ed988cf9))
* my indicator response basic data ([571cc3d](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/571cc3de3ffea0d236ec25ed2e648ee7ac89e769))
* popup survey disable ([38020d5](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/38020d594a52268cc3082d11d4fd43374e0ee1f7))
* signal model input ([8c81f57](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/8c81f577da860c18d4d75214b37799731142dac8))

## [5.0.0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v4.2.0...v5.0.0) (2024-01-24)


### Features

* login google recaptcha ([e0920f0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/e0920f0198f33e7fe69aae12bc2d0dc6b51d1d11))
* login sso digiteam ([29baea1](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/29baea1a1ef8e4ee57f466157dd64278f99c31f7))


### Bug Fixes

* header organization is undefined ([2a9fe57](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/2a9fe576ba668ca15ad9ca553dcaf7815c92c53e))
* indicator detail description font size ([f85699b](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/f85699bd03def4b87db6db16f99bdbe62d4b65db))
* login remove sso siap jabar ([55e29c2](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/55e29c2f794e5f4aee4469a10fdc4d482d621714))
* login status code ([3581b58](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/3581b580de51845c026a6a0d3ecca1d562cc0953))
* login to many request ([4a173db](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/4a173db4a4807bd4aae323935ffe7138774e71b4))
* my indicator progress final dont reset ([825f9b7](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/825f9b7442f2eb4766b536864636ee6bbf014e8e))
* my indicator progress final dont reset ([92aafbf](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/92aafbff70976fa86c6c0e384bf4f394cf1a1f48))
* my indicator remove mandatory program input ([305fa4f](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/305fa4f2a7798dc9853fe7d8bca919bf0100f80d))
* popup survey disable ([828e745](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/828e745d532fcc62caa291336ebbe6af1827d6dd))

## [4.2.0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v4.1.0...v4.2.0) (2023-11-02)

### Features

- indicator form dataset, indicator, & program ([e461d1f](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/e461d1f02079625be9fbde6eed2138b2bccd393d))
- indicator page filter category ([c66b03b](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/c66b03bdc884e7bbae28e8e5a4216da3979890b3))
- indicator page filter category ([086a4b9](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/086a4b9fc1a95a6fab57f16f5ad0794faae0f5c9))

### Bug Fixes

- desk foreach ([a41648b](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/a41648b7e4add3047326071fff37a782d8364e3f))
- e-walidata foreach ([320e7db](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/320e7db9164e9271f1f104c13192f8622266a517))
- filter foreach ([dc5a279](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/dc5a279969ea301e60dc40fbea702714ecc5345a))
- indicator form ([8066f08](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/8066f08b109c63069cd51ca367225a7e4972cb07))
- indicator form dataset reference ([b14702e](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/b14702e2a7bbd345c3d69f00a2d127f3f1d90f30))
- indicator form required ([d904c73](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/d904c73b747d186621768dbb1fd6b47596b05a4e))
- indicator formula image ([43936f2](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/43936f28d6965e176ed81cfd6a7d184eb00f46f7))
- modal survey ([80ddb63](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/80ddb63f6181c753ef521d4edc4d391cea6971f7))
- table head ([7e0a66e](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/7e0a66ecf6f8534483e6538efef215cfd702a6e1))

## [4.1.0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v4.0.3...v4.1.0) (2023-10-12)

### Features

- modal survey ([f159175](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/f159175112c18867758c6e622d0fc5f79af363cc))

### Bug Fixes

- dataset rating remove ([fb5679d](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/fb5679d80406ac565307d954d848322e7a670c0c))
- proxy log fluentd ([71462b5](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/71462b5b224ced92d2d2bcff9da6a599606d7cf8))

### [4.0.3](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v4.0.2...v4.0.3) (2023-09-04)

### Features

- dataset meilisearch ([15e2ae1](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/15e2ae1bb1e8172f67acd6d88744562978c1b28e))

### Bug Fixes

- improvment list dataset & desk action delete ([d87994a](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/d87994ae035879557506a64a481b793f626476ef))
- list dataset filter quality score ([f599000](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/f59900066b086463f5ded08fab975f61a228f7fd))
- modal mapset status ([a4130c3](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/a4130c37407f262ac18cf9b50ea0bb0173ff6cf1))

### [4.0.2](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v4.0.1...v4.0.2) (2023-08-15)

### Bug Fixes

- e-walidata handling error ([2437602](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/243760236c9359ef29425c2a4a05693500ac4dd1))

### [4.0.1](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v4.0.0...v4.0.1) (2023-08-14)

### Bug Fixes

- e-walidata assign & list indicator ([bbe9302](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/bbe9302cf40bb76bfef87df3b61b0c24709083c4))

## [4.0.0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v3.0.4...v4.0.0) (2023-08-10)

### Features

- e-walidata 2 fill ([2a9354e](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/2a9354e405efd52f3cd517f3d62dac133f464d28))
- e-walidata 2 header, tab, sort, & filter ([2c6a46f](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/2c6a46f2ae0150b93a93ba83ed6e59d3e8804332))
- e-walidata 2 monitor business field ([3ffacdf](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/3ffacdf179698a624e18dc00f44ffec4e79bcfc5))
- e-walidata 2 monitor indicator ([6fac8db](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/6fac8db7bee160eb3383308d508451867a655854))
- e-walidata fill business field ([bfbe575](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/bfbe5752fdaf3da476c28843bc144ab0335105ac))
- e-walidata fill input data ([c4d21c2](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/c4d21c2bc4359bcfa18ed17b6f62ea0be7396aa0))
- e-walidata monitor business field ([3bf65c6](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/3bf65c67f6a17cce1273e238c20560d8e9e5ec8b))
- e-walidata monitor indicator ([dc6f648](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/dc6f6482eb98460382af12cefe8118aba26544e2))
- e-walidata opd decline indicator ([1deba58](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/1deba585337474b484899c63dab332d8addb2dd7))
- mapset upload arcgis ([32bfe9c](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/32bfe9ce89ff9039ec61e37fbef239c7e84a144d))
- notification e-walidata ([bb40512](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/bb40512c44bc70c21d9b518a8b008e0165ca45d1))

### Bug Fixes

- daterangepicker ([67c523b](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/67c523b1f0fff1af79480bc35fd65471dae48f61))
- desk request dataset fetch sorting ([0891f26](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/0891f2646af584e64a225c77be7ad5741f854d06))
- e-walidata assign remove selected indicator ([6315390](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/6315390469633abbb336e9295cba11902cbc36f7))
- e-walidata fill business field status assignment ([cfdff0b](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/cfdff0be5fce305196f10d7bfd6c020db86e84a5))
- e-walidata fill id assignment ([ebc7be3](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/ebc7be3f738c266df7206d682e6171dc82d89810))
- e-walidata guards ([d647b56](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/d647b564177e12ebbc1e05e0567f0ebd5a1112ef))
- e-walidata route ([2952c53](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/2952c53ac96708e7aee0b54311d879f7ebabd92a))
- e-walidata select all by business field ([d66bbf8](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/d66bbf89a67ccf67ff57ee65388cdc5da22dee2f))
- e-walidata status date & confirm dialog ([6524be3](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/6524be36a56659701a8e59ab90c978f23e4cef1c))
- e-walidata tooltip status assignment ([a7052b7](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/a7052b7d516dfd6471f3ec736aebf76c460ddb26))
- hide quality score ([99b4ad8](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/99b4ad854bd4b9ece253ac1444f2a9b6d55bb5f9))
- hotjar ([013c960](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/013c960fc302e5230d06eae92df96189013f3d0a))
- image editor change blob to api upload ([60ed4c0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/60ed4c0fbd0448aed1a3ecc1eba3e1a3745da8f1))
- navbar notification button & scrollbar ([5d2c901](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/5d2c90129f661d7590793d4bc615e7ad1373e626))
- navbar notification revamp ([6479d8e](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/6479d8eb53b3f249e5fe757bbff0bda879329b30))
- notification revamp ([a7d522f](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/a7d522f24eeaed1031007f1f9e8370e5bfd6e279))
- notification service ([ebc87c9](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/ebc87c987e57e556c027d11ba3b2d381c3a8bed2))
- preconnect ([1bf9012](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/1bf9012efa4f72939ff5160b73abff96097a5fe2))
- scrollbar ([792d8c6](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/792d8c643e148c3676c96341dd46650ea3bc2b2e))
- tooltip to popper ([5175d1c](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/5175d1cf434874cfee420076cbbbc8e493ad45d5))
- whatsapp number ([993de31](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/993de3163d5731b2cdc0676bb2a0bb62dde502c4))

### [3.0.4](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v3.0.3...v3.0.4) (2023-06-05)

### Bug Fixes

- mapset attribute ([5258374](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/5258374d09e459c599a44bd4f3cd2838965f44dd))

### [3.0.3](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v3.0.2...v3.0.3) (2023-05-30)

### Bug Fixes

- navbar profile ([d5a065f](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/d5a065f8c41c2520fc5cd434ab39bec10862f64c))

### [3.0.2](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v3.0.1...v3.0.2) (2023-05-30)

### [3.0.1](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v3.0.0...v3.0.1) (2023-05-30)

### Bug Fixes

- add domain ekosistemdatajabar ([b7a72f2](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/b7a72f21adfa5b294a47b99d51d730bc57a23fb6))
- combobox business filed ([c5d6020](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/c5d602003a0b1c8cd77bafd9b3388dc187561331))
- navbar notification & profile ([ac35577](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/ac3557712c132d328905ffd0c70ace448865b4c2))

## [3.0.0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v2.5.1...v3.0.0) (2023-05-29)

### Features

- add api mapset ([58fd108](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/58fd108b195e668e4997dd95b8a81b4eac0907ed))
- add mapset ([0d28478](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/0d2847816a0ef639a6c99f285cba34fc646f14c0))
- e-walidata tahap 1 ([b39926b](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/b39926b641f77c63d97af4e07c50e839499a640d))
- mapset - edit form ([6b72a3b](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/6b72a3b35fc8082d5b03a9072881fa002836feb2))
- mapset - verification ([80614c2](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/80614c2e7352a8cc1b2f4ac7427de074e310d33a))
- mapset detail - layout & header ([935d609](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/935d6091eab99bfb2f937f59203ad7f440ddb0b9))
- mapset detail - layout & information ([f0b9aca](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/f0b9acae39f9ad79cca6635a83f19404658ede51))
- mapset page - filter topic, type, & classification ([df249c7](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/df249c749881a5af7bc237641953e880f7afffac))
- mapset page - list mapset ([2426ef4](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/2426ef4f1fb6bca6ac433f9010bf1cc49c96f428))
- mapset page - list mapset ([30ed7ef](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/30ed7ef2ecb6c4d1d687657160bc908f1936851e))
- mapset page - preview map ([1e63412](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/1e6341287ae1eb47d83acbd99fbce7d8db4b83df))
- mapset page - quick access catalog ([43dfcc2](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/43dfcc259d92d3a9fa7fc4c3f16859447b80b810))
- mapset page - quick access catalog ([a4527fd](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/a4527fde31f4ea27b2cd931d575e2610479ea252))
- mapset page - search & sort catalog ([efce952](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/efce95293f65b2ecf5d5680a3c592c73bf037d1d))
- my data - mapset ([c383582](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/c38358257f81e0bd8cea078de85761d41263408f))
- typesafe styles ([0feba26](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/0feba2639b644d85b364fb137db2785e5b118091))

### Bug Fixes

- add & edit mapset geoserver ([3a51974](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/3a519745409f123bd48ab118f269f7edaf5a08ed))
- add & edit mapset geoserver ([986ed72](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/986ed721f55780bf556d3410f6056f78c9097b9a))
- add & edit upload shp ([9bf3460](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/9bf3460cde0d2aab55c9a8a08900c686ecb52536))
- add & edit upload shp ([25a329c](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/25a329cb58e54d8bbf5118615fdb9c7d42aebc97))
- change arcgis to leaflet ([3a24222](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/3a242227d59c8ba9c3b7d2fad7f7850083b869cf))
- change assets leaflet to arcgis ([3a9e999](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/3a9e9994e35c64f8f97030cd6150b5f1442d9144))
- desk - request dataset ([25b194d](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/25b194d9082bacdde3ca02ebe94d2b15e7660a83))
- desk - request dataset ([70e12e4](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/70e12e4b07315a916249d480149479134bb5367a))
- desk - request dataset ([3b199a4](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/3b199a418e3098ec1af0280ecea1461f79917f04))
- desk - request dataset private ([f4a2616](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/f4a2616fb0efa5459f7ce81f580dc7fd285ca1c0))
- desk - request dataset public ([e8377b5](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/e8377b59ea12dfcca81b34de1f94d55eea2c2cb1))
- desk - sort ([5497360](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/5497360fc5aaa9c3648f2ea16833cac6270b24e3))
- desk - upload change ([38eb230](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/38eb2308addcc00b66973ea2d193ee7e72569f97))
- floating & footer ([3a4fa41](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/3a4fa4151fbd27277588391d7bb026c96d1f5130))
- mapset - add layer arcgis ([e17fa75](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/e17fa7577fd955773561de2f9eb43977637bcdeb))
- mapset - description ([414e4ba](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/414e4ba9b770c6cc8dd67a2cc3b18585f99ab0a0))
- mapset - detail header & information ([95cab7d](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/95cab7dd1505f11cdd97f198dfdc91c31edd7792))
- mapset - edit form ([6620ab2](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/6620ab234f6fea029df97a8805536a7929112f75))
- mapset - fetching from store ([69c37ea](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/69c37ea41de4aa1fce1aa5e667da7820aa5c60b2))
- mapset - guard ([8622f1a](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/8622f1a0cd69a59c6197f804cf3e970189e7f648))
- mapset - modal tracking ([287ce09](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/287ce0917cf77ae052f793939f4c2a9ba8d7ea07))
- mapset - modal tracking ([22217b4](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/22217b4968776ca5e34ac69e77a121f7dca9a330))
- mapset - status data ([f97aa37](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/f97aa377d67fffc35da4c82f3e5bb12c611f212a))
- mapset page ([8ffd53c](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/8ffd53c8753e2119b75ec059919f5c2b7363982f))
- mapset page - icons ([afefb84](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/afefb8425968cf486cc5e4a118659fa604296049))
- modal - feedback ([01f7a37](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/01f7a3732a3ea2801edc17d0c6b91a301a3b76c1))
- my data - fetching ([b336f70](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/b336f706b77b8a137dd612a64426b397ae6d92e6))
- my data - fetching from store ([357c061](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/357c06172acd6863c5db0dc57ada5640c9f614e7))
- my data - filter organization ([dcfbf64](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/dcfbf6402ae8f2559393226ed627a88a59e954db))
- my data - mapset action ([d9fc597](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/d9fc597686e559e7295ea2af5884f60b4d1db4a6))
- my mapset - fetching from store ([4eebbb6](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/4eebbb6e9e77b90a6d52d2f8da575ada68354ec8))
- navbar - add catalog ([2cc9f80](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/2cc9f803b6c8a96dd88a7ed77e81455278ee411e))
- navbar - notification ([3909151](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/390915187bbf7ba63e46ea7022693f4ae1cd1c09))
- navbar - profile ([84cac56](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/84cac56019ef403fc94b08a8a69faa36e6b5400d))
- prettier ([fab0c79](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/fab0c796b3be545c201925f1a85829edffca4e7f))
- preview mapset geo server ([ca750e7](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/ca750e75b1de0bdf2a4f77f61762f29773a9fb76))
- request dataset - modal action opendata ([959858e](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/959858e60d2682af69e3e8f86a01426d3d3f1740))
- search bar catalog - keyup enter ([bd0a135](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/bd0a135758894ca061d23ba1ef9871c4f21dcc1f))
- service - query ([666e887](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/666e8879f28700d715171b6b8375ae6bc97324df))
- services ([3f83dfd](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/3f83dfd3d914bb3e440f165114615c434052256f))
- visualization input double ([7ed4e5a](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/7ed4e5aebc9f0f115fc18de26f04e13d51375b52))

### [2.5.1](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v2.5.0...v2.5.1) (2023-03-09)

### Features

- mapset page ([7ba011a](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/7ba011a5deae34aad5a084ebb1979711ef2f0dd2))
- mapset page - filter organization ([be060e5](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/be060e59b8c9116a89a44be483891995a7a059dd))
- mapset page - filter organization ([72b0c8c](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/72b0c8ca38e73a6b37ad28ed7a66a909ab195199))

### Bug Fixes

- login - sso jabar ([a6916ca](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/a6916ca0730982e0fa63c27dc328d883a6b3f71c))
- revamp header catalog ([c297f02](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/c297f02f9d06ce01683a3f096a7247c24b2855ef))

## [2.5.0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v2.4.0...v2.5.0) (2023-02-24)

### Bug Fixes

- log service ([3fdb66d](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/3fdb66db4ee0504798c96750b288b306c8837935))

## [2.4.0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v2.3.1...v2.4.0) (2023-02-24)

### Bug Fixes

- backend service ([8720867](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/872086749e59c830e42add761dbda61218a417fc))
- backend service ([f4e7203](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/f4e7203b5f3bb04fb422e10f157858d021ab0c95))

## [2.3.0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v2.2.2...v2.3.0) (2023-02-10)

### [2.3.1](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v2.2.2...v2.3.1) (2023-02-22)

### Features

- autologin sso jabar ([1d2ad01](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/1d2ad0101c6c661a02e1d66038e86b6ec0abdf4e))
- filter - quality data ([d293054](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/d293054da9227714d560652dc0113a1f3e340b32))
- my mapset - store ([8b2f41b](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/8b2f41b5f273fc0367fb04c7000d631b0b150805))
- my mapset add - form attribute ([04dfe26](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/04dfe26fd8cc589d0e4a4b21502c3325854af5c7))
- my mapset add - form information ([8c6cd90](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/8c6cd90512591add16f75ea9135699994cb7f9fc))
- my mapset add - metadata ([2ccdc7b](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/2ccdc7b466e3a34e8f6d66914e2123ae68f2a0f5))
- my mapset add - stepper ([036ccf1](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/036ccf18bd89742383e5cbe1ddff23b258701e4a))
- my mapset preview - add table ([4b5992b](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/4b5992b94c5d8478943e009fc43daa94992b9188))
- my mapset preview - information ([9abb024](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/9abb0242a4eec2e5231adaa4a8780a975549d6e0))
- quick access - quality data ([6572ccd](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/6572ccdeff3c03acd71d23bbff8f99db3afd57f9))

### Bug Fixes

- autologin sso button ([cfb07d4](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/cfb07d4edf850f96ca6ac4acf8a51504019c1116))
- ckeditor style ([5cd393c](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/5cd393c3ecb0bb10559738aff53816a7c45c8257))
- list catalog - revamp search & sort ([2b98e5e](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/2b98e5e6f6b87079e1b1f97ad400373f1c11c143))
- list catalog - search clear icon ([ad603bf](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/ad603bf6bb16b919ed5bb7feaaa38feb87408abe))
- list catalog - search query param ([191d5ea](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/191d5ea01f4c29313b07a0b6cb50f58606e9207d))
- mapset - hide ([d8b314e](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/d8b314e7225dc8fcb15babd211d1b64fc74a51a3))
- my mapset add - change detection strategy ([6ecf6e7](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/6ecf6e7551ad3e06cbdf985d5fae99422288dd20))
- my mapset add - guard ([8ac28f9](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/8ac28f9fc05848b9ebfb8ca13c65c9fd047a7b4f))
- quality score percentage & description ([80dcb0a](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/80dcb0a7a8d4facd75ec68f327cdb1eb61a3985f))
- quick access - pick ([59c9f70](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/59c9f708344581263d4875a4f86c0ceeb6bffe3b))

## [2.3.0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v2.2.2...v2.3.0) (2023-02-10)

### Features

- autologin sso jabar ([1d2ad01](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/1d2ad0101c6c661a02e1d66038e86b6ec0abdf4e))

### Bug Fixes

- autologin sso button ([cfb07d4](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/cfb07d4edf850f96ca6ac4acf8a51504019c1116))
- ckeditor style ([5cd393c](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/5cd393c3ecb0bb10559738aff53816a7c45c8257))
- quality score percentage & description ([80dcb0a](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/80dcb0a7a8d4facd75ec68f327cdb1eb61a3985f))

### [2.2.2](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v2.2.1...v2.2.2) (2023-02-03)

### Bug Fixes

- cache strategy ([5f4e4e0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/5f4e4e076c1a9ca62b348ff2dfb74e5fd732376c))

### [2.2.1](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v2.2.0...v2.2.1) (2023-01-30)

### Bug Fixes

- quality score point ([bf9146d](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/bf9146d170eec39525134513a7d93a9316d93400))

## [2.2.0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v2.1.0...v2.2.0) (2023-01-30)

### Features

- dataset detail - layout quality ([dec37a3](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/dec37a3bef1c1ab1e40f2c69331d3bfc818ed090))
- dataset detail - layout quality ([b86d72b](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/b86d72bdc2ab7b74012855d4249c71c3b525a4d5))
- dataset detail - layout quality ([7945959](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/794595976a789510c6225921b092269bc3307d9b))
- dataset detail - quality score ([b14b0b0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/b14b0b05d2dba1143f7588c9f30937f3872b58dd))
- seo - new seo tag for satu data ([8362197](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/8362197a9010d3ed53ed61582e852b9884fa52d9))

### Bug Fixes

- dataset detail - guard ([d5b342f](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/d5b342fcc9fdfe05ddba01a6f17e2fd6ec6ee333))
- dataset detail - quality score ([4eed0c6](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/4eed0c69ebd7cb5f0da0080c5e1721ecec789cbb))
- dataset detail - quality score label ([8fff907](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/8fff907e2440c745b50edb47b2d33a98090a2c35))
- detail dataset - quality score conformity code area ([906ff7a](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/906ff7a5ea1e0f84836280b40532aa57534ab0f0))
- detail dataset - quality score empty stage ([e5df737](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/e5df7376e283e551d8f44ea6816c3d8c05b0218a))
- file replacements ([bc87d7a](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/bc87d7a9528518ff0f13104fd75535c08e5d55ac))
- for module ([1f7ff46](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/1f7ff46bd53ad975cf6751c9f3d681d0244a3062))
- for module ([1c904bd](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/1c904bd3992f6c2114489fc17f2017f746ea275a))
- for module ([f7bea08](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/f7bea08501539248cec9b440ada4177e2af55ce7))
- google analytics GA4 ([7d3a8c9](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/7d3a8c97c0ff5b48333aebf1740677bba997ccfe))
- help page ([722b8bd](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/722b8bd8a1d5e51752eee8c4f7669d4279752276))
- history dataset - insert from add or update dataset ([8c124b6](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/8c124b6699f998a466ce9afde4b809932f437dad))
- home - statistic ([e42be69](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/e42be69b8d38a5036e8bbd0f5bbdec9792a217d3))
- let module ([3fcbeb5](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/3fcbeb5dcbbd8d58ebd6ecd1d3e866f11ea1ad13))
- list dataset - quality score label ([7aef36f](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/7aef36f1860456b12d766f58c1b9b4c773995163))
- modal request dataset - request name ([e67914d](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/e67914ddcf70a6a13f51d8b057e2c8c69173ce7d))
- non blocking ui - desk ([18ed95f](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/18ed95f6131ba99cf8da910702b7400d9ff9eda1))
- non blocking ui - log activity ([c25469b](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/c25469bc211848666c043968efff1682d331d4a0))
- non blocking ui - login, home, & navbar ([4fbc797](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/4fbc79774f254abb615e3a2d26d8831686e8da44))
- non blocking ui - my profile ([915f70c](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/915f70c25fa3100398f98fbe21074170f7b623e6))
- optimized image ([4aff369](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/4aff369074e6b2b338a13c2c54cf5da05a82ec25))

## [2.1.0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v2.0.4...v2.1.0) (2022-11-04)

### Features

- pwa ([991dc88](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/991dc8821f00370fac62e8b5c518854cb766e953))

### [2.0.4](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v2.0.3...v2.0.4) (2022-11-04)

### Bug Fixes

- dataset detail - counter ([21daa4e](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/21daa4eb2cbf4750b0870679883aca869b07a57c))

### [2.0.3](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v2.0.2...v2.0.3) (2022-11-03)

### Bug Fixes

- index ([1eeb204](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/1eeb204c6bff84404caeff2f4987073094d0d2d6))
- index ([7af0481](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/7af04814eba397561ac02aa25231238e7896df44))
- preconnect & preload third party scripts ([5747fd8](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/5747fd8a300fdb0b007ba746e47ce693bf19f4f8))

### [2.0.2](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v2.0.1...v2.0.2) (2022-11-03)

### [2.0.1](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v2.0.0...v2.0.1) (2022-11-03)

## [2.0.0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v1.6.3...v2.0.0) (2022-11-03)

### Features

- business field ([69cea58](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/69cea58fe7b4aec22a503e41776ee1e8e0fb2a23))

### Bug Fixes

- badge - classification dataset ([509514b](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/509514b840ae83de07b211d2fcc20f553dcf5704))
- badge - source ([d102f35](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/d102f3509611b8019234b7db11f86cc75aaa9951))
- breadcrumb ([97bc87c](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/97bc87c19d82336676d027d1995de1557aaae973))
- carousel ([78ddccd](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/78ddccd6250597a7e606d7306ee57aaf5d8c8e4f))
- dataset module ([90f4004](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/90f400465554f01f43eef9e25171c67cd44745e3))
- desk - pagination ([c87fb5f](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/c87fb5fdf0a0da79ef7c0d94771522fc81e40e0e))
- detail dataset - preview table ([259e13e](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/259e13e11ba6fd991773ae2ef87a3f46a5fc8f4e))
- dockerfile ([102546f](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/102546fc24c77e0c1234f93deea966c2ab10ee8d))
- filter ([d906139](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/d9061391a4734c9e263174ae5eac463a0bdc03f2))
- form module ([5ecd566](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/5ecd5665b5f348668d4ff06bb07647dc2b2aff12))
- header & breadcrumb module ([e046820](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/e04682060b1915a30070c81123a905d633a1ab2c))
- home module ([5c4093b](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/5c4093ba5d023812a6bbec82db39f96b50a864e9))
- icon ([687fdc5](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/687fdc54f1ae21a9910e4009c45ce55927be1c7e))
- list empty ([b3a639b](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/b3a639b3fd8c171bda4f0f3c074f3808c9ee9dc9))
- list notification ([1a84e03](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/1a84e039aa55b91cb2a398faaef0ef37bef9c5cc))
- log activity - login pagination ([11265ba](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/11265ba1c56c438f79b40663c94b1346cd462599))
- log activity - page pagination ([336bf80](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/336bf806e3c03e7809c21dcf94470880fa76a4f4))
- log activity - view & access pagination ([0aa628b](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/0aa628b5db535423471fdf4e1884f9858929aac3))
- modal module ([5d544ec](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/5d544ec506eb26f52992fa319f79258c02135123))
- my dataset edit - business field ([b27fd67](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/b27fd67b634b3dc4ceb50f3e65f8281af595e64e))
- organization page - implementation pagination ([9ea413b](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/9ea413b62d64faf7be00689ec08d5efd81da7309))
- pagination ([bb0c378](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/bb0c378f88d9a4168f919f7f039e4f49948af0e2))
- shared module ([6182c7a](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/6182c7a660b6441fe2e4048a249631683c318a5e))
- shared module ([76dbafb](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/76dbafbc038d7bbf7886869694b3b6326d05f0ba))
- store ([ccd7b09](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/ccd7b09ec38ebf9a96770523c4aced29f16b3c28))
- style ckeditor ([131a135](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/131a135aebea24dad7f60e1e614cfe4da37ef6b0))
- style rem ([94376d5](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/94376d51e6169efd2bb27ac4515fc6af6fdacbe4))
- viewport scroller ([89122b4](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/89122b4cf2d3bfd53c7980aa6d9dad4acea08489))

### [1.6.3](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v1.6.2...v1.6.3) (2022-10-14)

### Bug Fixes

- filter ([1581d0a](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/1581d0a861ea59b87adbfd2b24c44d4a01c4ad31))

### [1.6.2](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v1.6.1...v1.6.2) (2022-10-07)

### Bug Fixes

- my dataset - add & edit frequency dataset ([db0e532](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/db0e5322588c0df97647115c3e8e8c71151b59e1))

### [1.6.1](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v1.6.0...v1.6.1) (2022-09-06)

### Bug Fixes

- footer & help - whatsapp ([f4734ab](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/f4734ab2fef5a0235c4c93e908e29adf38242c4a))

## [1.6.0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v1.5.2...v1.6.0) (2022-09-06)

### Bug Fixes

- login - redesign box ([0122d25](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/0122d259ac00ae89f85925c860d8ac580f40a555))

### [1.5.2](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v1.5.1...v1.5.2) (2022-08-31)

### Bug Fixes

- my dataset - edit status data validation ([27259f8](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/27259f85d7e9f0fe940c86e70a8532accfc05a67))

### [1.5.1](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v1.5.0...v1.5.1) (2022-08-08)

### Bug Fixes

- my dataset - note validation ([c67010d](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/c67010d94fce828d8ddf3e7b1cf596c087733abd))

## [1.5.0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v1.4.7...v1.5.0) (2022-08-01)

### Bug Fixes

- my dataset - metadata double ([1344b1d](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/1344b1ddbef848464c20798748c1cfbd898cd360))

### [1.4.7](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v1.4.6...v1.4.7) (2022-07-27)

### Features

- login - separator ([0cb11c3](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/0cb11c34d6ea9ea183395465ffaa5e1557d2e550))

### [1.4.6](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v1.4.5...v1.4.6) (2022-07-24)

### Bug Fixes

- request dataset - modal ([9a7a93d](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/9a7a93d5bfc2989ed4222fab6f2e5be86c3633ab))

### [1.4.5](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v1.4.4...v1.4.5) (2022-07-24)

### Bug Fixes

- tracking - sorting ([1446cd0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/1446cd0335be026e553fb0cf5024f6ad4c88088f))

### [1.4.4](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v1.4.3...v1.4.4) (2022-07-22)

### Bug Fixes

- google analytics ([3f72c78](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/3f72c78ab85eea020d27146f1dd7fc56456716a3))

### [1.4.3](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v1.4.2...v1.4.3) (2022-07-21)

### Bug Fixes

- google analytic ([f49ad79](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/f49ad79a2124f7dd72e80f4cb5053755af49fa72))

### [1.4.2](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v1.4.1...v1.4.2) (2022-07-21)

### Bug Fixes

- env ([b903053](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/b903053d4eedafc5e04b32207021febca735f920))

### [1.4.1](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v1.4.0...v1.4.1) (2022-07-21)

### Bug Fixes

- partytown & socket ([f6af0eb](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/f6af0ebe93aa853786999930f2983b4f4c3a7547))

## [1.4.0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v1.3.0...v1.4.0) (2022-07-21)

### Features

- partytown ([516c418](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/516c4184493d63ab26f2860c44aea1bb175af6c4))

### Bug Fixes

- desk - request dataset deleted ([f245504](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/f2455041a0d65724ddd7813659e2ad31ed1637ce))
- env ([1470f95](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/1470f951eddc11feb776f3c18d4248c01c02b814))
- tracking dataset - revamp ([789b666](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/789b666cee6e409fff818df90bbe59b49b7e7339))

## [1.3.0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v1.2.0...v1.3.0) (2022-07-15)

### Bug Fixes

- login - change method ([d886738](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/d88673881148bf0e06efcd4a1bd95920b5cb5f32))

## [1.2.0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v1.1.0...v1.2.0) (2022-07-15)

### Bug Fixes

- login - parameters ([8ba7ec7](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/8ba7ec7746682688c70233c84215c2ea6ce12827))

## [1.1.0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v1.0.0...v1.1.0) (2022-07-15)

### Bug Fixes

- login - decode uri ([f5f6ecd](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/f5f6ecdc731a88c8c11798a76cf3b4802c29b7be))

## [1.0.0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v0.2.0...v1.0.0) (2022-07-15)

### Features

- login - auto ([39c1215](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/39c12156dea5ae11ac3a08cf9eda3f3ae8efd27c))

### Bug Fixes

- login - auto ([46f5787](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/46f57876ce78eac3d9c8c99973cdc8bca9606b40))

## [0.2.0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v0.1.2...v0.2.0) (2022-07-04)

### Features

- desk opd - edit button & redirect ([66586e2](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/66586e23d79708e18e68afba824c4005594ef195))
- desk version 2 ([d7012b7](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/d7012b7b3ac8b9898aa11ed3ea5b9443aa68ba8c))

### Bug Fixes

- routing to desk version 2 ([125d0c0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/125d0c0102876f855aa3efeee3897d5212140d75))

### [0.1.2](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v0.1.1...v0.1.2) (2022-06-07)

### Bug Fixes

- add & edit indicator ([9c18369](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/9c18369fb5a5724ef4a68d45f7bf4b7d6551e37f))

### [0.1.1](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v0.1.0...v0.1.1) (2022-05-09)

### Features

- add label dimension in list & detail header dataset ([00c7e82](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/00c7e82116661f930a73270d80682644bf5a01ca))

### Bug Fixes

- filter dataset tag ([1f25e54](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/1f25e54df64c96b3462a294a946ddace0e9bf323))
- metadata source external in description dataset ([7cab3e0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/7cab3e024c857ca8063a93541e594becabc39c77))
- trending, period, category, & description dataset ([44bc736](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/44bc736c6b35fd95bf48b7545e4d5d88c6c61797))

## [0.1.0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v0.0.17...v0.1.0) (2022-04-10)

### Features

- add note active/archive dataset in detail dataset & my data ([a82c603](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/a82c60346ae21c7faf624f0fdd9965c2ecc7dbb0))
- category data & period in form dataset ([a608567](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/a608567b42df090f36f125d813dcca7afa92d645))
- log dataset preview graph ([cec3fd0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/cec3fd0a51d2640c277ef040bd8ea5182f3bf2af))
- log dataset preview map ([298f3ef](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/298f3ef2d546f13d25bd4452393cee2bbef527ba))
- log search ([545cce9](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/545cce98888cabb3942e9b159a3200867a9d4b16))

### Bug Fixes

- category in form dataset ([d0a5970](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/d0a597017bf8a8258a50fac591335a271ea7063e))
- debug in sonarqube ([9deadcb](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/9deadcb806fa6a46d32ae35bcbb29781765fbee0))
- debug in sonarqube ([86aac79](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/86aac797f3dfca2d199b723186b9f5d3551d376b))
- dimension in form dataset ([1db1fd9](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/1db1fd962560504276c8c7f2960f9f7fa397928a))
- form dataset ([110b4be](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/110b4be877509f47108ce3c30f24ec0741103e52))
- new tab information license in dataset create & update ([fb801a3](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/fb801a311e293a7898d7f8a8654228a0df1c7ea1))
- purpose in form request dataset ([da77670](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/da776708b1a59672cb67128942588eb0b3c825d9))
- qualitygate in sonarqube ([c271030](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/c27103085be840b0996769b5e37aa2a5b5b31a08))
- qualitygate in sonarqube ([27535fb](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/27535fbaf3c07bd989ff9a9bc0bd1ce205cd55d7))
- remove token in logs ([4b201fd](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/4b201fdd80aa035f4640e8f9a20a6ec252ba5ef5))
- spreadsheet num row in dataset form ([036cff1](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/036cff1425fc9f383670765185c8f4a97f20a415))
- wording status in request dataset ([4176926](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/4176926b77d0cec4439ef65351e141bb80f71913))

### [0.0.17](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v0.0.16...v0.0.17) (2022-03-02)

### Features

- hotjar ([ad1a7f3](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/ad1a7f39a2723c194476fcfb91c807a59ea4cbf5))

### [0.0.16](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v0.0.15...v0.0.16) (2022-02-24)

### Bug Fixes

- feedback modal ([8e34cef](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/8e34ceff028a331b1ad4eccef5583d0775433283))
- feedback modal ([8a68fbe](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/8a68fbeb6eab006206b1d92eed01d4eae1f1cb0b))
- feedback modal ([9ab194c](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/9ab194cc37631c237b8d8006f1e6776b19c93cf7))
- indicator download modal ([86a4a9b](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/86a4a9b3c73fb5509a120ef6a0e0cbd58d0585c9))
- request dataset opendata modal ([75fa4ff](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/75fa4ff927fb3c89018d8243a2bbca69f5f2abdc))

### [0.0.15](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v0.0.14...v0.0.15) (2022-02-21)

### Features

- add content help ([eead2e4](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/eead2e465f9473dfbbd1f62bea469ff6f99cbdde))
- add modal dialog when leaving page add and change dataset ([80922f1](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/80922f15659f17904447b7f4d1e6d203466d74ac))
- content help license, cleansing, & user manual ([3b813a8](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/3b813a8e92066349fc808782424ef98cad4d631d))
- navbar v2 ([88848b3](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/88848b33825d30a155c970777384dc748bf87543))

### Bug Fixes

- add notification badge ([e905fba](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/e905fba1f035f66e6cbd3b7a1c7963c258e7b7ca))
- bypass guard in my dataset edit ([ab0cca2](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/ab0cca24128695467e501cd9e39456a54890c60a))
- change data row information ([5a4ed46](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/5a4ed460bc55370ad16acb83425f8b7bacff4983))
- change data row information in dataset edit form ([5631f73](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/5631f737e47cc035454c2141a72a7e6a712b6986))
- change data row information in dataset edit form ([c4b030c](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/c4b030ce68897845bd8ac35545b3ca57c51fe93d))
- docker build add network host ([6d6985c](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/6d6985c60fa1c24b535ba8ac15678fce81bcc242))
- information in detail dataset preview graph ([f1aa9e4](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/f1aa9e4135296fb4c2b763a92bb641b5a72d0d51))
- notification line space ([04dc3a2](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/04dc3a24648a9bfd6ac23c6576fd813e6b0f5596))
- organization name in indicator download ([7fbbb56](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/7fbbb5681b02a3aa51f257a15eafa9773ecb191d))
- profile picture ([e6160d8](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/e6160d8542d0778e9d70983b22c59f584f24d26d))
- profile picture full url ([b4fb81b](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/b4fb81b7101a5467d1255700f4ade9f6b13dca36))
- sort content help & change whatsapp ([b4b7a37](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/b4b7a377b3c6ce2ec3ddb9074f288e90f63fa387))
- style notification ([7f9b61d](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/7f9b61df0b799bb706ce29e1c77e79fe00150a5c))
- wording help ([0ae4476](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/0ae4476ddd3d1815d7556ccdf74c6e2212ea5cdb))

### [0.0.14](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/compare/v0.0.13...v0.0.14) (2022-01-26)

### Features

- (WIP) add access permission on dataset add ([aa82cd5](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/aa82cd5aad0c7e7048e25d2792adb8a95f12e54c))
- (WIP) add access permission on dataset add ([98ce288](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/98ce288dfc2bb3522f13bb298780ef133a378dca))
- (WIP) add access permission on dataset add ([1cf4b46](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/1cf4b46d10215ffc14a65a3ada0a6bc7cac1f445))
- (WIP) add access permission on dataset edit ([3f832e1](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/3f832e1e84bcee5c6d5503300f7c91595e510d3d))
- (WIP) add feedback form ([ad12fc0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/ad12fc0814a5bb4fe661c34069855bc7e6398d7a))
- (WIP) add filter quick access on all list ([900f402](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/900f4029d692af4e2c11a7a2d91cf4d552ea0474))
- (WIP) add filter quick access on all list ([1a3d814](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/1a3d81482a5019a229620fba29d44708b6bde40b))
- (WIP) add list history dataset view & access in history page ([b145c3a](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/b145c3aa8f9f3b47f37a7e02aa0ba5e08a3e4882))
- (WIP) add list history dataset view & access in history page ([b338d19](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/b338d19bb94aba2ff4f79d9a55e4f64b9717e013))
- (WIP) add list history dataset view & access in history page ([315fb3b](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/315fb3b640420eee80e1921923aea02b252098cc))
- (WIP) add log activity view & access indicator on log activity page ([37548ed](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/37548ed5ed70c6c5413ae9ab4bf8c85c3dffefa2))
- (WIP) add log page ([910ced6](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/910ced6fffb9ec9e940ed034a9c5e952a997113a))
- (WIP) add preview dataset in map on dataset detail ([65239bc](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/65239bc2399d83b421a579dc95037b076ebd4ea5))
- (WIP) global style ([63f6c76](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/63f6c76608bb4b43d6aaab13bd5d9a5192c9d481))
- (WIP) home page & visualization trending ([8376b1a](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/8376b1a50f4353b01911019cce5d12512556dac9))
- (WIP) new ui on dataset detail page (detail dataset description & metadata) ([a457df1](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/a457df1fddea3cc6ca8ba6f69bd1585a5c3717b2))
- access control on form visualization edit ([98f0e73](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/98f0e730499291368a90808e258f5b1d13563143))
- add & serach nomor ticket in request dataset list ([0a77841](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/0a778414d00d5ffbd1379a7b4914c7404050844e))
- add a notes if it changes on dataset edit ([6eb38fc](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/6eb38fccbf58d38e7ed3907f795e7b6717df4b64))
- add access control on form visualization add & edit ([fa26fae](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/fa26fae69450083437831e12e76d95a267f37ab4))
- add action button on my data (dataset) page ([9b8f000](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/9b8f0005b2ca92e6c50389c6b73983b7592e5652))
- add action button on my data (indicator) page ([95c5be2](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/95c5be267131e9a48fa3a42b07807980666035fa))
- add active & archive indicator in indicator detail page ([5ed31df](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/5ed31df7db7d45dc67dfe3c48206c811fc00912f))
- add add log activity view & access indicator on log activity page ([141fd09](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/141fd0966039e79bc894e6ceff13d3e70ad985fc))
- add branch staging ([d59fe96](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/d59fe96c6c1a06debf1565ac6b1da977613a8e20))
- add ckeditor ([df23ca2](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/df23ca203cfa10eb36e4a58b0bc1e931078c2405))
- add create dataset organization & save to draft ([71fff2e](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/71fff2e19a73c64384e65178e88b5daa15a2d2af))
- add desk walidata & opd ([d69a636](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/d69a63679664b9a08737553096bf19c862f09137))
- add download image & pdf on visualization detail ([1c02c2c](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/1c02c2c67067d79e949802d0398885358270a708))
- add feedback form ([6d3e304](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/6d3e304924eb5d1a2ebe0079648147c4a4b60242))
- add filter classification dataset on dataset, visualization, & organization detail page ([34514b3](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/34514b3ef123bd721aba0292b9b12b7c61f8baeb))
- add filter multiple on dataset detail preview table ([216837c](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/216837c7bbc4f5b692f4ec3353ddae179e3c288c))
- add filter quick access on all list ([4b63917](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/4b63917b80d6c395a39442272e0d1249f67f69f9))
- add filter tag on dataset page ([efb7368](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/efb736871c6a1d14530063f81fde95c8d542b9e7))
- add floating to top ([c72b398](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/c72b398c0ac17d16e066835303d6668f8096e560))
- add global style card & table ([5ab53e8](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/5ab53e8181c0f8e196a0f15b807f5c51275fc0e5))
- add help page v2 ([70e2c53](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/70e2c539266c36671d293bb7383451f2337b7e25))
- add indicator & visualization on organization detail ([02fbee9](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/02fbee91225d4371519321b42b050ff1a1b41703))
- add list history dataset, indicator, & visualization status view & access in history page ([b1ce47c](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/b1ce47c8066802a270c44e1a57294e87bceb0d79))
- add log detail dataset, indicator, visualization, & organization ([62cf485](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/62cf48552be7e27435a2e409890202d145929242))
- add log download dataset & indicator ([62dba15](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/62dba151017db2fada04fd4f9687f17a56d11e05))
- add log download on detail visualization header ([3b3a0d9](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/3b3a0d97702e1a702c51aefbb564a0ca7743baa0))
- add log download visualization in list log activity access ([b3383ac](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/b3383aca6138c31864b767a12fa5cb37608a0116))
- add log login ([99e12aa](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/99e12aa455d28b5bce386a6d0bc05fea863354dd))
- add log page ([2e0d2ee](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/2e0d2eefe00d11b3120289a912e778519f4838e4))
- add month picker on indicator add & edit page ([9a73b0b](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/9a73b0be60be616fd58002d839b4afefd152b159))
- add month picker on indicator add & edit page ([984420b](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/984420b595da2d016c1d6e1d9d7b5e41c4f8455a))
- add notes modal when finishing a request on request dataset public ([be005ce](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/be005ce36f1ec24837506b2832d1b4f426190eb7))
- add number format on dataset add & edit ([f2addf6](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/f2addf63dc81fe1f74fad2c23be6b1d9636b94f1))
- add popup if leave page in my dataset add ([7545334](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/75453348da39d19ca1da3947bb402d918bc5476c))
- add popup note on desk request dataset private ([7b03884](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/7b03884f88dc7c374acff579c211690f2e176755))
- add preview dataset in graph on dataset detail ([92cf5c3](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/92cf5c34847bde2ab4955f633ea9c84841ccf424))
- add realtime data on dataset create ([d0e9dce](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/d0e9dce6ee8088a152c96a558b402f9e20a07716))
- add realtime data on dataset update ([cd4a8ef](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/cd4a8ef58a866799f5639a80e4413779656c4ae4))
- add request dataset detail on desk walidata ([4734326](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/4734326cacffa601b568e8d92fd6739c141aa5ef))
- add request dataset detail on desk walidata ([1b8301d](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/1b8301df1e411c2c7ba507bcfcb6b525a27693ea))
- add request dataset open data on notification ([01bb6ec](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/01bb6ec5e42771137414a06745713faac544f1cd))
- add request dataset v2 ([eb7a5c7](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/eb7a5c7bdbea54e753704391b057e4761c9aa2e2))
- add request dataset v2 ([5a71227](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/5a7122709643a9b4d31a96ceb12d617e8e610065))
- add request dataset v2 on desk walidata ([e7e1ff9](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/e7e1ff9a37b4b0ec61ff8bff1f64d737c5917469))
- add resizavle on dataset detail preview table ([322ac2d](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/322ac2deea8e77fbb737c8c391dcaf6edd096baa))
- add search in filter organization ([9e74c64](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/9e74c643fa1a7f6efe69c79d9c3e5bfafe842cc0))
- add search suggestion in dataset, indicator, visualization, organization, organization detail, & my data ([262ed5a](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/262ed5a5aa565dfccdbad85757e92832d3cdbe64))
- add sentry ([756cc1f](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/756cc1f9830ee4c051229a7523f143244f14198c))
- add show hide password & change placeholder ([aface1a](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/aface1a607340d62af556071f07411c4639c427f))
- add sonarqube ([2080ab2](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/2080ab23f48079a140cde9943d9351d58b531b8c))
- add sortable on dataset detail preview table ([d0f987c](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/d0f987c35ad419b095b07fab40ab016fc97e06fe))
- add source & interpretation on indicator add page ([b393194](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/b39319492b1aae9ceed2b07d3b0a3fe554488fae))
- add status realtime on dataset list, detail, & preview ([c866cb9](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/c866cb964c8527803c438e1970e897c63ef3df51))
- add terms and conditions on authentication login ([2c7d78e](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/2c7d78e42e655714c9cf4a1f8fa308f6c8e20092))
- add text editor on indicator add & edit page ([97daa9a](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/97daa9a61e707eafc06729f9c205f87ef38363c7))
- add tooltip in button more action ([3d85c5b](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/3d85c5b3cfacac14316ad09639e9f80cfda18367))
- add tracking dataset on desk walidata & opd ([18cff92](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/18cff9266ceb69edeed0c155ec540958feef7505))
- add trending indicator & visualization ([3b1e25d](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/3b1e25d6b1f4eddf00f6ff048cb5de9141e0ccb3))
- add trending indicator & visualization ([7d9c5a9](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/7d9c5a9efa76bf6dadcb558e6b17527f69aefc99))
- airbnb eslint ([c0d65ad](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/c0d65adefcd17866f2c5cd791192b900f9259c0e))
- api login v2 ([ac6e2f2](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/ac6e2f28e415839feb9aa1ed1629a3dd73e80f8e))
- change filter redaction - diunduh terbanyak ([c971e22](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/c971e226eb51d72e1091c2a59538a36b13f3c156))
- change text editor in add & edit indicator page ([68087cf](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/68087cff2108d8da1ba9ef29698d97fd95ecd796))
- dataset request public navigation tab ([34253dd](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/34253dd719a8421406a84f60c6f3f6051b4ba0f1))
- filter - urutkan redaction ([8b57d9b](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/8b57d9bb564382df7262f7ea957492ab2387457b))
- footer - add new icon + whatsapp label ([61680ad](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/61680ad9f08f9504e0c411c0dbe945c87f98b6d6))
- help - add contact us box ([5d71b30](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/5d71b30e2540d233cf26abf428f19948582e0f12))
- history tracking for request dataset public as notification tracking public ([0e7aa54](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/0e7aa54606740d9a089e8a3c876a274d6d7bcb6f))
- husky ([6be1a10](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/6be1a105b6497c0961a56ad453fdde10cab39ab6))
- log activity ([5e6c068](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/5e6c068c52197776d342a4870451aabc4bf5c5a2))
- new navbar icon ([abd82a5](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/abd82a54c3afa34053b1be14a2a1e61788e19121))
- new ui on dataset detail page (detail dataset description, metadata, & about) ([71d375d](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/71d375d10b3ec991a2dd1c01fda02a9693a628c7))
- new ui on dataset detail page (detail dataset header) ([3a297f9](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/3a297f9041630a265812ba86c8cd9e8e6b3f2a80))
- new ui on dataset detail page (detail dataset preview graph & preview map) ([b96a92c](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/b96a92c0e8090bd2547b127d4929a87eb7482192))
- new ui on dataset detail page (detail dataset preview table) ([d292ee2](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/d292ee292c84b55aea106c811ee4d342688d1e48))
- new ui on dataset detail page (detail dataset review) ([22d0d09](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/22d0d09b595a933c5ef840d6dcd738355e26bd37))
- new ui on dataset page ([1f29e1a](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/1f29e1a59f8368b906370662f930aa1563972255))
- new ui on dataset page (list dataset & search bar) ([af4df7b](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/af4df7b56976d240ad4ac527094947e7ba77666a))
- new ui on filter ([b3b27bd](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/b3b27bd409c3feb04af3a60d74cf7c91339adec1))
- new ui on home page (list trending dataset) ([7e45b4c](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/7e45b4c58910f96539993539beea8dd75fc3a336))
- new ui on indicator detail (header, description, & preview) ([4dcdf24](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/4dcdf24d4a14f350b7f70c412801dec301d8fb4a))
- new ui on indicator page ([51b07ca](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/51b07ca7f7223df4338d48cf70da19f660d015ab))
- new ui on organization detail page ([0034b22](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/0034b22b6991b57bfe0656fa37f2e0597f2a2676))
- new ui on visualization detail page (header & description) ([8f6ce35](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/8f6ce359cf3ce934b39c5019f07086ca8d1b9ef7))
- new ui on visualization, my data, & organization detail page (list) ([2a34b80](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/2a34b80b7943c554d71fac40a9a97680b777fa1a))
- no menu navbar - help icon ([7c43868](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/7c43868d87791ac32b8b23af74ebfecdb2caba66))
- package-lock ([aa7591e](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/aa7591ecf1841259cc09a0617224abc4aec2627b))
- prettier config eslint ([61d5ea0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/61d5ea08e5c35df69dbefcd5ffa4c231bc907313))
- prettier eslint ([dcabb30](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/dcabb308aba0215f181c490b8d581aa598663c4b))
- redaction change on successfull download swal popup ([3e1ddcb](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/3e1ddcb79a5a5b6c5e5347930dd723c382350e68))
- refactor indicator add page, fix mandatory & padding classification ([0487f3e](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/0487f3efcc8298c7997a0cda14a419a738d652ec))
- request dataset public - store & ui list ([8cfee2c](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/8cfee2ca1b707665a1537613cfb595f18c9f76c9))
- request-dataset-public 4 actions done ([e5a1c1e](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/e5a1c1eef934fa226c1cf868534269acd91b64cf))
- request-dataset-public detail modal ([bb369cb](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/bb369cb47d27a030f3302544fef9bd228db4fc02))

### Bug Fixes

- add default row spreadsheet on dataset add ([2e800e0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/2e800e086904cdf366111a2c9f2662ab882da3b7))
- add information & loading on forgot password ([14e218e](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/14e218e5ba64b870ffc58e4987fc2fd60e4bb737))
- add quill library ([dee7421](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/dee7421d5a64f60f99e6ad009c8b67229cd7ccfa))
- add skeleton on notification & change score on feedback ([30726a6](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/30726a69314992ea2c61f1fbe5012c4d64eb8e6f))
- add username in desk verification ([6524f3c](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/6524f3c98030287a144341db75569c17d68918cd))
- agreement ([d77b71e](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/d77b71e3a45813f11b31770aeb27fe6b3fe5b1b3))
- align, width, & height on label information ([58d1058](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/58d105862c5668899907f819f7a0f6261dd0b8ff))
- allow save to draft if not mandatory in my dataset add ([3c78c71](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/3c78c7114125e0c0df9872af3f1d22b442821f1c))
- allow save to draft if not mandatory in my dataset edit ([4c9419c](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/4c9419cc1b4a5f2cb58fd84ff16268e36f25dc99))
- always get data after access dataset update ([937c7c4](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/937c7c48b991d82f0689b2329101d317756ebb28))
- bug on indicator download ([c9613b6](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/c9613b6e1572f2eba5023f825dc00fc8b69e90c5))
- bugs ([4518fbb](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/4518fbbd5b6433f00ae37deb7e9354775fbf39d8))
- bugs ([61f91c0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/61f91c0caca220dba6531c69cdb19532df02bea4))
- call organization ([1876ebf](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/1876ebfe356f6ec791e3a9493a692a0f59844e7e))
- call url api-bigdata ([135bf3f](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/135bf3f0efb36b27b3780e5349e5577575de8f60))
- call url api-bigdata - direct call ([d9d4ac3](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/d9d4ac3917d5e55c1752a1b231a864dc100c4ddc))
- cant paste on dataset create & update ([cde2857](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/cde28577039c82236f48b6f5669d5d485da19e16))
- category on indicator detail page ([4b95a41](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/4b95a416372b7204fefd22a842d11899e710d861))
- change app & app service to schema & table on dataset update ([9ff0cd5](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/9ff0cd5d76a367d3496a22145a9fe8fe6200b705))
- change counter dataset to private ([848f645](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/848f645c656eb023f52976feed0a68bad86d19d6))
- change counter dataset to private ([87c2c5f](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/87c2c5f1ebf1a72ad09bb0a5f712a725f703ab3b))
- change download csv & excel to zip on dataset ([150acdb](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/150acdb166031b0b90be0c0440dd088ea46a7267))
- change layout add & edit dataset, indicator, visualization, and detail indicator ([34fb17b](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/34fb17b9b25f946f8282b3410189ac378c96dcd8))
- change location request dataset on dataset page ([d29f9f0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/d29f9f01012481a0d2fa1ac2c66ac49f5691a174))
- change sort filter & quick access ([bed8c2f](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/bed8c2f239c3964992564ada4b4a774b76f4e49c))
- change source coredata ([1bb489b](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/1bb489baa11ae6ffa9864fd8d3b567aa81838697))
- change tslint to eslint ([173be83](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/173be83f062b3afa8c8844f9bf9638a954720a0c))
- change wording reset password ([8f69bc6](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/8f69bc69d1b23f8b5ecac9fdeec149f59905bd51))
- checkbox active in agreement modal ([5334a64](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/5334a64596e5fd645fa3f0277f50ea734950c6ce))
- checkbox active on scroll agreement ([0984f5e](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/0984f5e72e3899b1bca526250a9665e7bb2d1aa7))
- ci/cd ([065c004](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/065c00413ac4979759ec69b4e63a1cab1f097b3c))
- ci/cd ([05b437b](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/05b437b4454ca0b8be01bce0b77424eacbbd80d4))
- ci/cd log url ([d762789](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/d7627892fc2b6473dbc6081e47d33092837a5ae9))
- ci/cd staging ([da47721](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/da47721bcfe4d6189df335adef793d4868c6e8fd))
- ckeditor link & remove p, nbsp tags ([580ca52](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/580ca52257d71c0d04da937cbbecf0cd5e6ceee5))
- ckeditor link & remove p, nbsp tags ([557f906](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/557f90645a6fc1b9b76861334729c8a49002059a))
- ckeditor toolbar & remove html tags ([fb25091](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/fb250917cfa915bbee336dbe0410d3b663bcd9cc))
- content in modal agreement ([e78bc50](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/e78bc50d6fd7fa50de0cef14ec8dbe8fa423c5d9))
- counter view & access dataset ([37854df](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/37854df698649f746757a24f257448816312a1fe))
- counter visualization ([1111920](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/11119206f6bc2f18b846845895f470b9a929bf68))
- dataset detail role opd ([9f67fa6](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/9f67fa6e04523cf4a78e8014d7be4fce3276ca24))
- dataset edit ([ec72b7b](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/ec72b7b3a654b0d03994ab7cf5ead8d38e6902c5))
- dataset page - description change ([87aef59](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/87aef59f776551df1ad020a21ec2b571c052b607))
- dataset service ([3c3a3e1](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/3c3a3e1d7d447d69291cb08043173b4bebeee4db))
- dataset tag ([446f57a](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/446f57af1f5fbdc0b577d9f676e79cdf3bfc5333))
- dataset tag ([b8b9d03](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/b8b9d0344d258a86f50bd479fa87b1a29c9bb215))
- date end ([64d2347](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/64d23478480864ec43460e93565cb9548d3bff94))
- daterangepicker ([8107e4a](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/8107e4a4f95438ed54f2b729b9f407eb6c8b44bb))
- daterangepicker ([142c4ba](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/142c4bae5587ae4f573d4118c49b89c76d356492))
- delay ([028ae61](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/028ae61ec528e6732a1d3cbf5aeaad0313515569))
- delay ([2ca3f62](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/2ca3f6251232459e8a6eceb3fbace715875d96b4))
- desk approve is validate ([aa88eaa](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/aa88eaa67c03c8c0016ccf11eb7c3be300a78aaa))
- detectChanges ([2f02749](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/2f027494cb66d27c183817421a2db109d512967c))
- download on log activity ([82337ba](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/82337badb562678763665d71bb5e87cfe99e111d))
- empty metadata on dataset edit ([94487bf](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/94487bfa66b655d083a1f11525daa73b828e8539))
- empty state on organization detail ([5961389](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/5961389ac236a8642cd3936250a9ef514de4db5d))
- endpoint url generate map ([42f20c9](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/42f20c9e06db5cc7daec28d22f514227864e9ef3))
- enhancement notification ([c3d82b1](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/c3d82b1cc1270078215de6a4c62d620e78a21c17))
- environment ([f12ed09](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/f12ed09f051be53e2dda3a1e2a81549cd221b596))
- environment ([d570f4b](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/d570f4ba55a12ecf4b5d89956d8661bf75cf06f6))
- environment development ([f532bb3](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/f532bb3c933abe39cc4860893185d52e71cb8c98))
- error handling agreement ([5db98b7](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/5db98b749f55721add1652c199d80ef9d6fe1451))
- error handling on dataset add, edit, preview ([3c258d3](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/3c258d3c0aa8386ca2fb12c7a3780d8f40c15725))
- error handling user dont have role ([c6eeab5](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/c6eeab52801cd8729f5d97d03fdb1f76790c230a))
- error handling user dont have role ([c107070](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/c107070d864243aecabf50e3efe81dab96584098))
- eslint ([445c7ff](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/445c7ff563289b8f66a35686fff65f99c4f41669))
- filter search on dataset detail preview table ([0d4334c](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/0d4334cdf6f26e050e05c8de0f096833080d22d3))
- flow on dataset edit ([76e9670](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/76e96704e1e63eb1504eaac833e1bd32daa38365))
- form indicator add & edit ([87b610d](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/87b610d3e2e7dbcbaae7f224987c9968d3cf33f8))
- form indicator create & update ([59ed173](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/59ed1730ee603e119375eeddf4a056bb6dfb247c))
- gitlab ci/cd ([0963075](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/09630753450aaf4e09acdc8a7c4188a2c2bd9363))
- gitlab-ci environment ([553ac38](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/553ac381609f6f515fab6df8d85a659f0f92c699))
- gitlab-ci environment ([d6a4b03](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/d6a4b03e880edd2b5111d6d6e4f5f1c49e1c9fe7))
- guard visualization edit ([604fda4](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/604fda4266169cbf8a77e64205c95b58df4295ae))
- handle data if deleted in guard ([25f2e54](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/25f2e548a8c381bde0a538c9b03cd50e4b911046))
- handle undefined & format number on spreadsheet ([ef3bce4](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/ef3bce419443b7b36a2f71e9f50ff9553de85854))
- handling core data can access on dataset detail & edit ([c312467](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/c312467726a0ac90db72da059f131ba04c13e22b))
- handling error on dataset add, edit, & preview ([625f167](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/625f1672de399caaeae1300b8b4b305a5178abb0))
- handling error visualization ([d8daff8](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/d8daff8c5ea396ddd50725a94e33c57f85e617cc))
- header organization ([00f0eff](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/00f0efffaf534803f8329c3f55e10856559049b2))
- hidden preview graph & map if realtime in dataset detail ([c985e02](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/c985e028cf9a071e563a1787592597e5c1cf0f8a))
- hide tableau toolbar ([11711a9](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/11711a9b3cd5e80afee0cb00e1cb2f0bbf707bcb))
- homepage - trending data title redaction ([53ef7ec](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/53ef7ec7923c8113c7c368a7434f93a1dfd81705))
- indicator module ([57f36db](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/57f36db36488561a3904f1b3fc88005ae821e513))
- knowing organization in form request dataset ([cfc6adc](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/cfc6adc1a21985beea23b6e85ae39f20221e41a8))
- limit row on dataset download ([f9fea65](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/f9fea6555cbb2f0f82331f7c81cb6efebf6661df))
- load large dataset on dataset edit ([f3e54c2](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/f3e54c295bef9e9537f1ae9358b7bd4e3e156e11))
- loading on select ([5fa3724](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/5fa37245fcefc733a083f0be40de9f7c3c9e60ba))
- log page indicator ([7071f39](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/7071f3920f70e51ef975c90258075dd8a51d56c6))
- maximum budget ([4a0aaa4](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/4a0aaa4826d1211748af2dd753fb8ec376c3e7c7))
- metadata empty on dataset update ([c85d06c](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/c85d06c63bd705215861214f7d77f8bfbee31409))
- min limit row on spreadsheet ([045e2cf](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/045e2cf11bc8603572ebfec1198e95e08c7d85b2))
- navbar & my profile ([44aaa56](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/44aaa560bc008af69b99131efc7cc657e9e5b3f0))
- navbar name ([3c0d844](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/3c0d844a2dda0e9b9f8eec3c8c9043749d6961df))
- ngx-loading-bar/core ([983d599](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/983d5998a15b77d58d276d1e5eb9508f0da12a3d))
- no cache on download dataset csv ([df3738a](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/df3738a798766c05c0f198479aa5efcd8776007e))
- no mandatory organization sub in indicator add & edit page ([71d0ae8](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/71d0ae8283d2216906310f460a15b4024b003b46))
- notification icon hover color ([09703a0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/09703a06e12a1be22350923603e55ea47697ffd1))
- notification on dataset edit ([976a108](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/976a1084c0a8b1cf83cfcf674692809878d87e5a))
- notification on request dataset ([daebcdf](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/daebcdf3e670d9c172d5baa575e70f638f76f8b1))
- notification redirect ([ad5badc](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/ad5badc1beee1c632a552b31f1311a7f87a06d5b))
- option is used on form dataset create ([fc19e4d](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/fc19e4de33a3589e2e56c50294157de181780c9a))
- option is used on form dataset update ([f426c23](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/f426c2379abdfde6b4ce5b636e02b60d702fb5c5))
- organization sub on modal request dataset detail ([1d3c093](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/1d3c09377d3a243c7b44f5230293f256a8a78b2a))
- preview graph ([5385bb9](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/5385bb9a50ba725de153e9565bcf8ca6d7a48223))
- preview map url ([ade39e1](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/ade39e172fcf56647ea82e345b98f5cb062f1956))
- rebranding Satu Data Jabar ([98010b0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/98010b05aa03d00bc1e14bbbd7ad8f901c23053b))
- redirect in draft ([831df43](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/831df43d83e74eac4b7f77dda33e8507c62596fc))
- redirect in notification ([1f4649a](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/1f4649a651d2749f7a72b33856a8b91c088bd4ae))
- registry only dev ([9b774e0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/9b774e0704b37bdc1b1fc692b6e6456c9d23d189))
- registry only master ([00ed1c6](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/00ed1c69d4f47cd60c26830086a2bb249faab630))
- remove console log ([fc40f88](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/fc40f88544f4c59844c2a539101fd40e86fe573f))
- remove console.log ([d4e24c0](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/d4e24c0d2f0f677d022aa451ec00c370a8256027))
- remove log in authentication login ([67d744b](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/67d744b6e29d769d8f02f620cc1669e1fb550e89))
- remove mCustomScrollbar types ([c212edf](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/c212edf48cadcaab8bd5262a3d8c1c426fb718b0))
- remove prettier eslint html ([5a36798](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/5a36798c7719589903b3f37570bdfd7565b2ea41))
- remove preview on history draft desk walidata & opd ([679b3d7](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/679b3d71c2bfd81c61995f29f08efa159238929e))
- remove quill.min.js ([1bf11c4](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/1bf11c482677937919d708b6015ec8dae2e9c1b7))
- request dataset public action response ([eb34046](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/eb34046e7d03ca8b88a9e503452bb1361094a4b7))
- reset password ([826bf7b](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/826bf7bf7cf4fa7bd4cd75edf6ddbb6e3a133542))
- response create & change state to get id on my indicator preview ([f86056d](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/f86056d626bc304cc6f479f22f9c8aa912225207))
- revision dataset on desk walidata/organization ([abe9778](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/abe97785c9f69716125b2c65ee7a639ad4a29b34))
- row-eq-height on list indicator & visualization ([c8d9d28](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/c8d9d289eb9d4a32c434ff090f06100fb06cb516))
- satudataUser: LoginData; ([0944c36](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/0944c368b70c95b1bdbca75d4d39361c06996d23))
- search in history page ([21055a5](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/21055a505d86a3c682f6e77e22b27dd757f2a880))
- select group on detail dataset preview map ([bc0063b](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/bc0063b09a7a979267718139ca53065bbae896d3))
- select null on dataset detail preview table ([50c4e77](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/50c4e77af4cb7d61fd272b0f4bde2c398c95a211))
- sentry ([a1b9a2f](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/a1b9a2f2bc40e2b4dfb383afdcad9762b09ae576))
- sentry dialog, access edit visualization, & notification redirect ([59efea7](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/59efea7accd44c06551b47c690656eb9d63662fe))
- show achievement if finalization true ([178f969](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/178f969ea3e06601b43e77f98543f1ef2325ddda))
- social media on footer ([b04744c](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/b04744c283e3b9ba3beb6d0e7650a6f53778a854))
- socket ([f574de9](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/f574de96ff1316350a4d8eccbb5952eb4fc239a9))
- socket notification ([7f1cdc9](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/7f1cdc94f61cad9e400fdf960c267c59ef960ab4))
- sourceMap ([f657d3d](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/f657d3d04a2676d7fd862651baea2671ae5ad5b2))
- spreadsheet row value undefined ([b4e7a90](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/b4e7a90397fca11aff141e58f791b4c2690194f0))
- stage gitlab-ci ([a1cc277](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/a1cc277c9228b9a9b32569deffb8f7ba0cc1f724))
- statistic on home page ([9c309b7](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/9c309b7859fe0aa38e847213e50d9f70e39ad784))
- swal fire - text typo ([d9859dd](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/d9859dd2374dcf325b9fbfac1f007f552c4a577c))
- token missing on dataset edit ([dd988b5](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/dd988b5223f16008437a15ec26b150e394b19710))
- topic is_deleted false & tag dataset detail ([dbcd6dd](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/dbcd6dd24992797ba8a4ffbb41502adf278cb434))
- total dataset on home ([6fac9d5](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/6fac9d54d0c940da1d8cb36c91a967c2c1fcbab5))
- total organization in home statistic ([85a6ac8](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/85a6ac8a03166a79fc0095e215e0412a81bd5954))
- total record list dataset ([2a591b3](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/2a591b3ddf7eb55914f500bc98b57ef700d91f56))
- total record list indicator ([fa3437e](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/fa3437e7495b3e720532b420c4a84a85f751f61d))
- typo - pencaian ([73fe5db](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/73fe5dbafeac68e3a1a35d87b6a77c51148b8bb5))
- url api log ([36fa77a](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/36fa77a2a64eec24a09bba0775f15c1e2b08db41))
- url proxy to https ([9e45232](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/9e452328a183d1790c3927adf5cbb848a68bf8dd))
- user permission not found ([d34ed15](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/d34ed15712bf70d52a7379edf080172d33268c8f))
- withlist domain ([5c54059](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/5c54059470bd7b25d243090304dfde8c4bdee788))
- wording ([05c5580](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/05c558057a4880851b0e11e304a7cfca5d5e7d37))
- wording & date in request dataset private ([6079fca](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/6079fca99da7abc39c74aade6d78e71501642307))
- wording handling core data can access on dataset detail & edit ([e397767](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/e397767186e8df4532016950c429ef9ec411f56a))
- wording in popup download dataset ([12cf01e](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/12cf01eaa5dd957be478a45a9ade161961205e10))
- workers ([12d80e4](https://gitlab.com/jdsteam/satu-data-jawa-barat/satudata-frontend/commit/12d80e489a0fa2671243525ef6a95c89a4df1f82))
